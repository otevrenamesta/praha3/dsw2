<?php

use Migrations\AbstractMigration;

class SettlementItemsAddNote extends AbstractMigration
{
    public function change()
    {
        $settlementItems = $this->table('settlement_items');
        if (!$settlementItems->hasColumn('note')) {
            $settlementItems->addColumn('note', 'string', [
                'after' => 'description',
                'null' => true,
                'default' => null,
                'limit' => 255
            ]);
        }
        $settlementItems->update();
    }
}
