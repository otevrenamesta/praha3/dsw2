<?php

use Migrations\AbstractMigration;

class InitMigration extends AbstractMigration
{
    public function change()
    {
        $this->execute(file_get_contents(ROOT . DS . "docs" . DS . "sql" . DS . "schema.sql"));
        # insert example domain and organization
        $this->execute("INSERT INTO `organizations` (`id`, `name`, `modified`, `created`) VALUES (1,'Ukázková organizace',NOW(),NOW()) ON DUPLICATE KEY UPDATE id=id;");
        $this->execute("INSERT INTO `domains` (`organization_id`, `domain`, `is_enabled`, `modified`, `created`) VALUES ('1', 'localhost', '1', NOW(), NOW()) ON DUPLICATE KEY UPDATE id=id;");
    }
}