<?php
declare(strict_types=1);


use Migrations\AbstractMigration;

class PaperSubmissionLock extends AbstractMigration
{

    public function up()
    {
        // add new state
        $requestStates = $this->table('request_states');
        $requestStates->insert([
            'id' => 14,
            'name' => 'Papírové podání - zamčeno'
        ]);
        $requestStates->saveData();

        $requests = $this->table('requests');
        $requests->addColumn('verification_code', 'string', [
            'null' => true,
            'default' => null,
            'after' => 'lock_comment',
            'limit' => 32
        ])->update();
    }

    public function down()
    {
        // lock state id
        $this->execute('DELETE FROM request_states WHERE id=14');
        // remove appropriate column
        $requests = $this->table('requests');
        if ($requests->hasColumn('verification_code')) {
            $requests->removeColumn('verification_code')->save();
        }
    }

}