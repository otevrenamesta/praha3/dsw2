<?php

use Migrations\AbstractMigration;

class UpdateProgramsAddBudgetHideEarnings extends AbstractMigration
{
    public function change()
    {
        $budgetItems = $this->table('programs');
        if (!$budgetItems->hasColumn('budget_hide_earnings')) {
          $budgetItems->addColumn('budget_hide_earnings', 'boolean', [
              'default' => 0,
              'null' => false,
              'after' => 'requires_budget'
          ]);
        }
        $budgetItems->update();
    }
}
