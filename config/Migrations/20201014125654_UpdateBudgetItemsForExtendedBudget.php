<?php

use Migrations\AbstractMigration;

class UpdateBudgetItemsForExtendedBudget extends AbstractMigration
{
    public function change()
    {
        $budgetItems = $this->table('budget_items');
        if (!$budgetItems->hasColumn('original_amount')) {
          $budgetItems->addColumn('original_amount', 'decimal', [
            'precision' => 18,
            'scale' => 2,
            'default' => 0,
            'null' => false,
            'after' => 'amount'
          ]);
        }
        if (!$budgetItems->hasColumn('comment')) {
          $budgetItems->addColumn('comment', 'string', [
            'after' => 'description',
            'null' => true,
            'default' => null,
            'limit' => 255
          ]);
        }
        $budgetItems->update();
    }
}
