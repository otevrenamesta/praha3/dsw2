<?php

use Migrations\AbstractMigration;

class UpdateFondsRealmsAddIsHidden extends AbstractMigration
{
    public function change()
    {
        $fonds = $this->table('fonds');
        if (!$fonds->hasColumn('is_hidden')) {
            $fonds->addColumn('is_hidden', 'boolean', [
                'default' => false,
                'null' => false,
                'after' => 'is_enabled',
            ]);
            $fonds->update();
        }

        $realms = $this->table('realms');
        if (!$realms->hasColumn('is_hidden')) {
            $realms->addColumn('is_hidden', 'boolean', [
                'default' => false,
                'null' => false,
                'after' => 'is_enabled',
            ]);
            $realms->update();
        }
    }
}
