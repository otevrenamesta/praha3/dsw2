<?php

use Migrations\AbstractMigration;

class UpdateProgramsAddExtendedCosts extends AbstractMigration
{
    public function change()
    {
        $programs = $this->table('programs');
        if (!$programs->hasColumn('requires_extended_budget')) {
            $programs->addColumn('requires_extended_budget', 'boolean', [
                'default' => false,
                'null' => false,
                'after' => 'requires_balance_sheet'
            ]);
            $programs->update();
        }
    }
}
