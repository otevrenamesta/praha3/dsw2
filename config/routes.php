<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

const REGEX_POSITIVE_NUMBER = '[1-9][0-9]*';
const REGEX_ANY_NUMBER = '[0-9]+';
// appeal:program,appeal:program,...
const APPEAL_PROGRAM_FILTER_SPEC = '([0-9]+:[0-9]+,?)+';


Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // CSRF
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
        'secure' => true,
        'cookieName' => '__Host-dswCSRF',
    ]));

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Public', 'action' => 'index']);
    $routes->connect('/favicon.ico', ['controller' => 'Public', 'action' => 'favicon']);
    $routes->connect('/explore/fonds', ['controller' => 'Public', 'action' => 'exploreFonds'], ['_name' => 'public_fonds']);
    $routes->connect('/explore/appeals', ['controller' => 'Public', 'action' => 'exploreAppeals'], ['_name' => 'public_appeals']);
    $routes->connect('/podpora', ['controller' => 'Public', 'action' => 'wiki'], ['_name' => 'public_wiki']);
    $routes->connect('/about', ['controller' => 'Public', 'action' => 'about']);
    //$routes->connect('/about_new', ['controller' => 'Public', 'action' => 'aboutNew']);
    $routes->connect('/accessibility', ['controller' => 'Public', 'action' => 'accessibility']);
    $routes->connect('/gdpr', ['controller' => 'Public', 'action' => 'gdpr']);
    $routes->connect('/old-browser', ['controller' => 'Public', 'action' => 'oldBrowser']);
    $routes->connect('/admin/docs', ['controller' => 'Public', 'action' => 'adminDocs'], ['_name' => 'admin_docs']);
    $routes->connect('/opendata', ['controller' => 'OpenData', 'action' => 'index'], ['_name' => 'open_data']);
    $routes->connect('/opendata/:id', ['controller' => 'OpenData', 'action' => 'openDataDetail'], ['_name' => 'open_data_detail', 'id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/opendata/:id/download_file/:file_id', ['controller' => 'OpenData', 'action' => 'openDataPublicFile'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login'], ['_name' => 'login']);
    $routes->connect('/process_nia_login', ['controller' => 'Users', 'action' => 'processNiaLogin'], ['_name' => 'process_nia_login']);
    $routes->connect('/process_nia_logout', ['controller' => 'Users', 'action' => 'processNiaLogout'], ['_name' => 'process_nia_logout']);
    $routes->connect('/user/logout', ['controller' => 'Users', 'action' => 'logout'], ['_name' => 'logout']);
    $routes->connect('/register', ['controller' => 'Users', 'action' => 'register'], ['_name' => 'register']);
    $routes->connect('/new_email/:token/:requestId', ['controller' => 'Users', 'action' => 'newEmail'], ['_name' => 'new_email']);
    $routes->connect('/verify_email/:token/:requestId', ['controller' => 'Users', 'action' => 'verifyEmail'], ['_name' => 'verify_email']);
    $routes->connect('/invite_accept/:token/:requestId', ['controller' => 'Users', 'action' => 'inviteAccept'], ['_name' => 'invite_accept']);
    $routes->connect('/password_recovery', ['controller' => 'Users', 'action' => 'passwordRecovery'], ['_name' => 'password_recovery']);
    $routes->connect('/password_recovery_action/:token/:requestId', ['controller' => 'Users', 'action' => 'passwordRecoveryGo'], ['_name' => 'password_recovery_go']);

    $routes->connect('/user/change_email', ['controller' => 'Users', 'action' => 'changeEmail'], ['_name' => 'change_email']);
    $routes->connect('/user/change_password', ['controller' => 'Users', 'action' => 'changePassword'], ['_name' => 'change_password']);
    $routes->connect('/user/notifications', ['controller' => 'Users', 'action' => 'changeNotifications'], ['_name' => 'notifications']);
    $routes->connect('/user/share_my_account', ['controller' => 'Users', 'action' => 'shareMyAccount'], ['_name' => 'share_my_account']);
    $routes->connect('/user/switch_to_allowed/:id', ['controller' => 'Users', 'action' => 'switchToAllowed'], ['pass' => ['id'], '_name' => 'switch_to_user']);
    $routes->connect('/user/update_info/public_income', ['controller' => 'Users', 'action' => 'publicIncomeHistories'], ['_name' => 'update_self_history']);
    $routes->connect('/user/update_info/public_income/:id/delete', ['controller' => 'Users', 'action' => 'incomeHistoryDelete'], ['pass' => ['id']]);
    $routes->connect('/user/update_info', ['controller' => 'Users', 'action' => 'changeInfo'], ['_name' => 'update_self_info']);
    $routes->connect('/user/update_info/ares/:ico', ['controller' => 'Users', 'action' => 'aresFetch', 'type' => 'bas'], ['_name' => 'ares', 'pass' => ['ico', 'type'], 'ico' => REGEX_ANY_NUMBER]);
    $routes->connect('/user/update_info/ares/or/:ico', ['controller' => 'Users', 'action' => 'aresFetch', 'type' => 'or'], ['_name' => 'ares_or', 'pass' => ['ico', 'type'], 'ico' => REGEX_ANY_NUMBER]);
    $routes->connect('/user/:id/download_file/:file_id', ['controller' => 'Users', 'action' => 'identityAttachmentFile'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/toggle_file/:file_id', ['controller' => 'Users', 'action' => 'identityAttachmentToggle'], ['pass' => ['file_id'], 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/delete_file/:file_id', ['controller' => 'Users', 'action' => 'identityAttachmentDelete'], ['pass' => ['file_id'], 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/user/requests/old/:id', ['controller' => 'UserRequests', 'action' => 'dswZadost'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/old/:id/ohlasit', ['controller' => 'UserRequests', 'action' => 'dswOhlasit'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    // create and link new settlement to old zadost
    $routes->connect('/user/requests/old/:id/vyuctovani/create', ['controller' => 'UserRequests', 'action' => 'dswVyuctovani'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/user/requests', ['controller' => 'UserRequests', 'action' => 'index'], ['_name' => 'user_requests']);
    $routes->connect('/user/requests/add', ['controller' => 'UserRequests', 'action' => 'addModify']);
    $routes->connect('/user/requests/:id', ['controller' => 'UserRequests', 'action' => 'requestDetail'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER, '_name' => 'request_detail']);
    $routes->connect('/user/requests/:id/pdf', ['controller' => 'UserRequests', 'action' => 'getPdf'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/pdf_view', ['controller' => 'UserRequests', 'action' => 'getPdfView'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/lock_for_submission/lock', ['controller' => 'UserRequests', 'action' => 'lockUnlockRequest', 'do' => 'lock'], ['pass' => ['id', 'do'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/lock_for_submission/lock_and_download', ['controller' => 'UserRequests', 'action' => 'lockUnlockRequest', 'do' => 'lockAndDownload'], ['pass' => ['id', 'do'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/lock_for_submission/unlock', ['controller' => 'UserRequests', 'action' => 'lockUnlockRequest', 'do' => 'unlock'], ['pass' => ['id', 'do'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/lock_for_submission', ['controller' => 'UserRequests', 'action' => 'lockUnlockRequest'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/verification_pdf', ['controller' => 'UserRequests', 'action' => 'downloadRequestVerificationPdf'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/submit/databox', ['controller' => 'UserRequests', 'action' => 'requestSubmitDatabox'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/submit/databox/result', ['controller' => 'UserRequests', 'action' => 'requestSubmitDataboxResult'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/delete', ['controller' => 'UserRequests', 'action' => 'requestDelete'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/budget', ['controller' => 'UserRequests', 'action' => 'requestBudget'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER, '_name' => 'request_budget']);
    $routes->connect('/user/requests/:id/budgetchange', ['controller' => 'UserRequests', 'action' => 'requestBudgetChange'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER, '_name' => 'request_budget_change']);
    $routes->connect('/user/requests/:id/settings', ['controller' => 'UserRequests', 'action' => 'addModify'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER, '_name' => 'request_settings']);
    $routes->connect('/user/requests/:id/fill_form/:form_id', ['controller' => 'UserRequests', 'action' => 'formFill'], ['pass' => ['id', 'form_id'], 'id' => REGEX_POSITIVE_NUMBER, 'form_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/fill_form_change/:form_id', ['controller' => 'UserRequests', 'action' => 'formFillChange'], ['pass' => ['id', 'form_id'], 'id' => REGEX_POSITIVE_NUMBER, 'form_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/reset_form/:form_id', ['controller' => 'UserRequests', 'action' => 'formFillReset'], ['pass' => ['id', 'form_id'], 'id' => REGEX_POSITIVE_NUMBER, 'form_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/upload', ['controller' => 'UserRequests', 'action' => 'attachmentAdd'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/attachment_delete/:file_id', ['controller' => 'UserRequests', 'action' => 'attachmentDelete'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/attachment/:file_id/download', ['controller' => 'UserRequests', 'action' => 'fileDownload'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/file_delete/:file_id/:form_id/:field_id', ['controller' => 'UserRequests', 'action' => 'formFileDelete'], ['pass' => ['id', 'file_id', 'field_id', 'form_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER, 'field_id' => REGEX_POSITIVE_NUMBER, 'form_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/file/:file_id/download', ['controller' => 'UserRequests', 'action' => 'formFileDownload'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/user/requests/:id/changerequest/', ['controller' => 'UserRequests', 'action' => 'requestChange'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER,]);
    // create and link new settlement to new request
    $routes->connect('/user/requests/:id/settlement/create/:payment_id', ['controller' => 'UserRequests', 'action' => 'settlementCreate'], ['pass' => ['id', 'payment_id'], 'id' => REGEX_POSITIVE_NUMBER, 'payment_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/settlement/create', ['controller' => 'UserRequests', 'action' => 'settlementCreate'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    // notifications / ohlášení akce, díla nebo celoroční činnosti
    $routes->connect('/user/requests/:id/notifications', ['controller' => 'UserRequests', 'action' => 'notifications'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/notifications/create', ['controller' => 'UserRequests', 'action' => 'notificationCreate'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);

    // ChangeRequest for user
    $routes->connect('/user/requests/:id/request_form_change_view', ['controller' => 'ChangeRequest', 'action' => 'requestFormChangeView'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/requests/:id/request_budget_change_view', ['controller' => 'ChangeRequest', 'action' => 'requestBudgetChangeView'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);

    // general settlement routes
    $routes->connect('/user/settlement/:settlement_id', ['controller' => 'UserRequests', 'action' => 'settlementDetail'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/delete', ['controller' => 'UserRequests', 'action' => 'settlementDelete'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/item/:id', ['controller' => 'UserRequests', 'action' => 'settlementItemAddModify'], ['pass' => ['settlement_id', 'id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/item/delete/:id', ['controller' => 'UserRequests', 'action' => 'settlementItemDelete'], ['pass' => ['settlement_id', 'id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/item/create', ['controller' => 'UserRequests', 'action' => 'settlementItemAddModify'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, '_name' => 'create_settlement_item']);
    $routes->connect('/user/settlement/:settlement_id/delete_file/:file_id', ['controller' => 'UserRequests', 'action' => 'settlementDeleteFile'], ['pass' => ['settlement_id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'settlement_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/attachment/:file_id', ['controller' => 'UserRequests', 'action' => 'settlementAddModifyFile'], ['pass' => ['settlement_id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/upload_file', ['controller' => 'UserRequests', 'action' => 'settlementAddModifyFile'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/upload_file/report', ['controller' => 'UserRequests', 'action' => 'settlementAddModifyFile', 'type' => 'report'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, '_name' => 'settlement_new_attachment_report']);
    $routes->connect('/user/settlement/:settlement_id/upload_file/publicity', ['controller' => 'UserRequests', 'action' => 'settlementAddModifyFile', 'type' => 'publicity'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, '_name' => 'settlement_new_attachment_publicity']);

    $routes->connect('/user/settlement/:settlement_id/download_file/:file_id', ['controller' => 'UserRequests', 'action' => 'settlementDownloadFile'], ['pass' => ['settlement_id', 'file_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/view_file/:file_id', ['controller' => 'UserRequests', 'action' => 'settlementViewFile'], ['pass' => ['settlement_id', 'file_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/pdf', ['controller' => 'UserRequests', 'action' => 'settlementPdf'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/pdf_view', ['controller' => 'UserRequests', 'action' => 'settlementPdfView'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/return_full_subsidy', ['controller' => 'UserRequests', 'action' => 'settlementReturn'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/submit/databox', ['controller' => 'UserRequests', 'action' => 'settlementSubmitDatabox'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/submit/databox/result', ['controller' => 'UserRequests', 'action' => 'settlementSubmitDataboxResult'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/user/settlement/:settlement_id/submit/directly', ['controller' => 'UserRequests', 'action' => 'settlementSubmitDirectly'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/user/isds_auth', ['controller' => 'Isds', 'action' => 'authRedirect'], ['_name' => 'isds_auth']);
    $routes->connect('/user/isds_auth/:sessionId', ['controller' => 'Isds', 'action' => 'authCallback'], ['_name' => 'isds_auth_callback', 'pass' => ['sessionId']]);
    $routes->connect('/user/after_isds_auth', ['controller' => 'Isds', 'action' => 'storeVerifiedAttributes']);
    $routes->connect('/user/isds_test', ['controller' => 'Isds', 'action' => 'testISDS']);

    $routes->connect('/admin/organizations', ['controller' => 'Organizations', 'action' => 'index'], ['_name' => 'admin_organizations']);
    $routes->connect('/admin/organizations/create', ['controller' => 'Organizations', 'action' => 'create']);
    $routes->connect('/admin/organizations/edit', ['controller' => 'Organizations', 'action' => 'edit']);
    $routes->connect('/admin/organizations/edit/:id', ['controller' => 'Organizations', 'action' => 'edit'], ['pass' => ['id']]);
    $routes->connect('/admin/organizations/delete/:id', ['controller' => 'Organizations', 'action' => 'delete'], ['pass' => ['id']]);

    $routes->connect('/admin/public_income_sources', ['controller' => 'PublicIncomeSources', 'action' => 'index'], ['_name' => 'admin_public_income_sources']);
    $routes->connect('/admin/public_income_sources/:id', ['controller' => 'PublicIncomeSources', 'action' => 'addModify'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/public_income_sources/:id/delete', ['controller' => 'PublicIncomeSources', 'action' => 'delete'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/public_income_sources/add', ['controller' => 'PublicIncomeSources', 'action' => 'addModify']);

    $routes->connect('/admin/organizations/:org_id/domain/add', ['controller' => 'Organizations', 'action' => 'domainAdd'], ['pass' => ['org_id']]);
    $routes->connect('/admin/organizations/:org_id/domain/delete/:id', ['controller' => 'Organizations', 'action' => 'domainDelete'], ['pass' => ['org_id', 'id']]);
    $routes->connect('/admin/organizations/:org_id/domain/trigger/:id', ['controller' => 'Organizations', 'action' => 'domainTrigger'], ['pass' => ['org_id', 'id']]);

    $routes->connect('/admin/users', ['controller' => 'UsersManager', 'action' => 'index'], ['_name' => 'admin_users']);
    $routes->connect('/admin/users/invite', ['controller' => 'UsersManager', 'action' => 'invite'], ['_name' => 'admin_users_invite']);
    $routes->connect('/admin/users/:id/edit', ['controller' => 'UsersManager', 'action' => 'edit'], ['_name' => 'admin_users_edit', 'pass' => ['id']]);
    $routes->connect('/admin/users/:id/old_link', ['controller' => 'UsersManager', 'action' => 'oldLink'], ['pass' => ['id']]);
    $routes->connect('/admin/users/:id/role_delete/:role_id', ['controller' => 'UsersManager', 'action' => 'roleDelete'], ['pass' => ['id', 'role_id']]);
    $routes->connect('/admin/users/:id/role_add', ['controller' => 'UsersManager', 'action' => 'roleAdd'], ['pass' => ['id']]);
    $routes->connect('/admin/users/:id/login', ['controller' => 'UsersManager', 'action' => 'loginAs'], ['pass' => ['id']]);

    $routes->connect('/admin/teams', ['controller' => 'TeamsManager', 'action' => 'index'], ['_name' => 'admin_teams']);
    $routes->connect('/admin/teams/add', ['controller' => 'TeamsManager', 'action' => 'addModify']);
    $routes->connect('/admin/teams/:id', ['controller' => 'TeamsManager', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/teams/:id/delete', ['controller' => 'TeamsManager', 'action' => 'teamDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/teams/:id/copy', ['controller' => 'TeamsManager', 'action' => 'teamCopy'], ['pass' => ['id']]);

    $routes->connect('/csu/countries', ['controller' => 'Csu', 'action' => 'countries'], ['_name' => 'csu_countries']);
    $routes->connect('/csu/countries.json', ['controller' => 'Csu', 'action' => 'countries', 'json'], ['_name' => 'csu_countries_json']);
    $routes->connect('/csu/legal_forms', ['controller' => 'Csu', 'action' => 'legalForms'], ['_name' => 'csu_legal_forms']);
    $routes->connect('/csu/legal_forms.json', ['controller' => 'Csu', 'action' => 'legalForms', 'json'], ['_name' => 'csu_legal_forms_json']);
    $routes->connect('/csu/municipalities', ['controller' => 'Csu', 'action' => 'municipalities'], ['_name' => 'csu_municipalities']);
    $routes->connect('/csu/municipalities.json', ['controller' => 'Csu', 'action' => 'municipalities', 'json'], ['_name' => 'csu_municipalities_json']);
    $routes->connect('/csu/municipality_parts', ['controller' => 'Csu', 'action' => 'municipalityParts'], ['_name' => 'csu_municipality_parts']);
    $routes->connect('/csu/municipality_parts.json', ['controller' => 'Csu', 'action' => 'municipalityParts', 'json'], ['_name' => 'csu_municipality_parts_json']);
    $routes->connect('/csu/municipality_parts/list.json', ['controller' => 'Csu', 'action' => 'municipalityPartsList'], ['_name' => 'csu_municipality_parts_list_json']);

    $routes->connect('/admin/grants/fonds', ['controller' => 'GrantsManager', 'action' => 'indexFonds'], ['_name' => 'admin_fonds']);
    $routes->connect('/admin/grants/fonds/add', ['controller' => 'GrantsManager', 'action' => 'fondAddModify']);
    $routes->connect('/admin/grants/fonds/:id', ['controller' => 'GrantsManager', 'action' => 'fondAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/copy', ['controller' => 'GrantsManager', 'action' => 'fondCopy'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'fondToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/fonds/:id/delete', ['controller' => 'GrantsManager', 'action' => 'fondDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/realms', ['controller' => 'GrantsManager', 'action' => 'indexRealms'], ['_name' => 'admin_realms']);
    // adding realm to fond
    $routes->connect('/admin/grants/realms/add/for_fond/:fond_id', ['controller' => 'GrantsManager', 'action' => 'realmAddModify']);
    $routes->connect('/admin/grants/realms/add', ['controller' => 'GrantsManager', 'action' => 'realmAddModify']);
    $routes->connect('/admin/grants/realms/:id', ['controller' => 'GrantsManager', 'action' => 'realmAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/realms/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'realmToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/realms/:id/delete', ['controller' => 'GrantsManager', 'action' => 'realmDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/programs', ['controller' => 'GrantsManager', 'action' => 'indexPrograms'], ['_name' => 'admin_programs']);
    // adding top-level program to realm
    $routes->connect('/admin/grants/programs/add/for_realm/:realm_id', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    // adding child program to parent
    $routes->connect('/admin/grants/programs/add/for_program/:parent_id', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    $routes->connect('/admin/grants/programs/add', ['controller' => 'GrantsManager', 'action' => 'programAddModify']);
    $routes->connect('/admin/grants/programs/:id', ['controller' => 'GrantsManager', 'action' => 'programAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/programs/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'programToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/programs/:id/delete', ['controller' => 'GrantsManager', 'action' => 'programDelete'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/appeals', ['controller' => 'GrantsManager', 'action' => 'indexAppeals'], ['_name' => 'admin_appeals']);
    $routes->connect('/admin/grants/appeals/add', ['controller' => 'GrantsManager', 'action' => 'appealAddModify']);
    $routes->connect('/admin/grants/appeals/:id', ['controller' => 'GrantsManager', 'action' => 'appealAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/details', ['controller' => 'GrantsManager', 'action' => 'appealDetailSettings'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/toggle', ['controller' => 'GrantsManager', 'action' => 'appealToggle'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/delete', ['controller' => 'GrantsManager', 'action' => 'appealDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/beginning', ['controller' => 'GrantsManager', 'action' => 'appealIsBeginning'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/appeals/:id/ending', ['controller' => 'GrantsManager', 'action' => 'appealIsEnding'], ['pass' => ['id']]);

    $routes->connect('/admin/grants/evaluation_criteria', ['controller' => 'EvaluationCriteria', 'action' => 'index'], ['_name' => 'admin_evaluation_criteria']);
    $routes->connect('/admin/grants/evaluation_criteria/add/for/:parent_id', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify']);
    $routes->connect('/admin/grants/evaluation_criteria/add', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify']);
    $routes->connect('/admin/grants/evaluation_criteria/:id', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/evaluation_criteria/:id/delete', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/grants/evaluation_criteria/:id/copy', ['controller' => 'EvaluationCriteria', 'action' => 'criteriumCopy'], ['pass' => ['id']]);

    $routes->connect('/admin/my_teams', ['controller' => 'Teams', 'action' => 'index'], ['_name' => 'my_teams']);
    $routes->connect('/admin/my_teams/request_attachment/:request_id/:file_id', ['controller' => 'Teams', 'action' => 'downloadAttachment'], ['pass' => ['request_id', 'file_id'], 'file_id' => '\d+', 'request_id' => '\d+']);
    $routes->connect('/admin/my_teams/request_attachment/:request_id/:file_id/:form_id/:field_id', ['controller' => 'Teams', 'action' => 'downloadAttachment'], ['pass' => ['request_id', 'file_id', 'form_id', 'field_id'], 'file_id' => '\d+', 'request_id' => '\d+', 'form_id' => '\d+', 'field_id' => '\d+', '_name' => 'my_teams_download_form_attachment']);
    $routes->connect('/admin/my_teams/formal_control', ['controller' => 'Teams', 'action' => 'indexFormalControl'], ['_name' => 'team_formal_control_index']);
    $routes->connect('/admin/my_teams/formal_control/pdf/:id', ['controller' => 'Teams', 'action' => 'getRequestPdf'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/formal_control/pdf_view/:id', ['controller' => 'Teams', 'action' => 'getRequestPdfView'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/formal_control/manual_submit/:id', ['controller' => 'Teams', 'action' => 'formalControlManualSubmit'], ['_name' => 'formal_control_manual_submit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/unlock_with_code/:id', ['controller' => 'Teams', 'action' => 'formalControlUnlockWithCode'], ['_name' => 'formal_control_unlock_with_code', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/review/:id', ['controller' => 'Teams', 'action' => 'formalControlCheckRequest'], ['_name' => 'formal_control_check_request', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/send_email/:id', ['controller' => 'Teams', 'action' => 'formalControlSendEmail'], ['_name' => 'formal_control_send_notice', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/formal_control/paper/:id', ['controller' => 'Teams', 'action' => 'formalControlAddModifyPaperRequest'], ['_name' => 'formal_control_paper_modify', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/formal_control/paper/:id/delete', ['controller' => 'Teams', 'action' => 'formalControlDeletePaperRequest'], ['_name' => 'formal_control_paper_delete', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/formal_control/paper/new', ['controller' => 'Teams', 'action' => 'formalControlAddModifyPaperRequest'], ['_name' => 'formal_control_paper_add']);
    $routes->connect('/admin/my_teams/comments', ['controller' => 'Teams', 'action' => 'indexComments'], ['_name' => 'team_evaluators_index']);
    $routes->connect('/admin/my_teams/comments/evaluate/:id', ['controller' => 'Teams', 'action' => 'commentsEvaluate'], ['_name' => 'comments_evaluate', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/proposals', ['controller' => 'Teams', 'action' => 'indexProposals'], ['_name' => 'team_proposals_index']);
    $routes->connect('/admin/my_teams/proposals/meeting', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'price_proposal_programs'], ['_name' => 'proposals_meeting']);
    $routes->connect('/admin/my_teams/proposals/meeting/:filter', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'price_proposal_programs'], ['_name' => 'proposals_meeting_filter', 'filter' => APPEAL_PROGRAM_FILTER_SPEC]);
    $routes->connect('/admin/my_teams/proposals/:id', ['controller' => 'Teams', 'action' => 'proposalsEdit'], ['_name' => 'proposals_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/approvals', ['controller' => 'Teams', 'action' => 'indexApprovals'], ['_name' => 'team_approvals_index']);
    $routes->connect('/admin/my_teams/approvals/meeting', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'price_approval_programs'], ['_name' => 'approvals_meeting']);
    $routes->connect('/admin/my_teams/approvals/meeting/:filter', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'price_approval_programs'], ['_name' => 'approvals_meeting_filter', 'filter' => APPEAL_PROGRAM_FILTER_SPEC]);
    $routes->connect('/admin/my_teams/approvals/:id', ['controller' => 'Teams', 'action' => 'approvalsEdit'], ['_name' => 'approvals_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/managers', ['controller' => 'Teams', 'action' => 'indexManagers'], ['_name' => 'team_managers_index']);
    $routes->connect('/admin/my_teams/managers/meeting', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'request_manager_programs'], ['_name' => 'managers_meeting']);
    $routes->connect('/admin/my_teams/managers/meeting/:filter', ['controller' => 'Teams', 'action' => 'assemblyMeetingInterface', 'request_manager_programs'], ['_name' => 'managers_meeting_filter', 'filter' => APPEAL_PROGRAM_FILTER_SPEC]);
    $routes->connect('/admin/my_teams/managers/:id', ['controller' => 'Teams', 'action' => 'managersEdit'], ['_name' => 'managers_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/preview', ['controller' => 'Teams', 'action' => 'indexPreview'], ['_name' => 'team_preview_index']);
    $routes->connect('/admin/my_teams/request_detail/:id', ['controller' => 'Teams', 'action' => 'requestDetailReadOnly'], ['_name' => 'my_teams_request_detail', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_detail/:id/notifications', ['controller' => 'Teams', 'action' => 'requestDetailNotificationsOnly'], ['_name' => 'my_teams_request_detail_notifications', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_detail/:id/send_email', ['controller' => 'Teams', 'action' => 'sendEmail'], ['_name' => 'my_teams_send_email', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/changerequests', ['controller' => 'ChangeRequest', 'action' => 'index'], ['_name' => 'change_request_index']);
    $routes->connect('/admin/my_teams/request_form_change_detail/:id', ['controller' => 'ChangeRequest', 'action' => 'requestFormChangeDetail'], ['_name' => 'request_form_change_detail', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_form_change_accept/:id', ['controller' => 'ChangeRequest', 'action' => 'requestFormChangeAccept'], ['_name' => 'request_form_change_accept', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_form_change_decline/:id', ['controller' => 'ChangeRequest', 'action' => 'requestFormChangeDecline'], ['_name' => 'request_form_change_decline', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_budget_change_detail/:id', ['controller' => 'ChangeRequest', 'action' => 'requestBudgetChangeDetail'], ['_name' => 'request_budget_change_detail', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_budget_change_accept/:id', ['controller' => 'ChangeRequest', 'action' => 'requestBudgetChangeAccept'], ['_name' => 'request_budget_change_accept', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/request_budget_change_decline/:id', ['controller' => 'ChangeRequest', 'action' => 'requestBudgetChangeDecline'], ['_name' => 'request_budget_change_decline', 'pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/admin/my_teams/admin_section/:id', ['controller' => 'Teams', 'action' => 'adminSectionEdit'], ['_name' => 'admin_section_edit', 'pass' => ['id']]);
    $routes->connect('/admin/my_teams/admin_section/:id/attachment_delete/:file_id', ['controller' => 'Teams', 'action' => 'attachmentDelete'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/my_teams/admin_section/:id/attachment/:file_id/download', ['controller' => 'Teams', 'action' => 'fileDownload'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/admin/my_teams/messages', ['controller' => 'Teams', 'action' => 'indexMessages'], ['_name' => 'my_teams_messages']);
    $routes->connect('/admin/my_teams/messages/:id/delete', ['controller' => 'Teams', 'action' => 'indexMessagesDelete'], ['_name' => 'my_teams_messages_delete', 'pass' => ['id']]);

    $routes->connect('/admin/economics', ['controller' => 'Economics', 'action' => 'index'], ['_name' => 'my_economics']);
    $routes->connect('/admin/economics/payout/:id', ['controller' => 'Economics', 'action' => 'doPayout'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/payout/:request_id/deletePayment/:payment_id', ['controller' => 'Economics', 'action' => 'deletePayment'], ['pass' => ['request_id', 'payment_id'], 'request_id' => REGEX_POSITIVE_NUMBER, 'payment_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/payout/:payment_id/download_file/:file_id', ['controller' => 'Economics', 'action' => 'additionalFinancingDownloadFile'], ['pass' => ['payment_id', 'file_id'], 'payment_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/payout/:payment_id/view_file/:file_id', ['controller' => 'Economics', 'action' => 'additionalFinancingViewFile'], ['pass' => ['payment_id', 'file_id'], 'payment_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/admin/economics/settlements', ['controller' => 'Economics', 'action' => 'settlements'], ['_name' => 'my_economic_settlements']);
    $routes->connect('/admin/economics/settlements/:settlement_id', ['controller' => 'Economics', 'action' => 'settlementDetail'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/settlements/:settlement_id/pdf', ['controller' => 'Economics', 'action' => 'settlementDownloadPdf'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/settlements/:settlement_id/pdf_view/:filename', ['controller' => 'Economics', 'action' => 'settlementDownloadPdfView'], ['pass' => ['settlement_id', 'filename'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/settlements/:settlement_id/pdf_attachments', ['controller' => 'Economics', 'action' => 'settlementPdfAttachments'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/settlements/:settlement_id/unapprove', ['controller' => 'Economics', 'action' => 'settlementUnapprove'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/settlements/:settlement_id/manual_submit', ['controller' => 'Economics', 'action' => 'settlementManualSubmit'], ['pass' => ['settlement_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/:settlement_id/download_file/:file_id', ['controller' => 'Economics', 'action' => 'settlementDownloadFile'], ['pass' => ['settlement_id', 'file_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/economics/:settlement_id/view_file/:file_id', ['controller' => 'Economics', 'action' => 'settlementViewFile'], ['pass' => ['settlement_id', 'file_id'], 'settlement_id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/admin/reports/', ['controller' => 'ReportsManager', 'action' => 'index'], ['_name' => 'admin_reports']);
    $routes->connect('/admin/reports/:id', ['controller' => 'ReportsManager', 'action' => 'addModify'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/reports/add', ['controller' => 'ReportsManager', 'action' => 'addModify']);
    $routes->connect('/admin/reports/:id/delete', ['controller' => 'ReportsManager', 'action' => 'delete'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/reports/:id/column/:column_id', ['controller' => 'ReportsManager', 'action' => 'addModifyColumn'], ['id' => REGEX_POSITIVE_NUMBER, 'column_id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id', 'column_id']]);
    $routes->connect('/admin/reports/:id/column/:column_id/delete', ['controller' => 'ReportsManager', 'action' => 'deleteColumn'], ['id' => REGEX_POSITIVE_NUMBER, 'column_id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id', 'column_id']]);
    $routes->connect('/admin/reports/:id/column/add', ['controller' => 'ReportsManager', 'action' => 'addModifyColumn'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/reports/:id/render', ['controller' => 'ReportsManager', 'action' => 'renderReport'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/reports/agendio-presets', ['controller' => 'ReportsManager', 'action' => 'agendioPresets']);

    $routes->connect('/admin/opendata/:id/toggle', ['controller' => 'OpenData', 'action' => 'toggle'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/opendata/:id/anonymize', ['controller' => 'OpenData', 'action' => 'openDataAnonymize'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/opendata/:id/anonymize/upload_edits', ['controller' => 'OpenData', 'action' => 'openDataUploadEdits'], ['id' => REGEX_POSITIVE_NUMBER, 'pass' => ['id']]);
    $routes->connect('/admin/opendata/:id/delete_file/:file_id', ['controller' => 'OpenData', 'action' => 'openDataDeleteFile'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/attachment/:file_id', ['controller' => 'OpenData', 'action' => 'openDataAddModifyFile'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/upload_file', ['controller' => 'OpenData', 'action' => 'openDataAddModifyFile'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/download_file/:file_id', ['controller' => 'OpenData', 'action' => 'openDataDownloadFile'], ['pass' => ['id', 'file_id'], 'id' => REGEX_POSITIVE_NUMBER, 'file_id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/save_and_publish', ['controller' => 'OpenData', 'action' => 'openDataSaveAndPublish'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/redirect', ['controller' => 'OpenData', 'action' => 'openDataSaveOnly'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/opendata/:id/discard', ['controller' => 'OpenData', 'action' => 'openDataDiscard'], ['pass' => ['id'], 'id' => REGEX_POSITIVE_NUMBER]);

    $routes->connect('/admin/translations', ['controller' => 'Translations', 'action' => 'index'], ['_name' => 'admin_translations']);
    $routes->connect('/admin/translations/:id/copy', ['controller' => 'Translations', 'action' => 'formCopy'], ['pass' => ['id']]);
    $routes->connect('/admin/translations/:id', ['controller' => 'Translations', 'action' => 'addModify'], ['pass' => ['id']]);

    $routes->connect('/admin/locate', ['controller' => 'UserRequests', 'action' => 'locate'], ['_name' => 'locate_request']);
    $routes->connect('/admin/locate/:id', ['controller' => 'UserRequests', 'action' => 'locateByID'], ['pass' => ['id']]);

    $routes->connect('/admin/forms', ['controller' => 'Forms', 'action' => 'index'], ['_name' => 'admin_forms']);
    $routes->connect('/admin/forms/add', ['controller' => 'Forms', 'action' => 'addModify']);
    $routes->connect('/admin/forms/:id', ['controller' => 'Forms', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/copy', ['controller' => 'Forms', 'action' => 'formCopy'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/preview', ['controller' => 'Forms', 'action' => 'formPreview'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/delete', ['controller' => 'Forms', 'action' => 'formDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/detail', ['controller' => 'Forms', 'action' => 'formDetail'], ['pass' => ['id']]);
    $routes->connect('/admin/forms/:id/fields/:field_id', ['controller' => 'Forms', 'action' => 'fieldAddModify'], ['pass' => ['id', 'field_id'], 'field_id' => '\d+']);
    $routes->connect('/admin/forms/:id/fields/add', ['controller' => 'Forms', 'action' => 'fieldAddModify'], ['pass' => ['id', 0]]);
    $routes->connect('/admin/forms/:id/fields/:field_id/delete', ['controller' => 'Forms', 'action' => 'fieldDelete'], ['pass' => ['id', 'field_id']]);

    $routes->connect('/admin/histories', ['controller' => 'History', 'action' => 'index'], ['_name' => 'admin_history_identities']);
    $routes->connect('/admin/histories/search', ['controller' => 'History', 'action' => 'index'], ['_name' => 'admin_history_search']);
    $routes->connect('/admin/histories/identity/upload', ['controller' => 'History', 'action' => 'uploadIdentities']);
    $routes->connect('/admin/histories/identity/:id/edit', ['controller' => 'History', 'action' => 'addModifyIdentity'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/identity/add', ['controller' => 'History', 'action' => 'addModifyIdentity']);
    $routes->connect('/admin/histories/identity/:id', ['controller' => 'History', 'action' => 'detail'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/:id', ['controller' => 'History', 'action' => 'addModifyHistory'], ['pass' => ['identity_id', 'id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/:id/delete', ['controller' => 'History', 'action' => 'deleteHistory'], ['pass' => ['identity_id', 'id']]);
    $routes->connect('/admin/histories/identity/:identity_id/record/add', ['controller' => 'History', 'action' => 'addModifyHistory'], ['pass' => ['identity_id']]);
    $routes->connect('/admin/histories/identity_detail/:user_id', ['controller' => 'History', 'action' => 'detailFromUserId'], ['_name' => 'detailFromUserId', 'pass' => ['user_id']]);

    $routes->connect('/admin/notifications', ['controller' => 'UserRequests', 'action' => 'notificationsIndex'], ['_name' => 'admin_notifications']);

    // only useful when OldDsw plugin loaded
    $routes->connect('/admin/histories/dsw/hidden', ['controller' => 'History', 'action' => 'dsw', 'hidden' => true]);
    $routes->connect('/admin/histories/dsw', ['controller' => 'History', 'action' => 'dsw'], ['_name' => 'admin_history_dsw']);
    $routes->connect('/admin/histories/dsw/zadosti/:year', ['controller' => 'History', 'action' => 'dswZadosti'], ['year' => REGEX_POSITIVE_NUMBER]);
    $routes->connect('/admin/histories/dsw/zadosti/hidden', ['controller' => 'History', 'action' => 'dswZadosti', 'hidden' => true]);
    $routes->connect('/admin/histories/dsw/zadosti', ['controller' => 'History', 'action' => 'dswZadosti'], ['_name' => 'admin_history_dsw_zadosti']);
    $routes->connect('/admin/histories/dsw/:id', ['controller' => 'History', 'action' => 'dswDetail'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/:id/hide', ['controller' => 'History', 'action' => 'dswHideAccount'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/zadost/:id', ['controller' => 'History', 'action' => 'dswZadost'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/zadost/:id/hide', ['controller' => 'History', 'action' => 'dswHideZadost'], ['pass' => ['id']]);
    $routes->connect('/admin/histories/dsw/download/:id', ['controller' => 'History', 'action' => 'dswDownload'], ['pass' => ['id']]);

    $routes->connect('/admin/wiki', ['controller' => 'WikiManager', 'action' => 'index'], ['_name' => 'admin_wiki']);
    $routes->connect('/admin/wiki/category/:id', ['controller' => 'WikiManager', 'action' => 'categoryAddModify'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/category/:id/delete', ['controller' => 'WikiManager', 'action' => 'categoryDelete'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/category/add', ['controller' => 'WikiManager', 'action' => 'categoryAddModify']);

    $routes->connect('/admin/wiki/:id', ['controller' => 'WikiManager', 'action' => 'addModify'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/:id/delete', ['controller' => 'WikiManager', 'action' => 'delete'], ['pass' => ['id']]);
    $routes->connect('/admin/wiki/add', ['controller' => 'WikiManager', 'action' => 'addModify']);

    $routes->connect('/sitemap_index.xml', ['controller' => 'Public', 'action' => 'sitemap']);
    $routes->connect('/sitemap.xml', ['controller' => 'Public', 'action' => 'sitemap']);
    $routes->redirect('/sitemap.xml.gz', '/sitemap.xml');
    $routes->connect('/atom.xml', ['controller' => 'Public', 'action' => 'rss']);

    $routes->redirect('/prihlaseni', '/login');
    $routes->redirect('/admin', '/login');
    $routes->redirect('/admin/sign/in', '/login');
    $routes->redirect('/login.html', '/login');
    $routes->redirect('/registration', '/register');
    $routes->redirect('/registration/new', '/register');
    $routes->redirect('/registration/obcan', '/register');
    $routes->redirect('/registration/pravnicka-osoba', '/register');
    $routes->redirect('/registration/zapomenute-heslo', '/password_recovery');
    $routes->redirect('/registration/reset-hesla', '/password_recovery');
    $routes->redirect('/www/podpora', '/podpora');
    $routes->redirect('/blog', '/about');
    $routes->redirect('/dotacni-fond/*', '/');
    // common typos
    $routes->redirect('/.', '/');
    $routes->redirect('/,', '/');

    $routes->redirect('/www/ticket', '/rickroll');
    $routes->redirect('/UploadTest.aspx', '/rickroll');
    $routes->redirect('/Telerik{rest}', '/rickroll');
    $routes->redirect('/=/**', '/rickroll');
    $routes->redirect('/s=/**', '/rickroll');
    $routes->redirect('/s=index/**', '/rickroll');
    $routes->redirect('/log', '/rickroll');
    $routes->redirect('/log/*', '/rickroll');
    $routes->redirect('/app/config/key.pem', '/rickroll');
    $routes->redirect('/app/config/key.pem.', '/rickroll');
    $routes->redirect('/solr/*', '/rickroll');
    $routes->redirect('/api/*', '/rickroll');
    $routes->redirect('/HNAP1', '/rickroll');
    $routes->redirect('/console', '/rickroll');
    $routes->redirect('/webfig', '/rickroll');
    $routes->redirect('/axis2/*', '/rickroll');
    $routes->redirect('/wp/*', '/rickroll');
    $routes->redirect('/boaform/**', '/rickroll');
    $routes->redirect('/stalker_portal/**', '/rickroll');
    $routes->redirect('/currentsetting.htm', '/rickroll');
    $routes->redirect('/index.html**', '/rickroll');
    $routes->redirect('/admin/accounts/', '/rickroll');
    $routes->redirect('/wordpress/', '/rickroll');
    $routes->redirect('/_ignition/execute-solution', '/rickroll');
    $routes->redirect('/jenkins/login', '/rickroll');
    $routes->redirect('/nette.micro', '/rickroll');
    $routes->redirect('/client_area/**', '/rickroll');
    $routes->redirect('/rickroll', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ');
});

Router::scope('/api', function (RouteBuilder $routes) {

    // json API
    $routes->connect('/rest/v1/roles', ['controller' => 'Api', 'action' => 'roles']);
    $routes->connect('/rest/v1/teams', ['controller' => 'Api', 'action' => 'teams']);
    $routes->connect('/rest/v1/managers', ['controller' => 'Api', 'action' => 'managers']);
    $routes->connect('/rest/v1/report-columns', ['controller' => 'Api', 'action' => 'reportColumns']);

    // WS / SOAP
    $routes->connect('/ws/v1/public', ['controller' => 'Api', 'action' => 'wsPublic', 'type' => 'ws'])->setPass(['type']);
});
