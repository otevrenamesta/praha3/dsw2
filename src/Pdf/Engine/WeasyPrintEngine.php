<?php

namespace App\Pdf\Engine;

use CakePdf\Pdf\CakePdf;
use Cake\Core\Exception\Exception;
use CakePdf\Pdf\Engine\AbstractPdfEngine;

define('PDF_STYLE', '<style type="text/css">
@media print {
    * {
        page-break-inside: auto;
        page-break-after: auto;
        page-break-before: auto;
    }

    @page {
        margin: 3mm;
    }

    body {
        font-family: Arial, Helvetica, sans-serif;
    }
}

table {
    page-break-inside: auto;
}

table tr {
    page-break-inside: auto;
    page-break-after: auto;
    page-break-before: auto;
}

table th {
    padding: 0.33em;
    vertical-align: top;
    text-align: left;
}

table td {
    padding: 2px;
    border-top: 1px solid #ddd;
    /* Light border */
    vertical-align: top;
    text-align: left;
    page-break-inside: auto;
    break-inside: auto;
    page-break-after: auto;
    page-break-before: auto;
    padding: 0.33em;
}

thead {
    display: table-header-group;
    font-weight: bold;
}

tfoot {
    display: table-footer-group;
}

.card {
    border: 1px solid #ddd;
    /* Light border */
    border-radius: 8px;
    /* Rounded corners */
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    /* Subtle shadow */
    overflow: hidden;
    /* Fix for rounded corners */
    margin-top: 1rem;
    /* Margin top for spacing */
}

.card-header {
    background-color: #f8f9fa;
    /* Light grey background for header */
    border-bottom: 1px solid #ddd;
    /* Border below header */
    padding: 0.75rem 1rem;
    /* Padding for header */
    font-size: 1.25rem;
    /* Font size for header */
    font-weight: 500;
    /* Semi-bold font for header */
}

.card-body {
    padding: 1.25rem;
    /* Padding for body */
    font-size: 1rem;
    /* Font size for body */
    color: #333;
    /* Text color */
}

.mt-2 {
    margin-top: 0.5rem;
    /* Margin top utility class */
}

hr {
    border: 0 none;
    border-top: 1px solid #666;
}

h1,
.h1 {
    font-size: 125%;
}

h2,
.h2 {
    font-size: 120%;
}

h3,
.h3 {
    font-size: 115%;
}

h4,
.h4 {
    font-size: 110%;
}

h5,
.h5 {
    font-size: 105%;
}

h6,
.h6 {
    font-size: 100%;
}

div,
td,
th {
    font-size: 75%;
    font-family: Arial, Helvetica, sans-serif;
}
</style>');

class WeasyPrintEngine extends AbstractPdfEngine
{
    /**
     * Path to the wkhtmltopdf executable binary
     *
     * @var string
     */
    // protected $_binary = 'weasyprint';
    protected $_binary = '/usr/local/bin/weasyprint';

    /**
     * Constructor
     *
     * * @param \CakePdf\Pdf\CakePdf $Pdf CakePdf instance
     */
    public function __construct(CakePdf $Pdf)
    {
        parent::__construct($Pdf);
    }


    /**
     * Generates Pdf from html
     *
     * @throws \Cake\Core\Exception\Exception
     * @return string Raw PDF data
     * @throws \Exception If no output is generated to stdout by wkhtmltopdf.
     */
    public function output()
    {
        $command = $this->_getCommand();
        $content = $this->_exec($command, $this->_Pdf->html());

        if (!empty($content)) {
            return $content;
        }

        throw new Exception("WeasyPrint didn't return any data");
    }

    /**
     * Execute the WeasyPrint commands for rendering pdfs
     *
     * @param string $cmd the command to execute
     * @param string $input Html to pass to wkhtmltopdf
     * @return array the result of running the command to generate the pdf
     */

    protected function _exec($cmd, $input)
    {
        // Temporary file for HTML content
        $htmlFile = tempnam(sys_get_temp_dir(), 'weasyprint') . '.html';

        // Temporary file for PDF output
        $pdfFile = tempnam(sys_get_temp_dir(), 'weasyprint') . '.pdf';

        // Get HTML content from CakePdf instance
        $htmlContent = $this->_Pdf->html();
        $input = $this->removeStylesFromHead($htmlContent);

        file_put_contents($htmlFile, $input);

        // Construct the WeasyPrint command
        $cmd = "weasyprint $htmlFile $pdfFile";

        // Execute the command
        exec($cmd, $output, $returnVar);



        // Handle errors (if any)
        if ($returnVar !== 0) {
            throw new Exception("Error generating PDF: " . implode("\n", $output));
        }

        // Get the generated PDF content
        $pdfContent = file_get_contents($pdfFile);


        // Cleanup: delete temporary files
        unlink($htmlFile);
        unlink($pdfFile);

        return $pdfContent;
    }


    /**
     * Get the command to render a pdf
     *
     * @return string the command for generating the pdf
     * @throws \Cake\Core\Exception\Exception
     */
    protected function _getCommand()
    {
        $binary = $this->getConfig('binary');

        // Test if binary is installed
        $binary = "ghostscript";
        $output = array();
        $return_var = 0;
        exec("which $binary", $output, $return_var);
        $binary = $output[0] ?? false;

        if ($binary) {
            $this->_binary = $binary;
        }
        if (!is_executable($this->_binary)) {
            throw new Exception(sprintf('wkhtmltopdf binary is not found or not executable: %s', $this->_binary));
        }

        $options = [
            'quiet' => true,
            'print-media-type' => true,
            'orientation' => $this->_Pdf->orientation(),
            'page-size' => $this->_Pdf->pageSize(),
            'encoding' => $this->_Pdf->encoding(),
            'title' => $this->_Pdf->title(),
            'javascript-delay' => $this->_Pdf->delay(),
            'window-status' => $this->_Pdf->windowStatus(),
        ];

        $margin = $this->_Pdf->margin();
        foreach ($margin as $key => $value) {
            if ($value !== null) {
                $options['margin-' . $key] = $value . 'mm';
            }
        }
        $options = array_merge($options, (array)$this->getConfig('options'));

        /*if ($this->_windowsEnvironment) {
            $command = '"' . $this->_binary . '"';
        } else {*/
        $command = $this->_binary . ' --media-type print';
        /*}*/

        //TODO: Handle weasyprint options, for now no options
        $options = [];

        foreach ($options as $key => $value) {
            if (empty($value)) {
                continue;
            } elseif (is_array($value)) {
                foreach ($value as $k => $v) {
                    $command .= sprintf(' --%s %s %s', $key, escapeshellarg($k), escapeshellarg($v));
                }
            } elseif ($value === true) {
                $command .= ' --' . $key;
            } else {
                $command .= sprintf(' --%s %s', $key, escapeshellarg($value));
            }
        }
        /* TODO: Headers and footers
        $footer = $this->_Pdf->footer();
        foreach ($footer as $location => $text) {
            if ($text !== null) {
                $command .= " --footer-$location \"" . addslashes($text) . "\"";
            }
        }

        $header = $this->_Pdf->header();
        foreach ($header as $location => $text) {
            if ($text !== null) {
                $command .= " --header-$location \"" . addslashes($text) . "\"";
            }
        }
        $command .= " - -";

        if ($this->_windowsEnvironment) {
            $command = '"' . $command . '"';
        }
        */

        return $command;
    }

    protected function removeStylesFromHead($html)
    {


        // Define the pattern to match <style>...</style> tags inside <head>...</head>
        $pattern = '/<head.*?>.*?<style.*?>.*?<\/style>.*?<\/head>/is';

        // Use preg_replace_callback to handle the replacement within the <head> tag
        $html = preg_replace_callback($pattern, function ($matches) {
            // Remove the <style>...</style> content from the matched <head> section
            return preg_replace('/<style.*?>.*?<\/style>/is', PDF_STYLE, $matches[0]);
        }, $html);

        return $html;
    }
}
