<?php
declare(strict_types=1);

namespace App\Budget;


use App\Controller\UserRequestsController;
use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Table\RequestBudgetsTable;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\View\View;

class NoProjectBudget extends DefaultProjectBudget
{
    use LocatorAwareTrait;

    public function getTemplate(): string
    {
        return 'Budget/no_budget';
    }

    public function renderFullTable(View $appView, Request $request): string
    {
        return $appView->element('Budget/no_project_budget_full_table', compact('request'));
    }

    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget
    {
        return $budget;
    }

    public function canUserEditRequestedAmount(): bool
    {
        return true;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program)
    {
        return RequestBudget::BUDGET_OK;
    }

}