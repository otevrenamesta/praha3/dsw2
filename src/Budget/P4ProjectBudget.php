<?php


namespace App\Budget;


use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use Cake\I18n\Number;
use Cake\Utility\Hash;
use Cake\View\View;

class P4ProjectBudget extends DefaultProjectBudget
{
    public const MAX_ROWS = 50;
    public const COL_TITLE = 0,
        COL_TOTAL_COSTS = 1,
        COL_REQUESTED_AMOUNT = 2;

    private array $_data = [];

    public $budget_id;
    public $design_id;
    public $definitions;

    public function __construct($id)
    {
        $this->budget_id = 'p4';
        $this->design_id = $id;
        $this->definitions = $this->getDefinitions();
    }

    public function getSectionMaxRows(?int $section): int
    {
        switch ($section) {
            default:
                return self::MAX_ROWS;
        }
    }

    public function getSections(): array
    {
        return $this->definitions['SECTIONS'];
    }

    public function getSectionColumns(int $sectionId): array
    {
        return $this->definitions['COLUMNS']['section' . $sectionId] ?? $this->definitions['COLUMNS']['DEFAULT'];
    }

    public function getSectionHeaders(int $sectionId): array
    {
        return $this->definitions['HEADERS']['section' . $sectionId] ?? $this->definitions['HEADERS']['DEFAULT'];
    }

    public function getSectionFooters(int $sectionId): array
    {
        $footers = $this->definitions['FOOTERS']['section' . $sectionId] ?? $this->definitions['FOOTERS']['DEFAULT'];

        foreach ($footers as $key => $footer) {

            if ($footer['text'] == '%sum') {
                $footers[$key]['text'] = str_replace('%sum', $this->getSectionColumnSum($key, $sectionId, true), $footer['text']);
            }
        }

        return $footers;
    }

    public function getSectionColumnSum(int $column, ?int $section = null, bool $asCurrency = false)
    {
        $sum = 0;
        foreach ($this->_data as $sectionId => $sectionRows) {
            if ($section === null || $sectionId === $section) {
                foreach ($sectionRows as $rowId => $rowData) {
                    $sum += intval($rowData[$column] ?? 0);
                }
            }
        }
        return $asCurrency ? Number::currency(round($sum, 2), 'CZK') : $sum;
    }

    public function sumUpByTarget($data, $target)
    {
        $sum = 0;
        foreach ($data as $sectionId => $rows) {
            $sectionColumns = $this->getSectionColumns($sectionId);
            $colIdsToCount = [];
            foreach ($sectionColumns as $col => $settings) {
                if ($target == ($settings['data-sum-to'] ?? '')) {
                    $colIdsToCount[] = $col;
                }
            }

            foreach ($rows as $rowData) {
                foreach ($colIdsToCount as $col) {
                    if (intval($rowData[$col])) {
                        $sum += intval($rowData[$col]);
                    } else {
                        $sum += floatval($rowData[$col]);
                    }
                }
            }
        }
        return $sum;
    }

    public function getTemplate(): string
    {
        return 'Budget/budget_P4';
    }

    public function loadBudget(RequestBudget $budget): self
    {

        if (empty($budget->extra_data)) {
            $extra_data = $this->prefillExtraData();
            $budget->extra_data = serialize($extra_data);
        };

        if (!empty($budget->extra_data)) {
            $data = unserialize($budget->extra_data);
            if (is_array($data)) {
                //TODO: Why would $data[$this->budget_id] even exist ?
                $this->_data = $data[$this->budget_id] ?? $data;
            }
        }
        $budget->set($this->budget_id, $this->_data);
        return $this;
    }

    public function prefillExtraData()
    {
        $extra_data = null;
        $prefills = $this->definitions['PREFILL'] ?? [];

        foreach ($prefills as $section => $prefill) {
            $sid = str_replace('section', '', $section);
            foreach ($prefill as $row => $values)
                $extra_data[$sid][$row] = $values;
        }

        return $extra_data;
    }

    public function disabledInPrefills($sectionId, $row, $column)
    {
        //This is just a default: if prefilled, then disable
        $prefill = $this->definitions['PREFILL'] ?? [];
        return $prefill['section' . $sectionId][$row][$column] ?? false;
    }

    public function getSectionFilledRows(int $sectionId): int
    {
        return empty($this->_data[$sectionId]) ? 0 : max(array_keys($this->_data[$sectionId])) + 1;
    }

    public function getData(string $key)
    {
        return Hash::get($this->_data, $key);
    }

    public function getAllData()
    {
        return $this->_data;
    }

    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget
    {
        if (!is_array($postData)) {
            return $budget;
        }
        $this->_data = $postData[$this->budget_id] ?? $postData;

        foreach ($this->_data as $sectionId => $rows) {
            foreach ($rows as $rowNo => $rowData) {

                $hasTitle = !empty($rowData[self::COL_TITLE] ?? 0);
                $colAmount = intval($rowData[self::COL_REQUESTED_AMOUNT] ?? 0);
                $hasAmount = $colAmount >= 0;
                $totalAmount = intval($rowData[self::COL_TOTAL_COSTS] ?? 0);
                $hasTotalAmount = !empty($totalAmount);

                if (!$hasTitle && !$hasTotalAmount) {
                    unset($this->_data[$sectionId][$rowNo]);
                    continue;
                }

                if (!$hasTitle || !$hasAmount) {
                    $budget->setError(sprintf("%s.%d.%d.%d", $this->budget_id, $sectionId, $rowNo, $hasAmount ? self::COL_TITLE : self::COL_REQUESTED_AMOUNT), __('Tento řádek není vyplněn správně'));
                }
                $this->_data[$sectionId][$rowNo][self::COL_REQUESTED_AMOUNT] = $colAmount;
                $this->_data[$sectionId][$rowNo][self::COL_TOTAL_COSTS] = $totalAmount;
            }
        }
        $budget->extra_data = serialize($this->_data);

        // Sums

        $budget->total_costs = $this->sumUpByTarget($this->_data, 'expenses');

        $budget->requested_amount = $this->sumUpByTarget($this->_data, 'requested');

        //$budget->total_income = $this->sumUpByTarget($this->_data,'income');

        $budget->total_own_sources = $this->sumUpByTarget($this->_data, 'own_sources');

        $budget->total_other_subsidy = $this->sumUpByTarget($this->_data, 'other_subsidy');

        return $budget;
    }

    public function canUserEditRequestedAmount(): bool
    {
        return false;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program)
    {

        if (empty($budget->requested_amount)) {
            return RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT;
        }

        if (!$appeal->checkRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        if (!$appeal->checkMinRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_LOWER_THAN_ALLOWED;
        }

        if (!0 == $budget->total_costs - $budget->total_own_sources - $budget->total_other_subsidy - $budget->requested_amount) {
            return RequestBudget::BUDGET_INCORRECT_BALANCE;
        }

        return RequestBudget::BUDGET_OK;
    }

    public function renderFullTable(View $appView, Request $request): string
    {
        $budget_id = $this->budget_id;
        $design_id = $this->design_id;
        return $appView->element('Budget/P4_project_budget_full_table', compact('request', 'budget_id', 'design_id'));
    }

    public function getDefinitions():array
    {
        $defs = [
            'SECTIONS' => [
                ['title' => __('Položkový rozpočet nákladů')],
                ['title' => false, 'savebutton' => true, 'type' => 'sum'],
            ],
            'COLUMNS' => [
                'DEFAULT' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'own_sources',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'text',
                        'readonly' => true,
                        'data-row-formula' => 'P4custom', // Custom row calculation
                        'data-sum-to' => 'requested',
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'other_subsidy',
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => false,
                        //'placeholder' => __('Zde musíte vyplnit'),
                    ],
                ],
            ],
            'HEADERS' => [
                'DEFAULT' =>
                [
                    __('Položka'),
                    __('Celkem'),
                    __('Částka, kterou pokryjete z vlastních zdrojů'),
                    __('Částka, kterou požadujete z dotace MČ Praha 4'),
                    __('Částka, kterou pokryjete z jiných zdrojů'),
                    __('Popis jiného zdroje využitého pro financování'),
                ],
            ],
            'FOOTERS' => [
                'DEFAULT' => [],
                'section0' => [
                    ['text' => __('Celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'own_sources', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'other_subsidy', 'text' => '%sum'],
                    ['text' => __('&nbsp;')],
                ],
            ],
        ];
        return $defs;
    }
}
