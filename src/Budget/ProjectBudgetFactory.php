<?php
declare(strict_types=1);

namespace App\Budget;


use App\Model\Entity\ProjectBudgetDesign;

class ProjectBudgetFactory
{

    public static function getBudgetHandler(int $budget_design_id): DefaultProjectBudget
    {
        switch ($budget_design_id) {
            default:
            case ProjectBudgetDesign::DESIGN_P3:
                return new DefaultProjectBudget();
            case ProjectBudgetDesign::DESIGN_P14:
                return new P14ProjectBudget();
            case ProjectBudgetDesign::DESIGN_USTI_V1: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V1);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V2: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V2);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V3: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V3);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V4: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V4);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V5: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V5);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V6: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V6);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V7: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V7);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V8: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V8);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V9: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V9);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V10: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V10);
            }
            case ProjectBudgetDesign::DESIGN_P4: {
                return new P4ProjectBudget(ProjectBudgetDesign::DESIGN_P4);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V11: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V11);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V12: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V12);
            }

            // Virtual budget designs - see #238
            case ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE);
            }
            // Issue #307
            case ProjectBudgetDesign::DESIGN_USTI_V13: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V13);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V14: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V14);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V15: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V15);
            }
            // No budget - see issue #380
            case ProjectBudgetDesign::DESIGN_NONE: {
                return new NoProjectBudget();
            }
            case ProjectBudgetDesign::DESIGN_USTI_V16: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V16);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V17: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V17);
            }
            case ProjectBudgetDesign::DESIGN_USTI_V18: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_USTI_V18);
            }
            case ProjectBudgetDesign::DESIGN_TISNOV: {
                return new UstiProjectBudget(ProjectBudgetDesign::DESIGN_TISNOV);
            }
            case ProjectBudgetDesign::DESIGN_P2A: {
                return new P2ProjectBudget(ProjectBudgetDesign::DESIGN_P2A);
            }
            case ProjectBudgetDesign::DESIGN_P2B: {
                return new P2ProjectBudget(ProjectBudgetDesign::DESIGN_P2B);
            }
        }
    }

}