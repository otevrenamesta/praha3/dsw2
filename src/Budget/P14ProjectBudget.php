<?php


namespace App\Budget;


use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use Cake\I18n\Number;
use Cake\Utility\Hash;
use Cake\View\View;

class P14ProjectBudget extends DefaultProjectBudget
{
    public const MAX_ROWS = 20;
    public const SECTION_MATERIAL = 1,
        SECTION_SERVICES = 2,
        SECTION_PERSONAL_COSTS = 3,
        SECTION_TRAVEL_EXPENSES = 4,
        SECTION_OTHERS = 5;
    public const COL_TITLE = 1,
        COL_REQUESTED_AMOUNT = 2,
        COL_OTHER_SOURCES_AMOUNT = 3,
        COL_OTHER_SOURCES = 4,
        COL_ROWSUM = 5;
    public const BUDGET_ID = 'p14';

    private array $_data = [];

    public function getSectionMaxRows(?int $section): int
    {
        switch ($section) {
            default:
                return self::MAX_ROWS;
        }
    }

    public function getSections(): array
    {
        return [
            self::SECTION_MATERIAL => __('Materiálové náklady'),
            self::SECTION_SERVICES => __('Nemateriálové náklady - služby'),
            self::SECTION_PERSONAL_COSTS => __('Personální náklady'),
            self::SECTION_TRAVEL_EXPENSES => __('Cestovné'),
            self::SECTION_OTHERS => __('Jiné náklady (pojistné, správní poplatky, ...)'),
        ];
    }

    public function getSectionColumns(int $sectionId): array
    {
        switch ($sectionId) {
            default:
                return [
                    self::COL_TITLE => [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    self::COL_REQUESTED_AMOUNT => [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'sum-type' => 'requested-amount',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    self::COL_OTHER_SOURCES_AMOUNT => [
                        'type' => 'number',
                        'step' => '1',
                        'sum-type' => 'total-own-sources',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    self::COL_OTHER_SOURCES => [
                        'type' => 'text',
                        'is-required' => false,
                    ],
                    self::COL_ROWSUM => [
                        'disabled' => true,
                        'sum-type' => 'total-costs',
                        'type' => 'text',
                        'class' => 'text-right',
                    ]
                ];
        }
    }

    public function getSectionHeaders(int $sectionId): array
    {
        switch ($sectionId) {
            default:
                return [
                    __('Účel nákladu / výdaje'),
                    __('Částka, kterou požadujete z dotace MČ'),
                    __('Částka, kterou pokryjete z jiných zdrojů'),
                    __('Další zdroj financování'),
                    __('Celkové náklady/výdaje na projekt'),
                ];
        }
    }

    public function getSectionFooters(int $sectionId): array
    {
        switch ($sectionId) {
            default:
                return [
                    self::COL_TITLE => ['text' => __('Celkem')],
                    self::COL_REQUESTED_AMOUNT => ['class' => 'text-right colsum', 'sum-type' => 'requested-amount', 'text' => $this->getSectionColumnSum(self::COL_REQUESTED_AMOUNT, $sectionId, true)],
                    self::COL_OTHER_SOURCES_AMOUNT => ['class' => 'text-right colsum', 'sum-type' => 'total-own-sources', 'text' => $this->getSectionColumnSum(self::COL_OTHER_SOURCES_AMOUNT, $sectionId, true)],
                    self::COL_OTHER_SOURCES => [],
                    self::COL_ROWSUM => ['class' => 'text-right colsum', 'sum-type' => 'total-costs', 'text' => $this->getSectionColumnSum(self::COL_ROWSUM, $sectionId, true)],
                ];
        }
    }

    public function getSectionColumnSum(int $column, ?int $section = null, bool $asCurrency = false)
    {
        $sum = 0;
        foreach ($this->_data as $sectionId => $sectionRows) {
            if ($section === null || $sectionId === $section) {
                foreach ($sectionRows as $rowId => $rowData) {
                    $sum += $column !== self::COL_ROWSUM ?
                        intval($rowData[$column] ?? 0)
                        : intval($rowData[self::COL_REQUESTED_AMOUNT] ?? 0) + intval($rowData[self::COL_OTHER_SOURCES_AMOUNT] ?? 0);
                }
            }
        }
        return $asCurrency ? Number::currency(round($sum, 2), 'CZK') : $sum;
    }

    public function getTemplate(): string
    {
        return 'Budget/budget_p14';
    }

    public function loadBudget(RequestBudget $budget): self
    {
        if (!empty($budget->extra_data)) {
            $data = unserialize($budget->extra_data);
            if (is_array($data)) {
                $this->_data = $data[self::BUDGET_ID] ?? $data;
            }
        }
        $budget->set(self::BUDGET_ID, $this->_data);

        return $this;
    }

    public function getSectionFilledRows(int $sectionId): int
    {
        return empty($this->_data[$sectionId]) ? 0 : max(array_keys($this->_data[$sectionId])) + 1;
    }

    public function getData(string $key)
    {
        return Hash::get($this->_data, $key);
    }

    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget
    {
        if (!is_array($postData)) {
            return $budget;
        }
        $this->_data = $postData[self::BUDGET_ID] ?? $postData;

        foreach ($this->_data as $sectionId => $rows) {
            foreach ($rows as $rowNo => $rowData) {

                $hasTitle = !empty($rowData[self::COL_TITLE] ?? 0);
                $colAmount = intval($rowData[self::COL_REQUESTED_AMOUNT] ?? 0);
                $hasAmount = $colAmount >= 0;

                $hasOtherSource = !empty($rowData[self::COL_OTHER_SOURCES] ?? 0);
                $otherSourceAmount = intval($rowData[self::COL_OTHER_SOURCES_AMOUNT] ?? 0);
                $hasOtherSourceAmount = !empty($otherSourceAmount);

                if (!$hasTitle && !$hasOtherSource && !$hasOtherSourceAmount) {
                    unset($this->_data[$sectionId][$rowNo]);
                    continue;
                }

                if (!$hasTitle || !$hasAmount) {
                    $budget->setError(sprintf("%s.%d.%d.%d", self::BUDGET_ID, $sectionId, $rowNo, $hasAmount ? self::COL_TITLE : self::COL_REQUESTED_AMOUNT), __('Tento řádek není vyplněn správně'));
                }

                if (($hasOtherSource || $hasOtherSourceAmount) && (!$hasOtherSource || !$hasOtherSourceAmount || $otherSourceAmount < 1)) {
                    $budget->setError(sprintf("%s.%d.%d.%d", self::BUDGET_ID, $sectionId, $rowNo, $hasOtherSource ? self::COL_OTHER_SOURCES_AMOUNT : self::COL_OTHER_SOURCES), __('Musí být vyplněna informace o zdroji financí'));
                }

                if ($colAmount < 1 && $hasOtherSourceAmount < 1) {
                    $budget->setError(sprintf("%s.%d.%d.%d", self::BUDGET_ID, $sectionId, $rowNo, self::COL_TITLE), __('Musíte vyplnit alespoň jednu z částek v tomto řádku'));
                }

                $this->_data[$sectionId][$rowNo][self::COL_REQUESTED_AMOUNT] = $colAmount;
                $this->_data[$sectionId][$rowNo][self::COL_OTHER_SOURCES_AMOUNT] = $otherSourceAmount;
                $this->_data[$sectionId][$rowNo][self::COL_ROWSUM] = Number::currency(intval($rowData[self::COL_REQUESTED_AMOUNT]) + intval($rowData[self::COL_OTHER_SOURCES_AMOUNT]), 'CZK');
            }

            sort($this->_data[$sectionId]);
        }
        $budget->extra_data = serialize($this->_data);

        $budget->total_own_sources = $this->getSectionColumnSum(self::COL_OTHER_SOURCES_AMOUNT);
        $budget->total_costs = $this->getSectionColumnSum(self::COL_ROWSUM);
        $budget->requested_amount = $this->getSectionColumnSum(self::COL_REQUESTED_AMOUNT);

        return $budget;
    }

    public function canUserEditRequestedAmount(): bool
    {
        return false;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program)
    {
        if (empty($budget->requested_amount)) {
            return RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT;
        }

        if (!$appeal->checkRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        return RequestBudget::BUDGET_OK;
    }

    public function renderFullTable(View $appView, Request $request): string
    {
        return $appView->element('Budget/p14_project_budget_full_table', compact('request'));
    }
}
