<?php


namespace App\Budget;


use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use Cake\I18n\Number;
use Cake\Utility\Hash;
use Cake\View\View;

class UstiProjectBudget extends DefaultProjectBudget
{
    public const MAX_ROWS = 50;
    public const COL_TITLE = 0,
        COL_TOTAL_COSTS = 1,
        COL_REQUESTED_AMOUNT = 2;

    public const MINISTRIES = [
        0 => 'Vyberte ze seznamu',
        306 => 'Ministerstvo zahraničních věcí',
        307 => 'Ministerstvo obrany',
        312 => 'Ministerstvo financí',
        313 => 'Ministerstvo práce a sociálních věcí',
        314 => 'Ministerstvo vnitra',
        315 => 'Ministerstvo životního prostředí',
        317 => 'Ministerstvo pro místní rozvoj',
        322 => 'Ministerstvo průmyslu a obchodu',
        327 => 'Ministerstvo dopravy',
        329 => 'Ministerstvo zemědělství',
        333 => 'Ministerstvo školství, mládeže a tělovýchovy',
        334 => 'Ministerstvo kultury',
        335 => 'Ministerstvo zdravotnictví',
        336 => 'Ministerstvo spravedlnosti'
    ];

    public array $_data = [];

    public $budget_id;
    public $design_id;
    public $definitions;
    public $org_name = "Ústí nad Labem";

    public function __construct($id)
    {
        $this->budget_id = 'usti' . $id;
        $this->design_id = $id;
        $this->definitions = $this->getDefinitions();
    }

    public function getSectionMaxRows(?int $section): int
    {
        switch ($section) {
            default:
                return self::MAX_ROWS;
        }
    }

    public function getSections(): array
    {
        return $this->definitions['SECTIONS'];
    }

    public function getSectionColumns(int $sectionId): array
    {
        return $this->definitions['COLUMNS']['section' . $sectionId] ?? $this->definitions['COLUMNS']['DEFAULT'];
    }

    public function getSectionHeaders(int $sectionId): array
    {
        return $this->definitions['HEADERS']['section' . $sectionId] ?? $this->definitions['HEADERS']['DEFAULT'];
    }

    public function getSectionFooters(int $sectionId): array
    {
        $footers = $this->definitions['FOOTERS']['section' . $sectionId] ?? $this->definitions['FOOTERS']['DEFAULT'];

        foreach ($footers as $key => $footer) {

            if ($footer['text'] == '%sum') {
                $footers[$key]['text'] = str_replace('%sum', $this->getSectionColumnSum($key, $sectionId, true), $footer['text']);
            }
        }

        return $footers;
    }

    public function getSectionColumnSum(int $column, ?int $section = null, bool $asCurrency = false)
    {
        $sum = 0;
        foreach ($this->_data as $sectionId => $sectionRows) {
            if ($section === null || $sectionId === $section) {
                foreach ($sectionRows as $rowId => $rowData) {
                    $sum += intval($rowData[$column] ?? 0);
                }
            }
        }
        return $asCurrency ? Number::currency(round($sum, 2), 'CZK') : $sum;
    }

    public function sumUpByTarget($data, $target)
    {
        $sum = 0;
        foreach ($data as $sectionId => $rows) {
            $sectionColumns = $this->getSectionColumns($sectionId);
            $colIdsToCount = [];
            foreach ($sectionColumns as $col => $settings) {
                if ($target == ($settings['data-sum-to'] ?? '')) {
                    $colIdsToCount[] = $col;
                }
            }

            foreach ($rows as $rowData) {
                foreach ($colIdsToCount as $col) {
                    $sum += $rowData[$col];
                }
            }
        }
        return $sum;
    }

    public function getTemplate(): string
    {
        return 'Budget/budget_usti';
    }

    // Handle this odd budget change on the fly
    private function budgetSpecialMangle_V11(&$budget)
    {
        if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE || $this->design_id == ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE) {
            $prefills = $this->prefillExtraData();
            $extra_data = unserialize($budget->extra_data);
            $extra_data[8] = $extra_data[4];
            $extra_data[5] = [];
            $extra_data[6] = $extra_data[2];
            $extra_data[2] =  array_values($prefills[2]);
            $extra_data[3] = array_values($prefills[3]);
            $extra_data[4] = array_values($prefills[4]);
            ksort($extra_data);
            $budget->extra_data = serialize($extra_data);
        }
    }

    private function budgetSpecialMangle_V12(&$budget)
    {
        if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE || $this->design_id == ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE) {
            $prefills = $this->prefillExtraData();
            $extra_data = unserialize($budget->extra_data);
            $extra_data[7] = $extra_data[3];
            $extra_data[4] = [];
            $extra_data[5] = $extra_data[2];
            $extra_data[1] =  array_values($prefills[1]);
            $extra_data[2] = array_values($prefills[2]);
            $extra_data[3] = array_values($prefills[3]);
            ksort($extra_data);
            $budget->extra_data = serialize($extra_data);
        }
    }




    public function loadBudget($budget, $nomangle = false): self
    {
        if (!$nomangle) {
            if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE) {
                $this->budgetSpecialMangle_V11($budget);
            }
            if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE) {
                $this->budgetSpecialMangle_V12($budget);
            }
        }

        if (empty($budget->extra_data)) {
            $extra_data = $this->prefillExtraData();
            $budget->extra_data = serialize($extra_data);
        };

        if (!empty($budget->extra_data)) {
            $data = unserialize($budget->extra_data);
            if (is_array($data)) {
                ksort($data);
                //TODO: Why would $data[$this->budget_id] even exist ?
                $this->_data = $data[$this->budget_id] ?? $data;
            }
        }
        $budget->set($this->budget_id, $this->_data);

        return $this;
    }

    public function prefillExtraData()
    {
        $extra_data = null;
        $prefills = $this->definitions['PREFILL'] ?? [];

        foreach ($prefills as $section => $prefill) {
            $sid = str_replace('section', '', $section);
            foreach ($prefill as $row => $values)
                $extra_data[$sid][$row] = $values;
        }

        return $extra_data;
    }

    public function disabledInPrefills($sectionId, $row, $column)
    {
        //This is just a default: if prefilled, then disable
        $prefill = $this->definitions['PREFILL'] ?? [];
        $out = $prefill['section' . $sectionId][$row][$column] ?? false;
        if (($out !== false) && ($out !== 0)) {
            return $out;
        } else {
            return false;
        }
    }

    public function getSectionFilledRows(int $sectionId): int
    {
        return empty($this->_data[$sectionId]) ? 0 : max(array_keys($this->_data[$sectionId])) + 1;
    }

    public function getData(string $key)
    {
        return Hash::get($this->_data, $key);
    }

    public function getAllData()
    {
        return $this->_data;
    }

    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget
    {
        if (!is_array($postData)) {
            return $budget;
        }
        $this->_data = $postData[$this->budget_id] ?? $postData;

        foreach ($this->_data as $sectionId => $rows) {
            foreach ($rows as $rowNo => $rowData) {

                $hasTitle = !empty($rowData[self::COL_TITLE] ?? 0);
                $colAmount = intval($rowData[self::COL_REQUESTED_AMOUNT] ?? 0);
                $hasAmount = $colAmount >= 0;
                $totalAmount = intval($rowData[self::COL_TOTAL_COSTS] ?? 0);
                $hasTotalAmount = !empty($totalAmount);

                if (!$hasTitle && !$hasTotalAmount) {
                    unset($this->_data[$sectionId][$rowNo]);
                    continue;
                }

                if (!$hasTitle && $hasAmount === false) {
                    $budget->setError(sprintf("%s.%d.%d.%d", $this->budget_id, $sectionId, $rowNo, $hasAmount ? self::COL_TITLE : self::COL_REQUESTED_AMOUNT), __('Tento řádek není vyplněn správně'));
                }
                $this->_data[$sectionId][$rowNo][self::COL_REQUESTED_AMOUNT] = $colAmount;
                $this->_data[$sectionId][$rowNo][self::COL_TOTAL_COSTS] = $totalAmount;
            }
        }
        $budget->extra_data = serialize($this->_data);

        // Sums
        $this->sumUpByTarget($this->_data, 'expenses');

        $budget->total_costs = $this->sumUpByTarget($this->_data, 'expenses');

        $budget->requested_amount = $this->sumUpByTarget($this->_data, 'requested');

        $budget->total_income = $this->sumUpByTarget($this->_data, 'income');

        return $budget;
    }

    public function canUserEditRequestedAmount(): bool
    {
        return false;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program)
    {

        if (empty($budget->requested_amount)) {
            return RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT;
        }

        if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V11 || $this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
            return RequestBudget::BUDGET_OK;
        }

        if (!$appeal->checkRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        if (!$appeal->checkMinRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_LOWER_THAN_ALLOWED;
        }

        if (array_key_exists('VALIDATION',$this->definitions)) {
            if (!$this->runCustomValidations($budget)) {
                return RequestBudget::BUDGET_CUSTOM_VALIDATION_FAILED;
            }
        }

        if (!0 == $budget->total_income + $budget->requested_amount - $budget->total_costs) {
            return RequestBudget::BUDGET_INCORRECT_BALANCE;
        }

        $total = $budget->total_costs ?? false;
        $percentage = 0;
        if ($total) {
            $percentage = ceil($budget->requested_amount / $total * 100);
        }
        // https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/363

        if (!$appeal->checkMaxSupport($percentage, $program->id)) {
            return RequestBudget::BUDGET_MAX_SUPPORT_EXCEEDED;
        }

        return RequestBudget::BUDGET_OK;
    }

    /**
     * Perform custom budget validation as defined in definitions['VALIDATION']
     * NOTE: Only partially implemented 
     * 
     * @param RequestBudget $budget
     * 
     * @return bool
     */

    public function runCustomValidations(RequestBudget $budget):bool {
        //TODO: Should be more generic, the code below is only a quick fix for #429, only sum is implemented 
        $extra_data = unserialize($budget->extra_data);
        $validation = $this->definitions['VALIDATION'];
        
        // Sums cells and compare them with the target
        foreach ($validation['sum'] as $target => $cells) {
            $sum = 0;
            foreach ($cells as $cell) {
                list($i1,$i2,$i3) = explode(',',$cell);
                $sum += $extra_data[$i1][$i2][$i3];
            }
            if (intval($budget->{$target}) == intval($sum)) return true;
        }
        return false;
    }

    public function renderFullTable(View $appView, Request $request): string
    {
        $budget_id = $this->budget_id;
        $design_id = $this->design_id;

        return $appView->element('Budget/usti_project_budget_full_table', compact('request', 'budget_id', 'design_id'));
    }

    public function getDefinitions():array
    {
        $culture = [
            'SECTIONS' => [
                ['title' => __('Materiálové náklady'), 'cardopen' => __('Náklady')],
                ['title' => __('Nemateriálové náklady - služby')],
                ['title' => __('Majetek')],
                ['title' => __('Personální náklady')],
                ['title' => false, 'savebutton' => true, 'cardclose' => true, 'type' => 'sum']
            ],
            'COLUMNS' => [
                'DEFAULT' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
            ],
            'HEADERS' => [
                'DEFAULT' =>
                [
                    __('Druh nákladu - jednotlivé položky rozpočtu konkrétně specifikujte:'),
                    __('Celkové předpokládané náklady činnosti'),
                    __('Z toho náklady hrazené z požadované dotace'),
                ],
            ],
            'FOOTERS' => [
                'DEFAULT' => [],
                'section4' => [
                    ['text' => __('Celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                ],
            ],
            'PREFILL' => [
                // Section 0
                'section0' => [
                    [ // Row 0
                        // Column 0
                        0 => 'Kancelářské potřeby'
                    ]
                ],
                'section1' => [
                    [0 => 'Nájemné vč. služeb a energií'],
                    [0 => 'Pronájem techniky a zařízení'],
                    [0 => 'Ozvučení'],
                    [0 => 'Osvětlení'],
                    [0 => 'Cestovné, přeprava'],
                    [0 => 'Ubytování'],
                    [0 => 'Honoráře'],
                    [0 => 'Propagace'],
                    [0 => 'Autorské poplatky'],
                    [0 => 'Fotodokumentace'],
                    [0 => 'Spoje (poštovné, telefonní služby, internet)'],
                ],
                'section2' => [
                    [0 => 'Dlouhodobý nehmotný majetek'],
                    [0 => 'Dlouhodobý hmotný majetek']
                ],
                'section3' => [
                    [0 => 'Mzdy'],
                    [0 => 'Dohody PČ a PP'],
                    [0 => 'Zákonné odvody']
                ]
            ]
        ];

        $sport = [
            'SECTIONS' => [
                ['title' => __('Materiálové náklady'), 'cardopen' => __('Náklady')],
                ['title' => __('Nemateriálové náklady - služby')],
                ['title' => __('Majetek')],
                ['title' => __('Personální náklady')],
                ['title' => false, 'savebutton' => true, 'cardclose' => true, 'type' => 'sum']
            ],
            'COLUMNS' => [
                'DEFAULT' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
            ],
            'HEADERS' => [
                'DEFAULT' =>
                [
                    __('Druh nákladu - jednotlivé položky rozpočtu konkrétně specifikujte:'),
                    __('Celkové předpokládané náklady činnosti'),
                    __('Z toho náklady hrazené z požadované dotace'),
                ],
            ],
            'FOOTERS' => [
                'DEFAULT' => [],
                'section4' => [
                    ['text' => __('Celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                ],
            ],
            'PREFILL' => [
                // Section 0
                'section0' => [
                    [ // Row 0
                        // Column 0
                        0 => 'Kancelářské potřeby'
                    ],
                    [
                        0 => 'Věcné ceny'
                    ]

                ],
                'section1' => [
                    [0 => 'Nájemné vč. služeb a energií'],
                    [0 => 'Spotřeba energií'],
                    [0 => 'Startovné, poplatky federacím'],
                    [0 => 'Pronájem techniky a zařízení'],
                    [0 => 'Ozvučení, osvětlení'],
                    [0 => 'Časoměřič, časomíra'],
                    [0 => 'Rozhodčí, porotce, komisař'],
                    [0 => 'Pořadatelská, zdravotní služba, hasiči apod.'],
                    [0 => 'Propagace'],
                    [0 => 'Cestovné, přeprava'],
                    [0 => 'Ubytování'],
                    [0 => 'Stravování'],
                    [0 => 'Honoráře'],
                    [0 => 'Autorské poplatky'],
                    [0 => 'Fotodokumentace'],
                    [0 => 'Spoje (poštovné, telefonní služby, internet)'],
                ],
                'section2' => [
                    [0 => 'Dlouhodobý nehmotný majetek'],
                    [0 => 'Dlouhodobý hmotný majetek']
                ],
                'section3' => [
                    [0 => 'Mzdy'],
                    [0 => 'Dohody PČ a PP'],
                    [0 => 'Zákonné odvody']
                ]
            ]
        ];

        $social = [
            'SECTIONS' => [
                ['title' => false, 'cardopen' => __('Náklady'), 'no_body' => true, 'type' => 'section_heading'],
                ['title' => __('Materiálové:'), 'noadd' => true, 'cardopen' => __('Provozní náklady'), 'runsum' => 'true'],
                ['title' => __('Energie:'), 'noadd' => true],
                ['title' => __('Služby:'), 'noadd' => true],
                ['title' => __('Jiné náklady:'), 'cardclose' => true],
                ['title' => __('Mzdové náklady:'), 'noadd' => true, 'cardclose' => false],
                ['title' => false, 'noadd' => true, 'savebutton' => true, 'cardclose' => true],
            ],
            'COLUMNS' => [
                'DEFAULT' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section1' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'data-run-sum-to' => 'runsum1_expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run-sum-to' => 'runsum1_requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section2' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'data-run-sum-to' => 'runsum2_expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run-sum-to' => 'runsum2_requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section3' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'data-run-sum-to' => 'runsum3_expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run-sum-to' => 'runsum3_requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section4' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'data-run-sum-to' => 'runsum4_expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run-sum-to' => 'runsum4_requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section5' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'data-run-sum-to' => 'runsum5_expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'data-run-sum-to' => 'runsum5_requested',
                        'data-run2-sum-to' => 'ulgrant', 'data-run3-sum-to' => 'income',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ]

            ],
            'HEADERS' => [
                'DEFAULT' =>
                [
                    __('Druh nákladu - jednotlivé položky rozpočtu konkrétně specifikujte:'),
                    __('Celkové předpokládané náklady činnosti'),
                    __('Z toho náklady hrazené z požadované dotace'),
                ],
                'section6' =>
                [
                    '',
                    __('Celkové náklady činnosti'),
                    __('Celkem požadovaná dotace'),
                ],


            ],
            'FOOTERS' => [
                'DEFAULT' => [],
                'section1' => [
                    ['text' => __('Celkem materiálové náklady:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum1_expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum1_requested', 'text' => '%sum'],
                ],
                'section2' => [
                    ['text' => __('Celkem energie:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum2_expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum2_requested', 'text' => '%sum'],
                ],
                'section3' => [
                    ['text' => __('Celkem služby:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum3_expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum3_requested', 'text' => '%sum'],
                ],
                'section4' => [
                    ['text' => __('Celkem jiné náklady:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum4_expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum4_requested', 'text' => '%sum'],
                ],
                'section5' => [
                    ['text' => __('Celkem mzdové náklady:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum5_expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'runsum5_requested', 'text' => '%sum'],
                ],
                'section6' => [
                    ['text' => __('Plánované náklady celkem:')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                ],

            ],
            'PREFILL' => [
                'section1' => [
                    [0 => 'Ochranné pomůcky'],
                    [0 => 'Hygienické a dezinfekční prostředky'],
                    [0 => 'Léky a zdravotnický materiál'],
                    [0 => 'Potraviny'],
                    [0 => 'Tiskoviny a odborná literatura'],
                    [0 => 'Kancelářské potřeby'],
                    [0 => 'Vybavení DHIM'],
                    [0 => 'Ostatní'],
                ],
                'section2' => [
                    [0 => 'Elektřina, plyn, teplo'],
                ],
                'section3' => [
                    [0 => 'Nájemné'],
                    [0 => 'Vodné, stočné'],
                    [0 => 'Konzultační, poradenské a právní služby'],
                    [0 => 'Školení, vzdělávání, supervize'],
                    [0 => 'Nákup softwaru'],
                    [0 => 'Bezpečnostní, požární služby, revize'],
                    [0 => 'Oprava, údržba'],
                    [0 => 'Likvidace odpadu'],
                    [0 => 'Telefon, internet, poštovné'],
                    [0 => 'Cestovní náklady (doprava, jízdné klientů, cestovní příkazy)'],
                    [0 => 'Ostatní služby '],
                ],
                'section4' => [
                    [0 => 'Dlouhodobý hmotný majetek', 2 => '--not-applicable'],
                    [0 => 'Dlouhodobý nehmotný majetek', 2 => '--not-applicable'],
                    [0 => 'Odpisy', 2 => '--not-applicable']
                ],
                'section5' => [
                    [0 => 'Hrubé mzdy'],
                    [0 => 'DPP/DPČ'],
                    [0 => 'Odvody soc. a zdrav. pojištění (hrazeného zaměstnavatelem)']
                ]

            ]

        ];
        $out = $culture; // Default V1

        switch ($this->design_id) {

            case ProjectBudgetDesign::DESIGN_USTI_V1:
                // Nothing to do
                $out = $culture;
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V2:
                $out = $culture;
                unset($out['PREFILL']['section1'][9]); // Fotodokumentace
                unset($out['PREFILL']['section1'][10]); // Spoje
                $out['PREFILL']['section1'] = array_values($out['PREFILL']['section1']); // See #421 - unsets started causing empty lines - not necessary here though as it's the tail of the array 
                // Remove Section Majetek
                $out['SECTIONS'][2] = $out['SECTIONS'][3];
                $out['SECTIONS'][3] = $out['SECTIONS'][4];
                $out['FOOTERS']['section3'] = $out['FOOTERS']['section4'];
                unset($out['SECTIONS'][4]);
                unset($out['FOOTERS']['section4']);
                $out['PREFILL']['section2'] = $out['PREFILL']['section3']; // Shift prefills
                unset($out['PREFILL']['section3']);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V3:
                $out = $culture;
                // Remove Section Majetek
                $out['SECTIONS'][2] = $out['SECTIONS'][3];
                $out['SECTIONS'][3] = $out['SECTIONS'][4];
                $out['FOOTERS']['section3'] = $out['FOOTERS']['section4'];
                unset($out['SECTIONS'][4]);
                unset($out['FOOTERS']['section4']);
                $out['PREFILL']['section2'] = $out['PREFILL']['section3']; // Shift prefills
                unset($out['PREFILL']['section3']);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V4:
                $out = $sport;
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V5:
                $out = $sport;
                unset($out['PREFILL']['section3'][0]); // Mzdy
                $out['PREFILL']['section3'] = array_values($out['PREFILL']['section3']); // See #421 - unsets started causing empty lines 
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V6:
                $out = $sport;
                unset($out['PREFILL']['section3'][0]); // Mzdy
                unset($out['PREFILL']['section1'][10]);  // Ubytování
                unset($out['PREFILL']['section1'][11]);  // Stravování
                $out['PREFILL']['section1'] = array_values($out['PREFILL']['section1']); // See #421 - unsets started causing empty lines 
                $out['PREFILL']['section3'] = array_values($out['PREFILL']['section3']); // See #421 - unsets started causing empty lines 
                // Remove Section Majetek
                $out['SECTIONS'][2] = $out['SECTIONS'][3];
                $out['SECTIONS'][3] = $out['SECTIONS'][4];
                $out['FOOTERS']['section3'] = $out['FOOTERS']['section4'];
                unset($out['SECTIONS'][4]);
                unset($out['FOOTERS']['section4']);
                $out['PREFILL']['section2'] = $out['PREFILL']['section3']; // Shift prefills
                unset($out['PREFILL']['section3']);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V7:
                $out = $sport;
                unset($out['PREFILL']['section3'][0]); // Mzdy
                $out['PREFILL']['section3'] = array_values($out['PREFILL']['section3']); // See #421 - unsets started causing empty lines 
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V8:
                $out = $sport;
                $out['SECTIONS'][0] = $out['SECTIONS'][1];
                $out['SECTIONS'][1] = $out['SECTIONS'][4];
                unset($out['SECTIONS'][2]);
                unset($out['SECTIONS'][3]);
                unset($out['SECTIONS'][4]);
                $out['FOOTERS']['section1'] = $out['FOOTERS']['section4'];
                unset($out['FOOTERS']['section4']);
                $out['PREFILL']['section0'] = [
                    [0 => 'Nájemné vč. služeb a energií']
                ];
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V9:
                $out = $sport;
                unset($out['PREFILL']['section3'][0]); // Mzdy
                unset($out['PREFILL']['section1'][10]);  // Ubytování
                unset($out['PREFILL']['section1'][11]);  // Stravování
                $out['PREFILL']['section3'] = array_values($out['PREFILL']['section3']); // See #421 - unsets started causing empty lines 
                $out['PREFILL']['section1'] = array_values($out['PREFILL']['section1']); // See #421 - unsets started causing empty lines 
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V10:
                $out = $sport;
                unset($out['PREFILL']['section3'][0]); // Mzdy
                // Neuznatelné náklady - disabled by prefill
                $out['PREFILL']['section1'][14][0] .= __(" - neuznatelný náklad");
                $out['PREFILL']['section1'][15][0] .= __(" - neuznatelný náklad");
                $out['PREFILL']['section1'][14][2] = '0'; // Fotodokumentace
                $out['PREFILL']['section1'][15][2] = '0'; // Spoje

                break;
            case ProjectBudgetDesign::DESIGN_USTI_V11:
                $out = $sport;
                unset($out['PREFILL']);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V12:
                $out = $sport;
                unset($out['PREFILL']);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V11;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V6;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE;
                return $this->combine_V11($a, $b);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V12;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V6;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE;
                return $this->combine_V12($a, $b);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V13:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V11;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V4;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V13;
                return $this->combine_V6V11($a, $b);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V14:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V12;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V4;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V14;
                return $this->combine_V6V12($a, $b);
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V15:
                // Nothing to do
                $out = $social;
                break;
            case ProjectBudgetDesign::DESIGN_USTI_V16:
                // Like V14 but with some changes 
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V12;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V4;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V16;
                $out = $this->combine_V6V12($a, $b);
                // See #416
                $out['SECTIONS'][0]['title'] = "Celoroční volnočasová činnost od 5 do 20 let";
                $out['PREFILL']['section2'][14][2] = '--not-applicable'; // Fotodokumentace
                $out['PREFILL']['section2'][15][2] = '--not-applicable'; // Spoje
                // See #429
                $out['VALIDATION'] = [
                    // Součet hodnot odevzdaných šeků musí odpovídat požadované částce
                    'sum'=> ['requested_amount'=>['0,0,2']],
                    '_message' => __('Požadovaná částka musí odpovídat součtu hodnot odevzdaných šeků')
                ];
                return $out;
            case ProjectBudgetDesign::DESIGN_USTI_V17:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V11;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V4;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V17;
                $out = $this->combine_V6V11($a, $b);
                // See #421
                //$out['SECTIONS'][0]['title'] = "Celoroční sportování dětí a mládeže do 20 let";
                $out['PREFILL']['section0'][0][1] = 5000; // New cheque nominal value 
                $out['PREFILL']['section1'][0][1] = 3000; // New cheque nominal value 
                //$out['PREFILL']['section0'][0][1] = 2500; // New cheque nominal value 
                $out['PREFILL']['section3'][14][2] = '--not-applicable'; // Fotodokumentace
                $out['PREFILL']['section3'][15][2] = '--not-applicable'; // Spoje
                // See #429
                $out['VALIDATION'] = [
                    // Součet hodnot odevzdaných šeků musí odpovídat požadované částce
                    'sum'=> ['requested_amount'=>['0,0,2', '1,0,2']],
                    '_message' => __('Požadovaná částka musí odpovídat součtu hodnot odevzdaných šeků')
                ];
                return $out;
            case ProjectBudgetDesign::DESIGN_USTI_V18:
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V12;
                $a = $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V4;
                $b =  $this->getDefinitions();
                $this->design_id = ProjectBudgetDesign::DESIGN_USTI_V18;
                $out = $this->combine_V6V12($a, $b);
                // See #421
                //$out['SECTIONS'][0]['title'] = "Celoroční podpora pohybových aktivit občanů města nad 21 let";
                $out['PREFILL']['section0'][0][1] = 3500; // New cheque nominal value 
                $out['PREFILL']['section2'][14][2] = '--not-applicable'; // Fotodokumentace
                $out['PREFILL']['section2'][15][2] = '--not-applicable'; // Spoje
                // See #429
                $out['VALIDATION'] = [
                    // Součet hodnot odevzdaných šeků musí odpovídat požadované částce
                    'sum'=> ['requested_amount'=>['0,0,2']],
                    '_message' => __('Požadovaná částka musí odpovídat součtu hodnot odevzdaných šeků')
                ];
                return $out;

            // Tisnov wants a simillar project budget design so we accomodate it here for brevity - see #437
            case ProjectBudgetDesign::DESIGN_TISNOV:
                $this->org_name = "Tišnov";
                $out = $culture;
                $out['PREFILL']['section0'][] = ['Pohonné hmoty'];
                $out['PREFILL']['section0'][] = ['Sportovní vybavení'];
                $out['PREFILL']['section0'][] = ['Ostatní vybavení'];
                $out['PREFILL']['section1'][0] = ['Nájemné'];
                $this->array_insert($out['PREFILL']['section1'],1,['Energie']);
                $out['PREFILL']['section1'][] = ['Tisk a kopírování'];
                $out['PREFILL']['section1'][] = ['Sportovní prohlídky'];
                $out['PREFILL']['section2'] = [];
                $out['SECTIONS'][2]['title'] = "Dlouhodobý nehmotný/hmotný majetek";
                //$out['PREFILL']['section3'][2] = ['Jiné náklady'];
                unset($out['PREFILL']['section3'][2]);
                $out['SECTIONS'][5] = $out['SECTIONS'][4];
                $out['SECTIONS'][4] = ['title' => 'Jiné náklady'];

                $out['COLUMNS']['DEFAULT'][2]['data-run2-sum-to'] = 'ulgrant';
                $out['COLUMNS']['DEFAULT'][2]['data-run3-sum-to'] = 'income';
                break;
        }

        // VZ11 and VZ12 are rather different, see #161

        if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V11 || $this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
            // $attach_common = []; // No income section - UPDATE: Now they want it back
            // VZ11 first, VZ12 is similar
            $out = [
                'SECTIONS' => [
                    ['title' => __('Výkonnostní sport'), 'cardopen' => __('Náklady'), 'noadd' => true],
                    ['title' => __('Zájmová činnost'), 'noadd' => true, 'savebutton' => true, 'cardclose' => true],
                ],
                'HEADERS' => [
                    'DEFAULT' =>
                    [
                        __('Počet odevzdaných šeků'),
                        __('Hodnota šeku'),
                        __('Požadovaná výše dotace'),
                    ],
                ],

                // Hidden footers as sinks for sums calculation
                'FOOTERS' => [
                    'section0' => [
                        ['text' => __('Celkem'), 'class' => 'd-none'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'text' => '%sum'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                    ],
                    'section1' => [
                        ['text' => __('Celkem'), 'class' => 'd-none'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'text' => '%sum'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                    ]
                ],
                'PREFILL' => [
                    'section0' => [
                        [
                            1 => 4000,
                            2 => 0
                        ],
                    ],
                    'section1' => [
                        [
                            1 => 2500,
                            2 => 0
                        ]
                    ]
                ],
                'COLUMNS' => [
                    'DEFAULT' => [
                        [
                            'type' => 'number',
                            'data-noquilljs' => 'data-noquilljs',
                            'rows' => 1,
                            'is-required' => false,
                            'default' => 0,
                            'placeholder' => __('Zde musíte vyplnit'),
                        ],
                        [
                            'type' => 'number',
                            'is-required' => false,
                            'class' => 'text-right',
                            'default' => 0,
                        ],
                        [
                            'type' => 'number',
                            'step' => '1',
                            'is-required' => false,
                            'data-sum-to' => 'requested',
                            'data-multiply' => 'true',
                            'class' => 'text-right',
                            'default' => 0,
                        ]
                    ],
                ]

            ];
            if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
                $out['SECTIONS'] = [
                    ['title' => __('Celoroční činnost od 21 let'), 'cardopen' => __('Náklady'), 'noadd' => true, 'cardclose' => true],
                ];
                $out['HEADERS'] = [
                    'DEFAULT' =>
                    [
                        __('Počet odevzdaných šeků'),
                        __('Hodnota šeku'),
                        __('Požadovaná výše dotace'),
                    ],
                ];

                // Hidden footers as sinks for sums calculation
                $out['FOOTERS'] =  [
                    'section0' => [
                        ['text' => __('Celkem'), 'class' => 'd-none'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'text' => '%sum'],
                        ['class' => 'text-right colsum d-none', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                    ],
                ];
                $out['PREFILL'] =  [
                    'section0' => [
                        [
                            1 => 2000,
                            2 => 0
                        ],
                    ],
                ];
            }
        }

        // Add sections common to all
        $offset = count($out['SECTIONS']);
        $section1 = 'section' . strval($offset);
        $section2 = 'section' . ($offset + 1);
        $section3 = 'section' . ($offset + 2);
        $section4 = 'section' . ($offset + 3);
        $attach_common_default_columns = [
            [
                'type' => 'textarea',
                'data-noquilljs' => 'data-noquilljs',
                'rows' => 1,
                'is-required' => true,
                'placeholder' => __('Zde musíte vyplnit'),
            ],
            [
                'type' => 'number',
                'step' => '1',
                'data-sum-to' => 'income',
                'is-required' => false,
                'class' => 'text-right',
                'default' => 0,
            ]
        ];

        $attach_common = [
            'SECTIONS' => [
                ['title' => __('Dotace od ministerstev'), 'cardopen' => __('Příjmy')],
                ['title' => __('Dotace od jiných resortů státní správy/samosprávy'),],
                ['title' => __('Další příjmy')],
                ['title' => false, 'savebutton' => true, 'cardclose' => true, 'type' => 'sum'],

            ],
            'COLUMNS' => [
                $section1 => [
                    [
                        'type' => 'select',
                        'data-noquilljs' => 'data-noquilljs',
                        'options' => UstiProjectBudget::MINISTRIES,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'income',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                $section2 => $attach_common_default_columns,
                $section3 => $attach_common_default_columns
            ],
            'HEADERS' => [
                $section1 => [
                    __('Ministerstva'),
                    __('Částka')
                ],
                $section2 => [
                    __('Resorty'),
                    __('Částka')
                ],
                $section3 => [
                    __('Zdroj'),
                    __('Částka')
                ],
                $section4 => [],
            ],
            'FOOTERS' => [
                $section1 => [],
                $section2 => [],
                $section3 => [],
                $section4 => [
                    ['text' => __('Celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'income', 'text' => '%sum'],
                ],

            ],
            'PREFILL' => [
                $section3 => [
                    [0 => 'Dotace z Ústeckého kraje'],
                    [0 => 'Dotace z fondů EU'],
                    [0 => 'Sponzorské dary'],
                    [0 => 'Vlastní podíl žadatele'],
                    [0 => 'Příjmy/výnosy  (vstupenky, prodej)'],
                    [0 => 'Příjmy/výnosy z reklamy']
                ]
            ]
        ];

        if ($this->design_id == ProjectBudgetDesign::DESIGN_USTI_V15) {
            // Adjust the common parts for VZ15 (social)
            $i = $offset + 1;
            $j = $offset + 3;
            $this->shift_after($attach_common, 1, $i);
            // Add that one extra section 
            $attach_common['SECTIONS'][1] =  ['title' => __('Dotace od města Ústí nad Labem'), 'noadd' => true];
            $attach_common['HEADERS']['section' . $i] =  ['', 'Částka'];
            $attach_common['COLUMNS']['section' . $i] = $attach_common_default_columns;
            $attach_common['PREFILL']['section' . $i] = [['Město Ústí nad Labem', '--data-target-id:ulgrant']];
            $attach_common['FOOTERS']['section' . $i] = [];
            $attach_common['PREFILL']['section' . $j] = [
                [0 => 'Dotace z Ústeckého kraje'],
                [0 => 'Dotace z fondů EU'],
                [0 => 'Sponzorské dary'],
                [0 => 'Příjmy od klientů'],
                [0 => 'Vlastní činnost'],
                [0 => 'Sbírky']
            ];
            // Some custom modifications
            $attach_common['SECTIONS'][0]['cardopen'] = __('Finanční zdroje');
            $attach_common['SECTIONS'][3]['title'] = __('Další zdroje');
            $attach_common['FOOTERS']['section' . ($j + 1)][0]['text'] = __('Zdroje celkem');
        }

        if ($this->design_id == ProjectBudgetDesign::DESIGN_TISNOV) {
            $i = $offset + 1;
            $j = $offset + 3;
            $this->shift_after($attach_common, 1, $i);
            // Add that one extra section 
            $attach_common['SECTIONS'][1] =  ['title' => __('Dotace od města Tišnov'), 'noadd' => true];
            $attach_common['HEADERS']['section' . $i] =  ['', 'Částka'];
            $attach_common['COLUMNS']['section' . $i] = $attach_common_default_columns;
            $attach_common['PREFILL']['section' . $i] = [['Město Tišnov', '--data-target-id:ulgrant']];
            $attach_common['FOOTERS']['section' . $i] = [];
            $attach_common['PREFILL']['section' . $j] = [
                [0 => 'Členské příspěvky'],
                [0 => 'Dotace z fondů EU'],
                [0 => 'Sponzorské dary'],
                [0 => 'Příjmy od klientů'],
                [0 => 'Vlastní činnost'],
                [0 => 'Sbírky']
            ];
            // Some custom modifications
            $attach_common['SECTIONS'][0]['cardopen'] = __('Finanční zdroje');
            $attach_common['SECTIONS'][3]['title'] = __('Další zdroje');
            $attach_common['FOOTERS']['section' . ($j + 1)][0]['text'] = __('Zdroje celkem');
            $attach_common['SECTIONS'][2]['title'] .=  ' (obec, město, kraj)';
        }

        $merged = array_merge_recursive($out, $attach_common);

        // Fix the ['SECTIONS'] numeric keys after array_merge_recursive messes them up
        $new_sections_order =  [];
        foreach ($merged['SECTIONS'] as $section) {
            $new_sections_order[] = $section;
        }
        $merged['SECTIONS'] = $new_sections_order;

        return $merged;
    }


    private function shift_after(&$in, $section_index, $section_number)
    {

        $main_keys = ['SECTIONS', 'COLUMNS', 'HEADERS', 'FOOTERS', 'PREFILL'];
        foreach ($main_keys as $main_key) {
            if (array_key_exists($main_key, $in)) {
                if ($main_key == 'SECTIONS') { // Use nummeric indexes and $section_index
                    $i = count($in['SECTIONS']);
                    while ($i >= $section_index) {
                        $in['SECTIONS'][$i] = $in['SECTIONS'][$i - 1];
                        $i--;
                    }
                    $in['SECTIONS'][$section_index] = [];
                } else { // use textual indexes and $section_number
                    $new = [];
                    foreach ($in[$main_key] as $section_X => $value) {
                        $x = intval(str_replace('section', '', $section_X));
                        if ($x >= $section_number) {
                            $new['section' . ($x + 1)] = $value;
                        } else {
                            $new['section' . $x] = $value;
                        }
                    }
                    $in[$main_key] = $new;
                    $in[$main_key]['section' . $section_number] = [];
                }
            }
        }
    }

    // This function combines V11/V12 with V6, the result is V11_CHANGE/V12_CHANGE. See issue #238
    private function combine_V11($a, $b)
    {
        $out['SECTIONS'][0] = $a['SECTIONS'][0];
        $out['SECTIONS'][1] = $a['SECTIONS'][1];
        unset($out['SECTIONS'][1]['cardclose']);
        $out['SECTIONS'][2] = $b['SECTIONS'][0];
        unset($out['SECTIONS'][2]['cardopen']);
        $out['SECTIONS'][3] = $b['SECTIONS'][1];
        $out['SECTIONS'][4] = $b['SECTIONS'][2];
        $out['SECTIONS'][5] = $b['SECTIONS'][3];
        $out['SECTIONS'][6] = $b['SECTIONS'][4];
        $out['SECTIONS'][7] = $b['SECTIONS'][5];
        $out['SECTIONS'][8] = $b['SECTIONS'][6];
        $out['SECTIONS'][9] = $b['SECTIONS'][7];
        $out['PREFILL']['section0'] = [0 => ['n/a', 'n/a', 'n/a']];
        $out['PREFILL']['section1'] = [0 => ['n/a', 'n/a', 'n/a']];
        $out['PREFILL']['section2'] = array_values($b['PREFILL']['section0']);
        $out['PREFILL']['section3'] = array_values($b['PREFILL']['section1']);
        $out['PREFILL']['section4'] = array_values($b['PREFILL']['section2']);
        $out['PREFILL']['section8'] = array_values($b['PREFILL']['section6']);
        $out['HEADERS']['section0'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['section1'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['DEFAULT'] = $b['HEADERS']['DEFAULT'];
        $out['HEADERS']['section6'] = $b['HEADERS']['section4'];
        $out['HEADERS']['section7'] = $b['HEADERS']['section5'];
        $out['HEADERS']['section8'] = $b['HEADERS']['section6'];
        $out['HEADERS']['section9'] = $b['HEADERS']['section7'];
        $out['COLUMNS']['section0'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section1'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['DEFAULT'] = $b['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section6'] = $b['COLUMNS']['section4'];
        $out['COLUMNS']['section7'] = $b['COLUMNS']['section5'];
        $out['COLUMNS']['section8'] = $b['COLUMNS']['section6'];
        $out['FOOTERS']['DEFAULT'] = [];
        $out['FOOTERS']['section9'] = $b['FOOTERS']['section7'];;
        $out['FOOTERS']['section5'] = $b['FOOTERS']['section3'];;
        $out['SECTIONS'][0]['cardopen'] = __('Náklady - původní (šeky)');
        $out['SECTIONS'][1]['savebutton'] = false;
        $out['SECTIONS'][1]['cardclose'] = true;
        $out['SECTIONS'][2]['cardopen'] = __('Náklady - položkové');
        $out['SECTIONS'][5]['savebutton'] = false;
        unset($out['COLUMNS']['section0'][2]['data-sum-to']);
        unset($out['COLUMNS']['section1'][2]['data-sum-to']);
        return $out;
    }

    private function combine_V12($a, $b)
    {
        $out['SECTIONS'][0] = $a['SECTIONS'][0];
        //$out['SECTIONS'][1] = $a['SECTIONS'][1];
        unset($out['SECTIONS'][1]['cardclose']);
        $out['SECTIONS'][1] = $b['SECTIONS'][0];
        unset($out['SECTIONS'][2]['cardopen']);
        $out['SECTIONS'][2] = $b['SECTIONS'][1];
        $out['SECTIONS'][3] = $b['SECTIONS'][2];
        $out['SECTIONS'][4] = $b['SECTIONS'][3];
        $out['SECTIONS'][5] = $b['SECTIONS'][4];
        $out['SECTIONS'][6] = $b['SECTIONS'][5];
        $out['SECTIONS'][7] = $b['SECTIONS'][6];
        $out['SECTIONS'][8] = $b['SECTIONS'][7];
        $out['PREFILL']['section0'] = [0 => ['n/a', 'n/a', 'n/a']];
        //$out['PREFILL']['section1'] = [0=>['n/a','n/a','n/a']];
        $out['PREFILL']['section1'] = array_values($b['PREFILL']['section0']);
        $out['PREFILL']['section2'] = array_values($b['PREFILL']['section1']);
        $out['PREFILL']['section3'] = array_values($b['PREFILL']['section2']);
        $out['PREFILL']['section7'] = array_values($b['PREFILL']['section6']);
        $out['HEADERS']['section0'] = $a['HEADERS']['DEFAULT'];
        //$out['HEADERS']['section1'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['DEFAULT'] = $b['HEADERS']['DEFAULT'];
        $out['HEADERS']['section5'] = $b['HEADERS']['section4'];
        $out['HEADERS']['section6'] = $b['HEADERS']['section5'];
        $out['HEADERS']['section7'] = $b['HEADERS']['section6'];
        $out['HEADERS']['section8'] = $b['HEADERS']['section7'];
        $out['COLUMNS']['section0'] = $a['COLUMNS']['DEFAULT'];
        //$out['COLUMNS']['section1'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['DEFAULT'] = $b['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section5'] = $b['COLUMNS']['section4'];
        $out['COLUMNS']['section6'] = $b['COLUMNS']['section5'];
        $out['COLUMNS']['section7'] = $b['COLUMNS']['section6'];
        $out['FOOTERS']['DEFAULT'] = [];
        $out['FOOTERS']['section8'] = $b['FOOTERS']['section7'];;
        $out['FOOTERS']['section4'] = $b['FOOTERS']['section3'];;
        $out['SECTIONS'][0]['cardopen'] = __('Náklady - původní (šeky)');
        $out['SECTIONS'][0]['savebutton'] = false;
        $out['SECTIONS'][0]['cardclose'] = true;
        $out['SECTIONS'][1]['cardopen'] = __('Náklady - položkové');
        $out['SECTIONS'][4]['savebutton'] = false;
        unset($out['COLUMNS']['section0'][2]['data-sum-to']);
        //unset($out['COLUMNS']['section1'][2]['data-sum-to']);
        return $out;
    }

    private function combine_V6V11($a, $b)
    {
        $out['SECTIONS'][0] = $a['SECTIONS'][0];
        $out['SECTIONS'][1] = $a['SECTIONS'][1];
        unset($out['SECTIONS'][1]['cardclose']);
        $out['SECTIONS'][2] = $b['SECTIONS'][0];
        unset($out['SECTIONS'][2]['cardopen']);
        $out['SECTIONS'][3] = $b['SECTIONS'][1];
        $out['SECTIONS'][4] = $b['SECTIONS'][2];
        $out['SECTIONS'][5] = $b['SECTIONS'][3];
        $out['SECTIONS'][6] = $b['SECTIONS'][4];
        $out['SECTIONS'][7] = $b['SECTIONS'][5];
        $out['SECTIONS'][8] = $b['SECTIONS'][6];
        $out['SECTIONS'][9] = $b['SECTIONS'][7];
        $out['SECTIONS'][10] = $b['SECTIONS'][8];
        $out['PREFILL']['section0'] = $a['PREFILL']['section0'];
        $out['PREFILL']['section1'] = $a['PREFILL']['section1'];
        $out['PREFILL']['section2'] = array_values($b['PREFILL']['section0']);
        $out['PREFILL']['section3'] = array_values($b['PREFILL']['section1']);
        $out['PREFILL']['section4'] = array_values($b['PREFILL']['section2']);
        $out['PREFILL']['section5'] = array_values($b['PREFILL']['section3']);
        $out['PREFILL']['section9'] = array_values($b['PREFILL']['section7']);
        $out['HEADERS']['section0'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['section0'][2] = __("Náklady celkem");
        $out['HEADERS']['section1'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['section1'][2] = __("Náklady celkem");
        $out['HEADERS']['DEFAULT'] = $b['HEADERS']['DEFAULT'];
        $out['HEADERS']['section7'] = $b['HEADERS']['section5'];
        $out['HEADERS']['section8'] = $b['HEADERS']['section6'];
        $out['HEADERS']['section9'] = $b['HEADERS']['section7'];
        $out['HEADERS']['section10'] = $b['HEADERS']['section8'];
        $out['COLUMNS']['section0'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section1'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['DEFAULT'] = $b['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section7'] = $b['COLUMNS']['section5'];
        $out['COLUMNS']['section8'] = $b['COLUMNS']['section6'];
        $out['COLUMNS']['section9'] = $b['COLUMNS']['section7'];
        $out['FOOTERS']['DEFAULT'] = [];
        $out['FOOTERS']['section6'] = $b['FOOTERS']['section4'];;
        $out['FOOTERS']['section10'] = $b['FOOTERS']['section8'];;
        $out['SECTIONS'][0]['cardopen'] = __('Náklady - šeky');
        $out['SECTIONS'][1]['savebutton'] = false;
        $out['SECTIONS'][1]['cardclose'] = true;
        $out['SECTIONS'][2]['cardopen'] = __('Náklady - položkové');
        $out['SECTIONS'][5]['savebutton'] = false;
        unset($out['COLUMNS']['section0'][2]['data-sum-to']);
        unset($out['COLUMNS']['section1'][2]['data-sum-to']);
        return $out;
    }

    private function combine_V6V12($a, $b)
    {
        $out['SECTIONS'][0] = $a['SECTIONS'][0];
        //$out['SECTIONS'][1] = $a['SECTIONS'][1];
        //unset($out['SECTIONS'][1]['cardclose']);
        $out['SECTIONS'][1] = $b['SECTIONS'][0];
        unset($out['SECTIONS'][2]['cardopen']);
        $out['SECTIONS'][2] = $b['SECTIONS'][1];
        $out['SECTIONS'][3] = $b['SECTIONS'][2];
        $out['SECTIONS'][4] = $b['SECTIONS'][3];
        $out['SECTIONS'][5] = $b['SECTIONS'][4];
        $out['SECTIONS'][6] = $b['SECTIONS'][5];
        $out['SECTIONS'][7] = $b['SECTIONS'][6];
        $out['SECTIONS'][8] = $b['SECTIONS'][7];
        $out['SECTIONS'][9] = $b['SECTIONS'][8];
        $out['PREFILL']['section0'] = $a['PREFILL']['section0'];
        //$out['PREFILL']['section1'] = $a['PREFILL']['section1'];
        $out['PREFILL']['section1'] = array_values($b['PREFILL']['section0']);
        $out['PREFILL']['section2'] = array_values($b['PREFILL']['section1']);
        $out['PREFILL']['section3'] = array_values($b['PREFILL']['section2']);
        $out['PREFILL']['section4'] = array_values($b['PREFILL']['section3']);
        $out['PREFILL']['section8'] = array_values($b['PREFILL']['section7']);
        $out['HEADERS']['section0'] = $a['HEADERS']['DEFAULT'];
        $out['HEADERS']['section0'][2] = __("Náklady celkem");
        //$out['HEADERS']['section1'] = $a['HEADERS']['DEFAULT'];
        //$out['HEADERS']['section1'][2] = __("Náklady celkem");
        $out['HEADERS']['DEFAULT'] = $b['HEADERS']['DEFAULT'];
        $out['HEADERS']['section6'] = $b['HEADERS']['section5'];
        $out['HEADERS']['section7'] = $b['HEADERS']['section6'];
        $out['HEADERS']['section8'] = $b['HEADERS']['section7'];
        $out['HEADERS']['section9'] = $b['HEADERS']['section8'];
        $out['COLUMNS']['section0'] = $a['COLUMNS']['DEFAULT'];
        //$out['COLUMNS']['section1'] = $a['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['DEFAULT'] = $b['COLUMNS']['DEFAULT'];
        $out['COLUMNS']['section6'] = $b['COLUMNS']['section5'];
        $out['COLUMNS']['section7'] = $b['COLUMNS']['section6'];
        $out['COLUMNS']['section8'] = $b['COLUMNS']['section7'];
        $out['FOOTERS']['DEFAULT'] = [];
        $out['FOOTERS']['section5'] = $b['FOOTERS']['section4'];;
        $out['FOOTERS']['section9'] = $b['FOOTERS']['section8'];;
        $out['SECTIONS'][0]['cardopen'] = __('Náklady - šeky');
        $out['SECTIONS'][1]['savebutton'] = false;
        $out['SECTIONS'][1]['cardclose'] = true;
        $out['SECTIONS'][1]['cardopen'] = __('Náklady - položkové');
        $out['SECTIONS'][4]['savebutton'] = false;
        unset($out['COLUMNS']['section0'][2]['data-sum-to']);
        //unset($out['COLUMNS']['section1'][2]['data-sum-to']);
        return $out;
    }
    private function array_insert(&$array, $position, $element) {
        // Check if position is valid
        if ($position < 0 || $position > count($array)) {
            return false; // you could throw an exception or handle this situation differently
        }
    
        // Slice the array and insert the element
        $firstHalf = array_slice($array, 0, $position);
        $secondHalf = array_slice($array, $position);
    
        // Merge and reindex
        $array = array_merge($firstHalf, [$element], $secondHalf);
    
        return true; // If you want to indicate the insertion was successful
    }
}
