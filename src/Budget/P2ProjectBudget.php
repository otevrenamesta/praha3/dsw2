<?php


namespace App\Budget;


use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use Cake\I18n\Number;
use Cake\Utility\Hash;
use Cake\View\View;

class P2ProjectBudget extends DefaultProjectBudget {
    public const MAX_ROWS = 50;
    public const COL_TITLE = 0,
        COL_TOTAL_COSTS = 1,
        COL_REQUESTED_AMOUNT = 2;

    public array $_data = [];

    public $budget_id;
    public $design_id;
    public $definitions;
    public $org_name = "MČ Praha 2";

    public function __construct($id) {
        $this->budget_id = 'p2_' . $id;
        $this->design_id = $id;
        $this->definitions = $this->getDefinitions();
    }

    public function getSections(): array {
        return $this->definitions['SECTIONS'];
    }

    public function getSectionColumns(int $sectionId): array {
        return $this->definitions['COLUMNS']['section' . $sectionId] ?? $this->definitions['COLUMNS']['DEFAULT'];
    }

    public function getSectionHeaders(int $sectionId): array {
        return $this->definitions['HEADERS']['section' . $sectionId] ?? $this->definitions['HEADERS']['DEFAULT'];
    }

    public function getSectionFooters(int $sectionId): array {
        $footers = $this->definitions['FOOTERS']['section' . $sectionId] ?? $this->definitions['FOOTERS']['DEFAULT'];

        foreach ($footers as $key => $footer) {

            if ($footer['text'] == '%sum') {
                $footers[$key]['text'] = str_replace('%sum', $this->getSectionColumnSum($key, $sectionId, true), $footer['text']);
            }
        }

        return $footers;
    }

    public function getSectionMaxRows(?int $section): int {
        $defs = $this->getDefinitions();
        if (is_array($defs['PREFILL']['section' . $section])) {
            return count($defs['PREFILL']['section' . $section]);
        } else {
            return P2ProjectBudget::MAX_ROWS;
        }
    }

    public function getSectionColumnSum(int $column, ?int $section = null, bool $asCurrency = false) {
        $sum = 0;
        foreach ($this->_data as $sectionId => $sectionRows) {
            if ($section === null || $sectionId === $section) {
                foreach ($sectionRows as $rowId => $rowData) {
                    $sum += intval($rowData[$column] ?? 0);
                }
            }
        }
        return $asCurrency ? Number::currency(round($sum, 2), 'CZK') : $sum;
    }

    public function sumUpByTarget($data, $target) {
        $sum = 0;
        foreach ($data as $sectionId => $rows) {
            $sectionColumns = $this->getSectionColumns($sectionId);
            $colIdsToCount = [];
            foreach ($sectionColumns as $col => $settings) {
                if ($target == ($settings['data-sum-to'] ?? '')) {
                    $colIdsToCount[] = $col;
                }
            }

            foreach ($rows as $rowData) {
                foreach ($colIdsToCount as $col) {
                    $sum += $rowData[$col];
                }
            }
        }
        return $sum;
    }

    public function getTemplate(): string {
        return 'Budget/budget_p2';
    }


    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget {
        if (!is_array($postData)) {
            return $budget;
        }
        $this->_data = $postData[$this->budget_id] ?? $postData;

        foreach ($this->_data as $sectionId => $rows) {
            foreach ($rows as $rowNo => $rowData) {

                $hasTitle = !empty($rowData[self::COL_TITLE] ?? 0);
                $colAmount = intval($rowData[self::COL_REQUESTED_AMOUNT] ?? 0);
                $hasAmount = $colAmount >= 0;
                $totalAmount = intval($rowData[self::COL_TOTAL_COSTS] ?? 0);
                $hasTotalAmount = !empty($totalAmount);

                if (!$hasTitle && !$hasTotalAmount) {
                    unset($this->_data[$sectionId][$rowNo]);
                    continue;
                }

                if (!$hasTitle && $hasAmount === false) {
                    $budget->setError(sprintf("%s.%d.%d.%d", $this->budget_id, $sectionId, $rowNo, $hasAmount ? self::COL_TITLE : self::COL_REQUESTED_AMOUNT), __('Tento řádek není vyplněn správně'));
                }
                $this->_data[$sectionId][$rowNo][self::COL_REQUESTED_AMOUNT] = $colAmount;
                $this->_data[$sectionId][$rowNo][self::COL_TOTAL_COSTS] = $totalAmount;
            }
        }
        $budget->extra_data = serialize($this->_data);

        // Sums
        $this->sumUpByTarget($this->_data, 'expenses');

        $budget->total_costs = $this->sumUpByTarget($this->_data, 'expenses');

        $budget->requested_amount = $this->sumUpByTarget($this->_data, 'requested');

        $budget->total_income = $this->sumUpByTarget($this->_data, 'income');

        return $budget;
    }



    public function loadBudget($budget): self {
        if (empty($budget->extra_data)) {
            $extra_data = $this->prefillExtraData();
            $budget->extra_data = serialize($extra_data);
        };

        if (!empty($budget->extra_data)) {
            $data = unserialize($budget->extra_data);
            if (is_array($data)) {
                ksort($data);
                //TODO: Why would $data[$this->budget_id] even exist ?
                $this->_data = $data[$this->budget_id] ?? $data;
            }
        }
        $budget->set($this->budget_id, $this->_data);

        return $this;
    }

    public function prefillExtraData() {
        $extra_data = null;
        $prefills = $this->definitions['PREFILL'] ?? [];

        foreach ($prefills as $section => $prefill) {
            $sid = str_replace('section', '', $section);
            foreach ($prefill as $row => $values)
                $extra_data[$sid][$row] = $values;
        }

        return $extra_data;
    }

    public function disabledInPrefills($sectionId, $row, $column) {
        //This is just a default: if prefilled, then disable
        $prefill = $this->definitions['PREFILL'] ?? [];
        $out = $prefill['section' . $sectionId][$row][$column] ?? false;
        if (($out !== false) && ($out !== 0)) {
            return $out;
        } else {
            return false;
        }
    }

    public function getSectionFilledRows(int $sectionId): int {
        return empty($this->_data[$sectionId]) ? 0 : max(array_keys($this->_data[$sectionId])) + 1;
    }

    public function getData(string $key) {
        return Hash::get($this->_data, $key);
    }

    public function getAllData() {
        return $this->_data;
    }

    public function canUserEditRequestedAmount(): bool {
        return false;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program) {

        if (empty($budget->requested_amount)) {
            return RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT;
        }

        if (!$appeal->checkRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        if (!$appeal->checkMinRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_LOWER_THAN_ALLOWED;
        }

        if (array_key_exists('VALIDATION', $this->definitions)) {
            if (!$this->runCustomValidations($budget)) {
                return RequestBudget::BUDGET_CUSTOM_VALIDATION_FAILED;
            }
        }

        if (!0 == $budget->total_income + $budget->requested_amount - $budget->total_costs) {
            return RequestBudget::BUDGET_INCORRECT_BALANCE;
        }

        $total = $budget->total_costs ?? false;
        $percentage = 0;
        if ($total) {
            $percentage = ceil($budget->requested_amount / $total * 100);
        }
        // https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/363

        if (!$appeal->checkMaxSupport($percentage, $program->id)) {
            return RequestBudget::BUDGET_MAX_SUPPORT_EXCEEDED;
        }

        return RequestBudget::BUDGET_OK;
    }

    /**
     * Perform custom budget validation as defined in definitions['VALIDATION']
     * NOTE: Only partially implemented 
     * 
     * @param RequestBudget $budget
     * 
     * @return bool
     */

    public function runCustomValidations(RequestBudget $budget): bool {
        die('koko');
        //TODO: Should be more generic, the code below is only a quick fix for #429, only sum is implemented 
        $extra_data = unserialize($budget->extra_data);
        $validation = $this->definitions['VALIDATION'];

        // Sums cells and compare them with the target
        foreach ($validation['sum'] as $target => $cells) {
            $sum = 0;
            foreach ($cells as $cell) {
                list($i1, $i2, $i3) = explode(',', $cell);
                $sum += $extra_data[$i1][$i2][$i3];
            }
            if (intval($budget->{$target}) == intval($sum)) return true;
        }
        return false;
    }

    public function renderFullTable(View $appView, Request $request): string {
        $budget_id = $this->budget_id;
        $design_id = $this->design_id;

        return $appView->element('Budget/p2_project_budget_full_table', compact('request', 'budget_id', 'design_id'));
    }

    public function getDefinitions(): array {
        $base = [
            'SECTIONS' => [
                ['title' => __('Neinvestiční výdaje'), 'cardopen' => "Výdaje projektu a způsob použití dotace", 'noadd' => true],
                ['title' => __('Investiční výdaje'), 'cardclose' => true, 'noadd' => true],
                ['title' => false, 'noadd' => true],
                ['title' => __('Předpokládané další zdroje financování projektu (bez požadované dotace)'), 'cardopen' => "Příjmy", 'noadd' => true,'savebutton' => true, 'cardclose' => true],
            ],
            'COLUMNS' => [
                'DEFAULT' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'expenses',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'is-required' => true,
                        'data-sum-to' => 'requested',
                        'placeholder' => __('Zde musíte vyplnit'),
                        'class' => 'text-right',
                        'default' => 0,
                    ]
                ],
                'section3' => [
                    [
                        'type' => 'textarea',
                        'data-noquilljs' => 'data-noquilljs',
                        'rows' => 1,
                        'is-required' => true,
                        'placeholder' => __('Zde musíte vyplnit'),
                    ],
                    [
                        'type' => 'number',
                        'step' => '1',
                        'data-sum-to' => 'income',
                        'is-required' => false,
                        'class' => 'text-right',
                        'default' => 0,
                    ],
                ],
            ],
            'HEADERS' => [
                'DEFAULT' =>
                [
                    __('Výdajové položky'),
                    __('Rozpočet v Kč'),
                    __('Požadovaný příspěvek v Kč'),
                ],
                'section1' =>
                [
                    __(''),
                    __('Částka v Kč'),
                    __('Požadovaný příspěvek v Kč'),
                ],
                'section2' =>
                [  __(''),
                __('Celkové výdaje'),
                __('Výše požadované dotace'),],
                'section3' =>
                [ 
                __('Příjmové položky'),
                __('Předpokládaná částka'),],
            ],
            'FOOTERS' => [
                'DEFAULT' => [],
                'section2' => [
                    ['text' => __('Výdaje celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'expenses', 'text' => '%sum'],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'requested', 'text' => '%sum'],
                ],
                'section3' => [
                    ['text' => __('Příjmy celkem')],
                    ['class' => 'text-right colsum', 'data-fill' => 'sum', 'data-target-id' => 'income', 'text' => '%sum'],
                ],
            ],
            'PREFILL' => [
                // Section 0
                'section0' => [
                    [0 => 'Mzdy'],
                    [0 => 'Služby včetně energií'],
                    [0 => 'Materiál'],
                ],
                'section1' => [
                    [0 => 'Investice celkem'],
                ],
                'section2' => [
                ]   ,
                'section3' => [
                    [0 => 'Příjmy od klientů'],
                    [0 => 'Ostatní vlastní zdroje'],
                    [0 => 'Zdroje od státu'],
                    [0 => 'Zdroje od kraje'],
                    [0 => 'Zdroje z jiné MČ'],
                    [0 => 'Jiné veřejné zdroje'],
                    [0 => 'Nadace'],
                    [0 => 'Sponzorské dary'],
                    [0 => 'Sbírky'],
                    [0 => 'Ostatní zdroje financování'],
                ],
            ]
        ];

        switch ($this->design_id) {
            case ProjectBudgetDesign::DESIGN_P2A:
                // Nothing to do
                $out = $base;
                break;
            case ProjectBudgetDesign::DESIGN_P2B:
                $out = $base;
                $out['PREFILL']['section0'] = [
                    [0 => 'Služby včetně energií'],
                    [0 => 'Materiál'],
                ];

                break;
        }

        return $out;
    }
}
