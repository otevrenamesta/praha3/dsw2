<?php
declare(strict_types=1);

namespace App\Budget;


use App\Controller\UserRequestsController;
use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Table\RequestBudgetsTable;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\View\View;

class DefaultProjectBudget
{
    use LocatorAwareTrait;

    public function getTemplate(): string
    {
        return 'Budget/default_budget';
    }

    public function renderFullTable(View $appView, Request $request): string
    {
        return $appView->element('Budget/default_project_budget_full_table', compact('request'));
    }

    public function modifyBudget(Request $request, RequestBudget $budget, ?array $postData = []): RequestBudget
    {
        $data = $budget->cleanPostData($postData);
        /** @var RequestBudgetsTable $requestBudgetsTable */
        $requestBudgetsTable = $this->getTableLocator()->get('RequestBudgets');
        $budget = $requestBudgetsTable->patchEntity(
            $budget,
            $data,
            [
                'associated' => [
                    'Incomes',
                    'Costs',
                    'OwnSources',
                    'OtherSubsidies',
                ],
            ]
        );
        $budget->recountItems();

        return $budget;
    }

    /**
     * Prototype method for getting custom budget  definitions 
     * 
     * @return array
     */

    public function getDefinitions():array {
        return [];
    }

    public function afterSuccessfullSave(RequestBudget $budget, UserRequestsController $controller): void
    {
        $budget->deleteRemovedItems($controller->BudgetItems);
    }

    public function canUserEditRequestedAmount(): bool
    {
        return true;
    }

    public function validateBudgetRequirements(RequestBudget $budget, Appeal $appeal, Program $program)
    {
        if (empty($budget->requested_amount)) {
            return RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT;
        }
        //if ($program->requires_budget && empty($budget->own_sources) && empty($budget->other_subsidies)) {
            // this shall be replaced by additional program setting, that organization requires the own-sources and other-subsidies provided
            //return self::BUDGET_MISSING_BUDGET;
        //}
        if ($program->requires_balance_sheet && empty($budget->total_costs) && empty($budget->costs) && empty($budget->total_income) && empty($budget->incomes)) {
            return RequestBudget::BUDGET_MISSING_BALANCE_SHEET;
        }
        if (!$appeal->checkRequestLimit($budget->requested_amount, $program->id)) {
            return RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED;
        }

        return RequestBudget::BUDGET_OK;
    }

}