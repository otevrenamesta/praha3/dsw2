<?php

namespace App\Middleware;

use App\Model\Entity\User;
use App\Traits\EmailThrowableTrait;
use Cake\Controller\Exception\AuthSecurityException;
use Cake\Http\Exception\InvalidCsrfTokenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\ServerRequest;
use Cake\Routing\Exception\MissingRouteException;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

class ErrorHandlerMiddleware extends \ErrorEmail\Middleware\ErrorHandlerMiddleware
{
    use EmailThrowableTrait;

    /**
     * Add email funcitonality to handle exception
     *
     * @param Exception $exception The exception to handle.
     * @param ServerRequestInterface $request The request.
     * @return ResponseInterface A response
     */
    public function handleException($exception, $request, $response)
    {
        if (($request instanceof ServerRequest) && !$this->middlewareSkipEmail($exception, $request)) {
            // allow correct IP in email
            $request->trustProxy = true;
            // Add emailing throwable functionality
            $this->emailThrowable($exception);
            // Use parent funcitonality
        }

        return $this->_callParent($exception, $request, $response);
    }

    private function middlewareSkipEmail(?Throwable $exception, ?ServerRequest $request)
    {
        if (empty($exception) || empty($request)) {
            return false;
        }

        $userdata = $request->getSession()->read('Auth.User');
        if (!empty($exception) && (empty($userdata) || !($userdata instanceof User))) {
            // skip emailing for these errors if it's unauthenticated (no user) request
            $exception_class = get_class($exception);
            switch ($exception_class) {
                case MissingRouteException::class:
                case InvalidCsrfTokenException::class:
                case NotFoundException::class:
                case AuthSecurityException::class:
                    return true;
            }
        }

        return false;
    }
}
