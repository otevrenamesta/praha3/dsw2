<?php

namespace App\View\Helper;

class FormHelper extends \Cake\View\Helper\FormHelper
{

    public function control($fieldName, array $options = [])
    {
        $class = isset($options['class']) ? $options['class'] : '';
        if ($this->isFieldError($fieldName)) {
            $class .= ' is-invalid';
        }
        if (endsWith($fieldName, '._ids') && mb_strpos($class, 'treeselect') === false) {
            $class .= ' select2';
        }
        $options['templateVars']['class'] = $class;

        if ((isset($options['type']) && $options['type'] === 'checkbox') || isset($options['checked']) || (isset($options['value']) && is_bool($options['value']))) {
            $options['templateVars']['customType'] = 'form-check';
        }
        $options['nestedInput'] = false;


        if (isset($options['type']) && $options['type'] === 'file') {

            if (!isset($options['templates']) || !is_array($options['templates'])) {
                $options['templates'] = [];
            }

            // Extract description from the label and place it after the <legend></legend> tag
            $caption = $options['label'];
            $description = '';
            if ($pos = strpos($options['label'], '<legend')) {
                $caption = substr($options['label'], 0, $pos);
                $description = strip_tags(substr($options['label'], $pos), '<a> <em> <i> <strong>');
            }
            $options['label'] = $caption;
            $options['templates']['formgroup'] = '{{label}}{{input}}';
            $options['templates']['label'] = '<label {{attrs}}>{{text}}</label><legend class="small">{{description}}</legend><div class="custom-file-upload-container clearfix"><span class="custom-file-label">' . __('Nahrajte soubor') . '</span></div>';
            $options['templateVars']['customType'] = 'custom-file mb-2';
            $options['templateVars']['description'] = $description;
            //TODO: Fix via the template
            $out = parent::control($fieldName, $options);
            $out = str_replace('</div><input type="file"','<input type="file"',$out);
            $out = str_replace('</div>','</div></div><p>&nbsp;</p>',$out);
            return $out;
        }

        return parent::control($fieldName, $options);
    }

    public function submit($caption = null, array $options = [])
    {
        if (empty($options['class'])) {
            $options['class'] = 'btn btn-success';
        }
        return parent::submit($caption, $options);
    }
}
