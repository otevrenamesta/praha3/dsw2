<?php

namespace App\View\Helper;

use Cake\View\Helper;
use \DOMDocument;
use DOMElement;

class AnonymizeHelper extends Helper
{

    const ANONYMIZE_EDIT = 1;
    const ANONYMIZE_VIEW = 2;
    const TYPE_ANONYMIZED = 7;

    /**
     * @param string $s input string
     * 
     * @return string
     * 
     * Replace &nbsp; with spaces 
     */

    function replaceNbsps(string $s): string
    {
        $s = html_entity_decode($s);
        $s = str_replace("\xc2\xa0", ' ', $s);
        return trim($s);
    }

    /**
     * @param mixed $n
     * 
     * @return DOMElement
     *
     * 1:1 echo just to keep the IDE happy 
     */
    private static function castDOMNode($n): DOMElement
    {
        return $n;
    }

    function processReplacements(string $haystack, string $json_replacements, int $mode = AnonymizeHelper::ANONYMIZE_VIEW): string
    {

        
        $haystack = preg_replace("/&(?!\S+;)/", "&amp;", $haystack);// See: https://stackoverflow.com/questions/14648442/domdocumentloadhtml-warning-htmlparseentityref-no-name-in-entity
        $doc = new DOMDocument();
        $doc->loadHTML('<?xml encoding="utf-8" ?>' . $haystack);
        $editableNodes = $doc->getElementsByTagName('div');
        $i = $editableNodes->length - 1;

        $replacements = json_decode($json_replacements, true);
        $searches = [];
        $replaces = [];
        foreach ($replacements as $rep) {
            $searches[] = $rep[0];
            $replaces[] = $rep[1];
        }
        if (!empty($searches)) {

            while ($i > -1) {

                $div = AnonymizeHelper::castDOMNode($editableNodes->item($i));
                $classes = explode(' ', $div->getAttribute('class'));
                if (in_array('ed', $classes)) {  // Editable div
                    $match = $this->replaceNbsps($div->nodeValue);
                    if (in_array($match, $searches)) {
                        $new_value = str_replace($searches, $replaces, $match);
                        $new_element = $doc->createElement('div', $new_value);
                        $c = ($mode == AnonymizeHelper::ANONYMIZE_EDIT)?"edited":"changed";
                        $new_element->setAttribute('class', 'ed ' . $c);
                        if ($mode == AnonymizeHelper::ANONYMIZE_EDIT) {
                            $new_element->setAttribute('data-orig', $match);
                            $new_element->setAttribute('data-toggle', "tooltip");
                            $new_element->setAttribute('data-placement', "bottom");
                            $new_element->setAttribute('title', $match);
                        }
                        $div->parentNode->replaceChild($new_element, $div);
                    }
                }
                $i--;
            }
        }

        return $doc->saveHTML();
    }
}
