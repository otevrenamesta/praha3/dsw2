<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\User;
use App\Model\Table\I18nMessagesTable;
use Cake\Cache\Cache;

/**
 * @property I18nMessagesTable $I18nMessages
 */
class TranslationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('I18nMessages');
    }

    /**
     * @param  null|User $user
     * @return bool
     */
    public function isAuthorized($user = null): bool
    {
        return parent::isAuthorized($user) && ($user->isSystemsManager() || $user->isPortalsManager());
    }

    public function index()
    {
        // filtering default items and items for the current organization (without "cake" items)
        $conditions = array(
            'and' => [

                'or' => [
                    [
                        'and' => [
                            'domain' => 'default',
                            'singular NOT IN' => $this->I18nMessages->find()->select('singular')->where(
                                sprintf('domain = "%s"', OrgDomainsMiddleware::getCurrentI18nDefaultDomain())
                            )
                        ]
                    ],
                    [
                        'and' => [
                            'domain' => 'email',
                            'singular NOT IN' => $this->I18nMessages->find()->select('singular')->where(
                                sprintf('domain = "%s"', OrgDomainsMiddleware::getCurrentI18nEmailDomain())
                            ),
                        ]
                    ],
                    'domain IN' => [
                        OrgDomainsMiddleware::getCurrentI18nDefaultDomain(),
                        OrgDomainsMiddleware::getCurrentI18nEmailDomain()
                    ]
                ],
                'locale IN' => [
                    'cs'
                ],
            ]
        );

        $this->set(
            'messages',
            $this->I18nMessages->find('all', [
                'conditions' => $conditions
            ])
        );
    }

    public function addModify(int $id = 0, $copy = false)
    {
        $message = $id > 0 ? $this->I18nMessages->get($id) : $this->I18nMessages->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $message = $this->I18nMessages->patchEntity($message, $this->getRequest()->getData());
            if (empty(trim($message->value_0))) {
                // ability to remove translation
                $message->value_0 = null;
            }
            // copying record from "domain" into "domain_ID"
            if ($copy === true && $id > 0) {
                $message = $this->I18nMessages->newEntity([
                    'domain' => $message->domain . '_' . OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'locale' => $message->locale,
                    'singular' => $message->singular,
                    'value_0' => $message->value_0
                ]);
            }
            // saving
            if ($this->I18nMessages->save($message)) {
                $this->Flash->success(__('Překlad byl uložen úspěšně'));
                $this->redirect(['action' => 'index']);
                Cache::clear(false, '_cake_core_');
            } else {
                $this->Flash->error(__('Nastala chyba při ukládání'));
            }
        }

        $this->set('crumbs', [__('Překlady') => 'admin_translations']);
        $this->set(compact('message'));
    }

    public function formCopy(int $id = 0)
    {
        $this->addModify($id, true);
    }
}
