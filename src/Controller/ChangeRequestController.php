<?php

declare(strict_types=1);

namespace App\Controller;

use App\Budget\ProjectBudgetFactory;
use App\Controller\UserRequestsController;
use App\Form\AbstractFormController;
use App\Form\StandardRequestFormChangeController;
use App\Form\StandardRequestFormController;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Form;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudgetChange;
use App\Model\Entity\RequestChange;
use App\Model\Entity\User;
use App\Model\Table\RequestBudgetsChangesTable;
use App\Model\Table\RequestsTable;
use Cake\Http\Exception\NotFoundException;

/**
 * @property-read AppealsTable $Appeals
 * @property-read IdentitiesTable $Identities
 * @property-read OldDswComponent $OldDsw
 * @property-read RequestsTable $Requests
 * @property-read SettlementsTable $Settlements
 * @property-read ProgramsTable $Programs
 */
class ChangeRequestController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Requests');
        $this->loadModel('RequestBudgetsChanges');
        $this->loadModel('RequestChanges');
        $this->loadModel('RequestBudgets');
    }

    public function isAuthorized($user = null): bool
    {
        if (in_array($this->getRequest()->getParam('action'), ['requestFormChangeView', 'requestBudgetChangeView'])) {
            /**
             * @var User $user
             */
            $user = $user instanceof User ? $user : $this->Auth->user();

            return parent::isAuthorized($user);
        }

        return parent::isAuthorized($user) && $this->getCurrentUser()->isEconomicsManager();
    }

    public function getNumPending()
    {
        $this->initialize();
        $defaultConditions = [
            RequestBudgetChange::CHANGE_PENDING,
        ];
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'status IN' => $defaultConditions,
        ];
        $budget_change_requests = $this->RequestBudgetsChanges->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => ['Requests'],
            ]
        )->toArray();
        $form_change_requests = $this->RequestChanges->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => ['Requests'],
            ]
        )->toArray();

        return sizeof($budget_change_requests) + sizeof($form_change_requests);
    }

    public function index()
    {

        $states = RequestBudgetChange::getLabels();
        $defaultConditions = [
            //RequestBudgetChange::CHANGE_ACCEPTED,
            //RequestBudgetChange::CHANGE_DECLINED,
            RequestBudgetChange::CHANGE_PENDING,
        ];
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'status IN' => $defaultConditions,
        ];

        if ($this->getRequest()->is(['get', 'post', 'put', 'patch'])) {
            if ($this->getRequest()->is('get')) {
                $change_request_state_ids = $this->getRequest()->getQuery('state_ids');
            } else {
                $change_request_state_ids = $this->getRequest()->getData('state_ids');
            }

            if (is_array($change_request_state_ids)) {
                foreach ($change_request_state_ids as $change_request_state_id) {
                    if (!in_array(intval($change_request_state_id), RequestBudgetChange::KNOWN_STATUSES, true)) {
                        throw new NotFoundException();
                    }
                }
                $conditions['status IN'] = $change_request_state_ids;
            }
        }


        $budget_change_requests = $this->RequestBudgetsChanges->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Requests',
                ]
            ]
        )->toArray();


        $form_change_requests = $this->RequestChanges->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Requests',
                ],
            ]
        )->toArray();

        $change_requests = [];

        // Collate
        foreach ($budget_change_requests as $bchr) {
            $bchr->change_type = "BUDGET";
            $change_requests[] = $bchr;
        }

        foreach ($form_change_requests as $fchr) {
            $fchr->change_type = "FORM";
            $change_requests[] = $fchr;
        }

        // Sort them by date
        usort($change_requests, function ($a, $b) {
            return $b->created->getTimestamp() <=> $a->created->getTimestamp();
        });


        $this->setRequest(
            $this->getRequest()
                ->withData('state_ids', $conditions['status IN'])
        );

        $withNotifications = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, true));

        $this->set(compact('change_requests', 'states'));
        $this->set('crumbs', [__('Žádosti o změnu') => 'change_request_index']);
        $this->set('defaultStates', array_values($defaultConditions));
    }

    public function getChangeBudgetRequestByID($change_request_id)
    {
        return $this->RequestBudgetsChanges->get(
            $change_request_id,
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => 'Requests',
            ]
        );
    }

    public function getChangeFormRequestByID($change_request_id): RequestChange
    {
        return $this->RequestChanges->get(
            $change_request_id,
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => 'Requests',
            ]
        );
    }


    public function getBudgetByID($budget_id)
    {
        return $this->RequestBudgets->get($budget_id);
    }

    private function getBudgetBefore($request_id, $change_request_id)
    {
        $previous_budget_chages = $this->RequestBudgetsChanges->find(
            'all',
            [
                'conditions' => [
                    'request_id' => $request_id,
                    'id NOT IN' => [$change_request_id],
                    'status IN' => [RequestBudgetChange::CHANGE_ORIGINAL, RequestBudgetChange::CHANGE_ACCEPTED]
                ],
                'order' => ['id' => 'DESC']
            ]
        )->toArray();
        if (empty($previous_budget_chages)) {
            throw new Exception('Nothing to compare the budget to !');
            return;
        } else {
            return array_shift($previous_budget_chages);
        }
    }

    public function requestBudgetChangeDetail(int $change_request_id, ?string $read_mode = '')
    {
        $change_request = $this->getChangeBudgetRequestByID($change_request_id);
        $request = $this->Requests->get(
            $change_request->request_id,
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => ['RequestBudgets', 'Programs']
            ]
        );

        $design_id = $request->program->project_budget_design_id;

        // Usti wants to change budget design is for V11 and V12
        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V11) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE;
        }

        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE;
        }

        $budgetHandler = ProjectBudgetFactory::getBudgetHandler($design_id);

        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE || $design_id == ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE) {
            $budget = $change_request;
            $budget_new = $change_request;
            $mode = 'V11orV12';
            $change_request_state = $change_request->status;
        } else {
            $budget_before = $this->getBudgetBefore($change_request->request_id, $change_request_id);
            $budget = $budget_before;
            $budget_new = $change_request;
            $mode = 'view_changes';
            $change_request_state = $change_request->status;
        }

        $this->set(compact('request', 'budget', 'budget_new', 'design_id', 'change_request_id', 'change_request_state', 'mode', 'read_mode'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestBudgetChangeDetail', $request->id], 'Žádost o změnu' => '#',]);

        return $this->render('../UserRequests/' . $budgetHandler->getTemplate());
    }

    public function requestBudgetChangeView(int $change_request_id)
    {
        $this->requestBudgetChangeDetail($change_request_id, 'read_only');
    }

    public function requestFormChangeDetail(int $change_request_id, ?string $mode = 'view_changes')
    {
        $request = NULL; // To be populated by getFormByChangeID call
        $change_request = NULL; // To be populated by getFormByChangeID call
        $requestForm = $this->getFormByChangeID($change_request_id, $request, $change_request);

        $highlight = $requestForm->highlight_ids;

        $this->set(compact('highlight', 'mode', 'request', 'requestForm', 'change_request'));

        //$this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestBudgetChangeDetail', $request->id], 'Žádost o změnu' => '#',]);
        return $this->render('../UserRequests/form_fill');
    }

    public function requestFormChangeView(int $change_request_id)
    {
        $this->requestFormChangeDetail($change_request_id, 'read_only');
    }

    private function getFormById(int $form_id): Form
    {
        return $this->Requests->Programs->Forms->get(
            $form_id,
            [
                'contain' => [
                    'FormFields',
                    'FormFields.FormFieldTypes',
                ],
            ]
        );
    }

    public function changeRequestDetail(int $id)
    {

        $this->set(compact('settlement', 'attachmentTypes', 'program'));
        $this->set('crumbs', [__('Žádosti o změnu') => 'my_economics', __('Vyúčtování') => 'my_economic_settlements']);
    }

    /**
     * Update budget witch change request budget
     *
     * @param mixed $budget
     * @param mixed $change_request
     *
     * @return [type]
     *
     */

    public function updateBudget($budget, $change_request)
    {
        foreach ($budget->getVisible() as $key) {
            if ($key == 'id') continue;
            $budget[$key] = $change_request->{$key};
        };
        return $budget;
    }

    public function requestBudgetChangeAccept(int $change_request_id)
    {
        $change_request = $this->getChangeBudgetRequestByID($change_request_id);
        if (!$change_request) {
            throw new Exception('Request to accept budget change not found !');
        } else {
            $request = $this->Requests->get(
                $change_request->request_id,
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => ['RequestBudgets', 'Programs', 'Users']
                ]
            );

            $budget = $this->getBudgetByID($request->request_budget->id);
            if (!$budget) {
                throw new Exception('Budget not found!');
            }
            $budget = $this->updateBudget($budget, $change_request);

            $change_request->status = RequestBudgetChange::CHANGE_ACCEPTED;
            if ($this->RequestBudgetsChanges->save($change_request)) {
                if ($this->RequestBudgets->save($budget)) {
                    if ($this->sendMailSafe('User', 'changeRequestNotice', [$request->user, $request, $change_request])) {
                        $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                    }
                    $this->Flash->success(__('Žádost o změnu rozpočtu byla akceptována.'));
                    $this->redirect(['action' => 'index']);
                } else {
                    throw new Exception('Error updating budget via change !');
                }
            } else {
                throw new Exception('Error saving budget change !');
            }
        }
    }

    public function requestBudgetChangeDecline(int  $change_request_id)
    {
        $change_request = $this->getChangeBudgetRequestByID($change_request_id);
        if (!$change_request) {
            throw new Exception('Request to decline budget change not found !');
        } else {
            $request = $this->Requests->get(
                $change_request->request_id,
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => ['RequestBudgets', 'Programs', 'Users']
                ]
            );
            $change_request->status = RequestBudgetChange::CHANGE_DECLINED;
            if ($this->RequestBudgetsChanges->save($change_request)) {
                if ($this->sendMailSafe('User', 'changeRequestNotice', [$request->user, $request, $change_request])) {
                    $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                }
                $this->Flash->success(__('Žádost o změnu rozpočtu byla zamítnuta.'));
                $this->redirect(['action' => 'index']);
            } else {
                throw new Exception('Error saving budget change !');
            }
        }
    }

    public function getFormByChangeID(int $change_request_id, Request &$request = null, RequestChange &$change_request = null, $return_original = false): AbstractFormController
    {

        $change_request = $this->getChangeFormRequestByID($change_request_id);


        $form = $this->getFormById($change_request->form_id);

        $request = $this->Requests->get(
            $change_request->request_id,
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => ['RequestBudgets', 'Programs']
            ]
        );

        if ($return_original) {
            $requestForm = $form->getFormController($request);
        } else {
            $requestForm = $form->getFormController($request, true);
        }


        $requestForm->prefill($request, $change_request);

        return $requestForm;
    }

    public function requestFormChangeAccept(int $change_request_id)
    {
        $change_request = $this->getChangeFormRequestByID($change_request_id);
        if (!$change_request) {
            throw new Exception('Request to accept budget change not found !');
        } else {
            $request = $this->Requests->get(
                $change_request->request_id,
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => ['Programs', 'Users']
                ]
            );

            $change_form = $this->getFormByChangeID($change_request_id);
            $live_form = $this->getFormByChangeID($change_request_id, $foo1, $foo2, true);

            $change_request->status = RequestChange::CHANGE_ACCEPTED;
            if ($this->RequestChanges->save($change_request)) {
                // This is where the data from the proposed change overwrite the original form values
                if ($live_form->execute($change_form->get__data())) {

                    if ($this->sendMailSafe('User', 'changeRequestNotice', [$request->user, $request, $change_request])) {
                        $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                    }
                    $this->Flash->success(__('Žádost o změnu rozpočtu byla akceptována.'));
                    $this->redirect(['action' => 'index']);
                } else {
                    throw new Exception('Error updating form via change !');
                }
            } else {
                throw new Exception('Error saving budget change !');
            }
        }
    }

    public function requestFormChangeDecline(int  $change_request_id)
    {

        $change_request = $this->getChangeFormRequestByID($change_request_id);
        if (!$change_request) {
            throw new Exception('Request to decline form change not found !');
        } else {
            $request = $this->Requests->get(
                $change_request->request_id,
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => ['RequestBudgets', 'Programs', 'Users']
                ]
            );
            $change_request->status = RequestBudgetChange::CHANGE_DECLINED;
            if ($this->RequestChanges->save($change_request)) {
                if ($this->sendMailSafe('User', 'changeRequestNotice', [$request->user, $request, $change_request])) {
                    $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                }
                $this->Flash->success(__('Žádost o změnu rozpočtu byla zamítnuta.'));
                $this->redirect(['action' => 'index']);
            } else {
                throw new Exception('Error saving budget change !');
            }
        }
    }

    public static function changeRequestNotifyOrgByEmail($text, Request $request)
    {
        $appController = new AppController();
        $id = $request->id;
        $text = str_replace('%name', $request->name, $text);
        $text = str_replace('%id', "$id", $text);
        $email = OrganizationSetting::getSetting(OrganizationSetting::CONTACT_EMAIL);
        if ($appController->sendMailSafe('User', 'changeRequestAlert', [$request->user, $request, $email, __("Nový požadavek na změnu v žádosti ") . $request->id, $text])) {
            $request->addMailLog($appController->getCurrentUser()->id, __('Upozornění organizaci na změnu stavu žádosti'), '');
        }
    }

    public static function identityChangeNotify($user)
    {
        $finder = new ChangeRequestController();
        $finder->initialize();
        $requests = $finder->Requests->find(
            'all',
            [
                'conditions' => [
                    'Requests.user_id' => $user->id,
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->toArray();
        $text = '';
        foreach ($requests as $request) {
            if ($request->canUserIssueRequestChange()) {
                $text .= $request->name . ' (' . $request->id . ')<br>';
            }
        }
        if (trim($text)) {
            $appController = new AppController();
            $text = __('Došlo k změně údajů v identitě uživatele u těchto podaných žádostí:<br>') . $text;
            $email = OrganizationSetting::getSetting(OrganizationSetting::CONTACT_EMAIL);
            if ($appController->sendMailSafe('User', 'changeRequestAlert', [$user, $request, $email, __("Změna v identitě uživatele id ") . $user->id, $text])) {
                $request->addMailLog($user->id, __('Upozornění organizaci na změnu stavu žádosti'), '');
            }
        }
    }
}
