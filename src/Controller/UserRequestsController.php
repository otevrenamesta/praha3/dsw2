<?php

namespace App\Controller;

use App\Budget\ProjectBudgetFactory;
use App\Controller\Component\IsdsComponent;
use App\Form\IdentityForm;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\EconomicsController;
use App\Model\Entity\Form;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestBudgetChange;
use App\Model\Entity\RequestChange;
use App\Model\Entity\RequestState;
use App\Model\Entity\SettlementAttachmentType;
use App\Model\Entity\SettlementState;
use App\Model\Entity\SettlementsToOlddswRequest;
use App\Model\Entity\SettlementType;
use App\Model\Entity\Settlement;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\AppealsTable;
use App\Model\Table\AppealsToProgramsTable;
use App\Model\Table\BudgetItemsTable;
use App\Model\Table\FondsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\PublicIncomeHistoriesTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\Model\Table\RequestNotificationsTable;
use App\Model\Table\RequestNotificationTypesTable;
use App\Model\Table\RequestsTable;
use App\Model\Table\RequestBudgetsChangesTable;
use App\Model\Table\SettlementsTable;
use App\Model\Table\HistoryIdentitiesTable;
use App\Model\Table\UsersTable;
use App\Traits\AppealsAwareTrait;
use App\Traits\PdfDownloadTrait;
use Cake\Core\Configure;
use Cake\Database\Query;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use OldDsw\Controller\Component\OldDswComponent;
use OldDsw\Model\Entity\Zadost;
use Throwable;
use Exception;
use function Laminas\Diactoros\createUploadedFile;

/**
 * @property RequestsTable $Requests
 * @property RequestFilledFieldsTable $RequestFilledFields
 * @property PublicIncomeHistoriesTable $PublicIncomeHistories
 * @property OldDswComponent $OldDsw
 * @property FondsTable $Fonds
 * @property IsdsComponent $Isds
 * @property BudgetItemsTable $BudgetItems
 * @property OrganizationsTable $Organizations
 * @property-read ProgramsTable $Programs
 * @property SettlementsTable $Settlements
 * @property AppealsTable $Appeals
 * @property RequestNotificationsTable $RequestNotifications
 * @property RequestNotificationTypesTable $RequestNotificationTypes
 * @property HistoryIdentitiesTable $HistoryIdentities
 */
class UserRequestsController extends AppController
{
    use AppealsAwareTrait;
    use PdfDownloadTrait;

    public function isAuthorized($user = null): bool
    {
        /**
         * @var User $user
         */
        $user = $user instanceof User ? $user : $this->Auth->user();

        return parent::isAuthorized($user);
    }

    public function initialize()
    {
        parent::initialize();
        if ($this->getCurrentUser() instanceof User && $this->getCurrentUser()->hasRoles([UserRole::MANAGER_SYSTEMS], OrgDomainsMiddleware::getCurrentOrganizationId(), true)) {
            Configure::write('debug', true);
        }
        $this->loadModel('Requests');
        $this->loadModel('BudgetItems');
        $this->loadModel('Fonds');
        $this->loadModel('Appeals');
        $this->loadModel('Programs');
        $this->loadModel('PublicIncomeHistories');
        $this->loadModel('RequestFilledFields');
        $this->loadModel('Organizations');
        $this->loadModel('Settlements');
        $this->loadModel('RequestNotifications');
        $this->loadModel('RequestNotificationTypes');
        $this->loadModel('HistoryIdentities');
        $this->loadModel('Identities');
        $this->loadModel('RequestBudgetsChanges');
        $this->loadModel('RequestChanges');

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true) === true) {
            $this->loadComponent('OldDsw.OldDsw');
        }
        $this->loadComponent('Isds');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->config('unlockedActions', ['formFill']);
    }

    public function attachmentAdd(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if (!RequestState::canUserEditRequest($request->request_state_id)) {
            $this->redirect(['action' => 'requestDetail', $request->id]);

            return;
        }
        $file = $this->Requests->Files->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $filedata = $this->request->getData('filedata');
            $uploadedFile = createUploadedFile($filedata);

            if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                $file->setError('filedata', __('Nahrávání souboru se nezdařilo'));
                $this->Flash->error(__('Nahrávání souboru se nezdařilo'));
            } else {
                $fileMoveResult = $file->fileUploadMove($filedata, $request->user_id);

                if (is_string($fileMoveResult)) {
                    $file->filepath = $fileMoveResult;
                    $file->original_filename = $uploadedFile->getClientFilename();
                    $file->filesize = $uploadedFile->getSize();
                    $file->user_id = $request->user_id;

                    $request->files[] = $file;
                    $request->setDirty('files');

                    if ($this->Requests->save($request, ['associated' => ['Files']])) {
                        $this->Flash->success(__('Soubor byl úspěšně nahrán'));
                        $this->redirect(['action' => 'requestDetail', $request_id]);
                    } else {
                        $this->Flash->error(__('Error adding new version of file'));
                    }
                }
            }
        }

        $this->set(compact('request', 'file'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
    }

    /**
     * Ajax endpoint for deleting files uploaded by FIELD_FILE_MULTIPLE
     *
     * @param int $settlement_id
     *
     * @return [type]
     */
    public function formFileDelete(int $request_id, int $file_id, int $form_id, int $field_id)
    {
        $request = $this->getRequestById($request_id);
        if (!$request) return $this->ajaxFail();

        /** @var FilesTable $filesTable */
        $filesTable = TableRegistry::getTableLocator()->get('Files');
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        $file = $filesTable->find(
            'all',
            [
                'conditions' => [
                    'Files.user_id' => $request->user_id,
                    'Files.id' => $file_id,
                ],
            ]
        )->first();
        if (!$file) {
            return $this->ajaxFail();
        }

        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        $filledField = $filledFieldsTable->findOrCreate(
            [
                'form_id' => $form_id,
                'request_id' => $request_id,
                'form_field_id' => $field_id,
            ]
        );
        if (!$filledField) {
            return $this->ajaxFail();
        }
        try {
            $value = unserialize($filledField->value);
        } catch (Throwable $t) {
            return $this->ajaxFail();
        }

        if (($key = array_search($file_id, $value)) !== false) {
            unset($value[$key]);
        } else {
            return $this->ajaxFail();
        }
        $filledField->value = serialize($value);
        $filledField->setDirty('value');

        try {
            $result = $filledFieldsTable->save($filledField);
            if (!$result) {
                return $this->ajaxFail();
            }
        } catch (Throwable $t) {
            return $this->ajaxFail();
        }

        try {
            if ($filesTable->delete($file)) {
                $file->deletePhysically();
            }
        } catch (Throwable $t) {
            // failing to file
            Log::error($t);
            return $this->ajaxFail();
        }
        echo ($file_id);
        $this->response->withType('json');
        $this->response->withstringBody('OK');
        return $this->response;
    }

    private function ajaxFail()
    {
        die('FAIL');
        $this->response->withType('json');
        $this->response->withstringBody('FAIL');
        return $this->response;
    }

    public function formfileDownload(int $request_id, int $file_id)
    {
        $user = $this->getCurrentUser();
        $request = $this->getRequestById($request_id, $user->isManager());
        $grantAccess = ($user->id == $request->user_id) || $user->isManager();
        if ($grantAccess) {
            /** @var FilesTable $filesTable */
            $filesTable = TableRegistry::getTableLocator()->get('Files');
            /** @noinspection PhpIncompatibleReturnTypeInspection */
            $file = $filesTable->find(
                'all',
                [
                    'conditions' => [
                        //'Files.user_id' => $request->user_id, // we must allow managers too
                        'Files.id' => $file_id,
                    ],
                ]
            )->first();
            if (!$file) {
                return $this->getResponse()->withStatus(404);
            }
            $target = $file->getRealPath();
            $_file = new File($target);

            if ($_file->exists()) {
                return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
            }

            return $this->getResponse()->withStatus(404);
        }
        return $this->getResponse()->withStatus(403); // Access denied
    }

    public function fileDownload(int $request_id, int $file_id)
    {
        $user = $this->getCurrentUser();
        $request = $this->getRequestById($request_id, $user->isManager());
        $file = $request->findAttachment($file_id, $user->isManager());
        if (!$file) {
            return $this->getResponse()->withStatus(404);
        }
        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }

    public function attachmentDelete(int $request_id, int $attachment_id)
    {
        $request = $this->getRequestById($request_id);
        if (RequestState::canUserEditRequest($request->request_state_id)) {
            $file = $request->findAttachment($attachment_id);
            if ($this->Requests->Files->delete($file)) {
                $file->deletePhysically();
            }
        }
        $this->redirect(['action' => 'requestDetail', $request_id]);
    }

    public function formFillReset(int $request_id, int $form_id)
    {
        $request = $this->getRequestById($request_id);
        $form = $request->getFormById($form_id);

        $status = false;

        if ($form instanceof Form && RequestState::canUserEditRequest($request->request_state_id)) {
            $requestForm = $form->getFormController($request);
            $status = $requestForm->deleteSavedData();
        }

        if ($status === true) {
            $this->Flash->success(__('Vyplněné hodnoty formuláře byly kompletně vymazány'));
        } else {
            $this->Flash->error(__('Data formuláře nemohla být vymazána'));
        }

        return $this->redirect(['action' => 'formFill', 'id' => $request->id, 'form_id' => $form->id]);
    }
    public function formFillChange(int $request_id, int $form_id)
    {
        $request = $this->getRequestById($request_id);
        $form = $request->getFormById($form_id);
        if (empty($form)) {
            $this->Flash->error(__('Formulář nebyl nalezen'));

            $this->redirect(['action' => 'requestDetail', $request_id]);

            return;
        }
        $form = $this->getFormById($form_id);


        $requestForm = $form->getFormController($request, true);

        $requestForm->setUser($this->getCurrentUser());
        // if (RequestState::canUserEditRequest($request->request_state_id) && $this->getRequest()->is(['post', 'put', 'patch'])) {
        // TODO: Perform more comprehensive check than just $form->allow_change (depending on the request status)
        if ($form->allow_change && $this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($requestForm->execute($this->getRequest()->getData())) {
                $this->Flash->success(__('Žádost o změnu v formuláři byla odeslána'));
                $this->redirect($this->retrieveReferer() ?? ['_name' => 'request_detail', 'id' => $request_id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }
        $mode = "track_changes";
        $this->viewBuilder()->setTemplate('form_fill');
        $this->persistReferer();
        $this->set(compact('mode', 'request', 'requestForm'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestChange', $request_id]]);
    }

    public function formFill(int $request_id, int $form_id)
    {
        $request = $this->getRequestById($request_id);
        $form = $request->getFormById($form_id);
        if (empty($form)) {
            $this->Flash->error(__('Formulář nebyl nalezen'));

            $this->redirect(['action' => 'requestDetail', $request_id]);

            return;
        }
        $form = $this->getFormById($form_id);

        $requestForm = $form->getFormController($request);
        $requestForm->setUser($this->getCurrentUser());

        if ($form->shared) {
            $sharedRequests = $request->getRequestsSet();
        } else {
            $sharedRequests = [];
        }

        if (RequestState::canUserEditRequest($request->request_state_id) && $this->getRequest()->is(['post', 'put', 'patch'])) {

            if ($requestForm->execute($this->getRequest()->getData())) {
                $this->Flash->success(__('Formulář byl uložen'));
                $this->redirect($this->retrieveReferer() ?? ['_name' => 'request_detail', 'id' => $request_id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->persistReferer();
        $this->set(compact('request', 'requestForm', 'sharedRequests'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
    }

    public function getPdfView(int $id)
    {
        $this->getPdf($id, false);
    }

    public function getPdf(int $id, bool $forceDownload = true, string $orientation = 'portrait')
    {
        $request = $this->getRequestById($id);
        if (!$request->canUserDownloadPdf()) {
            $this->Flash->error(__('Tuto žádost ještě není možné stáhnout'));
            $this->redirect($this->referer());

            return;
        }
        $request->user->public_income_histories = array_merge(
            (isset($request->user->public_income_histories) ? $request->user->public_income_histories : []),
            $this->historyFromArchive($request)
        );
        $this->set('request', $request);

        Configure::write('debug', false);
        Configure::write(
            'CakePdf',
            [
                //'engine' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'App.WeasyPrint',

                'orientation' => $orientation,
                'margin' => [
                    'bottom' => 6,
                    'left' => 3,
                    'right' => 3,
                    'top' => 6,
                ],
            ]
        );
        if ($forceDownload) {
            $this->viewBuilder()->setOptions(
                [
                    'pdfConfig' => [
                        'download' => $forceDownload,
                        'filename' => urlencode($request->name) . '.pdf',
                    ],
                ]
            );
        }
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->setTemplate('/UserRequests/get_pdf');
    }

    public function index()
    {
        $reqs = $this->Requests->find(
            'all',
            [
                'conditions' => [
                    'Requests.user_id' => $this->getCurrentUserId(),
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Appeals',
                    'Programs',
                    'PaperEvidencingUsers',
                    'RequestChanges',
                    'RequestBudgetsChanges',
                ],
            ]
        )->order(['Requests.modified' => 'DESC'])->notMatching('PaperEvidencingUsers', function (Query $query) {
            return $query->where(['PaperEvidencingUsers.id' => $this->getCurrentUserId()]);
        });

        $requests = [];
        $merged_request = [];

        foreach ($reqs as $key => $request) {
            $request->loadSettlements();
            $requests[$key] = $request;
            if ($request['merged_request'] > 0) {
                $merged_request[] = $request['merged_request'];
            }
        }

        $this->set(compact('requests', 'merged_request'));

        if ($this->getCurrentUser()->canSwitchUsers($this->getRequest())) {

            $shared_requests = $this->Requests->find('all', [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'Requests.user_id IN' => $this->getCurrentUser()->getUserIdsForSwitch($this->getRequest()),
                    'Requests.user_id !=' => $this->getCurrentUserId(),
                ],
                'contain' => [
                    'Users'
                ]
            ])->order(['Requests.modified' => 'DESC'])->toArray();

            $this->set(compact('shared_requests'));
        }

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true) === true) {
            $this->set('old_requests', $this->OldDsw->getZadostiByAccountIds($this->getCurrentUser()->getStareUctyIds(), true));
            $this->set('old_settlements', $this->Settlements->find('all', [
                'conditions' => [
                    'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()
                ],
                'contain' => [
                    'SettlementsToOlddswRequests'
                ]
            ])->matching('SettlementsToOlddswRequests'));
        }
    }

    public function settlementReturn(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $settlement_attachment = $this->Settlements->SettlementAttachments->newEntity();
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list')->order(['order' => 'ASC']);

        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);
        $settlement->notRequireAttachments = isset($program) ? boolval($program->not_require_tax_documents) : false;

        if (!$settlement->canUserEdit()) {
            $this->Flash->error(__('Tuto dotaci jste již vrátili'));
            $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id]);
            return;
        }

        if ($settlement->canUserEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $settlement = $this->Settlements->patchEntity($settlement, $this->getRequest()->getData());
            $settlement->settlement_type_id = SettlementType::FULL_SUBSIDY_RETURN;
            $this->settlementAddModifyFile($settlement_id);

            if ($settlement->getValidationsReduced()) {
                $settlement->setCurrentStatus(SettlementState::STATE_FILLED_COMPLETELY, $this->getCurrentUserId());
            }
            if (empty($settlement->return_reason)) {
                $settlement->setError('return_reason', __('Toto pole je při vrácení podpory v plné výši povinné'));
            }
            if (!$this->Settlements->save($settlement)) {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $canSubmitRightAway = !$settlement->hasErrors() && $settlement->canUserSubmitSettlement($this->getCurrentUser()) && $settlement->getValidationsReduced();

        if ($canSubmitRightAway) {
            $this->Flash->success(__('Nyní můžete odeslat informaci o vrácení podpory'));
        }

        $this->set(compact('settlement', 'canSubmitRightAway', 'settlement_attachment'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], sprintf("%s #%d", __('Vyúčtování žádosti'), $settlement->getRequest()->id) => ['action' => 'settlementDetail', 'settlement_id' => $settlement->id]]);
    }

    public function settlementDelete(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        if ($settlement->canUserDelete() && $this->Settlements->delete($settlement)) {
            $this->Flash->success(__('Smazáno úspěšně'));
            $this->redirect(['_name' => 'user_requests']);
        } else {
            $this->Flash->error(__('Vyúčtování není možné smazat'));
            $this->redirect($this->referer());
        }
    }

    public function settlementDetail(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $settlement_item = $this->Settlements->SettlementItems->newEntity();
        $settlement_attachment = $this->Settlements->SettlementAttachments->newEntity();
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list')->order(['order' => 'ASC']);

        if ($settlement->canUserEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $settlement = $this->Settlements->patchEntity($settlement, $this->getRequest()->getData());
            $settlement->agreement_no = trim($settlement->agreement_no);
            if ($this->Settlements->save($settlement)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect($this->getRequest()->getRequestTarget());
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);
        $settlement->notRequireAttachments = isset($program) ? boolval($program->not_require_tax_documents) : false;
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;

        list($hasOneItem, $itemsHasRequiredAttachments, $hasFinalReport, $hasProofOfPublicity, $hasAgreementNo, $hasNoEmptyAttachments) = $settlement->getValidations();

        if ($settlement->canUserEdit()) {
            $isReturned = $settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES;
            $state = $isReturned ? $settlement->settlement_state_id : SettlementState::STATE_NEW;
            if (!$isReturned && $hasOneItem && $itemsHasRequiredAttachments && $hasFinalReport && $hasProofOfPublicity && $hasAgreementNo && $hasNoEmptyAttachments) {
                $state = SettlementState::STATE_FILLED_COMPLETELY;
            }
            $settlement->setCurrentStatus($state, $this->getCurrentUserId());
            $settlement->fixItemsOrderNumber();
            if ($settlement->isDirty()) {
                $saved = $this->Settlements->save($settlement);
                if (!$saved) {
                    foreach ($settlement->getErrors() as $key => $errors) {
                        foreach ($errors as $key => $error) {
                            $this->Flash->error($error);
                        }
                    }
                }
            }
        }

        // add budget data
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index']]);
        $this->set(compact('settlement', 'settlement_item', 'attachmentTypes', 'settlement_attachment', 'program', 'mergedRequest', 'budget'));
    }

    public function settlementPdfView(int $settlement_id)
    {
        $this->settlementPdf($settlement_id, false);
    }

    public function settlementPdf(int $settlement_id, bool $forceDownload = true)
    {
        $settlement = $this->Settlements->getWithPermissionCheck(
            $settlement_id,
            $this->getCurrentUser(),
            OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true)
        );

        if (false && !$settlement->canUserGetPdf()) {
            $this->Flash->error(__('Vyúčtování není možné stáhnout'));
            $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id]);
            return;
        }

        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;

        // add budget data
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list');
        $this->set(compact('settlement', 'attachmentTypes', 'program', 'mergedRequest', 'budget'));

        $this->renderAsPdf(
            sprintf('VyuctovaniDotace_%d_%d.pdf', $settlement->getRequest()->id, date('YmdHis')),
            $forceDownload
        );

        $this->viewBuilder()->setTemplate('/UserRequests/settlement_pdf');
    }

    public function settlementFileDownload(int $settlement_id, int $file_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $file = $settlement->getFileById($file_id, true);

        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }

    public function settlementItemAddModify(int $settlement_id, ?int $item_id = null)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $item = $item_id > 0 ? $settlement->getItemById($item_id, true) : $this->Settlements->SettlementItems->newEntity();
        if (!$item->isNew()) {
            $this->Settlements->SettlementItems->loadInto($item, ['SettlementAttachments']);
        }
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list')->order(['order' => 'ASC']);

        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);
        $notRequireAttachments = isset($program) ? boolval($program->not_require_tax_documents) : false;

        if ($settlement->canUserEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $data = $this->getRequest()->getData();
            $data['settlement_id'] = $settlement->id;
            $removed_attachments = [];
            foreach ($data['settlement_attachments'] as $key => $attachment_data) {
                // set TYPE_OTHERS as default attachment type
                if (!isset($attachment_data['settlement_attachment_type_id']) || empty($attachment_data['settlement_attachment_type_id'])) {
                    $attachment_data['settlement_attachment_type_id'] = SettlementAttachmentType::TYPE_OTHERS;
                }
                if (boolval($attachment_data['select_existing'] ?? false)) {
                    $existingAttachment = $settlement->getAttachmentsById(intval($attachment_data['existing_id']));
                    if ($existingAttachment) {
                        $attachment_data['id'] = $existingAttachment->id;
                        $attachment_data['settlement_attachment_type_id'] = $existingAttachment->settlement_attachment_type_id;
                        $attachment_data['file_id'] = $existingAttachment->file_id;
                        unset($attachment_data['filedata']);
                        unset($attachment_data['existing_id']);
                        unset($attachment_data['select_existing']);
                    } else if (!empty($attachment_data['id'])) {
                        $removed_attachments[] = intval($attachment_data['id']);
                    }
                } else if (empty($attachment_data['existing_id']) && empty($attachment_data['id']) && empty($attachment_data['settlement_attachment_type_id'])) {
                    $attachment_data = null;
                } else {
                    if (empty($attachment_data['filedata']['tmp_name'])) {
                        unset($data['settlement_attachments'][$key]);
                        continue;
                    }

                    $uploadedFile = createUploadedFile($attachment_data['filedata']);
                    $file = $this->Settlements->SettlementAttachments->Files->newEntity();
                    $uploadedMovedFilePath = ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) ? null : $file->fileUploadMove($attachment_data['filedata'], $this->getCurrentUserId());

                    if ($notRequireAttachments && ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1 || !is_string($uploadedMovedFilePath))) {
                        $attachment_data = null;
                    } else if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1 || !is_string($uploadedMovedFilePath)) {
                        if (!empty($attachment_data['id'])) {
                            $removed_attachments[] = $attachment_data['id'];
                        } else {
                            $item->setError(sprintf('settlement_attachments.%d.filedata', $key), __('Nahrání souboru se nezdařilo'));
                            $this->Flash->error(__('Nahrání souboru se nezdařilo'));
                        }
                    } else {
                        $attachment_data = [
                            'settlement_id' => $settlement->id,
                            'settlement_attachment_type_id' => $attachment_data['settlement_attachment_type_id'],
                            'file' => [
                                'user_id' => $this->getCurrentUserId(),
                                'filepath' => $uploadedMovedFilePath,
                                'filesize' => $uploadedFile->getSize(),
                                'original_filename' => $uploadedFile->getClientFilename()
                            ]
                        ];
                    }
                }
                $data['settlement_attachments'][$key] = $attachment_data;
            }

            $item = $this->Settlements->SettlementItems->patchEntity($item, $data, ['associated' => ['SettlementAttachments.Files']]);
            foreach ($item->settlement_attachments as $attachment) {
                $attachment->settlement_id = $settlement->id;
            }

            if ($this->Settlements->SettlementItems->save($item)) {
                if (!empty($removed_attachments)) {
                    $this->Settlements->SettlementItems->SettlementItemsToSettlementAttachments->deleteAll([
                        'settlement_item_id' => $item->id,
                        'settlement_attachment_id IN' => $removed_attachments
                    ]);
                }
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id, '#' => 'fileUpload']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        // add budget data
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $this->set(compact('settlement', 'item', 'attachmentTypes', 'program', 'budget'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], sprintf("%s #%d", __('Vyúčtování žádosti'), $settlement->getRequest()->id) => ['action' => 'settlementDetail', 'settlement_id' => $settlement->id]]);
    }

    public function settlementItemDelete(int $settlement_id, int $item_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $item = $settlement->getItemById($item_id, true);
        if ($settlement->canUserEdit() && $this->Settlements->SettlementItems->delete($item)) {
            $this->Flash->success(__('Úspěšně smazáno'));
        } else {
            $this->Flash->error(__('Nebylo možné smazat položku vyúčtování'));
        }
        return $this->redirect($this->referer());
    }

    public function settlementAddModifyFile(int $settlement_id, ?int $attachment_id = null)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $attachment = $attachment_id > 0 ? $settlement->getAttachmentsById($attachment_id, true) :
            $this->Settlements->SettlementAttachments->newEntity();

        if ($settlement->canUserEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $originalFileToDelete = null;
            $fileWasUploaded = false;
            $attachment = $this->Settlements->SettlementAttachments->patchEntity(
                $attachment,
                $this->getRequest()->getData() + [
                    'settlement_id' => $settlement->id,
                    'is_public' => false
                ]
            );
            $filedata = $this->getRequest()->getData('filedata');
            if (!empty($filedata)) {
                $uploadedFile = createUploadedFile($this->getRequest()->getData('filedata'));
                if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                    if ($uploadedFile->getError() !== UPLOAD_ERR_NO_FILE) {
                        $attachment->setError('filedata', __('Nahrávání souboru se nepodařilo'));
                        $this->Flash->error(__('Nahrávání souboru se nepodařilo'));
                    } else {
                        $attachment->setError('filedata', __('Nebyl nahrán žádný soubor'));
                    }
                } else {
                    $file = $this->Settlements->SettlementAttachments->Files->newEntity();
                    $uploadedMovedFilePath = $file->fileUploadMove($this->getRequest()->getData('filedata'), $this->getCurrentUserId());
                    if (is_string($uploadedMovedFilePath)) {
                        $file->original_filename = $uploadedFile->getClientFilename();
                        $file->filesize = $uploadedFile->getSize();
                        $file->filepath = $uploadedMovedFilePath;
                        $file->user_id = $this->getCurrentUserId();
                        $originalFileToDelete = $attachment->file_id;
                        $attachment->unsetProperty('file_id');
                        $attachment->file = $file;
                        $attachment->setDirty('file');
                        $fileWasUploaded = true;
                    } else {
                        $this->Flash->error(__('Chyba při zpracování nahrávaného souboru'));
                        $attachment->setError('filedata', __('Chyba při zpracování nahrávaného souboru'));
                    }
                }
            }

            if ($this->Settlements->SettlementAttachments->save($attachment)) {
                if ($originalFileToDelete > 0) {
                    $originalFile = $this->Settlements->SettlementAttachments->Files->get($originalFileToDelete);
                    if ($this->Settlements->SettlementAttachments->Files->delete($originalFile)) {
                        $originalFile->deletePhysically();
                    }
                }
                $this->Flash->success($fileWasUploaded ? __('Příloha nahrána úspěšně') : __('Uloženo úspěšně'));
                $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id]);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list')->order(['order' => 'ASC']);
        $uploadFileType = $this->getRequest()->getParam('type');
        if (!empty($uploadFileType)) {
            switch ($uploadFileType) {
                case "report":
                    $attachmentTypes->where([
                        'id IN' => [SettlementAttachmentType::TYPE_FINAL_REPORT_WITH_PUBLICITY, SettlementAttachmentType::TYPE_FINAL_REPORT],
                    ]);
                    break;
                case "publicity":
                    $attachmentTypes->where([
                        'id IN' => [SettlementAttachmentType::TYPE_PUBLICITY_PROOF, SettlementAttachmentType::TYPE_FINAL_REPORT_WITH_PUBLICITY],
                    ]);
                    break;
            }
        }
        $this->set(compact('settlement', 'attachment', 'attachmentTypes'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], sprintf("%s #%d", __('Vyúčtování žádosti'), $settlement->getRequest()->id) => ['action' => 'settlementDetail', 'settlement_id' => $settlement->id]]);
    }

    public function settlementDeleteFile(int $settlement_id, int $attachment_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $attachment = $settlement->getAttachmentsById($attachment_id, true);
        if ($settlement->canUserEdit()) {
            if ($attachment->isUsed($settlement, false)) {
                $this->Flash->error(__('Nelze smazat přílohu, která je použita ve vyúčtování'));
            } elseif ($this->Settlements->SettlementAttachments->delete($attachment)) {
                $this->Flash->success(__('Smazáno úspěšně'));
            } else {
                $this->Flash->error(__('Přílohu není možné smazat'));
            }
            return $this->redirect($this->referer());
        }
        return $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id]);
    }

    public function settlementViewFile(int $settlement_id, int $file_id)
    {
        return $this->settlementDownloadFile($settlement_id,  $file_id, false);
    }

    public function settlementDownloadFile(int $settlement_id, int $file_id, bool $download = true)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        $file = $settlement->getFileById($file_id, true);

        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists() && $download) {
            return $this->getResponse()->withFile(
                $target,
                ['name' => urlencode($file->original_filename), 'download' => true]
            );
        } elseif ($_file->exists()) {
            return $this->getResponse()
                ->withHeader('Content-type', mime_content_type($target))
                ->withHeader('Content-disposition', 'inline; filename="' . urlencode($file->original_filename) . '"')
                ->withFile(
                    $target,
                    ['name' => urlencode($file->original_filename), 'download' => false]
                );
        }

        return $this->getResponse()->withStatus(404);
    }

    public function notifications(int $request_id)
    {
        // Skip auth check for privileged users
        if ($this->getCurrentUser()->isManager() || $this->getCurrentUser()->userHasRoleInAnyProgram($this->getCurrentUser()->id)) {
            $request = $this->getRequestById($request_id, true)->loadNotifications();
        } else {
            $request = $this->getRequestById($request_id)->loadNotifications();
        }
        if (empty($request->request_notifications)) {
            $this->redirect(['action' => 'notificationCreate', 'id' => $request_id]);
            return;
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], h($request->name) => false]);
    }

    public function locate() {}


    /**
     * locateByID - AJAX endpoint - translates request id to the user (the user which owns the request) id and email
     *
     * @param  int $request_id
     * @return void
     */

    public function locateByID($request_id)
    {
        $res = ['status' => "FAIL"];
        if (is_numeric($request_id)) {
            $result = $this->Requests->find(
                'all',
                [
                    'conditions' => [
                        'Requests.id' => $request_id,
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                    'contain' => RequestsTable::CONTAIN_EXTENDED,
                ]
            )->first();
            if ($result != null) {
                $res['status'] = 'OK';
                $res['id'] = $result->user_id;
                $users_table = new UsersTable;
                $user = $users_table->find(
                    'all',
                    [
                        'conditions' => [
                            'id' => $result->user_id,
                        ]
                    ]
                )->first();
                if ($user != null) {
                    $res['email'] = $user->email;
                }
            }
        }
        return $this->response->withType("application/json")->withStringBody(json_encode($res));
    }


    public function notificationCreate(int $request_id)
    {
        $request = $this->getRequestById($request_id)->loadNotifications();
        if (!RequestState::canUserCreateNotification($request->request_state_id)) {
            $this->redirect(['action' => 'index']);
            $this->Flash->error(__('K této žádosti nyní nemůžete provést ohlášení'));
            return;
        }
        $notification = $this->RequestNotifications->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $modifiedData = [
                'request_id' => $request->id,
            ] + $this->getRequest()->getData();
            foreach ($modifiedData['files'] ?? [] as $key => $filedata) {
                $filedata = $filedata['filedata'] ?? [];
                if (!empty($filedata)) {
                    $uploaded = createUploadedFile($filedata);
                    if ($uploaded->getError() === UPLOAD_ERR_OK) {
                        $modifiedData['files'][$key] = [
                            'user_id' => $this->getCurrentUserId(),
                            'filesize' => $uploaded->getSize(),
                            'original_filename' => h($uploaded->getClientFilename()),
                            'filepath' => $filedata['tmp_name']
                        ];
                    } else {
                        unset($modifiedData['files'][$key]);
                    }
                }
            }
            $notification = $this->RequestNotifications->newEntity($modifiedData);

            $isSuccess = false;
            if ($this->RequestNotifications->save($notification)) {
                foreach ($notification->files as $file) {
                    $file->set('filepath', $file->fileUploadMove([
                        'error' => UPLOAD_ERR_OK,
                        'size' => $file->filesize,
                        'tmp_name' => $file->filepath
                    ], $this->getCurrentUserId()));
                    $notification->setDirty('files');
                }
                if ($this->RequestNotifications->save($notification)) {
                    $isSuccess = true;
                }
            }
            if ($isSuccess) {
                $this->sendNewNotificationAlerts($request, $notification);
                $this->Flash->success(__('Ohlášení provedeno úspěšně'));
                $this->redirect(['action' => 'notifications', 'id' => $request->id]);
                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        // request can have multiple notifications irrelevant to payments currently
        $this->set(compact('request', 'notification'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], sprintf("%s #%d", __('Ohlášení k žádosti'), $request->id) => ['action' => 'notifications', 'id' => $request->id]]);
    }

    /**
     * When a new "notification" is submitted, send an email alert/notice to all in team "Hodnotitelé" (#402)
     * @param mixed $request
     * @param mixed $notification
     *
     * @return [type]
     */

    public function sendNewNotificationAlerts($request, $notification)
    {
        // Only send if allowed globally in the organisation settings
        if (OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS_ALERTS, false)) {
            $program_id = $request->get('program_id');
            // Only send to those who have role comments_programs in this request's program
            $toUsers = $this->getUsersByRoleAndProgram($program_id, 'comments_programs');
            if (!empty($toUsers)) {
                $recipients = [];
                foreach ($toUsers as $user) {
                    $identityForm = new IdentityForm();
                    $identityForm->loadIdentities($this->Identities, $this->getCurrentUser());
                    $notifications_disabled = boolval($identityForm->getData(Identity::NO_NOTIFICATIONS));
                    if (!$notifications_disabled) {
                        // Only send if the user has not disabled notifications in his settings
                        $recipients[] = $user->email;
                    }
                }

                $url = Router::url([
                    'controller' => 'UserRequests',
                    'action' => 'notifications',
                    'id' => $request->id
                ], true);

                $notification->request = $request; // for the template
                $this->sendMailSafe('User', 'notificationAlert', [$notification, $recipients, $request, $url]);
            }
            return true;
        }
    }

    public function getUsersByRoleAndProgram($program_id, $role_name)
    {

        $role_fields = TeamsManagerController::getTeamsRoles();
        $role = str_replace('_programs', '', $role_name);
        $program_role_id = array_search($role, $role_fields);

        $locator = new TableLocator();
        $usersTable = $locator->get('Users');

        $query = $usersTable->find()
            ->select(['Users.email'])
            ->distinct()
            ->innerJoinWith('Teams.TeamsRolesInPrograms', function ($q) use ($program_id, $program_role_id) {
                return $q->where([
                    'TeamsRolesInPrograms.program_id' => $program_id,
                    'TeamsRolesInPrograms.program_role_id' => $program_role_id
                ]);
            });


        return $query->toList();
    }

    public function notificationsIndex()
    {
        /*$appeals = $this->Appeals->find(
                'list',
                [
                    'conditions' => [
                        'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                ]
            )->order(
                ['`Appeals`.`open_from`' => 'DESC', '`Appeals`.`name`' => 'ASC']
            )->toArray();*/

        $result = $this->RequestNotificationTypes->find(
            'all',
            [
                'conditions' => [],
            ]
        )->order(
            ['id' => 'ASC']
        )->toArray();
        $notificationTypes = [];
        foreach ($result as $r) {
            $notificationTypes[$r->id] = $r->type_name;
        }


        // We always load all to get the program ids and filter out non matching later
        $requestNotifications = $this->RequestNotifications->find('all')
            ->contain(['Requests']) // Join with the requests table
            ->where(['Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()])
            ->order(
                ['`RequestNotifications`.`id`' => 'DESC']
            )
            ->toArray();

        if ($this->getRequest()->is(['get'])) {
            $request_program_id = $this->getRequest()->getQuery('program_id');
        } else {
            $request_program_id = NULL;
        }

        //Prepare program ids
        $program_ids = [];
        $programs = [];
        foreach ($requestNotifications as $notification) {
            $program_ids[$notification->request->program_id] = true;
        }
        $program_ids = array_keys($program_ids);
        if (!empty($program_ids)) {
            $result = $this->Programs->find()
                ->where(['id IN' => $program_ids])
                ->order(['id' => 'DESC'])
                ->all()->toArray();
            foreach ($result as $program) {
                $programs[$program->id] = $program->name;
            }
        }

        // Filter out non matching requests
        if (isset($request_program_id) && trim($request_program_id)) {
            foreach ($requestNotifications as $key => $notification) {
                if (

                    $notification->request->program_id != $request_program_id
                ) {
                    unset($requestNotifications[$key]);
                };
            }
        } else {
            $request_program_id = NULL;
        }
        //$this->setRequest($this->getRequest()->withData('program_id', []));

        $this->set(compact('requestNotifications', 'programs', 'notificationTypes', 'request_program_id'));
        $this->set('crumbs', [__('Seznam ohlášení') => 'notificationsIndex']);
    }


    public function settlementCreate(int $request_id, ?int $payment_id = null)
    {
        $request = $this->getRequestById($request_id);

        // request can have one or more payments
        // settled can be only non-refund payments
        // if there are multiple choices, give user to decide

        $hasPayments = !empty($request->getPayments());
        $hasSinglePayment = count($request->getPayments()) === 1;
        $hasUnsettledPayments = $request->hasUnsettledPayments();

        if (!$hasPayments) {
            $this->Flash->error(__('Pro tuto dotaci neexistuje platba, kterou byste mohli vyúčtovat'));
            $this->redirect(['_name' => 'user_requests']);
            return;
        }

        if (intval($payment_id) > 0) {
            $payment = $request->getPaymentById($payment_id);
            if (!empty($payment)) {
                if (!empty($payment->settlement_id)) {
                    $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $payment->settlement_id]);
                    return;
                }
                $newSettlement = $this->Settlements->newEntity([
                    'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ]);
                $newSettlement->setCurrentStatus(SettlementState::STATE_NEW, $this->getCurrentUserId());
                $newSettlement->requests_to_settlements = [
                    $this->Settlements->RequestsToSettlements->newEntity([
                        'request_id' => $request->id,
                    ])
                ];
                if ($this->Settlements->save($newSettlement)) {
                    $payment->settlement_id = $newSettlement->id;
                    $payment->setDirty('settlement_id');
                    $request->setDirty('payments');
                    $this->Requests->save($request);
                    $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $newSettlement->id]);
                } else {
                    $this->Flash->error(__('Vyúčtování nebylo možné vytvořit'));
                    $this->redirect(['_name' => 'user_requests']);
                }
            }
        }

        if ($hasSinglePayment && !$hasUnsettledPayments) {
            $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $request->payments[0]->settlement_id]);
            return;
        }

        if ($hasSinglePayment && $hasUnsettledPayments) {
            // has no payment or has single unsettled payment, create automatically
            if (empty($request->payments[0]->settlement_id)) {
                $newSettlement = $this->Settlements->newEntity([
                    'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ]);
                $newSettlement->setCurrentStatus(SettlementState::STATE_NEW, $this->getCurrentUserId());
                $newSettlement->requests_to_settlements = [
                    $this->Settlements->RequestsToSettlements->newEntity([
                        'request_id' => $request->id,
                    ])
                ];
                if ($this->Settlements->save($newSettlement)) {
                    $request->payments[0]->settlement_id = $newSettlement->id;
                    $request->setDirty('payments');
                    $request->payments[0]->setDirty('settlement_id');
                    $this->Requests->save($request);
                    $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $newSettlement->id]);
                } else {
                    $this->Flash->error(__('Vyúčtování nebylo možné vytvořit'));
                    $this->redirect(['_name' => 'user_requests']);
                }
                return;
            } else {
                $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $request->payments[0]->settlement_id]);
                return;
            }
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', h($request->name) => false]);
    }

    public function settlementSubmitDirectly(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        if (!$settlement->canUserSubmitSettlement($this->getCurrentUser())) {
            $this->Flash->error(__('Toto vyúčtování není možné odeslat'));
            $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement_id]);

            return;
        }

        $returnedSettlement = $settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES;
        $settlement->setCurrentStatus(SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, $this->getCurrentUserId());

        if ($this->Settlements->save($settlement)) {
            $this->Flash->success(__('Vyúčtování bylo úspěšně odesláno'));
        } else {
            Log::error('settlementSubmitDirectly saving error, ' . json_encode($settlement->getErrors()));
        }

        $request = $settlement->getRequest();
        if (!$settlement->hasErrors() && $request instanceof Request) {
            $request->setCurrentStatus(RequestState::STATE_SETTLEMENT_SUBMITTED);
            $this->sendMailToOrganization($request, $request->request_state_id, true, $returnedSettlement);

            if (!$this->Requests->save($request)) {
                Log::error('After settlement submit could not set new state to request');
                Log::error(json_encode($request->getErrors()));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function settlementSubmitDatabox(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));
        if (!$settlement->canUserSubmitSettlement($this->getCurrentUser())) {
            $this->Flash->error(__('Toto vyúčtování není možné odeslat'));
            $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement_id]);

            return;
        }

        list($cert, $key) = $this->Isds->getCertificateAndPrivateKey();

        if (empty($cert) || empty($key)) {
            $this->Flash->error(__('Datová Schránka dotačního portálu není nastavena správně'));
            $this->redirect(['action' => 'index']);

            return;
        }

        $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['action' => 'settlementSubmitDatabox', 'controller' => 'UserRequests', 'settlement_id' => $settlement_id]);
        $timeLimitedId = $this->getRequest()->getSession()->consume(IsdsController::ISDS_TIME_LIMITED_ID);
        if (empty($timeLimitedId)) {
            $this->redirect(['_name' => 'isds_auth']);

            return;
        }

        // we have timeLimitedId, so we can first check the target box against identity
        // TODO: verify the session dbID against dbID in original request
        $_session_dbID = trim($this->getRequest()->getSession()->read(IsdsController::ISDS_VERIFIED_DS_ID));
        // this step persists verified identity attributes to db, so it can be loaded before generating PDF
        $this->Isds->storeVerifiedAttributes($this);

        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);

        // put concept into databox and redirect to it
        $this->set('settlement', $settlement);
        $this->set('attachmentTypes', $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list'));
        $this->set('program', $program);

        Configure::write(
            'CakePdf',
            [
                //'engine' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'App.WeasyPrint',
                'orientation' => 'portrait',
                'download' => true,
                'margin' => [
                    'bottom' => 6,
                    'left' => 3,
                    'right' => 3,
                    'top' => 6,
                ],
            ]
        );
        $this->viewBuilder()->setOptions(
            [
                'pdfConfig' => [
                    'disable-smart-shrinking' => 'disable-smart-shrinking',
                ],
            ]
        );

        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->setTemplate('/UserRequests/settlement_pdf');
        $pdfData = base64_encode($this->render()->getBody()->__toString());

        $url = $this->Isds->getSetConceptRequestUrl();
        $request_id = $settlement->getRequest()->id;
        $filename = ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT ? 'vyuctovani-dotace-' : 'vraceni-dotace-') . $request_id;
        $message_title = ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) ? __('Vyúčtování dotace č.') : __('Vrácení dotace č.');
        $xml = $this->Isds->getSetConceptRequest(OrganizationSetting::getSetting(OrganizationSetting::DS_ID), $message_title . $request_id, $filename, $pdfData);

        $response = null;
        $parsed = null;
        $conceptId = null;
        try {
            $response = $this->Isds->execSoapAction($url, $cert, $key, $xml, ['ExtWS', $timeLimitedId]);
            $parsed = $this->Isds->extractSetConceptResponse($response);

            $conceptId = $parsed[0];
        } catch (Throwable $t) {
            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
        }

        if (!empty($conceptId)) {
            $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['controller' => 'UserRequests', 'action' => 'settlementSubmitDataboxResult', 'settlement_id' => $settlement_id]);
            $this->redirect($this->Isds->getKonceptViewUrl($conceptId));

            return;
        }

        $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement->id]);
    }

    public function settlementSubmitDataboxResult(int $settlement_id)
    {
        $settlement = $this->Settlements->getWithPermissionCheck($settlement_id, $this->getCurrentUser(), OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true));

        $result_dm_id = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_DM_ID);
        $result_message = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_MESSAGE);
        $result_code = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_CODE);

        if (empty($result_dm_id)) {
            $this->Flash->error(sprintf("%s %s (%s)", __('Vyúčtování nebylo odesláno z důvodu:'), $result_message, $result_code));
            return $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $settlement_id]);
        }

        $returnedSettlement = $settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES;
        $settlement->setCurrentStatus(SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, $this->getCurrentUserId());

        if ($this->Settlements->save($settlement)) {
            $this->Flash->success(__('Vyúčtování bylo úspěšně odesláno pomocí datové schránky'));
        } else {
            Log::error('settlementSubmitDataboxResult saving error, ' . json_encode($settlement->getErrors()));
        }

        $request = $settlement->getRequest();
        if (!$settlement->hasErrors() && $request instanceof Request) {
            $request->setCurrentStatus(RequestState::STATE_SETTLEMENT_SUBMITTED);
            $this->sendMailToOrganization($request, $request->request_state_id, true, $returnedSettlement);

            if (!$this->Requests->save($request)) {
                Log::error('After settlement submit could not set new state to request');
                Log::error(json_encode($request->getErrors()));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function dswZadost(int $id = 0, bool $render = true)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id, $this->getCurrentUser()->getStareUctyIds());
        $this->set(compact('zadost'));
        if ($render === true) {
            $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index']]);
            $this->render('OldDsw.OldDsw/zadost');
        }
    }

    public function dswVyuctovani(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true) || !isset($this->OldDsw) || $id < 1) {
            return $this->redirect(['action' => 'index']);
        }

        $zadost = $this->OldDsw->getZadost($id, $this->getCurrentUser()->getStareUctyIds());
        /** @var SettlementsToOlddswRequest $existingSettlement */
        $existingSettlement = $this->Settlements->SettlementsToOlddswRequests->find('all', [
            'conditions' => [
                'zadost_id' => $zadost->id,
            ],
        ])->first();

        if (empty($existingSettlement)) {
            $existingSettlement = $this->Settlements->newEntity([
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]);
            $existingSettlement->setCurrentStatus(SettlementState::STATE_NEW, $this->getCurrentUserId());
            $existingSettlement->settlements_to_olddsw_requests = [
                $this->Settlements->SettlementsToOlddswRequests->newEntity([
                    'zadost_id' => $zadost->id
                ]),
            ];

            if (!$this->Settlements->save($existingSettlement)) {
                $this->Flash->error(__('Vyúčtování nebylo možné vytvořit'));
                Log::error('dswVyuctovani::' . $zadost->id . '::' . json_encode($existingSettlement->getErrors()));
                return $this->redirect(['_name' => 'user_requests']);
            }
            return $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $existingSettlement->id]);
        }

        return $this->redirect(['action' => 'settlementDetail', 'settlement_id' => $existingSettlement->settlement_id]);
    }

    public function dswOhlasit(int $id = 0)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW, true) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $this->dswZadost($id, false);
        $zadost = isset($this->viewVars['zadost']) ? $this->viewVars['zadost'] : null;
        if (!($zadost instanceof Zadost)) {
            $this->Flash->error(__('Není možné provést ohlášení k žádosti, které nebylo vyhověno'));
            $this->redirect(['action' => 'dswZadost', $id]);

            return;
        }
        $ohlaseni = $this->OldDsw->Zadosti->OhlasovaciFormular->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $ohlaseni = $this->OldDsw->Zadosti->OhlasovaciFormular->patchEntity($ohlaseni, $this->getRequest()->getData());
            $ohlaseni->zadost_id = $zadost->id;
            $ohlaseni->added_by_uzivatel_id = $zadost->pridano_kym;
            $ohlaseni->added_when = FrozenTime::now(null);
            if ($this->OldDsw->Zadosti->OhlasovaciFormular->save($ohlaseni)) {
                $this->Flash->success(__('Ohlášení bylo úspěšně provedeno'));
                $this->redirect(['action' => 'index']);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], h($zadost->nazev) => ['action' => 'dswZadost', $id]]);
        $this->set(compact('ohlaseni'));
    }

    public function historyFromArchive(Request $request)
    {
        // hide HISTORIES records unless enabled for the organization
        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES)) {
            return [];
        }

        // get IČO and VAT from identities data and prepare query for searching in the HistoryIdentities
        $histories = array();
        $userIdentities = array_fill_keys([
            'HistoryIdentities.ico IN',
            'HistoryIdentities.dic IN'
        ], []);

        foreach ($request->identities as $k => $v) {
            if (empty(trim($v['value']))) {
                continue;
            }
            switch ($v['name']) {
                case Identity::PO_BUSINESS_ID:
                    $userIdentities['HistoryIdentities.ico IN'][] = $v['value'];
                    break;

                case Identity::FO_VAT_ID:
                case Identity::PO_VAT_ID:
                    $userIdentities['HistoryIdentities.dic IN'][] = $v['value'];
                    break;
            }
        }

        if (!empty(array_filter($userIdentities))) {
            // get list of archived history
            $archive = $this->HistoryIdentities->find('all', [
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'OR' => array_filter($userIdentities)
                ],
                'contain' => [
                    'Histories',
                ],
            ])->toArray();

            $orgRange = (int)OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true);
            $yearsRange = range((int)date('Y'), (int)date('Y') - max(0, $orgRange));
            // old items filtered by year
            if (isset($archive[0]) && isset($archive[0]->histories)) {
                foreach ($yearsRange as $year) {
                    foreach ($archive[0]->histories as $v) {
                        if ((int)$v['year'] === $year) {
                            $histories[] = (object)
                            [
                                'id' => null,
                                'amount_czk' => $v['czk_amount'],
                                'fiscal_year' => $v['year'],
                                'public_income_source_id' => null,
                                'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
                                'project_name' => $v['project_name']
                            ];
                        }
                    }
                }
            }
        }

        return $histories;
    }

    public function requestChange(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if (!$request->canUserIssueRequestChange()) {
            $this->Flash->error(__('K této žádosti nemůžete podat návrh na změnu'));
            $this->redirect(['action' => 'index']);
            return;
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests']);
    }

    public function requestDetail(int $request_id)
    {

        $request = $this->getRequestById($request_id);

        // Fill shared forms #339 - new shared forms need to be filled before checking if they are filled completely
        $request->fillFromPeers();

        if (!$request->canUserEditRequest()) {
            $this->Flash->error(__('Tuto žádost nemůžete upravovat'));
            $this->redirect(['action' => 'index']);

            return;
        }
        $public_income_history = $this->PublicIncomeHistories->find(
            'list',
            [
                'conditions' => [
                    'PublicIncomeHistories.user_id' => $request->user_id,
                    'PublicIncomeHistories.fiscal_year >=' =>
                    intval(date('Y')) - OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true),
                ],
            ]
        )->toArray();

        $isReadyToSubmit = false;
        // validate identity
        if ($request->user_identity_version !== 0 && $this->getCurrentUser()->isIdentityCompleted($request->user_identity_version)) {
            // validate budget is
            if ($request->validateBudgetRequirements() === RequestBudget::BUDGET_OK) {
                if ($request->appeal->checkRequestLimit($request->request_budget->requested_amount, $request->program_id)) {
                    $formsFilled = true;
                    foreach ($request->getForms() as $form) {
                        $formsFilled = $formsFilled && $request->isFormCompleted($form->id);
                    }
                    if ($formsFilled) {
                        $isReadyToSubmit = true;
                    }
                }
            }
        }
        $newState = $request->request_state_id;
        if (!$isReadyToSubmit) {
            $newState = RequestState::STATE_NEW;
        } elseif ($request->request_state_id === RequestState::STATE_NEW) {
            $newState = RequestState::STATE_READY_TO_SUBMIT;
        }
        if ($newState !== $request->request_state_id) {
            $this->sendMailToOrganization($request, $newState);

            // persist only if state changed
            $request->setCurrentStatus($newState, $this->getCurrentUserId());
            $this->Requests->save($request);
        }

        // join historical records
        $public_income_history = array_merge(
            $public_income_history,
            $this->historyFromArchive($request)
        );

        $this->set(compact('request', 'public_income_history'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests']);
    }

    public function requestSubmitDataboxResult(int $request_id)
    {
        $request = $this->getRequestById($request_id);

        $result_dm_id = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_DM_ID);
        $result_message = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_MESSAGE);
        $result_code = $this->getRequest()->getSession()->read(IsdsController::ISDS_CONCEPT_STATUS_CODE);

        if (empty($result_dm_id)) {
            $this->Flash->error(sprintf("%s %s (%s)", __('Žádost nebyla podána z důvodu:'), $result_message, $result_code));
            $this->redirect(['action' => 'requestDetail', 'id' => $request_id]);

            return;
        }

        $request->setCurrentStatus(RequestState::STATE_SUBMITTED, $this->getCurrentUserId());
        if ($this->Requests->save($request)) {
            $this->Flash->success(__('Žádost byla úspěšně odeslána pomocí datové schránky'));

            $this->sendMailToOrganization($request, RequestState::STATE_SUBMITTED);
        }
        $this->redirect(['action' => 'index']);
    }

    public function requestSubmitDatabox(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if ($request->request_state_id !== RequestState::STATE_READY_TO_SUBMIT || !$request->canUserSubmitRequest()) {
            $this->Flash->error(__('Tuto žádost není možné odeslat'));
            $this->redirect(['action' => 'requestDetail', 'id' => $request_id]);

            return;
        }

        list($cert, $key) = $this->Isds->getCertificateAndPrivateKey();

        if (empty($cert) || empty($key)) {
            $this->Flash->error(__('Datová Schránka dotačního portálu není nastavena správně'));
            $this->redirect(['action' => 'index']);

            return;
        }

        $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['action' => 'requestSubmitDatabox', 'controller' => 'UserRequests', 'id' => $request_id]);
        $timeLimitedId = $this->getRequest()->getSession()->consume(IsdsController::ISDS_TIME_LIMITED_ID);
        if (empty($timeLimitedId)) {
            $this->redirect(['_name' => 'isds_auth']);

            return;
        }

        // we have timeLimitedId, so we can first check the target box against identity
        $_session_dbID = trim($this->getRequest()->getSession()->read(IsdsController::ISDS_VERIFIED_DS_ID));
        if (!empty($_session_dbID)) {
            $request->loadIdentities();
            $_request_dbID = null;
            $_request_verified_dbID = null;
            foreach ($request->identities ?? [] as $identity) {
                if ($identity->name === Identity::CONTACT_DATABOX) {
                    $_request_dbID = trim($identity->value);
                } elseif ($identity->name === Identity::ISDS_VERIFIED_DATABOX_ID) {
                    $_request_verified_dbID = trim($identity->value);
                }
            }
            if (!in_array($_session_dbID, [$_request_verified_dbID, $_request_dbID], true) && (!empty($_request_dbID) || !empty($_request_verified_dbID))) {
                $this->Flash->error(__('Datová schránka, kterou jste se pokusili žádost odeslat, nesouhlasí s identitou žadatele'));
                $this->Flash->error(sprintf(__('Odesíláte z Datové Schránky ID "%s" ale v identitě je uvedeno "%s"'), $_session_dbID, $_request_verified_dbID ?? $_request_dbID));

                $this->redirect(['action' => 'index']);

                return;
            }
            // this step persists verified identity attributes to db, so it can be loaded before generating PDF
            $this->Isds->storeVerifiedAttributes($this);
        }

        // put concept into databox and redirect to it
        $pdfData = null;
        $request = $this->getRequestById($request_id);
        $program = $this->Programs->get($request->program_id);

        $this->set('request', $request);
        $this->set('program', $program);

        Configure::write(
            'CakePdf',
            [
                //'engine' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'App.WeasyPrint',
                'orientation' => 'portrait',
                'download' => true,
                'margin' => [
                    'bottom' => 6,
                    'left' => 3,
                    'right' => 3,
                    'top' => 6,
                ],
            ]
        );
        $this->viewBuilder()->setOptions(
            [
                'pdfConfig' => [
                    'disable-smart-shrinking' => 'disable-smart-shrinking',
                ],
            ]
        );
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->setTemplate('/UserRequests/get_pdf');
        $pdfData = base64_encode($this->render()->getBody()->__toString());

        $url = $this->Isds->getSetConceptRequestUrl();
        $filename = 'zadost-' . $request_id;
        $xml = $this->Isds->getSetConceptRequest(OrganizationSetting::getSetting(OrganizationSetting::DS_ID), __('Žádost o poskytnutí dotace č.') . $request_id, $filename, $pdfData);

        $conceptId = null;
        try {
            $response = $this->Isds->execSoapAction($url, $cert, $key, $xml, ['ExtWS', $timeLimitedId]);
            $parsed = $this->Isds->extractSetConceptResponse($response);

            $conceptId = $parsed[0];
        } catch (Throwable $t) {
            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
        }

        if (!empty($conceptId)) {
            $this->getRequest()->getSession()->write(IsdsController::ISDS_AFTER_AUTH_HANDLER, ['controller' => 'UserRequests', 'action' => 'requestSubmitDataboxResult', 'id' => $request_id]);
            $this->redirect($this->Isds->getKonceptViewUrl($conceptId));

            return;
        }

        $this->redirect(['action' => 'requestDetail', $request_id]);
    }

    public function requestDelete(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if ($request->user_id !== $this->getCurrentUser()->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Žádost může být smazána pouze vlastníkem'));
        } elseif (RequestState::canBeDeleted($request->request_state_id) && $this->Requests->delete($request)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Žádost nebylo možné smazat'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function budgetToNewRequestBudgetChange($budget)
    {
        $chr_fields = [];
        foreach ($budget->getVisible() as $key) {
            if ($key == 'id') continue;
            $chr_fields[$key] = $budget->{$key};
        };
        $change_request = $this->Requests->RequestBudgetsChanges->newEntity($chr_fields);
        $change_request->created = Time::now(null);
        $change_request->modified = $change_request->created;
        return $change_request;
    }

    public function requestBudgetChange(int $request_id)
    {

        $request = $this->getRequestById($request_id);

        $appealsToPrograms = new AppealsToProgramsTable();
        $request->appeal->appeals_to_programs = $appealsToPrograms->find(
            'all',
            [
                'conditions' => [
                    'AppealsToPrograms.appeal_id' => $request->appeal->id
                ]
            ]
        )->toArray();

        if (!$request->request_budget) {
            throw new Exception('Requested budget change on an empty budget !');
        } else {
            $budget = $request->request_budget;
        }

        $save_original = empty($request->request_budgets_changes);

        $design_id = $request->program->project_budget_design_id;

        // Usti wants to change budget design is for V11 and V12 see issue #238
        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V11) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE;
        }

        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE;
        }

        $budgetHandler = ProjectBudgetFactory::getBudgetHandler($design_id);

        if ($request->canUserIssueRequestChange() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            if (!$save_original) {
                foreach ($request->request_budgets_changes as $past_change_request) {
                    if ($past_change_request->status == RequestBudgetChange::CHANGE_PENDING) {
                        throw new Exception('Requested another budget change but there are pending change from the past !');
                    }
                }
            }
            $original_budget = $this->budgetToNewRequestBudgetChange($budget);
            $original_budget->status = RequestBudgetChange::CHANGE_ORIGINAL;
            $budget = $budgetHandler->modifyBudget($request, $budget, $this->getRequest()->getData());
            if (!$request->appeal->checkRequestLimit($budget->requested_amount, $request->program_id)) {
                $budget->setError('requested_amount', sprintf("%s %s", __('Žádáte o částku vyšší než je možné, maximum v tomto programu je'), Number::currency($request->appeal->getMaxRequestBudget($request->program_id), 'CZK')));
            }
            if (!$request->appeal->checkMinRequestLimit($budget->requested_amount, $request->program_id)) {
                $budget->setError('requested_amount', sprintf("%s %s", __('Žádáte o částku nižší než je možné, minimum v tomto programu je'), Number::currency($request->appeal->getMinRequestBudget($request->program_id), 'CZK')));
            }
            $total = $budget->total_costs ?? false;
            $percentage = 0;
            if ($total) {
                $percentage = ceil($budget->requested_amount / $total * 100);
            }

            if (!$request->appeal->checkMaxSupport($percentage, $request->program_id)) {
                $budget->setError('percentage', sprintf("%s %s", __('Žádáte o podporu vyšší než je možné, maximum poměru nákladů a dotace je v tomto programu '), Number::toPercentage($request->appeal->getMaxSupportPercentage($request->program_id))));
            }
            $change_request = $this->budgetToNewRequestBudgetChange($budget);
            $change_request->status = RequestBudgetChange::CHANGE_PENDING;
            if ($save_original) $this->Requests->RequestBudgetsChanges->save($original_budget);
            if ($this->Requests->RequestBudgetsChanges->save($change_request)) {
                $changeRequestController = new ChangeRequestController();
                $text = __("K žádosti %name (%id) byl zadán nový požadavek na změnu v rozpočtu.<br> Všechny požadavky na změny najdete v sekci Mé týmy -> Žádosti o změnu<br>");
                $changeRequestController->changeRequestNotifyOrgByEmail($text, $request);
                $this->Flash->success(__('Žádost o změnu rozpočtu byla úspěšně odeslána.'));
                $this->redirect(['action' => 'requestChange', $request->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }
        $mode = 'track_changes';
        $this->set(compact('request', 'budget', 'design_id', 'mode'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestBudgetChange', $request_id], 'Žádost o změnu' => '#',]);
        return $this->render($budgetHandler->getTemplate());
    }

    public function requestBudget(int $request_id)
    {
        $request = $this->getRequestById($request_id);

        $appealsToPrograms = new AppealsToProgramsTable();
        $request->appeal->appeals_to_programs = $appealsToPrograms->find(
            'all',
            [
                'conditions' => [
                    'AppealsToPrograms.appeal_id' => $request->appeal->id
                ]
            ]
        )->toArray();

        $budget = $request->request_budget ?? $this->Requests->RequestBudgets->newEntity(
            [
                'request_id' => $request_id,
            ]
        );

        $design_id = $request->program->project_budget_design_id;
        $budgetHandler = ProjectBudgetFactory::getBudgetHandler($request->program->project_budget_design_id);

        if (RequestState::canUserEditRequest($request->request_state_id) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $budget = $budgetHandler->modifyBudget($request, $budget, $this->getRequest()->getData());
            if (!$request->appeal->checkRequestLimit($budget->requested_amount, $request->program_id)) {
                $budget->setError('requested_amount', sprintf("%s %s", __('Žádáte o částku vyšší než je možné, maximum v tomto programu je'), Number::currency($request->appeal->getMaxRequestBudget($request->program_id), 'CZK')));
            }
            if (!$request->appeal->checkMinRequestLimit($budget->requested_amount, $request->program_id)) {
                $budget->setError('requested_amount', sprintf("%s %s", __('Žádáte o částku nižší než je možné, minimum v tomto programu je'), Number::currency($request->appeal->getMinRequestBudget($request->program_id), 'CZK')));
            }

            $total = $budget->total_costs ?? false;
            $percentage = 0;
            if ($total) {
                $percentage = ceil($budget->requested_amount / $total * 100);
            }

            if (!$request->appeal->checkMaxSupport($percentage, $request->program_id)) {
                $budget->setError('percentage', sprintf("%s %s", __('Žádáte o podporu vyšší než je možné, maximum poměru nákladů a dotace je v tomto programu '), Number::toPercentage($request->appeal->getMaxSupportPercentage($request->program_id))));
            }

            if ($this->Requests->RequestBudgets->save($budget)) {
                $budgetHandler->afterSuccessfullSave($budget, $this);
                $this->Flash->success(__('Rozpočet uložen úspěšně'));
                $this->redirect(['action' => 'requestDetail', $request->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('request', 'budget', 'design_id'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
        return $this->render($budgetHandler->getTemplate());
    }

    public function addModify(?int $id = null)
    {
        $request = $id > 1 ? $this->Requests->get($id, [
            'conditions' => [
                'Requests.user_id' => $this->getCurrentUser()->id,
            ],
            'contain' => ['RequestBudgets', 'Programs'],
        ]) : $this->Requests->newEntity([
            'request_state_id' => RequestState::STATE_NEW,
        ], ['associated' => ['RequestBudgets']]);

        $appeals_conditions = [
            'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Appeals.open_to >=' => date('Y-m-d'),
        ];
        if (!$this->getCurrentUser()->isGrantsManager()) {
            $appeals_conditions['Appeals.is_active'] = true;
        }

        $appeals = $this->Requests->Appeals->find(
            'all',
            [
                'conditions' => $appeals_conditions,
                'contain' => [
                    'Programs',
                ],
            ]
        )->toArray();

        /** @var Appeal[] $appeals */
        if (empty($appeals) && $request->isNew()) {
            $this->Flash->error(__('Aktuálně není žádná otevřená výzva, ve které byste mohli žádat o podporu'));
            $this->redirect(['action' => 'index']);
            return;
        }

        $appeals_programs = $this->getAppealsToPrograms();
        $program_descriptions = $this->_program_descriptions;

        if (in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT], true) && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $request_program_id = $this->getRequest()->getData('program_id');
            $appeal_id = $this->getRequest()->getData('appeal_id');

            if (!empty($request_program_id) && in_array($request_program_id, $this->_allowed_programs)) {
                $request = $this->Requests->patchEntity(
                    $request,
                    [
                        'program_id' => $request_program_id,
                        'name' => $this->getRequest()->getData('name'),
                        'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'user_id' => $this->Auth->user('id'),
                        'appeal_id' => !empty($appeal_id) ? (int)$appeal_id : ($this->_program_id_to_appeal_id[$request_program_id] ?? null),
                        'merged_request' => $this->getRequest()->getData('merged_request') ?? null,
                        'user_identity_version' => $this->getCurrentUser()->getLastIdentityVersion(false),
                    ]
                );
                if (!($request->request_budget instanceof RequestBudget)) {
                    $request->request_budget = $this->Requests->RequestBudgets->newEntity();
                }
                if (!empty($request->program_id)) {
                    $request->program = $this->Requests->Programs->get($request->program_id);
                }
                if ($request->program instanceof Program && ProjectBudgetFactory::getBudgetHandler($request->program->project_budget_design_id)->canUserEditRequestedAmount()) {
                    $this->Requests->RequestBudgets->patchEntity($request->request_budget, ['requested_amount' => floatval($this->getRequest()->getData('request_budget.requested_amount'))]);
                    $request->setDirty('request_budget', $request->request_budget->isDirty());
                }

                if ($this->Requests->save($request, ['associated' => ['RequestBudgets']])) {
                    // Prefill shared forms with values form already existing peer requests - TODO: only need to run when the request is new
                    // TODO: This doesn't work though, so as a workaround $request->fillFromPeers() is called from the template
                    // it looks like the $request object is not as complete here as it is there, for instance getForms returns and empty array
                    /*
                    if (!$request->fillFromPeers()) {
                        $this->Flash->error(__('Nepodalo se inicializovat sdlené formuláře'));
                        Log::debug(json_encode($request));
                        return;
                    };
                    */
                    $this->Flash->success(__('Žádost byla úspěšně uložena'));
                    $this->redirect(['action' => 'requestDetail', $request->id]);
                } else {
                    $this->Flash->error(__('Nebylo možné uložit žádost, formulář obsahuje chyby'));
                    Log::debug(json_encode($request->getErrors()));
                }
            }
        }

        $crumbs = [__('Mé žádosti o podporu') => 'user_requests'];
        if (!$request->isNew()) {
            $crumbs[h($request->name)] = ['_name' => 'request_detail', 'id' => $request->id];
        }

        // List of programs with "merging_of_requests"
        $programs_to_join = $this->Programs->find(
            'all',
            [
                'fields' => array('id'),
                'conditions' => array('Programs.merging_of_requests' => '1'),
            ]
        )->toArray();
        $programs_to_join_ids = count($programs_to_join) > 0
            ? array_reduce($programs_to_join, function ($result, $value) {
                $result[] = $value['id'];
                return $result;
            })
            : [];
        $requests_to_join = $this->Requests->find(
            'all',
            [
                'fields' => array('id', 'name'),
                'conditions' => [
                    'Requests.id !=' => $id > 0 ? $id : 0,
                    'Requests.merged_request IS' => NULL,
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    // 'Requests.program_id IN' => $programs_to_join_ids,
                    'Requests.request_state_id >=' => RequestState::STATE_FORMAL_CHECK_APPROVED,
                    'Requests.user_id' => $this->getCurrentUser()->id,
                ],
            ]
        )->toArray();
        $requests_to_join_ids = count($programs_to_join) > 0
            ? array_reduce($requests_to_join, function ($result, $value) {
                $result[$value['id']] = $value['name'];
                return $result;
            })
            : [];

        $this->set(compact(
            'appeals_programs',
            'appeals',
            'crumbs',
            'program_descriptions',
            'programs_to_join_ids',
            'request',
            'requests_to_join_ids'
        ));
    }

    /**
     * Download verification pdf, if possible
     *
     * @param int $request_id
     */
    public function downloadRequestVerificationPdf(int $request_id)
    {
        $request = $this->getRequestById($request_id);
        if (!$request->canUserDownloadOwnVerificationPdf()) {
            $this->Flash->error(__('Potvrzení o elektronické evidenci žádosti nyní nelze stáhnout'));
            $this->redirect($this->referer());
            return;
        }

        $this->set(compact('request'));

        $this->renderAsPdf(sprintf('Potvrzeni_o_elektronicke_evidenci_zadosti_%d_%d.pdf', $request->id, date('YmdHis')), true, 'portrait');

        $this->viewBuilder()->setTemplate('/UserRequests/download_request_verification_pdf');
    }

    /**
     * Allow lock request if possible, allow unlock if possible
     *
     * @param int $request_id
     * @param string $do
     */
    public function lockUnlockRequest(int $request_id, string $do = '')
    {
        $request = $this->getRequestById($request_id);
        if (!$request->canUserLockOwnRequest() && !$request->canUserUnlockOwnRequest()) {
            $this->Flash->error(__('Tuto žádost nemůžete aktuálně ani zamknout ani odemknout'));
            $this->redirect($this->referer());
            return;
        }

        if (!empty($do)) {
            if ($request->canUserUnlockOwnRequest()) {
                switch ($do) {
                    case 'unlock':
                        $request->setCurrentStatus(RequestState::STATE_READY_TO_SUBMIT, $this->getCurrentUserId());
                        break;
                }
            } else if ($request->canUserLockOwnRequest()) {
                switch ($do) {
                    /** @noinspection PhpMissingBreakStatementInspection */
                    case 'lockAndDownload':
                        $this->redirect(['action' => 'downloadRequestVerificationPdf', 'id' => $request->id]);
                        // fallthrough intentional
                    case 'lock':
                        $request->setCurrentStatus(RequestState::STATE_SUBMIT_LOCK, $this->getCurrentUserId());
                        break;
                }
            }
            if ($request->isDirty()) {
                if ($this->Requests->save($request)) {
                    $this->Flash->success(__('Provedeno úspěšně'));

                    $this->sendMailToOrganization($request, $request->request_state_id);
                } else {
                    $this->Flash->error(__('Akci nebylo možné provést'));
                    $this->Flash->error($request->getFirstError());
                }
            }
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests', $request->name => ['action' => 'requestDetail', $request_id]]);
    }

    public function getRequestById(int $request_id, bool $skip_auth_check = false): Request
    {
        if (!$skip_auth_check) {
            $conditions = [
                'Requests.user_id' => $this->Auth->user('id'),
                'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ];
        } else {
            $conditions = [
                'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ];
        }
        return $this->Requests->get(
            $request_id,
            [
                'conditions' => $conditions,
                'contain' => RequestsTable::CONTAIN_EXTENDED,
            ]
        );
    }

    public function sendMailToOrganization(mixed $request, int $new_request_state_id, ?bool $state_settlement_submitted = false, ?bool $returned_settlement = false)
    {
        // if allowed EMAIL_MESSAGE_NEW_REQUEST/EMAIL_MESSAGE_UPDATED_REQUEST send an e-mail
        $text = '';
        $sendAnEmail = ($new_request_state_id === RequestState::STATE_SUBMIT_LOCK) ||
            ($state_settlement_submitted && $new_request_state_id === RequestState::STATE_SETTLEMENT_SUBMITTED) ||
            (($request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) && ($new_request_state_id === RequestState::STATE_SUBMIT_LOCK || $new_request_state_id === RequestState::STATE_SUBMITTED)) ||
            ($request->request_state_id === RequestState::STATE_SUBMIT_LOCK && $new_request_state_id === RequestState::STATE_SUBMITTED);
        $sendAnUpdatedEmail = ($state_settlement_submitted && $returned_settlement) || ($sendAnEmail && !empty($request->lock_comment));

        $orgEmail = OrganizationSetting::getSetting(OrganizationSetting::CONTACT_EMAIL);
        $orgName =  OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME);

        if (
            $sendAnEmail && (OrganizationSetting::getSetting(OrganizationSetting::EMAIL_MESSAGE_NEW_REQUEST) ||
                ($state_settlement_submitted && OrganizationSetting::getSetting(OrganizationSetting::EMAIL_MESSAGE_NEW_SETTLEMENT)))
        ) {
            $subtext = !$state_settlement_submitted ? __('informujeme Vás o nové žádosti') : __('informujeme Vás o novém vyúčtování k žádosti');
            $text = sprintf(
                "<br/>" . __('Dobrý den') . ",<br/><br/>" . $subtext . " \"%s\" č. \"%s\" na dotačním portále.<br/><br/>S pozdravem<br/>%s",
                $request->name,
                $request->id,
                $orgName,
            );
        } else if (
            $sendAnUpdatedEmail && (OrganizationSetting::getSetting(OrganizationSetting::EMAIL_MESSAGE_UPDATED_REQUEST) ||
                ($state_settlement_submitted && OrganizationSetting::getSetting(OrganizationSetting::EMAIL_MESSAGE_UPDATED_SETTLEMENT)))
        ) {
            $subtext = !$state_settlement_submitted ? __('informujeme Vás o opravené žádosti') : __('informujeme Vás o opraveném vyúčtování k žádosti');
            $text = sprintf(
                "<br/>" . __('Dobrý den') . ",<br/><br/>" . $subtext . " \"%s\" č. \"%s\" na dotačním portále.<br/><br/>S pozdravem<br/>%s",
                $request->name,
                $request->id,
                $orgName,
            );
        }

        $subject = $state_settlement_submitted
            ? ($sendAnUpdatedEmail
                ? __('Přišlo opravené vyúčtování od uživatele dotačního portálu')
                : __('Přišla nové vyúčtování od uživatele dotačního portálu'))
            : ($sendAnUpdatedEmail
                ? __('Přišla opravená žádost od uživatele dotačního portálu')
                : __('Přišla nová žádost od uživatele dotačního portálu'));

        if (!empty($text)) {
            $mailIsSent = $this->sendMailSafe('User', 'teamMessage', [
                [$orgEmail],
                $subject,
                $text
            ]);

            if ($mailIsSent) {
                $this->Flash->success($state_settlement_submitted
                    ? __('E-mail o odeslání vyúčtování byl úspěšně odeslán')
                    : __('E-mail o odeslání žádosti byl úspěšně odeslán'));
            } else {
                $this->Flash->error($state_settlement_submitted
                    ? __('Při odesílání e-mailu o odeslání vyúčtování došlo k neočekávané chybě')
                    : __('Při odesílání e-mailu o odeslání žádosti došlo k neočekávané chybě'));
            }
        }
    }

    private function getFormById(int $form_id): Form
    {
        return $this->Requests->Programs->Forms->get(
            $form_id,
            [
                'contain' => [
                    'FormFields',
                    'FormFields.FormFieldTypes',
                ],
            ]
        );
    }
}
