<?php

namespace App\Controller;

use App\Controller\Component\IsdsComponent;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Table\IdentitiesTable;
use Cake\Http\Response;
use Cake\Log\Log;
use Exception;
use Throwable;

/**
 * @property IsdsComponent $Isds
 * @property IdentitiesTable $Identities
 */
class IsdsController extends AppController
{
    public const ISDS_APP_TOKEN = 'isds.app_token';
    public const ISDS_TIME_LIMITED_ID = 'isds.time_limited_id';
    public const ISDS_VERIFIED_DS_ID = 'isds.verified.ds_id';
    public const ISDS_VERIFIED_USER_TYPE = 'isds.verified.user_type';
    public const ISDS_VERIFIED_DS_TYPE = 'isds.verified.db_type';
    public const ISDS_VERIFIED_SUBJECT_NAME = 'isds.verified.full_user_name';
    public const ISDS_VERIFIED_FIRM_NAME = 'isds.verified.firm_name';
    public const ISDS_VERIFIED_ICO = 'isds.verified.ico';
    public const ISDS_VERIFIED_FO_BIRTH_DATE = 'isds.verified.fo_birth_date';
    public const ISDS_VERIFIED_FO_FIRST_NAME = 'isds.verified.fo_first_name';
    public const ISDS_VERIFIED_FO_LAST_NAME = 'isds.verified.fo_last_name';
    public const ISDS_VERIFIED_FO_ADDRESS_CITY = 'isds.verified.fo_address_city';
    public const ISDS_VERIFIED_FO_ADDRESS_ZIP = 'isds.verified.fo_address_zip';
    public const ISDS_VERIFIED_FO_ADDRESS_FULL = 'isds.verified.fo_address_full';
    public const ISDS_VERIFIED_FO_ADDRESS_STREET_FULL = 'isds.verified.fo_address_street_full';
    public const ISDS_VERIFIED_FO_ADDRESS_DISTRICT = 'isds.verified.fo_address_district';
    public const ISDS_VERIFIED_FO_ROB_IDENT = 'isds.verified.fo_rob_ident';
    public const ISDS_RAW_USER_ATTRIBUTES = 'isds.raw.user_attributes';
    public const ISDS_CONCEPT_DM_ID = 'isds.concept.dm_id';
    public const ISDS_CONCEPT_STATUS_CODE = 'isds.concept.status_code';
    public const ISDS_CONCEPT_STATUS_MESSAGE = 'isds.concept.status_message';

    public const MAP_IDENTITY_STORAGE_ISDS_ATRRIBUTE = [
        Identity::ISDS_VERIFIED_BUSINESS_ID => self::ISDS_VERIFIED_ICO,
        Identity::ISDS_VERIFIED_BUSINES_NAME => self::ISDS_VERIFIED_FIRM_NAME,
        Identity::ISDS_VERIFIED_DATABOX_ID => self::ISDS_VERIFIED_DS_ID,
        Identity::ISDS_VERIFIED_DATABOX_TYPE => self::ISDS_VERIFIED_DS_TYPE,
        Identity::CONTACT_DATABOX => self::ISDS_VERIFIED_DS_ID,
        Identity::ISDS_VERIFIED_FO_DATE_OF_BIRTH => self::ISDS_VERIFIED_FO_BIRTH_DATE,
        Identity::ISDS_VERIFIED_FO_FIRST_NAME => self::ISDS_VERIFIED_FO_FIRST_NAME,
        Identity::ISDS_VERIFIED_FO_LAST_NAME => self::ISDS_VERIFIED_FO_LAST_NAME,
        Identity::ISDS_VERIFIED_FO_ADDRESS_ZIP => self::ISDS_VERIFIED_FO_ADDRESS_ZIP,
        Identity::ISDS_VERIFIED_FO_ADDRESS_CITY => self::ISDS_VERIFIED_FO_ADDRESS_CITY,
        Identity::ISDS_VERIFIED_FO_ADDRESS_FULL => self::ISDS_VERIFIED_FO_ADDRESS_FULL,
        Identity::ISDS_VERIFIED_FO_ADDRESS_STREET => self::ISDS_VERIFIED_FO_ADDRESS_STREET_FULL,
        Identity::ISDS_VERIFIED_FO_ROB_IDENT => self::ISDS_VERIFIED_FO_ROB_IDENT,
        Identity::ISDS_VERIFIED_FO_ADDRESS_DISTRICT => self::ISDS_VERIFIED_FO_ADDRESS_DISTRICT,
    ];

    public const ISDS_AFTER_AUTH_HANDLER = 'isds.after_auth_handler';

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Isds');
        $this->loadModel('Identities');
    }

    public function authRedirect(): ?Response
    {
        $atsId = $this->Isds->filter_atsId(OrganizationSetting::getSetting(OrganizationSetting::DS_ATS_ID));
        if (empty($atsId)) {
            $this->Flash->error(sprintf(__('%s není nastaveno, kontaktujte prosím správce dotačního portálu'), 'Datová Schránka atsId'));
            // not referer, to prevent infinite loop
            return $this->redirect('/');
        }
        $url = $this->Isds->getAtsLoginUrl($atsId, $this->getRequest()->getSession()->read(self::ISDS_APP_TOKEN));

        return $this->redirect($url);
    }

    public function authCallback(string $sessionId, $test_only = false)
    {
        
        $sessionId = $this->Isds->filter_sessionId($sessionId);
        if (empty($sessionId)) {
            $this->Flash->error(__('Portál Datových Schránek neposkytl platnou odpověď, akce není možná'));

            return $this->redirect('/');
        }
        
        list($cert, $key) = $this->Isds->getCertificateAndPrivateKey();
        if (empty($cert) || empty($key)) {
            return $this->redirect('/');
        }
        
        $xml = $this->Isds->getAuthConfirmationRequest($sessionId);
        $url = $this->Isds->getAuthConfirmationRequestUrl();
        $response = null;
        $parsed = null;
        
        try {
            $response = $this->Isds->execSoapAction($url, $cert, $key, $xml);
            if ($test_only) {
                return true;
            }
            Log::debug('isds returned raw xml ' . $response);
            $parsed = $this->Isds->extractAuthConfirmationResponse($response);
            $this->getRequest()->getSession()->write(self::ISDS_RAW_USER_ATTRIBUTES, $parsed);
            Log::debug('isds returned: ' . json_encode($parsed));
            if (!empty($parsed['timeLimitedId'])) {
                $this->getRequest()->getSession()->write(self::ISDS_TIME_LIMITED_ID, $parsed['timeLimitedId']);
            } else {
                throw new Exception('timeLimitedId not found');
            }

            if (isset($parsed['appToken'])) {
                $this->getRequest()->getSession()->write(self::ISDS_APP_TOKEN, $parsed['appToken']);
            }
            $mapIsdsToSession = [
                'dbID' => self::ISDS_VERIFIED_DS_ID,
                'firmName' => self::ISDS_VERIFIED_FIRM_NAME,
                'ic' => self::ISDS_VERIFIED_ICO,
                'fullUserName' => self::ISDS_VERIFIED_SUBJECT_NAME,
                'dbType' => self::ISDS_VERIFIED_DS_TYPE,
                'userType' => self::ISDS_VERIFIED_DS_TYPE,
                'conceptDmId' => self::ISDS_CONCEPT_DM_ID,
                'conceptStatusCode' => self::ISDS_CONCEPT_STATUS_CODE,
                'conceptStatusMessage' => self::ISDS_CONCEPT_STATUS_MESSAGE,
                'biDate' => self::ISDS_VERIFIED_FO_BIRTH_DATE,
                'adCity' => self::ISDS_VERIFIED_FO_ADDRESS_CITY,
                'adDistrict' => self::ISDS_VERIFIED_FO_ADDRESS_DISTRICT,
                'adZipCode' => self::ISDS_VERIFIED_FO_ADDRESS_ZIP,
                'fullAddress' => self::ISDS_VERIFIED_FO_ADDRESS_FULL,
                'pnFirstName' => self::ISDS_VERIFIED_FO_FIRST_NAME,
                'pnLastName' => self::ISDS_VERIFIED_FO_LAST_NAME,
            ];
            foreach ($mapIsdsToSession as $isdsAttribute => $sessionAttribute) {
                if (isset($parsed[$isdsAttribute])) {
                    $this->getRequest()->getSession()->write($sessionAttribute, $parsed[$isdsAttribute]);
                }
            }
            // specially handled return values
            if (isset($parsed['robIdent'])) {
                // convert text TRUE/FALSE to native bool
                $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_FO_ROB_IDENT, mb_strtolower($parsed['robIdent']) === 'true');
            }
            // assemble street with numbers, same as in identity form
            $assembledStreet = '';
            if (isset($parsed['adStreet'])) {
                $assembledStreet .= $parsed['adStreet'];
            }
            if (!empty($parsed['adNumberInMunicipality'])) {
                $assembledStreet .= sprintf(" %s", $parsed['adNumberInMunicipality']);
            }
            if (!empty($parsed['adNumberInStreet'])) {
                $assembledStreet .= sprintf("/%s", $parsed['adNumberInStreet']);
            }
            $this->getRequest()->getSession()->write(self::ISDS_VERIFIED_FO_ADDRESS_STREET_FULL, $assembledStreet);
        } catch (Throwable $t) {
            $this->getRequest()->getSession()->delete(self::ISDS_APP_TOKEN);
            $this->getRequest()->getSession()->delete(self::ISDS_TIME_LIMITED_ID);
            $this->Flash->error(__('Nebylo možné získat identifikátor ze systému Datových Schránek'));

            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
            Log::error('response: ' . $response);
            Log::error('parsed: ' . json_encode($parsed));

            return $this->redirect('/');
        }
        
        // return to user handler to perform whatever was meant to be done after ISDS auth
        return $this->redirect($this->getRequest()->getSession()->consume(self::ISDS_AFTER_AUTH_HANDLER) ?? ['action' => 'storeVerifiedAttributes']);
    }

    public function storeVerifiedAttributes()
    {
        if ($this->Isds->storeVerifiedAttributes($this)) {
            $this->Flash->success(__('Údaje z datové schránky byly úspěšně získány'));
        } else {
            $this->Flash->error(__('Údaje z datové schránky bohužel nebylo možné získat'));
        }
        $this->redirect(['_name' => 'update_self_info']);
    }

    public function testISDS() {
        if ($this->authCallback('1234567890',true) === TRUE) {
            return $this->getResponse()->withType('json')->withstringBody(json_encode(['result'=>'OK']));
        } else {
            return $this->getResponse()->withType('json')->withstringBody(json_encode(['result'=>'FAIL']));
        }        
    }
}
