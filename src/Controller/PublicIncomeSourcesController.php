<?php


namespace App\Controller;


use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\PublicIncomeSourcesTable;
use Cake\Http\Response;

/**
 * @property-read PublicIncomeSourcesTable $PublicIncomeSources
 */
class PublicIncomeSourcesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('PublicIncomeSources');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isAuthorized($user = null): bool
    {
        if (!parent::isAuthorized($user)) {
            return false;
        }

        if (
            !$this->isAuthorizedToOrganization()
            && !$this->isAuthorizedToOrganization(null, UserRole::MANAGER_PORTALS)
        ) {
            return false;
        }

        return true;
    }

    public function index()
    {
        $sources = $this->PublicIncomeSources->find('all', [
            'conditions' => [
                'OR' => [
                    'PublicIncomeSources.organization_id IS' => null,
                    'PublicIncomeSources.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ]
            ]
        ]);
        $this->set(compact('sources'));
    }

    public function addModify(int $id = null)
    {
        $source = $id > 0 ? $this->PublicIncomeSources->get($id, [
            'conditions' => [
                'PublicIncomeSources.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ]) : $this->PublicIncomeSources->newEntity([
            'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()
        ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $source = $this->PublicIncomeSources->patchEntity($source, $this->getRequest()->getData());
            if ($this->PublicIncomeSources->save($source)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('source'));
        $this->set('crumbs', [__('Zdroje veřejné podpory') => ['action' => 'index']]);
    }

    public function delete(int $id): ?Response
    {
        $source = $this->PublicIncomeSources->get($id, [
            'conditions' => [
                'PublicIncomeSources.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ]);
        if ($this->PublicIncomeSources->delete($source)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error($source->getFirstError());
        }
        return $this->redirect($this->referer());
    }

}