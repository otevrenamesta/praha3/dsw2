<?php

declare(strict_types=1);

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use Cake\Event\Event;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\Model\Table\AppealsTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\RequestsTable;
use App\Model\Table\SettlementsTable;
use Cake\Filesystem\File;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
use Cake\Http\Exception\ForbiddenException;
use OldDsw\Controller\Component\OldDswComponent;
use function Laminas\Diactoros\createUploadedFile;

/**
 * @property-read AppealsTable $Appeals
 * @property-read ProgramsTable $Programs
 * @property-read RequestsTable $Requests
 * @property-read SettlementsTable $Settlements
 * @property-read OldDswComponent $OldDsw
 */
class OpenDataController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Appeals');
        $this->loadModel('Programs');
        $this->loadModel('Requests');
        $this->loadModel('Settlements');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $this->loadComponent('OldDsw.OldDsw');
        }
        $this->Auth->allow();
    }

    public function isAuthorized($user = null): bool
    {
        return parent::isAuthorized($user) && $this->getCurrentUser()->isEconomicsManager();
    }

    // Disable POST request form chcecks for AJAX
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['openDataUploadEdits']);
    }



    public function index()
    {
        // The index is public
        $this->Auth->allow();

        $states = SettlementState::getKnownStatesWithLabels();

        $appeals = $this->Appeals->find(
            'list',
            [
                'conditions' => [
                    'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->order(
            ['`Appeals`.`open_from`' => 'DESC', '`Appeals`.`name`' => 'ASC']
        )->toArray();


        $conditions = [
            'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Settlements.published' => 1
        ];

        if ($this->getRequest()->is(['get'])) {
            $request_appeal_id = $this->getRequest()->getQuery('appeal_id');
            if ($request_appeal_id && !in_array(intval($request_appeal_id), array_keys($appeals), true)) {
                throw new NotFoundException();
            }
        }



        /** @var Settlement[] $settlements */

        $settlements = $this->Settlements->find('all', [
            'conditions' => $conditions,
            'contain' => [
                'RequestsToSettlements.Requests',
                'SettlementItems',
            ]
        ])->order(
            ['`Settlements`.`id`' => 'DESC']
        )
            ->toArray();

        // Filter out non matching requests
        // TODO: should be in the query

        if (isset($request_appeal_id) && trim($request_appeal_id)) {
            foreach ($settlements as $key => $settlement) {
                if (
                    !isset($settlement->requests_to_settlements) ||
                    empty($settlement->requests_to_settlements) ||
                    $request_appeal_id != $settlement->getRequest()->get('appeal_id')
                ) {
                    unset($settlements[$key]);
                };
            }
        }

        if (!empty($settlements) && OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            $this->Settlements->loadInto($settlements, [
                'SettlementsToOlddswRequests',
                'SettlementsToOlddswRequests.Zadosti' => [
                    'Ucet',
                    'Ucet.FyzickeOsoby',
                    'Ucet.StatutarniOrgan',
                ]
            ]);
        }

        $this->setRequest($this->getRequest()->withData('state_ids', []));

        $this->set(compact('settlements', 'appeals', 'states'));
        $this->set('crumbs', [__('Otevřená data') => 'open_data']);
    }

    /**
     * Ajax endpoint for toggling published/unpublished settlement status
     *
     * @param int $settlement_id
     *
     * @return [type]
     */
    public function toggle(int $id, $set = false)
    {

        $settlement = $this->authGetSettlement($id);

        // Togle published status
        if ($set == false) {
            $settlement->published = ($settlement->published == 1) ? 0 : 1;
        } else {
            $settlement->published = $set;
        }
        if (!$this->Settlements->save($settlement)) {
            $this->Flash->error($settlement->getFirstError());
        }
        if ($set) return;
        $this->response->withType('json');
        $this->response->withstringBody('OK (' . $settlement->published . ')');
        return $this->response;
    }

    /**
     * Ajax endpoint for uploading an array of strings replacements for anonymisation
     *
     * @param int $settlement_id
     *
     * @return [type]
     */

    public function openDataUploadEdits(int $id)
    {
        $settlement = $this->authGetSettlement($id);

        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        // Save replacement's data
        $settlement->anynonym_mask = $this->request->getData('anonym_mask', "{}");
        if (!$this->Settlements->save($settlement)) {
            $this->Flash->error($settlement->getFirstError());
        } else {
            $this->response->withType('json');
            $this->response->withstringBody('OK');
            return $this->response;
        }
    }


    public function openDataDetail(int $id)
    {
        $P3Exception = OrganizationSetting::isException(OrganizationSetting::P3);
        if ($P3Exception && $this->isP3OldDSWSettlement($id)) {
            return $this->openDataDownloadP3OldDSWSettlement($id);
        }

        $settlement = $this->authGetSettlement($id, true);
        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);

        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list');
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;

        $this->set(compact('settlement', 'attachmentTypes', 'program', 'P3Exception', 'mergedRequest'));
    }

    public function openDataAnonymize(int $id)
    {
        $P3Exception = OrganizationSetting::isException(OrganizationSetting::P3);
        $settlement = $this->authGetSettlement($id);
        $request = $settlement->getRequest();
        $program = $this->Programs->get($request->program_id);

        $settlement_attachment = $this->Settlements->SettlementAttachments->newEntity();
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list');
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $this->set(compact('settlement', 'settlement_attachment', 'attachmentTypes', 'program', 'P3Exception', 'mergedRequest', 'budget'));
    }

    public function openDataDeleteFile(int $settlement_id, int $attachment_id)
    {
        $settlement = $this->authGetSettlement($settlement_id);

        $attachment = $settlement->getAttachmentsById($attachment_id, true);

        if ($this->Settlements->SettlementAttachments->delete($attachment)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Přílohu není možné smazat'));
        }

        return $this->redirect(['action' => 'openDataAnonymize', 'id' => $settlement->id]);
    }

    public function openDataAddModifyFile(int $id, ?int $attachment_id = null)
    {

        $settlement = $this->authGetSettlement($id);


        $attachment = $attachment_id > 0 ? $settlement->getAttachmentsById($attachment_id, true) :
            $this->Settlements->SettlementAttachments->newEntity();


        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $originalFileToDelete = null;
            $fileWasUploaded = false;
            $attachment = $this->Settlements->SettlementAttachments->patchEntity(
                $attachment,
                $this->getRequest()->getData() + [
                    'settlement_id' => $settlement->id,
                    'is_public' => false
                ]
            );
            $filedata = $this->getRequest()->getData('filedata');

            if (!empty($filedata)) {

                $uploadedFile = createUploadedFile($this->getRequest()->getData('filedata'));
                if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                    if ($uploadedFile->getError() !== UPLOAD_ERR_NO_FILE) {
                        $attachment->setError('filedata', __('Nahrávání souboru se nepodařilo'));
                        $this->Flash->error(__('Nahrávání souboru se nepodařilo'));
                    } else {
                        $attachment->setError('filedata', __('Nebyl nahrán žádný soubor'));
                    }
                } else {
                    $file = $this->Settlements->SettlementAttachments->Files->newEntity();
                    $uploadedMovedFilePath = $file->fileUploadMove($this->getRequest()->getData('filedata'), $this->getCurrentUserId());
                    if (is_string($uploadedMovedFilePath)) {
                        $file->original_filename = $uploadedFile->getClientFilename();
                        $file->filesize = $uploadedFile->getSize();
                        $file->filepath = $uploadedMovedFilePath;
                        $file->user_id = $this->getCurrentUserId();
                        $originalFileToDelete = $attachment->file_id;
                        $attachment->unsetProperty('file_id');
                        $attachment->file = $file;
                        $attachment->setDirty('file');
                        $fileWasUploaded = true;
                    } else {
                        $this->Flash->error(__('Chyba při zpracování nahrávaného souboru'));
                        $attachment->setError('filedata', __('Chyba při zpracování nahrávaného souboru'));
                    }
                }
            }


            if ($this->Settlements->SettlementAttachments->save($attachment)) {
                if ($originalFileToDelete > 0) {
                    $originalFile = $this->Settlements->SettlementAttachments->Files->get($originalFileToDelete);
                    if ($this->Settlements->SettlementAttachments->Files->delete($originalFile)) {
                        $originalFile->deletePhysically();
                    }
                }
                $this->Flash->success($fileWasUploaded ? __('Příloha nahrána úspěšně') : __('Uloženo úspěšně'));
                $this->redirect(['action' => 'openDataAnonymize', 'id' => $settlement->id]);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list')->order(['order' => 'ASC']);
        $uploadFileType = $this->getRequest()->getParam('type');
        if (!empty($uploadFileType)) {
            switch ($uploadFileType) {
                case "report":
                    $attachmentTypes->where([
                        'id IN' => [SettlementAttachmentType::TYPE_FINAL_REPORT_WITH_PUBLICITY, SettlementAttachmentType::TYPE_FINAL_REPORT],
                    ]);
                    break;
                case "publicity":
                    $attachmentTypes->where([
                        'id IN' => [SettlementAttachmentType::TYPE_PUBLICITY_PROOF, SettlementAttachmentType::TYPE_FINAL_REPORT_WITH_PUBLICITY],
                    ]);
                    break;
            }
        }
        $this->set(compact('settlement', 'attachment', 'attachmentTypes'));
        //$this->set('crumbs', [__('Mé žádosti o podporu') => ['action' => 'index'], sprintf("%s #%d", __('Vyúčtování žádosti'), $settlement->getRequest()->id) => ['action' => 'settlementDetail', 'id' => $settlement->id]]);
    }

    private function authGetSettlement(int $settlement_id, $no_auth = false)
    {

        if (!$no_auth) {
            if (!$this->isAuthorized()) {
                throw new ForbiddenException();
            }
        }


        $settlement = $this->Settlements->get($settlement_id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);

        if (!$settlement) {
            throw new NotFoundException('Loading settlement ' . $settlement_id . 'has failed');
        }

        return $settlement;
    }

    public function openDataSaveAndPublish(int $id)
    {

        $settlement = $this->authGetSettlement($id);
        $this->toggle($id, 1);
        return $this->redirect(['controller' => 'Economics', 'action' => 'settlements', '#' => $settlement->getRequest()->id]);
    }

    public function openDataDiscard(int $id)
    {

        $settlement = $this->authGetSettlement($id);
        $settlement->anynonym_mask = "{}";
        if (!$this->Settlements->save($settlement)) {
            $this->Flash->error($settlement->getFirstError());
        }
        return $this->redirect(['action' => 'openDataAnonymize', 'id' => $settlement->id]);
    }

    public function openDataSaveOnly(int $id)
    {

        // Nothing to do, everything has already been saved via ajax
        $settlement = $this->authGetSettlement($id);
        return $this->redirect(['controller' => 'Economics', 'action' => 'settlements', '#' => $settlement->getRequest()->id]);
    }

    public function openDataPublicFile(int $id, int $file_id)
    {
        // TODO: check the settlement is published and type is anonymized
        $settlement = $this->authGetSettlement($id, true);

        $file = $settlement->getFileById($file_id, true);


        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }



    public function openDataDownloadFile(int $id, int $file_id)
    {
        $settlement = $this->authGetSettlement($id);

        $file = $settlement->getFileById($file_id, true);

        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }
    public function openDataDownloadP3OldDSWSettlement(int $id)
    {
        if (!$this->isP3OldDSWSettlement($id)) {
            return $this->getResponse()->withStatus(404);
        }
        $fs = new \App\Model\Entity\File;
        $path = $fs->getFileStoragePath();
        $file = $path . 'P3Vyuctovani/' . $id . '_vyuctovani_komplet.pdf';
        if (!file_exists($file)) {
            return $this->getResponse()->withStatus(404);
        } else {
            return $this->getResponse()->withFile($file, ['name' => urlencode($id . '_vyuctovani_komplet.pdf'), 'download' => true]);
        }
    }

    // Some OldDSW P3 settlements were published manually.

    private function isP3OldDSWSettlement(int $id)
    {
        return in_array($id, [
            100, 138, 192, 227, 265, 69,
            101, 139, 193, 228, 266, 71,
            102, 13, 194, 229, 267, 72,
            103, 140, 195, 22, 26, 73,
            104, 141, 196, 230, 28, 74,
            105, 142, 197, 231, 30, 75,
            106, 143, 198, 232, 31, 76,
            107, 145, 199, 233, 32, 77,
            108, 147, 19, 234, 33, 78,
            109, 148, 200, 235, 34, 79,
            110, 149, 201, 236, 37, 80,
            111, 150, 202, 238, 44, 81,
            113, 151, 204, 239, 45, 82,
            114, 152, 205, 23, 46, 83,
            115, 153, 207, 240, 47, 84,
            116, 154, 209, 241, 49, 85,
            117, 155, 20, 242, 4,  86,
            119, 156, 210, 243, 51, 87,
            11, 157, 211, 245, 52, 88,
            120, 158, 212, 246, 53, 89,
            121, 159, 213, 247, 54, 8,
            122, 15, 214, 248, 55, 90,
            124, 160, 215, 249, 56, 91,
            125, 161, 216, 24, 57, 92,
            126, 162, 217, 250, 58, 93,
            127, 163, 218, 251, 59, 95,
            128, 164, 219, 252, 5,  96,
            129, 165, 21, 253, 60, 97,
            130, 170, 220, 255, 61, 98,
            131, 183, 221, 256, 62, 9,
            132, 186, 222, 257, 64,
            134, 188, 223, 258, 65,
            135, 18, 224, 25, 66,
            136, 190, 225, 261, 67,
            137, 191, 226, 264, 68,
        ]);
    }
}
