<?php

declare(strict_types=1);

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\AgendioReport;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\ReportColumn;
use App\Model\Entity\RequestState;
use App\Model\Entity\User;
use App\Model\Table\AgendioColumnsTable;
use App\Model\Table\AgendioReportsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\ReportsTable;
use App\Model\Table\RequestsTable;
use App\Traits\ReportColumnsAwareTrait;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\Utility\Hash;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read ReportsTable $Reports
 * @property-read RequestsTable $Requests
 * @property-read ProgramsTable $Programs
 * @property-read AgendioReportsTable $AgendioReports
 * @property-read AgendioColumnsTable $AgendioColumns
 */
class ReportsManagerController extends AppController
{
    use ReportColumnsAwareTrait;

    public const SESSION_LAST_STATE_ID = 'reports_manager.%d.last_state_id';
    public const SESSION_LAST_APPEAL_ID = 'reports_manager.%d.last_appeal_id';
    public const SESSION_LAST_PROGRAMS = 'reports_manager.%d.last_programs';
    public const SESSION_MAPPING = 'reports_manager.column_mapping';

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Reports');
        $this->loadModel('Requests');
        $this->loadModel('Programs');
        $this->loadModel('AgendioReports');
        $this->loadModel('AgendioColumns');
    }

    public function isAuthorized($user = null): bool
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isGrantsManager();
    }

    public function index()
    {
        $this->set('reports', $this->Reports->findWithOrganization(OrgDomainsMiddleware::getCurrentOrganizationId()));
    }

    public function addModify(int $report_id = 0)
    {
        $report = $report_id > 0 ? $this->Reports->getWithOrganization($report_id, OrgDomainsMiddleware::getCurrentOrganizationId())
            : $this->Reports->newEntity([
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $report = $this->Reports->patchEntity($report, $this->getRequest()->getData());
            if ($this->Reports->save($report)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('report'));
        $this->set('crumbs', [__('Sestavy') => 'admin_reports']);
    }

    public function delete(int $report_id)
    {
        $report = $this->Reports->getWithOrganization($report_id, OrgDomainsMiddleware::getCurrentOrganizationId());
        if ($this->Reports->delete($report)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Sestavu nebylo možné vymazat'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleteColumn(int $report_id, int $column_id)
    {
        $report = $this->Reports->getWithOrganization($report_id, OrgDomainsMiddleware::getCurrentOrganizationId());
        $column = $this->Reports->ReportColumns->getWithReport($column_id, $report_id);
        if ($this->Reports->ReportColumns->delete($column)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Sloupec nebylo možné smazat'));
        }
        return $this->redirect(['action' => 'renderReport', 'id' => $report->id]);
    }

    public function addModifyColumn(int $report_id, int $column_id = 0)
    {
        $report = $this->Reports->getWithOrganization($report_id, OrgDomainsMiddleware::getCurrentOrganizationId());
        $column = $column_id > 0 ? $this->Reports->ReportColumns->getWithReport($column_id, $report_id)
            : $this->Reports->ReportColumns->newEntity([
                'report_id' => $report_id,
            ]);

        $columnOrder = $this->Reports->ReportColumns->find('list', [
            'keyField' => 'order',
            'valueField' => 'name',
            'conditions' => [
                'report_id' => $report->id,
                'id !=' => $column->id ?? 0,
            ]
        ])->order(['order' => 'ASC'])->toArray();

        $columnDataOptions = $this->getReportColumnsDataOptions();

        $agendioColumnOptions = [];
        /** @var AgendioReport[] $allAgendioReports */
        $allAgendioReports = $this->AgendioReports->find('all', [
            'contain' => [
                'AgendioColumns'
            ]
        ]);
        foreach ($allAgendioReports as $agendio_report) {
            $values = [];
            foreach ($agendio_report->agendio_columns as $report_column) {
                $values[$report_column->id] = sprintf("%s (%s)", $report_column->label, $report_column->export_name);
            }
            $agendioColumnOptions[$agendio_report->label] = $values;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $mapping = $this->getRequest()->getSession()->read(self::SESSION_MAPPING) ?? [];
            $postData = $this->getRequest()->getData();
            if (isset($postData['path']) && is_numeric($postData['path'])) {
                $path_id = intval($postData['path']);
                $postData['path'] = $mapping[$path_id] ?? null;
            }

            $column = $this->Reports->ReportColumns->patchEntity($column, $postData);
            $column->order++;
            $this->fixColumnOrder($column);
            if ($this->Reports->ReportColumns->save($column)) {
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->redirect(['action' => 'renderReport', 'id' => $report->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $columnDataOptions = $this->extractMapping($columnDataOptions);
        $this->getRequest()->getSession()->write(self::SESSION_MAPPING, $this->_mapping);

        $column->order = $column->isNew() ? (empty($columnOrder) ? 0 : max(array_keys($columnOrder))) : --$column->order;
        $column->path = $column->isNew() ? $column->path : array_flip($this->_mapping)[$column->path];
        $this->set(compact('report', 'column', 'columnOrder', 'columnDataOptions', 'agendioColumnOptions'));
        $this->set('crumbs', [__('Sestavy') => 'admin_reports', $report->name => ['action' => 'renderReport', 'id' => $report->id]]);
    }

    private array $_mapping = [];
    private int $_mappingCounter = 1;

    function extractMapping(array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = $this->extractMapping($value);
            } elseif (is_string($key)) {
                $id = $this->_mappingCounter++;
                $this->_mapping[$id] = $key;
                $data[$id] = $value;
                unset($data[$key]);
            }
        }
        return $data;
    }

    public function renderReport(int $report_id)
    {
        $states = [0 => __('- všechny stavy -')] + RequestState::getLabels();

        $report = $this->Reports->getWithOrganization($report_id, OrgDomainsMiddleware::getCurrentOrganizationId(), ['ReportColumns']);
        $appeals_temp = $this->Requests->Appeals->find(
            'list',
            [
                'conditions' => [
                    'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ]
            ]
        )->order(
            ['`Appeals`.`open_from`' => 'DESC', '`Appeals`.`name`' => 'ASC']
        )->toArray();
        $appeals = [0 => __('- všechny výzvy -')] + $appeals_temp;

        $programs = $this->Programs->find(
            'all',
            [
                'conditions' => [],
                'contain' => [
                    'ParentPrograms',
                    'ChildPrograms',
                    'Realms',
                    'Realms.Fonds',
                    'Appeals',
                    'EvaluationCriteria',
                ],
            ]
        )->order(
            ['Programs.realm_id' => 'ASC',]
        )->matching(
            'Realms.Fonds',
            function (Query $query) {
                return $query->where(
                    ['Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),]
                );
            }
        )->distinct('Programs.id')
            ->filter(function ($value) {
                return empty($value->child_programs);
            })->map(function ($value) {
                /**@var $value Program */
                return [
                    'value' => $value->id,
                    'text' => $value->name,
                    'data-section' => $value->getDataSection(),
                ];
            });

        $request_states = [0 => __('- všechny výzvy -')] + $this->Requests->RequestStates->find('list')->order(['id' => 'ASC'])->toArray();

        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Requests.request_state_id' => $this->getRequest()->getSession()->read(self::SESSION_LAST_STATE_ID) ?? RequestState::STATE_SUBMITTED,
        ];
        $session_programs = $this->getRequest()->getSession()->read(self::SESSION_LAST_PROGRAMS);
        if (!empty($session_programs) && is_array($session_programs)) {
            $conditions['Requests.program_id IN'] = $session_programs;
        }
        $session_appeal = $this->getRequest()->getSession()->read(self::SESSION_LAST_APPEAL_ID);
        if (!empty($session_appeal) && in_array($session_appeal, array_keys($appeals))) {
            $conditions['Requests.appeal_id'] = $session_appeal;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $appeal_id = $this->getRequest()->getData('appeal_id');
            if (isset($appeal_id) && is_numeric($appeal_id)) {
                $conditions['Requests.appeal_id'] = $appeal_id;
            }
            $program_ids = $this->getRequest()->getData('program_ids');

            if (!empty($program_ids)) {
                $conditions['Requests.program_id IN'] = $program_ids;
            } else {
                $this->getRequest()->getSession()->write(self::SESSION_LAST_PROGRAMS, []);
                unset($conditions['Requests.program_id IN']);
            }
            $request_state = intval($this->getRequest()->getData('request_state_id'));
            if (in_array($request_state, array_keys($request_states), true)) {
                $conditions['Requests.request_state_id'] = $request_state;
            }
        } else if (!empty($appeals)) {
            $first_appeal_id = array_keys($appeals)[0];
            $conditions['Requests.appeal_id'] = $first_appeal_id;
        }

        $greather_appeal_id = '';
        if ($conditions['Requests.appeal_id'] <= 0) {
            $greather_appeal_id = ' >=';
            $conditions['Requests.appeal_id' . $greather_appeal_id] = $conditions['Requests.appeal_id'];
            unset($conditions['Requests.appeal_id']);
        }

        $greather_request_state_id = '';
        if ($conditions['Requests.request_state_id'] <= 0) {
            $greather_request_state_id = ' >=';
            $conditions['Requests.request_state_id' . $greather_request_state_id] = $conditions['Requests.request_state_id'];
            unset($conditions['Requests.request_state_id']);
        }

        $modifiedRequest = $this->getRequest()
            ->withData('appeal_id', $conditions['Requests.appeal_id' . $greather_appeal_id])
            ->withData('request_state_id', $conditions['Requests.request_state_id' . $greather_request_state_id]);

        $this->getRequest()->getSession()->write(self::SESSION_LAST_APPEAL_ID, $conditions['Requests.appeal_id' . $greather_appeal_id]);
        $this->getRequest()->getSession()->write(self::SESSION_LAST_STATE_ID, $conditions['Requests.request_state_id' . $greather_request_state_id]);

        if (isset($conditions['Requests.program_id IN']) && !empty($conditions['Requests.program_id IN'])) {
            $this->getRequest()->getSession()->write(self::SESSION_LAST_PROGRAMS, $conditions['Requests.program_id IN']);
            $modifiedRequest = $modifiedRequest->withData('program_ids', $conditions['Requests.program_id IN']);
        }
        $this->setRequest($modifiedRequest);

        $requests = $this->Requests->find('all', [
            'conditions' => $conditions,
            'contain' => [
                'RequestBudgets',
                'RequestFilledFields',
                'Identities',
                'Programs.ParentPrograms',
                'Programs.Forms.FormFields',
                'Programs.Forms.FormSettings',
                'Programs.PaperForms.FormFields',
                'Programs.PaperForms.FormSettings'
            ]
        ]);

        $show_agendio_headers = in_array($this->getRequest()->getData('agendio_report_id'), AgendioReport::KNOWN_REPORTS);
        $agendio_report = null;
        $agendio_presets = [];
        if ($show_agendio_headers) {
            $agendio_report = $this->AgendioReports->get($this->getRequest()->getData('agendio_report_id'), [
                'contain' => [
                    'AgendioColumns'
                ]
            ]);
            foreach ($agendio_report->agendio_columns as $agendio_column) {
                $key = $agendio_column->getPresetStorageKey();
                if ($key) {
                    $agendio_presets[$key] = OrganizationSetting::getSetting($key);
                }
            }
        }

        $this->set(compact('agendio_report', 'show_agendio_headers', 'agendio_presets'));

        $this->set(compact('report', 'requests', 'appeals', 'programs', 'request_states', 'states'));
        $this->set('crumbs', [__('Sestavy') => 'admin_reports']);
    }

    public function agendioPresets()
    {
        /** @var AgendioReport[] $reports */
        $reports = $this->AgendioReports->find('all', [
            'contain' => [
                'AgendioColumns' => function (Query $query) {
                    return $query->where(['AgendioColumns.can_have_organization_preset' => true]);
                }
            ]
        ]);

        $current_presets = [];
        foreach ($reports as $report) {
            foreach ($report->agendio_columns as $column) {
                $key = OrganizationSetting::getAgendioPresetKey($column->agendio_report_id, $column->export_name);
                $current_presets[$key] = OrganizationSetting::getSetting($key);
            }
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $has_errors = false;
            $flat = Hash::flatten($this->getRequest()->getData());
            foreach ($flat as $preset_storage_key => $preset_value) {
                $db_conditions = [
                    'OrganizationSettings.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'OrganizationSettings.name' => $preset_storage_key
                ];
                if (empty($preset_value)) {
                    // delete obsolete value from storage, if exists
                    $this->Organizations->OrganizationSettings->deleteAll($db_conditions);
                } else {
                    /** @var OrganizationSetting $existing_preset */
                    $existing_preset = $this->Organizations->OrganizationSettings->find('all', [
                        'conditions' => $db_conditions
                    ])->first();
                    if (empty($existing_preset)) {
                        // create entity if it does not exist in database yet
                        $existing_preset = $this->Organizations->OrganizationSettings->newEntity([
                            'name' => $preset_storage_key,
                            'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                            'value' => $preset_value,
                        ]);
                    } else {
                        $existing_preset->set('value', $preset_value);
                    }
                    if (!$this->Organizations->OrganizationSettings->save($existing_preset)) {
                        $this->Flash->error(__('Formulář obsahuje chyby'));
                        $this->Flash->error($existing_preset->getFirstError());
                        $has_errors = true;
                    } else {
                        Log::debug(json_encode($existing_preset));
                    }
                }
            }
            if (!$has_errors) {
                $this->Flash->success(__('Uloženo úspěšně'));
            }
            OrgDomainsMiddleware::flushCache();
            $this->redirect($this->getRequest()->getRequestTarget());
        }

        $this->set(compact('reports', 'current_presets'));
        $this->set('crumbs', [__('Sestavy') => 'admin_reports']);
    }

    private function fixColumnOrder(ReportColumn $column)
    {
        $columns = $this->Reports->ReportColumns->find('list', [
            'conditions' => [
                'report_id' => $column->report_id,
            ],
            'keyField' => 'order',
            'valueField' => 'id',
        ])->toArray();
        if (empty($column->order)) {
            // automatically prefill after last existing column
            $column->order = max(array_keys($columns)) + 1;
            return;
        }

        if (!isset($columns[$column->order])) {
            // no need to modify
            return;
        }

        if ($column->id === $columns[$column->order]) {
            // update of entity without changing order
            return;
        }

        if ($column->hasErrors()) {
            // won't touch ORM/DB if the entity is not save-able
            return;
        }

        $this->Reports->ReportColumns->getConnection()->transactional(function () use ($column, $columns) {
            $updates = [];
            foreach ($columns as $originalOrder => $originalColumnId) {
                if ($originalOrder < $column->order) {
                    // skip preceding columns
                    continue;
                }
                $updates[$originalOrder + 1] = $originalColumnId;
            }
            krsort($updates);
            foreach ($updates as $newOrder => $originalColumnId) {
                $this->Reports->ReportColumns->updateAll(
                    ['order' => $newOrder],
                    ['ReportColumns.id' => $originalColumnId],
                );
            }
            $this->Reports->ReportColumns->save($column);
        });
    }
}
