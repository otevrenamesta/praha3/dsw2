<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Model\Entity\OrganizationSetting;
use Cake\Controller\Component;

/**
 * Per RDM web services 1.17 specification
 * http://eagri.cz/public/web/file/532415/RDM_web_services_1._17.rar
 */
class EagriRDMComponent extends Component
{
    public const DOMAIN_TEST = 'eagritest.cz';
    public const DOMAIN_PRODUCTION = 'eagri.cz';
    public const API_PATH = 'ssl/nosso-app/EPO/WS/v2Online/vOKOsrv.ashx';
    public const WSDL_PATH = 'ssl/nosso-app/EPO/WS/Online/vOKOsrv.aspx?SERVICEID=';

    public const SERVICE_SEARCH_SUBJECT = 'RDM_SUS01A';
    public const SERVICE_INSERT_SUBSIDY = 'RDM_POI01B';
    public const SERVICE_EDIT_DELETE_SUBSIDY = 'RDM_EDI01B';
    public const SERVICE_PUBLIC_QUERY = 'RDM_PUB01B';

    public function useTestEnvironment(): bool
    {
        return OrganizationSetting::getSetting(OrganizationSetting::EAGRI_RDM_TEST_ENVIRONMENT) === true;
    }

    public function getEnvironmentUrl(): string
    {
        return 'https://' . ($this->useTestEnvironment() ? self::DOMAIN_TEST : self::DOMAIN_PRODUCTION);
    }

    public function getAPIUrl(): string
    {
        return $this->getEnvironmentUrl() . '/' . self::API_PATH;
    }

    public function getWSDLUrl(string $service): string
    {
        return $this->getEnvironmentUrl() . '/' . self::WSDL_PATH . $service;
    }

    public function getSearchRequest()
    {
        $wsdl_path = $this->getWSDLUrl(self::SERVICE_PUBLIC_QUERY);

        return [
            $wsdl_path,
        ];
    }

}