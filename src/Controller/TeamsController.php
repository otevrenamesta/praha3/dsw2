<?php

namespace App\Controller;

use App\Form\PaperRequestForm;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestType;
use App\Model\Entity\User;
use App\Model\Entity\Users;
use App\Model\Table\FondsTable;
use App\Model\Table\HistoryIdentitiesTable;
use App\Model\Table\RequestsTable;
use App\Model\Table\TeamsMessagesTable;
use App\Traits\AppealsAwareTrait;
use App\Traits\IndexedArrayTrait;
use App\Traits\PdfDownloadTrait;
use Cake\Database\Query;
use Cake\Datasource\EntityInterface;
use Cake\Filesystem\File;
use Cake\Http\Exception\NotFoundException;
use Cake\Log\Log;
use DateTimeInterface;
use function Laminas\Diactoros\createUploadedFile;

/**
 * To perform all lifecycle tasks related to requests
 *
 * @property-read RequestsTable $Requests
 * @property-read FondsTable $Fonds
 * @property HistoryIdentitiesTable $HistoryIdentities
 */
class TeamsController extends AppController {
    use AppealsAwareTrait;
    use PdfDownloadTrait;
    use IndexedArrayTrait;

    public const NOTICE_AFTER_SENDING_EMAIL =
    'E-mail informující o změně stavu žádosti byl odeslán';

    public function initialize() {
        parent::initialize();
        $this->loadModel('Fonds');
        $this->loadModel('HistoryIdentities');
        $this->loadModel('Requests');
        $this->loadModel('TeamsMessages');
        $this->loadModel('Users');
        $this->loadModel('Identities');
    }

    /**
     * @param User|null $user
     * @return bool
     */
    public function isAuthorized($user = null): bool {
        $authorized = parent::isAuthorized($user) && $user->hasAtLeastOneTeam();

        if (!$authorized && $user->isEconomicsManager()) {
            $economics_allowed_actions = ['requestDetailReadOnly', 'downloadAttachment'];
            $requested_action = $this->getRequest()->getParam('action');
            if (in_array($requested_action, $economics_allowed_actions, true)) {
                $authorized = true;
            }
        }

        return $authorized;
    }

    public function index() {
    }

    public function indexMessages() {
        $messages = $this->TeamsMessages->find('all', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                'OR' => [
                    'user_id' => $this->getCurrentUserId(),
                    'recipient_user_id' => $this->getCurrentUserId(),
                ]
            ],
            'order' => ['created' => 'DESC'],
            'limit' => 100
        ])->toArray();

        $users = [];
        $all_users = $this->Users->find(
            'all',
            [
                'contain' => [
                    'UsersToRoles',
                    'Teams',
                ],
            ]
        )->toArray();

        $idOrg = OrgDomainsMiddleware::getCurrentOrganizationId();
        foreach ($all_users as $key => $val) {
            if ($val->hasAtLeastOneTeam()) {
                $users[$val->id] = trim(strtolower($val->email));
            }
        }
        asort($users);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $text = $this->getRequest()->getData('text');
            $recipient = $this->getRequest()->getData('recipient');

            /** @var TeamsMessages $message */
            $message = $this->TeamsMessages->newEntity([
                'message' =>  $text,
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                'recipient_user_id' => (int)$recipient,
                'user_id' =>  $this->getCurrentUserId(),
            ]);

            if ($this->TeamsMessages->save($message)) {
                $this->Flash->success(__('Zpráva byla odeslána'));
                $this->redirect(['action' => 'indexMessages']);
            } else {
                $this->Flash->error(__('Zprávu není možné odeslat'));
                $this->redirect($this->referer());
            }
        }

        $this->set('messages', $messages);
        $this->set('users', $users);
        $this->set('user', $this->getCurrentUserId());
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexMessagesDelete(int $id) {
        // TODO: add message delete
        $this->Flash->success(__('Zpráva byla smazána'));
        $this->redirect(['action' => 'indexMessages']);
    }

    public function indexFormalControl() {
        $allowedStates = [
            RequestState::STATE_NEW,
            RequestState::STATE_READY_TO_SUBMIT,
            RequestState::STATE_SUBMITTED,
            RequestState::STATE_FORMAL_CHECK_APPROVED,
            RequestState::STATE_FORMAL_CHECK_REJECTED,
            RequestState::STATE_SUBMIT_LOCK,
        ];

        $requests = $this->getSuitableRequests(
            $this->getCurrentUser()->getProgramIdsForFormalControl(),
            $allowedStates
        );

        $appeals = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $this->set(compact('appeals'));

        $this->set('requests', $requests);
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function formalControlDeletePaperRequest(int $request_id) {
        $allowed_program_ids = $this->getCurrentUser()->getProgramIdsForFormalControl();
        $allowed_state_ids = [
            RequestState::STATE_NEW,
            RequestState::STATE_READY_TO_SUBMIT
        ];
        $request = $this->getSuitableRequests($allowed_program_ids, $allowed_state_ids, $request_id)->firstOrFail();
        if ($this->Requests->delete($request)) {
            $this->Flash->success(__('Papírová žádost byla úspěšně smazána'));
            $this->redirect(['action' => 'indexFormalControl']);
        } else {
            $this->Flash->error(__('Žádost není možné smazat'));
            $this->Flash->error(json_encode($request->getErrors()));
            $this->redirect($this->referer());
        }
    }

    public function formalControlUnlockWithCode(int $request_id) {
        $request = $this->getRequestOrFail(
            $this->getCurrentUser()->getProgramIdsForFormalControl(),
            [RequestState::STATE_SUBMIT_LOCK],
            $request_id
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {

            if ((!OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, false))) {
                $codeVerified = $request->unlockWithoutVerificationCode($this->getCurrentUserId()); // Bypass verification code checking
            } else {
                $code = $this->getRequest()->getData('verification_code');
                $codeVerified = false;
                if (!empty($code)) {
                    $codeVerified = $request->unlockWithVerificationCode($code, $this->getCurrentUserId());
                }
            }

            if ($codeVerified && $this->Requests->save($request)) {
                $this->Flash->success(__('Žádost byla úspěšně odemčena'));
                $this->redirect(['_name' => 'formal_control_check_request', $request->id]);
                return;
            } else {
                $this->Flash->error(__('Zadaný kód k odemčení žádosti není správný'));
            }
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Formální kontrola') => 'team_formal_control_index']);
    }

    public function formalControlAddModifyPaperRequest(int $request_id = 0) {
        // allow formal team evidence only of requests within their scope
        $allowed_program_ids = $this->getCurrentUser()->getProgramIdsForFormalControl();
        // do not allow edit of requests that are already submitted or further in the process
        $allowed_state_ids = [
            RequestState::STATE_NEW,
            RequestState::STATE_READY_TO_SUBMIT
        ];
        // selectable tree of programs
        $appeals_programs = $this->getAppealsToPrograms();

        $request = $request_id > 0 ? $this->getSuitableRequests($allowed_program_ids, $allowed_state_ids, $request_id)->firstOrFail()
            : $this->Requests->newEntity();

        if ($request->isNew()) {
            if (!OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, true)) {
                $this->Flash->error(__('Nemůžete evidovat nové papírové žádosti, možnost je v nastavení organizace zakázána'));
                $this->redirect(['action' => 'indexFormalControl']);
                return;
            }
        }

        $form = new PaperRequestForm();
        $form->setAdminAllowedPrograms($allowed_program_ids);
        $form->setProgramToAppealsMap($this->_program_id_to_appeal_id);
        $form->setRequest($request, $this->getCurrentUser());

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($form->execute($this->getRequest()->getData())) {
                if ($form->getRequest()->id > 0) {
                    $this->redirect(['_name' => 'formal_control_paper_modify', 'id' => $form->getRequest()->id]);

                    return;
                }
            }
        }

        $newOrFilled = $form->getCurrentNewOrFilledState();
        if (!$form->getRequest()->isNew() && $form->getRequest()->request_state_id !== $newOrFilled) {
            $form->getRequest()->request_state_id = $newOrFilled;
            $this->Requests->save($form->getRequest());
        }

        $this->set(compact('appeals_programs', 'form'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexComments() {
        $appeals = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $this->set(compact('appeals'));

        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexProposals() {
        $appeals = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $this->set(compact('appeals'));

        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForProposals(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexApprovals() {
        $appeals = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $this->set(compact('appeals'));

        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForApprovals(), [RequestState::STATE_FORMAL_CHECK_APPROVED]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexManagers() {
        $appeals = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $this->set(compact('appeals'));

        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdsForManagers(), [RequestState::STATE_REQUEST_APPROVED, RequestState::STATE_READY_TO_SIGN_CONTRACT, RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT]));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function indexPreview() {
        $conditions = [];

        $appeals_temp = $this->Requests->Appeals->find('list', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ])->toArray();
        $appeals = [0 => __('- všechny výzvy -')] + $appeals_temp;

        if (!empty($appeals)) {
            $conditions['Requests.appeal_id'] = array_keys($appeals)[0];
        }

        $requestStates = $this->Requests->RequestStates->find('list')->toArray();
        $defaultConditions = [
            RequestState::STATE_SUBMITTED,
            RequestState::STATE_FORMAL_CHECK_APPROVED,
            RequestState::STATE_REQUEST_APPROVED,
            RequestState::STATE_READY_TO_SIGN_CONTRACT,
            RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
            RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT
        ];
        $conditions['Requests.request_state_id IN'] = $defaultConditions;

        if ($this->getRequest()->is(['post', 'put', 'patch', 'get'])) {
            $request_appeal_id = intval($this->getRequest()->getQuery('appeal_id'));
            if (!empty($request_appeal_id) && in_array($request_appeal_id, array_keys($appeals), true)) {
                $conditions['Requests.appeal_id'] = $request_appeal_id;
            }
            $request_state_ids = $this->getRequest()->getQuery('state_ids');
            if (is_array($request_state_ids) && !empty($request_state_ids)) {
                $conditions['Requests.request_state_id IN'] = $request_state_ids;
            }
        }

        $this->setRequest(
            $this->getRequest()
                ->withData('appeal_id' . ($conditions['Requests.appeal_id'] <= 0 ? ' >=' : ''), $conditions['Requests.appeal_id'])
                ->withData('state_ids', $conditions['Requests.request_state_id IN'])
        );

        $withNotifications = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, true));

        if ($conditions['Requests.appeal_id'] <= 0) {
            $conditions['Requests.appeal_id >='] = '0';
            unset($conditions['Requests.appeal_id']);
        }

        $this->set('withNotifications', $withNotifications);
        $this->set('appeals', $appeals);
        $this->set('states', $requestStates);
        $this->set('requests', $this->getSuitableRequests($this->getCurrentUser()->getProgramIdesForPreview())->where($conditions));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
        $this->set('defaultStates', array_values($defaultConditions));
    }

    public function historyFromArchive(Request $request) {
        // hide HISTORIES records unless enabled for the organization
        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES)) {
            return [];
        }

        // get IČO and VAT from identities data and prepare query for searching in the HistoryIdentities
        $histories = array();
        $userIdentities = array_fill_keys([
            'HistoryIdentities.ico IN',
            'HistoryIdentities.dic IN'
        ], []);

        foreach ($request->identities as $k => $v) {
            if (empty(trim($v['value']))) {
                continue;
            }
            switch ($v['name']) {
                case Identity::PO_BUSINESS_ID:
                    $userIdentities['HistoryIdentities.ico IN'][] = $v['value'];
                    break;

                case Identity::FO_VAT_ID:
                case Identity::PO_VAT_ID:
                    $userIdentities['HistoryIdentities.dic IN'][] = $v['value'];
                    break;
            }
        }

        if (!empty(array_filter($userIdentities))) {
            // get list of archived history
            $archive = $this->HistoryIdentities->find('all', [
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'OR' => array_filter($userIdentities)
                ],
                'contain' => [
                    'Histories',
                ],
            ])->toArray();

            $orgRange = (int)OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true);
            $yearsRange = range((int)date('Y'), (int)date('Y') - max(0, $orgRange));
            // old items filtered by year
            if (isset($archive[0]) && isset($archive[0]->histories)) {
                foreach ($yearsRange as $year) {
                    foreach ($archive[0]->histories as $v) {
                        if ((int)$v['year'] === $year) {
                            $histories[] = (object)
                            [
                                'id' => null,
                                'amount_czk' => $v['czk_amount'],
                                'fiscal_year' => $v['year'],
                                'public_income_source_id' => null,
                                'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
                                'project_name' => $v['project_name']
                            ];
                        }
                    }
                }
            }
        }

        return $histories;
    }

    public function getRequestPdfView(int $id) {
        $this->getRequestPdf($id, false);
    }

    public function getRequestPdf(int $id, bool $forceDownload = true) {
        $request = $this->getSuitableRequests($this->getCurrentUser()->getAllTeamProgramIds(), [], $id)->first();
        $request->user->public_income_histories = array_merge(
            (isset($request->user->public_income_histories) ? $request->user->public_income_histories : []),
            ($request instanceof Request ? $this->historyFromArchive($request) : [])
        );

        $this->set(compact('request'));
        $this->viewBuilder()->setTemplate('/UserRequests/get_pdf');
        $this->renderAsPdf(sprintf('zadost-%d.kopie.pdf', $request->id), $forceDownload);
    }

    public function requestDetailReadOnly(int $id) {
        $request = $this->justGetRequestNoFlippingFail($id);

        if (!$request) {
            // TODO: More informative program names
            $this->render('request_exception');
            return $this->getResponse()->withStatus(404);
        } else {
            $request->loadLogs();

            /** @var RequestLog $newLog */
            $newLog = $request->getTableLocator()->get('RequestLogs')->newEntity();
            $newLog->executed_by_user_id = $this->getCurrentUser()->id;
            $newLog->is_history = true;
            $newLog->is_locked = true;
            $newLog->request_id = $request->id;
            $newLog->request_state_id = $request->request_state_id;

            $request->request_logs[] = $newLog;
            $request->setDirty('request_logs');
            $this->Requests->save($request);
        }

        $request->user->public_income_histories = array_merge(
            (isset($request->user->public_income_histories) ? $request->user->public_income_histories : []),
            $this->historyFromArchive($request)
        );

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    private function justGetRequestNoFlippingFail(int $id): \App\Model\Entity\Request|array|null {

        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];

        $conditions['Requests.id'] = $id;

        $allowed_program_ids = $this->getCurrentUser()->getAllTeamProgramIds();

        if (empty($allowed_program_ids) && !$this->getCurrentUser()->isEconomicsManager()) {
            // read-only is for team members only
            throw new NotFoundException(__('Nemáte aktivován přístup k této žádosti'));
            return $this->getResponse()->withStatus(404);
        }

        if (!$this->getCurrentUser()->isEconomicsManager() && !empty($allowed_program_ids)) {
            $conditions['OR'] = [
                'Programs.id IN' => $allowed_program_ids,
                'ParentPrograms.id IN' => $allowed_program_ids,
            ];
        }

        $contain = [
            'Identities',
            'Appeals',
            'Programs',
            'Programs.EvaluationCriteria',
            'Programs.ParentPrograms',
            'Programs.ParentPrograms.EvaluationCriteria',
            'Programs.Realms',
            'Programs.ParentPrograms.Realms',
            'Programs.Realms.Fonds',
            'Programs.ParentPrograms.Realms.Fonds',
            'Users',
            'RequestBudgets',
            'Evaluations',
            'Evaluations.EvaluationCriteria',
            'Files',
        ];

        if ($id > 0) {
            $contain['RequestBudgets'] = [
                'OwnSources',
                'Incomes',
                'Costs',
                'OtherSubsidies',
            ];
            $contain['Users'] = [
                'PublicIncomeHistories',
                'PublicIncomeHistories.PublicIncomeSources',
            ];
            $contain[] = 'Programs.Forms';
            $contain[] = 'Programs.Forms.FormFields';
            $contain[] = 'Programs.ParentPrograms.Forms';
            $contain[] = 'Programs.ParentPrograms.Forms.FormFields';
        }

        $request = $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        )->first();
        return $request;
    }

    public function requestDetailNotificationsOnly(int $id) {
        $withNotifications = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, true));

        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];

        $conditions['Requests.id'] = $id;

        $allowed_program_ids = $this->getCurrentUser()->getAllTeamProgramIds();
        if (!$withNotifications || empty($allowed_program_ids) && !$this->getCurrentUser()->isEconomicsManager()) {
            // read-only is for team members only
            return $this->getResponse()->withStatus(404);
        }
        if (!empty($allowed_program_ids)) {
            $conditions['OR'] = [
                'Programs.id IN' => $allowed_program_ids,
                'ParentPrograms.id IN' => $allowed_program_ids,
            ];
        }

        $contain = [
            'Identities',
            'Appeals',
            'Programs',
            'Programs.ParentPrograms',
        ];

        $request = $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        )->firstOrFail();


        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function downloadAttachment(int $request_id, int $file_id, ?int $form_id = null, ?int $field_id = null) {
        //$request = $this->getRequestOrFail($this->getCurrentUser()->getAllTeamProgramIds(), [], $request_id);
        $request = $this->justGetRequestNoFlippingFail($request_id);

        $_foundFile = null;

        if (is_object($request)) {
            if ($form_id < 1) {
                $_foundFile = $request->findAttachment($file_id);
            } else {
                $form = $request->getFormById($form_id);
                if ($form) {
                    $formController = $form->getFormController($request);
                    $field = $formController->getFieldById($field_id);
                    $file = $formController->getCurrentlyAssignedFileOrNull($field);
                    if ($file) {
                        $_foundFile = $file;
                    }
                }
            }

            if ($_foundFile) {
                $_file = new File($_foundFile->getRealPath());
                if ($_file->exists()) {
                    return $this->getResponse()->withFile(
                        $_file->path,
                        [
                            'name' => urlencode($_foundFile->original_filename),
                            'download' => true,
                        ]
                    );
                }
            }
        }
        return $this->getResponse()->withStatus(404);
    }

    public function formalControlCheckRequest(int $id) {

        $request = $this->getRequestOrFail(
            $this->getCurrentUser()->getProgramIdsForFormalControl(),
            [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_SUBMITTED, RequestState::STATE_FORMAL_CHECK_APPROVED, RequestState::STATE_FORMAL_CHECK_REJECTED],
            $id
        );
        $request_state_id = intval($this->getRequest()->getData('request_state_id'));
        $changed_back = $request_state_id > 0 && $request_state_id !== $request->request_state_id;

        $requires_pre_evaluation = $request->program->requires_pre_evaluation;


        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $form_data = $this->getRequest()->getData();

            // This is only needed for pre_evaluation (Usti)
            $went_through_pre_evaluation = false;
            if ($requires_pre_evaluation && array_key_exists('rating-result', $form_data)) {
                $evaluation['rate1'] = $form_data['rate1'];
                $evaluation['rate2'] = $form_data['rate2'];
                $evaluation['rate3'] = $form_data['rate3'];
                $evaluation['rating-comment'] = $form_data['rating-comment'];
                $evaluation['rating-result'] = $form_data['rating-result'];
                unset($form_data['rate1']);
                unset($form_data['rate2']);
                unset($form_data['rate3']);
                unset($form_data['rating-comment']);
                unset($form_data['rating-result']);
                $rating_result = $evaluation['rating-result'];
                $rating_comment = $evaluation['rating-comment'];
                $form_data['pre_evaluation'] = serialize($evaluation);
                $went_through_pre_evaluation = true;
            }

            $request = $this->Requests->patchEntity($request, $form_data);

            $lock_comment = trim($request->lock_comment);
            // patchEntity changed internal request_state_id
            $request->setCurrentStatus(
                intval($this->getRequest()->getData('request_state_id')),
                $this->getCurrentUserId(),
                $changed_back
            );

            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));

                if (!$went_through_pre_evaluation) {
                    if (RequestState::canUserEditRequest($request->request_state_id) && $request->lock_when instanceof DateTimeInterface) {
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id . ', lock_when: ' . $request->lock_when->nice());
                        if ($this->sendMailSafe('User', 'requestFormalControlReturned', [$request->user, $request])) {
                            $request->addMailLog($this->getCurrentUser()->id, __('Vaše žádost byla vrácena k opravě chyb'), '');
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                    } elseif ((int)$request->request_state_id === (int)RequestState::STATE_FORMAL_CHECK_REJECTED) {
                        $request->lock_comment = $lock_comment;
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                        if ($this->sendMailSafe('User', 'requestFormalControlRejected', [$request->user, $request])) {
                            $request->addMailLog(
                                $this->getCurrentUser()->id,
                                __('Žádost byla vyřazena pro nesplnění podmínek'),
                                ($request->lock_comment ? __('Odůvodnění: ') . $request->lock_comment : '')
                            );
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                    } elseif (RequestState::canUserCheckApproved($request->request_state_id)) {
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                        if ($this->sendMailSafe('User', 'requestNotice', [$request->user, $request])) {
                            $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                    }
                } else { // Send different email if it went through pre-evaluation and was rejected
                    if ((int)$request->request_state_id === (int)RequestState::STATE_FORMAL_CHECK_REJECTED) {
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                        if ($this->sendMailSafe('User', 'requestFormalControlRejected', [$request->user, $request])) {
                            $request->addMailLog(
                                $this->getCurrentUser()->id,
                                __('Žádost byla vyřazena pro nedostatečné bodové hodnocení'),
                                ($rating_comment ? __('Odůvodnění: ') . $rating_comment : '')
                            );
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                    } elseif (RequestState::canUserCheckApproved($request->request_state_id)) {
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                        if ($this->sendMailSafe('User', 'requestNotice', [$request->user, $request])) {
                            $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                    }
                }
                $this->redirect(['action' => 'indexFormalControl', '#' => $id]);
            }
        }

        $this->set(compact('request', 'requires_pre_evaluation'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Formální kontrola') => 'team_formal_control_index']);
    }

    public function formalControlSendEmail(int $id) {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForFormalControl(), [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], $id);
        if (RequestState::canUserEditRequest($request->request_state_id) && $request->lock_when instanceof DateTimeInterface) {
            $this->sendMailSafe('User', 'requestFormalControlReturned', [$request->user, $request]);
            $this->Flash->success(__('E-mail byl odeslán'));
        } else {
            $this->Flash->error(__('Tato žádost je již uzamčena, e-mail není možné odeslat'));
        }

        return $this->redirect($this->getRequest()->referer());
    }

    public function formalControlManualSubmit(int $id) {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForFormalControl(), [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_SUBMITTED], $id);
        if (!OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)) {
            $this->Flash->error(__('Vaše organizace nemá tuto funkci povolenou'));
            $this->redirect(['action' => 'indexFormalControl']);

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $request->setCurrentStatus(RequestState::STATE_SUBMITTED, $this->getCurrentUserId());
            if ($this->Requests->save($request)) {
                if ($request->request_type_id === RequestType::PAPER_REQUEST) {
                    $request->setCurrentStatus(RequestState::STATE_FORMAL_CHECK_APPROVED, $this->getCurrentUserId());
                    if (!$this->Requests->save($request)) {
                        Log::debug(json_encode($request->getErrors()));
                    }
                }
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexFormalControl', '#' => $id]);
            } else {
                $this->Flash->error($request->getFirstError());
                Log::debug(json_encode($request->getErrors())); //dd($request);
            }
        }
        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Formální kontrola') => 'team_formal_control_index']);
    }

    public function commentsEvaluate(int $id) {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForComments(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);
        $evaluation = $this->Requests->Evaluations->newEntity();
        foreach ($request->evaluations as $candidate) {
            if ($candidate->user_id === $this->getCurrentUser()->id) {
                $evaluation = $candidate;
                $evaluation->set('criterium', unserialize($evaluation->responses));
            }
        }

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->getEvaluationCriteriumId(),
                ],
            ]
        )->toArray();
        $parentCriterium = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'id' => $request->getEvaluationCriteriumId(),
                ],
            ],
        )->first();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $this->Requests->loadInto($request, ['Appeals.AppealsToPrograms']);

            if ($this->getRequest()->getData('reference_number')) {
                // save request update
                $request->reference_number = $this->getRequest()->getData('reference_number');
                if ($this->Requests->save($request)) {
                    $this->Flash->success(__('Hotovo'));
                    $this->redirect(['action' => 'indexComments', '#' => $id]);
                }
            } elseif ($request->appeal->canCommentsSubmitEvaluation($request)) {
                $evaluation->user_id = $this->getCurrentUser()->id;
                // preference on closest to request
                $evaluation->evaluation_criteria_id = $request->getEvaluationCriteriumId();
                $evaluation->request_id = $request->id;
                $points = $this->getRequest()->getData('criterium');
                $evaluation->set('responses', serialize($points));
                if (is_array($points)) {
                    $evaluation->points = array_sum($points);
                } else {
                    $evaluation->points = 0;
                }
                $evaluation->comment = $this->getRequest()->getData('comment');
                $request->addStatusLog($this->getCurrentUser()->id, __('Hodnotitelé - uložení hodnocení'));
                $this->Requests->save($request);

                if ($this->Requests->Evaluations->save($evaluation)) {
                    $this->Flash->success(__('Hotovo'));
                    $this->redirect(['action' => 'indexComments', '#' => $id]);
                } else {
                    $this->Flash->error(__('Formulář obsahuje chyby'));
                }
            } else {
                $this->Flash->error(__('Již proběhla uzávěrka hodnocení v tomto programu, není možné vkládat nová hodnocení nebo upravovat existující'));
            }
        }

        $this->set(compact('request', 'evaluation', 'parentCriterium', 'criteria'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Hodnotitelé') => 'team_evaluators_index']);
    }

    public function proposalsEdit(int $id) {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForProposals(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);
        $request_orig = clone ($request);

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->getEvaluationCriteriumId(),
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $eval_proposal = $this->getRequest()->getData('criterium');
            if (is_array($eval_proposal)) {
                $request->final_evaluation = serialize($eval_proposal);
            }
            // logging & find changes
            $add_changes = '';
            $find_changes = [
                'final_subsidy_amount' => __('Návrh částky'),
                'comment' => __('Slovní hodnocení'),
                'purpose' => __('Účel dotace'),
                'final_evaluation' => __('Bodové hodnocení')
            ];
            foreach ($find_changes as $key => $value) {
                if ($request_orig[$key] !== $request[$key]) {
                    $value_orig = $request_orig[$key];
                    $value_new = $request[$key];
                    if ($key === 'final_evaluation') {
                        $value_orig = '';
                        $value_orig_serialized = !empty($request_orig[$key]) ? unserialize($request_orig[$key]) : [];
                        foreach ($value_orig_serialized  as $k => $v) {
                            $value_orig .= $k === 'sum' ? __('celkem: ') . $v : $v . ' - ';
                        }
                        $value_new = '';
                        $value_new_serialized = !empty($request[$key]) ? unserialize($request[$key]) : [];
                        foreach ($value_new_serialized  as $k => $v) {
                            $value_new .= $k === 'sum' ? __('celkem: ') . $v : $v . ' - ';
                        }
                    }
                    $add_changes .= '<p><strong>' . $value . '</strong><br />';
                    $add_changes .= __('- původní hodnota') . ': <em>' . $value_orig . '</em><br />';
                    $add_changes .= __('- nová hodnota') . ': <em>' . $value_new . '</em></p>';
                }
            }
            $request->addStatusLog(
                $this->getCurrentUser()->id,
                __('Navrhovatelé - úprava návrhu') . ($add_changes ? '<p>' . $add_changes . '</p>' : '')
            );

            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));
                $this->redirect(['action' => 'indexProposals', '#' => $id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
                $this->Flash->error($request->getFirstError());
            }
        }

        $this->set(compact('request', 'criteria'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Navrhovatelé') => 'team_proposals_index']);
    }

    public function approvalsEdit(int $id) {
        $request = $this->getRequestOrFail($this->getCurrentUser()->getProgramIdsForApprovals(), [RequestState::STATE_FORMAL_CHECK_APPROVED], $id);

        $criteria = $this->Requests->Evaluations->EvaluationCriteria->find(
            'all',
            [
                'conditions' => [
                    'EvaluationCriteria.parent_id' => $request->getEvaluationCriteriumId(),
                ],
            ]
        )->toArray();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            $eval_proposal = $this->getRequest()->getData('criterium');
            if (is_array($eval_proposal)) {
                $request->final_evaluation = serialize($eval_proposal);
            }
            $request->setCurrentStatus(RequestState::STATE_REQUEST_APPROVED, $this->getCurrentUserId());

            if ($this->Requests->save($request)) {
                $this->Flash->success(__('Hotovo'));
                Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                $this->redirect(['action' => 'indexApprovals']);
            } else {
                $this->Flash->error(__('Chyba při ukládání žádosti') . ': ' . $request->getFirstError());
            }
        }

        $this->set(compact('request', 'criteria'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Schvalovatelé') => 'team_approvals_index']);
    }

    public function managersEdit(int $id) {
        $request = $this->getRequestOrFail(
            $this->getCurrentUser()->getProgramIdsForManagers(),
            [RequestState::STATE_REQUEST_APPROVED, RequestState::STATE_READY_TO_SIGN_CONTRACT, RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT],
            $id
        );

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($this->getRequest()->getData('name') === 'STATE_FORMAL_CHECK_APPROVED') {
                $request->setCurrentStatus(RequestState::STATE_FORMAL_CHECK_APPROVED);
                if ($this->Requests->save($request)) {
                    $this->Flash->success(__('Hotovo'));
                    $this->redirect(['action' => 'indexManagers']);
                }
            } elseif ($request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
                $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
                if ($this->getRequest()->getData('STATE_READY_TO_SIGN_CONTRACT') !== null) {
                    // if resolution is final, requester is ready to sign contract
                    $request->setCurrentStatus(RequestState::STATE_READY_TO_SIGN_CONTRACT, $this->getCurrentUserId());
                } elseif ($this->getRequest()->getData('STATE_CLOSED_FINISHED') !== null) {
                    // close request if it was not approved by statutories
                    $request->setCurrentStatus(RequestState::STATE_CLOSED_FINISHED, $this->getCurrentUserId());
                }
                if ($this->Requests->save($request)) {
                    if ($request->request_state_id === RequestState::STATE_READY_TO_SIGN_CONTRACT) {
                        Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                        /*
                        // temporarily stopped
                        if ($this->sendMailSafe('User', 'requestNotice', [$request->user, $request])) {
                            $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                            $this->Requests->save($request);
                            $this->Flash->success(__(self::NOTICE_AFTER_SENDING_EMAIL));
                        }
                        */
                    }
                    $this->Flash->success(__('Hotovo'));
                }
            } elseif (($state = $this->getRequest()->getData('STATE_CONTRACT_SIGNED')) === 'STATE_CONTRACT_SIGNED'
                || ($state = $this->getRequest()->getData('STATE_REQUEST_APPROVED')) === 'STATE_REQUEST_APPROVED'
                || ($state = $this->getRequest()->getData('STATE_CLOSED_FINISHED')) === 'STATE_CLOSED_FINISHED'
                || ($state = $this->getRequest()->getData('STATE_READY_TO_SIGN_CONTRACT')) === 'STATE_READY_TO_SIGN_CONTRACT'
            ) {
                $target = null;
                switch ($state) {
                    case 'STATE_CONTRACT_SIGNED':
                        $target = RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT;
                        break;
                    case 'STATE_REQUEST_APPROVED':
                        $target = RequestState::STATE_REQUEST_APPROVED;
                        break;
                    case 'STATE_CLOSED_FINISHED':
                        $target = RequestState::STATE_CLOSED_FINISHED;
                        break;
                    case 'STATE_READY_TO_SIGN_CONTRACT':
                        $target = RequestState::STATE_READY_TO_SIGN_CONTRACT;
                        break;
                }
                if ($target != null) {
                    $request->setCurrentStatus($target, $this->getCurrentUserId());
                    if ($this->Requests->save($request)) {
                        $this->Flash->success(__('Hotovo'));

                        // Scroll down to the particular request (application) id in the index view
                        // WAS: $this->redirect(['action' => 'indexManagers']);

                        $redirect = dirname(($this->getRequest()->getPath())) . '#' . $id;
                        $this->redirect($redirect);
                    } else {
                        $this->Flash->error(__('Chyba při ukládání žádosti'));
                        $this->Flash->error($request->getFirstError());
                    }
                }
            }
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams', __('Finalizace') => 'team_managers_index']);
    }

    public function adminSectionEdit(int $id) {
        $request = $this->getSuitableRequests([], [], $id)->first();

        if (!$request || !$this->getCurrentUser()->hasFormalControlTeam()) {
            return $this->getResponse()->withStatus(404);
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $request = $this->Requests->patchEntity($request, $this->getRequest()->getData());
            if ($this->getRequest()->getData('de_minimis') === '') {
                $request->de_minimis = null;
            }
            if ($this->getRequest()->getData('public_control') === '') {
                $request->public_control = null;
            }
            if ($this->getRequest()->getData('public_control_ok') === '') {
                $request->public_control_ok = null;
            }

            foreach ($this->request->getData('filedata') as $key => $filedata) {
                if (empty($filedata['tmp_name'])) {
                    continue;
                }
                $uploadedFile = createUploadedFile($filedata);
                $file = $this->Requests->Files->newEntity();

                if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                    $file->setError('filedata', __('Nahrávání souboru se nezdařilo'));
                    $this->Flash->error(__('Nahrávání souboru se nezdařilo'));
                } else {
                    $fileMoveResult = $file->fileUploadMove($filedata, $request->user_id);

                    if (is_string($fileMoveResult)) {
                        $file->filepath = $fileMoveResult;
                        $file->original_filename = $uploadedFile->getClientFilename();
                        $file->filesize = $uploadedFile->getSize();
                        $file->user_id = $request->user_id;
                        $file->file_type = $this->request->getData('file_type-' . $key);

                        $request->files[] = $file;
                    }
                }
            }
            $request->setDirty('files');

            if ($this->Requests->save($request, ['associated' => ['Files']])) {
                $this->Flash->success(__('Změny byly uloženy'));
            } else {
                $this->Flash->error(__('Chyba při ukládání') . ' ' . $request->getFirstError());
            }

            if ($this->request->getQuery('from')) {
                $this->redirect(['action' => $this->request->getQuery('from'), 'id' => $id]);
            }
        }

        $this->set(compact('request'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function fileDownload(int $id, int $file_id) {
        $request = $this->getSuitableRequests($this->getCurrentUser()->getAllTeamProgramIds(), [], $id)->first();
        if (!$request || !$this->getCurrentUser()->hasFormalControlTeam()) {
            return $this->getResponse()->withStatus(404);
        }

        $file = $request->findAttachment($file_id);
        if (!$file) {
            return $this->getResponse()->withStatus(404);
        }
        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists()) {
            return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
        }

        return $this->getResponse()->withStatus(404);
    }

    public function attachmentDelete(int $id, int $attachment_id) {
        $request = $this->getSuitableRequests($this->getCurrentUser()->getAllTeamProgramIds(), [], $id)->first();
        if (!$request || !$this->getCurrentUser()->hasFormalControlTeam()) {
            return $this->getResponse()->withStatus(404);
        }

        $file = $request->findAttachment($attachment_id);
        if ($this->Requests->Files->delete($file)) {
            $file->deletePhysically();
        }
        $action = $this->getRequest()->getQuery('from') ?: 'adminSectionEdit';
        $this->Flash->success(__('Příloha byla smazána'));
        $this->redirect(['action' => $action, $id]);
    }

    public function sendEmail(int $request_id) {
        /** @var Request $request */
        $request = $this->getSuitableRequests($this->getCurrentUser()->getAllTeamProgramIds(), [], $request_id)->first();
        if (!$request) {
            return $this->getResponse()->withStatus(404);
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $validate = true;
            $subject = trim($this->getRequest()->getData('subject'));
            $text = trim($this->getRequest()->getData('contents'));
            if (empty($subject)) {
                $this->Flash->error(__('Předmět e-mailu musí být vyplněn'));
                $validate = false;
            }
            if (empty(strip_tags($text))) {
                $this->Flash->error(__('Text e-mailu musí být vyplněn'));
                $validate = false;
            }
            if ($validate) {
                $mailIsSent = $this->sendMailSafe('User', 'requestNotice', [
                    $request->user,
                    $request,
                    $subject,
                    $text
                ]);
                if ($mailIsSent) {
                    $request->addMailLog(
                        $this->getCurrentUser()->id,
                        $subject,
                        $text,
                    );
                    $this->Requests->save($request);
                    $this->Flash->success(__('E-mail byl úspěšně odeslán'));
                    $this->redirect(['action' => 'requestDetailReadOnly', 'id' => $request->id]);
                }
            }
        }

        // Latest emails from the latest version of identity
        $identities = $this->Identities->find('all', [
            'conditions' => [
                'Identities.user_id' => $request->user_id,
                'Identities.name LIKE' => '%mail%',
            ],
            'order' => ['version' => 'DESC'],
            'limit' => 5
        ])->toArray();

        $emails = [];
        $version = null;

        foreach ($identities as $value) {
            $version =  $version === null ? $value['version'] :  $version;
            if (
                $value['version'] === $version &&
                in_array($value['name'], ['contact.email', 'contact.email2', 'statutory.email']) &&
                !in_array($value['value'], $emails)
            ) {
                $emails[] = $value['value'];
            }
        }

        $this->set(compact('request', 'emails'));
        $this->set('crumbs', [__('Mé týmy') => 'my_teams']);
    }

    public function assemblyMeetingInterface(string $team) {
        // for price_proposal_programs => comment, purpose, final_subsidy_amount, final_evaluation
        // for price_approval_programs => comment, purpose, final_subsidy_amount, final_evaluation
        // for request_manager_programs => comment, purpose, final_subsidy_amount (not evaluations)
        // only for requests current user has team competence over
        $team_programs = $this->getCurrentUser()->getProgramIdsForTeam($team);
        $appeals_conditions = [
            'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()
        ];
        $can_set_propsals_approved = $team === 'price_approval_programs';
        $can_set_requests_finished = $team === 'request_manager_programs';

        // filter spec appeal_id:program_id,appeal_id:program_id,...
        $filter = $this->getRequest()->getParam('filter');
        if ($filter) {
            $filter_program_ids = [];
            $filter_appeal_ids = [];
            foreach (explode(",", $filter) as $spec) {
                if (empty(trim($spec))) {
                    continue;
                }
                $appeal_to_program = explode(":", $spec);
                $filter_appeal_ids[] = intval($appeal_to_program[0]);
                $filter_program_ids[] = intval($appeal_to_program[1]);
            }
            if (!empty($filter_program_ids)) {
                $team_programs = array_intersect($team_programs, $filter_program_ids);
            }
            if (!empty($filter_appeal_ids)) {
                $appeals_conditions['Appeals.id IN'] = $filter_appeal_ids;
            }
        }

        $suitable_request_states = [RequestState::STATE_FORMAL_CHECK_APPROVED];
        if ($team === 'request_manager_programs') {
            $suitable_request_states = [RequestState::STATE_REQUEST_APPROVED];
        }

        $appeals_with_programs = $this->Requests->Appeals->find('all', [
            'conditions' => $appeals_conditions,
            'contain' => [
                'Programs',
                'AppealsToPrograms',
            ]
        ])->matching('Programs', function (Query $q) use ($team_programs) {
            return $q->where(['Programs.id IN' => $team_programs]);
        })->group(['Appeals.id'])->toArray();

        /** @var Program[] $programs */
        $programs = $this->Requests->Programs->find('all', [
            'conditions' => [
                'Programs.id IN' => $team_programs
            ],
            'contain' => [
                'ParentPrograms',
            ]
        ])->toArray();
        $criteria_groups = [];
        foreach ($programs as $program) {
            if ($program->evaluation_criteria_id > 0) {
                $criteria_groups[$program->evaluation_criteria_id] = true;
            } elseif ($program->parent_program && $program->parent_program->evaluation_criteria_id > 0) {
                $criteria_groups[$program->parent_program->evaluation_criteria_id] = true;
            }
        }

        $can_show_evaluation_criteria = count(array_keys($criteria_groups)) === 1 && $team !== 'request_manager_programs';
        $can_filter = count($team_programs) > 1 || count($appeals_with_programs) > 1;

        /** @var Request[] $requests */
        $requests = $this->getSuitableRequests($team_programs, $suitable_request_states)->order(['Requests.program_id' => 'ASC', 'Requests.id' => 'ASC'])->toArray();
        $this->_indexArrayByElementIds($requests);

        $sums = [];
        foreach ($appeals_with_programs as $appeal) {
            $sums[$appeal->id] = [];
        }
        foreach ($requests as $request) {
            $sums[$request->appeal_id][$request->program_id] = ($sums[$request->appeal_id][$request->program_id] ?? 0)
                + $request->getFinalSubsidyAmount();
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            // full data requests.{id}.[comment, purpose, final_subsidy_amount]
            $data = $this->getRequest()->getData('requests');
            $successfully_updated = 0;
            $has_errors = false;
            foreach ($data ?? [] as $request_id => $request_properties) {
                if (!isset($requests[$request_id])) {
                    return $this->getResponse()->withStatus(404);
                }
                $request = $this->Requests->patchEntity($requests[$request_id], $request_properties);
                if (key_exists(Request::FIELD_REQUEST_STATE_ID, $request_properties)) {
                    $new_requested_state = intval($request_properties[Request::FIELD_REQUEST_STATE_ID]);
                    $request->setCurrentStatus($new_requested_state, $this->getCurrentUserId());
                }
                if ($request->isDirty()) {
                    if (!$this->Requests->save($request)) {
                        $this->Flash->error(sprintf("%s: %d", __('Nastala chyba při ukládání dat žádosti'), $request->id));
                        $this->Flash->error($request->getFirstError());
                        $has_errors = true;
                        break;
                    } else {
                        $successfully_updated++;
                    }
                }
            }
            if ($successfully_updated > 0) {
                $this->Flash->success(sprintf("%s: %d", __('Úspěšně upravených žádostí'), $successfully_updated));
            }
            if (!$has_errors) {
                $this->redirect($this->getRequest()->getRequestTarget());
            }
        }

        $crumbs = [__('Mé týmy') => 'my_teams'];
        $title = '';
        switch ($team) {
            case 'price_proposal_programs':
                $crumbs[__('Navrhovatelé')] = 'team_proposals_index';
                $title = __('Rozhraní pro jednání komise nebo výboru');
                break;
            case 'price_approval_programs':
                $crumbs[__('Schvalovatelé')] = 'team_approvals_index';
                $title = __('Rozhraní pro jednání komise nebo výboru');
                break;
            case 'request_manager_programs':
                $crumbs[__('Finalizace')] = 'team_managers_index';
                $title = __('Rozhraní pro evidenci rozhodnutí rady nebo zastupitelstva');
                break;
        }
        $this->set(compact(
            'can_filter',
            'can_show_evaluation_criteria',
            'appeals_with_programs',
            'team_programs',
            'programs',
            'requests',
            'crumbs',
            'title',
            'sums',
            'team',
            'can_set_propsals_approved',
            'can_set_requests_finished'
        ));
        if ($can_filter && empty($filter)) {
            $this->render('assembly_meeting_interface_filter');
        }
    }

    /**
     * @param array $allowed_program_ids
     * @param array $request_state_ids
     * @param int $id
     * @return Request|EntityInterface
     */
    private function getRequestOrFail(array $allowed_program_ids, array $request_state_ids, int $id) {
        return $this->getSuitableRequests($allowed_program_ids, $request_state_ids, $id)->firstOrFail();
    }

    /**
     * @param array $allowed_program_ids
     * @param array $request_state_ids
     * @param int|null $request_id
     * @return \Cake\ORM\Query
     */
    private function getSuitableRequests(array $allowed_program_ids, array $request_state_ids = [], ?int $request_id = null): \Cake\ORM\Query {
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];

        if (!empty($request_state_ids)) {
            $conditions['Requests.request_state_id IN'] = $request_state_ids;
        }

        if (!empty($allowed_program_ids)) {
            $conditions['OR'] = [
                'Programs.id IN' => $allowed_program_ids,
                'ParentPrograms.id IN' => $allowed_program_ids,
            ];
        } else if (!$this->getCurrentUser()->isEconomicsManager()) {
            // if allowed programs are empty, and user is not econom, throw for illegal access
            throw new NotFoundException();
        }

        if (!empty($request_id)) {
            $conditions['Requests.id'] = $request_id;
        }

        $contain = $request_id > 0 ? RequestsTable::CONTAIN_EXTENDED : RequestsTable::CONTAIN_DEFAULT;

        return $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => $contain,
            ]
        );
    }
}
