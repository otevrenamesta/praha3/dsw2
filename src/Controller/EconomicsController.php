<?php

declare(strict_types=1);

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Budget\ProjectBudgetFactory;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\Model\Table\AppealsTable;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\RequestsTable;
use App\Model\Table\SettlementsTable;
use App\Traits\PdfDownloadTrait;
use Cake\Filesystem\File;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\Number;
use Cake\Log\Log;
use OldDsw\Controller\Component\OldDswComponent;
use stdClass;
use function Laminas\Diactoros\createUploadedFile;

require_once __DIR__ . str_replace('/', DIRECTORY_SEPARATOR, '/../../vendor/myokyawhtun/pdfmerger/PDFMerger.php');
require_once __DIR__ . str_replace('/', DIRECTORY_SEPARATOR, '/../../vendor/myokyawhtun/pdfmerger/tcpdf/tcpdf.php');

use PDFMerger\PDFMerger;
use TCPDF;

/**
 * @property-read AppealsTable $Appeals
 * @property-read IdentitiesTable $Identities
 * @property-read OldDswComponent $OldDsw
 * @property-read RequestsTable $Requests
 * @property-read SettlementsTable $Settlements
 * @property-read ProgramsTable $Programs
 */
class EconomicsController extends AppController
{
    use PdfDownloadTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Appeals');
        $this->loadModel('Identities');
        $this->loadModel('Programs');
        $this->loadModel('Requests');
        $this->loadModel('Settlements');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
            $this->loadComponent('OldDsw.OldDsw');
        }
    }

    public function isAuthorized($user = null): bool
    {
        return parent::isAuthorized($user) && $this->getCurrentUser()->isEconomicsManager();
    }

    public function index()
    {
        $appeals = $this->Appeals->find(
            'list',
            [
                'conditions' => [
                    'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->order(
            ['`Appeals`.`open_from`' => 'DESC', '`Appeals`.`name`' => 'ASC']
        )->toArray();

        $states = RequestState::getLabels();
        $defaultConditions = array_keys($states);
        $conditions = [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Requests.request_state_id IN' => $defaultConditions,
        ];

        /*if (!empty($appeals)) {
            $conditions['Requests.appeal_id'] = array_keys($appeals)[0];
        }*/

        if ($this->getRequest()->is(['get', 'post', 'put', 'patch'])) {

            if ($this->getRequest()->is('post')) {
                $requests_state = $this->getRequest()->getData('requests_action');
                $states_table = [
                    RequestState::STATE_PAID_READY_FOR_SETTLEMENT => RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT
                ];
                $requests_post = is_array($this->getRequest()->getData('requests_item'))
                    ? $this->getRequest()->getData('requests_item')
                    : [];
                $requests_list = array_filter($requests_post, function ($id) {
                    return $id > 0;
                });

                $contain_inner = [
                    'Identities',
                    'Appeals',
                    'Programs',
                    'Users',
                ];
                $conditions_inner = [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'Requests.request_state_id' =>  isset($states_table[$requests_state]) ? $states_table[$requests_state] : 0,
                    'Requests.id IN' =>  $requests_list,
                ];
                $requests = count($requests_list)
                    ? $this->Requests->find(
                        'all',
                        [
                            'conditions' => $conditions_inner,
                            'contain' => $contain_inner,
                        ]
                    )->toArray()
                    : [];

                // set new state and send email
                $requests_saved = [];
                if (count($requests_list) === count($requests)) {
                    foreach ($requests as $key => $request) {
                        $request->setCurrentStatus((int)$requests_state, $this->getCurrentUserId());

                        if ($this->Requests->save($request)) {
                            Log::debug('current request ' . $request->id . ' state id ' . $request->request_state_id);
                            if ($this->sendMailSafe('User', 'requestNotice', [$request->user, $request])) {
                                $request->addMailLog($this->getCurrentUser()->id, __('Upozornění na změnu stavu žádosti'), '');
                                $this->Requests->save($request);
                                $requests_saved[] = $request->id;
                            }
                        }
                    }
                }

                // final messages
                $id_list = ' (ID: ' . implode(', ', $requests_list) . ')';
                $id_list_saved = ' (ID: ' . implode(', ', $requests_saved) . ')';

                if (count($requests_list) <= 0) {
                    $this->Flash->error(__('Žádné položky nebyly vybrány'));
                } else if (count($requests_list) !== count($requests)) {
                    $this->Flash->error(__('Nastavení neproběhlo z důvodu neoprávněné změny stavu mezi žádostmi'));
                } else if (count($requests_list) !== count($requests_saved) && count($requests_saved)  > 0) {
                    $this->Flash->warning(__('Upozornění: Nedošlo ke změnám u všech položek. U chybějícíh žádostí chybí oprávnění ke změně.'));
                    $this->Flash->success(__('Úspěšně nastaveno') . $id_list_saved);
                    $this->Flash->success(__('E-mail informující o změně stavu žádostí byl odeslán pouze u změněných žádostí'));
                } else {
                    $this->Flash->success(__('Úspěšně nastaveno') . $id_list);
                    $this->Flash->success(__('E-mail informující o změně stavu každé žádosti byl odeslán'));
                }

                $this->redirect(['action' => 'index']); // redirect
            }

            if ($this->getRequest()->is('get')) {
                $request_appeal_id = $this->getRequest()->getQuery('appeal_id');
                $request_state_ids = $this->getRequest()->getQuery('state_ids');
            } else {
                $request_appeal_id = $this->getRequest()->getData('appeal_id');
                $request_state_ids = $this->getRequest()->getData('state_ids');
            }

            if (is_array($request_state_ids)) {
                foreach ($request_state_ids as $request_state_id) {
                    if (!in_array(intval($request_state_id), RequestState::KNOWN_STATUSES, true)) {
                        throw new NotFoundException();
                    }
                }
                $conditions['Requests.request_state_id IN'] = $request_state_ids;
            }

            if (!empty($request_appeal_id) && is_numeric($request_appeal_id)) {
                if (!in_array(intval($request_appeal_id), array_keys($appeals), true)) {
                    throw new NotFoundException();
                }
                $conditions['Requests.appeal_id'] = $request_appeal_id;
            }
        }

        $requests = $this->Requests->find(
            'all',
            [
                'conditions' => $conditions,
                'contain' => [
                    'Programs',
                    'Programs.Realms',
                    'RequestBudgets',
                    'Payments',
                    'Payments.Settlements',
                ],
            ]
        );

        $this->setRequest(
            $this->getRequest()
                ->withData('appeal_id', $conditions['Requests.appeal_id'] ?? 0)
                ->withData('state_ids', $conditions['Requests.request_state_id IN'])
        );

        $withNotifications = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, true));

        $this->set(compact('appeals', 'requests', 'states', 'withNotifications'));
        $this->set('crumbs', [__('Ekonomické oddělení') => 'my_economics']);
        $this->set('defaultStates', []);
    }

    public function deletePayment(int $request_id, int $payment_id)
    {
        // to check if the request can be managed by current user
        $request = $this->Requests->get($request_id, [
            'conditions' => [
                'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ]
        ]);
        // to check if there is payment of same id as the request claims
        $payment = $this->Requests->Payments->get($payment_id, [
            'conditions' => [
                'Payments.request_id' => $request->id
            ]
        ]);
        if ($this->Requests->Payments->delete($payment)) {
            $this->Flash->success(__('Platba byla vymazána'));
            // add log
            $request->addStatusLog(
                $this->getCurrentUser()->id,
                __('Smazání platby') . ' ' .
                    Number::currency($payment->amount_czk, 'CZK') . ' #' .
                    $payment_id . ' ' .
                    ($payment->in_addition ? __('(dofinancování)') : '')
            );
            // change final_subsidy_amount or original_subsidy_amount
            if ($payment->in_addition && $request->original_subsidy_amount) {
                $newAmount = $request->final_subsidy_amount - $payment->amount_czk;
                $originalAmount = $request->original_subsidy_amount;
                $finalAmount = max(0, $newAmount, $request->original_subsidy_amount);
                $request->set('final_subsidy_amount', $finalAmount);
                if ((float)$originalAmount === (float)$finalAmount) {
                    $request->set('original_subsidy_amount', 0);
                }
            }
            // save request
            if ($this->Requests->save($request)) {
                $this->Flash->success($payment->in_addition
                    ? __('Změny ve financování podpory uloženy')
                    : __('Změny v historii plateb uloženy'));
            }
        } else {
            $this->Flash->error(__('Platba nemohla být vymazána'));
            $this->Flash->error($payment->getFirstError());
        }
        $this->redirect($this->referer());
    }

    public function additionalFinancingViewFile(int $payment_id, int $file_id)
    {
        return $this->additionalFinancingDownloadFile($payment_id, $file_id, false);
    }

    public function additionalFinancingDownloadFile(int $payment_id, int $file_id, bool $download = true)
    {
        $file = $this->Requests->Files->get($file_id);

        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists() && $download) {
            return $this->getResponse()->withFile(
                $target,
                ['name' => urlencode($file->original_filename), 'download' => true]
            );
        } elseif ($_file->exists()) {
            return $this->getResponse()
                ->withHeader('Content-type', mime_content_type($target))
                ->withHeader('Content-disposition', 'inline; filename="' . urlencode($file->original_filename) . '"')
                ->withFile(
                    $target,
                    ['name' => urlencode($file->original_filename), 'download' => false]
                );
        }

        return $this->getResponse()->withStatus(404);
    }

    public function doPayout(int $request_id)
    {
        $request = $this->Requests->get($request_id, [
            'conditions' => [
                'Requests.request_state_id IN' => [
                    RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
                    RequestState::STATE_SETTLEMENT_SUBMITTED,
                    RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                    RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                    RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
                    RequestState::STATE_CLOSED_FINISHED,
                ],
                'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => RequestsTable::CONTAIN_EXTENDED,
        ]);

        // if there is a merged request
        $mergedRequest = $request->merged_request > 0
            ? $this->Requests->get($request->merged_request, ['contain' => RequestsTable::CONTAIN_EXTENDED])
            : null;
        $mergedRequestAmount = $mergedRequest && $mergedRequest->id > 0
            ? (float)$mergedRequest->final_subsidy_amount
            : 0;

        $additionalFinancing = false;
        $unspentPayment = $request->getFinalSubsidyAmount() - $request->getPaymentsSum() + $mergedRequestAmount;
        $payment = $this->Requests->Payments->newEntity([
            'request_id' => $request->id,
            'amount_czk' => max(0, $unspentPayment),
        ]);
        $payment->setError('amount_czk', null, true);
        if ($payment->amount_czk > 0) {
            $payment->set('new_request_state_id', RequestState::STATE_PAID_READY_FOR_SETTLEMENT);
        }

        if ($this->getRequest()->is(['post', 'put'])) {
            $isRefund = $this->getRequest()->getData('is_refund');
            $amountCzk = $this->getRequest()->getData('amount_czk');
            $additionalAmount = $request->getPaymentsSum() + $amountCzk - $request->getFinalSubsidyAmount();

            $additionalFinancing = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_ADDITIONAL_FINANCING) &&
                $this->getRequest()->getData('in_addition') &&
                !$isRefund &&
                $additionalAmount > 0;

            $shouldCreatePayment = $payment->amount_czk > 0 || $isRefund;
            if ($this->getRequest()->getData('SET_REQUEST_STATE') !== null) {
                $requested_state = intval($this->getRequest()->getData('new_request_state_id'));
                $sendMail = $requested_state !== $request->request_state_id;
                $request->setCurrentStatus($requested_state, $this->getCurrentUserId());

                if ($additionalFinancing) {
                    if (!isset($request->original_subsidy_amount) || empty($request->original_subsidy_amount) || $request->original_subsidy_amount <= 0) {
                        $request->set('original_subsidy_amount', $request->getFinalSubsidyAmount());
                    }
                    $request->set('final_subsidy_amount', $request->getPaymentsSum() + $amountCzk);

                    $request->addStatusLog(
                        $this->getCurrentUser()->id,
                        __('Dofinancování (navýšení původně schválené podpory)')
                    );
                } else {
                    $payment->setDirty('in_addition');
                    $payment->setDirty('comment');
                    $payment->setDirty('files');
                }

                if (!$this->Requests->save($request)) {
                    $shouldCreatePayment = false;
                    $this->Flash->error($request->getFirstError());
                } else {
                    $this->Flash->success(__('Stav žádosti byl nastaven'));
                    if ($additionalFinancing) {
                        $this->sendMailSafe(
                            'User',
                            'requestNotice',
                            [
                                $request->loadUser()->user,
                                $request,
                                __('Dofinancování (navýšení původně schválené podpory)')
                            ]
                        );
                    } else if ($sendMail) {
                        // it works!  message is sent to the "identity" e-mail address
                        $this->sendMailSafe('User', 'requestNotice', [$request->loadUser()->user, $request]);

                        // if there is a merged request
                        if (
                            $mergedRequest &&
                            $mergedRequest->id > 0 &&
                            $request->request_state_id === RequestState::STATE_CLOSED_FINISHED
                        ) {
                            $mergedRequest->set('request_state_id', $request->request_state_id);
                            $this->Requests->save($mergedRequest);
                            $this->sendMailSafe('User', 'requestNotice', [$request->loadUser()->user, $mergedRequest]);
                            $this->Flash->success(__('Stav související žádosti byl nastaven') . ' (#' . $mergedRequest->id . ')');
                        }
                    }
                }

                if ($amountCzk > 0 && $additionalAmount && !$additionalFinancing && !$shouldCreatePayment) {
                    $payment->setError('amount_czk', __('Částka vyplacená nemůže být vyšší než částka schválená k vyplacení!'));
                }
            }

            if ($shouldCreatePayment || $additionalFinancing) {
                $modifiedData = $this->getRequest()->getData();

                foreach ($modifiedData['files'] ?? [] as $key => $filedata) {
                    $filedata = $filedata['filedata'] ?? [];
                    if (!empty($filedata)) {
                        $uploaded = createUploadedFile($filedata);
                        if ($uploaded->getError() === UPLOAD_ERR_OK) {
                            $modifiedData['files'][$key] = [
                                'user_id' => $this->getCurrentUserId(),
                                'filesize' => $uploaded->getSize(),
                                'original_filename' => h($uploaded->getClientFilename()),
                                'filepath' => $filedata['tmp_name']
                            ];
                        } else {
                            unset($modifiedData['files'][$key]);
                        }
                    }
                }
                $payment = $this->Requests->Payments->patchEntity($payment, $modifiedData);

                if ($payment->amount_czk > $unspentPayment && !$additionalFinancing && !$isRefund) {
                    $payment->setError('amount_czk', __('Částka vyplacená nemůže být vyšší než částka schválená k vyplacení.'));
                }

                $isSuccess = false;
                if ($this->Requests->Payments->save($payment)) {
                    foreach ($payment->files ?? [] as $file) {
                        $file->set('filepath', $file->fileUploadMove([
                            'error' => UPLOAD_ERR_OK,
                            'size' => $file->filesize,
                            'tmp_name' => $file->filepath
                        ], $this->getCurrentUserId()));
                        $payment->setDirty('files');
                    }

                    if ($this->Requests->Payments->save($payment)) {
                        $isSuccess = true;
                    }
                }
                if ($isSuccess || $additionalFinancing) {
                    $this->Flash->success(__('Uloženo úspěšně.'));
                    $this->redirect($this->getRequest()->getRequestTarget());
                } else {
                    $this->Flash->error(__('Formulář obsahuje chyby'));
                }
            } else if (!$shouldCreatePayment && $amountCzk > 0) {
                $payment->setError('amount_czk', __('Tuto platbu nelze provést.'));
            }
        }

        $this->set(compact('request', 'payment', 'mergedRequest'));
        $this->set('crumbs', [__('Ekonomické oddělení') => 'my_economics', h($request->name) => false]);
    }

    public function settlements()
    {
        $appeals = $this->Appeals->find(
            'list',
            [
                'conditions' => [
                    'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->order(
            ['`Appeals`.`open_from`' => 'DESC', '`Appeals`.`name`' => 'ASC']
        )->toArray();

        $states = SettlementState::getKnownStatesWithLabels();
        $defaultConditions = [
            SettlementState::STATE_FILLED_COMPLETELY,
            SettlementState::STATE_SUBMITTED_READY_TO_REVIEW,
            SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES,
        ];
        $conditions = [
            'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Settlements.settlement_state_id IN' => $defaultConditions
        ];

        if ($this->getRequest()->is(['get', 'post', 'put', 'patch'])) {
            if ($this->getRequest()->is('get')) {
                $request_appeal_id = $this->getRequest()->getQuery('appeal_id');
                $request_state_ids = $this->getRequest()->getQuery('state_ids');
            } else {
                $request_appeal_id = $this->getRequest()->getData('appeal_id');
                $request_state_ids = $this->getRequest()->getData('state_ids');
            }

            if (is_array($request_state_ids)) {
                foreach ($request_state_ids as $request_state_id) {
                    if (!in_array(intval($request_state_id), SettlementState::KNOWN_STATES, true)) {
                        throw new NotFoundException();
                    }
                }
                $conditions['Settlements.settlement_state_id IN'] = $request_state_ids;
            }

            if (!empty($request_appeal_id) && is_numeric($request_appeal_id)) {
                if (!in_array(intval($request_appeal_id), array_keys($appeals), true)) {
                    throw new NotFoundException();
                }
            }
        }

        /** @var Settlement[] $settlements */
        $settlements = $this->Settlements->find('all', [
            'conditions' => $conditions,
            'contain' => [
                'RequestsToSettlements.Requests',
                'SettlementItems',
                'Payments',
            ]
        ])->toArray();

        if (!empty($settlements) && OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            $this->Settlements->loadInto($settlements, [
                'SettlementsToOlddswRequests',
                'SettlementsToOlddswRequests.Zadosti' => [
                    'Ucet',
                    'Ucet.FyzickeOsoby',
                    'Ucet.StatutarniOrgan',
                ]
            ]);
        }

        // Filter out non matching requests
        // TODO: should be in the query

        if (isset($request_appeal_id) && trim($request_appeal_id)) {
            foreach ($settlements as $key => $settlement) {
                if (
                    !isset($settlement->requests_to_settlements) ||
                    empty($settlement->requests_to_settlements) ||
                    $request_appeal_id != $settlement->getRequest()->get('appeal_id')
                ) {
                    unset($settlements[$key]);
                };
            }
        }

        foreach ($settlements as $key => $settlement) {
            if (isset($settlement->payments) && empty($settlement->payments) && !isset($settlement->settlements_to_olddsw_requests)) {
                unset($settlements[$key]);
            }
        }

        $this->setRequest(
            $this->getRequest()
                ->withData('appeal_id', $request_appeal_id ?? 0)
                ->withData('state_ids', $conditions['Settlements.settlement_state_id IN'])
        );

        $this->set(compact('appeals', 'settlements', 'states'));
        $this->set('crumbs', [__('Ekonomické oddělení') => 'my_economics']);
        $this->set('defaultStates', array_values($defaultConditions));
    }

    public function settlementUnapprove(int $settlement_id)
    {
        $settlement = $this->Settlements->get($settlement_id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                'settlement_state_id' => SettlementState::STATE_ECONOMICS_APPROVED
            ]
        ]);
        $settlement->setCurrentStatus(SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, $this->getCurrentUserId());
        if ($this->Settlements->save($settlement)) {
            $this->Flash->success(__('Úspěšně provedeno'));
        } else {
            $this->Flash->error(__('Nebylo možné změnit stav vyúčtování'));
        }
        $this->redirect($this->referer());
    }

    public function settlementManualSubmit(int $settlement_id)
    {
        $settlement = $this->Settlements->get($settlement_id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                'settlement_state_id IN' => [
                    SettlementState::STATE_FILLED_COMPLETELY,
                    SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES,
                ]
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            $this->Settlements->loadInto($settlement, [
                'SettlementsToOlddswRequests',
                'SettlementsToOlddswRequests.Zadosti' => [
                    'Ucet',
                    'Ucet.FyzickeOsoby',
                    'Ucet.StatutarniOrgan',
                ]
            ]);
        }
        $settlement->setCurrentStatus(SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, $this->getCurrentUserId());
        if ($this->Settlements->save($settlement)) {

            // after settlement successfuly modify modify standard request as well
            $request = $settlement->getRequest();
            if ($request instanceof Request) {
                $request->setCurrentStatus(RequestState::STATE_SETTLEMENT_SUBMITTED, $this->getCurrentUserId());
                if (!$this->Requests->save($request)) {
                    Log::error('After settlement submit could not set new state to request');
                    Log::error(json_encode($request->getErrors()));
                }
            }

            $this->Flash->success(__('Provedeno úspěšně'));
        } else {
            $this->Flash->error(__('Vyúčtování nebylo možné označit jako došlé'));
            $this->Flash->error($settlement->getFirstError());
        }
        $this->redirect($this->referer());
    }

    public function settlementDetail(int $id)
    {
        $settlement = $this->Settlements->get($id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);

        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            foreach ($settlement->settlements_to_olddsw_requests as $oldDswLink) {
                $oldDswLink->set('zadosti', $this->OldDsw->getZadost($oldDswLink->zadost_id));
            }
        }
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list');

        if ($this->getRequest()->is(['post']) && $this->getRequest()->getData('approvalProcess')) {
            $items = [];
            foreach ($settlement->settlement_items as $id => $item) {
                $item->approved =  in_array($item->id, (array)$this->getRequest()->getData('approved')) ? 1 : 0;
                $item->approved_note = $this->getRequest()->getData('approved_note:' . $item->id) ?? '';
                $items[$id] = $item;
            }
            $settlement = $this->Settlements->SettlementItems->patchEntity($settlement, ['settlement_items' => $items]);

            $items2 = [];
            foreach ($settlement->settlement_attachments as $id => $item) {
                $item->approved =  in_array($item->id, (array)$this->getRequest()->getData('approved2')) ? 1 : 0;
                $item->approved_note = $this->getRequest()->getData('approved2_note:' . $item->id) ?? '';
                $items2[$id] = $item;
            }
            $settlement = $this->Settlements->SettlementAttachments->patchEntity($settlement, ['settlement_attachments' => $items2]);

            if ($this->Settlements->save($settlement)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect($this->getRequest()->getRequestTarget());
            } else {
                $this->Flash->error(__('Chyba při ukládání'));
                $this->Flash->error($settlement->getFirstError());
            }
        } else if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $settlement = $this->Settlements->patchEntity($settlement, $this->getRequest()->getData());

            $newState = intval($this->getRequest()->getData('settlement_state_id'));
            $sendMail = $newState !== $settlement->settlement_state_id;

            $settlement->setCurrentStatus($newState, $this->getCurrentUserId());
            if ($this->Settlements->save($settlement)) {

                if ($newState === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES) {
                    // related change of the Request !
                    $request = $settlement->getRequest();
                    if ($request instanceof Request) {
                        $request->setCurrentStatus(RequestState::STATE_PAID_READY_FOR_SETTLEMENT);
                        if (!$this->Requests->save($request)) {
                            Log::error(sprintf('Economics requested changes for settlement %d but request could not be set to state', $settlement->id));
                            Log::error(json_encode($request->getErrors()));
                        }
                    }
                } else if ($newState === SettlementState::STATE_SETTLEMENT_REJECTED) {
                    // related change of the Request !
                    $request = $settlement->getRequest();
                    if ($request instanceof Request) {
                        $request->setCurrentStatus(RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS);
                        if (!$this->Requests->save($request)) {
                            Log::error(json_encode($request->getErrors()));
                        }
                    }
                }

                $this->Flash->success(__('Úspěšně uloženo'));
                if ($sendMail) {
                    // it works!  message is sent to the "user" e-mail address
                    $this->sendMailSafe('User', 'settlementNotice', [$settlement]);
                }
                $this->redirect($this->getRequest()->getRequestTarget());
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
                $this->Flash->error($settlement->getFirstError());
            }
        }

        $request = $settlement->getRequest();
        $program = (object)['name' => ''];
        try {
            $program = $this->Programs->get($request->program_id);
        } catch (\Exception $e) {
            // missing program name
        }
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;

        // Add budget data
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $settlement_attachment = $this->Settlements->SettlementAttachments->newEntity();
        $this->set(compact('settlement', 'attachmentTypes', 'program', 'settlement_attachment', 'mergedRequest', 'budget'));
        $this->set('crumbs', [__('Ekonomické oddělení') => 'my_economics', __('Vyúčtování') => 'my_economic_settlements']);
    }

    public function settlementDownloadPdfView(int $id)
    {
        $this->settlementDownloadPdf($id, false);
    }

    public function settlementDownloadPdf(int $id, bool $forceDownload = true)
    {
        $settlement = $this->Settlements->get($id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            foreach ($settlement->settlements_to_olddsw_requests as $oldDswLink) {
                $oldDswLink->set('zadosti', $this->OldDsw->getZadost($oldDswLink->zadost_id));
            }
        }
        $attachmentTypes = $this->Settlements->SettlementAttachments->SettlementAttachmentTypes->find('list');

        $request = $settlement->getRequest();
        if ($request instanceof Request) {
            $request->loadUser()->user;
        }
        $identities = $this->Identities->find('all', [
            'conditions' => [
                'Identities.version' => $request->user_identity_version,
                'Identities.user_id' => $request->user_id
            ],
        ])->toArray();

        $program = (object)['name' => ''];
        try {
            $program = $this->Programs->get($request->program_id);
        } catch (\Exception $e) {
            // missing program name
        }
        $mergedRequest = $request->merged_request > 0 ? $this->Requests->get($request->merged_request) : null;

        // add budget data
        $budget = Settlement::budgetInSettlement($program, $settlement);

        $this->set(compact('settlement', 'attachmentTypes', 'identities', 'program', 'mergedRequest', 'budget'));
        $this->viewBuilder()->setTemplate('/UserRequests/settlement_pdf');
        $this->renderAsPdf(sprintf("vyuctovani-kopie-%d.pdf", $settlement->id), $forceDownload);
    }

    public function settlementViewFile(int $settlement_id, int $file_id)
    {
        return $this->settlementDownloadFile($settlement_id, $file_id, false);
    }

    public function settlementDownloadFile(int $settlement_id, int $file_id, bool $download = true)
    {
        $settlement = $this->Settlements->get($settlement_id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);
        $file = $settlement->getFileById($file_id, true);

        $target = $file->getRealPath();
        $_file = new File($target);

        if ($_file->exists() && $download) {
            return $this->getResponse()->withFile(
                $target,
                ['name' => urlencode($file->original_filename), 'download' => true]
            );
        } elseif ($_file->exists()) {
            return $this->getResponse()
                ->withHeader('Content-type', mime_content_type($target))
                ->withHeader('Content-disposition', 'inline; filename="' . urlencode($file->original_filename) . '"')
                ->withFile(
                    $target,
                    ['name' => urlencode($file->original_filename), 'download' => false]
                );
        }

        return $this->getResponse()->withStatus(404);
    }

    public function settlementPdfAttachments(int $id)
    {
        $pdfm_add = array();
        $pdfm_delete = array();
        $pdfm_tmp_dir = __DIR__ . str_replace('/', DIRECTORY_SEPARATOR, '/../../tmp/');

        $settlement = $this->Settlements->get($id, [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => SettlementsTable::ALL_CONTAINS,
        ]);
        $request = $settlement->getRequest();

        foreach ($settlement->sortedAttachments() as $attachment) {
            $file = $settlement->getFileById($attachment->file_id, true);
            $filepath = $file->getRealPath();
            $filetype = file_exists($filepath) ? mime_content_type($filepath) : null;
            $attachment->filetype = $filetype;

            $filepath_tmp = $pdfm_tmp_dir . implode('.', [$id, $attachment->file_id, time(), 'pdf']);
            $pdfm_delete[] = $filepath_tmp;
            $attachment->export = false;

            try {
                switch ($filetype) {
                    case 'application/pdf':
                        // pdf export
                        $filepath_tmp = $filepath;
                        break;
                    case 'image/jpeg':
                    case 'image/jpg':
                    case 'image/png':
                        // image export
                        $pdf = new TCPDF('portrait', 'mm', 'A4', true, 'UTF-8', false);
                        $pdf->SetPrintHeader(false);
                        $pdf->SetPrintFooter(false);
                        $pdf->SetAutoPageBreak(true, 5);
                        $pdf->AddPage();
                        $pdf->Image('@' . file_get_contents($filepath), 5, 5, 200, '', '', '', '', false, 150);
                        $pdf->Output($filepath_tmp, 'F');
                        break;
                    case 'application/vnd.oasis.opendocument.text':
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        // text export
                        $inputFiletype = strpos($filetype, 'opendocument') ? 'ODText' : 'Word2007';
                        $pdfRendererPath = __DIR__ . str_replace('/', DIRECTORY_SEPARATOR, '/../../vendor/myokyawhtun/pdfmerger/tcpdf');
                        \PhpOffice\PhpWord\Settings::setPdfRendererPath($pdfRendererPath);
                        \PhpOffice\PhpWord\Settings::setPdfRendererName('TCPDF');
                        $phpWord = \PhpOffice\PhpWord\IOFactory::load($filepath, $inputFiletype);
                        $phpWord->setDefaultFontName('DejaVu Sans, sans-serif');
                        $phpWord->save($filepath_tmp, 'PDF');
                        break;
                    case 'application/vnd.ms-excel':
                    case 'application/vnd.oasis.opendocument.spreadsheet':
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                        // spreadsheet export
                        $inputFiletype = strpos($filetype, 'opendocument')
                            ? 'Ods'
                            : (strpos($filetype, 'ms-excel') ? 'Xls' : 'Xlsx');
                        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFiletype);
                        $reader->setReadDataOnly(true);
                        $spreadsheet = $reader->load($filepath);
                        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Tcpdf');
                        $writer->save($filepath_tmp);
                        break;
                }
            } catch (\Exception $e) {
                // no pdf export
            }

            // add to list for export
            if ($filepath_tmp && file_exists($filepath_tmp)) {
                $attachment->export = true;
                $pdfm_add[] = $filepath_tmp;
            }
        }

        // table of all exported files
        $this->set(compact('settlement', 'request'));
        $html_table = $this->render('/Economics/settlement_pdf_attachments', false);
        $filepath_tmp = $pdfm_tmp_dir . implode('.', [$id, 0, time(), 'pdf']);

        try {
            // export table into pdf
            $pdf = new TCPDF('portrait', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetFont('dejavusans', '', 9);
            $pdf->SetAutoPageBreak(true, 5);
            $pdf->AddPage();
            $pdf->writeHTML($html_table, true, false, true, false, '');
            $pdf->lastPage();
            $pdf->Output($filepath_tmp, 'F');
        } catch (\Exception $e) {
            // no pdf export
        }

        if ($filepath_tmp && file_exists($filepath_tmp)) {
            $pdfm_add = array_merge([$filepath_tmp], $pdfm_add);
        }
        $pdfm_delete[] = $filepath_tmp;

        if (!empty($pdfm_add)) {
            $this->pdfMergeCompactServe($pdfm_add, sprintf('prilohy-vyuctovani-zadosti-%d.pdf', $request->id));
        }

        // delete temporary pdf files
        foreach ($pdfm_delete as $filename) {
            if (file_exists($filename)) {
                unlink($filename);
            }
        }

        return empty($pdfm_add)
            ? $this->getResponse()->withStatus(404)
            : true;
    }

    /** Tries to merge and compact pdf files using command line utility ghostscript,
     * fails back to a (buggy) myokyawhtun/pdfmerger if ghostscript is not installed
     * @param mixed $files
     * @param mixed $target_name
     *
     * @return [type]
     */

    private function pdfMergeCompactServe($files, $target_name)
    {
        // Test if ghostscript is installed
        $command = "ghostscript";
        $output = array();
        $return_var = 0;
        exec("which $command", $output, $return_var);
        $ghost_command = $output[0] ?? false;

        if ($ghost_command && ($return_var === 0)) {
            // Ghostscript is installed
            $options = ' -dQUIET -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook ';
            $file = '/tmp/' . $target_name;
            if (file_exists($file)) unlink($file);
            $cmd = $ghost_command . $options . '-sOutputFile=/tmp/' . $target_name . ' ' . implode(' ', $files);
            $handle = popen($cmd . ' 2>&1', 'r');
            $output = fread($handle, 2096);
            $output = '';
            while (!feof($handle)) {
                $output .= fread($handle, 2096);
            }
            pclose($handle); // Make sure the command has finished executing

            // Optionally wait for the file to exist
            $fileCheckAttempts = 0;
            $maxAttempts = 10; // Set a reasonable limit to avoid infinite loop

            while (!file_exists($file) && $fileCheckAttempts < $maxAttempts) {
                usleep(100000); // sleep for 100 milliseconds
                clearstatcache(); // clear the file status cache
                $fileCheckAttempts++;
            }
            if (file_exists($file)) {
                header("Content-Type: application/pdf");
                header("Content-Disposition: attachment; filename=$target_name");
                header("Content-Length: " . filesize($file));
                readfile($file);
                unlink($file);
                // Done, exit here, the file has been sent and deleted
                exit;
            } else {
                //TODO: Perhaps report the $output for inspection
            }
        }
        // We only reach this part of the code if Ghostscript not installed or has failed

        // add files to merge queue
        $pdfm = new PDFMerger;
        foreach ($files as $filename) {
            $pdfm->addPDF($filename);
        }

        // final merge and download
        $err_level = error_reporting();
        error_reporting(0);
        $pdfm->merge(
            'download',
            $target_name
        );
        error_reporting($err_level);
    }
}
