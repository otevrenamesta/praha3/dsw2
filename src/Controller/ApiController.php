<?php
declare(strict_types=1);

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Team;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\TeamsTable;
use App\Model\Table\UserRolesTable;
use App\Model\Table\UsersTable;
use App\Traits\ReportColumnsAwareTrait;
use App\Webservices\PublicService;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Laminas\Soap\AutoDiscover;
use Laminas\Soap\Server;
use Laminas\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;

/**
 * @property UserRolesTable $UserRoles
 * @property TeamsTable $Teams
 * @property UsersTable $Users
 */
class ApiController extends AppController
{
    use ReportColumnsAwareTrait;

    private string $wsUrl;
    private string $wsdlUrl;
    private string $wsNamespaceUri = 'https://dsw2.otevrenamesta.cz/ws/v1';
    private string $serviceName = 'Public';

    private function isWS(): bool
    {
        return $this->getRequest()->getParam('type') === 'ws';
    }

    private function getWSServer(): Server
    {
        $server = new Server($this->wsdlUrl, [
            'cache_wsdl' => false,
        ]);
        $server->setObject(new PublicService($this));
        $server->setReturnResponse(true);

        return $server;
    }

    public function wsdl()
    {
        $autodiscover = new AutoDiscover();
        $autodiscover->setClass(new PublicService($this))
            ->setServiceName($this->serviceName)
            ->setUri($this->wsUrl)
            ->setComplexTypeStrategy(new ArrayOfTypeSequence());

        return $this->getResponse()->withType('xml')->withStringBody($autodiscover->generate()->toXML());
    }

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->wsUrl = Router::url(['controller' => 'Api', 'action' => 'wsPublic', 'type' => 'ws'], true);
        $this->wsdlUrl = $this->wsUrl . '?wsdl';
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
        $this->Security->setConfig([
            'unlockedActions' => [
                $this->getRequest()->getParam('action')
            ]
        ]);
        $this->loadModel('UserRoles');
        $this->loadModel('Teams');
        $this->loadModel('Users');
    }

    public function wsPublic(): Response
    {
        if (!$this->isWS()) {
            return $this->getResponse()->withStatus(404);
        }

        if (isset($this->getRequest()->getQueryParams()['wsdl'])) {
            return $this->wsdl();
        }
        $response = $this->getWSServer()->handle($this->getRequest()->getBody());

        if ($response instanceof \SoapFault) {
            Log::debug('WS API error');
            Log::debug(strval($response));
            return $this->getResponse()->withStatus(400);
        } else {
            return $this->getResponse()->withType('xml')->withStringBody($response);
        }
    }

    public function roles()
    {
        $roles = $this->UserRoles->find('list');

        return $this->getResponse()->withType('json')->withStringBody(json_encode($roles));
    }

    public function managers()
    {
        /** @var User[] $managers */
        $managers = $this->Users->find('all', [
            'contain' => [
                'Teams' => TeamsTable::ALL_TEAMS,
                'UsersToRoles.UserRoles'
            ]
        ])->filter(function (User $user) {
            return !$user->isSystemsManager() && ($user->isManager() || $user->hasAtLeastOneTeam());
        });

        $overview = [];
        foreach ($managers as $manager) {
            $manager_teams = $manager->getAllTeamIds(true);
            $manager_roles = $manager->getAllRoleIds(true);
            if (empty($manager_teams) && count($manager_roles) === 1 && $manager_roles[0] === UserRole::USER_PORTAL_MEMBER) {
                // user who has no team and is only user of this portal is not a manager
                continue;
            }
            $overview[$manager->email] = [
                'id' => $manager->id,
                'teams' => $manager_teams,
                'roles' => $manager_roles,
            ];
        }

        return $this->getResponse()->withType('json')->withStringBody(json_encode($overview));
    }

    public function teams()
    {
        /** @var Team[] $teams */
        $teams = $this->Teams->find('all', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => TeamsTable::ALL_TEAMS
        ]);

        $overview = [];
        foreach ($teams as $team) {
            $team_summary = [
                'name' => $team->name,
            ];
            foreach (Team::ALL_TEAMS as $team_field) {
                if (!empty($team->{$team_field})) {
                    $team_summary[$team_field] = Hash::combine($team->{$team_field}, '{n}.id', '{n}.name');
                }
            }
            $overview[$team->id] = $team_summary;
        }

        return $this->getResponse()->withType('json')->withStringBody(json_encode($overview));
    }

    public function reportColumns()
    {
        return $this->getResponse()->withType('json')->withStringBody(json_encode($this->getReportColumnsDataOptions()));
    }

}