<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\HistoryIdentity;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\User;
use App\Model\Table\HistoriesTable;
use App\Model\Table\HistoryIdentitiesTable;
use App\Model\Table\SettlementsTable;
use App\Model\Table\IdentitiesTable;
use Cake\Filesystem\File;
use Cake\Http\Response;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use OldDsw\Controller\Component\OldDswComponent;
use Throwable;

/**
 * @property HistoriesTable $Histories
 * @property HistoryIdentitiesTable $HistoryIdentities
 * @property OldDswComponent $OldDsw
 * @property SettlementsTable $Settlements
 * @property-read RequestsTable $Requests
 * @property-read IdentitiesTable $Identities
 * @property-read PublicIncomeHistories $PublicIncomeHistories
 */
class HistoryController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Histories');
        $this->loadModel('HistoryIdentities');
        $this->loadModel('Identities');
        $this->loadModel('PublicIncomeHistories');
        $this->loadModel('Requests');
        $this->loadModel('Settlements');
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            $this->loadComponent('OldDsw.OldDsw');
        }
    }

    public function isAuthorized($user = null): bool
    {
        if (!($user instanceof User)) {
            $user = $this->Auth->user();
        }

        if (!parent::isAuthorized($user)) {
            return false;
        }

        if ($user->isHistoryManager()) {
            return true;
        }

        if (in_array($this->getRequest()->getParam('action'), ['index', 'dsw', 'dswZadosti', 'dswDetail', 'dswZadost', 'dswDownload'], true)) {
            return $user->isEconomicsManager() || $user->hasAtLeastOneTeam();
        }

        return false;
    }

    public function index()
    {
        $identities = $this->HistoryIdentities->find(
            'all',
            [
                'contain' => [
                    'Histories',
                ],
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->toArray();

        // select ico/dic only
        $existings = ['ico' => [], 'dic' => []];
        foreach ($identities as $key => $value) {
            foreach (array_keys($existings) as $k => $v) {
                if (isset($value[$v]) && !empty($value[$v]) && strlen($value[$v]) > 2) {
                    $existings[$v][] = (string)$value[$v];
                }
            }
        }

        // find requests with other ico/dic
        $identity = $this->Identities->find('all', [
            'conditions' => [
                'name IN' => [Identity::FO_FIRSTNAME, Identity::FO_SURNAME, Identity::FO_FULLNAME, Identity::PO_FULLNAME, Identity::FO_VAT_ID, Identity::PO_BUSINESS_ID, Identity::PO_VAT_ID, Identity::CONTACT_DATABOX, Identity::CONTACT_EMAIL, Identity::CONTACT_PHONE],
                'user_id IN' => $this->Requests->find()
                    ->select(['user_id'])
                    ->where(['organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()])
                    ->distinct(),
                'value IS NOT' => NULL,
                'value !=' => ''
            ]
        ])->order(['version' => 'DESC'])->toArray();

        $identity_list = [];
        foreach ($identity as $key => $value) {
            // skip empty values
            if (empty($value['value'])) {
                continue;
            }

            // create empty user if missing
            if (!isset($identity_list[$value['user_id']])) {
                $identity_list[(int)$value['user_id']] = new HistoryIdentity;
                $identity_list[$value['user_id']]['name'] = '';
                $identity_list[$value['user_id']]['id_user_id'] = $value['user_id'];
                $identity_list[$value['user_id']]['no_edit'] = true;
                $identity_list[$value['user_id']]['histories'] = [];
                $identity_list[$value['user_id']]['user_id'] = $value['user_id'];
                $identity_list[$value['user_id']]['version'] = $value['version'];
            }

            if ($identity_list[$value['user_id']]['version'] !== $value['version']) {
                continue;
            }

            switch ($value['name']) {
                case Identity::FO_VAT_ID:
                case Identity::PO_VAT_ID:
                    $identity_list[$value['user_id']]['dic'] = $value['value'];
                    break;
                case Identity::PO_BUSINESS_ID:
                    $identity_list[$value['user_id']]['ico'] = $value['value'];
                    break;
                case Identity::FO_FIRSTNAME:
                    $identity_list[$value['user_id']]['first_name'] = $value['value'];
                    break;
                case Identity::FO_SURNAME:
                    $identity_list[$value['user_id']]['last_name'] = $value['value'];
                    break;
                case Identity::FO_SURNAME:
                    $identity_list[$value['user_id']]['ico'] = $value['value'];
                    break;
                case Identity::FO_FULLNAME:
                case Identity::PO_FULLNAME:
                    $identity_list[$value['user_id']]['name'] = $value['value'];
                    break;
                case Identity::CONTACT_DATABOX:
                    $identity_list[$value['user_id']]['databox_id'] = $value['value'];
                    break;
                case Identity::CONTACT_EMAIL:
                    $identity_list[$value['user_id']]['email'] = $value['value'];
                    break;
                case Identity::CONTACT_PHONE:
                    $identity_list[$value['user_id']]['phone'] = $value['value'];
                    break;
                default:
                    $identity_list[$value['user_id']][$value['name']] = $value['value'];
                    break;
            }
        }

        // all requests
        $requests = $this->Requests->find(
            'all',
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->toArray();

        // add public_income_histories
        $income_histories = $this->PublicIncomeHistories->find(
            'all',
            [
                'conditions' => [
                    'user_id IN' => $this->Requests->find()
                        ->select(['user_id'])
                        ->where(['organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()])
                        ->distinct(),
                ],
            ]
        )->toArray();

        foreach ($income_histories as $key => $value) {
            if (isset($value['user_id'])) {
                $identity_list[$value['user_id']]['histories'][] = $value;
            }
        }

        foreach ($requests as $key => $value) {
            if (isset($identity_list[$value['user_id']]) &&  $value['final_subsidy_amount'] > 0) {
                $identity_list[$value['user_id']]['histories'][] = $value;
            }
        }

        // join identities with identity_list via ico/dic
        foreach ($identities as $key => $value) {
            foreach ($identity_list as $k => $v) {
                if (((isset($v['ico']) && isset($value['ico']) && !empty($value['ico']) &&
                        $value['ico'] === $v['ico']) ||
                        (isset($v['dic']) && isset($value['dic']) && !empty($value['dic']) &&
                            $value['dic'] === $v['dic'])
                    ) && !empty($v['histories'])
                ) {
                    $identities[$key]['databox_id'] = $v['databox_id'];
                    $identities[$key]['email'] = $v['email'];
                    $identities[$key]['phone'] = $v['phone'];
                    $identities[$key]['user_id'] = $v['user_id'];
                    $identities[$key]['histories'] = array_merge($value['histories'], $v['histories']);
                }
            }
        }

        // add unique items into identities
        foreach ($identity_list as $key => $value) {
            // skip existings or empty values
            if ((isset($value['ico']) && in_array((string)$value['ico'], $existings['ico'], true)) ||
                (isset($value['dic']) && in_array((string)$value['dic'], $existings['dic'], true)) ||
                (empty($value['name']) && empty($value['last_name']))
                // || (empty($value['ico']) && empty($value['dic']))
            ) {
                continue;
            }
            $identities[] =  $value;
        }

        // final list of identities
        $this->set(compact('identities'));
    }

    public function addModifyIdentity(int $id = 0)
    {
        $identity = $id > 0 ? $this->HistoryIdentities->get($id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]) : $this->HistoryIdentities->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $identity = $this->HistoryIdentities->patchEntity($identity, $this->getRequest()->getData());
            if (empty($identity->get('name'))) {
                $identity = $this->HistoryIdentities->patchEntity($identity, ['name' => sprintf("%s %s", $identity->first_name, $identity->last_name)]);
            }

            $identity->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();

            if ($this->HistoryIdentities->save($identity)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                $this->redirect(['action' => 'index']);

                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('identity'));
        $this->set('crumbs', [__('Žadatelé (včetně historie)') => ['action' => 'index']]);
    }

    public function detailFromUserId(int $user_id)
    {
        return $this->detail(0, $user_id);
    }

    public function detail(int $id, int $user_id = null)
    {
        if ($id > 0) {
            $identity = $this->HistoryIdentities->get($id, [
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Histories',
                ],
            ]);
        }

        if ($user_id) {
            // missing user identity
            $identity = new HistoryIdentity;
            $identity['dic'] = '';
            $identity['email'] = '';
            $identity['first_name'] = '';
            $identity['histories'] = [];
            $identity['ico'] = '';
            $identity['last_name'] = '';
            $identity['name'] = '';
            $identity['phone'] = '';
            $identity['user_id'] = $user_id;


            $current_identity = $this->Identities->find('all', [
                'conditions' => [
                    'name IN' => [Identity::FO_FIRSTNAME, Identity::FO_SURNAME, Identity::FO_FULLNAME, Identity::PO_FULLNAME, Identity::FO_VAT_ID, Identity::PO_BUSINESS_ID, Identity::PO_VAT_ID, Identity::CONTACT_DATABOX, Identity::CONTACT_EMAIL, Identity::CONTACT_PHONE],
                    'user_id' => $user_id,
                    'value IS NOT' => NULL,
                    'value !=' => ''
                ]
            ])->order(['version' => 'DESC'])->toArray();
            $latest_identity_version = isset($current_identity[0]) ? $current_identity[0]['version'] : 0;

            foreach ($current_identity as $key => $value) {
                // skip empty values
                if (empty($value['value']) || $value['version'] < $latest_identity_version) {
                    continue;
                }

                switch ($value['name']) {
                    case Identity::FO_VAT_ID:
                    case Identity::PO_VAT_ID:
                        $identity['dic'] = $value['value'];
                        break;
                    case Identity::PO_BUSINESS_ID:
                        $identity['ico'] = $value['value'];
                        break;
                    case Identity::FO_FIRSTNAME:
                        $identity['first_name'] = $value['value'];
                        break;
                    case Identity::FO_SURNAME:
                        $identity['last_name'] = $value['value'];
                        break;
                    case Identity::FO_SURNAME:
                        $identity['ico'] = $value['value'];
                        break;
                    case Identity::FO_FULLNAME:
                    case Identity::PO_FULLNAME:
                        $identity['name'] = $value['value'];
                    case Identity::CONTACT_DATABOX:
                        $identity['databox_id'] = $value['value'];
                        break;
                    case Identity::CONTACT_EMAIL:
                        $identity['email'] = $value['value'];
                        break;
                    case Identity::CONTACT_PHONE:
                        $identity['phone'] = $value['value'];
                        break;
                    default:
                        $identity[$value['name']] = $value['value'];
                        break;
                }
            }
        }

        // join data from PublicIncomeHistories
        $request = null;

        if ($user_id) {
            $income_histories = $this->PublicIncomeHistories->find(
                'all',
                ['conditions' => [
                    'user_id' => $user_id
                ], 'contain' => ['PublicIncomeSources']]
            )->toArray();

            foreach ($income_histories as $key => $value) {
                $value['added_by'] = __('uživatel');
                $value['czk_amount'] = $value['amount_czk'];
                $value['no_edit'] = true;
                $value['public_income_source'] = $value['public_income_source']['source_name'];
                $value['year'] = $value['fiscal_year'];
                $identity['histories'][] = $value;
            }

            // historical records
            $identities = $this->HistoryIdentities->find(
                'all',
                [
                    'contain' => [
                        'Histories',
                    ],
                    'conditions' => [
                        'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                ]
            )->toArray();

            // join identities via ico/dic
            foreach ($identities as $key => $value) {
                if ((
                        (!empty($value['ico']) && !empty($identity['ico']) &&  $value['ico'] === $identity['ico']) ||
                        (!empty($value['dic']) && !empty($identity['dic']) &&  $value['ico'] === $identity['dic'])) &&
                    (isset($value['histories']) && !empty($value['histories']))
                ) {
                    $identity['histories'] = array_merge($identity['histories'], $value['histories']);
                    $identity['id'] = $value['id'];
                }
            }

            // add dsw2 requests
            $allowedStates = [
                RequestState::STATE_CLOSED_FINISHED,
                RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                RequestState::STATE_SETTLEMENT_SUBMITTED,
                RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
            ];
            $requests = $this->Requests->find(
                'all',
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'Requests.user_id' => $user_id,
                        'Requests.request_state_id IN (' . implode(',', $allowedStates) . ')',
                    ],
                    'contain' => ['RequestLogs'],
                ]
            )->order(['user_identity_version' => 'DESC', 'id'  => 'DESC'])->toArray();

            foreach ($requests as $key => $value) {
                if ($value['final_subsidy_amount'] <= 0) {
                    continue;
                }

                $validLogs = array_filter($value->request_logs, function ($val) {
                    return (int)$val['request_state_id'] === RequestState::STATE_PAID_READY_FOR_SETTLEMENT;
                });
                $lastLog = count($validLogs) > 0 ? reset($validLogs) : [];
                $logDate = !empty($lastLog) && isset($lastLog['created']) ? substr($lastLog['created']->year, 0, 4) : '';

                $v = [];
                $v['added_by'] = __('systém');
                $v['czk_amount'] = $value['final_subsidy_amount'];
                $v['no_edit'] = true;
                $v['notes'] =  $value['request_comment'];
                $v['project_name'] = $value['name'];
                $v['public_income_source'] = OrgDomainsMiddleware::getCurrentOrganization()->name;
                $v['request_id'] = $value['id'];
                $v['request_state'] =   RequestState::getLabelByStateId($value['request_state_id']);
                $v['year'] = !empty($logDate) ? $logDate : substr($value['created']->year, 0, 4);
                $identity['histories'][] = (object)$v;
            }

            $request = isset($requests[0]) ? $requests[0] : $request;
            if ($request !== null) {
                $request['user_identity_version'] = $latest_identity_version;
                $request->loadUser()->user;
            }
        }

        // send e-mail
        if ($request !== null && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $subject = trim($this->getRequest()->getData('subject'));
            $text = trim($this->getRequest()->getData('contents'));

            if (empty($subject)) {
                $this->Flash->error(__('Předmět e-mailu musí být vyplněn'));
            } elseif (empty(strip_tags($text))) {
                $this->Flash->error(__('Text e-mailu musí být vyplněn'));
            } else {
                $mailIsSent = $this->sendMailSafe('User', 'requestNotice', [
                    $request->user,
                    $request,
                    $subject,
                    $text
                ]);
                if ($mailIsSent) {
                    $this->Flash->success(__('E-mail byl úspěšně odeslán'));
                } else {
                    $this->Flash->error(__('Při odesílání e-mailu došlo k neočekávané chybě'));
                }
            }
        }

        // final list of identity
        $this->set(compact('identity'));
        $this->set(compact('request'));
        $this->set('crumbs', [__('Žadatelé (včetně historie)') => ['action' => 'index']]);

        return $this->render('detail');
    }

    public function deleteHistory(int $identity_id, int $history_id)
    {
        $identity = $this->HistoryIdentities->get($identity_id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]);
        $history = $this->Histories->get($history_id, [
            'conditions' => [
                'Histories.history_identity_id' => $identity->id,
            ],
        ]);
        $this->Histories->delete($history);

        return $this->redirect(['action' => 'detail', 'id' => $identity->id]);
    }

    public function addModifyHistory(int $identity_id, $history_id = null)
    {
        $identity = $this->HistoryIdentities->get($identity_id, [
            'conditions' => [
                'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
        ]);

        $history = (int)$history_id > 0 ? $this->Histories->get($history_id, [
            'conditions' => [
                'Histories.history_identity_id' => $identity->id,
            ],
        ]) : $this->Histories->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $history->history_identity_id = $identity->id;
            $history = $this->Histories->patchEntity($history, $this->getRequest()->getData());
            if ($this->Histories->save($history)) {
                $this->Flash->success(__('Úspěšně uloženo'));
                // $this->redirect(['action' => 'detail', 'id' => $identity->id]);
                $this->redirect(['action' => 'index']);
                return;
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
                $this->Flash->error($history->getFirstError());
            }
        }

        $this->set(compact('identity', 'history'));
    }

    public function uploadIdentities()
    {
        $this->set('crumbs', [__('Žadatelé (včetně historie)') => ['action' => 'index']]);
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (empty($this->getRequest()->getData('filedata')) || $this->getRequest()->getData('filedata.error') !== UPLOAD_ERR_OK) {
                $this->Flash->error(__('Soubor nebyl nahrán nebo došlo k chybě při nahrávání'));
                return;
            }

            $fpath = $this->getRequest()->getData('filedata.tmp_name');
            $rowcount = 0;
            $new_identities_count = 0;
            $new_histories_count = 0;
            $handle = null;

            $COL_NAME = 0;
            $COL_DATABOX_ID = 1;
            $COL_BUSINESS_ID = 2;
            $COL_VAT_ID = 3;
            $COL_FIRST_NAME = 4;
            $COL_LAST_NAME = 5;
            $COL_DATE_OF_BIRTH = 6;
            $COL_SUBSIDY_AMOUNT_CZK = 7;
            $COL_PROJECT_TITLE = 8;
            $COL_SUBSIDY_YEAR = 9;
            $COL_NOTES = 10;

            try {
                if (($handle = fopen($fpath, "r")) !== false) {
                    while (($orig_line = fgets($handle)) !== false) {
                        $rowcount++;
                        if ($rowcount === 1) {
                            continue;
                        }
                        $utf8_line = autoUTF($orig_line);
                        $csv_values = str_getcsv($utf8_line, ",", '"', "\\");
                        $columns = count($csv_values);

                        $history_identity = null;
                        $history = null;

                        if ($columns === 7 || $columns === 11) {
                            // identity
                            $or_conditions = [];
                            if (!empty($csv_values[$COL_DATE_OF_BIRTH])) {
                                $or_conditions[] = [
                                    'HistoryIdentities.first_name' => trim(h($csv_values[$COL_FIRST_NAME])),
                                    'HistoryIdentities.last_name' => trim(h($csv_values[$COL_LAST_NAME])),
                                    'HistoryIdentities.date_of_birth' => FrozenDate::parseDate($csv_values[$COL_DATE_OF_BIRTH]),
                                ];
                            }
                            if (!empty($csv_values[$COL_BUSINESS_ID])) {
                                $po_conditions = [
                                    'HistoryIdentities.ico' => trim(h($csv_values[$COL_BUSINESS_ID])),
                                ];
                                if (!empty($csv_values[$COL_VAT_ID])) {
                                    $po_conditions['HistoryIdentities.dic'] = trim(h($csv_values[$COL_VAT_ID]));
                                }
                                $or_conditions[] = $po_conditions;
                            }
                            if (!empty($csv_values[$COL_DATABOX_ID])) {
                                $or_conditions[] = [
                                    'HistoryIdentities.databox_id' => trim(h($csv_values[$COL_DATABOX_ID])),
                                ];
                            }

                            $history_identity = $this->HistoryIdentities->find('all', [
                                'conditions' => [
                                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                                    'OR' => $or_conditions,
                                ],
                            ])->first();
                            if (empty($history_identity)) {
                                $history_identity = $this->HistoryIdentities->newEntity([
                                    'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                                    'name' => $csv_values[$COL_NAME],
                                    'first_name' => $csv_values[$COL_FIRST_NAME],
                                    'last_name' => $csv_values[$COL_LAST_NAME],
                                    'date_of_birth' => FrozenDate::parseDate($csv_values[$COL_DATE_OF_BIRTH]),
                                    'ico' => $csv_values[$COL_BUSINESS_ID],
                                    'dic' => $csv_values[$COL_VAT_ID],
                                    'databox_id' => $csv_values[$COL_DATABOX_ID],
                                ]);
                                if (!$this->HistoryIdentities->save($history_identity)) {
                                    $this->Flash->error(sprintf("%s %d", __('Žadatele nelze uložit, řádek: '), $rowcount));
                                    $this->Flash->error(json_encode($history_identity->getErrors()));
                                    $this->Flash->error(json_encode($csv_values));

                                    return;
                                }
                                $new_identities_count++;
                            }
                        }
                        if ($columns === 11 && $history_identity instanceof HistoryIdentity) {
                            $history_conditions = [
                                'Histories.history_identity_id' => $history_identity->id,
                                'Histories.year' => intval($csv_values[$COL_SUBSIDY_YEAR]),
                                'Histories.czk_amount' => intval($csv_values[$COL_SUBSIDY_AMOUNT_CZK]),
                                'Histories.project_name' => trim(h(strip_tags($csv_values[$COL_PROJECT_TITLE])))
                            ];
                            $history = $this->Histories->find('all', [
                                'conditions' => $history_conditions,
                            ]);
                            if ($history->count() > 1) {
                                // more than 1 history matched
                                $history_conditions['Histories.project_name'] = trim(h($csv_values[$COL_PROJECT_TITLE]));
                                $history = $this->Histories->find('all', [
                                    'conditions' => $history_conditions,
                                ])->first();
                            } elseif ($history->count() === 1) {
                                $history = $history->firstOrFail();
                            } else {
                                $history = $this->Histories->newEntity([
                                    'history_identity_id' => $history_identity->id,
                                    'czk_amount' => $history_conditions['Histories.czk_amount'],
                                    'project_name' => $history_conditions['Histories.project_name'] ?? trim(h(strip_tags($csv_values[$COL_PROJECT_TITLE]))),
                                    'year' => $history_conditions['Histories.year'],
                                    'notes' => trim(h(strip_tags($csv_values[$COL_NOTES]))),
                                ]);
                                if (!$this->Histories->save($history)) {
                                    $this->Flash->error(sprintf("%s %d", __('Poskytnutou podporu nelze uložit, řádek: '), $rowcount));
                                    $this->Flash->error(json_encode($history->getErrors()));
                                    $this->Flash->error(json_encode($csv_values));

                                    return;
                                }
                                $new_histories_count++;
                            }
                        }

                        if (empty($history_identity)) {
                            $this->Flash->error(sprintf(__('Špatný počet sloupců na řádku %d (nalezeno %d, očekáváno 7 nebo 11)'), $rowcount, $columns));

                            return;
                        }
                    }
                }
                $this->Flash->success(sprintf(__('Úspěšně upraveno %d žadatelů (%d nových žadatelů, %d nových záznamů o podpoře)'), $rowcount, $new_identities_count, $new_histories_count));
                $this->redirect(['action' => 'index']);
            } catch (Throwable $t) {
                Log::error($t->getMessage());
                Log::error($t->getTraceAsString());
                $this->Flash->error(__('Došlo k chybě při čtení nahraného souboru'));

                return;
            } finally {
                if ($handle !== null) {
                    fclose($handle);
                    unlink($fpath);
                }
            }
        }
    }

    public function dsw()
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw)) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $showingHidden = boolval($this->getRequest()->getParam('hidden'));
        $accounts = $this->OldDsw->getAccounts([], $showingHidden);

        $this->set(compact('accounts', 'showingHidden'));
        $this->render('OldDsw.OldDsw/listing');
    }

    public function dswZadosti()
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw)) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $showingHidden = boolval($this->getRequest()->getParam('hidden'));
        $zadosti = $this->OldDsw->getZadosti($showingHidden);
        if ($this->getRequest()->getParam('year')) {
            $zadosti = $zadosti->where([
                'Zadosti.dotacni_obdobi' => $this->getRequest()->getParam('year')
            ]);
        }
        $vyuctovani = $this->Settlements->findAllMatchingOldDsw();

        $this->set(compact('zadosti', 'vyuctovani', 'showingHidden'));
        $this->render('OldDsw.OldDsw/listing_zadosti');
    }

    /**
     * @param int $id dsw.ucty.id
     */
    public function dswDetail(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $account = $this->OldDsw->getAccountById($id);
        $vyuctovani = $this->Settlements->findAllMatchingOldDsw();
        $this->set(compact('account', 'vyuctovani'));
        $this->render('OldDsw.OldDsw/detail');
    }

    public function dswHideAccount(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $account = $this->OldDsw->getAccountById($id);
        $account->is_hidden = true;
        $this->OldDsw->Ucty->save($account);

        $this->redirect($this->referer());
    }

    /**
     * @param int $id dsw.prilohy.id
     * @return Response|null
     */
    public function dswDownload(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            return $this->redirect(['action' => 'index']);
        }

        /**
         * Original files archive should be placed in OldDsw plugin directory, with permissions, so it is readable
         * by web-server user/group
         * eg. folder uploads should be in path /var/www/html/plugins/OldDsw/uploads
         * paths in DSW1 DB are prefixed with '/uploads/' so we look up in the plugin folder directly
         */

        // This will throw if record is not found, resulting in NotFoundException
        $priloha = $this->OldDsw->getPriloha($id);
        $old_dsw_plugin_dir = ROOT . DS . 'plugins' . DS . 'OldDsw' . DS;
        $file = new File($old_dsw_plugin_dir . $priloha->filename);
        if ($file->exists()) {
            return $this->getResponse()->withFile(
                $file->path,
                [
                    'name' => urlencode($priloha->orig_filename),
                    'download' => true,
                ]
            );
        }
        // handle case when file is not present physically, or permissions are incorrect (we cannot tell the difference)
        return $this->getResponse()->withStatus(404);
    }

    /**
     * @param int $id dsw.zadosti.id
     */
    public function dswZadost(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id);
        $this->set(compact('zadost'));
        $this->render('OldDsw.OldDsw/zadost');
    }

    public function dswHideZadost(int $id)
    {
        if (!OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) || !isset($this->OldDsw) || $id < 1) {
            $this->redirect(['action' => 'index']);

            return;
        }

        $zadost = $this->OldDsw->getZadost($id);
        $zadost->is_hidden = true;
        $this->OldDsw->Zadosti->save($zadost);

        $this->redirect($this->referer());
    }
}
