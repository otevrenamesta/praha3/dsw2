<?php

namespace App\Controller;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\Team;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\ProgramsTable;
use App\Model\Table\TeamsTable;
use App\Model\Table\UsersTable;
use Cake\Database\Query;

/**
 * @property UsersTable $Users
 * @property TeamsTable $Teams
 * @property ProgramsTable $Programs
 */
class TeamsManagerController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Teams');
        $this->loadModel('Programs');
    }

    public function isAuthorized($user = null): bool
    {
        /**
         * @var User $user
         */
        $user = $this->Auth->user();

        return parent::isAuthorized($user) && $user->isUsersManager();
    }

    public function index()
    {
        $this->set('teams', $this->finder()->find('all'));
    }

    private function finder()
    {
        return $this->Teams->find(
            'all',
            [
                'conditions' => [
                    'Teams.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Users',
                ],
            ]
        );
    }

    public function addModify(int $id = 0)
    {
        $team = $id > 0 ? $this->finder()->where(['Teams.id' => $id])->firstOrFail() : $this->Teams->newEntity();
        $team->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
        $managerList = $this->Users->find(
            'list',
            [
                'contain' => [
                    'UsersToRoles',
                ],
            ]
        )->matching(
            'UsersToRoles',
            function (Query $q) {
                return $q->where(
                    [
                        // the user has at least once signed in on this portal
                        'UsersToRoles.user_role_id' => UserRole::USER_PORTAL_MEMBER,
                        'UsersToRoles.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ]
                );
            }
        );
        $user = $this->getCurrentUser();
        $allowed_programs = $this->Programs->find(
            'all',
            [
                'conditions' => [],
                'contain' => [
                    'ParentPrograms',
                    'ChildPrograms',
                    'Realms',
                    'Realms.Fonds',
                    'Appeals',
                    'EvaluationCriteria',
                    'TeamsRolesInProgram',
                ],
            ]
        )->order(
            [
                'Programs.realm_id' => 'ASC',
            ]
        );

        if (!$user->isSystemsManager()) {
            $allowed_programs->matching(
                'Realms.Fonds',
                function (\Cake\ORM\Query $query) use ($user) {
                    return $query->where(
                        ['Fonds.organization_id IN' => array_intersect(
                            $user->getOrganizationIdsWhereUserHasRoleWithFallbacks(UserRole::MANAGER_GRANTS, false, [UserRole::MANAGER_PORTALS]),
                            [OrgDomainsMiddleware::getCurrentOrganizationId()]
                        )]
                    );
                }
            )->distinct('Programs.id');
        }

        $role_fields = TeamsManagerController::getTeamsRoles();

        $programs_by_roles = [];
        foreach ($role_fields as $role_id => $rf) {
            $programs_by_roles[$role_id] = $allowed_programs->filter(function ($value, $key) {
                return empty($value->child_programs);
            })->map(function ($value, $key) use ($role_id, $id) {
                $selected = false;
                foreach ($value->get('teams_roles_in_program') as $role) {
                    if ($role->team_id != $id) continue;
                    if ($role->program_role_id != $role_id) continue;
                    $selected  = true;
                    break;
                }
                return [
                    'value' => $value->id,
                    'text' => $value->name,
                    'data-section' => $value->getDataSection(),
                    'selected' => $selected
                ];
            });
        }

        $emails = $team->users && count($team->users) > 0
            ? array_reduce($team->users, function ($carry,  $item) {
                if ($item) {
                    $carry[] = $item->email;
                }
                return  $carry;
            }) : [];

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $team = $this->Teams->patchEntity($team, $this->getRequest()->getData());

            $emails_new = $team->users && count($team->users) > 0
                ? array_reduce($team->users, function ($carry,  $item) {
                    if ($item) {
                        $carry[] = $item->email;
                    }
                    return  $carry;
                }) : [];

            $added = [];
            $removed = [];
            $orgName =  OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME);

            foreach ($emails_new as $key => $value) {
                if (!in_array($value, $emails)) {
                    $added[] = $value;
                }
            }
            foreach ($emails as $key => $value) {
                if (!in_array($value, $emails_new)) {
                    $removed[] = $value;
                }
            }

            if ($this->Teams->save($team)) {
                $this->Flash->success(__('Uloženo úspěšně'));

                // Adding
                foreach ($added as $key => $email) {
                    $subject = __('Zařazení do týmu na dotačním portále');
                    $text = sprintf(
                        "<br/>" . __('Dobrý den') . ",<br/><br/>informujeme Vás o zařazení do týmu \"%s\" na dotačním portále \"%s\". <br/><br/>S pozdravem<br/>%s, v z. %s",
                        $team->name,
                        $orgName,
                        $orgName,
                        $this->getCurrentUser()->email,
                    );
                    $mailIsSent = $this->sendMailSafe('User', 'teamMessage', [
                        [$email],
                        $subject,
                        $text
                    ]);
                    if ($mailIsSent) {
                        $this->Flash->success(__('E-mail o zařazení byl úspěšně odeslán') . ' (' . $email . ')');
                    } else {
                        $this->Flash->error(__('Při odesílání e-mailu o zařazení došlo k neočekávané chybě') . ' (' . $email . ')');
                    }
                }

                // Removing
                foreach ($removed as $key => $email) {
                    $subject = __('Vyřazení z týmu na dotačním portále');
                    $text = sprintf(
                        "<br/>" . __('Dobrý den') . ",<br/><br/>informujeme Vás o vyřazení z týmu \"%s\" na dotačním portále \"%s\". <br/><br/>S pozdravem<br/>%s, v z. %s",
                        $team->name,
                        $orgName,
                        $orgName,
                        $this->getCurrentUser()->email,
                    );
                    $mailIsSent = $this->sendMailSafe('User', 'teamMessage', [
                        [$email],
                        $subject,
                        $text
                    ]);
                    if ($mailIsSent) {
                        $this->Flash->success(__('E-mail o vyřazení byl úspěšně odeslán') . ' (' . $email . ')');
                    } else {
                        $this->Flash->error(__('Při odesílání e-mailu o vyřazení došlo k neočekávané chybě') . ' (' . $email . ')');
                    }
                }

                $this->redirect(['action' => 'addModify', $team->id]);
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $this->set(compact('team', 'managerList', 'programs_by_roles'));
        $this->set('crumbs', [__('Struktura Dotačního Úřadu') => 'admin_teams']);
    }

    public function teamCopy(int $team_id)
    {
        $team = $this->finder()->where(['Teams.id' => $team_id])->firstOrFail();
        $team->unsetProperty('created')->unsetProperty('modified')->unsetProperty('id');
        $team->name .= sprintf(" - %s %s", __('Kopie'), date('Y-m-d H:i:s'));
        $team->isNew(true);

        //TODO: Copy roles in programs too, now when it's possible to have multiple teams having the same role in a program, see TeamsTable::beforeMarshal
        // Copy without the roles in programs; those must stay with the original
        /*foreach (Team::ALL_TEAMS as $team_role) {
            $team->unsetProperty($team_role);
        }*/


        if ($this->Teams->save($team)) {
            $this->Flash->success(__('Kopie vytvořena úspěšně'));
        } else {
            $this->Flash->error(__('Chyba při vytvářené kopie'));
        }
        $this->redirect(['action' => 'index']);
    }

    public function teamDelete(int $team_id)
    {
        $team = $this->finder()->where(['Teams.id' => $team_id])->firstOrFail();
        if ($this->Teams->delete($team)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Tým nebylo možné smazat'));
        }
        $this->redirect(['action' => 'index']);
    }

    public static function getTeamsRoles(): array
    {
        return [
            1 => 'formal_check',
            3 => 'price_proposal',
            4 => 'price_approval',
            2 => 'comments',
            5 => 'request_manager',
            6 => 'preview'
        ];
    }
}
