<?php

namespace App\Controller;

use App\Form\IdentityForm;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\IdentityAttachmentType;
use App\Model\Entity\RequestState;
use Cake\Filesystem\File;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\CsuCountriesTable;
use App\Model\Table\CsuLegalFormsTable;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;
use App\Model\Table\HistoriesTable;
use App\Model\Table\HistoryIdentitiesTable;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\PublicIncomeHistoriesTable;
use App\Model\Table\UsersTable;
use App\Model\Table\IdentityAttachmentsTable;
use App\Model\Table\IdentityAttachmentTypesTable;
use App\Controller\ChangeRequestController;
use Cake\Cache\Cache;
use Cake\I18n\FrozenTime;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Log\Log;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\View\Helper\HtmlHelper;
use Cake\ORM\TableRegistry;
use Exception;
use OldDsw\Controller\Component\OldDswComponent;
use OldDsw\Model\Entity\Ucet;
use OldDsw\Model\Entity\Uzivatel;
use Throwable;
use function Laminas\Diactoros\createUploadedFile;

/**
 * Controller to handle basic user actions, such as login/register/logout/password_recovery,
 * managing user details, etc.
 *
 * @property UsersTable $Users
 * @property CsuCountriesTable $CsuCountries
 * @property CsuMunicipalitiesTable $CsuMunicipalities
 * @property CsuMunicipalityPartsTable $CsuMunicipalityParts
 * @property CsuLegalFormsTable $CsuLegalForms
 * @property HistoriesTable $HistoriesTable
 * @property HistoryIdentitiesTable $HistoryIdentities
 * @property IdentitiesTable $Identities
 * @property OldDswComponent $OldDsw
 * @property PublicIncomeHistories $PublicIncomeHistories
 * @property PublicIncomeHistoriesTable $PublicIncomeHistories
 * @property RequestsTable $Requests
 * @property IdentityattachmentsTable $IdentityAttachments
 * @property IdentityattachmentsTypesTable $IdentityAttachmentsTypes
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'processNiaLogin', 'processNiaLogout', 'register', 'passwordRecovery', 'passwordRecoveryGo', 'verifyEmail', 'newEmail', 'inviteAccept']);
        $this->loadModel('Users');
        $this->loadModel('CsuCountries');
        $this->loadModel('CsuLegalForms');
        $this->loadModel('CsuMunicipalities');
        $this->loadModel('CsuMunicipalityParts');
        $this->loadModel('Histories');
        $this->loadModel('HistoryIdentities');
        $this->loadModel('Identities');
        $this->loadModel('PublicIncomeHistories');
        $this->loadModel('Requests');
        $this->loadModel('IdentityAttachments');
        $this->loadModel('IdentityAttachmentTypes');
    }

    public function isAuthorized($user = null): bool
    {
        if ('logout' === $this->getRequest()->getParam('action')) {
            return $this->getCurrentUser() instanceof User;
        }

        return parent::isAuthorized($user);
    }


    /**
     * Make file available for opening or download for the authorized user
     *
     * @param int $id  user id
     * @param int $file_id file id
     *
     */
    public function identityAttachmentFile(int $id, int $file_id)
    {

        $user = $this->getCurrentUser();
        $grantAccess = ($user->id == $id) || $user->isManager();

        if ($grantAccess) {
            $res = $this->IdentityAttachments->find('all', [
                'conditions' => [
                    'IdentityAttachments.file_id' => $file_id,
                ],
                'contain' => ['Files']
            ])->toArray();
            if (count($res)) {
                $file = $res[0]->file;
                $target = $file->getRealPath();
                $_file = new File($target);

                if ($_file->exists()) {
                    return $this->getResponse()->withFile($target, ['name' => urlencode($file->original_filename), 'download' => true]);
                }
            }
        }
        return $this->getResponse()->withStatus(404);
    }

    /**
     * Toggle identity attachment archive column
     * @param int $id  user id
     * @param int $file_id file id
     */

    public function identityAttachmentToggle(int $file_id)
    {

        $user = $this->getCurrentUser();
        if ($id = $user->id) { // Is the user logged in at all ?
            $res = $this->IdentityAttachments->find('all', [
                'conditions' => [
                    'IdentityAttachments.user_id' => $id,
                    'IdentityAttachments.file_id' => $file_id,
                ],
                'contain' => ['Files']
            ])->toArray();
            if (count($res)) {
                $attachment = $res[0];
                if ($attachment->is_archived == 1) {
                    $attachment->is_archived = 0;
                } else {
                    $attachment->is_archived = 1;
                }
                if ($this->IdentityAttachments->save($attachment)) {
                    $result = 'OK';
                } else {
                    $result = 'FAIL';
                }
                return $this->getResponse()->withType('json')->withstringBody($result . ' (' . $attachment->is_archived . ')');
            }
        }
        return $this->getResponse()->withStatus(404);
    }

    /** Ajax endpoint for deleting identity attachment files
     *
     * @param int $file_id
     *
     * @return [type]
     */

    public function identityAttachmentDelete(int $file_id)
    {
        $result = 'FAIL';
        $user = $this->getCurrentUser();
        if ($id = $user->id) { // Is the user logged in at all ?
            $res = $this->IdentityAttachments->find('all', [
                'conditions' => [
                    'IdentityAttachments.user_id' => $id,
                    'IdentityAttachments.file_id' => $file_id,
                ],
                'contain' => ['Files']
            ])->toArray();
            if (count($res)) {
                $attachment = $res[0];
                if ($attachment->is_archived == 1) {
                    // Now we can proceed with deleting
                    $this->IdentityAttachments->delete($attachment);
                    /** @var FilesTable $filesTable */
                    $filesTable = TableRegistry::getTableLocator()->get('Files');
                    /** @noinspection PhpIncompatibleReturnTypeInspection */
                    $file = $filesTable->find(
                        'all',
                        [
                            'conditions' => [
                                'Files.user_id' => $id,
                                'Files.id' => $attachment->file_id,
                            ],
                        ]
                    )->first();
                    if ($file) {
                        try {
                            if ($filesTable->delete($file)) {
                                $file->deletePhysically();
                                $result = 'OK';
                            }
                        } catch (Throwable $t) {
                            // failing to file
                            Log::error($t);
                        }
                    }
                }
            }
        }
        return $this->getResponse()->withType('json')->withstringBody($result);
    }

    public function changeInfo()
    {
        try {
            $parsedReferer = Router::getRouteCollection()->parse($this->getRequest()->referer(true));
        } catch (Exception $e) {
            $parsedReferer = null;
        }
        if (!empty($parsedReferer) && ($parsedReferer['_name'] ?? null) === 'request_detail') {
            $this->persistReferer();
        }
        $identityForm = new IdentityForm();
        $identityForm->setCountries($this->CsuCountries);
        $identityForm->setCities($this->CsuMunicipalities);
        $identityForm->setCityParts($this->CsuMunicipalityParts);
        $identityForm->setLegalForms($this->CsuLegalForms);
        $identityForm->loadIdentities($this->Identities, $this->getCurrentUser());
        $identityForm->loadAttachments($this->IdentityAttachments, $this->getCurrentUser());
        $identityForm->loadAttachmentTypes($this->IdentityAttachmentTypes);

        $type_names = [];
        foreach ($identityForm->identity_attachment_types as $type) {
            if ($type['order'] < 1000) {
                $type_names[__('Přílohy k žádostem')][$type['id']] = $type['type_name'];
            } else {
                $type_names[__('Samostatné přílohy')][$type['id']] = $type['type_name'];
            }
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($identityForm->execute($this->getRequest()->getData())) {
                $data = $this->getRequest()->getData();
                if (array_key_exists('filedata', $data) && (trim($data['filedata']['tmp_name']))) {
                    $uploadedFile = createUploadedFile($this->getRequest()->getData('filedata'));
                    if ($uploadedFile->getError() !== UPLOAD_ERR_OK || $uploadedFile->getSize() <= 1) {
                        if ($uploadedFile->getError() !== UPLOAD_ERR_NO_FILE) {
                            $this->Flash->error(__('Nahrávání souboru se nepodařilo'));
                        }
                    } else {
                        $attachment = $this->Users->IdentityAttachments->newEntity();
                        $file = $this->Users->IdentityAttachments->Files->newEntity();
                        $uploadedMovedFilePath = $file->fileUploadMove($this->getRequest()->getData('filedata'), $this->getCurrentUserId());
                        if (is_string($uploadedMovedFilePath)) {
                            $file->original_filename = $uploadedFile->getClientFilename();
                            $file->filesize = $uploadedFile->getSize();
                            $file->filepath = $uploadedMovedFilePath;
                            $file->user_id = $this->getCurrentUserId();
                            $attachment->file = $file;
                            $attachment->setDirty('file');
                            $fileWasUploaded = true;
                            $attachment->user_id = $this->getCurrentUserId();
                            $attachment->identity_attachment_type_id = $data['file_type'] ?? IdentityAttachmentType::TYPE_GENERAL_ATTACHMENT;
                            $attachment->description = $data['file_description'] ?? '';
                            if (!$this->Users->IdentityAttachments->save($attachment)) {
                                $this->Flash->error(__('Chyba při zpracování nahrávaného souboru'));
                                return;
                            }
                        }
                    }
                }
                if (!empty($identityForm->getErrors())) {
                    $this->Flash->error(__('Formulář obsahuje chyby, opravte je prosím a znovu zkuste uložit'));
                } else {
                    $this->Flash->success(__('Vaše identita byla úspěšně uložena'));
                    // Identity change notification
                    ChangeRequestController::identityChangeNotify($this->getCurrentUser());
                    // update all non-locked requests to use latest identity version
                    $maxIdentityVersion = $this->getCurrentUser()->getLastIdentityVersion();
                    $this->Users->Requests->updateAll([
                        'user_identity_version' => $maxIdentityVersion,
                    ], [
                        'user_id' => $this->getCurrentUser()->id,
                        'is_locked' => false,
                    ]);
                    $this->redirect($this->retrieveReferer());

                    return;
                }
            } else {
                $this->Flash->error(__('Nastala chyba při ukládání vaší identity, pravděpodobně chybí povinné položky.'));
            }
        }

        $flash_counter = 0;
        $form_errors = [];
        foreach ($this->getCurrentUser()->getIdentityMissingFields() ?? [] as $missing_identity_field) {
            if ($flash_counter < 3) {
                $this->Flash->error(sprintf("%s: %s", __('Chybí vyplnit'), Identity::getFieldLabel($missing_identity_field)));
            }
            $form_errors[$missing_identity_field] = __('Chybí vyplnit');
            $flash_counter++;
        }
        if ($flash_counter > 3) {
            $this->Flash->error(sprintf(__('Celkem je potřeba opravit %d chyb'), $flash_counter));
        }
        $identityForm->setErrors(array_merge_recursive($identityForm->getErrors(), Hash::expand($form_errors)));

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (count($identityForm->getErrors()) === 0) {
                $this->redirect($this->getRequest()->getRequestTarget());
            }
        }
        $newAttachment = $this->IdentityAttachments->newEntity();
        $this->set(compact('identityForm', 'newAttachment', 'type_names'));
    }

    /**
     * ARES https://ares.gov.cz/stranky/popis
     * API https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/v3/api-docs
     *
     * @param string $ico
     * @param string $type
     * @return Response|null
     */
    public function aresFetch(string $ico, string $type)
    {
        $type = ($type === 'or' ? 'vr' : 'res');
        $ico = preg_replace("/[^0-9]/", "", $ico);
        if (empty($ico)) {
            throw new NotFoundException();
        }
        $cacheKey = 'ares_' . md5($ico) . $type;
        $cacheData = Cache::read($cacheKey);

        if (!empty($cacheData) && mb_strpos($cacheData, 'CHYBA_VSTUPU') !== false) {
            Cache::delete($cacheKey);
            $cacheData = false;
        }

        if ($cacheData === false) {
            try {
                $url = 'https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty-' . $type . '/' . $ico;
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);

                $cacheData = curl_exec($ch);

                if (curl_errno($ch)) {
                    throw new Exception(curl_error($ch));
                }

                curl_close($ch);
            } catch (Throwable $t) {
                $cacheData = false;
                Log::error($t);
            }

            if (!empty($cacheData)) {
                Cache::write($cacheKey, $cacheData);
            } else {
                return $this->getResponse()->withStatus(404);
            }
        }

        return $this->getResponse()->withType('json')->withStringBody($cacheData);
    }

    public function shareMyAccount()
    {
        $origin = $this->getCurrentUser()->getOriginIdentity($this->getRequest());
        $this->Users->loadInto($origin, ['AllowedToUsers', 'AllowedToUsers.AllowedUser']);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $postData = $this->getRequest()->getData('allowed_users_emails', []);
            $postData = !is_array($postData) ? [] : $postData;
            $allowedUsersToDelete = $origin->patchAllowedToUsers($postData, $this->Users);
            if ($this->Users->save($origin)) {
                foreach ($allowedUsersToDelete as $usersToUserEntity) {
                    $this->Users->AllowedUsers->delete($usersToUserEntity);
                }
                $this->Flash->success(__('Uloženo úspěšně'));
                $this->getCurrentUser()->setOriginIdentity(
                    $this->getRequest(),
                    $this->Users->find('withRoles', ['conditions' => ['Users.id' => $origin->id]])->firstOrFail()
                );
            } else {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            }
        }

        $existing_shares = [];
        foreach ($origin->allowed_to_users as $existing_share) {
            $existing_shares[$existing_share->allowed_user->email] = $existing_share->allowed_user->email;
        }

        $this->set(compact('origin', 'existing_shares'));
    }

    public function switchToAllowed(int $target_user_id)
    {
        $target_user = $this->Users->find('forSwitch', [
            'conditions' => [
                'Users.id' => $target_user_id,
            ],
        ])->firstOrFail();

        if (!($this->getCurrentUser()->getOriginIdentity($this->getRequest()))->switchToUser($this->getRequest(), $target_user)) {
            $this->Flash->error(__('Nemáte oprávnění zastupovat tohoto uživatele'));

            return $this->redirect('/');
        }

        return $this->redirect(['_name' => 'user_requests']);
    }

    public function login()
    {
        if ($this->Auth->user() instanceof User) {
            // already logged in
            $this->redirect('/');

            return;
        }

        $this->set('user', $this->Users->newEntity());

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            /**
             * @var User $user
             */
            $user = $this->Auth->identify();

            // OldDsw provide guidance on account migration
            $is_email = filter_var($this->getRequest()->getData('email'), FILTER_VALIDATE_EMAIL);
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true && !$is_email && !($user instanceof User)) {
                $this->loadComponent('OldDsw.OldDsw');
                $old_dsw_user = $this->OldDsw->authenticate($this->getRequest()->getData('email'), $this->getRequest()->getData('password'));
                if ($old_dsw_user instanceof Uzivatel) {
                    $user_by_email = $this->Users->find(
                        'all',
                        [
                            'conditions' => [
                                'email' => $old_dsw_user->email,
                            ],
                        ]
                    )->first();
                    if ($user_by_email instanceof User) {
                        $this->Flash->set(sprintf(__('Přihlaste se prosím svým e-mailem %s a heslem k novému účtu'), $old_dsw_user->email), ['element' => 'info']);
                    } else {
                        $this->Flash->set(sprintf(__('Pokusili jste se přihlásit starým uživatelským účtem. Vytvořte si prosím nový účet s e-mailem %s a získáte přístup ke svým historickým žádostem'), $old_dsw_user->email), ['element' => 'info']);
                        $this->Flash->success((new HtmlHelper($this->viewBuilder()->build()))->link(__('Registrovat se'), ['_name' => 'register']), ['escape' => false]);
                    }

                    return;
                }
            }

            if (!($user instanceof User)) {
                $this->Flash->error(__('Přihlášení se nezdařilo'));

                return;
            }

            $this->getRequest()->getSession()->delete(User::SESSION_ORIGIN_IDENTITY);

            if (!$user->is_enabled) {
                // do not send recovery email, if disabled user is not yet member of this portal
                if ($user->hasRole(UserRole::USER_PORTAL_MEMBER, OrgDomainsMiddleware::getCurrentOrganizationId())) {
                    // regenerate email token first
                    $user->regenerateEmailToken();
                    $this->Users->save($user);
                    // send verify_email
                    $this->sendMailSafe('User', 'verifyEmail', [$user]);
                }

                $this->Flash->error(__('Váš účet je zakázán. Musíte nejdříve aktivovat svůj účet odkazem zaslaným Vám do e-mailu.'));

                return;
            }
            if (
                !OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ENABLE_LOGIN_USERS, true)
                && !$user->hasRoles(UserRole::ALL_MANAGER, OrgDomainsMiddleware::getCurrentOrganizationId(), true)
            ) {
                $this->Flash->error(__('V tuto chvíli je zakázáno přihlašování uživatel, zkuste to prosím později'));

                return;
            }
            // create member role if not yet created
            $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
            // this will touch db only if previous call made any field dirty
            try {
                $this->Users->save($user);
            } catch (Exception $ignore) {
                // rare case duplicate index violation
                // this shall not prevent user from being logged in
            }

            $this->Auth->setUser($user);
            $this->Flash->success(__('Vítejte'));
            if ($user->isManager() && $certificate = OrganizationSetting::getSetting('ds.cert', false)) {
                $certificate_info = OrganizationSetting::extractCertificateInfo($certificate);
                if ((!$certificate_info['err_msg']) && array_key_exists('notAfter', $certificate_info)) {
                    if (($certificate_info['notAfter'] > time()) && ($certificate_info['notAfter'] < (time() + (time() + 2.678e+6)))) {
                        $timestamp1 = time();
                        $timestamp2 = $certificate_info['notAfter'];
                        $date1 = new \DateTime("@$timestamp1");
                        $date2 = new \DateTime("@$timestamp2");
                        $diff = $date1->diff($date2);
                        $days = $diff->days;
                        $this->Flash->warning(__('Certifikát pro odesílací bránu ISDS vyprší za ') . $days . ' dní');
                    } elseif (($certificate_info['notAfter'] <= time())) {
                        $this->Flash->error(__('Certifikát pro odesílací bránu ISDS již není platný !!!'));
                    }
                }
            }

            $this->redirect($this->Auth->redirectUrl());

            return;
        }

        $nia_enabled = false;
        $organization = OrgDomainsMiddleware::getCurrentOrganization();
        if ($organization->getSettingValue(OrganizationSetting::ENABLE_LOGIN_NIA, false)) {
            $nia_enabled = true;
        }
        $this->set(compact('nia_enabled', 'organization'));
    }

    public function register()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ENABLE_REGISTRATION, true)) {
            $this->Flash->error(__('Registrace uživatelů aktuálně není povolena, zkuste to prosím později'));
            $this->redirect('/');

            return;
        }

        $user = $this->Users->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $this->Users->patchEntity($user, $this->getRequest()->getData());

            $user->regenerateEmailToken();
            $user->regenerateSalt();
            $user->set('is_enabled', false);
            $user->setNewPassword($this->getRequest()->getData('password'));

            if ($this->Users->save($user)) {
                if ($this->Users->find('all')->count() === 1) {
                    // set first user as systems manager to avoid default user with default password config
                    $user->set('user_roles', [$this->Users->UserRoles->get(UserRole::MANAGER_SYSTEMS)]);
                    $user->set('is_enabled', true);
                    $this->Users->save($user);
                } else {
                    // create association between this portal and the user on successfull registration
                    $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
                    $this->Users->save($user);
                }

                if ($this->sendMailSafe('User', 'verifyEmail', [$user])) {
                    $this->Flash->success(__('Registrace proběhla úspěšně. Následujte prosím instrukce odeslané vám do e-mailu.'));
                    $this->Flash->warning(_('Zkontrolujte složku se spamem a hromadnými zprávami!!!'));
                }

                $this->redirect('/');

                return;
            } else {
                $is_registering_valid_email = filter_var($this->getRequest()->getData('email'), FILTER_VALIDATE_EMAIL);
                $flashMessage = __('Registrační formulář obsahuje chyby');
                if ($is_registering_valid_email) {
                    /** @var User $user_by_email */
                    $user_by_email = $this->Users->find('all', [
                        'conditions' => [
                            'Users.email' => $is_registering_valid_email
                        ]
                    ])->first();
                    if ($user_by_email) {
                        $firstPortal = $this->Users->getNameOfFirstOtherPortalWhereUserIsMember($user_by_email);
                        if (!empty($firstPortal)) {
                            $flashMessage = sprintf("%s %s, %s", __('Tento e-mail je již registrován, např. na portále'), $firstPortal, __('stačí se jen přihlásit'));
                        }
                    }
                }
                $this->Flash->error($flashMessage);
            }
        }

        $this->set(compact('user'));
    }

    public function processNiaLogout()
    {
        $this->logout(true);
    }

    public function processNiaLogin()
    {

        // Already logged in
        if ($this->Auth->user()) {
            $this->redirect('/');
            return;
        }


        if (!$this->getRequest()->is(['get'])) {
            $this->Flash->error(__('Bad request method.'));
            $this->redirect('/');
            return;
        }

        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ENABLE_LOGIN_NIA, true)) {
            $this->Flash->error(__('Přihlašování přes NIA není povoleno, kontaktujte správce.'));
            $this->redirect('/');
            return;
        }

        $user = $this->Auth->identify();
        /** @var User $user */

        if (!$user) {
            $this->Flash->error(__('Přihlašování přes NIA selhalo, kontaktujte správce.'));
            $this->redirect('/');
            return;
        }

        if (property_exists($user, 'is_new') &&  $user->is_new) {
            $user = $this->registerNewViaNIA($user);
        }

        $this->Auth->setUser($user);
        $this->Flash->success(__('Vítejte'));
        $this->redirect($this->Auth->redirectUrl());

        return;
    }

    public function registerNewViaNIA($user_data): User
    {

        $user = $this->Users->newEntity();
        $user->set('is_enabled', true);
        $user->set('email', $user_data->email);
        $user->set('new_email', null);
        $user->set('original_email', null);
        $user->set('nia_id', $user_data->nia_id);
        $user->setNewPassword($this->randomPassword());
        $user->regenerateEmailToken();
        $user->regenerateSalt();

        if ($new_user = $this->Users->save($user)) {
            // create association between this portal and the user on successfull registration
            $user->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
            $this->Users->save($user);
        } else {
            $this->Flash->error(__('Nepodařilo se vytvořit uživatele pro NIA přihlašování.'));
            $this->redirect('/');
        }
        return $new_user;
    }


    /**
     * Generate random password to be assigned to new users registered via NIA
     * @return string
     */
    private function randomPassword(): string
    {
        $length = 12; // desired password length
        $password = base64_encode(random_bytes($length));
        // Replace non-alphanumeric characters with a random digit
        $password = preg_replace('/[^a-zA-Z0-9]/', mt_rand(0, 9), $password);
        // Trim to desired length
        $password = substr($password, 0, $length);
        return $password;
    }

    public function changeEmail()
    {
        $user = $this->Users->get($this->Auth->user('id'));

        if ($user->id !== $user->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Pro změnu e-mailu, se musíte nejdříve přepnout zpět do svého účtu'));
            $this->redirect('/');
            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $new_email = filter_var($this->getRequest()->getData('new_email'), FILTER_VALIDATE_EMAIL);

            if ($new_email) {
                /** @var User $user_by_email */
                $user_by_email = $this->Users->find('all', [
                    'conditions' => [
                        'Users.email' => $new_email
                    ]
                ])->first();
                if ($user_by_email) {
                    $firstPortal = $this->Users->getNameOfFirstOtherPortalWhereUserIsMember($user_by_email);
                    if (!empty($firstPortal)) {
                        $flashMessage = sprintf("%s %s, %s", __('Tento e-mail je již registrován, např. na portále'), $firstPortal, __('stačí se jen přihlásit.'));
                        $this->Flash->error($flashMessage);
                    }
                } else {
                    $user->regenerateEmailToken();
                    $user->set('new_email', $new_email);

                    if ($this->Users->save($user)) {
                        if ($this->sendMailSafe('User', 'newEmail', [$user])) {
                            $this->Flash->success(__('Probíhá změna e-mailu. Následujte prosím instrukce odeslané vám do e-mailu.'));
                            $this->Flash->warning(_('Zkontrolujte složku se spamem a hromadnými zprávami!!!'));
                        }
                    } else {
                        $this->Flash->error(__('Nový e-mail nebylo možné uložit.'));
                    }

                    $this->redirect('/');
                    return;
                }
            } else {
                $flashMessage = __('Formulář pro změnu e-mailu obsahuje chyby');
                $this->Flash->error($flashMessage);
            }
        }

        $this->set(compact('user'));
    }

    public function newEmail()
    {
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                ],
                'contain' => [
                    'StareUcty',
                ],
            ]
        )->first();

        if (!($user instanceof User)) {
            $this->Flash->error(__('Neplatný odkaz změnu e-mailu. Nechte si prosím zaslat nový.'));

            return $this->redirect(['_name' => 'change_email']);
        }

        $user->regenerateEmailToken();
        $originalEmail = $user->original_email;
        if (empty($originalEmail)) {
            $user->set('original_email', $user->email);
        }
        $user->set('email', $user->new_email);
        $user->set('new_email', null);

        if ($this->Users->save($user)) {
            $this->Flash->success(__('Vaše nová e-mailová adresa byla ověřena. Nyní se s ní můžete přihlásit.'));
        } else {
            $this->Flash->error(__('Nový e-mail nebylo možné ověřit.'));
        }

        return $this->redirect(['_name' => 'login']);
    }

    public function changePassword()
    {
        $user = $this->Users->get($this->Auth->user('id'));

        if ($user->id !== $user->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Pro změnu hesla, se musíte nejdříve přepnout zpět do svého účtu'));
            $this->redirect('/');

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            // check old password first
            if (password_verify($this->getRequest()->getData('old_password') . $user->salt, $user->password)) {
                $new_password = $this->getRequest()->getData('new_password');
                if (empty($new_password)) {
                    $user->setError('new_password', __('Nové heslo nesmí být prázdné'));
                } else {
                    $user->regenerateSalt();
                    $user->setNewPassword($new_password);
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Heslo bylo změněno úspěšně'));
                        $this->redirect('/');
                    } else {
                        $this->Flash->error(__('Chyba při ukládání'));
                    }
                }
            } else {
                $user->setError('old_password', __('Aktuální heslo nesouhlasí'));
            }
        }

        $this->set(compact('user'));
    }

    public function logout($skip_nia_check = false)
    {
        if (!$skip_nia_check) {
            $authenticated_via_nia = $this->request->getSession()->read('NIAAuth');
            if ($authenticated_via_nia) {
                // We redirect here to NIA logout and do the actual DSW2 logout in processNiaLogout which is invoked from the node app
                $query = 'nameid=' . urlencode($this->request->getSession()->read('NameID'));
                $query .= '&sessionindex=' . urlencode($this->request->getSession()->read('SessionIndex'));
                $this->redirect('/nia/' . OrgDomainsMiddleware::getCurrentOrganizationId() . '/logout?' . $query);
            }
        }
        $this->Auth->logout();
        $this->getRequest()->getSession()->destroy();
        $this->Flash->success(__('Odhlášení proběhlo úspěšně'));
        $this->redirect('/');
    }

    public function verifyEmail()
    {
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                ],
                'contain' => [
                    'StareUcty',
                ],
            ]
        )->first();
        if (!($user instanceof User)) {
            $this->Flash->error(__('Neplatný odkaz pro obnovu hesla. Nechte si prosím zaslat nový.'));

            return $this->redirect(['_name' => 'password_recovery']);
        }
        // previous will fail hard if user is not matched
        $user->regenerateEmailToken();
        $user->set('is_enabled', true);
        if ($this->Users->save($user)) {
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true) {
                $this->loadComponent('OldDsw.OldDsw');
                // pair old dsw accounts to user by verified email address
                try {
                    /**
                     * @var Ucet[] $ucty
                     */
                    $ucty = $this->OldDsw->getAccounts()->where(['Ucty.email' => $user->email])->toArray();
                    $uzivatele = $this->OldDsw->getAccountsByUsersEmail($user->email);
                    // keep old verified accounts that were set by portal manager for example
                    foreach ($user->stare_ucty as $stary_ucet) {
                        $user->stare_ucty[$stary_ucet->id] = $stary_ucet;
                    }
                    // add all newly matched accounts by verified email
                    foreach (($ucty + $uzivatele) as $ucet) {
                        // add unique
                        $user->stare_ucty[$ucet->id] = $ucet;
                    }
                    // reset keys
                    $user->stare_ucty = array_values($user->stare_ucty);
                    // force save association
                    $user->setDirty('stare_ucty');
                    $this->Users->save($user);
                } catch (Throwable $t) {
                    Log::error($t->getMessage(), $t);
                }
            }

            $this->Flash->success(__('Vaše e-mailová adresa byla ověřena. Nyní se můžete přihlásit'));
        } else {
            $this->Flash->error(__('Nebylo možné povolit váš účet'));
        }

        return $this->redirect(['_name' => 'login']);
    }

    public function passwordRecovery()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        $oneShotStoreKey = 'passwordRecovery_oneShot';
        $oneShotCode = $this->getOrGenerateOneShotCode($oneShotStoreKey);
        $this->set(compact('oneShotCode'));

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if ($this->getRequest()->getData('oneShot') != $oneShotCode) {
                $this->Flash->error(__('Obnovte stránku a odešlete formulář znovu, prosím'));

                return;
            }
            $this->regenerateOneShotCode($oneShotStoreKey);

            /**
             * @var User $user_by_email
             */
            $user_by_email = $this->Users->find(
                'all',
                [
                    'conditions' => [
                        'Users.email' => $this->getRequest()->getData('email'),
                    ],
                    'contain' => [
                        'UsersToRoles',
                    ],
                ]
            )->first();

            // OldDsw provide guidance on account migration
            $is_email = filter_var($this->getRequest()->getData('email'), FILTER_VALIDATE_EMAIL);
            if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW) === true && $is_email && !($user_by_email instanceof User)) {
                $this->loadComponent('OldDsw.OldDsw');
                $old_dsw_accounts = $this->OldDsw->getAccountsByUsersEmail($this->getRequest()->getData('email'));
                if (!empty($old_dsw_accounts)) {
                    $this->Flash->set(sprintf(__('Pokusili jste se obnovit heslo ke starému uživatelskému účtu. Vytvořte si prosím nový účet s e-mailem %s a získáte přístup ke svým historickým žádostem'), $this->getRequest()->getData('email')), ['element' => 'info']);
                    $this->Flash->success((new HtmlHelper($this->viewBuilder()->build()))->link(__('Registrovat se'), ['_name' => 'register']), ['escape' => false]);

                    return;
                }
            }

            if (empty($user_by_email) || !($user_by_email->hasRole(UserRole::USER_PORTAL_MEMBER, OrgDomainsMiddleware::getCurrentOrganizationId()))) {

                $firstPortal = $user_by_email instanceof User ? $this->Users->getNameOfFirstOtherPortalWhereUserIsMember($user_by_email) : null;
                if (!empty($firstPortal)) {
                    $this->Flash->error(sprintf("%s %s, %s", __('Tento e-mail je již registrován, např. na portále'), $firstPortal, __('stačí se jen přihlásit')));
                } else {
                    $this->Flash->success(__('Pokračujte prosím otevřením odkazu v e-mailu, který jsme vám právě zaslali'));
                }

                return;
            }

            $user_by_email->regenerateEmailToken();
            $user_by_email->createMemberRoleIfNotExists(OrgDomainsMiddleware::getCurrentOrganizationId());
            if (!$this->Users->save($user_by_email)) {
                Log::error(json_encode($user_by_email->getErrors()));
            }
            if ($this->sendMailSafe('User', 'forgotPassword', [$user_by_email])) {
                $this->Flash->success(__('Pokračujte prosím otevřením odkazu v e-mailu, který jsme vám právě zaslali'));
            }
        }
    }

    public function historyFromArchive()
    {
        // hide HISTORIES records unless enabled for the organization
        if (!OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES)) {
            return [];
        }

        // get IČO and VAT from identities data and prepare query for searching in the HistoryIdentities
        $histories = array();
        $identity = $this->Identities->find('all', [
            'conditions' => [
                'name IN' => [Identity::FO_VAT_ID, Identity::PO_BUSINESS_ID, Identity::PO_VAT_ID],
                'user_id' => $this->Auth->user('id'),
                'value IS NOT' => NULL,
                'value !=' => ''
            ]
        ])->toArray();

        $userIdentities = array_fill_keys([
            'HistoryIdentities.ico IN',
            'HistoryIdentities.dic IN'
        ], []);

        foreach ($identity as $k => $v) {
            if (empty(trim($v['value']))) {
                continue;
            }
            switch ($v['name']) {
                case Identity::PO_BUSINESS_ID:
                    $userIdentities['HistoryIdentities.ico IN'][] = $v['value'];
                    break;

                case Identity::FO_VAT_ID:
                case Identity::PO_VAT_ID:
                    $userIdentities['HistoryIdentities.dic IN'][] = $v['value'];
                    break;
            }
        }

        // merge of both - current history and archived history
        if (!empty(array_filter($userIdentities))) {
            // get list of archived history
            $archive = $this->HistoryIdentities->find('all', [
                'conditions' => [
                    'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'OR' => array_filter($userIdentities)
                ],
                'contain' => [
                    'Histories',
                ],
            ])->toArray();

            $orgRange = (int)OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true);
            $yearsRange = range((int)date('Y'), (int)date('Y') - max(0, $orgRange));
            // old items filtered by year
            if (isset($archive[0]) && isset($archive[0]->histories)) {
                foreach ($yearsRange as $year) {
                    foreach ($archive[0]->histories as $v) {
                        if ((int)$v['year'] === $year) {
                            $histories[] = (object)
                            [
                                'id' => null,
                                'added_by' => __('úředník'),
                                'amount_czk' => $v['czk_amount'],
                                'fiscal_year' => $v['year'],
                                'public_income_source_id' => null,
                                'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
                                'project_name' => $v['project_name']
                            ];
                        }
                    }
                }
            }
        }

        return $histories;
    }

    public function publicIncomeHistories()
    {
        $sources = $this->PublicIncomeHistories->PublicIncomeSources->find(
            'list',
            [
                'conditions' => [
                    'OR' => [
                        'PublicIncomeSources.organization_id IS' => null,
                        'PublicIncomeSources.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    ],
                ],
            ]
        )->order(['organization_id' => 'DESC'])->toArray();
        $history = $this->PublicIncomeHistories->newEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $history = $this->PublicIncomeHistories->patchEntity(
                $history,
                [
                    'user_id' => $this->Auth->user('id'),
                ] + $this->getRequest()->getData()
            );
            if ($this->PublicIncomeHistories->save($history)) {
                $this->Flash->success(__('Přidáno úspěšně'));
            } else {
                $this->Flash->error(__('Při ukládání nastala chyba'));
            }
        }
        $histories = $this->PublicIncomeHistories->find(
            'all',
            ['conditions' => [
                'PublicIncomeHistories.user_id' => $this->Auth->user('id'),
            ], 'contain' => ['PublicIncomeSources']]
        )
            ->order(['fiscal_year' => 'DESC'])
            ->toArray();

        $allowedStates = [
            RequestState::STATE_CLOSED_FINISHED,
            RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
            RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
            RequestState::STATE_SETTLEMENT_SUBMITTED,
            RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
        ];
        $requests = $this->Requests->find(
            'all',
            [
                'conditions' => [
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                    'Requests.user_id' => $this->Auth->user('id'),
                    'Requests.request_state_id IN (' . implode(',', $allowedStates) . ')',
                ],
                'contain' => ['RequestLogs'],
            ]
        )->order(['created' => 'DESC'])
            ->toArray();

        // join historical records
        $historyFromArchive = !empty($requests) ? $this->historyFromArchive($requests[0]) : [];
        $histories = array_merge(
            $histories,
            $historyFromArchive
        );

        foreach ($requests as $k => $v) {
            if ($v['final_subsidy_amount'] <= 0) {
                continue;
            }

            $validLogs = array_filter($v->request_logs, function ($val) {
                return (int)$val['request_state_id'] === RequestState::STATE_PAID_READY_FOR_SETTLEMENT;
            });
            $lastLog = count($validLogs) > 0 ? reset($validLogs) : [];
            $logDate = !empty($lastLog) && isset($lastLog['created']) ? substr($lastLog['created']->year, 0, 4) : '';

            $histories[] = (object)
            [
                'added_by' => __('systém'),
                'amount_czk' => $v['final_subsidy_amount'],
                'fiscal_year' => !empty($logDate) ? $logDate : substr($v['created']->year, 0, 4),
                'id' => null,
                'project_name' => $v['name'],
                'public_income_source_id' => null,
                'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
            ];
        }

        // public_income_histories
        $this->set(compact('histories', 'history', 'sources'));
        if (!empty($this->request->getQuery('id'))) {
            $request_id = intval($this->request->getQuery('id'));
            $request_name = !empty($this->request->getQuery('name')) ? h($this->request->getQuery('name')) : '#' . $request_id;
            $this->set('crumbs', [__('Mé žádosti o podporu') => 'user_requests',  $request_name => 'user/requests/' . $request_id]);
        }
    }

    public function incomeHistoryDelete(int $id)
    {
        $history = $this->PublicIncomeHistories->get(
            $id,
            [
                'conditions' => [
                    'PublicIncomeHistories.user_id' => $this->Auth->user('id'),
                ],
            ]
        );
        if ($this->PublicIncomeHistories->delete($history)) {
            $this->Flash->success(__('Smazáno úspěšně'));
        } else {
            $this->Flash->error(__('Záznam nebylo možné smazat'));
        }

        return $this->redirect(['action' => 'publicIncomeHistories']);
    }

    public function inviteAccept()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }
        /**
         * @var User $user
         */
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                ],
            ]
        )->first();

        if (empty($user)) {
            $this->Flash->error(__('Neplatný odkaz pozvánky'));
            $this->redirect('/');

            return;
        }

        if ($user->password != 'INVITE') {
            $user->regenerateEmailToken();
            $this->Users->save($user);
            $this->Flash->error(__('Neplatný odkaz na pozvánku'));
            $this->redirect('/');

            return;
        }
        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $new_password = $this->getRequest()->getData('new_password');
            $new_password_check = $this->getRequest()->getData('new_password_check');
            if (empty($new_password)) {
                $user->setError('new_password', __('Heslo musí být vyplněno'));
            }
            if (empty($new_password_check)) {
                $user->setError('new_password_check', __('Heslo musí být vyplněno'));
            }
            if (strcmp($new_password, $new_password_check) != 0) {
                $user->setError('new_password', __('Hesla nejsou stejná'));
                $user->setError('new_password_check', __('Hesla nejsou stejná'));
            }
            if ($user->hasErrors()) {
                $this->Flash->error(__('Formulář obsahuje chyby'));
            } else {
                $user->regenerateSalt();
                $user->setNewPassword($new_password);
                $user->is_enabled = true;
                $user->regenerateEmailToken();
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Vaše e-mailová adresa byla ověřena. Nyní se můžete přihlásit'));
                } else {
                    $this->Flash->error(__('Nebylo možné povolit váš účet'));
                }
                $this->redirect(['_name' => 'login']);

                return;
            }
        }
        $this->set(compact('user'));
    }

    public function passwordRecoveryGo()
    {
        if ($this->Auth->user()) {
            $this->redirect('/');

            return;
        }

        /**
         * @var User $user
         */
        $user = $this->Users->find(
            'all',
            [
                'conditions' => [
                    'Users.mail_verification_code' => $this->getRequest()->getParam('token'),
                    'Users.id' => $this->getRequest()->getParam('requestId'),
                ],
            ]
        )->first();

        if (!($user instanceof User)) {
            $this->Flash->error(__('Neplatný odkaz pro obnovu hesla. Nechte si prosím zaslat nový.'));

            $this->redirect(['_name' => 'password_recovery']);

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            if (!empty($this->getRequest()->getData('password'))) {
                if (!empty($this->getRequest()->getData('password_2'))) {
                    if (strcmp($this->getRequest()->getData('password'), $this->getRequest()->getData('password_2')) !== 0) {
                        $user->setError('password', __('Hesla nejsou stejná'));
                    }
                }
                $user->set('mail_verification_code', null);
                $user->regenerateSalt();
                $user->setNewPassword($this->getRequest()->getData('password'));
                $user->is_enabled = true;
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Nové heslo nastaveno, nyní se můžete přihlásit'));
                    $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Nebylo možné nastavit nové heslo'));
                }
            }
        }

        $this->set(compact('user'));
    }

    public function changeNotifications()
    {
        $user = $this->Users->get($this->Auth->user('id'));

        if ($user->id !== $user->getOriginIdentity($this->getRequest())->id) {
            $this->Flash->error(__('Pro změnu nastavení oznámení se musíte nejdříve přepnout zpět do svého účtu'));
            $this->redirect('/');
            return;
        }

        $enabled_settings = []; // Presence of a key enables the setting, the value is the actual value

        // See 402 - turn off notifications
        if (User::userHasRoleInAnyProgram($this->getCurrentUser()->id, 'comments_programs')) {
            $identityForm = new IdentityForm();
            $identityForm->loadIdentities($this->Identities, $this->getCurrentUser());
            $notifications_disabled = boolval($identityForm->getData(Identity::NO_NOTIFICATIONS));
            $enabled_settings['no_notifications'] = $notifications_disabled;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $no_notifications = intval($this->getRequest()->getData('no_notifications'));

            if ($no_notifications === 1 || $no_notifications === 0) {
                $this->identitySetValue($this->getCurrentUser()->id, 'nonotifications', $no_notifications);
                $this->redirect('/user/notifications');
                return;
            } else {
                $flashMessage = __('Formulář obsahuje chyby');
                $this->Flash->error($flashMessage);
            }
        }

        $this->set(compact('user', 'enabled_settings'));
    }

    private function identitySetValue($userID, $propertyName, $value)
    {
        $identitiesTable = TableRegistry::getTableLocator()->get('Identities');
        $identities = $identitiesTable->find('all')
            ->where(['user_id' => $userID, 'name' => $propertyName])
            ->all();

        if (!$identities->isEmpty()) {
            $allRecordsUpdated = true;
            foreach ($identities as $identity) {
                $identity->value = $value;
                $identity->modified = FrozenTime::now();
                if (!$identitiesTable->save($identity)) {
                    $allRecordsUpdated = false;
                }
            }
            if ($allRecordsUpdated) {
                $this->Flash->success(__('Nastavení bylo aktualizováno.'));
            } else {
                $this->Flash->error(__('Některá nastavení se nepodařilo aktualizovat.'));
            }
        } else {
            // No records found, insert a new one
            $newIdentity = $identitiesTable->newEntity([
                'user_id' => $userID,
                'name' => $propertyName,
                'value' => $value,
                'created' => FrozenTime::now(),
                'modified' => FrozenTime::now()
            ]);

            if ($identitiesTable->save($newIdentity)) {
                $this->Flash->success(__('Nastavení bylo uloženo.'));
            } else {
                $this->Flash->error(__('Některá nastavení se nepodařilo uložit.'));
            }
        }
    }
}
