<?php

declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\Form;
use App\Model\Entity\FormField;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\FormSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestFilledField;
use App\Model\InstanceReportInterface;
use App\Model\Table\FormFieldsTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Event\EventManager;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\View\Helper\FormHelper;

class SocialServiceBudgetFormController extends AbstractFormController implements InstanceReportInterface
{

    const FORM_FIELD_NAME = 'SocialServiceBudget';
    public const SETTING_COLUMN_1_YEAR = 'year.1';
    public const SETTING_COLUMN_2_YEAR = 'year.2';
    public const SETTING_SHOW_YEAR_2 = 'show_year_2';
    public const COLUMN_EXPECTED_EXPENSES = 1,
        COLUMN_EXPENSES_TO_SECTION_RATIO = 5,
        COLUMN_SUBSIDY_REQUEST = 3,
        COLUMN_COMMENT = 4,
        COLUMN_SUBSIDY_TO_EXPENSES_RATIO = 2,
        COLUMN_SUBSIDY_REQUEST_YEAR_2 = 6;
    static array $YEAR_DEFAULTS = [];
    private bool $_readOnly = false;
    private array $_sections = [];
    private ?FormField $_formField = null;
    private array $_colSums = [];

    public function __construct(Form $formDefinition, Request $userRequest = null, EventManager $eventManager = null)
    {
        self::$YEAR_DEFAULTS = [
            self::SETTING_COLUMN_1_YEAR => date('Y', strtotime('+1 year')),
            self::SETTING_COLUMN_2_YEAR => date('Y', strtotime('+2 years')),
        ];
        parent::__construct($formDefinition, $userRequest, $eventManager);
    }

    public function setup(): self
    {
        /** @var FormFieldsTable $formFieldsTable */
        $formFieldsTable = $this->getTableLocator()->get('FormFields');

        $this->_formField = $formFieldsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'is_required' => true,
            'form_field_type_id' => FormFieldType::FIELD_TEXT,
            'field_order' => 1,
        ], function ($entity) {
            $entity->set('name', self::FORM_FIELD_NAME);
        });

        if ($this->_formField->name !== self::FORM_FIELD_NAME) {
            $this->_formField->name = self::FORM_FIELD_NAME;
            $this->_formField = $formFieldsTable->save($this->_formField);
        }

        $formSettingsTable = $this->getTableLocator()->get('FormSettings');
        foreach (self::$YEAR_DEFAULTS as $setting_year_name => $setting_year_default_value) {
            $this->_settings[$setting_year_name] = $formSettingsTable->findOrCreate([
                'form_id' => $this->getFormDefinition()->id,
                'name' => $setting_year_name
            ], function (FormSetting $yearSetting) use ($setting_year_default_value) {
                $yearSetting->value = $setting_year_default_value;
            });
        }
        $this->_settings[self::SETTING_SHOW_YEAR_2] = $formSettingsTable->findOrCreate([
            'form_id' => $this->getFormDefinition()->id,
            'name' => self::SETTING_SHOW_YEAR_2
        ], function (FormSetting $showYear2) {
            $showYear2->value = 0;
        });

        return $this;
    }

    public function render(AppView $appView, array $options = []): ?string
    {
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $this->_readOnly = Hash::get($options, 'readOnly') === true;

        $return = $appView->element('Forms/social_service_budget_form', [
            'form' => $this,
            'request' => $this->getUserRequest(),
            'enableInput' => !$this->_readOnly,
        ]);

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    public function renderFormSettings(AppView $appView): string
    {
        return $appView->element('Forms/social_service_budget_settings', [
            'form' => $this,
        ]);
    }

    public function isFormFilledCompletely(?Request $request = null): bool
    {
        $this->sumColumn(self::COLUMN_EXPECTED_EXPENSES);
        $this->sumColumn(self::COLUMN_SUBSIDY_REQUEST);

        return !empty($this->_colSums[self::COLUMN_EXPECTED_EXPENSES]) && !empty($this->_colSums[self::COLUMN_SUBSIDY_REQUEST]);
    }

    public function sumColumn(int $column, bool $formatAsCurrency = true): string
    {
        if (!empty($this->_colSums[$column])) {
            return $formatAsCurrency ? Number::currency($this->_colSums[$column], 'CZK') : '0';
        }
        $this->markSections($this->getTables()['sections']);
        $sum = 0;
        $colString = strval($column);
        foreach ($this->_data as $key => $value) {
            if (strrpos($key, $colString) === strlen($key) - 1) {
                if (!in_array(substr($key, 0, strrpos($key, '.')), $this->_sections, true)) {
                    if (is_numeric($value)) {
                        $sum += $value;
                    }
                }
            }
        }
        $this->_colSums[$column] = $sum;

        return $formatAsCurrency ? Number::currency($this->_colSums[$column], 'CZK') : strval($this->_colSums[$column]);
    }

    public function markSections(array $sectionData = [], string $prefix = '', bool $recurse = false): array
    {
        if ($recurse || empty($this->_sections)) {
            $counter = 0;
            foreach ($sectionData as $key => $value) {
                $counter++;
                $section = sprintf("%s%d", $prefix, $counter);
                if (is_array($value)) {
                    $this->_sections[] = $section;
                    $this->markSections($value, sprintf("%s%d.", $prefix, $counter), true);
                }
            }
        }

        return $this->_sections;
    }

    public function getTables()
    {
        $year1 = $this->getSettingValue(self::SETTING_COLUMN_1_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_1_YEAR]);

        $headers = [
            __('Nákladová položka') => '2',
            sprintf("%s<br/>%s %s", __('Plánované náklady (rozpočet služby)'), __('Rok'), $year1) => '',
            __('Procentní vyjádření podílu položky v celkovém rozpočtu') => '',
            sprintf("%s<br/>%s %s", __('Požadavek na dotaci'), __('Rok'), $year1) => '',
            __('Procentní vyjádření nákladů k celkovému rozpočtu služby') => ''
        ];
        if ($this->getSettingValue(self::SETTING_SHOW_YEAR_2, false)) {
            $year2 = $this->getSettingValue(self::SETTING_COLUMN_2_YEAR, self::$YEAR_DEFAULTS[self::SETTING_COLUMN_2_YEAR]);
            $headers[sprintf("%s<br/>%s %s", __('Požadavek na dotaci'), __('Rok'), $year2)] = '';
        }
        $headers[__('Poznámka / slovní komentář')] = '';

        return [
            'headers' => $headers,
            'sumrow' => __('Celkové náklady na realizaci služby (projektu)'),
            'sections' => [
                __('Provozní náklady celkem') => [
                    __('Materiální náklady celkem') => [
                        __('Potraviny'),
                        __('Kancelářské potřeby'),
                        __('Vybavení (DDHIM do 40 tis. Kč)'),
                        __('Pohonné hmoty'),
                        __('Jiné (popište do poznámky)'),
                    ],
                    __('Nemateriálové náklady') => [
                        __('Energie') => [
                            __('Elektřina'),
                            __('Plyn'),
                            __('Vodné a stočné'),
                            __('Jiné (popište do poznámky)'),
                        ],
                        __('Opravy a udržování') => [
                            __('Opravy a udržování budov'),
                            __('Opravy a udržování vozidel'),
                            __('Jiné (popište do poznámky)'),
                        ],
                        __('Cestovné zaměstnanců'),
                        __('Ostatní služby') => [
                            __('Telefony'),
                            __('Poštovné'),
                            __('Ostatní spoje'),
                            __('Nájemné'),
                            __('Stravovací služby'),
                            __('Právní a ekonomické služby'),
                            __('Školení a kurzy'),
                            __('Pořízení (DNIM do 60 tis. Kč)'),
                            __('Jiné (popište do poznámky)'),
                        ],
                    ],
                    __('Jiné provozní náklady') => [
                        __('Odpisy'),
                        __('Jiné (popište do poznámky)'),
                    ],
                    __('Finanční náklady') => [
                        __('Daně a poplatky'),
                        __('Jiné (popište do poznámky)'),
                    ],
                ],
                __('Osobní náklady celkem') => [
                    __('Mzdové náklady') => [
                        __('Hrubé mzdy'),
                        __('OON na DPČ'),
                        __('OON na DPP'),
                        __('Ostatní mzdové náklady'),
                    ],
                    __('Odvody na sociální a zdravotní pojištění') => [
                        __('Pojistné ke mzdám'),
                        __('Pojistné k DPČ'),
                        __('Ostatní pojistné'),
                    ],
                    __('Ostatní sociální náklady'),
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function prefill(Request $request): self
    {
        $this->setUserRequest($request);

        $this->_colSums = [];
        $this->_sections = [];

        foreach ($this->getFilledFields($request->id, $request) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$field->id] = $field->value;
                $this->_data = unserialize($field->value);
            }
        }

        $this->markSections($this->getTables()['sections']);

        return $this;
    }

    public function renderSections(FormHelper $formHelper, array $sectionData = [], string $prefix = '')
    {
        $counter = 0;
        $showYear2 = $this->getSettingValue(self::SETTING_SHOW_YEAR_2, false);
        foreach ($sectionData as $key => $value) {
            $counter++;
            $isHeader = false;
            $title = null;
            $section = sprintf("%s%d", $prefix, $counter);
            if (is_string($value)) {
                $title = $value;
            } elseif (is_array($value)) {
                $title = $key;
                $isHeader = true;
            }
?>
            <tr class="<?= $isHeader ? 'tr-sums' : 'tr-inputs' ?>" data-sum-prefix="<?= $section ?>">
                <td class="text-left <?= $isHeader ? 'font-weight-bold' : '' ?>"><?= $section ?></td>
                <td class="font-weight-bold text-left"><?= $title ?></td>
                <td class="text-right"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_EXPECTED_EXPENSES, $isHeader) ?></td>
                <td class="text-center"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_EXPENSES_TO_SECTION_RATIO, $isHeader) ?></td>
                <td class="text-right"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_SUBSIDY_REQUEST, $isHeader) ?></td>
                <td class="text-center"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO, $isHeader) ?></td>
                <?php if ($showYear2) : ?>
                    <td class="text-right"><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_SUBSIDY_REQUEST_YEAR_2, $isHeader) ?></td>
                <?php endif; ?>
                <td><?= $this->fieldOrFormatted($formHelper, $section, self::COLUMN_COMMENT, $isHeader) ?></td>
            </tr>
<?php

            if ($isHeader) {
                $this->renderSections($formHelper, $value, sprintf("%s%d.", $prefix, $counter));
            }
        }
    }

    public function fieldOrFormatted(FormHelper $formHelper, string $section, int $column, bool $isHeader = false): string
    {
        $dataKey = sprintf("%s.%d", $section, $column);
        if ($this->_readOnly || $isHeader) {
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                case self::COLUMN_SUBSIDY_REQUEST_YEAR_2:
                    return Number::currency($this->getData($dataKey) ?? 0, 'CZK');
                case self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO:
                    return $this->getRatio($section);
                case self::COLUMN_EXPENSES_TO_SECTION_RATIO:
                    return $this->getSectionRatio($section);
                case self::COLUMN_COMMENT:
                    return h(strval($this->getData($dataKey) ?? ''));
            }
        } else {
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                case self::COLUMN_SUBSIDY_REQUEST_YEAR_2:
                    return sprintf(
                        '<div class="input-group">%s<div class="input-group-append"><span class="input-group-text">Kč</span></div></div>',
                        $formHelper->control($dataKey, ['type' => 'number', 'step' => .01, 'default' => 0])
                    );
                case self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO:
                    return $this->getRatio($section);
                case self::COLUMN_EXPENSES_TO_SECTION_RATIO:
                    return $this->getSectionRatio($section);
                case self::COLUMN_COMMENT:
                    return $formHelper->control($dataKey, ['type' => 'string', 'maxlength' => 255]);
            }
        }

        return 'invalid';
    }

    public function getData($field = null)
    {
        return $this->_data[$field] ?? parent::getData($field);
    }

    public function getRatio(string $section)
    {
        $data_a = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_EXPECTED_EXPENSES)));
        $data_b = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_SUBSIDY_REQUEST)));

        if (empty($data_a) || empty($data_b)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($data_b / $data_a) * 100);
    }

    public function getSectionRatio(string $section)
    {
        $row_value = intval($this->getData(sprintf("%s.%d", $section, self::COLUMN_EXPECTED_EXPENSES)));
        $this->sumColumn(self::COLUMN_EXPECTED_EXPENSES);
        $parent_section_sum = intval($this->_colSums[self::COLUMN_EXPECTED_EXPENSES]);

        if (empty($row_value) || empty($parent_section_sum)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($row_value / $parent_section_sum) * 100);
    }

    public function getOverallRatio(): string
    {
        $this->sumColumn(3);
        $this->sumColumn(1);

        $colsum3 = $this->_colSums[3];
        $colsum1 = $this->_colSums[1];

        if (empty($colsum3) || empty($colsum1)) {
            return Number::toPercentage(0);
        }

        return Number::toPercentage(($colsum3 / $colsum1) * 100);
    }

    protected function _execute(array $data)
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');

        $flat = Hash::flatten($data);
        $sectionSums = [];

        foreach ($flat as $rowColumn => $value) {
            $column = intval(mb_substr($rowColumn, -1));
            switch ($column) {
                case self::COLUMN_EXPECTED_EXPENSES:
                case self::COLUMN_SUBSIDY_REQUEST:
                case self::COLUMN_SUBSIDY_REQUEST_YEAR_2:
                    $flat[$rowColumn] = round(floatval($value), 2);
                    $sectionSums = $this->addToSectionSums($sectionSums, $rowColumn, $flat[$rowColumn]);
                    break;
                case self::COLUMN_COMMENT:
                    $flat[$rowColumn] = mb_substr($value, 0, 254);
                    break;
            }
        }

        $flat = array_merge($flat, $sectionSums);

        $filledField = $filledFieldsTable->findOrCreate([
            'request_id' => $this->getUserRequest()->id,
            'form_id' => $this->getFormDefinition()->id,
            'form_field_id' => $this->_formField->id,
        ]);
        $filledField->value = serialize($flat);

        return $filledFieldsTable->save($filledField) instanceof RequestFilledField;
    }

    public function addToSectionSums(array $existingSums, string $valueAddress, float $value)
    {
        $targetColumn = mb_substr($valueAddress, -1);
        $section = $valueAddress;
        while ($section !== null) {
            if (in_array($section, $this->_sections, true)) {
                $existingAddress = sprintf("%s.%s", $section, $targetColumn);
                $existingSums[$existingAddress] = ($existingSums[$existingAddress] ?? 0) + $value;
            }
            $dotPosition = strrpos($section, '.');
            if ($dotPosition === false) {
                $section = null;
            } else {
                $section = mb_substr($section, 0, $dotPosition);
            }
            if (empty($section)) {
                $section = null;
            }
        }

        return $existingSums;
    }

    private function transforSectionsForReportColumns(array $headers, ?array $sectionData = [], string $prefix = ''): array
    {
        $rtn = [];
        $counter = 1;
        foreach ($sectionData as $key => $value) {
            $thisKey = sprintf("%s%d.", $prefix, $counter);
            if (is_array($value)) {
                $rtn[$key] = $this->transforSectionsForReportColumns($headers, $value, $thisKey);
            } else {
                $rtn[$value] = [];
                foreach ($headers as $headerIndex => $headerTitle) {
                    $rtn[$value][$thisKey . $headerIndex] = $headerTitle;
                }
            }
            $counter++;
        }
        return $rtn;
    }

    public function getReportColumns(): array
    {
        $tablePrefix = sprintf("form.%d.", $this->getFormDefinition()->id);
        $tableDefinition = $this->getTables();
        $headers = array_keys($tableDefinition['headers']);
        $map = [
            0 => 0,
            1 => self::COLUMN_EXPECTED_EXPENSES,
            2 => self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO,
            3 => self::COLUMN_SUBSIDY_REQUEST,
            4 => self::COLUMN_EXPENSES_TO_SECTION_RATIO,
        ];
        if ($this->getSettingValue(self::SETTING_SHOW_YEAR_2, false)) {
            $map[6] = self::COLUMN_COMMENT;
            $map[5] = self::COLUMN_SUBSIDY_REQUEST_YEAR_2;
        } else {
            $map[5] = self::COLUMN_COMMENT;
        }
        foreach ($headers as $key => $header) {
            $headers[$map[$key] ?? -1] = strip_tags(str_replace('<', ' <', $header));
        }
        unset($headers[0]);

        $sections = $this->transforSectionsForReportColumns($headers, $tableDefinition['sections'], $tablePrefix);
        $sumrow = [];
        foreach ($headers as $headerIndex => $headerTitle) {
            $sumrow[sprintf("%ssumrow.%d", $tablePrefix, $headerIndex)] = $headerTitle;
        }

        // seznam, který se exportuje do Sestav
        $sections[$tableDefinition['sumrow']] = $sumrow;
        return $sections;
    }

    public function extractColumn(Request $request, string $column): ?array
    {
        list($form, $form_id, $key) = explode(".", $column, 3);

        $colIndex = explode(".", $key);
        $colIndex = intval(end($colIndex));

        if (in_array($colIndex, [self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO, self::COLUMN_EXPENSES_TO_SECTION_RATIO], true)) {
            if (startsWith($key, 'sumrow') && $colIndex === self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO) {
                return [
                    $this->getOverallRatio()
                ];
            }
            if ($colIndex === self::COLUMN_EXPENSES_TO_SECTION_RATIO) {
                return [
                    $this->getRatio(mb_substr($key, 0, -2))
                ];
            }
            if ($colIndex === self::COLUMN_SUBSIDY_TO_EXPENSES_RATIO) {
                return [
                    $this->getSectionRatio(mb_substr($key, 0, -2))
                ];
            }
        }

        if (startsWith($key, 'sumrow')) {
            return [
                $this->sumColumn($colIndex, false)
            ];
        }

        return [
            $this->getData()[$key] ?? ''
        ];
    }

    public function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public function getColumnHeaders(string $path, string $columnName): ?array
    {
        return null;
    }
}
