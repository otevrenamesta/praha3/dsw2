<?php

namespace App\Form;

use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Model\Table\CsuCountriesTable;
use App\Model\Table\CsuLegalFormsTable;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\IdentityAttachmentsTable;
use App\Model\Table\IdentityAttachmentTypesTable;
use App\View\Helper\FormHelper;
use Cake\Database\Query;
use Cake\Form\Form;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Throwable;

class IdentityForm extends Form
{
    public const MAX_NUMBER_OF_FILLED_INTERESTS = 20;
    public const MAX_NUMBER_OF_OWNED_INTERESTS = 20;
    public const MAX_NUMBER_OF_STATUTORY_MEMBERS = 20;

    private ?FormHelper $formHelper = null;
    private ?CsuCountriesTable $tableCountries = null;
    private ?CsuMunicipalitiesTable $tableCities = null;
    private ?CsuMunicipalityPartsTable $tableCityParts = null;
    private ?CsuLegalFormsTable $tableLegalForms = null;
    private ?IdentitiesTable $tableIdentities = null;
    /**
     * @var $identities Identity[]
     */
    private array $identities = [];

    private int $userId = 0;
    private int $_interestsCount = 0;
    private int $_ownedInterestsCount = 0;
    private int $_statutoriesCount = 0;

    public function setFormHelper(FormHelper $helper): void
    {
        $this->formHelper = $helper;
    }

    public function getNumberOfFilledInterests(): int
    {
        return $this->_interestsCount;
    }

    public function getNumberOfFilledOwnedInterests(): int
    {
        return $this->_ownedInterestsCount;
    }

    public function getNumberOfStatutoryMembers(): int
    {
        return $this->_statutoriesCount;
    }

    public function control(string $fieldId, array $options = []): void
    {
        $fieldIdSpecific = $fieldId;
        if (in_array($fieldId, array_merge(Identity::FULL_INTEREST_TEMPLATE, Identity::FULL_OWNED_INTERESTS_TEMPLATE, Identity::FULL_STATUTORY_TEMPLATE), true)) {
            $fieldIdSpecific = sprintf($fieldId, $options['row'] ?? 0);
        }

        $identity_options = [
            'label' => Identity::getFieldLabel($fieldId),
            'type' => Identity::getFieldFormControlType($fieldId),
            'placeholder' => Identity::getFieldFormPlaceholder($fieldId),
            'default' => Identity::getFieldFormDefaultValue($fieldId),
            'empty' => Identity::getFieldFormEmpty($fieldId),
            'value' => $this->getData($fieldIdSpecific),
            // do not require on each fill, validate ex-post
            // 'required' => Identity::getFieldFormIsRequired($fieldId),
            'class' => (
                Identity::getFieldFormIsRequired(
                    $fieldId,
                    $this->identities[Identity::IS_PO] ?? 0,
                    $this->identities[Identity::PO_CORPORATE_TYPE] ?? 0
                )
                && empty($this->getData($fieldId))
            ) ?
                'is-invalid' : '',
        ];
        if ($identity_options['type'] === 'select') {
            switch ($fieldId) {
                case Identity::INTEREST_X_COUNTRY:
                case Identity::OWNED_X_COUNTRY:
                case Identity::RESIDENCE_ADDRESS_CITY:
                case Identity::POSTAL_ADDRESS_CITY:
                    $identity_options['class'] .= ' select2search';
                    $identity_options['options'] = $this->getSelectValues($fieldId);
                    break;
                case Identity::RESIDENCE_ADDRESS_CITY_PART:
                case Identity::POSTAL_ADDRESS_CITY_PART:
                    $identity_options['class'] = 'city-parts-sideload';
                    if (!empty($identity_options['value'])) {
                        $identity_options['options'] = $this->tableCityParts->find('list', ['keyField' => 'number', 'conditions' => ['number' => $identity_options['value']]])->order('name');
                    }
                    break;
                case Identity::PO_CORPORATE_TYPE:
                    $identity_options['class'] .= ' select2';
                    $identity_options['options'] = $this->getSelectValues($fieldId);
                    break;
            }
        } elseif ($identity_options['type'] === 'checkbox') {
            $identity_options['checked'] = boolval($identity_options['value']);
            unset($identity_options['value']);
        }
        echo $this->formHelper->control($fieldIdSpecific, $options + $identity_options);
    }

    public function getSelectValues(string $fieldId, bool $orderValues = true, bool $throwOnInvalidFieldId = true)
    {
        $order = $orderValues ? 'name' : false;

        switch ($fieldId) {
            case Identity::INTEREST_X_COUNTRY:
            case Identity::OWNED_X_COUNTRY:
                return $this->tableCountries->find('list', ['keyField' => 'number'])->order($order);
            case Identity::RESIDENCE_ADDRESS_CITY:
            case Identity::POSTAL_ADDRESS_CITY:
                return $this->tableCities->find('list', ['keyField' => 'number'])->order($order);
            case Identity::PO_CORPORATE_TYPE:
                return $this->tableLegalForms->find('list', ['keyField' => 'number'])->order($order);
            case Identity::RESIDENCE_ADDRESS_CITY_PART:
            case Identity::POSTAL_ADDRESS_CITY_PART:
                return $this->tableCityParts->find('list', ['keyField' => 'number'])->order($order);
        }
        if ($throwOnInvalidFieldId) {
            throw new NotFoundException($fieldId);
        }

        return [];
    }

    public function getSelectsCurrentValues(): array
    {
        $rtn = [];
        foreach (Identity::SELECTS as $selectFormFieldId) {
            // normalize key to match form control id
            $rtn[str_replace('_', '-', str_replace('.', '-', $selectFormFieldId))] = $this->getData($selectFormFieldId);
        }

        return $rtn;
    }

    public function getCitiesToPartsMap()
    {
        return $this->tableCityParts->CsuMunicipalitiesToCsuMunicipalityParts->find(
            'all',
            [
                'fields' => [
                    'csu_municipalities_number',
                    'csu_municipality_parts_number',
                ],
            ]
        )->enableHydration(false)->toArray();
    }

    public function validationDefault(Validator $validator)
    {
        // [+] minimum age validation
        list($group, $key) = explode('.', Identity::FO_DATE_OF_BIRTH);
        $minimumAgeRequirement = OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_FO_MINIMUM_AGE, true);
        $minimumAgeErrorMessage = sprintf(__('Minimální věk žadatele, fyzické osoby, musí být alespoň %d let'), $minimumAgeRequirement);
        $foValidator = new Validator();
        $foValidator->add($key, 'minimum_age', [
            'rule' => function ($value) use ($minimumAgeErrorMessage, $minimumAgeRequirement) {
                $dateVal = FrozenDate::parseDate($value, 'Y-m-d');
                $years = $dateVal->diffInYears(FrozenDate::now(), false);
                if ($years < $minimumAgeRequirement) {
                    return $minimumAgeErrorMessage;
                }
                return true;
            },
            'message' => $minimumAgeErrorMessage,
        ]);
        $validator->addNested($group, $foValidator, null, function ($context) {
            // validate with foValidator if IS_PO is false
            return !boolval(Hash::get($context['data'] ?? [], Identity::IS_PO));
        });
        // [-] minimum age validation

        return $validator;
    }

    public function setCountries(CsuCountriesTable $CsuCountries)
    {
        $this->tableCountries = $CsuCountries;
    }

    public function setCities(CsuMunicipalitiesTable $CsuMunicipalities)
    {
        $this->tableCities = $CsuMunicipalities;
    }

    public function setCityParts(CsuMunicipalityPartsTable $CsuMunicipalityParts)
    {
        $this->tableCityParts = $CsuMunicipalityParts;
    }

    public function setLegalForms(CsuLegalFormsTable $CsuLegalForms)
    {
        $this->tableLegalForms = $CsuLegalForms;
    }

    /**
     * Load Identity attachments 
     * 
     * @param IdentityAttachmentsTable $IdentityAttachments
     * @param User $user
     * 
     */

    public function loadAttachments(IdentityAttachmentsTable $IdentityAttachments, User $user) {
        $this->identity_attachments = $IdentityAttachments->find('all', [
            'conditions' => [
                'IdentityAttachments.user_id' => $user->id,
            ],
            'contain' => ['IdentityAttachmentTypes','Files']
        ])->order(['IdentityAttachments.is_archived' => 'ASC', 'IdentityAttachments.modified' => 'DESC'])->toArray();
    }

    public function loadAttachmentTypes(IdentityAttachmentTypesTable $IdentityAttachmentTypes) {
        $this->identity_attachment_types = $IdentityAttachmentTypes->find('all', [])->order(['IdentityAttachmentTypes.order' => 'ASC'])->toArray();
    }

    public function loadIdentities(IdentitiesTable $Identities, User $user)
    {
        $this->identities = $Identities->find('all', [
            'conditions' => [
                'Identities.user_id' => $user->id,
            ],
        ])->order(['Identities.version' => 'ASC'])->toArray();
        $this->userId = $user->id;
        $this->tableIdentities = $Identities;

        $dataFromIdentities = [];
        foreach ($this->identities as $identity) {
            $dataFromIdentities[$identity->name] = $identity->value;
        }

        for ($i = 0; $i < max(self::MAX_NUMBER_OF_FILLED_INTERESTS, self::MAX_NUMBER_OF_OWNED_INTERESTS, self::MAX_NUMBER_OF_STATUTORY_MEMBERS); $i++) {
            $interestData = [];
            $ownedInterestData = [];
            $statutoryData = [];
            foreach (Identity::FULL_INTEREST_TEMPLATE as $interestField) {
                $interestData[$interestField] = $dataFromIdentities[sprintf($interestField, $i)] ?? null;
            }
            foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestField) {
                $ownedInterestData[$interestField] = $dataFromIdentities[sprintf($interestField, $i)] ?? null;
            }
            foreach (Identity::FULL_STATUTORY_TEMPLATE as $statutoryField) {
                $statutoryData[$statutoryField] = $dataFromIdentities[sprintf($statutoryField, $i)] ?? null;
            }
            // if one missing and the row is not completely empty, indicate error
            if (!empty(trim(join($interestData)))) {
                $this->_interestsCount++;
            }
            if (!empty(trim(join($ownedInterestData)))) {
                $this->_ownedInterestsCount++;
            }
            if (!empty(trim(join($statutoryData)))) {
                $this->_statutoriesCount++;
            }
        }

        $data = Hash::expand($dataFromIdentities);
        $this->setData($data);
    }

    protected function _execute(array $data)
    {

        $this->setData($data);
        $flat = Hash::flatten($data);

        $entities = [];
        $errors = [];
        $poCorporateType = intval($flat[Identity::PO_CORPORATE_TYPE] ?? 0);
        $poCorporateType = $poCorporateType > 0 ? $poCorporateType : null;
        $identityRequiredFields = Identity::getRequiredFieldIds(
            $flat[Identity::IS_PO] ?? false,
            $poCorporateType
        );

        // validate interests in PO filled rows
        $interestRequiredFields = [
           // Identity::INTEREST_X_SIZE,
            Identity::INTEREST_X_OWNER,
            Identity::INTEREST_X_COUNTRY,
        ];


        for ($i = 0; $i < self::MAX_NUMBER_OF_FILLED_INTERESTS; $i++) {
            $interestData = [];
            $valid = true;
            foreach (Identity::FULL_INTEREST_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $interestData[$flatKey] = $flat[$flatKey] ?? null;
                if (in_array($interestField, $interestRequiredFields, true) && empty(trim($interestData[$flatKey]))) {
                    $valid = false;
                }
            }

            // if one missing and the row is not completely empty, indicate error
            if (!$valid && !empty(trim(join($interestData)))) {
                $this->_interestsCount++;
                foreach ($interestData as $key => $value) {
                    $errors[$key] = __('Není platně vyplněno');
                }
            }
        }

        // validate owned interests filled rows
        $ownedInterestsRequiredFields = Identity::FULL_OWNED_INTERESTS_TEMPLATE;
        if (false !== ($pos = array_search(Identity::OWNED_X_SIZE, $ownedInterestsRequiredFields))) {
            unset($ownedInterestsRequiredFields[$pos]);
        }

        $interestDataAll = [];
        for ($i = 0; $i < self::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) {
            $interestData = [];
            $valid = true;
            foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $interestData[$flatKey] = $flat[$flatKey] ?? null;
                if (in_array($interestField, $ownedInterestsRequiredFields, true) && empty(trim($interestData[$flatKey]))) {
                    $valid = false;
                }
            }

            if (!$valid && !empty(trim(join($interestData)))) {
                $this->_ownedInterestsCount++;
                foreach ($interestData as $key => $value) {
                    $errors[$key] = __('Není platně vyplněno');
                }
            }

            $interestDataAll = array_merge($interestDataAll, $interestData);
        }

        // if "another share" is checked, validate with "owned interests"
        if (!empty(trim(join($interestDataAll))) && isset($flat[Identity::PO_ANOTHER_SHARE]) && boolval($flat[Identity::PO_ANOTHER_SHARE]) === true) {
            $errors[ Identity::PO_ANOTHER_SHARE ] = __('Odeberte právnické osoby, v nichž má žadatel přímý podíl (zobrazíte je odškrtuntím tohoho pole)');
        }

        // validate statutories
        $statutoryRequiredFields = [
            Identity::STATUTORY_X_FULLNAME,
            Identity::STATUTORY_X_REPRESENTATION_REASON
        ];
        $statutoryRowsFilled = [];
        for ($i = 0; $i < self::MAX_NUMBER_OF_STATUTORY_MEMBERS; $i++) {
            $statutoryData = [];
            $valid = true;
            foreach (Identity::FULL_STATUTORY_TEMPLATE as $statutoryField) {
                $flatKey = sprintf($statutoryField, $i);
                $statutoryData[$flatKey] = $flat[$flatKey] ?? null;
                if (in_array($statutoryField, $statutoryRequiredFields, true) && empty(trim($statutoryData[$flatKey]))) {
                    $valid = false;
                }
            }
            // if one missing and the row is not completely empty, indicate error
            if (!empty(trim(join($statutoryData)))) {
                // row has data
                if (!$valid) {
                    $this->_statutoriesCount++;
                    foreach ($statutoryData as $key => $value) {
                        $errors[$key] = __('Není platně vyplněno');
                    }
                } else {
                    $statutoryRowsFilled[] = $i;
                }
            }
        }

        // validate first statutory, which is persisted by different keys
        $firstStatutoryData = [];
        $firstStatutoryValid = true;
        foreach (Identity::ALL_STATUTORY as $firstStatutoryField) {
            $firstStatutoryData[$firstStatutoryField] = $flat[$firstStatutoryField];
            if (empty(trim($firstStatutoryData[$firstStatutoryField]))) {
                $firstStatutoryValid = false;
            }
        }
        $firstStatutoryFilled = !empty(trim(join($firstStatutoryData)));

        foreach (Identity::ALL_STATUTORY as $firstStatutoryField) {
            if (in_array($firstStatutoryField, $identityRequiredFields, true)) {
                if (!$firstStatutoryFilled && count($statutoryRowsFilled) >= 1) {
                    // case when statutories has filled second row and not the first one
                    $errors[$firstStatutoryField] = __('První řádek musí být vyplněn');
                } elseif (!$firstStatutoryValid && empty(trim($firstStatutoryData[$firstStatutoryField] ?? ''))) {
                    $errors[$firstStatutoryField] = __('Musí být vyplněno');
                }
            }
        }

        $this->setErrors(Hash::expand($errors));

        $targetIdentityVersion = 1;
        foreach ($this->identities as $identity) {
            if ($identity->version > $targetIdentityVersion) {
                $targetIdentityVersion = $identity->version;
            }

            if ($identity->version === $targetIdentityVersion && $identity->is_locked) {
                $targetIdentityVersion++;
            }
        }

        // then do the normal fields
        foreach ($flat as $fieldId => $value) {
            if (in_array($fieldId, Identity::ALL_VERIFIED_INFO)) {
                // this form does not allow saving/over-writing verified ISDS info
                continue;
            }

            if (!empty($value) && Identity::getFieldFormControlType($fieldId) === 'select') {
                // validate FK
                $values = $this->getSelectValues($fieldId, false, false);
                if ($values instanceof Query) {
                    $valuesArray = $values->toArray();
                    if (!in_array(intval($value), array_keys($valuesArray), true)) {
                        $this->addError($fieldId, __('Byla vybrána neplatná hodnota'));
                        continue;
                    }
                }
            }

            foreach ($this->identities as $identity) {
                if ($identity->name === $fieldId && $identity->version === $targetIdentityVersion && $identity->is_locked === false) {
                    // Log::debug(sprintf('patching existing identity %s value %s', $fieldId, Identity::serialize($fieldId, $value)));
                    $identity->set('value', Identity::serialize($fieldId, $value));
                    $entities[] = $identity;
                    continue 2;
                }
            }

            if (Identity::isFieldValueEmpty($fieldId, $value) || isset($errors[$fieldId])) {
                // not creating new entities for empty values
                // Log::debug(sprintf('Not creating empty for id: %s value: %s', $fieldId, $value));
                continue;
            }

            $new_identity = $this->tableIdentities->newEntity(
                [
                    'name' => $fieldId,
                    'value' => $value,
                    'user_id' => $this->userId,
                    'is_locked' => false,
                    'version' => $targetIdentityVersion,
                ]
            );
            if (!$new_identity->hasErrors()) {
                $entities[] = $new_identity;
            } else {
                Log::error('new_identity has errors ' . json_encode($new_identity->getErrors()));
            }
        }

        try {
            foreach ($entities as $entity) {
                if (!$this->tableIdentities->save($entity)) {
                    Log::error('identity could not be saved ' . json_encode($entity->getErrors()));

                    return false;
                }
            }
        } catch (Throwable $e) {
            Log::error('identity saving produced exception ' . $e->getMessage());
            Log::error($e->getTraceAsString());

            return false;
        }

        return true;
    }

    public function addError(string $key, string $error)
    {
        $errors = Hash::flatten($this->getErrors());
        $errors[$key] = $error;
        $this->setErrors(Hash::expand($errors));
    }
}
