<?php
declare(strict_types=1);

namespace App\Form;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Identity;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestType;
use App\Model\Entity\User;
use App\Model\Table\CsuMunicipalityPartsTable;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\RequestsTable;
use App\View\Helper\FormHelper;
use Cake\Form\Form;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use InvalidArgumentException;

class PaperRequestForm extends Form
{
    use LocatorAwareTrait;

    private ?FormHelper $_formHelper = null;
    private ?Request $_request = null;
    private array $_allowed_programs = [];
    private array $_program_id_to_appeal_id = [];

    public function getCurrentNewOrFilledState(): int
    {
        foreach ($this->getValidations() as $bool) {
            if ($bool !== true) {
                return RequestState::STATE_NEW;
            }
        }
        return RequestState::STATE_READY_TO_SUBMIT;
    }

    public function getValidations(): array
    {
        $containedFormsValid = true;
        foreach ($this->getRequest()->getForms() as $request_form) {
            $formController = $request_form->getFormController($this->getRequest());
            $containedFormsValid = $containedFormsValid && $formController->isFormFilledCompletely();
        }

        return [
            // has program and appeal set
            !empty($this->getRequest()->program_id) && !empty($this->getRequest()->appeal_id),
            // is named
            !empty($this->getRequest()->name),
            // identity is completed
            empty($this->getRequest()->user->getIdentityMissingFields($this->getRequest()->user_identity_version, intval($this->getRequest()->request_type_id))),
            // budget validates correctly
            $this->getRequest()->validateBudgetRequirements() === RequestBudget::BUDGET_OK,
            // all contained forms are valid
            $containedFormsValid,
        ];
    }

    public function setRequest(Request $request, User $currentUser): self
    {
        $request->loadUser();
        // do not change the currently assigned user on edit
        if (empty($request->user)) {
            $request->user = $currentUser;
            $request->user_id = $currentUser->id;
            $request->paper_evidenced_by_user_id = $currentUser->id;
        }
        // preset paper form
        if ($request->isNew()) {
            $request->request_type_id = RequestType::PAPER_REQUEST;
        } elseif ($request->request_type_id !== RequestType::PAPER_REQUEST) {
            throw new InvalidArgumentException('Unsupported form type');
        }
        $request->organization_id = OrgDomainsMiddleware::getCurrentOrganizationId();
        // load relevant identities)
        $this->_request = $request;
        $this->loadData();
        return $this;
    }

    public function getTitle(): string
    {
        return $this->_request->isNew() ? __('Evidence papírové žádosti') : __('Úprava papírové žádosti');
    }

    public function setAdminAllowedPrograms(array $allowed_program_ids): self
    {
        $this->_allowed_programs = $allowed_program_ids;
        return $this;
    }

    public function setProgramToAppealsMap(array $_program_id_to_appeal_id)
    {
        $this->_program_id_to_appeal_id = $_program_id_to_appeal_id;
        return $this;
    }

    public function canRenderFullForm(): bool
    {
        return !empty($this->_request->program_id);
    }

    public function getRequest(): ?Request
    {
        return $this->_request;
    }

    public function identity(string $fieldId, array $options = []): ?string
    {
        $options['label'] = Identity::getPaperLabel($fieldId);
        $options = array_merge($options, [
            'type' => Identity::getFieldFormControlType($fieldId),
            'placeholder' => Identity::getFieldFormPlaceholder($fieldId),
            'default' => Identity::getFieldFormDefaultValue($fieldId),
            'empty' => Identity::getFieldFormEmpty($fieldId),
            'class' => '',
            'value' => $this->getData('identity.' . $fieldId),
        ]);
        // field specific changes
        switch ($fieldId) {
            case Identity::IS_PO:
                $options['type'] = 'checkbox';
                break;
        }
        // select specific changes
        if ($options['type'] === 'select') {
            switch ($fieldId) {
                case Identity::PO_CORPORATE_TYPE:
                    $options['class'] .= ' select2';
                    $options['options'] = $this->getSelectValues($fieldId);
                    break;
                case Identity::RESIDENCE_ADDRESS_CITY:
                    $options['class'] .= ' select2search';
                    $options['options'] = $this->getSelectValues($fieldId);
                    break;
                case Identity::RESIDENCE_ADDRESS_CITY_PART:
                    $options['class'] .= ' city-parts-sideload';
                    if (!empty($options['value'])) {
                        $options['options'] = $this->getTableLocator()->get('CsuMunicipalityParts')->find('list', ['keyField' => 'number', 'conditions' => ['number' => $options['value']]])->order('name');
                    }
                    break;
            }
        } elseif ($options['type'] === 'checkbox') {
            $options['checked'] = boolval($options['value']);
            unset($options['value']);
        }

        return $this->_formHelper->control('identity.' . $fieldId, $options);
    }

    public function getSelectsCurrentValues(): array
    {
        $rtn = [];

        foreach (Identity::SELECTS as $selectFormFieldId) {
            // normalize key to match form control id
            $rtn['identity-' . str_replace('_', '-', str_replace('.', '-', $selectFormFieldId))] = $this->getData('identity.' . $selectFormFieldId);
        }

        return $rtn;
    }

    public function getCitiesToPartsMap()
    {
        /** @var CsuMunicipalityPartsTable $municipalitiesTable */
        $municipalitiesTable = $this->getTableLocator()->get('CsuMunicipalityParts');
        return $municipalitiesTable->CsuMunicipalitiesToCsuMunicipalityParts->find(
            'all',
            [
                'fields' => [
                    'csu_municipalities_number',
                    'csu_municipality_parts_number',
                ],
            ]
        )->enableHydration(false)->toArray();
    }

    public function dummy(string $fieldId, array $options = []): ?string
    {
        return $this->_formHelper->control('dummy.' . $fieldId, $options);
    }

    public function request(string $fieldId, array $options = []): ?string
    {
        return $this->_formHelper->control('request.' . $fieldId, $options);
    }

    public function budget(string $fieldId, array $options = []): ?string
    {
        return $this->_formHelper->control('budget.' . $fieldId, $options);
    }

    public function setFormHelper(FormHelper $formHelper): self
    {
        $this->_formHelper = $formHelper;
        return $this;
    }

    private function getSelectValues(string $fieldId)
    {
        switch ($fieldId) {
            default:
                return [];
            case Identity::PO_CORPORATE_TYPE:
                return $this->getTableLocator()->get('CsuLegalForms')->find('list', ['keyField' => 'number'])->order('name');
            case Identity::RESIDENCE_ADDRESS_CITY:
                return $this->getTableLocator()->get('CsuMunicipalities')->find('list', ['keyField' => 'number'])->order('name');
        }
    }

    public function loadData(): self
    {
        $this->loadIdentities();

        $flatData = [];
        foreach ($this->getRequest()->getVisible() as $requestVisibleProperty) {
            $flatData['request.' . $requestVisibleProperty] = $this->getRequest()->get($requestVisibleProperty);
        }

        foreach ($this->getRequest()->identities ?? [] as $existingIdentity) {
            $flatData['identity.' . $existingIdentity->name] = $existingIdentity->value;
        }

        if ($this->getRequest()->request_budget instanceof RequestBudget) {
            foreach ($this->getRequest()->request_budget->getVisible() as $budgetVisibleProperty) {
                $flatData['budget.' . $budgetVisibleProperty] = $this->getRequest()->request_budget->get($budgetVisibleProperty);
            }
        }

        foreach ($this->getRequest()->getForms() as $request_form) {
            $formController = $request_form->getFormController($this->getRequest());
            $flatData = array_merge($flatData, $formController->getData());
        }

        $this->setData(Hash::expand($flatData));

        return $this;
    }

    public function loadIdentities(): self
    {
        if ($this->_request instanceof Request) {
            $this->_request->loadIdentities();
        }
        return $this;
    }

    protected function _execute(array $data): bool
    {
        /** @var IdentitiesTable $identitiesTable */
        $identitiesTable = $this->getTableLocator()->get('Identities');
        /** @var RequestsTable $requestsTable */
        $requestsTable = $this->getTableLocator()->get('Requests');

        $this->setData($data);

        if (!$this->canRenderFullForm()) {
            $this->_request->name = $this->getData('request.name');
            $request_program_id = intval($this->getData('request.program_id'));
            if (!in_array($request_program_id, $this->_allowed_programs, true)) {
                $this->setErrors(array_merge_recursive($this->getErrors(), [
                    'request' => [
                        'program_id' => __('Nemáte právo evidovat žádosti v tomto programu')
                    ]
                ]));
            } else {
                $this->_request->program_id = $request_program_id;
            }
            $this->_request->appeal_id = $this->_program_id_to_appeal_id[$this->_request->program_id] ?? null;
            if (!$this->canRenderFullForm()) {
                return true;
            }
        }

        $this->getRequest()->setDirty('request_budget');
        if (!($this->getRequest()->loadBudget()->request_budget instanceof RequestBudget)) {
            $this->getRequest()->request_budget = $requestsTable->RequestBudgets->newEntity();
        }
        foreach ($this->getData('budget') ?? [] as $budgetField => $budgetValue) {
            $this->getRequest()->request_budget->set($budgetField, intval($budgetValue));
        }

        // persist identity
        $flatIdentities = Hash::flatten($this->getData('identity') ?? []);
        $targetIdentityVersion = $this->_request->user_identity_version;
        if (empty($targetIdentityVersion)) {
            $maxIdentity = $identitiesTable->find('all', [
                'conditions' => [
                    'user_id' => $this->_request->user->id
                ],
                'fields' => [
                    'max' => 'MAX(version)'
                ]
            ])->first()->get('max');
            $targetIdentityVersion = intval($maxIdentity) + 1;
            $this->_request->user_identity_version = $targetIdentityVersion;
        }

        $requestsTable = $this->getTableLocator()->get('Requests');
        if (!$requestsTable->save($this->getRequest())) {
            Log::error(json_encode($this->getRequest()->getErrors()));
            return false;
        }

        foreach ($flatIdentities as $identityKey => $identityValue) {
            $targetValue = Identity::serialize($identityKey, $identityValue);
            $row = $identitiesTable->findOrCreate([
                'user_id' => $this->_request->user->id,
                'version' => $targetIdentityVersion,
                'name' => $identityKey
            ], function (Identity $identityRow) use ($targetValue) {
                $identityRow->value = $targetValue;
            });

            if ($row->get('value') !== $targetValue) {
                $row->value = $targetValue;
                $identitiesTable->save($row);
            }
        }

        foreach ($this->getRequest()->getForms() as $request_form) {
            $formController = $request_form->getFormController($this->getRequest());
            $formController->execute($data);
        }

        return true;
    }
}