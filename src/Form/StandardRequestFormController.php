<?php

declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\File;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Request;
use App\Model\InstanceReportInterface;
use App\Model\Table\FilesTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\View\AppView;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use InvalidArgumentException;
use Throwable;

class StandardRequestFormController extends AbstractFormController implements InstanceReportInterface
{

    /**
     * @inheritDoc
     */
    public function render(AppView $appView, array $options = []): ?string
    {
        $return = null;
        $returnAsString = isset($options['returnAsString']) ? $options['returnAsString'] === true : false;
        $readOnly = Hash::get($options, 'readOnly') === true;

        if ($readOnly) {
            $return = $appView->element('Forms/standard_request_readonly', ['requestForm' => $this]);
        } else {
            foreach ($this->getFieldsInOrder() as $field) {
                if ($field->form_field_type_id == FormFieldType::FIELD_FILE_MULTIPLE) {
                    $return .= $appView->renderFormField($field, ['file' => $this->getCurrentlyAssignedFileOrNull($field)]);
                    $return .= $this->renderAdditionalFieldInfo($field, $appView, true);
                } else {
                    $return .= $this->renderAdditionalFieldInfo($field, $appView, true);
                    $return .= $appView->renderFormField($field, ['file' => $this->getCurrentlyAssignedFileOrNull($field)]);
                }
            }
        }

        if ($returnAsString) {
            return $return;
        }

        echo $return;

        return null;
    }

    /**
     * @inheritDoc
     */
    public function isFormFilledCompletely(?Request $request = null): bool
    {
        $request = $request ?? $this->getUserRequest();
        if ($request === null) {
            throw new InvalidArgumentException('UserRequest is not present, cannot proceed');
        }

        if (empty($this->getFieldsInOrder())) {
            // table without fields is never filled, to prevent invalid forms accross the system
            return false;
        }

        $filledFields = $this->getFilledFields($request->id, $request);

        $completed = true;
        foreach ($this->getFieldsInOrder() as $formField) {
            if (!$formField->canBeFilled()) {
                continue;
            }

            foreach ($filledFields as $filledField) {
                if ($filledField->form_field_id === $formField->id) {
                    $assignedFile = $this->getCurrentlyAssignedFileOrNull($formField);
                    if ($formField->isFilled($filledField, ['file' => $assignedFile])) {
                        continue 2;
                    } else {
                        return false;
                    }
                }
            }

            // request does not contain filled field (saved form field) for current formField
            $completed = false;
        }

        // validate incorrectly submitted files
        // $files_map => [{uploaded file path / real file path} => {count}]
        $files_map = [];
        $found_files = [];
        foreach ($this->getFieldsInOrder() as $formField) {
            if (!in_array($formField->form_field_type_id, [FormFieldType::FIELD_FILE, FormFieldType::FIELD_FILE_MULTIPLE, FormFieldType::FIELD_YES_NO_ATTACHMENT])) {
                continue;
            }
            $assigned = $this->getCurrentlyAssignedFileOrNull($formField);
            if (!empty($assigned)) {
                $files_map[$assigned->getRealPath()] = ($files_map[$assigned->getRealPath()] ?? 0) + 1;
                $found_files[] = $assigned;
            }
        }
        if (!empty($files_map) && max($files_map) > 1) {
            // if there are files, in this form, with duplicate real file path, those are incorrectly processed file uploads
            $filesTable = $this->getTableLocator()->get('Files');
            foreach ($found_files as $found_file) {
                if (in_array($found_file->getRealPath(), array_keys($files_map), true) && $files_map[$found_file->getRealPath()] > 1) {
                    if ($found_file instanceof File) {
                        $found_file->deletePhysically();
                        $filesTable->delete($found_file);
                    }
                }
            }

            $completed = false;
        }

        return $completed;
    }

    /**
     * @inheritDoc
     */
    public function renderFormSettings(AppView $appView): string
    {
        return $appView->element('Forms/standard_request_settings', [
            'form' => $this,
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function _execute(array $data): bool
    {

        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        /** @var FilesTable $filesTable */
        $filesTable = TableRegistry::getTableLocator()->get('Files');

        $toSave = [];

        foreach ($this->getFieldsInOrder() as $definitionField) {
            $definitionFieldKey = $definitionField->getFormControlId();
            if (isset($data[$definitionFieldKey])) {
                $value = $data[$definitionFieldKey];

                // upload file as part of form processing
                if (in_array($definitionField->form_field_type_id, [FormFieldType::FIELD_FILE, FormFieldType::FIELD_FILE_MULTIPLE, FormFieldType::FIELD_YES_NO_ATTACHMENT])) {
                    if (is_array($value) && $value['error'] !== UPLOAD_ERR_NO_FILE) {
                        // array can be only if new upload is processed
                        if ($value['error'] !== 0 || $value['size'] <= 1 || $value['size'] > getMaximumFileUploadSize()) {
                            continue;
                        }
                        $fileResult = $filesTable->newEntity();
                        $fileMoveResult = $fileResult->fileUploadMove($value, $this->getUser()->id);
                        if (is_string($fileMoveResult)) {
                            $fileResult->filepath = $fileMoveResult;
                            $fileResult->original_filename = $value['name'];
                            $fileResult->filesize = $value['size'];
                            $fileResult->user_id = $this->getUser()->id;
                            if ($filesTable->save($fileResult)) {
                                // if this does not get hit, serialization of FormFieldType::File will result in null
                                $value = $fileResult->id;
                            }
                        }
                    } elseif (is_array($value) && $value['error'] === UPLOAD_ERR_NO_FILE) {
                        // file was not provided in form
                        $value = $this->getData($definitionFieldKey);
                    }
                    // Deal with the old file deletion, but only if it's not a multiple file upload
                    if (($definitionField->form_field_type_id === FormFieldType::FIELD_FILE || $definitionField->form_field_type_id === FormFieldType::FIELD_YES_NO_ATTACHMENT) &&
                        intval($this->getData($definitionFieldKey)) > 0 &&
                        $value !== $this->getData($definitionFieldKey)
                    ) {
                        // file was changed, remove the old one virtually and physically
                        try {
                            $previousFileId = $this->getData($definitionFieldKey);
                            $previousFile = $filesTable->get(
                                $previousFileId,
                                [
                                    'conditions' => [
                                        'Files.user_id' => $this->getUser()->id,
                                    ],
                                ]
                            );
                            if ($filesTable->delete($previousFile)) {
                                $previousFile->deletePhysically();
                            }
                        } catch (Throwable $t) {
                            // failing to remove previous file virtually/physically, should not prevent the form from being processed further correctly
                            Log::error($t);
                        }
                    }
                    // For multiple file upload return an array containing also the previous uploads
                    if ($definitionField->form_field_type_id === FormFieldType::FIELD_FILE_MULTIPLE) {
                        if (is_array($this->getData($definitionFieldKey))) {
                            $original_uploads = $this->getData($definitionFieldKey);
                        } else {
                            $original_uploads = [];
                        }
                        if (!is_array($value) && $value > 0) { // sucessfully uploaed file will just be an integer
                            if (!in_array($value, $original_uploads)) {
                                $original_uploads[] = $value;
                                $value = $original_uploads; // add the newly uploaded file to the array
                            }
                        }
                    }
                }

                $filledField = $filledFieldsTable->findOrCreate(
                    [
                        'form_id' => $this->getFormDefinition()->id,
                        'request_id' => $this->getUserRequest()->id,
                        'form_field_id' => $definitionField->id,
                    ]
                );

                $filledField->value = $definitionField->serializeValue($value);
                $this->_data[$definitionFieldKey] = $filledField->value;
                $toSave[] = $filledField;
            }
        }

        if ($this->getFormDefinition()->shared) { // For shared forms, we save a copy to each request
            $request_set = $this->getUserRequest()->getRequestsSet();
            $request_set_ids = [];
            foreach ($request_set as $request) {
                $request_set_ids[] = $request->id;
            }

            $form_field_ids = [];
            $values = [];
            foreach ($toSave as $key => $ts) {
                $form_field_ids[] = $ts->form_field_id;
                $values[] = $ts->value;
            }
            $form_id = $this->getFormDefinition()->id;
            $toSave = $this->getFilledSharedFields($request_set_ids, $form_id, $form_field_ids, $values);
        }

        try {
            if ($filledFieldsTable->saveMany($toSave)) {
                return true;
            }
        } catch (Throwable $t) {
            Log::error('StandardRequestFormController::_execute saveMany throwed: ' . $t->getMessage());
            Log::error($t->getTraceAsString());
        }


        return false;
    }

    /**
     * Get (and create missing) all shared filled fields with correct set values
     *
     * @param mixed $request_sets_ids  - IDs of the requests in the shared set
     * @param mixed $form_id - and ID of the shared form
     *
     * @return array - and array of triplets (request_id, form_field_id, id) - we are not interested in the values as they will be owerwriten
     *
     */

    public function getFilledSharedFields($request_sets_ids, $form_id, $form_field_ids, $values)
    {
        $toSave = [];
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        foreach ($request_sets_ids as $request_id) {
            foreach ($form_field_ids as $key => $form_field_id) {
                $filledField = $filledFieldsTable->findOrCreate(
                    [
                        'form_id' => $form_id,
                        'request_id' => $request_id,
                        'form_field_id' => $form_field_id,
                    ]
                );
                $filledField->value = $values[$key];
                $toSave[] = $filledField;
            }
        }
        return $toSave;
    }

    public function getReportColumns(): array
    {
        $columns = [];
        foreach ($this->getFieldsInOrder() as $form_field) {
            $columns[sprintf('form.%d.field.%d', $this->getFormDefinition()->id, $form_field->id)] = $form_field->name;
        }
        return $columns;
    }

    public function extractColumn(Request $request, string $column): ?array
    {
        list($ignore1, $form_id, $ignore2, $field_id) = explode(".", $column);
        $form_id = intval($form_id);
        $field_id = intval($field_id);

        foreach ($request->request_filled_fields as $filled_field) {
            if ($filled_field->form_id === $form_id && $filled_field->form_field_id === $field_id) {
                return [$filled_field->value];
            }
        }

        return null;
    }

    public function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public function getColumnHeaders(string $path, string $columnName): ?array
    {
        return null;
    }
}
