<?php

declare(strict_types=1);

namespace App\Form;

use App\Model\Entity\File;
use App\Model\Entity\Form;
use App\Model\Entity\FormFieldType;
use App\Model\Entity\Request;
use App\Model\Entity\RequestChange;
use App\Model\Entity\RequestFilledFieldChange;
use App\Model\InstanceReportInterface;
use App\Model\Table\FilesTable;
use App\Model\Table\RequestFilledFieldsTable;
use App\Middleware\OrgDomainsMiddleware;
use App\Controller\ChangeRequestController;
use App\View\AppView;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Cake\Utility\Hash;
use InvalidArgumentException;
use Throwable;

class StandardRequestFormChangeController extends StandardRequestFormController implements InstanceReportInterface
{

    public function __construct(Form $formDefinition, Request $userRequest = null, EventManager $eventManager = null)
    {
        //parent::__construct($eventManager);
        $this->setFormDefinition($formDefinition);
        $this->setUserRequest($userRequest);
        $this->setup();
        if ($userRequest instanceof Request) {
            //Called manually with values from change reguest
            $this->prefill($userRequest);
        }
    }

    public function getChangedFields(RequestChange $change_request)
    {

        /* @var RequestFilledFieldsTable $filledFieldsTable
         */
        $changedFieldsTable = $this->getTableLocator()->get('RequestFilledFieldsChanges');

        /**
         * @var RequestFilledField[] $filledFields
         */
        return $changedFieldsTable->find(
            'all',
            [
                'conditions' => [
                    'form_id' => $change_request->form_id,
                    'request_id' => $change_request->request_id,
                    'change_id' => $change_request->id
                ],
            ]
        )->toArray();
    }

    /**
     * @inheritDoc
     */

    public function prefill(Request $request, RequestChange $change_request = null): self
    {
        if (!$change_request) return parent::prefill($request);
        $this->setUserRequest($request);

        $this->_rawValues = [];
        $this->_data = [];

        // Get the changed fields
        foreach ($this->getChangedFields($change_request) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                $this->_rawValues[$definitionField->getFormControlId()] = $field->value;
                $this->_data[$definitionField->getFormControlId()] = $definitionField->parseFieldValue($field->value);
            }
        }

        // Compare it with the original fields
        $highlight_ids = [];
        foreach ($this->getFilledFields($request->id, $request) as $field) {
            $definitionField = $this->getFieldById($field->form_field_id);
            if (!empty($definitionField)) {
                if ($field->value != $this->_rawValues[$definitionField->getFormControlId()]) {
                    $highlight_ids[] = $definitionField->getFormControlId();
                }
            }
        }
        $this->highlight_ids = $highlight_ids;
        return $this;
    }



    /**
     * @inheritDoc
     */
    protected function _execute(array $data): bool
    {

        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFieldsChanges');
        $requestChangesTable = TableRegistry::getTableLocator()->get('RequestChanges');
        /** @var FilesTable $filesTable */
        $filesTable = TableRegistry::getTableLocator()->get('Files');
        $toSave = [];

        // Copy original if necessary
        $this->copyOriginal($this->getUserRequest()->id, $this->getFormDefinition()->id);
        $requestChange = new RequestChange();
        $requestChange->request_id = $this->getUserRequest()->id;
        $requestChange->form_id = $this->getFormDefinition()->id;
        $requestChange->status = RequestChange::CHANGE_PENDING;

        $changeRequestController = new ChangeRequestController();
        $form_name = $this->getFormDefinition()->name_displayed ?? $this->getFormDefinition()->name;
        $text = __("K žádosti %name (%id) byl zadán nový požadavek na změnu ve formuláři: <br> $form_name <br> Všechny požadavky na změny najdete v sekci Mé týmy -> Žádosti o změnu<br>");
        $changeRequestController->changeRequestNotifyOrgByEmail($text, $this->getUserRequest());
        $change = $requestChangesTable->save($requestChange);

        foreach ($this->getFieldsInOrder() as $definitionField) {
            $definitionFieldKey = $definitionField->getFormControlId();
            if (isset($data[$definitionFieldKey])) {
                $value = $data[$definitionFieldKey];

                // upload file as part of form processing
                if (in_array($definitionField->form_field_type_id, [FormFieldType::FIELD_FILE, FormFieldType::FIELD_FILE_MULTIPLE, FormFieldType::FIELD_YES_NO_ATTACHMENT])) {
                    if (is_array($value) && $value['error'] !== UPLOAD_ERR_NO_FILE) {
                        // array can be only if new upload is processed
                        if ($value['error'] !== 0 || $value['size'] <= 1 || $value['size'] > getMaximumFileUploadSize()) {
                            continue;
                        }
                        $fileResult = $filesTable->newEntity();
                        $fileMoveResult = $fileResult->fileUploadMove($value, $this->getUser()->id);
                        if (is_string($fileMoveResult)) {
                            $fileResult->filepath = $fileMoveResult;
                            $fileResult->original_filename = $value['name'];
                            $fileResult->filesize = $value['size'];
                            $fileResult->user_id = $this->getUser()->id;
                            if ($filesTable->save($fileResult)) {
                                // if this does not get hit, serialization of FormFieldType::File will result in null
                                $value = $fileResult->id;
                            }
                        }
                    } elseif (is_array($value) && $value['error'] === UPLOAD_ERR_NO_FILE) {
                        // file was not provided in form
                        $value = $this->getData($definitionFieldKey);
                    }
                    if (intval($this->getData($definitionFieldKey)) > 0 && $value !== $this->getData($definitionFieldKey)) {
                        // file was changed, remove the old one virtually and physically
                        try {
                            $previousFileId = $this->getData($definitionFieldKey);
                            $previousFile = $filesTable->get(
                                $previousFileId,
                                [
                                    'conditions' => [
                                        'Files.user_id' => $this->getUser()->id,
                                    ],
                                ]
                            );
                            if ($filesTable->delete($previousFile)) {
                                $previousFile->deletePhysically();
                            }
                        } catch (Throwable $t) {
                            // failing to remove previous file virtually/physically, should not prevent the form from being processed further correctly
                            Log::error($t);
                        }
                    }
                }

                $filledField = $filledFieldsTable->findOrCreate(
                    [
                        'change_id' => $change->id,
                        'form_id' => $this->getFormDefinition()->id,
                        'request_id' => $this->getUserRequest()->id,
                        'form_field_id' => $definitionField->id,
                    ]
                );

                $filledField->value = $definitionField->serializeValue($value);
                $this->_data[$definitionFieldKey] = $filledField->value;
                $toSave[] = $filledField;
            }
        }
        try {
            if ($filledFieldsTable->saveMany($toSave)) {
                return true;
            }
        } catch (Throwable $t) {
            Log::error('StandardRequestFormController::_execute saveMany throwed: ' . $t->getMessage());
            Log::error($t->getTraceAsString());
        }

        return false;
    }

    /**
     * Copy the original form values from RequestFilledFields to RequestFilledFieldsChange, if it's the very first change request
     *
     * @param mixed $request_id
     * @param mixed $form_id
     *
     * @return boolean true if the original has been save, false if it already exists
     *
     */
    private function copyOriginal($request_id, $form_id)
    {
        $requestChangesTable = TableRegistry::getTableLocator()->get('RequestChanges');
        $original = $requestChangesTable->find('all', [
            'conditions' => [
                'request_id' => $request_id,
                'form_id' => $form_id,
                'status' => RequestChange::CHANGE_ORIGINAL
            ]
        ])->toArray();
        if (!empty($original)) return false; // If original exists, do nothing

        // Otherwise, create a copy
        $requestChange = new RequestChange();
        $requestChange->request_id = $request_id;
        $requestChange->form_id = $form_id;
        $requestChange->status = RequestChange::CHANGE_ORIGINAL;
        $change = $requestChangesTable->save($requestChange);

        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        $filledFieldsTableChanges = TableRegistry::getTableLocator()->get('RequestFilledFieldsChanges');
        $data = $filledFieldsTable->find('all', [
            'conditions' => [
                'request_id' => $request_id,
                'form_id' => $form_id,
            ]
        ])->toArray();
        $toSave = [];
        foreach ($data as $key => $filled_field) {
            //TODO: Maybe there is a better way (type cast ?)
            $toSave[$key] = new RequestFilledFieldChange();
            $toSave[$key]->request_id = $filled_field->request_id;
            $toSave[$key]->change_id = $change->id;
            $toSave[$key]->form_id = $filled_field->form_id;
            $toSave[$key]->form_field_id = $filled_field->form_field_id;
            $toSave[$key]->value = $filled_field->value;
            $toSave[$key]->modified = $filled_field->modified;
            $toSave[$key]->created = $filled_field->created;
        }
        return $filledFieldsTableChanges->saveMany($toSave);
    }
}
