<?php

namespace App\Mailer\Preview;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use DebugKit\Mailer\MailPreview;

/**
 * @property UsersTable $Users
 */
class UserMailPreview extends MailPreview
{
    /**
     * @return mixed
     */
    public function verifyEmail()
    {
        $this->loadModel('Users');
        /** @var User $user */
        $user = $this->Users->find('all')->firstOrFail();

        return $this->getMailer('User')->verifyEmail(
            $user
        );
    }

    /**
     * @return mixed
     */
    public function newEmail()
    {
        $this->loadModel('Users');
        /** @var User $user */
        $user = $this->Users->find('all')->firstOrFail();

        return $this->getMailer('User')->newEmail(
            $user
        );
    }

    /**
     * @return mixed
     */
    public function forgotPassword()
    {
        $this->loadModel('Users');
        /** @var User $user */
        $user = $this->Users->find('all')->firstOrFail();

        return $this->getMailer('User')->forgotPassword(
            $user
        );
    }
}
