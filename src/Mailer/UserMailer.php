<?php

namespace App\Mailer;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\Request\RequestBudgetChange;
use App\Model\Entity\Settlement;
use App\Model\Entity\User;
use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{

    /**
     * @param User $user
     * @return Mailer
     */
    public function verifyEmail(User $user)
    {
        $this->viewBuilder()->setTemplate('verify_email');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Potvrzení registrace'), $siteName);

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->getMailVerificationCode(),
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
    }

    /**
     * @param User $user
     * @return Mailer
     */
    public function newEmail(User $user)
    {
        $this->viewBuilder()->setTemplate('change_email');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Potvrzení e-mailu'), $siteName);

        return $this->setTo($user->new_email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->getMailVerificationCode(),
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                    'new_email' => $user->new_email,
                ]
            );
    }

    public function forgotPassword(User $user)
    {
        $this->viewBuilder()->setTemplate('lost_password');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Obnovení hesla'), $siteName);

        return $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->getMailVerificationCode(),
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
    }

    public function invite(User $user)
    {
        $this->viewBuilder()->setTemplate('invite');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Pozvánka'), $siteName);

        $mail = $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->getMailVerificationCode(),
                    'user_id' => $user->id,
                    'orgName' => $siteName,
                    'title' => $title,
                ]
            );
        return $this->addReplyToByOrganizationSettings($mail);
    }

    public function requestNotice(User $user, Request $request, ?string $subject = null, ?string $text = null)
    {
        $this->viewBuilder()->setTemplate('request_notice');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = $subject ?? sprintf("%s - %s", __d('email', 'Upozornění na změnu stavu žádosti'), $siteName);

        $mail = $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'text' => $text,
                'request' => $request,
            ]);
        $mail = $this->addCCByUserSettings($user, $mail, $request);
        return $this->addReplyToByOrganizationSettings($mail);
    }

    public function notificationAlert($notification, $recipients, $request, $url)
    {
        $this->viewBuilder()->setTemplate('notification_alert');
        $title = 'Nové ohlášení k žádosti #' . $request->id;
        $mail = $this->setTo($recipients)
            ->setSubject($title)
            ->setEmailFormat('html')
            ->setViewVars([
                'notification' => $notification,
                'title' => $title,
                'request' => $request,
                'url' => $url
            ]);
        return $this->addReplyToByOrganizationSettings($mail);
    }

    public function teamMessage(array $emails, string $subject, string $text, ?array $emails_bcc = [])
    {
        $this->viewBuilder()->setTemplate('request_notice');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $mail = $this->setTo($emails)
            ->setSubject($subject)
            ->setEmailFormat('both')
            ->addBCC($emails_bcc)
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $subject,
                'text' => $text,
                'request' => null,
            ]);

        return $this->addReplyToByOrganizationSettings($mail);
    }

    /**
     * Change request alert for the applicant (requester :-) )
     *
     * @param User $user
     * @param Request $request
     * @param mixed $change_request
     * @param string|null $subject
     * @param string|null $text
     *
     * @return [type]
     *
     */

    public function changeRequestNotice(User $user, Request $request, $change_request, ?string $subject = null, ?string $text = null)
    {
        $this->viewBuilder()->setTemplate('change_request_notice');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = $subject ?? sprintf("%s - %s", __d('email', 'Upozornění na změnu stavu žádosti'), $siteName);

        $mail = $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'text' => $text,
                'request' => $request,
                'change_request' => $change_request
            ]);
        $mail = $this->addCCByUserSettings($user, $mail, $request);
        return $this->addReplyToByOrganizationSettings($mail);
    }

    /**
     * Change request alert for the organisation itself
     *
     * @param User $user
     * @param Request $request
     * @param mixed $change_request
     * @param string|null $subject
     * @param string|null $text
     *
     * @return [type]
     *
     */

    public function changeRequestAlert(User $user, Request $request, $to, ?string $subject = null, ?string $text = null)
    {
        $this->viewBuilder()->setTemplate('change_request_org_alert');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = $subject ?? sprintf("%s - %s", __d('email', 'Nový požadavek na změnu v žádosti'), $siteName);

        $mail = $this->setTo($to)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'text' => $text,
                'request' => $request
            ]);
        return $this->addReplyToByOrganizationSettings($mail);
    }


    public function settlementNotice(Settlement $settlement)
    {
        $this->viewBuilder()->setTemplate('settlement_notice');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Upozornění na změnu stavu vyúčtování'), $siteName);

        $mail = $this->setTo($settlement->getRequesterContactEmails())
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'settlement' => $settlement,
            ]);
        $request = $settlement->getRequest();
        if ($request instanceof Request) {
            return $this->addCCByUserSettings($request->loadUser()->user, $mail, $request);
        }
        return $this->addReplyToByOrganizationSettings($mail);
    }

    public function requestFormalControlReturned(User $user, Request $request)
    {
        $this->viewBuilder()->setTemplate('request_formal_control_returned');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Vaše žádost byla vrácena k opravě chyb'), $siteName);

        $mail = $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'request' => $request,
            ]);
        $mail = $this->addCCByUserSettings($user, $mail, $request);
        return $this->addReplyToByOrganizationSettings($mail);
    }

    public function requestFormalControlRejected(User $user, Request $request)
    {
        $this->viewBuilder()->setTemplate('request_formal_control_rejected');
        $siteName = h(OrganizationSetting::getSetting(OrganizationSetting::SITE_NAME));
        $title = sprintf("%s - %s", __d('email', 'Žádost byla vyřazena pro nesplnění podmínek'), $siteName);

        $mail = $this->setTo($user->email)
            ->setSubject($title)
            ->setEmailFormat('both')
            ->setViewVars([
                'orgName' => $siteName,
                'title' => $title,
                'request' => $request,
            ]);
        $mail = $this->addCCByUserSettings($user, $mail, $request);
        return $this->addReplyToByOrganizationSettings($mail);
    }

    private function addReplyToByOrganizationSettings(Mailer $mail): Mailer
    {
        $replyToEmail = OrgDomainsMiddleware::getCurrentOrganization()->getReplyToEmailAddress();
        if (filter_var($replyToEmail, FILTER_VALIDATE_EMAIL)) {
            $mail->setReplyTo($replyToEmail);
        }
        return $mail;
    }

    private function addCCByUserSettings(User $user, Mailer $mail, ?Request $request = null): Mailer
    {
        $cc_emails = $user->getEmailRecipients($request ? $request->user_identity_version : null, true);

        foreach ($cc_emails as $cc_email) {
            if (filter_var($cc_email, FILTER_VALIDATE_EMAIL)) {
                $mail->addCc($cc_email);
            }
        }

        return $mail;
    }
}
