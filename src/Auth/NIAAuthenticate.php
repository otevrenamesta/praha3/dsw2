<?php

namespace App\Auth;


use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Auth\BaseAuthenticate;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\UserRole;
use App\Model\Table\UsersTable;
use stdClass;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class NIAAuthenticate extends BaseAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        

        $secret_key = \Cake\Core\Configure::read('NIA_SECRET');      
        $query_params = $request->getQueryParams();
        // Check the JWT token in  in the query data
        if (!array_key_exists('t', $query_params)) {
            Log::debug('Unhandled incoming NIA Authenticate request: ' . serialize($request));    
            return;
        } 
        $result = JWT::decode($query_params['t'], new Key($secret_key, 'HS256'));
        
        if (!$result) {
            Log::debug('Unhandled incoming NIA Authenticate request: ' . serialize($request));    
            return;
        } 
        //TODO: for debugging, remove at some point
        Log::debug('Incoming NIA Authenticate request: ' . serialize($result));
   
        if (!(property_exists($result,'tenant') && OrgDomainsMiddleware::getCurrentOrganization()->id == $result->tenant)) return;
        if (!(property_exists($result,'user') && $nia_user = $result->user)) return;
        if (!(property_exists($nia_user,'PersonIdentifier') && $nia_id = $nia_user->PersonIdentifier)) return;

        // If NIA doesn't provide email, create a dummy, but unique and persistent one, based on the NIA PersonIdentifier parameter 
        if (property_exists($nia_user, 'Email') && filter_var($nia_user->Email, FILTER_VALIDATE_EMAIL)) {
            $email = $nia_user->Email;
        } else {
            $slug = strtolower(substr(base64_encode(crc32($nia_id)), 0, 5));
            // TODO: Make this configurable !!!
            $email = 'user_' . $slug . '@nia.otevrenamesta.cz';
        }

        // Try to find user by NIA ID
        // TODO: Make sure PersonIdentifier is consistent across different NIA login methods
        $user_by_nia_id = $this->findByNIAID($nia_id);

        // Try to find user by email, if email has been provided by NIA 
        /** @var User $user_by_email */
        $user_by_email = $this->_query($email)->first();

        if (is_object($user_by_email) && is_object($user_by_nia_id)) {
            // If both are found but ids don't match, log a warning 
            if ($user_by_email->ID != $user_by_nia_id->ID) {
                Log::warning('Mismatch user id when logging via NIA. By email it is ' . $user_by_email->ID . ' whereas by PersonIdentifier it would be ' . $user_by_nia_id->ID);
            }
            $user = $user_by_email;
        } else {
            if (is_object($user_by_email)) {
                $user = $user_by_email;
            }
            if (is_object($user_by_nia_id)) {
                $user = $user_by_nia_id;
            }
            if (!is_object($user_by_email) && !is_object($user_by_nia_id)) {
                // Just return a standard class with the necessary values
                $user = new stdClass;
                $user->is_new = true;
                $user->nia_id = $nia_id;
                $user->email = $email;
                $user->payload = $nia_user;
            }
        }

        // For now we only allow ordinary users to log in via NIA 
        $ban_roles = UserRole::ALL_MANAGER;
        if ($user instanceof User) {
            foreach ($ban_roles as $role) {
                if ($user->hasRole($role, OrgDomainsMiddleware::getCurrentOrganizationId())) return;
            }
        }

        // For the logout purposes, set NIAAuth to true in the user's session   
        $request->getSession()->write('NIAAuth', true);
        // And add NIA session parameters
        $request->getSession()->write('SessionIndex', $result->SessionIndex??'');
        $request->getSession()->write('NameID', $result->NameID??'');
        

        // Authentication somewhat always succeds if done via NIAsucceeded, return the user data
        return $user;
    }


    public function findByNIAID($nia_id)
    {
        $users = TableRegistry::getTableLocator()->get('Users');

        $query = $users->find('all', [
            'conditions' => [
                'nia_id' => $nia_id
            ]
        ]);

        return $query->all()->first();
    }
}
