<?php

use App\Model\Entity\Form;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form Form
 * @var $programs array
 * @var $formTypes array
 */
$UstiException =  OrganizationSetting::isException(OrganizationSetting::USTI);

echo $this->element('simple_treeselect');
$this->assign('title', $form->id > 0 ? __('Úprava formuláře') : __('Vytvoření formuláře'));

?>
<h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($form);
echo $this->Form->control('name', ['label' => __('Název formuláře')]);
echo $this->Form->control('name_displayed', ['label' => __('Zobrazovaný název formuláře')]);
echo $this->Form->control('weight', ['label' => __('Váha (pro řazení)'), 'description' => __('Čím "težší", tím níže v pořadí. Je dobré zvolit třeba krok 10 (10, 20, 30, atd...)')]);
echo $this->Form->control('form_type_id', ['label' => __('Typ Formuláře'), 'options' => $formTypes]);
echo $this->Form->control('shared',['type' => 'checkbox', 'label' => __('Formulář bude sdílený pro všechny žádosti daného uživatele v daném programu')]);
echo $this->Form->control('programs._ids', ['class' => 'treeselect', 'options' => $programs, 'label' => __('Dotační programy, pro které je formulář povinný')]);
if ($this->getSiteSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)) {
    echo '<strong>' . __('Příjem papírových žádostí') . '</strong>';
    echo $this->Form->control('paper_programs._ids', ['class' => 'treeselect', 'options' => $programs, 'label' => __('Dotační programy, pro které je formulář povinný při evidenci papírově přijaté žádosti')]);
}
if ($UstiException) {
    echo $this->Form->control('allow_change',['type' => 'checkbox', 'label' => __('Povolit podání žádosti o změnu dat v tomto formuláři ?')]);
}
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
if ($form->id) {
    echo $this->Html->link(__('Zahodit změny a vrátit se k detailu formuláře'), ['action' => 'formDetail', $form->id], ['class' => 'btn btn-warning mt-2']);
}
?>
