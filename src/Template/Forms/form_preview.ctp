<?php

use App\Form\AbstractFormController;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form AbstractFormController
 */

$this->assign('title', __('Náhled'));

?>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h1><?= $this->fetch('title') ?></h1>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?= $form->render($this); ?>
    </div>
</div>
