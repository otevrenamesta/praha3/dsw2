<?php

use App\Form\AbstractFormController;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form AbstractFormController
 * @var $disableFormUpdate boolean
 * @var inRequestsList Array
 */

$this->assign('title', $form->getFormDefinition()->name);
$formID = $form->getFormDefinition()->id;
$nav = (!$disableFormUpdate && $form->hasUserDefinedFields() ? $this->Html->link(__('Přidat nové pole'), ['action' => 'fieldAddModify', $formID], ['class' => 'btn btn-success mb-2']) : '') .
    (!$disableFormUpdate ? $this->Html->link(__('Úprava formuláře'), ['action' => 'addModify', $formID], ['class' => 'btn btn-warning mb-2 ml-2']) : '') .
    $this->Html->link(__('Náhled na finální formulář'), ['action' => 'formPreview', $formID], ['class' => 'btn btn-info mb-2 ml-2']) .
    $this->Html->link(__('Vytvořit kopii formuláře'), ['action' => 'formCopy', $formID], ['class' => 'btn btn-primary mb-2 ml-2']);
$listOfRequests = count($inRequestsList) > 0 ? '<ul><li>' . implode(', ', $inRequestsList) . '</li></ul>' : '';
?>
<div class="card">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>
        <?= $nav ?><br />
        <div>
            <?= !$disableFormUpdate
                ? ''
                : '<div class="alert alert-dismissible alert-warning"><strong>' .
                __('Formulář je vyplněn v následujících žádostech číslo:') .
                '</strong>' . $listOfRequests . '</div>' ?>
            <?php
            $allPrograms = array_merge($form->getFormDefinition()->programs ?: [], $form->getFormDefinition()->paper_programs ?: []);
            if (!empty($allPrograms)) {
                echo '<div class="alert alert-dismissible alert-warning"><strong>' .
                    __('Formulář se používá v těchto programech:') .
                    '</strong><ul>';
                foreach ($form->getFormDefinition()->programs as $key => $value) {
                    echo isset($value->name) ? '<li>' . $value->name . '</li>' : '';
                }
                echo '</ul></div>';
            }
            ?>
        </div>
    </div>
    <div class="card-body">
        <?= $form->renderFormSettings($this) ?>
    </div>
    <div class="card-footer">
        <?= count($form->getFieldsInOrder()) > 0 ? $nav : '' ?>
    </div>
</div>