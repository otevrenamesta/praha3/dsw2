<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

$this->assign('title', __('Detail žádosti - Ohlášení'));

$listOfRoles = [
  $this->isTeamFormalControlor(),
  $this->isTeamEvaluator(),
  $this->isTeamProposals(),
  $this->isTeamApprovals(),
  $this->isTeamManagers(),
];

echo $this->element('request_notifications', compact('request'));

