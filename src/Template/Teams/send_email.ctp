<?php

/**
 * @var $this AppView
 * @var $request Request
 * @var $emails Array
 */

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;

$this->assign('title', __('Odeslat e-mail'));

echo $this->Form->create(null);
?>

<div class="card">
    <div class="card-header">
        <h2><?= sprintf("%s %d", __('Odeslat e-mail k žádosti č.'), $request->id) ?></h2>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?php
                echo $this->Form->control('_sender', [
                    'label' => __('Odesílatel'),
                    'disabled' => true,
                    'value' => $this->getSiteSetting(OrganizationSetting::EMAIL_FROM)
                ]);
                ?>
            </div>
            <div class="col-md">
                <?php
                echo $this->Form->control('_replyTo', [
                    'label' => __('Adresa pro odpověď na zasílaný e-mail'),
                    'disabled' => true,
                    'value' => OrgDomainsMiddleware::getCurrentOrganization()->getReplyToEmailAddress()

                ]);
                ?>
            </div>
        </div>
        <?php
        echo $this->Form->control('_recipient', [
            'label' => __('Příjemci'),
            'disabled' => true,
            'value' => join(
                ", ",
                isset($emails) && !empty($emails)
                    ? $emails
                    : $request->user->getEmailRecipients($request->user_identity_version, true)
            )
        ]);
        echo $this->Form->control('subject', [
            'label' => __('Předmět e-mailu'),
            'required' => true,
            'default' => sprintf("%s %d - %s", __('Upozornění k vaší žádosti o podporu č.'), $request->id, $request->name)
        ]);
        echo $this->Form->control('contents', [
            'label' => __('Kompletní text e-mailu'),
            'required' => true,
            'data-quilljs' => 'data-quilljs',
            'rows' => 10,
            'default' => sprintf(
                __('Dobrý den') . ",<br/><br/><br/><br/>S pozdravem<br/>%s, v z. %s",
                $this->getSiteSetting(OrganizationSetting::SITE_NAME),
                $this->getCurrentUser()->email
            )
        ]);
        ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Odeslat')) ?>
    </div>
</div>

<?= $this->Form->end() ?>