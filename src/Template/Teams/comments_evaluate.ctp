<?php

use App\Model\Entity\Evaluation;
use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $evaluation Evaluation
 * @var $criteria EvaluationCriterium[]
 * @var $parentCriterium EvaluationCriterium
 */
$this->assign('title', __('Ohodnotit žádost'));
?>

<div class="card">
    <div class="card-header">
        <h4><?= __('Ohodnotit žádost') ?>
            <?php
            if ($this->isTeamFormalControlor()) {
                $helpTitle = __('Sekce pro úředníky slouží k nastavení úředních informací: vlastní identifikátor, de minimis, veřejnosprávní kontrola a poznámka k žádosti');
                echo '<span class="float-right" data-toggle="tooltip" data-placement="left" title="' . $helpTitle . '"><i class="far fa-question-circle help-icon"></i></span>';
                echo $this->Html->link(
                    '<i class="fas fa-user-check"></i>' . __('Sekce pro úředníky'),
                    ['_name' => 'admin_section_edit', $request->id, '?' => ['from' => $this->getRequest()->getParam('action')]],
                    ['class' => 'float-right float-end btn btn-primary', 'escape' => false]
                );
            }
            echo !$this->isUsersManager() ? '' : $this->Html->link(
                '<i class="fas fa-user-cog"></i>' . __('Správa žadatele'),
                ['_name' => 'admin_users_edit', $request->user_id],
                ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
            );

            echo !($this->isManager() || $this->isTeamMember()) ? '' : $this->Html->link(
                '<i class="fas fa-coins"></i>' . __('Historie podpory žadatele'),
                ['_name' => 'detailFromUserId', $request->user_id],
                ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
            );
            ?>
        </h4>
        <?= $parentCriterium ? '<hr />' . $parentCriterium->description : '' ?>
    </div>

    <div class="card-body">
        <strong><?= __('Hodnotící kritéria') ?></strong>
        <?php
        echo $this->Form->create($evaluation);
        foreach ($criteria as $criterium) {
            echo $this->Form->control('criterium.' . $criterium->id, [
                'type' => 'number',
                'min' => $criterium->min_points,
                'max' => $criterium->max_points,
                'label' => $criterium->name . ($criterium->max_points === 0
                    ? ''
                    : sprintf(' (od %d do %d bodů)', $criterium->min_points, $criterium->max_points)) . sprintf('<br/><span>%s</span>', $criterium->description),
                'default' => null,
                'required' => true,
                'disabled' => $criterium->max_points === 0,
                'class' => $criterium->max_points === 0 ? 'd-none' : '',
                'escape' => false
            ]);
        }
        echo count($criteria) <= 0 ? '<p><br /><em class="text-muted">- ' . __('kritéria nejsou nastavena') . ' -</em></p>' : '';
        echo '<hr/>';
        echo $this->Form->control('comment', ['type' => 'textarea', 'label' => __('Vlastní komentář (nebude nikde zobrazeno)')]);

        echo $this->Html->link('<i class="fas fa-times add-space"></i>' . __('Zavřít'), ['_name' => 'team_evaluators_index'], ['escape' => false, 'class' => 'float-left float-start mr-2 btn btn-secondary']);
        echo $this->Form->button('<i class="fas fa-check add-space"></i>' . __('Uložit hodnocení'), ['class' => 'btn btn-success', 'escape' => false, 'type' => 'submit']);
        echo $this->Form->end();
        ?>
    </div>
</div>

<p class='m-5' />
<?php
echo $this->element('request_full_table', compact('request'));
