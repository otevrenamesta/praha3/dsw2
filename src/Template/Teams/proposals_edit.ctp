<?php

use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $criteria EvaluationCriterium[]
 */

$this->assign('title', __('Navrhnout úpravu výsledku žádosti'));
?>

<div class="card">
    <div class="card-header">
        <h4><?= __('Navrhnout úpravu výsledku žádosti') ?>
            <?php
            if ($this->isTeamFormalControlor()) {
                $helpTitle = __('Sekce pro úředníky slouží k nastavení úředních informací: vlastní identifikátor, de minimis, veřejnosprávní kontrola a poznámka k žádosti');
                echo '<span class="float-right" data-toggle="tooltip" data-placement="left" title="' . $helpTitle . '"><i class="far fa-question-circle help-icon"></i></span>';
                echo $this->Html->link(
                    '<i class="fas fa-user-check"></i>' . __('Sekce pro úředníky'),
                    ['_name' => 'admin_section_edit', $request->id, '?' => ['from' => $this->getRequest()->getParam('action')]],
                    ['class' => 'float-right float-end btn btn-primary', 'escape' => false]
                );
            }
            echo !$this->isUsersManager() ? '' : $this->Html->link(
                '<i class="fas fa-user-cog"></i>' . __('Správa žadatele'),
                ['_name' => 'admin_users_edit', $request->user_id],
                ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
            );

            echo !($this->isManager() || $this->isTeamMember()) ? '' : $this->Html->link(
                '<i class="fas fa-coins"></i>' . __('Historie podpory žadatele'),
                ['_name' => 'detailFromUserId', $request->user_id],
                ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
            );
            ?>
        </h4>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create($request);
        $addSubsidyInfo = '';
        if ($this->getSiteSetting(OrganizationSetting::RATINGS_AMOUNT_FROM_CRITERIA_SUM, false) === true) {
            $maxPoints = (float)array_sum(array_column($criteria, 'max_points'));
            $finalSubsidyAmount = min(
                (float)$request->request_budget->requested_amount,
                round((float)$request->request_budget->requested_amount * ((float)$request->getFinalCriteriaSum() / (float)$maxPoints))
            );
            $addSubsidyInfo = '<div class="alert alert-dismissible alert-success">' .
                __('Výsledná částka podpory vypočtená na základě bodového výsledku') . ',<br />' .
                __('z požadované výše dotace') . ' ' . Number::currency($request->request_budget->requested_amount, 'CZK') . ', ' .
                __('je částka') . ': <strong><span id="final-subsidy-amount-val">' . $finalSubsidyAmount . '</span> Kč </strong><br />' .
                __('Použít tuto vypočítanou částku jako "Návrh na výslednou částku podpory"?') .
                ' <input type="button" id="final-subsidy-amount-btn" class="btn btn-sm btn-secondary" value="' . __('Použít') . '" /></div>';
        } else {
            $finalSubsidyAmount = $request->getFinalSubsidyAmount();
        }
        echo $this->Form->control('final_subsidy_amount', ['label' => __('Návrh na výslednou částku podpory (Kč)'), 'default' => $finalSubsidyAmount, 'escape' => false]);
        echo $addSubsidyInfo;
        echo $this->Form->control('comment', ['label' => __('Návrh na slovní hodnocení žádosti'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
        echo $this->Form->control('purpose', ['label' => __('Návrh na úpravu účelu, na který bude dotace poskytnuta'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
        ?>
        <hr />
        <strong><?= (!empty($criteria) ? __('Návrh výsledného bodového hodnocení žádosti (zobrazený je průměr ze všech proběhlých hodnocení pro každé z kritérií)') : '') ?></strong>
        <?php
        $hasCriteria = false;
        $criteriaMin = 0;
        $criteriaSum = 0;
        foreach ($criteria as $criterium) {
            $hasCriteria = true;
            echo $this->Form->control('criterium.' . $criterium->id, [
                'type' => 'number',
                'min' => $criterium->min_points,
                'max' => $criterium->max_points,
                'step' => 0.01,
                'label' => $criterium->name . sprintf(' (od %d do %d bodů)', $criterium->min_points, $criterium->max_points) . sprintf('<br/><span class="text-muted">%s</span>', $criterium->description),
                'default' => $request->getFinalCriteriumMean($criterium->id),
                'do-sum' => 'do-sum',
                'escape' => false
            ]);
            $criteriaMin += $criterium->min_points;
            $criteriaSum += $criterium->max_points;
        }
        if ($hasCriteria) {
            echo $this->Form->control('criterium.sum', [
                'type' => 'number',
                'min' => $criteriaMin,
                'max' => $criteriaSum,
                'step' => 0.01,
                'sum-target' => 'sum-target',
                'label' => __('Výsledný součet bodů') . sprintf(" (od %d do %d bodů)", $criteriaMin, $criteriaSum),
                'default' => $request->getFinalCriteriaSum(),
            ]);
        }

        echo $this->Html->link('<i class="fas fa-times add-space"></i>' . __('Zavřít'), ['_name' => 'team_proposals_index'], ['escape' => false, 'class' => 'float-left float-start mr-2 btn btn-secondary']);
        echo $this->Form->button('<i class="fas fa-check add-space"></i>' . __('Uložit návrhy'), ['class' => 'btn btn-success', 'escape' => false, 'type' => 'submit']);
        echo $this->Form->end();
        ?>
        <script type="text/javascript">
            function updateSubsidyAmount(total) {
                var finalSubsidyAmount = total && total > 0 ?
                    Math.round(parseFloat(<?= $request->request_budget->requested_amount ?>) * (total / parseFloat(<?= $criteriaSum ?>))) :
                    0;
                $("#final-subsidy-amount-val").html(Math.min(finalSubsidyAmount, parseFloat(<?= $request->request_budget->requested_amount ?>)));
            }
            $(function() {
                $("input[type=number][do-sum=do-sum]").on('change keyup paste', function() {
                    var _sum = 0;
                    $("input[type=number][do-sum=do-sum]").each(function() {
                        _sum += parseFloat($(this).val());
                    });
                    $("input[type=number][sum-target]").val(_sum);
                    <?php if ($this->getSiteSetting(OrganizationSetting::RATINGS_AMOUNT_FROM_CRITERIA_SUM, false) === true) : ?>
                        updateSubsidyAmount(_sum);
                    <?php endif; ?>
                });
                <?php if ($this->getSiteSetting(OrganizationSetting::RATINGS_AMOUNT_FROM_CRITERIA_SUM, false) === true) : ?>
                    $("input[type=number][sum-target]").on('change keyup paste', function() {
                        updateSubsidyAmount($(this).val());
                    });
                    updateSubsidyAmount($("input[type=number][sum-target]").val());

                    $("#final-subsidy-amount-btn").on('click', function() {
                        $("#final-subsidy-amount").val($("#final-subsidy-amount-val").html());
                    })
                <?php endif; ?>
            });
        </script>
    </div>
</div>

<?php
if ($this->getSiteSetting(OrganizationSetting::SHOW_RATINGS_TEAM_PROPOSALS, true) === true) {
    echo $this->element('teams_rating_details', compact('request', 'criteria'));
}
?>

<p class='m-5' />
<?php
echo $this->element('request_full_table', compact('request'));
