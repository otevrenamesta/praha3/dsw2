<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $requires_pre_evaluation boolean
 */
$this->assign('title', __('Zkontrolovat obsah žádosti'));



?>
<div class="card">
    <h4 class="card-header"><?= __('Zkontrolovat obsah žádosti') ?>
        <?php
        if ($this->isTeamFormalControlor()) {
            $helpTitle = __('Sekce pro úředníky slouží k nastavení úředních informací: vlastní identifikátor, de minimis, veřejnosprávní kontrola a poznámka k žádosti');
            echo '<span class="float-right" data-toggle="tooltip" data-placement="left" title="' . $helpTitle . '"><i class="far fa-question-circle help-icon"></i></span>';
            echo $this->Html->link(
                '<i class="fas fa-user-check"></i>' . __('Sekce pro úředníky'),
                ['_name' => 'admin_section_edit', $request->id, '?' => ['from' => $this->getRequest()->getParam('action')]],
                ['class' => 'float-right float-end btn btn-primary', 'escape' => false]
            );
        }
        echo !$this->isUsersManager() ? '' : $this->Html->link(
            '<i class="fas fa-user-cog"></i>' . __('Správa žadatele'),
            ['_name' => 'admin_users_edit', $request->user_id],
            ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
        );

        echo !($this->isManager() || $this->isTeamMember()) ? '' : $this->Html->link(
            '<i class="fas fa-coins"></i>' . __('Historie podpory žadatele'),
            ['_name' => 'detailFromUserId', $request->user_id],
            ['class' => 'float-right float-end btn btn-secondary mr-2', 'escape' => false]
        );
        ?>
    </h4>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <?php if (!$requires_pre_evaluation) : ?>
                    <div class="card">
                        <h5 class="card-header text-info"><?= __('Žádost je v pořádku') ?></h5>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($request);
                            echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_FORMAL_CHECK_APPROVED]);
                            echo $this->Form->button('<i class="fas fa-check add-space"></i>' . __('Potvrdit správnost'), ['type' => 'submit', 'class' => 'btn btn-success', 'escape' => false]);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="card">
                        <h5 class="card-header text-info"><?= __('Ohodnotit žádost') ?></h5>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($request);
                            echo $this->Form->control('formal', ['label' => __('Žádost splňuje formální náležitosti (pokud ne, vraťte ji k doplnění nebo vyřaďte)'), 'type' => 'checkbox']);
                            $this->Form->unlockField('rate1');
                            echo $this->Form->control('rate1', ['label' => __('Soulad s platným KPP (45 bodů)'), 'type' => 'checkbox', 'disabled' => true]);
                            $this->Form->unlockField('rate2');
                            echo $this->Form->control('rate2', ['label' => __('Působnost služby na území SMÚ 4 roky a více (34 bodů)'), 'type' => 'checkbox', 'disabled' => true]);
                            $this->Form->unlockField('rate3');
                            echo $this->Form->control('rate3', ['label' => __('Kompletnost předkládané žádosti (0-21)'), 'type' => 'number', 'max' => 21, 'disabled' => true]);
                            $this->Form->unlockField('rating-comment');
                            echo $this->Form->control('rating-comment', ['label' => __('Poznámka k hodnocení '), 'type' => 'textarea', 'disabled' => true]);
                            $this->Form->unlockField('request_state_id');
                            echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_FORMAL_CHECK_APPROVED, 'id' => 'req-state']);
                            echo $this->Form->button('<i id="rating-result-icon" class="fas fa-times add-space"></i><span id="rating-result-number">' . __('Vyřadit žádost pro nízké hodnocení') . ' (0 bodů).</span>', ['type' => 'submit', 'class' => 'btn btn-danger', 'escape' => false, 'name' => 'rating-result', 'disabled' => true]);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col">
                <div class="card">
                    <h5 class="card-header text-info"><?= __('Vrátit žádost k doplnění') ?></h5>
                    <div class="card-body">
                        <?php
                        echo $this->Form->create($request);
                        echo $this->Form->control('lock_comment', ['label' => __('Vysvětlení, proč žádost není v pořádku'), 'type' => 'textarea']);
                        echo $this->Form->control('lock_when', ['label' => __('Datum, do kdy má žadatel čas žádost opravit'), 'type' => 'date', 'default' => date('Y-m-d', strtotime('+7 days')), 'class' => 'w-100']);
                        echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_NEW]);
                        echo $this->Form->button('<i class="fas fa-share add-space"></i>' . __('Vrátit k doplnění'), ['type' => 'submit', 'class' => 'btn btn-warning', 'escape' => false]);
                        echo $this->Form->end();
                        ?>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <h5 class="card-header text-info"><?= __('Žádost nevyhovuje a není možné ji opravit') ?></h5>
                    <div class="card-body">
                        <?php
                        echo $this->Form->create($request);
                        echo $this->Form->control('lock_comment', ['label' => __('Vysvětlení, proč je žádost vyřazena'), 'type' => 'textarea']);
                        echo $this->Form->hidden('request_state_id', ['value' => RequestState::STATE_FORMAL_CHECK_REJECTED]);
                        echo $this->Form->button('<i class="fas fa-times add-space"></i>' . __('Vyřadit žádost jako nevyhovující'), ['type' => 'submit', 'class' => 'btn btn-danger', 'escape' => false]);
                        echo $this->Form->end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<p class='m-5'>
    <?php
    echo $this->element('request_full_table', compact('request'));
    echo $this->element('request_logs', compact('request'));
    ?>
</p>
<script>
    $(document).ready(function() {
        // List of input names to be controlled
        var controlledInputs = ["rate1", "rate2", "rate3", "rating-comment", "rating-result"];

        // Disable them initially
        $.each(controlledInputs, function(index, value) {
            $('[name="' + value + '"]').prop('disabled', true);
        });

        // Add event listener for 'formal' checkbox
        $('input[name="formal"]').change(function() {
            if ($(this).is(':checked')) {
                // If 'formal' is checked, enable all other input controls
                $.each(controlledInputs, function(index, value) {
                    $('[name="' + value + '"]').prop('disabled', false);
                });
            } else {
                // If 'formal' is unchecked, disable all other input controls
                $.each(controlledInputs, function(index, value) {
                    $('[name="' + value + '"]').prop('disabled', true);
                });
            }
        });
        // Add common event listener for 'rate1', 'rate2' and 'rate3'
        $('input[name="formal"], input[name="rate1"], input[name="rate2"], input[name="rate3"]').on("change keyup", function() {
            var $total = 0;
            if ($('input[name="rate1"]').is(':checked')) {
                $total += 45
            }
            if ($('input[name="rate2"]').is(':checked')) {
                $total += 34
            }
            var rate3Value = parseInt($('input[name="rate3"]').val(), 10);
            if (!rate3Value) {
                rate3Value = 0;
            }
            if (rate3Value > 21) {
                $('input[name="rate3"]').val(21)
            }
            $total = $total + rate3Value;
            if ($total > 50) {
                $("#rating-result-icon").removeClass("fa-times").addClass("fa-check");
                $("[name=rating-result]").removeClass("btn-danger").addClass("btn-success");
                $("[name=rating-result]").val($total);
                $('#rating-result-number').text('Schválit žádost' + ' (' + $total + ' bodů).');
                $("#req-state").val(<?php echo RequestState::STATE_FORMAL_CHECK_APPROVED ?>);
            } else {
                $('#rating-result-number').text('Vyřadit žádost pro nízké hodnocení' + ' (' + $total + ' bodů).');
                $("#rating-result-icon").removeClass("fa-check").addClass("fa-times");
                $("[name=rating-result]").removeClass("btn-success").addClass("btn-danger");
                $("[name=rating-result]").val($total);
                $("#req-state").val(<?php echo RequestState::STATE_FORMAL_CHECK_REJECTED ?>);
            }
        });
    });
</script>
</script>