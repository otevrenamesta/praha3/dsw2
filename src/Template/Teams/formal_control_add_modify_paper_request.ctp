<?php

use App\Form\PaperRequestForm;
use App\Model\Entity\Identity;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\Routing\Router;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $form PaperRequestForm
 * @var $appeals_programs array
 */

$this->assign('title', $form->getTitle());
echo $this->element('simple_select2');
echo $this->element('simple_chained_select', ['data' => $appeals_programs, 'selectedKey' => $form->getRequest()->program_id]);

$form->setFormHelper($this->Form);
echo $this->Form->create($form, ['type' => 'file']);

if (empty($form->getErrors())) {
    $form->setErrors(Hash::expand(['dummy.preload_ico' => ['Zadejte IČ']]));
}

$submitButton = $this->Form->submit(__('Uložit žádost'));

$missingIdentityFields = $form->getRequest()->user->getIdentityMissingFields($form->getRequest()->user_identity_version, $form->getRequest()->request_type_id);
list($hasAppealAndProgram, $hasName, $identityIsComplete, $budgetIsOk, $formsAreValid) = $form->getValidations();
?>

<?php if ($form->canRenderFullForm()): ?>
    <div class="card m-2">
        <h2 class="card-header"><?= __('Kontrola papírové evidence žádosti') ?></h2>
        <div class="card-body">
            <ol>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $hasAppealAndProgram]) ?> <?= __('Výzva / Dotační program') ?></li>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $hasName]) ?> <?= __('Název žádosti / projektu') ?></li>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $identityIsComplete]) ?> <?= __('Identita žadatele') ?></li>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $budgetIsOk]) ?> <?= __('Rozpočet žádosti') ?></li>
                <?php foreach ($form->getRequest()->getForms() as $request_form): ?>
                    <?php $formController = $request_form->getFormController($form->getRequest()); ?>
                    <li><?= $this->element('simple_checkmark_indicator', ['bool' => $formController->isFormFilledCompletely()]) ?> <?= $formController->getFormName() ?></li>
                <?php endforeach; ?>
            </ol>
        </div>
        <div class="card-footer">
            <?php
            if ($form->getRequest()->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                echo $this->Html->link(__('Označit jako odeslanou žádost'), ['_name' => 'formal_control_manual_submit', $form->getRequest()->id], ['class' => 'btn btn-success m-1']);
            }
            if (in_array($form->getRequest()->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])) {
                echo $this->Html->link(__('Vymazat tuto papírovou žádost'), ['_name' => 'formal_control_paper_delete', $form->getRequest()->id], ['class' => 'btn btn-danger m-1']);
            }
            ?>
        </div>
    </div>
<?php endif; ?>

    <div class="card m-2">
        <h2 class="card-header"><?= $this->fetch('title') ?></h2>
        <div class="card-body">
            <?php
            echo $form->request('program_id', ['options' => [], 'type' => 'select', 'class' => 'chainedSelect', 'label' => __('Vyberte si Dotační Výzvu, Oblast podpory a Program, ve kterém chcete žádat'), 'required' => 'required']);
            echo $form->request('name', ['label' => __('Název projektu')]);
            echo $this->Form->control('reference_number', ['label' => __('Vlastní identifikátor žádosti (např. pořadové číslo, spisová značka, ...)')]);
            ?>
        </div>
        <div class="card-footer">
            <?= $submitButton ?>
        </div>
    </div>

<?php
if (!$form->canRenderFullForm()) {
    echo $this->Form->end();
    return;
}
?>

    <div class="card m-2">
        <div class="card-header">
            <h2><?= __('Identita žadatele') ?></h2>
            <?php
            $identityCollapsed = empty($missingIdentityFields);
            $collapsedClass = $identityCollapsed ? 'collapse' : '';

            $shownMissingFields = 0;
            foreach ($missingIdentityFields as $missingIdentityField) {
                $shownMissingFields++;
                if ($shownMissingFields >= 4) {
                    break;
                }
                ?>
                <div class="alert alert-danger">
                    <?= sprintf("%s: %s", __('Chybí vyplnit'), Identity::getFieldLabel($missingIdentityField)) ?>
                </div>
                <?php
            }
            if (count($missingIdentityFields) >= 4) {
                ?>
                <div class="alert alert-danger">
                    <?= sprintf(__('Celkem %d chyb'), count($missingIdentityFields)) ?>
                </div>
                <?php
            }
            ?>
            <?= $form->identity(Identity::IS_PO) ?>
            <div id="ares-loader" class="<?= $collapsedClass ?>">
                <hr/>
                <?= $form->dummy('preload_ico', ['label' => __('IČO pro načtení z ARES'), 'value' => $form->getData('identity.' . Identity::PO_BUSINESS_ID)]) ?>
                <?= $this->Html->link(__('Načíst data z ARES'), '#', ['class' => 'btn btn-success mt-3', 'id' => 'ares-load']) ?>
                <script type="text/javascript">
                    const $aresURL = "<?= mb_substr(Router::url(['_name' => 'ares', 'ico' => '0']), 0, -1) ?>";
                    let $lastResponse = null;
                    let $feedbackDiv = null;

                    function preloadData() {
                        if (!$lastResponse || $($lastResponse.getElementsByTagName('D:EK')).text() === '1') {
                            let $errorMessage = $lastResponse ? $($lastResponse.getElementsByTagName('D:ET')).text() : null;
                            $feedbackDiv?.removeClass('valid-feedback').addClass('invalid-feedback').html('<?= __('Načtení se nezdařilo') ?>' + ($errorMessage ? '<br/>' + $errorMessage : '')).toggle(true);
                            return;
                        }

                        let $kodPF = $($lastResponse.getElementsByTagName('D:KPF')).first().text();
                        let $psc = $($lastResponse.getElementsByTagName('D:PSC')).first().text();
                        let $fullname = $($lastResponse.getElementsByTagName('D:OF')).first().text();
                        let $ico = $($lastResponse.getElementsByTagName('D:ICO')).first().text();
                        let $ulice = $($lastResponse.getElementsByTagName('D:NU')).first().text();
                        let $cisloDomovni = $($lastResponse.getElementsByTagName('D:CD')).first().text();
                        let $cisloPopisne = $($lastResponse.getElementsByTagName('D:TCD')).first().text();

                        let $kodObce = $($lastResponse.getElementsByTagName('U:KO')).first().text();
                        let $kodCasti = $($lastResponse.getElementsByTagName('U:KCO')).first().text();

                        let $vznik = $($lastResponse.getElementsByTagName('D:DV')).first().text();
                        let $vlozka = $($lastResponse.getElementsByTagName('D:OV')).first().text();
                        let $soud = $($lastResponse.getElementsByTagName('D:T')).first().text();
                        let $urad = $($lastResponse.getElementsByTagName('D:NZU')).first().text();

                        $("#identity-po-corporate-type").val($kodPF).trigger('change').addClass('is-valid');
                        $("#identity-po-fullname").val($fullname).addClass('is-valid');
                        $("#identity-address-residence-zip").val($psc).addClass('is-valid');
                        $("#identity-address-residence-street").val($ulice + " " + $cisloDomovni + ($cisloPopisne ? "/" + $cisloPopisne : "")).addClass('is-valid');
                        $("#identity-po-business-id").val($ico).addClass('is-valid');
                        $("#identity-address-residence-city").val($kodObce).trigger('change').addClass('is-valid');
                        $("#identity-address-residence-city-part").val($kodCasti).trigger('change').addClass('is-valid');
                        $("#identity-po-registration-since").val($vznik).trigger('change').addClass('is-valid');
                        $("#identity-po-registration-details").val($vlozka ? ($vlozka + ", " + $soud) : $urad).trigger('change').addClass('is-valid');

                        if ($feedbackDiv) {
                            $feedbackDiv.removeClass('invalid-feedback').addClass('valid-feedback').text('<?= __('Načtení se zdařilo') ?>').toggle(true);
                        }
                    }

                    function loadAresData() {
                        let $input = $("#dummy-preload-ico");
                        let ico = $input.val();
                        ico = ico.replace(/\D/g, '');
                        if (!ico) {
                            $input.addClass('is-invalid').attr('placeholder', '<?= __('Musíte vyplnit IČO') ?>');
                            return;
                        }
                        $input.removeClass('is-invalid');
                        $("#aresLoadingModal").modal('show');
                        $.ajax({
                            url: $aresURL + ico
                        }).done(function (data) {
                            $lastResponse = data;
                            preloadData();
                            $("#aresLoadingModal").modal('hide');
                        }).fail(function () {
                            $lastResponse = null;
                            $("#aresLoadingModal").modal('hide');
                            if ($feedbackDiv) {
                                $feedbackDiv.removeClass('valid-feedback').addClass('invalid-feedback').text('<?= __('Načtení se nezdařilo') ?>').toggle(true);
                            }
                        });
                    }

                    $(function () {
                        let $preloadIco = $("#dummy-preload-ico");
                        $preloadIco.keypress(function (e) {
                            if (e.which === 13) {
                                e.preventDefault();
                                loadAresData();
                            }
                        });
                        $preloadIco.removeClass('is-invalid');
                        $feedbackDiv = $preloadIco.closest('.form-group').find('.invalid-feedback');
                        $feedbackDiv.toggle(false);
                        $("#ares-load").click(function () {
                            loadAresData();
                        });
                    });
                </script>
            </div>
        </div>
        <div class="card-header">
            <?= __('Základní identifikace žadatele') ?>
        </div>
        <div class="card-body" id="po-basic">
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::PO_FULLNAME) ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::PO_CORPORATE_TYPE) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::PO_BUSINESS_ID) ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::PO_VAT_ID) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::PO_REGISTRATION_SINCE) ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::PO_REGISTRATION_DETAILS) ?>
                </div>
            </div>
        </div>
        <div class="card-body" id="fo-basic">
            <div class="row">
                <div class="col-md-2">
                    <?= $form->identity(Identity::FO_DEGREE_BEFORE); ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::FO_FIRSTNAME); ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::FO_SURNAME); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->identity(Identity::FO_DEGREE_AFTER); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::FO_DATE_OF_BIRTH); ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::FO_VAT_ID); ?>
                </div>
            </div>
        </div>
        <div class="card-header">
            <?= __('Trvalá Adresa / Sídlo Právnické Osoby') ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::RESIDENCE_ADDRESS_STREET); ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::RESIDENCE_ADDRESS_CITY); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::RESIDENCE_ADDRESS_ZIP); ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::RESIDENCE_ADDRESS_CITY_PART); ?>
                </div>
            </div>
        </div>
        <div class="card-header">
            <?= __('Kontaktní informace') . ' / ' . __('Bankovní spojení') ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::CONTACT_PHONE) ?>
                </div>
                <div class="col-md">
                    <?= $form->identity(Identity::CONTACT_EMAIL, ['default' => $this->getUser('email')]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->identity(Identity::CONTACT_DATABOX) ?>
                </div>
            </div>
            <hr/>
            <?= $form->identity(Identity::BANK_NAME) ?>
            <div class="form-row">
                <div class="col-md-8 col-7 m-0 p-0">
                    <?= $form->identity(Identity::BANK_NUMBER) ?>
                </div>
                <div class="col-md-4 col-5 m-0 p-0">
                    <?= $form->identity(Identity::BANK_CODE, ['type' => 'bank_code', 'prepend' => '<div class="input-group-text">/</div>']) ?>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?= $submitButton ?>
        </div>
    </div>

    <script type="text/javascript">
        const PO_SEGMENTS = [
            'ares-loader',
            'po-basic',
        ];
        const FO_SEGMENTS = [
            'fo-basic',
        ];

        let $cityParts = [];
        const $currentValues = <?= json_encode($form->getSelectsCurrentValues()) ?>;
        const $citiesToParts = <?= json_encode($form->getCitiesToPartsMap()) ?>;

        function sideloadCityParts($elm, $data = null) {
            try {
                if ($elm.data('select2')) {
                    $elm.select2('destroy');
                }
            } catch (ignore) {
            }
            try {
                $elm.empty();
            } catch (ignore) {
            }

            let $defaultValue = null;
            if ($elm.prop('id') && $elm.prop('id') in $currentValues) {
                let $rawValue = $currentValues[$elm.prop('id')];
                let $value = parseInt($rawValue);
                if ($value > 0) {
                    $defaultValue = $rawValue;
                }
            }

            $data = $data ? $data : $.map($cityParts, function (name, key) {
                if (key === $defaultValue) {
                    return {'id': key, 'text': name, 'selected': true};
                }
                return {'id': key, 'text': name};
            });
            let $placeholder = [{
                id: '',
                text: '<?= __('Prosím vyberte si z možností'); ?>'
            }];
            $data = $placeholder.concat($data);

            $elm.select2({
                data: $data,
                theme: 'classic',
                allowClear: true,
                placeholder: '<?= __('Prosím vyberte si z možností'); ?>',
                minimumInputLength: 3
            });
        }

        $(function () {
            $("#identity-is-po").change(function () {
                let $isPO = $(this).is(':checked');
                let $allowed = $isPO ? PO_SEGMENTS : FO_SEGMENTS;
                let $disabled = $isPO ? FO_SEGMENTS : PO_SEGMENTS;

                $.each($disabled, (index, value) => {
                    $("#" + value).toggle(false);
                });
                $.each($allowed, (index, value) => {
                    $("#" + value).toggle(true);
                });
            }).change();


            $(".city-parts-sideload").each(function () {
                const $thisElement = $(this);
                $.get('<?= Router::url(['_name' => 'csu_municipality_parts_list_json'])?>', function (data) {
                    $cityParts = data;
                    sideloadCityParts($thisElement);
                });
            });

            $("#identity-address-residence-city").change(function () {
                const $forCity = parseInt($(this).val());

                const $links = $citiesToParts.filter(function (link) {
                    return link['csu_municipalities_number'] === $forCity;
                }).filter(function () {
                    return true;
                });

                let $matchingParts = [];
                $.each($links, function ($index, $link) {
                    const $key = $link['csu_municipality_parts_number'];
                    if ($key in $cityParts) {
                        $matchingParts[$key] = $cityParts[$key];
                    }
                });

                const $target = $(this).attr('id') === 'address-residence-city' ? $("#address-residence-city-part") : $("#address-postal-city-part");
                sideloadCityParts($target, $.map($matchingParts, function (name, key) {
                    if (name !== undefined) {
                        return {'id': key, 'text': name};
                    }
                }));
            });
        });
    </script>

    <div class="card m-2">
        <div class="card-header">
            <h2>
                <?= __('Rozpočet žádosti') ?>
            </h2>
            <?= RequestBudget::getValidationUserMessage($form->getRequest()->validateBudgetRequirements(), $form->getRequest()) ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <?= $form->budget('requested_amount', ['label' => __('Požadovaná podpora (Kč)')]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->budget('total_income', ['label' => __('Celkové výnosy projektu (Kč)')]) ?>
                </div>
                <div class="col-md">
                    <?= $form->budget('total_costs', ['label' => __('Celkové náklady na projekt (Kč)')]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?= $form->budget('total_own_sources', ['label' => __('Vlastní zdroje celkem (Kč)')]) ?>
                </div>
                <div class="col-md">
                    <?= $form->budget('total_other_subsidy', ['label' => __('Dotace z jiných zdrojů celkem (Kč)')]) ?>
                </div>
            </div>
        </div>
    </div>

<?php foreach ($form->getRequest()->getForms() as $request_form): ?>
    <?php $formController = $request_form->getFormController($form->getRequest()) ?>
    <div class="card m-2">
        <h2 class="card-header"><?= $formController->getFormName() ?></h2>
        <div class="card-body">
            <?= $formController->render($this) ?>
        </div>
        <div class="card-footer">
            <?= $submitButton ?>
        </div>
    </div>
<?php endforeach; ?>

<?php
echo $this->Form->end();
?>