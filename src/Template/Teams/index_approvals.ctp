<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals Appeal[]
 * @var $requests Request[]
 */

$this->assign('title', __('Schvalovatelé'));
echo $this->element('simple_datatable');
?>

<div class="row mb-2">
    <div class="col-md text-right">
        <?= $this->Html->link('<i class="fas fa-share add-space"></i>' . __('Otevřít rozhraní pro jednání komise / výboru'), ['_name' => 'approvals_meeting'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
    </div>
</div>

<table id="dtable" class="table">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Stav žádosti') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Program') ?></th>
            <th><?= __('Výzva') ?></th>
            <th><?= __('Oblast podpory') ?></th>
            <th><?= __('Navržená částka') ?></th>
            <th><?= __('Slovní hodnocení') ?></th>
            <th><?= __('Bodové hodnocení') ?></th>
            <th><?= __('Počet hodnocení') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($requests as $request) : ?>
            <tr>
                <td><?= $this->Html->link($request->id, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $request->reference_number ?></td>
                <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
                <td><?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
                <td><?= $request->program->name ?></td>
                <td><?= (isset($appeals) && !empty($appeals[$request->appeal_id]) ? $appeals[$request->appeal_id] : '') ?></td>
                <td><?= $request->program->realm->name ?></td>
                <td data-type="currency">
                    <?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount, 'CZK') ?>
                </td>
                <td>
                    <?= $request->comment ?>
                </td>
                <td>
                    <?php
                    $request->prefillFinalCriterium();
                    echo $request->getFinalCriteriaSum() . ' z ' . ($request->getMaximumCriteriumSum() > 0
                        ? $request->getMinimumCriteriumSum() . '-' . $request->getMaximumCriteriumSum()
                        : 0);
                    ?>
                </td>
                <td>
                    <?= count($request->evaluations ?? []) ?>
                </td>
                <td>
                    <?php
                    if ($request->request_state_id === RequestState::STATE_FORMAL_CHECK_APPROVED) {
                        echo $this->Html->link(__('Upravit / Schválit návrhy'), ['_name' => 'approvals_edit', $request->id], ['class' => 'btn btn-success']);
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>