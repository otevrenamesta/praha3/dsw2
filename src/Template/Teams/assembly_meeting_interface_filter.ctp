<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $title string
 * @var $can_filter boolean
 * @var $can_show_evaluation_criteria boolean
 * @var $can_set_propsals_approved boolean
 * @var $can_set_requests_finished boolean
 * @var $appeals_with_programs Appeal[]
 * @var $team_programs int[]
 * @var $programs Program[]
 * @var $requests Request[]
 * @var $sums array[]
 * @var $team string
 */

$this->assign('title', $title);
echo $this->Html->css('table-with-inputs.css', ['block' => true]);
echo $this->Html->script('jquery.textarea-auto-expand.js', ['block' => true]);
echo $this->element('simple_datatable');

$this->_indexArrayByElementIds($programs);
$this->_indexArrayByElementIds($appeals_with_programs);

$filter_options = [];

foreach ($appeals_with_programs as $appeal) {
    $appeal_name = $appeal->_getDescriptiveName();
    $existing = $filter_options[$appeal_name] ?? [];
    foreach ($appeal->programs as $program) {
        if (!in_array($program->id, $team_programs)) {
            continue;
        }
        $existing[sprintf("%d:%d", $appeal->id, $program->id)] = $program->name;
    }
    $filter_options[$appeal_name] = $existing;
}

?>

<div class="card">
    <h2 class="card-header"><?= __('Vyberte si o kterých dotačních programech bude jednáno') ?></h2>
    <div class="card-body">
        <?php foreach ($filter_options as $appeal_name => $options) : ?>
            <fieldset>
                <legend><?= $appeal_name ?></legend>
                <div class="pl-5">
                    <?php
                    foreach ($options as $filter_spec => $program_name) {
                        echo $this->Form->control($filter_spec, ['type' => 'checkbox', 'label' => $program_name, 'value' => $filter_spec]);
                    }
                    ?>
                </div>
            </fieldset>
        <?php endforeach; ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Html->link(
            '<i class="fas fa-times add-space"></i>' . __('Zavřít'),
            ['_name' => ($this->getRequest()->getParam('_name') === 'approvals_meeting' ? 'team_approvals_index' : ($this->getRequest()->getParam('_name') === 'managers_meeting' ? 'team_managers_index' : 'team_proposals_index'))],
            ['escape' => false, 'class' => 'float-left float-start mr-2 btn btn-secondary']
        );
        echo $this->Html->link('<i class="fas fa-check add-space"></i>' . __('Zobrazit vybrané programy'), '#', ['class' => 'btn btn-success', 'id' => 'go-filter', 'escape' => false]);
        ?>
    </div>
</div>

<script type="text/javascript">
    const BASE_URL = <?= json_encode($this->getRequest()->getPath()) ?>;
    $(function() {
        $("#go-filter").on('click', function(event) {
            event.preventDefault();
            let filter_param = $("input[type=checkbox]").get().reduce((accumulator, current, currentIndex, array) => {
                console.log(accumulator, current, currentIndex, array);
                let currentValue = $(current).is(':checked') ? $(current).val() + "," : "";
                return accumulator + currentValue;
            }, "");
            if (BASE_URL.slice(-1) !== '/') {
                filter_param = '/' + filter_param;
            }
            window.location.href = BASE_URL + filter_param;
        });
    });
</script>