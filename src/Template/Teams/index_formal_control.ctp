<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestType;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeals Appeal[]
 * @var $requests Request[]
 */

$this->assign('title', __('Formální kontrola'));
echo $this->element('simple_datatable');

$canManuallySubmitRequests = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS);
$orgCodeRequired = OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, false);

?>

<?php if ($canManuallySubmitRequests) : ?>
    <div class="row mb-2">
        <div class="col-md">
            <?= $this->Html->link(__('Evidovat novou papírovou žádost'), ['_name' => 'formal_control_paper_add'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php endif; ?>

<table id="dtable" class="table">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Stav žádosti') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Program') ?></th>
            <th><?= __('Výzva') ?></th>
            <th><?= __('Oblast podpory') ?></th>
            <th><?= __('Fond') ?></th>
            <?php if ($canManuallySubmitRequests) : ?>
                <th><?= __('Typ evidence žádosti') ?></th>
            <?php endif; ?>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($requests as $request) : ?>
            <?php
            if (
                !$canManuallySubmitRequests
                && in_array($request->request_state_id, [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], true) && empty($request->lock_when)
            ) {
                continue;
            }
            ?>
            <tr>
                <td>
                    <?= $this->Html->link('', '#', ['name' => $request->id]) ?>
                    <?= $this->Html->link($request->id, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?>
                </td>
                <td><?= $request->reference_number ?></td>
                <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
                <td><?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td>
                    <?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?>
                    <br />
                    <?= $request->user->email ?>
                </td>
                <td><?= $request->program->name ?></td>
                <td><?= (isset($appeals) && !empty($appeals[$request->appeal_id]) ? $appeals[$request->appeal_id] : '') ?></td>
                <td><?= $request->program->realm ? $request->program->realm->name : 'N/A' ?></td>
                <td><?= $request->program->realm ? $request->program->realm->fond->name : 'N/A' ?></td>
                <?php if ($canManuallySubmitRequests) : ?>
                    <td><?= RequestType::getLabel($request->request_type_id, true) ?></td>
                <?php endif; ?>
                <td>
                    <?php
                    if (in_array($request->request_state_id, [RequestState::STATE_READY_TO_SUBMIT, RequestState::STATE_NEW], true)) {
                        if ($request->canTransitionTo(RequestState::STATE_SUBMITTED) && OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS)) {
                            echo $this->Html->link(__('Označit jako odeslanou žádost'), ['_name' => 'formal_control_manual_submit', $request->id], ['class' => 'btn btn-success w-100']);
                        } elseif (!empty($request->lock_when) && empty($request->paper_evidenced_by_user_id)) {
                            echo sprintf("%s %s", __('Opravu žádosti může žadatel provést do:'), $request->lock_when->nice());
                            echo '<br/>';
                            echo $this->Html->link(__('Znovu odeslat e-mail žadateli'), ['_name' => 'formal_control_send_notice', $request->id], ['class' => 'btn btn-warning w-100']);
                        }
                    }

                    if (!RequestState::canUserEditRequest($request->request_state_id) && !in_array($request->request_state_id, [RequestState::STATE_SUBMITTED, RequestState::STATE_SUBMIT_LOCK], true) && $request->canTransitionTo(RequestState::STATE_SUBMITTED)) {
                        echo $this->Form->postButton(__('Vrátit žádost mezi žádosti k formální kontrole'), ['_name' => 'formal_control_check_request', $request->id], ['class' => 'btn btn-warning', 'data' => ['request_state_id' => RequestState::STATE_SUBMITTED]]);
                    }

                    if ($request->request_state_id === RequestState::STATE_SUBMIT_LOCK) {
                        $link_text = $orgCodeRequired ? __('Potvrdit přijetí žádosti kódem') : __('Potvrdit přijetí žádosti');
                        echo $this->Html->link($link_text, ['_name' => 'formal_control_unlock_with_code', $request->id], ['class' => 'btn btn-primary w-100']);
                    }

                    if ($request->request_state_id === RequestState::STATE_SUBMITTED) {
                        echo $this->Html->link(__('Zkontrolovat obsah žádosti'), ['_name' => 'formal_control_check_request', $request->id], ['class' => 'btn btn-success w-100']);
                    }

                    if ($request->request_type_id === RequestType::PAPER_REQUEST && in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])) {
                        echo $this->Html->link(__('Upravit tuto papírovou žádost'), ['_name' => 'formal_control_paper_modify', 'id' => $request->id], ['class' => 'btn btn-primary w-100']);
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>