<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals Appeal[]
 * @var $requests Request[]
 */

$this->assign('title', __('Finalizace'));
echo $this->element('simple_datatable');
?>

<div class="row mb-2">
    <div class="col-md text-right">
        <?= $this->Html->link('<i class="fas fa-share add-space"></i>' . __('Otevřít rozhraní pro evidenci rozhodnutí rady / zastupitelstva'), ['_name' => 'managers_meeting'], ['class' => 'btn btn-primary', 'escape' => false]) ?>
    </div>
</div>

<table id="dtable" class="table">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Stav žádosti') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Program') ?></th>
            <th><?= __('Výzva') ?></th>
            <th><?= __('Oblast podpory') ?></th>
            <th><?= __('Částka podpory') ?></th>
            <th><?= __('Slovní hodnocení') ?></th>
            <th><?= __('Bodové hodnocení') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($requests as $request) : ?>
            <tr>
                <td><?= $this->Html->link('', '#', ['name' => $request->id]) ?><?= $this->Html->link($request->id, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $request->reference_number ?></td>
                <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
                <td><?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
                <td><?= $request->program->name ?></td>
                <td><?= (isset($appeals) && !empty($appeals[$request->appeal_id]) ? $appeals[$request->appeal_id] : '') ?></td>
                <td><?= $request->program->realm->name ?></td>
                <td data-type="currency">
                    <?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount, 'CZK') ?>
                </td>
                <td>
                    <?= $request->comment ?>
                </td>
                <td>
                    <?php
                    $request->prefillFinalCriterium();
                    echo $request->getFinalCriteriaSum() . ' z ' . ($request->getMaximumCriteriumSum() > 0
                        ? $request->getMinimumCriteriumSum() . '-' . $request->getMaximumCriteriumSum()
                        : 0);
                    ?>
                </td>
                <td>
                    <?php
                    if ($request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
                        echo $this->Html->link(__('Upravit výsledný status žádosti'), ['_name' => 'managers_edit', $request->id], ['class' => 'btn btn-success w-100']);
                    } else if ($request->request_state_id === RequestState::STATE_READY_TO_SIGN_CONTRACT) {
                        echo $this->Form->postLink(__('Nastavit stav "Smlouva podepsána"'), ['_name' => 'managers_edit', $request->id], ['class' => 'btn btn-success text-nowrap w-100', 'data' => ['STATE_CONTRACT_SIGNED' => 'STATE_CONTRACT_SIGNED']]);
                        echo $this->Form->postLink(__('Vrátit žádost do stavu "Připraveno ke schválení zastupitelstvem"'), ['_name' => 'managers_edit', $request->id], ['class' => 'btn btn-warning w-100', 'data' => ['STATE_REQUEST_APPROVED' => 'STATE_REQUEST_APPROVED']]);
                        echo $this->Form->postLink(__('Vyřadit žádost z dalšího dotačního procesu'), ['_name' => 'managers_edit', $request->id], ['class' => 'btn btn-danger w-100', 'data' => ['STATE_CLOSED_FINISHED' => 'STATE_CLOSED_FINISHED']]);
                    } else if ($request->request_state_id === RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT) {
                        echo $this->Form->postLink(__('Vrátit žádost do stavu "Připraveno k podpisu smlouvy"'), ['_name' => 'managers_edit', $request->id], ['class' => 'btn btn-warning w-100', 'data' => ['STATE_READY_TO_SIGN_CONTRACT' => 'STATE_READY_TO_SIGN_CONTRACT']]);
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>