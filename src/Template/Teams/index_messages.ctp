<?php

/**
 * @var $this AppView
 * @var $messages array
 * @var $users array
 * @var $users int
 */

use App\Middleware\OrgDomainsMiddleware;
use App\View\AppView;

$this->assign('title', __('Zprávy'));
echo count($messages) > 0 ? '' : '<div class="mt-3 alert alert-info">' .
  __('Interní komunikace mezi úředníky. Možnost kontaktovat členy týmu prostřednictvím krátkých zpráv.') .
  '</div>';
?>

<div class="row">
  <div class="col-md">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title m-1"><?= __('Seznam zpráv') ?></h5>
      </div>
      <div class="card-body">
        <?php
        echo count($messages) <= 0 ? __('- žádné zprávy -') : '';
        foreach ($messages as $key => $value) {
          echo '<div class="card mt-3">
        <div class="card-header d-block">
        <span class="card-title m-1">' .
            ($user === $value->user_id
              ? __('Komu') . ': <strong class="text-success">'  . ($users[$value->recipient_user_id] ?? '#' . $value->recipient_user_id)
              : __('Od') . ': <strong class="text-primary">' . ($users[$value->user_id] ?? '#' . $value->user_id)) .
            '</strong></span>
        <span class="float-end float-right">' . $value->created->nice() . '</span>
        </div><div class="card-body">' . sprintf("%s", $value->message) . '</div></div>';
        }
        ?>
      </div>
    </div>
  </div>

  <div class="col-md">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title m-1"><?= __('Odeslat zprávu') ?></h5>
      </div>
      <div class="card-body">

        <?php
        echo $this->Form->create(null);
        echo $this->Form->control('recipient', [
          'label' => __('Příjemce'),
          'options' =>  $users,
          'required' => true,
          'type' =>  'select',
          'empty' => __('Vyberte příjemce...')
        ]);

        echo $this->Form->control('text', [
          'data-quilljs' => 'data-quilljs',
          'default' => '',
          'label' => __('Zpráva'),
          'required' => true,
          'rows' => 2,
        ]);

        echo $this->Form->submit(__('Odeslat'), ['class' => 'btn btn-success m-1']);
        echo $this->Form->end();
        ?>
      </div>
    </div>
  </div>
</div>