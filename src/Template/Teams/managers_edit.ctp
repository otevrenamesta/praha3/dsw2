<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */
$this->assign('title', __('Nastavit výsledek zpracování žádosti'));
?>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h4><?= __('Nastavit výsledek zpracování žádosti') ?>
                    <?php
                    if ($this->isTeamFormalControlor()) {
                        $helpTitle = __('Sekce pro úředníky slouží k nastavení úředních informací: vlastní identifikátor, de minimis, veřejnosprávní kontrola a poznámka k žádosti');
                        echo '<span class="float-right" data-toggle="tooltip" data-placement="left" title="' . $helpTitle . '"><i class="far fa-question-circle help-icon"></i></span>';
                        echo $this->Html->link(
                            '<i class="fas fa-user-check"></i>' . __('Sekce pro úředníky'),
                            ['_name' => 'admin_section_edit', $request->id, '?' => ['from' => $this->getRequest()->getParam('action')]],
                            ['class' => 'float-right float-end btn btn-primary', 'escape' => false]
                        );
                    }
                    ?>
                </h4>
            </div>
            <div class="col text-right">
                <?= $this->Form->postLink(__('Vrátit zpět pro hodnocení, návrhy a schválení'), null, ['class' => 'btn btn-warning', 'data' => ['name' => 'STATE_FORMAL_CHECK_APPROVED']]) ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?php

        if ($request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
            echo $this->Form->create($request);
            echo $this->Form->control('reference_number', ['label' => __('Vlastní identifikátor žádosti (např. pořadové číslo, spisová značka, ...)')]);
            echo $this->Form->control('final_subsidy_amount', ['label' => __('Výsledná částka podpory (Kč)'), 'default' => $request->final_subsidy_amount ?? $request->request_budget->requested_amount]);
            echo $this->Form->control('comment', ['label' => __('Výsledné slovní hodnocení žádosti'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
            echo $this->Form->control('purpose', ['label' => __('Výsledná formulace účelu, na který bude dotace poskytnuta'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);

            echo $this->Form->submit(__('Uložit výsledek zpracování žádosti'), ['class' => 'btn btn-success m-2']);
            echo $this->Form->submit(__('Uložit a nastavit do stavu "Připraveno k podpisu smlouvy"'), ['type' => 'submit', 'name' => 'STATE_READY_TO_SIGN_CONTRACT', 'escape' => 'false', 'class' => 'btn btn-warning m-2']);
            echo $this->Form->submit(__('Uložit a nastavit do stavu "Ukončeno / Uzavřeno", pokud žádost nezískala podporu'), ['type' => 'submit', 'name' => 'STATE_CLOSED_FINISHED', 'escape' => 'false', 'class' => 'btn btn-danger m-2']);
            echo $this->Form->end();
        }
        if ($request->request_state_id === RequestState::STATE_READY_TO_SIGN_CONTRACT) {
            echo $this->Form->create($request);
            echo $this->Form->button(__('Nastavit stav "Smlouva podepsána"'), ['type' => 'submit', 'class' => 'btn btn-success', 'name' => 'STATE_CONTRACT_SIGNED']);
            echo $this->Form->end();
        }
        ?>
    </div>
</div>

<p class='m-5' />
<?php
echo $this->element('request_full_table', compact('request'));
