<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Identity;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $title string
 * @var $can_filter boolean
 * @var $can_show_evaluation_criteria boolean
 * @var $can_set_propsals_approved boolean
 * @var $can_set_requests_finished boolean
 * @var $appeals_with_programs Appeal[]
 * @var $team_programs int[]
 * @var $programs Program[]
 * @var $requests Request[]
 * @var $sums array[]
 * @var $team string
 */

$this->assign('title', $title);
echo $this->Html->css('table-with-inputs.css', ['block' => true]);
echo $this->Html->script('jquery.textarea-auto-expand.js', ['block' => true]);
echo $this->element('simple_datatable');

$max_overall = 0;
$programs_rendered = 0;

echo $this->Form->create();
echo $this->Form->unlockField('dtable_length');
?>
<script type="text/javascript">
    let individual_sums = {};

    function updateCurrentTotalSum() {
        let currentTotalSum = 0;
        for (const [key, value] of Object.entries(individual_sums)) {
            let sum = parseInt(value)
            if (!isNaN(sum)) {
                currentTotalSum += sum;
            }
        }
        $("#total-sum").text(currentTotalSum.toLocaleString('cs-CZ', {
            style: 'currency',
            currency: 'CZK'
        }));
    }
</script>

<?php if ($team === 'request_manager_programs') : ?>
    <div class="alert alert-dismissible alert-primary">
        <?= __('Jsou zobrazeny jen žádosti, u kterých lze aktuálně nastavit výsledek jednání rady / zastupitelstva') ?>
    </div>
<?php endif; ?>

<?php foreach ($appeals_with_programs as $appeal) : ?>
    <div class="alert alert-dark sticky-top">
        <div class="row">
            <div class="col-md-8">
                <?= $appeal->name ?>
            </div>
            <div class="col-md-4 text-right">
                <?= $this->Form->button('<i class="fas fa-check add-space"></i>' . __('Uložit'), ['class' => 'btn btn-success', 'escape' => false, 'type' => 'submit']) ?>
            </div>
        </div>
        <hr />
        <div class="row font-weight-bold">
            <div class="col-md-4">
                Název programu
            </div>
            <div class="col-md-2">
                Maximální částka k alokaci
            </div>
            <div class="col-md-2">
                Aktuálně vyčerpáno
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <hr />
        <?php
        foreach ($appeal->programs as $program) :
            if (!in_array($program->id, $team_programs, true)) {
                continue;
            }
            $programs_rendered++;
            $budget = $appeal->getProgramBudget($program->id);
            $max_overall += $budget;
            $sum = Hash::get($sums, sprintf("%d.%d", $appeal->id, $program->id), 0);
            $ratio = $budget > 0 ? round(($sum / $budget) * 100) : 100;
        ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $program->name ?>
                </div>
                <div class="col-md-2">
                    <?= $budget > 0 ? Number::currency($budget, 'CZK') : 'Bez omezení' ?>
                </div>
                <div class="col-md-2" id="program-<?= $program->id ?>-sum">
                    <?= Number::currency($sum, 'CZK') ?>
                </div>
                <div class="col-md">
                    <div class="progress">
                        <div id="program-<?= $program->id ?>-progress" class="progress-bar" role="progressbar" style="width: <?= $ratio ?>%" aria-valuenow="<?= $sum ?>" aria-valuemin="0" aria-valuemax="<?= $budget ?? $sum ?>">
                            <?= $sum ?> Kč
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            let program_filter = "input[type=number][data-appeal=<?= $appeal->id ?>][data-program=<?= $program->id ?>]";
                            let max_budget = <?= intval($budget) ?>;
                            let progressbar_selector = "#program-<?= $program->id ?>-progress";
                            let sum_text_selector = "#program-<?= $program->id ?>-sum";
                            let program_id = <?= $program->id ?>;

                            $(program_filter)
                                .on('input change', function() {
                                    let currentSum = 0;
                                    $(program_filter).each(function() {
                                        let val = parseInt($(this).val());
                                        if (!isNaN(val)) {
                                            currentSum += val;
                                        }
                                    })
                                    individual_sums[program_id] = currentSum;
                                    updateCurrentTotalSum();
                                    let currentSumFormatted = currentSum.toLocaleString('cs-CZ', {
                                        style: 'currency',
                                        currency: 'CZK',
                                    });
                                    let $progress = $(progressbar_selector);
                                    let ratio = max_budget > 0 ? Math.round((currentSum / max_budget) * 100) : 100;
                                    $progress.prop('aria-valuenow', currentSum);
                                    $progress.css('width', ratio + '%');
                                    $progress.text(currentSumFormatted);
                                    $(sum_text_selector).text(currentSumFormatted);
                                })
                                .trigger('change');
                        });
                    </script>
                </div>
            </div>
        <?php
        endforeach;
        ?>
        <hr />
        <div class="row font-weight-bold <?= $programs_rendered > 1 ? '' : 'd-none' ?>">
            <div class="col-md-4 font-weight-bold">
                Součet
            </div>
            <div class="col-md-2">
                <?= $max_overall > 0 ? Number::currency($max_overall, 'CZK') : __('Bez omezení') ?>
            </div>
            <div class="col-md-2" id="total-sum">
                123 456,00 Kč
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php if ($can_set_requests_finished || $can_set_propsals_approved) : ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <?php
        if ($can_set_propsals_approved) {
            echo $this->Html->link('<i class="fas fa-check add-space"></i>' . __('Nastavit všechny žádosti do stavu "Návrhy schváleny"'), '#', ['class' => 'btn btn-primary m-2', 'id' => 'set-proposals-approved', 'escape' => false]);
        }
        if ($can_set_requests_finished) {
            echo $this->Html->link('<i class="fas fa-check add-space"></i>' . __('Nastavit žádosti, podle výše podpory, do stavu "Připraveno k podpisu smlouvy" nebo "Žádost ukončena"'), '#', ['class' => 'btn btn-warning m-2', 'id' => 'set-requests-final', 'escape' => false]);
        }
        ?>
    </nav>
    <script type="text/javascript">
        $(function() {
            $("#set-proposals-approved").on('click', function(event) {
                $("table input[type=radio]").filter('[value=<?= RequestState::STATE_REQUEST_APPROVED ?>]').trigger('click');
                if (event) {
                    event.preventDefault();
                }
            });
            $("#set-requests-final").on('click', function(event) {
                $("table input[type=radio]").filter(function() {
                    var $amount = parseInt($("input[type=number]", $(this).closest('tr')).val());
                    var expectedValue = $amount > 0 ? <?= RequestState::STATE_READY_TO_SIGN_CONTRACT ?> : <?= RequestState::STATE_CLOSED_FINISHED ?>;
                    return parseInt($(this).val()) === expectedValue;
                }).trigger('click');
                if (event) {
                    event.preventDefault();
                }
            });
        });
    </script>
<?php endif; ?>

<table class="table table-bordered" id="dtable">
    <colgroup>
        <col class="w-5">
        <col class="w-5">
        <col class="w-20">
        <col class="w-20">
        <col class="w-20">
    </colgroup>
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Slovní hodnocení žádosti') ?></th>
            <th><?= __('Úprava účelu na který je dotace poskytnuta') ?></th>
            <th><?= __('Výše podpory') ?></th>
            <?php if ($can_set_propsals_approved || $can_set_requests_finished) : ?>
                <th><?= __('Stav žádosti') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php
        $form_control_key = 'requests.%d.%s';
        ?>
        <?php foreach ($requests as $request) : ?>
            <tr>
                <td>
                    <?= $request->id ?>
                </td>
                <td><?= $request->reference_number ?></td>
                <td>
                    <?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id], ['target' => '_blank']) ?>
                    <br />
                    <?= Identity::format($request->getFlatIdentities()) ?>
                    <?php if ($can_filter) : ?>
                        <hr />
                        <?= $request->program->name ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?= $this->Form->control(sprintf($form_control_key, $request->id, 'comment'), ['label' => false, 'type' => 'textarea', 'data-noquilljs' => true, 'default' => $request->comment]) ?>
                </td>
                <td>
                    <?= $this->Form->control(sprintf($form_control_key, $request->id, 'purpose'), ['label' => false, 'type' => 'textarea', 'data-noquilljs' => true, 'default' => $request->purpose]) ?>
                </td>
                <td data-type="number" data-order="<?= $request->getFinalSubsidyAmount() ?>">
                    <?= $this->Form->control(
                        sprintf($form_control_key, $request->id, 'final_subsidy_amount'),
                        [
                            'label' => false,
                            'type' => 'number',
                            'default' => $request->getFinalSubsidyAmount(),
                            'data-appeal' => $request->appeal_id,
                            'data-program' => $request->program_id,
                        ]
                    ) ?>
                    <hr />
                    <?= __('Částka žádaná') ?>
                    : <?= Number::currency($request->request_budget->requested_amount, 'CZK') ?>
                </td>
                <?php if ($can_set_propsals_approved || $can_set_requests_finished) : ?>
                    <td class="text-nowrap">
                        <?php
                        $request_state_options = [
                            RequestState::STATE_FORMAL_CHECK_APPROVED => __('Návrh'),
                        ];
                        if ($can_set_propsals_approved || $request->request_state_id === RequestState::STATE_REQUEST_APPROVED) {
                            $request_state_options[RequestState::STATE_REQUEST_APPROVED] = __('Schválený návrh');
                        }
                        if ($can_set_requests_finished) {
                            $request_state_options[RequestState::STATE_READY_TO_SIGN_CONTRACT] = __('Žádost získala podporu');
                            $request_state_options[RequestState::STATE_CLOSED_FINISHED] = __('Žádost nezískala podporu');
                        }
                        ?>
                        <?= $this->Form->control(sprintf($form_control_key, $request->id, 'request_state_id'), [
                            'type' => 'radio',
                            'label' => false,
                            'options' => $request_state_options,
                            'default' => $request->request_state_id,
                        ]) ?>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php
echo $this->Form->end();
