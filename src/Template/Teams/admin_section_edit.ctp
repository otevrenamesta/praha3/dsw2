<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

$this->assign('title', __('Sekce pro úředníky'));

?>
<div class="card">
  <h4 class="card-header"><?= __('Sekce pro úředníky') ?></h4>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-6">
        <?php
        $n = 0;
        echo $this->Form->create($request, ['type' => 'file']);
        echo '<div class="card mb-3"><h5 class="card-header text-info">' . __('Vlastní identifikátor') . '</h5><div class="card-body">';
        echo $this->Form->control('reference_number', ['label' => __('Vlastní identifikátor žádosti (např. pořadové číslo, spisová značka, ...)')]);
        echo '</div></div>';

        $type = 'de_minimis';
        $attachments = array_filter($request->files ?? [], function ($v) use ($type) {
          return $v['file_type'] === $type;
        });
        echo '<div class="card mb-3"><h5 class="card-header text-info">' . __('De minimis') . '</h5><div class="card-body">';
        echo $this->Form->control(
          $type,
          ['label' => __('Podpora <em>de minimis</em>'), 'escape' => false, 'type' => 'select', 'options' => array(0 => __('Ne'), 1 => __('Ano')), 'empty' => true, 'class' => 'w-50'],
        );
        echo $this->Form->control('de_minimis_comment', ['label' => __('Poznámka k <em>de minimis</em>'), 'escape' => false, 'type' => 'textarea']);
        echo $this->Form->hidden('file_type-' . $n, ['value' => $type]);
        echo $this->Form->control('filedata.' . $n++, ['type' => 'file', 'required' => false, 'label' => __('Příloha k  <em>de minimis</em>'), 'escape' => false]);
        if ($attachments) {
          echo __('Uložené přílohy') . '<ul>';
          foreach ($attachments as $key => $attachment) {
            echo '<li>' .
              $this->Html->link($attachment->original_filename, ['action' => 'fileDownload', $request->id, $attachment->id], ['target' => '_blank']) .
              $this->Html->link(
                '<i class="fas fa-times"></i>',
                ['action' => 'attachmentDelete', $request->id, $attachment->id, '?' => ['from' => $this->getRequest()->getQuery('from')]],
                ['class' => 'ml-2 text-danger', 'title' => __('Smazat'), 'escapeTitle' => false, 'confirm' => __('Opravdu chcete smazat tuto přílohu?')]
              ) . '</li>';
          }
          echo '</ul>';
        }
        echo '</div></div>';

        $type = 'public_control';
        $attachments = array_filter($request->files ?? [], function ($v) use ($type) {
          return $v['file_type'] === $type;
        });
        echo '<div class="card mb-3"><h5 class="card-header text-info">' . __('Veřejnosprávní kontrola') . '</h5><div class="card-body">';
        echo $this->Form->control($type, ['label' => __('Veřejnosprávní kontrola uskutečněna'), 'type' => 'checkbox']);
        echo $this->Form->control(
          'public_control_ok',
          ['label' => __('Výsledek kontroly v pořádku'), 'escape' => false, 'type' => 'select', 'options' => array(0 => __('Ne'), 1 => __('Ano')), 'empty' => true, 'class' => 'w-50'],
        );
        echo $this->Form->control('public_control_comment', ['label' => __('Poznámka ke kontrole'), 'escape' => false, 'type' => 'textarea']);
        echo $this->Form->hidden('file_type-' . $n, ['value' => $type]);
        echo $this->Form->control('filedata.' . $n++, ['type' => 'file', 'required' => false, 'label' => __('Příloha ke kontrole'), 'escape' => false]);
        if ($attachments) {
          echo __('Uložené přílohy') . '<ul>';
          foreach ($attachments as $key => $attachment) {
            echo '<li>' .
              $this->Html->link($attachment->original_filename, ['action' => 'fileDownload', $request->id, $attachment->id], ['target' => '_blank']) .
              $this->Html->link(
                '<i class="fas fa-times"></i>',
                ['action' => 'attachmentDelete', $request->id, $attachment->id, '?' => ['from' => $this->getRequest()->getQuery('from')]],
                ['class' => 'ml-2 text-danger', 'title' => __('Smazat'), 'escapeTitle' => false, 'confirm' => __('Opravdu chcete smazat tuto přílohu?')]
              ) . '</li>';
          }
          echo '</ul>';
        }
        echo '</div></div>';

        $type = 'request_comment';
        $attachments = array_filter($request->files ?? [], function ($v) use ($type) {
          return $v['file_type'] === $type;
        });
        echo '<div class="card mb-4"><h5 class="card-header text-info">' . __('Poznámka') . '</h5><div class="card-body">';
        echo $this->Form->control($type, ['label' => __('Poznámka k žádosti'), 'type' => 'textarea']);
        echo $this->Form->hidden('file_type-' . $n, ['value' => $type]);
        echo $this->Form->control('filedata.' . $n++, ['type' => 'file', 'required' => false, 'label' => __('Příloha k poznámce'), 'escape' => false]);
        if ($attachments) {
          echo __('Uložené přílohy') . '<ul>';
          foreach ($attachments as $key => $attachment) {
            echo '<li>' .
              $this->Html->link($attachment->original_filename, ['action' => 'fileDownload', $request->id, $attachment->id], ['target' => '_blank']) .
              $this->Html->link(
                '<i class="fas fa-times"></i>',
                ['action' => 'attachmentDelete', $request->id, $attachment->id, '?' => ['from' => $this->getRequest()->getQuery('from')]],
                ['class' => 'ml-2 text-danger', 'title' => __('Smazat'), 'escapeTitle' => false, 'confirm' => __('Opravdu chcete smazat tuto přílohu?')]
              ) . '</li>';
          }
          echo '</ul>';
        }
        echo '</div></div>';

        echo '<div class="card mb-3"><h5 class="card-header text-info">' . __('Termín pro podání vyúčtování') . '</h5><div class="card-body">';
        echo $this->Form->control('settlement_date', ['type' => 'date', 'label' => __('Termín pro podání vyúčtování'), 'empty' => true]);
        echo '</div></div>';

        echo $this->Form->hidden('from', ['value' => $this->request->getQuery('from')]);
        echo $this->request->getQuery('from') ? $this->Html->link('<i class="fas fa-times add-space"></i>' . __('Zavřít'), ['action' => $this->request->getQuery('from'), $this->request->id], ['escape' => false, 'class' => 'float-left float-start mr-2 btn btn-secondary']) : '';
        echo $this->Form->button('<i class="fas fa-check add-space"></i>' . __('Uložit změny'), ['class' => 'btn btn-success', 'escape' => false, 'type' => 'submit']);
        echo $this->Form->end();
        ?>
      </div>
    </div>
  </div>
</div>

<p class='m-5' />
<?php
echo $this->element('request_full_table', compact('request'));
echo $this->element('request_logs', compact('request'));
