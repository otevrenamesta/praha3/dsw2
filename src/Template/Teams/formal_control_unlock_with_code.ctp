<?php

use App\Model\Entity\Request;
use App\View\AppView;
use App\Model\Entity\OrganizationSetting;

$orgCodeRequired = OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, false);


/**
 * @var $this AppView
 * @var $request Request
 */

$this->assign('title', __('Potvrdit přijetí Potvrzení o elektronické evidenci žádosti'))
?>
<div class="card">
    <div class="card-header">
        <?php if ($orgCodeRequired): ?>
        <?= __('Prosím zadejte ověřovací kód z Potvrzení o elektronické evidenci žádosti') ?>
        <?php else: ?>
        <?= __('Prosím potvrďte přijetí Potvrzení o elektronické evidenci žádosti tlačítkem Potvrdit') ?>
        <?php endif ?>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create(null);
        if ($orgCodeRequired) {
            echo $this->Form->control('verification_code', ['label' => __('Kód pro příjem žádosti'), 'required' => true]);
        }
        echo $this->Form->submit(__('Potvrdit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>

<p class='m-5' />
<?php
echo $this->element('request_full_table', compact('request'));
echo $this->element('request_logs', compact('request'));
