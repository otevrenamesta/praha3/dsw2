<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

$this->assign('title', __('Detail žádosti'));

$filename = urlencode(trim($request->id));
echo $this->Html->link(
  __('Stáhnout žádost ve formě PDF'),
  ['action' => 'getRequestPdf', 'id' => $request->id, 'download' => $filename . '.pdf'],
  ['class' => 'btn btn-success m-2']
);
echo $this->Html->link(
  __('Otevřít jako PDF'),
  ['action' => 'getRequestPdfView', 'id' => $request->id],
  ['class' => 'btn btn-success m-2', 'target' => '_blank']
);

$listOfRoles = [
  $this->isTeamFormalControlor(),
  $this->isTeamEvaluator(),
  $this->isTeamProposals(),
  $this->isTeamApprovals(),
  $this->isTeamManagers(),
];
$rolePreviewOnly = $this->isTeamPreview() && !implode($listOfRoles);
$hideSendEmail = $this->getSiteSetting(OrganizationSetting::PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL) === true;

echo !(!$hideSendEmail || !($hideSendEmail && $rolePreviewOnly)) ? '' : $this->Html->link(
  '<i class="far fa-envelope"></i> &nbsp;' . __('Odeslat žadateli e-mail'),
  ['action' => 'sendEmail', 'id' => $request->id],
  ['class' => 'btn btn-warning m-2', 'escape' => false]
);

echo !($this->isManager() || $this->isTeamMember()) ? '' : $this->Html->link(
  '<i class="fas fa-coins"></i> &nbsp;' . __('Historie podpory žadatele'),
  ['_name' => 'detailFromUserId', $request->user_id],
  ['class' => 'btn btn-secondary m-2', 'escape' => false]
);

echo !$this->isUsersManager() ? '' : $this->Html->link(
  '<i class="fas fa-user-cog"></i> &nbsp;' . __('Správa žadatele'),
  ['_name' => 'admin_users_edit', $request->user_id],
  ['class' => 'btn btn-secondary m-2', 'escape' => false]
);

echo !$this->isTeamFormalControlor() ? '' : $this->Html->link(
  '<i class="fas fa-user-check"></i> &nbsp;' . __('Sekce pro úředníky'),
  ['_name' => 'admin_section_edit', $request->id, '?' => ['from' => $this->getRequest()->getParam('action')]],
  ['class' => 'btn btn-primary m-2', 'escape' => false]
);

echo $this->element('request_full_table', compact('request'));
echo $this->element('request_notifications', compact('request'));

echo $this->element('request_logs', compact('request'));
echo ($this->isSystemsManager() || $this->isPortalsManager())
  ? $this->element('request_history_logs', compact('request'))
  : '';
