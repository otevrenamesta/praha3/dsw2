<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestType;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $requests Request[]
 * @var $appeals Appeal[]
 * @var $states array
 * @var $defaultStates array
 * @var $withNotification
 */

$this->assign('title', __('Náhled'));
echo $this->element('simple_select2');
echo $this->element('simple_datatable');

$canManuallySubmitRequests = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS);
?>

<?= $this->Form->create(null, ['type' => 'get']) ?>
<div class="card mt-2 mb-2">
    <h2 class="card-header"><?= __('Filtrovat') ?></h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <?= $this->Form->control('appeal_id', ['options' => $appeals, 'label' => __('Dotační výzva')]) ?>
            </div>
            <div class="col-md-8">
                <?= $this->Form->control('state_ids', ['options' => $states, 'multiple' => true, 'class' => 'select2', 'label' => __('Stav žádosti')]) ?>
                <?= $this->element('table_status_filter') ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Filtrovat')) ?>
    </div>
</div>
<?= $this->Form->end() ?>

<table id="dtable" class="table">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Stav žádosti') ?></th>
            <?php if ($canManuallySubmitRequests) : ?>
                <th><?= __('Typ evidence žádosti') ?></th>
            <?php endif; ?>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Program') ?></th>
            <th><?= __('Výzva') ?></th>
            <th><?= __('Oblast podpory') ?></th>
            <th><?= __('Navržená částka') ?></th>
            <th><?= __('Účel použití finančních prostředků') ?></th>
            <?php if ($withNotifications) : ?>
                <th><?= __('Ohlášeno?') ?></th>
            <?php endif; ?>
            <th><?= __('Slovní hodnocení') ?></th>
            <th><?= __('Bodové hodnocení') ?></th>
            <th><?= __('Počet hodnocení') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($requests as $request) : ?>
            <tr>
                <td><?= $this->Html->link($request->id, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $request->reference_number ?></td>
                <td><?= RequestState::getLabelByStateId($request->request_state_id) ?></td>
                <?php if ($canManuallySubmitRequests) : ?>
                    <td><?= RequestType::getLabel($request->request_type_id, true) ?></td>
                <?php endif; ?>
                <td><?= $this->Html->link($request->name, ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
                <td><?= $request->program->name ?></td>
                <td><?= (isset($appeals) && !empty($appeals[$request->appeal_id]) ? $appeals[$request->appeal_id] : '') ?></td>
                <td><?= $request->program->realm->name ?></td>
                <td data-type="currency">
                    <?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount ?? 0, 'CZK') ?>
                </td>
                <td><?= $request->purpose ?></td>
                <?php if ($withNotifications) : ?>
                    <td class="text-center">
                        <?php
                        if ($hn = $request->hasNotifications()) {
                            $link = '/admin/my_teams/request_detail/' . $request->id . '/notifications';
                        } else {
                            $link = null;
                        }
                        ?>
                        <?= $this->element(
                            'simple_color_indicator',
                            ['bool' => $hn, 'tooltip' => 0, 'link' => $link]
                        ) ?></td>
                <?php endif; ?>
                <td><?= $request->comment ?></td>
                <td>
                    <?php
                    $request->prefillFinalCriterium();
                    echo $request->getFinalCriteriaSum() . ' z ' . ($request->getMaximumCriteriumSum() > 0
                        ? $request->getMinimumCriteriumSum() . '-' . $request->getMaximumCriteriumSum()
                        : 0);
                    ?>
                </td>
                <td>
                    <?= count($request->evaluations ?? []) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
