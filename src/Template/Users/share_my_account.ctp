<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $existing_shares array [email => email]
 * @var $origin User
 */

$this->assign('title', __('Sdílet tento účet s dalšími uživateli'));
echo $this->element('simple_select2');
echo $this->Form->create($origin);
?>
<div class="card">
    <div class="card-header">
        <?= __('Zadejte e-mailové adresy osob, se kterými chcete sdílet tento žadatelský účet') ?>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->control('allowed_users_emails', ['options' => $existing_shares, 'value' => array_keys($existing_shares), 'class' => 'select2tagsWithSeparators', 'multiple' => 'multiple', 'label' => sprintf("%s (%s)", __('E-mailové adresy'), __('Napište e-mail a stiskněte enter, pak napište další a opakujte postup')), 'empty' => true]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
