<?php

/**
 * @var $this AppView
 * @var $user User
 * @var $enabled_settings
 */

use App\Model\Entity\User;
use App\View\AppView;

$this->assign('title', __('Změnit nastavení oznámení:'));
?>
<h1><?= $this->fetch('title') ?></h1>
<?php
if (empty($enabled_settings)) {
    __('Pro tento účet nejsou k dispozici žádná nastavení oznámení.');
} else {
    echo $this->Form->create($user);
    if (array_key_exists('no_notifications', $enabled_settings)) {
        echo $this->Form->control('no_notifications', [
            'label' => __('Nezasílat notifikace o provedených ohlášeních'),
            'type' => 'checkbox',
            'checked' => $enabled_settings['no_notifications']
        ]);
    }
    echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
    echo $this->Form->end();
}
