<?php

/**
 * @var $this AppView
 * @var $user User
 */

use App\Model\Entity\User;
use App\View\AppView;

$this->assign('title', __('Změna e-mailu'));
?>
<h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($user);
echo $this->Form->control('new_email', ['label' => __('Nový e-mail'), 'type' => 'email']);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
