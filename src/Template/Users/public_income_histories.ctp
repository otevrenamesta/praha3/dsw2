<?php

/**
 * @var $this AppView
 * @var $history PublicIncomeHistory
 * @var $histories PublicIncomeHistory[]
 * @var $sources array
 */

use App\Model\Entity\PublicIncomeHistory;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;
use Cake\I18n\Number;

$this->assign('title', __('Historie přijaté veřejné podpory'));

$historyNotRequested = $this->getSiteSetting(OrganizationSetting::SUBSIDY_HISTORY_NOT_REQUESTED);
$historyYears = max(0, intval($this->getSiteSetting(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true)));
$historyYearsMessage = ($historyYears > 0
    ? sprintf("%s %d až %d", __('Doplňte informace o přijaté veřejné podpoře za roky'), intval(date('Y')) - $historyYears, date('Y'))
    : sprintf("%s %d", __('Přidejte informace o přijaté veřejné podpoře za rok'), date('Y'))) .
    ', ' . __('pokud zde již není uvedena') . '.';
?>

<h1><?= __('Přidat nový záznam historie') ?></h1>
<?= !$historyNotRequested ? '' : '<p class="alert alert-warning">' . __('Vyplnění informací o přijaté veřejné podpoře není povinné.') . '</p>' ?>
<?= $historyNotRequested ? '' : '<p class="alert alert-info">' . $historyYearsMessage . '</p>' ?>
<?php
echo $this->Form->create($history);
echo $this->Form->control('project_name', ['required' => 'required', 'label' => __('Název projektu')]);
echo $this->Form->control('fiscal_year', ['type' => 'number', 'min' => 2000, 'max' => intval(date('Y')), 'default' => intval(date('Y')) - 1, 'label' => __('Rok poskytnutí')]);
echo $this->Form->control('amount_czk', ['type' => 'number', 'label' => __('Výše podpory v Kč'), 'required' => true]);
echo $this->Form->control('public_income_source_id', ['options' => $sources, 'label' => __('Poskytovatel podpory')]);
echo '<div id="public-income-source">' .
    $this->Form->control('public_income_source_name', ['type' => 'text', 'label' => __('Název zdroje')]) .
    '</div>';
echo $this->Form->submit(__('Přidat'), ['class' => 'btn btn-success']);
echo $this->Form->end();
?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#public-income-source-id").change(function() {
            if ($(this).val() === '3') {
                $("#public-income-source").toggle(true);
            } else {
                $("#public-income-source").toggle(false);
                $("#public-income-source-name").val(null);
            }
        }).trigger('change');
    });
</script>

<hr />
<h2><?= $this->fetch('title') ?></h2>

<?php
array_multisort(array_column($histories, 'fiscal_year'), SORT_DESC, $histories);

$lastYear = null;
foreach ($histories as $row) {
    if ($row->fiscal_year != $lastYear) {
        $lastYear = $row->fiscal_year;
        if ($lastYear != null) {
            echo '</tbody></table>';
        }
        echo sprintf('<h3>%s</h3>', $row->fiscal_year);
?>
        <table class="table">
            <thead>
                <tr>
                    <th><?= __('Poskytovatel') ?></th>
                    <th class="w-50"><?= __('Název, popis projektu') ?></th>
                    <th><?= __('Výše podpory') ?></th>
                    <th><?= __('Vložil') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php } ?>
            <tr>
                <td><?= $row->public_income_source->source_name .
                        (isset($row->public_income_source_name) && !empty($row->public_income_source_name) ? ' (' . h($row->public_income_source_name) . ')' : '')
                    ?></td>
                <td><?= $row->project_name ?></td>
                <td><?= Number::currency($row->amount_czk, 'CZK') ?></td>
                <td><?= empty($row->added_by) ? __('uživatel') : $row->added_by  ?></td>
                <td><?= $row->id > 0
                        ? $this->Html->link(__('Smazat'), ['action' => 'incomeHistoryDelete', $row->id], ['class' => 'text-danger'])
                        : '' ?></td>
            </tr>
        <?php } ?>
            </tbody>
        </table>