<?php
/**
 * @var $this AppView
 * @var $user User
 */

use App\Model\Entity\User;
use App\View\AppView;

$this->assign('title', __('Nastavení hesla nového uživatele'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<?php
echo $this->Form->create($user);
echo $this->Form->control('email', ['readonly' => 'readonly']);
echo $this->Form->control('new_password', ['label' => __('Nové heslo'), 'type' => 'password']);
echo $this->Form->control('new_password_check', ['label' => __('Nové heslo (znovu pro kontrolu)'), 'type' => 'password']);
echo $this->Form->submit(__('Nastavit heslo'));
echo $this->Form->end();