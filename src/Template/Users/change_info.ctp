<?php

/**
 * @var $this AppView
 * @var $identityForm IdentityForm
 */

use App\Controller\Component\IsdsComponent;
use App\Form\IdentityForm;
use App\Model\Entity\Identity;
use App\Model\Entity\User;
use App\View\AppView;
use Cake\Routing\Router;
use Cake\I18n\Number;
use Cake\Filesystem\File;

$this->assign('title', __('Moje identita'));
echo $this->element('simple_select2');
?>
<?= $this->Form->create($identityForm, ['type' => 'file']);
if (empty($identityForm->getErrors())) {
    $identityForm->setErrors(['preload-ico' => ['Zadejte IČ']]);
}
$identityForm->setFormHelper($this->Form);
$submitButton = $this->Form->submit(__('Uložit informace'), ['class' => 'btn btn-success m-2']);
?>
<div class="card m-2" id="ares-loader">
    <div class="card-header">
        <?= $identityForm->control(Identity::IS_PO) ?>
    </div>
    <div class="card-body fo-hide" style="display: none;">
        <?= $this->Form->control('preload-ico', ['type' => 'number', 'label' => __('Vyplnit z ARES podle IČO')]) ?>
    </div>
    <div class="card-footer fo-hide" style="display: none;">
        <?= $this->Html->link(__('Načíst data z ARES'), '#', ['class' => 'btn btn-success', 'id' => 'ares-load']) ?>
    </div>
</div>

<div class="card m-2" id="fo-basic">
    <h6 class="card-header"><?= __('Fyzická osoba nepodnikající') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md-2">
                <?= $identityForm->control(Identity::FO_DEGREE_BEFORE); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_FIRSTNAME); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_SURNAME); ?>
            </div>
            <div class="col-md-2">
                <?= $identityForm->control(Identity::FO_DEGREE_AFTER); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_DATE_OF_BIRTH); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::FO_VAT_ID); ?>
            </div>
        </div>
        <div class="row">
            <?php
            if (!boolval($identityForm->getData(Identity::IS_PO))) {
                foreach (Identity::ALL_VERIFIED_INFO_FO_BASIC as $verifiedInfoFieldId) {
                    $verifiedValue = $identityForm->getData($verifiedInfoFieldId);
                    if (!empty($verifiedValue) && $verifiedInfoFieldId === Identity::ISDS_VERIFIED_FO_ROB_IDENT) {
                        $verifiedValue = boolval($verifiedValue) ? __('ANO') : __('NE');
                    }
                    $label = Identity::getFieldLabel($verifiedInfoFieldId);
                    if (!empty($verifiedValue)) : ?>
                        <div class="col-md-6">
                            <?= $this->Form->control($verifiedInfoFieldId, ['disabled' => 'disabled', 'value' => $verifiedValue, 'label' => $label, 'type' => 'text']) ?>
                        </div>
            <?php
                    endif;
                }
            }
            ?>
        </div>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card m-2" id="po-basic">
    <h6 class="card-header"><?= __('Právnická osoba / Fyzická osoba podnikající s IČO') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_FULLNAME) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_CORPORATE_TYPE) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_BUSINESS_ID) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_VAT_ID) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_REGISTRATION_SINCE) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::PO_REGISTRATION_DETAILS) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card m-2" id="po-statutory">
    <h6 class="card-header">
        <?= __('Statutární orgán zastupující právnickou osobu žadatele') ?>
    </h6>
    <div class="card-body table-responsive">
        <table class="table table-bordered with-inputs" id="po-statutories-table">
            <colgroup>
                <col class="w-20">
                <col class="w-20">
                <col class="w-20">
                <col class="w-20">
                <col class="w-20">
            </colgroup>
            <thead class="thead-dark">
                <tr>
                    <th><?= __('Celé jméno statutárního zástupce (vč. titulu)') ?></th>
                    <th><?= __('Právní důvod zastoupení') ?></th>
                    <th><?= __('Telefonní číslo') ?></th>
                    <th><?= __('E-mailová adresa') ?></th>
                    <th><?= __('Zasílat na e-mail oznámení ohledně podaných žádostí o dotaci') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?= $identityForm->control(Identity::STATUTORY_FULLNAME) ?>
                    </td>
                    <td>
                        <?= $identityForm->control(Identity::STATUTORY_REPRESENTATION_REASON) ?>
                    </td>
                    <td>
                        <?= $identityForm->control(Identity::STATUTORY_PHONE) ?>
                    </td>
                    <td>
                        <?= $identityForm->control(Identity::STATUTORY_EMAIL) ?>
                    </td>
                    <td>
                        <?= $identityForm->control(Identity::STATUTORY_SEND_EMAIL_NOTIFICATIONS) ?>
                    </td>
                </tr>
                <?php for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_STATUTORY_MEMBERS; $i++) : ?>
                    <tr class="tr-inputs <?= $identityForm->getNumberOfStatutoryMembers() > $i ? 'visible' : 'd-none' ?>" data-po-statutory="<?= $i ?>">
                        <?php foreach (Identity::FULL_STATUTORY_TEMPLATE as $interestColumn) : ?>
                            <td>
                                <?= $identityForm->control($interestColumn, ['row' => $i]) ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endfor ?>
                <tr>
                    <td>
                        <div class="row no-gutters">
                            <div class="col">
                                <a id="po-statutory-add" class="btn btn-success w-100" style="margin: 0; border: 0; color: white;">+</a>
                            </div>
                            <div class="col">
                                <a id="po-statutory-remove" class="btn btn-danger w-100" style="margin: 0; border: 0; color: white;">-</a>
                            </div>
                        </div>
                    </td>
                    <td colspan="4"></td>
                </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            let currentStatutoriesCount = <?= $identityForm->getNumberOfStatutoryMembers() ?>;
            $(function() {
                $("#po-statutory-remove").click(function() {
                    let $target = $("#po-statutories-table tr.visible").last();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $("input, select", $target).val("");
                    $("input[type=checkbox]", $target).prop('checked', false);
                    currentStatutoriesCount--;
                    $(".single-statutory-hide").toggle(currentStatutoriesCount > 0);
                });
                $("#po-statutory-add").click(function() {
                    let $target = $("#po-statutories-table tr.d-none").first();
                    $target.toggleClass('visible').toggleClass('d-none');
                    currentStatutoriesCount++;
                    $(".single-statutory-hide").toggle(currentStatutoriesCount > 0);
                });
                $(".single-statutory-hide").toggle(currentStatutoriesCount > 0);
            });
        </script>
    </div>
    <div class="card-footer">
        <div class="single-statutory-hide">
            <?= $identityForm->control(Identity::STATUTORIES_ACT_STANDALONE) ?>
        </div>
        <?= $submitButton ?>
    </div>
</div>
<?php 
/* #408
<div class="card m-2" id="po-interests">
    <h6 class="card-header"><?= __('Osoby s podílem v této PO / skuteční majitelé') ?></h6>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Do Příloh v závěru sekce Moje identita vložte výpis z Evidence skutečných majitelů který získáte v ') .
                $this->Html->link(__('Informačním systému evidence skutečných majitelů'), 'https://esm.justice.cz/ias/issm/rejstrik-$sm?jenPlatne=PLATNE&polozek=50&typHledani=STARTS_WITH&typHledaniSpolku=ALL') .
                __(' NEBO vyplňte níže uvedený formulář. <strong>Zvolte tu variantu, kterou vyžaduje poskytovatel dotace, o kterou žádáte.</strong> Tato informace je vyžadována kvůli naplnění zákonné povinnosti evidence vlastnické struktury a skutečného majitele u příjemců dotací.') ?>
            <br />
            <?= __('Tato povinnost je evidována v')
                . ' '
                . $this->Html->link(__('Zákonu č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu'), 'https://www.zakonyprolidi.cz/cs/2008-253#p4-4-b')
                . ', '
                . $this->Html->link(__('Zákonu č. 37/2021 Sb. o evidenci skutečných majitelů'), 'https://www.zakonyprolidi.cz/cs/2021-37#p38-3')
                . ' a '
                . $this->Html->link(__('Zákonu č. 250/2000 Sb. o rozpočtových pravidlech územních rozpočtů'), 'https://www.zakonyprolidi.cz/cs/2015-24#cl1')
            ?>
            <hr />
            <strong>
                <?=
                __(
                    'Za skutečného majitele je považována fyzická osoba, která má fakticky nebo právně možnost vykonávat přímo nebo nepřímo rozhodující vliv v právnické osobě. Pro správné vyplnění použijte informační systém Evidence skutečných majitelů '
                )
                    . $this->Html->link('https://esm.justice.cz/', 'https://esm.justice.cz/') . '.'
                ?>
            </strong>
            <hr />
            <?= __('Více informací') ?>:
            <ul>
                <li><?= $this->Html->link('epravo.cz: Praktický přehled k nové evidenci skutečných majitelů', 'https://www.epravo.cz/top/clanky/prakticky-prehled-k-nove-evidenci-skutecnych-majitelu-113124.html') ?></li>
                <li><?= $this->Html->link('Certifix: Dotační úřady - poskytovatelé dotací z českých rozpočtů', 'https://www.certifix.eu/proc-je-certifikace-uzitecna-pro/#poskytovatele-dotaci-cr') ?></li>
                <li><?= $this->Html->link('Frank Bold: Kdo je skutečným majitelem?', 'https://www.fbadvokati.cz/cs/clanky/2209-evidence-skutecnych-majitelu-koho-se-tyka-jak-se-registrovat-a-dalsi-otazky#:~:text=Kdo%20je%20skute%C4%8Dn%C3%BDm%20majitelem') ?></li>
            </ul>
        </div>
        <small><?= __('Nepovinné pokud jste fyzická podnikající osoba.') ?>
            <br /><?= __('V každém podílu musí být vyplněno alespoň Název skutečného majitele, IČO nebo Datum narození a Stát') ?>
        </small>
        <table class="table table-bordered with-inputs" id="po-interests-table">
            <colgroup>
                <col class="w-10">
                <col class="w-25">
                <col class="w-20">
                <col class="w-10">
                <col class="w-25">
            </colgroup>
            <thead class="thead-dark">
                <tr>
                    <th><?= __('Název skutečného majitele') ?></th>
                    <th><?= __('IČO <br/>(právnická osoba)') ?></th>
                    <th><?= __('Datum narození <br/>(fyzická osoba)') ?></th>
                    <th><?= __('Stát') ?></th>
                    <th><?= __('Velikost podílu') ?></th>
                </tr>
            </thead>
            <?php for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_FILLED_INTERESTS; $i++) : ?>
                <tr class="tr-inputs <?= $identityForm->getNumberOfFilledInterests() > $i ? 'visible' : 'd-none' ?>" data-po-interest="<?= $i ?>">
                    <?php foreach (Identity::FULL_INTEREST_TEMPLATE as $interestColumn) : ?>
                        <td>
                            <?= $identityForm->control($interestColumn, ['row' => $i]) ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endfor ?>
            <tr>
                <td colspan="2">
                    <div class="row no-gutters">
                        <div class="col">
                            <a id="po-interest-add" class="btn btn-success w-100" style="margin: 0; border: 0; color: white;">+</a>
                        </div>
                        <div class="col">
                            <a id="po-interest-remove" class="btn btn-danger w-100" style="margin: 0; border: 0; color: white;">-</a>
                        </div>
                    </div>
                </td>
                <td colspan="3"></td>
            </tr>
        </table>

        <script type="text/javascript">
            let currentOwnedCount = <?= $identityForm->getNumberOfFilledInterests() ?>;
            $(function() {
                $("#po-interest-remove").click(function() {
                    let $target = $("#po-interests-table tr.visible").last();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $("input, select", $target).val("");
                });
                $("#po-interest-add").click(function() {
                    let $target = $("#po-interests-table tr.d-none").first();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $(".select2-container", $target).css('width', '100%');
                });
            });
        </script>

        <style type="text/css">
            table.with-inputs a {
                border-radius: 0;
                border: 0;
            }

            table.with-inputs td input {
                width: 100%;
                height: 100%;
                border: 0;
            }

            table.with-inputs label {
                display: none;
            }

            table.with-inputs .form-group {
                margin: 0;
            }

            table.with-inputs td {
                padding: .25rem;
            }

            table.with-inputs div.form-group.checkbox input {
                width: inherit;
                height: 1rem;
            }

            table.with-inputs div.form-group.checkbox {
                width: 100%;
            }

            .w-5 {
                width: 5% !important;
            }

            .w-15 {
                width: 15% !important;
            }

            .w-25 {
                width: 25% !important;
            }

            .w-20 {
                width: 20% !important;
            }

            .w-50 {
                width: 50% !important;
            }

            .w-10 {
                width: 10% !important;
            }
        </style>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>
*/
?>

<div class="card m-2" id="po-another-share-div">
    <h6 class="card-header"><?= __('Podíl v další právnické osobě') ?></h6>
    <div class="card-body">
        <div class="card-text p-2">
            <div class="col">
                <?= $this->Form->control(
                    Identity::PO_ANOTHER_SHARE,
                    [
                        'type' => 'checkbox',
                        'label' => Identity::getFieldLabel(Identity::PO_ANOTHER_SHARE),
                        'checked' => boolval($identityForm->getData(Identity::PO_ANOTHER_SHARE))
                    ]
                ) ?>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="po-owned-interests">
    <h6 class="card-header"><?= __('Právnické osoby v nichž má žadatel přímý obchodní podíl') ?></h6>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Pokud žadatel vlastní obchodní podíl v jiné právnické osobě, je povinen podle zákona tuto skutečnost nezamlčet, při žádosti o dotaci') ?>
        </div>
        <table class="table table-bordered with-inputs" id="po-owned-interests-table">
            <colgroup>
                <col class="w-25">
                <col class="w-25">
                <col class="w-25">
                <col class="w-25">
            </colgroup>
            <thead class="thead-dark">
                <tr>
                    <th><?= __('Velikost podílu') ?></th>
                    <th><?= __('IČO vlastněné společnosti') ?></th>
                    <th><?= __('Název vlastněné společnosti') ?></th>
                    <th><?= __('Stát <br/>(sídlo společnosti)') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) : ?>
                    <tr class="tr-inputs <?= $identityForm->getNumberOfFilledOwnedInterests() > $i ? 'visible' : 'd-none' ?>" data-po-owned-interest="<?= $i ?>">
                        <?php foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestColumn) : ?>
                            <td>
                                <?= $identityForm->control($interestColumn, ['row' => $i]) ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endfor ?>
                <tr>
                    <td>
                        <div class="row no-gutters">
                            <div class="col">
                                <a id="po-owned-interest-add" class="btn btn-success w-100" style="margin: 0; border: 0; color: white;">+</a>
                            </div>
                            <div class="col">
                                <a id="po-owned-interest-remove" class="btn btn-danger w-100" style="margin: 0; border: 0; color: white;">-</a>
                            </div>
                        </div>
                    </td>
                    <td colspan="3"></td>
                </tr>
            </tbody>
        </table>

        <script type="text/javascript">
            let currentCount = <?= $identityForm->getNumberOfFilledOwnedInterests() ?>;
            $(function() {
                $("#po-owned-interest-remove").click(function() {
                    let $target = $("#po-owned-interests-table tr.visible").last();
                    $target.toggleClass('visible').toggleClass('d-none');
                    $("input, select", $target).val("");
                });
                $("#po-owned-interest-add").click(function() {
                    let $target = $("#po-owned-interests-table tr.d-none").first();
                    $target.toggleClass('visible').toggleClass('d-none');
                });
            });
        </script>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card m-2" id="address-residence">
    <h6 class="card-header"><?= __('Trvalá Adresa / Sídlo Právnické Osoby') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_STREET); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_CITY); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_ZIP); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::RESIDENCE_ADDRESS_CITY_PART); ?>
            </div>
        </div>
        <div class="row">
            <?php
            if (!boolval($identityForm->getData(Identity::IS_PO))) {
                foreach (Identity::ALL_VERIFIED_INFO_FO_ADDRESS as $verifiedInfoFieldId) {
                    $verifiedValue = $identityForm->getData($verifiedInfoFieldId);
                    $label = Identity::getFieldLabel($verifiedInfoFieldId);
                    if (!empty($verifiedValue)) : ?>
                        <div class="col-md-6">
                            <?= $this->Form->control($verifiedInfoFieldId, ['disabled' => 'disabled', 'value' => $verifiedValue, 'label' => $label, 'type' => 'text']) ?>
                        </div>
            <?php
                    endif;
                }
            }
            ?>
        </div>
        <?= $this->Form->control('has-postal', ['type' => 'checkbox', 'label' => __('Zadat jinou doručovací adresu')]) ?>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card m-2" id="address-postal">
    <h6 class="card-header"><?= __('Doručovací Adresa') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_STREET); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_CITY); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_ZIP); ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::POSTAL_ADDRESS_CITY_PART); ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card m-2" id="contact">
    <h6 class="card-header"><?= __('Kontaktní informace') ?></h6>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_PHONE) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_EMAIL, ['default' => $this->getUser('email')]) ?>
                <strong><?= __('Na této e-mailové adrese vás budeme kontaktovat s oznámeními ohledně podaných žádostí o dotaci') ?></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_DATABOX) ?>
            </div>
            <div class="col-md pt-4">
                <?= $this->Html->link(__('Získat ověřené informace přihlášením do datové schránky'), ['_name' => 'isds_auth'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_EMAIL2) ?>
            </div>
            <div class="col-md">
                <?= $identityForm->control(Identity::CONTACT_EMAIL3) ?>
            </div>
        </div>
        <div class="row">
            <?php
            foreach (Identity::ALL_VERIFIED_INFO_CONTACTS as $verifiedInfoFieldId) {
                $verifiedValue = $identityForm->getData($verifiedInfoFieldId);
                if (!empty($verifiedValue) && $verifiedInfoFieldId === Identity::ISDS_VERIFIED_DATABOX_TYPE) {
                    $verifiedValue = IsdsComponent::DB_TYPES[intval($verifiedValue)] ?? null;
                    if (is_array($verifiedValue)) {
                        $verifiedValue = $verifiedValue['long'] ?? '';
                    }
                }
                $label = Identity::getFieldLabel($verifiedInfoFieldId);
                if (!empty($verifiedValue)) : ?>
                    <div class="col-md-6">
                        <?= $this->Form->control($verifiedInfoFieldId, ['disabled' => 'disabled', 'value' => $verifiedValue, 'label' => $label, 'type' => 'text']) ?>
                    </div>
            <?php
                endif;
            }
            ?>
        </div>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>
<div class="card m-2" id="vat">
    <h6 class="card-header"><?= __('Plátce DPH') ?></h6>
    <div class="card-text p-2">
        <div class="col">
            <?= $this->Form->control(
                Identity::VAT,
                [
                    'type' => 'checkbox',
                    'label' => Identity::getFieldLabel(Identity::VAT),
                    'checked' => boolval($identityForm->getData(Identity::VAT))
                ]
            ) ?>
        </div>
    </div>
</div>

<div class="card m-2" id="bank">
    <h6 class="card-header"><?= __('Hlavní bankovní spojení (účet, na který bude vyplacena schválená dotace)') ?></h6>
    <div class="card-body">
        <?= $identityForm->control(Identity::BANK_NAME) ?>
        <div class="form-row">
            <div class="col-md-8 col-7 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_NUMBER) ?>
            </div>
            <div class="col-md-4 col-5 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_CODE, ['type' => 'bank_code', 'prepend' => '<div class="input-group-text">/</div>']) ?>
            </div>
            <div class="col-md-4 col-5 m-0 p-0">
                <i><?= __('Nezapomeňte do Příloh (sekce na konci tohoto formuláře) připojit Potvrzení o vlastnictví účtu, pokud ho vyžaduje poskytovatel dotace!') ?></i>
            </div>
        </div>
    </div>
</div>

<div class="card m-2" id="bank2">
    <h6 class="card-header"><?= __('Doplňující bankovní spojení') ?></h6>
    <div class="card-body">
        <?= $identityForm->control(Identity::BANK_NAME_2) ?>
        <div class="form-row">
            <div class="col-md-8 col-7 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_NUMBER_2) ?>
            </div>
            <div class="col-md-4 col-5 m-0 p-0">
                <?= $identityForm->control(Identity::BANK_CODE_2, ['type' => 'bank_code', 'prepend' => '<div class="input-group-text">/</div>']) ?>
            </div>
            <div class="col-md-4 col-5 m-0 p-0">
                <i><?= __('Potvrzení o vlastnictví účtu můžete připojit jako přílohu na konci formuláře.') ?></i>
            </div>
        </div>
    </div>
</div>



<div class="card m-2" id="note">
    <h6 class="card-header"><?= __('Upřesnění informací o žadateli') ?></h6>
    <div class="card-text p-2">
        <div class="col">
            <?= $identityForm->control(Identity::NOTE, ['data-noquilljs' => 'data-noquilljs']) ?>
        </div>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<?php
$attachments_groups = [];
foreach ($identityForm->identity_attachments as $attachment) {
    if ($attachment->identity_attachment_type->order < 1000) {
        $attachments_groups[0][] = $attachment;
    } else {
        $attachments_groups[1][] = $attachment;
    }
}
?>

<div class="card m-2" id='attachments'>
    <h2 class="card-header"><?= __('Přílohy') ?></h2>
    <div class="card-body table-responsive">
        
        <?php $attachments_groups = array_reverse($attachments_groups); ?>
        <?php foreach ($attachments_groups as $type => $attachment_group) : ?>
        
        <?php if ($type == 0): ?>
        <h3><?= __('Přílohy k žádosti') ?></h3>
        <i><?= __('Přílohy, které vložíte do této sekce se automaticky přiloží ke všem žádostem o dotaci, které následně odešlete. Přílohy použité v předchozích žádostech prosím archivujte. Omylem nahrané přílohy archivujte, uložte formulář a poté je můžete smazat.') ?></i>
        <?php else: ?>
        <h3><?= __('Samostatné přílohy') ?></h3>
        <i><?= __('Jiné přílohy, např. Výroční zpráva/Účetní závěrka a další přílohy, které nemají být přímo součástí žádosti o dotaci.') ?></i>    
        <?php endif ?>
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th><?= __('Příloha č.') ?></th>
                    <th><?= __('Název souboru') ?></th>
                    <th><?= __('Popis souboru') ?></th>
                    <th><?= __('Typ přílohy') ?></th>
                    <th><?= __('Velikost') ?></th>
                    <th><?= __('Platnost') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($attachment_group as $attachment) : ?>
                    <tr>
                        <td>
                            <div class="ed"><?= $attachment->file_id ?></div>
                        </td>
                        <td>
                            <div class="ed"><?= h($attachment->file->original_filename) ?>
                                <?php
                                echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['action' => 'identityAttachmentFile', 'id' => $attachment->user_id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]);
                                ?>
                            </div>

                        </td>
                        <td>
                            <div class="ed"><?= $attachment->description ?></div>
                        </td>
                        <td>
                            <div class="ed"><?= $attachment->identity_attachment_type->type_name ?></div>
                        </td>
                        <td>
                            <div class="ed"><?= Number::toReadableSize($attachment->file->filesize) ?></div>
                        </td>
                        <td>
                            <div class="ed">
                                <?php
                                $archive = ($attachment->is_archived == 0) ? '' : 'd-none ';
                                $unarchive = ($attachment->is_archived == 1) ? '' : 'd-none ';
                                echo $this->Html->link(__('Archivovat'), '/user/toggle_file/' . $attachment->file_id, ['data-file-id' => $attachment->file_id, 'class' => $archive . 'arch-toggle-arch arch-toggle btn btn-primary btn-block']);
                                echo $this->Html->link(__('Zrušit archivaci'), '/user/toggle_file/' . $attachment->file_id, ['data-file-id' => $attachment->file_id, 'class' => $unarchive . 'arch-toggle-unarch arch-toggle btn btn-secondary btn-block']);
                                if ($attachment->is_archived) {
                                    echo $this->Html->link(__('Smazat přílohu'), '/user/delete_file/' . $attachment->file_id, ['data-file-id' => $attachment->file_id, 'class' => 'file-delete btn btn-danger btn-block']);
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endforeach; ?>

        <?php
        $this->Form->unlockField('filedata');
        $this->Form->unlockField('file_type');
        $this->Form->unlockField('file_description');
        ?>

        <div class="card m-2" id="attach">
            <div class="card-text p-2">
                <div class="col">
                    <?= $this->Form->control('filedata', ['type' => 'file', 'label' => __('Připojit další přílohu')]); ?>
                    <span class='d-none' id='file-desc'>
                        <?= $this->Form->control('file_description', ['label' => __('Přidejte popis souboru'), 'type' => 'text']) ?>
                    </span>
                    <span class='d-none' id='file-type'>
                        <?= $this->Form->control('file_type', ['label' => 'Typ přílohy', 'type' => 'select', 'options' => $type_names]) ?>
                    </span>
                </div>
            </div>


        </div>

    </div>

</div>

<div class="card-footer">
    <?= $submitButton ?>
</div>
</div>

<?php
echo $this->Form->end();
?>

<script type="text/javascript">
    let $lastResponse = null;
    let $orResponse = null;
    let $feedbackDiv = null;
    let $cityParts = [];
    const $currentValues = <?= json_encode($identityForm->getSelectsCurrentValues()) ?>;
    const $citiesToParts = <?= json_encode($identityForm->getCitiesToPartsMap()) ?>;
    const $aresURL = "<?= mb_substr(Router::url(['_name' => 'ares', 'ico' => '0']), 0, -1) ?>";
    const $ORaresURL = "<?= mb_substr(Router::url(['_name' => 'ares_or', 'ico' => '0']), 0, -1) ?>";
    const poCards = [
        'attachments',
        'note',
        'attach',
        'bank',
        'bank2',
        'address-postal',
        'address-residence',
        'contact',
        'po-basic',
        'ares-loader',
        //'po-interests', #408
        'po-statutory',
        'po-owned-interests',
        'po-another-share-div',
        'vat'
    ];
    const foCards = [
        'attachments',
        'note',
        'attach',
        'bank',
        'bank2',
        'address-postal',
        'address-residence',
        'contact',
        'fo-basic',
        'ares-loader',
        'po-owned-interests',
        'po-another-share-div',
        'vat'
    ];
    const foFields = [
        'fo-degree-before',
        'fo-firstname',
        'fo-surname',
        'fo-degree-after',
        'fo-date-of-birth',
        'fo-vat-id'
    ]
    const poFields = [
        'preload-ico',
        'po-fullname',
        'po-corporate-type',
        'po-business-id',
        'po-vat-id',
        'po-registration-since',
        'po-registration-details'
    ]

    const courthouses = {
        KSBR: 'Krajský soud v Brně',
        KSCB: 'Krajský soud v Českých Budějovicích',
        KSHK: 'Krajský soud v Hradci Králové',
        KSJI: 'Krajský soud v Brně – pobočka v Jihlavě',
        KSKV: 'Krajský soud v Plzni – pobočka v Karlových Varech',
        KSLB: 'Krajský soud v Ústí nad Labem – pobočka v Liberci',
        KSOL: 'Krajský soud v Ostravě – pobočka v Olomouci',
        KSOS: 'Krajský soud v Ostravě',
        KSPA: 'Krajský soud v Hradci Králové – pobočka v Pardubicích',
        KSPH: 'Krajský soud v Praze',
        KSPL: 'Krajský soud v Plzni',
        KSTB: 'Krajský soud v Českých Budějovicích – pobočka v Táboře',
        KSUL: 'Krajský soud v Ústí nad Labem',
        KSZL: 'Krajský soud v Brně – pobočka ve Zlíně',
        MSPH: 'Městský soud v Praze',
        NSCR: 'Nejvyšší soud České republiky',
        VSOL: 'Vrchní soud v Olomouci',
        VSPH: 'Vrchní soud v Praze',
    }

    function preloadData() {
        if (!$lastResponse || !!$lastResponse?.popis || !$lastResponse?.icoId || !$lastResponse?.zaznamy?.length) {
            let $errorMessage = !!$lastResponse?.popis ? $lastResponse?.popis.split('|').join('<br />') : null;
            $feedbackDiv?.removeClass('valid-feedback').addClass('invalid-feedback')
                .html('<?= __('Načtení se nezdařilo') ?>' + ($errorMessage ? '<br/>' + $errorMessage : '')).toggle(true);
            return;
        } else {
            $feedbackDiv?.removeClass('invalid-feedback').html('').toggle(true);
        }

        const $zaznam = {}
        Object.keys($lastResponse?.zaznamy?.[0] ?? {}).forEach((key) => {
            $zaznam[key] = $lastResponse?.zaznamy?.[0][key]
            if (key === 'sidlo') {
                Object.keys($lastResponse?.zaznamy?.[0][key] ?? {}).forEach((k) => {
                    $zaznam[k] = $lastResponse?.zaznamy?.[0][key][k]
                })
            }
        })

        if (!!$zaznam?.soud || !!$zaznam?.urad) {
            $("#po-registration-details").val($zaznam?.soud ?
                (($zaznam?.oddil + " " + $zaznam?.vlozka).trim() + ", " + (courthouses?.[$zaznam?.soud] ?? $zaznam?.soud)) :
                $zaznam?.urad).trigger('change').addClass('is-valid');
        } else if ($zaznam?.ico) {
            $orResponse = null;
            $.ajax({
                url: $ORaresURL + $zaznam?.ico
            }).done(function(data) {
                $orResponse = data;
                $("#po-registration-details").val('').removeClass('is-valid');

                const $zaznam = !!$orResponse?.zaznamy?.[0]?.spisovaZnacka?.length ? $orResponse?.zaznamy?.[0]?.spisovaZnacka?.[0] : {};
                if (!!$zaznam?.soud || !!$zaznam?.urad) {
                    $("#po-registration-details").val($zaznam?.soud ?
                        (($zaznam?.oddil + " " + $zaznam?.vlozka).trim() + ", " + (courthouses?.[$zaznam?.soud] ?? $zaznam?.soud)) :
                        $zaznam?.urad).trigger('change').addClass('is-valid');
                }
            });
        }

        $("#po-business-id").val($zaznam?.ico).addClass('is-valid');
        $("#po-corporate-type").val($zaznam?.pravniForma).trigger('change').addClass('is-valid');
        $("#po-fullname").val($zaznam?.obchodniJmeno).addClass('is-valid');
        $("#po-registration-since").val($zaznam?.datumVzniku).trigger('change').addClass('is-valid');

        $("#address-residence-city").val($zaznam?.kodObce).trigger('change').addClass('is-valid');
        $("#address-residence-street").val(($zaznam?.nazevUlice + " " + $zaznam?.cisloDomovni).trim() + ($zaznam?.cisloOrientacni ? "/" + $zaznam?.cisloOrientacni : "")).addClass('is-valid');
        $("#address-residence-zip").val($zaznam?.psc).addClass('is-valid');

        $("#address-residence-city-part").val($zaznam?.kodCastiObce).trigger('change').addClass('is-valid');
    }

    function loadAresData() {
        let $input = $("#preload-ico");
        let ico = $input.val();
        ico = ico.replace(/\D/g, '');
        if (!ico) {
            $input.addClass('is-invalid').attr('placeholder', '<?= __('Musíte vyplnit IČO') ?>');
            return;
        }
        $input.removeClass('is-invalid');
        $("#aresLoadingModal").modal('show');
        $.ajax({
            url: $aresURL + ico
        }).done(function(data) {
            $lastResponse = data;
            preloadData();
            $("#aresLoadingModal").modal('hide');
        }).fail(function() {
            $lastResponse = null;
            $("#aresLoadingModal").modal('hide');
            if ($feedbackDiv) {
                $feedbackDiv.removeClass('valid-feedback').addClass('invalid-feedback').text('<?= __('Načtení se nezdařilo') ?>').toggle(true);
            }
        });
    }

    function sideloadCityParts($elm, $data = null) {
        try {
            if ($elm.data('select2')) {
                $elm.select2('destroy');
            }
        } catch (ignore) {}
        try {
            $elm.empty();
        } catch (ignore) {}

        let $defaultValue = null;
        if ($elm.prop('id') && $elm.prop('id') in $currentValues) {
            let $rawValue = $currentValues[$elm.prop('id')];
            let $value = parseInt($rawValue);
            if ($value > 0) {
                $defaultValue = $rawValue;
            }
        }

        $data = $data ? $data : $.map($cityParts, function(name, key) {
            if (key === $defaultValue) {
                return {
                    'id': key,
                    'text': name,
                    'selected': true
                };
            }
            return {
                'id': key,
                'text': name
            };
        });
        let $placeholder = [{
            id: '',
            text: '<?= __('Prosím vyberte si z možností'); ?>'
        }];
        $data = $placeholder.concat($data);

        $elm.select2({
            data: $data,
            theme: 'classic',
            allowClear: true,
            placeholder: '<?= __('Prosím vyberte si z možností'); ?>',
            minimumInputLength: 3
        });
    }

    $(document).ready(function() {
        let $preloadIco = $("#preload-ico");
        $preloadIco.removeClass('is-invalid');
        $feedbackDiv = $preloadIco.closest('.form-group').find('.invalid-feedback');
        $feedbackDiv.toggle(false);
        $.get('<?= Router::url(['_name' => 'csu_municipality_parts_list_json']) ?>', function(data) {
            $(".city-parts-sideload").each(function() {
                const $thisElement = $(this);
                $cityParts = data;
                sideloadCityParts($thisElement);
            });
        });
        $("#is-po").change(function() {
            let isPO = $(this).is(':checked')
            let shownCards = isPO ? poCards : foCards;
            let fieldsToEmpty = isPO ? foFields : poFields;
            fieldsToEmpty.forEach(function(id) {
                $('#' + id).val(null).trigger('change')
            })
            $(".fo-hide").toggle(isPO);
            $("form .card").not("#address-postal").each(function() {
                $(this).toggle($.inArray($(this).attr('id'), shownCards) > -1);
            });
            if ($("#po-another-share").is(':checked')) {
                $("#po-owned-interests").toggle(false);
            }
        }).trigger('change');
        $("#ares-load").click(function() {
            loadAresData();
        });
        $("#po-corporate-type").change(function() {
            let showStatutory = false;
            if ($("#is-po").is(':checked')) {
                let thisVal = parseInt($(this).val());
                if (thisVal < 100 || thisVal > 110) {
                    showStatutory = true;
                }
            }
            $("#po-statutory").toggle(showStatutory);
            /* $("#po-interests").toggle(showStatutory); #408 */ 
        }).change();
        $("#has-postal").change(function() {
            $("#address-postal").toggle($(this).is(':checked'));
        }).trigger('change');
        $preloadIco.keypress(function(e) {
            if (e.which === 13) {
                e.preventDefault();
                loadAresData();
            }
        });
        $("#address-residence-city, #address-postal-city").change(function() {
            const $forCity = parseInt($(this).val());

            const $links = $citiesToParts.filter(function(link) {
                return link['csu_municipalities_number'] === $forCity;
            }).filter(function() {
                return true;
            });

            let $matchingParts = [];
            $.each($links, function($index, $link) {
                const $key = $link['csu_municipality_parts_number'];
                if ($key in $cityParts) {
                    $matchingParts[$key] = $cityParts[$key];
                }
            });

            const $target = $(this).attr('id') === 'address-residence-city' ? $("#address-residence-city-part") : $("#address-postal-city-part");
            sideloadCityParts($target, $.map($matchingParts, function(name, key) {
                if (name !== undefined) {
                    return {
                        'id': key,
                        'text': name
                    };
                }
            }));
        });

        // if "another share" checked allow to fill next fields
        if ($("#po-another-share").is(':checked')) {
            $("#po-owned-interests").toggle(false);
        }
        // trigger change
        $("#po-another-share").change(function() {
            $("#po-owned-interests").toggle(!$(this).is(':checked'));
        }).trigger('change');

        // show statutory/interests if valid corporate type
        var corpType = $('#po-corporate-type').length ? parseInt($('#po-corporate-type').val()) : null;
        var isValidCorpType = $("#is-po").is(':checked') && corpType && (corpType < 100 || corpType > 110)
        $("#po-statutory").toggle(isValidCorpType);
        /* $("#po-interests").toggle(isValidCorpType);  #408 */

        // error removal if the item is filled
        <?php
        foreach (array_merge(
            Identity::REQUIRED_FIELDS,
            Identity::PO_REQUIRED_FIELDS,
            Identity::FO_REQUIRED_FIELDS,
            Identity::ALL_STATUTORY,
            [Identity::CONTACT_DATABOX, Identity::PO_REGISTRATION_DETAILS]
        ) as $v) {
            echo "$(\"#" . str_replace(['.', '_'], '-', $v)  . "\").keyup(function() {
                $(this).val().length > 0 ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
            }); ";
        }
        ?>
            ['#fo-date-of-birth', '#address-residence-city', '#po-registration-since'].forEach(item => {
                $(item).change(function() {
                    $(this).val().length > 0 ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
                });
            });
        ['<?= implode("','", array_merge(Identity::FULL_STATUTORY_TEMPLATE, Identity::FULL_INTEREST_TEMPLATE, Identity::FULL_OWNED_INTERESTS_TEMPLATE)) ?>'].forEach(item => {
            item = '#' + item.replace(/[\._]/g, '-');
            for (var i = 0; i < 10; i++) {
                ['#interest-%d-date-of-birth', '#interest-%d-country', '#owned-%d-country'].includes(item) ?
                    $(item.replace(/%d/g, i)).change(function() {
                        $(this).val().length > 0 ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
                    }) :
                    $(item.replace(/%d/g, i)).keyup(function() {
                        $(this).val().length > 0 ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
                    });
            }
        });
        $("#contact-email").keyup(function() {
            $(this).val().length > 0 && $(this).val().match(/\S+@\S+\.\S+/) ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
        });
        $('#filedata').change(function() {
            if ($(this).val() != '') {
                $('#file-desc').removeClass('d-none');
                $('#file-type').removeClass('d-none');
            }
        });
    })

    $("a.arch-toggle").click(function(e) {
        e.preventDefault();
        let $fileId = $(this).attr('data-file-id');
        if ($(this).hasClass('arch-toggle-arch')) {
            $(this).next().removeClass('d-none');
            $(this).addClass('d-none');
        } else {
            $(this).prev().removeClass('d-none');
            $(this).addClass('d-none');
        }
        $.get("/user/toggle_file/" + $fileId, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });

    $("a.file-delete").click(function(e) {
        e.preventDefault();
        var result = confirm('Opravdu chcete smazat tuto přílohu?');
        if (result) {
            let $fileId = $(this).attr('data-file-id');
            let $currentElement = $(this); // Cache a reference to $(this)
            $.get("/user/delete_file/" + $fileId, function(data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
            });
            $currentElement.closest('tr').remove(); // Use the cached reference // Remove the closest table row
        }
    });
</script>

<div id="aresLoadingModal" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __('Vyčkejte prosím') ?></h5>
            </div>
            <div class="modal-body">
                <p><?= __('Načtení dat z ARES typicky trvá jen několik vteřin') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __('Zrušit') ?></button>
            </div>
        </div>
    </div>
</div>