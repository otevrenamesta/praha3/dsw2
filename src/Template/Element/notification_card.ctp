<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestNotification;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Routing\Router;

/**
 * @var $this AppView
 * @var $notification RequestNotification
 * @var $request Request
 */

if (empty($notification) || empty($request)) {
    echo sprintf('<!-- notification:%d request:%d -->', intval(empty($notification)), intval(empty($request)));
    return;
}

?>

<div class="card m-2">
    <div class="card-header">
        <?= __('Ohlášení') . ' (' . __('odesláno') . ' ' . $notification->created->nice() . ')' ?>
    </div>
    <div class="card-body">
        <table class="table">
            <tr>
                <td><?= __('Typ ohlášení') ?></td>
                <td><?= __($notification->request_notification_type->type_name) ?></td>
            </tr>
            <tr>
                <td><?= __('Termín') ?></td>
                <td>
                    <?php
                    $from = empty($notification->date_from) ? '' : sprintf("%s - ", $notification->date_from->nice());
                    echo $from . $notification->date_end->nice();
                    ?>
                </td>
            </tr>
            <tr>
                <td><?= __('Místo konání') ?></td>
                <td><?= preg_replace('/\n/', '<br/>', $notification->location) ?></td>
            </tr>
            <tr>
                <td><?= __('Garant akce') ?></td>
                <td>
                    <?= sprintf("%s (tel: %s, e-mail: %s)", $notification->garant_name, $notification->garant_phone, $notification->garant_email) ?>
                </td>
            </tr>
            <?php if (!empty($notification->costs_czk)): ?>
                <tr>
                    <td><?= __('Částka čerpaná z dotace') ?></td>
                    <td><?= Number::currency($notification->costs_czk, 'CZK') ?></td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($notification->participants_count)): ?>
                <tr>
                    <td><?= __('Počet účastníků (předpokládaný)') ?></td>
                    <td><?= $notification->participants_count ?></td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($notification->comment)): ?>
                <tr>
                    <td><?= __('Poznámka k ohlášení') ?></td>
                    <td><?= preg_replace('/\n/', '<br/>', $notification->comment) ?></td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($notification->files)): ?>
                <tr>
                    <td><?= __('Přílohy') ?></td>
                    <td>
                        <?php
                        foreach ($notification->files as $notification_attachment) {
                            $route = '#unknownRoute';
                            $teamDownloadRoute = ['action' => 'downloadAttachment', 'request_id' => $notification->request_id, 'file_id' => $notification_attachment->id];
                            $userDownloadRoute = ['action' => 'fileDownload', 'id' => $notification->request_id, 'file_id' => $notification_attachment->id];
                            if (Router::routeExists($teamDownloadRoute)) {
                                $route = $teamDownloadRoute;
                            } elseif (Router::routeExists($userDownloadRoute)) {
                                $route = $userDownloadRoute;
                            }
                            echo $this->Html->link($notification_attachment->getExtendedDescription(), $route);
                            echo '<br/>';
                        }
                        ?>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
    </div>
</div>
