<?php

/**
 * @var $this AppView
 * @var $bool bool
 * @var $warning bool
 */

use App\View\AppView;

$class = isset($warning) && !empty($warning)
  ? 'fa-exclamation-triangle text-warning'
  : (!empty($bool) ? 'fa-check text-success' : 'fa-times text-danger');

?>
<i class="fas <?= $class ?>"></i>
