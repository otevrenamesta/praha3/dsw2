<?php
/**
 * @var $this AppView
 * @var $request Request
 **/

use App\Model\Entity\Request;
use App\View\AppView;

if (empty($request) || !($request instanceof Request) || !$request->hasNotifications()) {
    return;
}

?>
<div class="card m-2">
    <h2 class="card-header">
        <?= __('Ohlášení k této žádosti') ?>
    </h2>
    <div class="card-body">
        <?php
        foreach ($request->request_notifications as $notification) {
            echo $this->element('notification_card', compact('request', 'notification'));
        }
        ?>
    </div>
</div>
