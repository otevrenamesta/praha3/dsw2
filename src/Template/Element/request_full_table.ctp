<?php

use App\Budget\ProjectBudgetFactory;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestType;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\ORM\TableRegistry;
use CakePdf\View\PdfView;

/**
 * @var $this AppView
 * @var $request Request
 * @var RequestsTable $Requests
 */

if (empty($request) || !($request instanceof Request)) {
    return;
}

$appView = $this instanceof AppView ? $this : new AppView($this->getRequest(), $this->getResponse());

// merged or joined requests
$requestsTable = TableRegistry::getTableLocator()->get('Requests');
$mergedRequest = $request->merged_request > 0 ?  $requestsTable->get($request->merged_request) : null;
$joinedRequest = $requestsTable->find(
    'all',
    [
        'conditions' => [
            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            'Requests.merged_request' => $request->id,
        ],
    ]
)->first();

?>

<?php if ($request->request_type_id === RequestType::PAPER_REQUEST) : ?>
    <div class="alert alert-warning">
        <?= __('Tato žádost byla podána bez registrace žadatele v dotačním portále, takže chybí například všechny přílohy nebo některé z formulářů vyžadovaných v daném dotačním programu.') ?>
        <br />
        <?= __('Více informací vám poskytne příslušný zaměstnanec dotačního úřadu zodpovědný za evidenci této žádosti') ?>
        <?php
        if (!empty($request->reference_number)) {
        ?>
            <br />
            <?= sprintf("%s: %s", __('Žádost je v evidenci uložena pod tímto identifikátorem'), $request->reference_number) ?>
        <?php
        }
        ?>
    </div>
<?php endif; ?>

<?php if (!($this instanceof AppView) && $appView->getSiteSetting(OrganizationSetting::EXPORT_SHOW_PDF_HEADER)) : ?>
    <table class="pdf-header">
        <tr>
            <td class="pdf-header-image">
                <?= $appView->Html->image($appView->getSiteLogo() ?: 'default.png') ?>
            </td>
            <td class="pdf-header-address">
                <?= $appView->getSiteSetting(OrganizationSetting::EXPORT_ADDRESS_FOR_PDF_HEADER)
                    ? $appView->getSiteSetting(OrganizationSetting::EXPORT_ADDRESS_FOR_PDF_HEADER)
                    : ''
                ?>
            </td>
        </tr>
        <?= $appView->getSiteSetting(OrganizationSetting::EXPORT_MESSAGE_FOR_PDF_HEADER)
            ? '<tr><td class="pdf-header-message" colspan="2">' .
            $appView->getSiteSetting(OrganizationSetting::EXPORT_MESSAGE_FOR_PDF_HEADER) .
            '</td></tr>'
            : ''
        ?>
    </table>
<?php endif; ?>

<div class="card mt-2">
    <h4 class="card-header"><?= __('Žádost') ?></h4>
    <div class="card-body">
        <table class="table table-striped">
            <tr>
                <td class='w-15'><strong><?= __('Název projektu') ?></strong></td>
                <td class='w-35'><?= $request->name ?></td>
                <td class='w-15'><strong><?= __('Identifikační číslo žádosti') ?></strong></td>
                <td class='w-35'><?= $request->id ?></td>
            </tr>
            <tr>
                <td><strong><?= __('Dotační program') ?></strong></td>
                <td><?= $request->program->name ?></td>
                <td><strong><?= __('Oblast podpory') ?></strong></td>
                <td><?= ($request->program->realm ?? $request->program->parent_program->realm)->name ?></td>
            </tr>
            <tr>
                <td><strong><?= __('Nadřízený dotační program') ?></strong></td>
                <td><?= $request->program->parent_program ? $request->program->parent_program->name : '' ?></td>
                <td><strong><?= __('Název dotační výzvy') ?></strong></td>
                <td><?= $request->appeal->name ?></td>
            </tr>
            <?php
            if ($mergedRequest && $mergedRequest->id > 0) {
                echo '<tr>
                <td><strong>' . __('Tato žádost navazuje na žádost č.') . ' ' . $mergedRequest->id . '</strong></td>
                <td>' . $mergedRequest->name . '</td>
            </tr>';
            } else  if ($joinedRequest && $joinedRequest->id > 0) {
                echo '<tr>
                <td><strong>' . __('Na tuto žádost navazuje žádost č.') . ' ' . $joinedRequest->id . '</strong></td>
                <td>' . $joinedRequest->name . '</td>
            </tr>';
            }
            ?>
        </table>
    </div>
</div>

<?php
if (
    !$this instanceof PdfView &&
    ($appView->getCurrentUser()->hasAtLeastOneTeam() || $appView->getCurrentUser()->isManager() || $appView->getCurrentUser()->isEconomicsManager())
) : ?>
    <div class="card mt-2">
        <h5 class="card-header">
            <?= __('Úřední informace') ?>
            <?= !$appView->getCurrentUser()->hasFormalControlTeam() ? '' : '<span class="d-print-none" data-toggle="tooltip" data-placement="top" title="' .
                __('Úřední informace se nastavují v Sekci pro úředníky') . '"><i class="far fa-question-circle help-icon"></i></span>' ?>
        </h5>
        <div class="card-body">
            <table class="table table-striped">
                <tr>
                    <td class='w-15'><strong><?= __('Vlastní identifikátor') ?></strong></td>
                    <td class='w-35'><?= $request->reference_number ?></td>
                    <td class='w-15'><strong><?= __('Veřejnosprávní kontrola') ?></strong></td>
                    <td class='w-35'><?= boolval($request->public_control) ? __('Ano') : __('Ne') ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Podpora de minimis') ?></strong></td>
                    <td><?= $request->de_minimis === null ? '' : (boolval($request->de_minimis) ? __('Ano') : __('Ne')) ?></td>
                    <td><strong><?= __('Kontrola v pořádku') ?></strong></td>
                    <td><?= $request->public_control_ok === null ? '' : (boolval($request->public_control_ok) ? __('Ano') : __('Ne')) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Poznámka k de minimis') ?></strong></td>
                    <td><?= sprintf("%s", $request->de_minimis_comment) ?></td>
                    <td><strong><?= __('Poznámka ke kontrole') ?></strong></td>
                    <td><?= sprintf("%s", $request->public_control_comment) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __(' Poznámka k žádosti') ?></strong></td>
                    <td colspan="3"><?= sprintf("%s", $request->request_comment) ?></td>
                </tr>
            </table>
        </div>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<div class="card mt-2">
    <h5 class="card-header"><?= __('Žadatel') ?></h5>
    <div class="card-body">
        <?= $this->element('identity_full_table', ['user' => $request->user, 'request' => $request]) ?>
    </div>
</div>

<div class="card mt-2 budget-start">
    <h5 class="card-header"><?= __('Rozpočet projektu') ?></h5>
    <div class="card-body">
        <?php
        $design_id = $request->program->project_budget_design_id;
        // Usti wants to change budget design is for V11 and V12
        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V11) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V11_CHANGE;
        }

        if ($design_id == ProjectBudgetDesign::DESIGN_USTI_V12) {
            $design_id = ProjectBudgetDesign::DESIGN_USTI_V12_CHANGE;
        }

        ?>
        <?= ProjectBudgetFactory::getBudgetHandler($design_id)->renderFullTable($this, $request) ?>
    </div>
</div>

<?php if (
    !in_array($request->request_type_id, [RequestType::PAPER_REQUEST]) &&
    !OrganizationSetting::getSetting(OrganizationSetting::SUBSIDY_HISTORY_NOT_REQUESTED)
) : ?>

    <div class="card mt-2">
        <h5 class="card-header"><?= __('Historie přijaté veřejné podpory') ?></h5>
        <div class="card-body">
            <?php $sorted = [];
            $orgYears = max(0, intval(OrganizationSetting::getSetting(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true)));
            $startYear = intval(date('Y', strtotime('-' . $orgYears . ' years')));

            // requests
            $final_history = array();
            $allowedStates = [
                RequestState::STATE_CLOSED_FINISHED,
                RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                RequestState::STATE_SETTLEMENT_SUBMITTED,
                RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
            ];

            $requests_table = TableRegistry::getTableLocator()->get('Requests');
            $requests = $requests_table->find(
                'all',
                [
                    'conditions' => [
                        'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'Requests.user_id' => $request->user->id,
                        'Requests.request_state_id IN (' . implode(',', $allowedStates) . ')',
                    ],
                    'contain' => ['RequestLogs'],
                ]
            )->order(['created' => 'DESC'])
                ->toArray();

            foreach ($requests as $k => $v) {
                if ($v['final_subsidy_amount'] <= 0) {
                    continue;
                }

                $validLogs = array_filter($v->request_logs, function ($val) {
                    return (int)$val['request_state_id'] === RequestState::STATE_PAID_READY_FOR_SETTLEMENT;
                });
                $lastLog = count($validLogs) > 0 ? reset($validLogs) : [];
                $logDate = !empty($lastLog) && isset($lastLog['created']) ? substr($lastLog['created']->year, 0, 4) : '';

                $final_history[] = (object)
                [
                    'added_by' => __('systém'),
                    'amount_czk' => $v['final_subsidy_amount'],
                    'fiscal_year' => !empty($logDate) ? $logDate : substr($v['created']->year, 0, 4),
                    'id' => null,
                    'project_name' => $v['name'],
                    'public_income_source_id' => null,
                    'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
                ];
            }

            // histories
            $histories = array();

            if (OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES)) {

                $identities_table = TableRegistry::getTableLocator()->get('Identities');
                $identity = $identities_table->find('all', [
                    'conditions' => [
                        'name IN' => [Identity::FO_VAT_ID, Identity::PO_BUSINESS_ID, Identity::PO_VAT_ID],
                        'user_id' => $request->user->id,
                        'value IS NOT' => NULL,
                        'value !=' => ''
                    ]
                ])->toArray();

                $userIdentities = array_fill_keys([
                    'HistoryIdentities.ico IN',
                    'HistoryIdentities.dic IN'
                ], []);

                foreach ($identity as $k => $v) {
                    if (empty(trim($v['value']))) {
                        continue;
                    }
                    switch ($v['name']) {
                        case Identity::PO_BUSINESS_ID:
                            $userIdentities['HistoryIdentities.ico IN'][] = $v['value'];
                            break;

                        case Identity::FO_VAT_ID:
                        case Identity::PO_VAT_ID:
                            $userIdentities['HistoryIdentities.dic IN'][] = $v['value'];
                            break;
                    }
                }

                if (!empty(array_filter($userIdentities))) {
                    $history_identities_table = TableRegistry::getTableLocator()->get('HistoryIdentities');
                    $archive = $history_identities_table->find('all', [
                        'conditions' => [
                            'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                            'OR' => array_filter($userIdentities)
                        ],
                        'contain' => [
                            'Histories',
                        ],
                    ])->toArray();

                    if (isset($archive[0]) && isset($archive[0]->histories)) {
                        foreach ($archive[0]->histories as $v) {
                            $histories[] = (object)
                            [
                                'id' => null,
                                'added_by' => __('úředník'),
                                'amount_czk' => $v['czk_amount'],
                                'fiscal_year' => $v['year'],
                                'public_income_source_id' => null,
                                'public_income_source' => (object)['source_name' => OrgDomainsMiddleware::getCurrentOrganization()->name],
                                'project_name' => $v['project_name']
                            ];
                        }
                    }
                }
            }

            $final_history = array_merge($final_history, $request->user->public_income_histories, $histories);

            foreach ($final_history as $history) {
                if ($history->fiscal_year >= $startYear) {
                    if (!isset($sorted[$history->fiscal_year])) {
                        $sorted[$history->fiscal_year] = [];
                    }
                    $source_name = $history->public_income_source->source_name;
                    $source_name .= (isset($history->public_income_source_name) && !empty($history->public_income_source_name) ? ' (' . h($history->public_income_source_name) . ')' : '');

                    if (!isset($sorted[$history->fiscal_year][$source_name])) {
                        $sorted[$history->fiscal_year][$source_name] = [];
                    }
                    $sorted[$history->fiscal_year][$source_name][] = [$history->project_name, $history->amount_czk, isset($history->added_by) ? $history->added_by : __('uživatel')];
                }
            }
            krsort($sorted);
            ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= __('Rok') ?></th>
                        <th><?= __('Poskytovatel') ?></th>
                        <th><?= __('Popis projektu') ?></th>
                        <th class="text-right"><?= __('Získaná podpora') ?></th>
                        <th><?= __('Vložil') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sorted as $year => $sources) : ?>
                        <?php foreach ($sources as $sourcename => $sourceitems) : ?>
                            <?php foreach ($sourceitems as $data) : ?>
                                <tr>
                                    <td><?= $year ?></td>
                                    <td><?= $sourcename ?></td>
                                    <td><?= $data[0] ?></td>
                                    <td class="text-right" data-type="currency"><?= Number::currency($data[1], 'CZK') ?></td>
                                    <td><?= (!empty($data[2]) ? $data[2] : __('úředník')) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

<?php endif; ?>

<?php foreach ($request->getForms() as $form) :
    $requestForm = $form->getFormController($request);
    $requestForm->setUser($request->user);
?>
    <div class="card mt-2">
        <h4 class="card-header">
            <?= $requestForm->getFormNameDisplayed() ?>
        </h4>
        <div class="card-body">
            <?= $requestForm->render($appView, ['readOnly' => true]) ?>
        </div>
    </div>
<?php endforeach; ?>

<?php
$attachments = array_filter($request->files, function ($v) {
    return empty($v['file_type']);
});
if (!empty($attachments) && $this instanceof AppView) : ?>
    <div class="card mt-2 d-print-none">
        <h5 class="card-header"><?= __('Nepovinné přílohy') ?></h5>
        <div class="card-body">
            <ul class="list-group">
                <?php
                foreach ($attachments as $file) : ?>
                    <li class="list-group-item">
                        <?php
                        echo $this->Html->link($file->getExtendedDescription(), ['action' => 'downloadAttachment', 'controller' => 'Teams', 'request_id' => $request->id, 'file_id' => $file->id], ['target' => '_blank']);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>

<?php if ($appView->getSiteSetting(OrganizationSetting::MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS) && $this instanceof PdfView) : ?>
    <?= $this->element('pdf_signatures_table') ?>
<?php endif; ?>