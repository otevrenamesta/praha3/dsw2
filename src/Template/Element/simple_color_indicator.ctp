<?php

/**
 * @var $this AppView
 * @var $bool bool|int
 * @var $tooltip int
 * @var $link string
 */

use App\View\AppView;

$STATUS_OK = 1;
$STATUS_NOK = 2;
$STATUS_WARNING = 3;

$status = $STATUS_OK;

if (!isset($link)) {
    $link = null;
}

if (is_bool($bool)) {
    $status = $bool ? $STATUS_OK : $STATUS_NOK;
}
if (is_int($bool)) {
    $status = intval($bool);
}
$tooltipText = [];
$color = null;
switch ($status) {
    case $STATUS_OK:
        $color = 'success'; // green
        $tooltipText = [__('Ohlášení k této žádosti bylo uskutečněno'), __('Vyúčtováno a schváleno')];
        break;
    case $STATUS_NOK:
        $color = 'danger'; // red
        $tooltipText = [__('Ohlášení k této žádosti chybí'), __('Chybí platba k této žádosti, nebo není schváleno její vyúčtování')];
        break;
    default:
    case $STATUS_WARNING:
        $color = 'warning'; // yellow (orange)
        $tooltipText = [__('Ohlášení k této žádosti - stav neznámý'), __('Existuje platba k této žádosti, ale vyúčtování chybí')];
        break;
}
?>

<span class="d-none"><?= $status ?></span>
<?php if ($link) : ?>
    <a href="<?= $link ?>">
    <?php endif; ?>
    <button type="button" class="btn pr-2 btn-<?= $color ?>" data-toggle="tooltip" data-placement="left" title="<?= $tooltipText[$tooltip] ?>">
        &nbsp;
    </button>
    <?php if ($link) : ?>
    </a>
<?php endif; ?>

<script type="text/javascript">
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>