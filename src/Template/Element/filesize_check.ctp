<?php

use Cake\I18n\Number;

?>
<input type="hidden" name="MAX_FILE_SIZE" value="<?= getMaximumFileUploadSize() ?>">

<script type="text/javascript">
    $(document).ready(function() {
        var totalsize = 0;
        $("input[type=hidden][name=MAX_FILE_SIZE]").each(function() {
            var filesize = $(this).val();
            $("input[type=file]").bind('change', function() {
                if (this.files.length > 0 && (this.files[0].size + totalsize) > filesize) {
                    alert(totalsize > 0 ?
                        '<?= __('Celková velikost současně vkládaných souborů je větší než maximální povolená velikost') . ' ' . Number::toReadableSize(getMaximumFileUploadSize()) ?>' :
                        '<?= __('Soubor je větší než maximální povolená velikost') . ' ' . Number::toReadableSize(getMaximumFileUploadSize()) ?>');
                    $(this).val("");
                    // set original-filename as label of custom-file-field or empty if attribute not provided
                    $(this).siblings('label').empty().html($(this).attr('original-filename'));
                } else if (this.files.length > 0 && this.files[0].size > 0) {
                    totalsize += this.files[0].size;
                }
            });
        });
    });
</script>
