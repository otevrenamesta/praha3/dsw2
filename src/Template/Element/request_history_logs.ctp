<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */

$request->loadLogs();
$history_logs = empty($request->request_logs)
  ? []
  : array_filter($request->request_logs, function ($request_log) {
    return $request_log->is_history >= 1 && $request_log->user && $request_log->user->email;
  });

if (empty($history_logs)) {
  return;
}
?>

<div class="card mt-2 d-print-none">
  <h5 class="card-header">
    <?= __('Historie práce s žádostí') ?>
  </h5>
  <div class="p-2 pl-3" id="history_logs_dialog">
    <button id="history_logs_button" type="button" class="btn btn-secondary"><?= __("Otevřít"); ?></button>
  </div>
  <div id="history_logs_table" class="card-body table-responsive" style="display:none;">
    <table class="table table-striped">
      <thead>
        <tr>
          <th><?= __('ID') ?></th>
          <th><?= __('Datum a čas otevření') ?></th>
          <th><?= __('Uživatel, který žádost otevřel') ?></th>
          <th><?= __('Stav žádosti') ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($history_logs as $request_log) : ?>
          <tr>
            <td><?= $request_log->id ?></td>
            <td><?= $request_log->created->nice() ?></td>
            <td><?= $request_log->user->email ?></td>
            <td><?= RequestState::getLabelByStateId($request_log->request_state_id) ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("#history_logs_button").click(function() {
      $("#history_logs_dialog").hide();
      $("#history_logs_table").show();
    })
  })
</script>