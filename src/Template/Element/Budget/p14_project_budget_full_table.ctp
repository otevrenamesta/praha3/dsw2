<?php

use App\Budget\P14ProjectBudget;
use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;
use CakePdf\View\PdfView;

/**
 * @var $this AppView|PdfView
 * @var $request Request
 */


if (empty($request) || !($request instanceof Request) || empty($request->request_budget)) {
    return;
}

$budgetHandler = new P14ProjectBudget();
$budgetHandler->loadBudget($request->request_budget);
?>

<table class="table table-striped">
    <tbody>
        <tr>
            <td><?= __('Požadovaná výše dotace') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->requested_amount, 'CZK') ?></td>
        </tr>
        <tr>
            <td><?= __('Celkové náklady/výdaje na projekt') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->total_costs, 'CZK') ?></td>
        </tr>
        <tr>
            <td><?= __('Součet financí z jiných zdrojů') ?></td>
            <td class="text-right"><?= Number::currency($request->request_budget->total_own_sources, 'CZK') ?></td>
        </tr>
</table>


<?php foreach ($budgetHandler->getSections() as $sectionId => $sectionTitle) : ?>
    <?php
    $sectionFilledRows = $budgetHandler->getSectionFilledRows($sectionId);
    ?>
    <h5><?= $sectionTitle ?></h5>
    <table class="table table-bordered" id="section-<?= $sectionId ?>">
        <colgroup>
            <col class="w-25">
            <col class="w-15">
            <col class="w-15">
            <col class="w-25">
            <col class="w-20">
        </colgroup>
        <thead>
            <tr>
                <?php foreach ($budgetHandler->getSectionHeaders($sectionId) as $columnHeader) : ?>
                    <th><?= $columnHeader ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php for ($sectionRowCounter = 0; $sectionRowCounter < $budgetHandler->getSectionMaxRows($sectionId); $sectionRowCounter++) : ?>
                <tr class="<?= $sectionRowCounter >= $sectionFilledRows ? 'd-none' : 'visible' ?>">
                    <?php foreach ($budgetHandler->getSectionColumns($sectionId) as $columnId => $columnExtra) : ?>
                        <td>
                            <?= $budgetHandler->getData(sprintf('%d.%d.%d', $sectionId, $sectionRowCounter, $columnId)) ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endfor; ?>
        </tbody>
        <tfoot>
            <tr class="thead-dark">
                <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes) : ?>
                    <th <?php foreach ($attributes as $name => $value) {
                            echo sprintf('%s="%s"', $name, $value);
                        } ?>><?= $attributes['text'] ?? '' ?></th>
                <?php endforeach; ?>
            </tr>
        </tfoot>
    </table>
<?php endforeach; ?>