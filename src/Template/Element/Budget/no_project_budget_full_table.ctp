<?php

use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;
use CakePdf\View\PdfView;

/**
 * @var $this AppView|PdfView
 * @var $request Request
 */

if (empty($request) || !($request instanceof Request) || empty($request->request_budget)) {
    return;
}
?>

<table class="table table-striped">
    <tbody>
    <tr>
        <td><?= __('Požadovaná výše dotace') ?></td>
        <td class="text-right"><?= Number::currency($request->request_budget->requested_amount, 'CZK') ?></td>
    </tr>
   </tbody>
</table>
