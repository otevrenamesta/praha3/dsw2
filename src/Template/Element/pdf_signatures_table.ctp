<div class="card mt-2">
    <h4 class="card-header">
        <?= __('Podpis osob oprávněných zastupovat žadatele') ?>
    </h4>
    <div class="card-body">
        <table class="table table-bordered">
            <colgroup>
                <col class="w-50">
                <col class="w-50">
            </colgroup>
            <tbody>
            <tr>
                <td>
                    <?= __('Jména a příjmení osob oprávněné zastupovat žadatele') ?>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
                <td>
                    <?= __('Podpis(y)') ?>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <?= __('V _______________________ dne _____________') ?>
                </td>
                <td>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <?= __('Razítko') ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>