<?php
/**
 * @var $this AppView
 * @var $file File
 * @var $field FormField
 * @var $showWarning bool
 * @var $link string|null
 */

use App\Model\Entity\File;
use App\Model\Entity\FormField;
use App\View\AppView;
use Cake\I18n\Number;

if (empty($file) || !($file instanceof File)) {
    return;
}
if (empty($field)) {
    $field = null;
}
if (!isset($showWarning)) {
    $showWarning = true;
}
?>
<div class="alert alert-success">
    <?php if ($field instanceof FormField): ?>
        <strong><?= $field->name ?></strong> <br/>
    <?php endif; ?>

    <?= __('Nahraný soubor') ?>: <?= h($file->original_filename) ?>, <?= __('velikost') ?>
    : <?= Number::toReadableSize($file->filesize) ?><br/>

    <?php if ($showWarning): ?>
        <strong><?= __('Pokud vyberete nový soubor, původní bude vymazán') ?></strong>
    <?php endif; ?>

    <?php
    if (!empty($link)) {
        echo $this->Html->link(__('Stáhnout soubor'), $link);
    }
    ?>
</div>
