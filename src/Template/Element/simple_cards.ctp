<?php
/**
 * @var $this AppView
 * @var $cards array
 */

use App\View\AppView;

if (!isset($cards) || !is_array($cards)) {
    return;
}
?>
<div class="row no-gutters">
    <?php foreach ($cards as $card): ?>
        <div class="d-flex col-lg-4 col-sm-6 mt-2 pl-2">
            <div class="card flex-fill">
                <div class="card-header">
                    <?= $card['header'] ?? '' ?>
                </div>
                <div class="card-body">
                    <p class="card-text">
                        <?= $card['text'] ?? '' ?>
                    </p>
                    <a href="<?= $this->Url->build($card['link']) ?>"
                       class="btn <?= !empty($card['class']) ? $card['class'] : 'btn-primary' ?>"><?= $card['action'] ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>