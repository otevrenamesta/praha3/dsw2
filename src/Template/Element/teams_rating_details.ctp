<?php

use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $criteria EvaluationCriterium[]
 */

if (!isset($request) || !($request instanceof Request)) {
    return;
}

$criteria = $this->indexArrayById($criteria);

?>

<div class="card mt-2">
    <h5 class="card-header"><?= __('Jednotlivá hodnocení') ?></h5>
    <div class="card-body table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th><?= __('Hodnotitel') ?></th>
                <?php foreach ($criteria as $criterium_id => $criterium): ?>
                    <th><?= $criterium->name ?></th>
                <?php endforeach; ?>
                <th><?= __('Celkem') ?></th>
                <?php if ($this->getSiteSetting(OrganizationSetting::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS, true) === true): ?>
                    <th><?= __('Komentář') ?></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($request->getSingleRatings() as $user_id => $stats): ?>
                <?php $sum = 0; ?>
                <tr>
                    <td><?= $stats['label'] ?? 'Chybí popis' ?></td>
                    <?php foreach ($criteria as $criterium_id => $criterium): ?>
                        <td>
                            <?php
                            $points = $stats[$criterium_id] ?? -1;
                            if ($points >= 0) {
                                $sum += $points;
                            } else {
                                $points = '';
                            }
                            echo $points;
                            ?>
                        </td>
                    <?php endforeach; ?>
                    <td><?= $sum ?></td>
                    <?php if ($this->getSiteSetting(OrganizationSetting::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS, true) === true): ?>
                        <th><?= $stats['comment'] ?? '' ?></th>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
