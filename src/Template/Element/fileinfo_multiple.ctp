<?php

/**
 * @var $this AppView
 * @var $file File
 * @var $field FormField
 * @var $showWarning bool
 * @var $link string|null
 */

use App\Model\Entity\File;
use App\Model\Entity\FormField;
use App\View\AppView;
use Cake\I18n\Number;

if (empty($files)) {
    return;
}
if (empty($field)) {
    $field = new stdClass;
}
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <strong><?= __('Seznam nahraných příloh:') ?></strong>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <table class="table">
                        <?php foreach ($files as $index => $file) : ?>
                            <tr id='row-file-<?= $file->id ?>'>
                                <td>
                                    <?= $this->Html->link($file->original_filename, ['action' => 'formFileDownload', $request->id, $file->id], ['target' => '_blank']); ?>
                                </td>
                                <td>
                                    <?= Number::toReadableSize($file->filesize) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link(__('Smazat přílohu'), ['action' => 'formFileDelete', $request->id, $file->id, $field->form_id, $field->id], ['class' => 'btn btn-danger mb-2 field-file-multiple-delete', 'id' => 'file-' . $file->id, 'confirm' => __('Opravdu chcete přílohu smazat ?')]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col">
                     <i><?= __('Poznámka: Po nahrání každé jednotlivé přílohy musíte celý formulář uložit a případně se vrátit.') ?></i>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.field-file-multiple-delete').on('click', function(event) {
            event.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                type: 'GET',
                success: function(response) {
                    var rowId = 'row-file-' + response;
                    $('#' + rowId).remove();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("AJAX Error: " + textStatus);
                }
            });
        });
    });
</script>