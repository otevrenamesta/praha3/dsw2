<?php

/**
 * @var $this AppView
 * @var $noscript bool
 */

use App\View\AppView;

$this->Html->css([
    'jquery.dataTables-1.10.20.min.css',
    'buttons.dataTables-1.6.2.min.css'
], ['block' => true]);
$this->Html->script([
    'jquery.dataTables-1.10.20.min.js',
    'moment.min.js',
    'dataTables.dsw2TypeDetect.js',
    'datetime-moment.js',
    'dataTables.buttons-1.6.2.min.js',
    'buttons.html5-1.6.2.min.js',
    'buttons.flash-1.6.2.min.js',
    'buttons.colVis-1.6.2.min.js',
    'buttons.print-1.6.2.min.js',
    'jszip-3.1.3.min.js',
    'pdfmake-0.1.53.min.js',
    'vfs_fonts-0.1.53.js',
], ['block' => true]);

$default_sort_col = 1;
if (in_array($this->name, ['Forms', 'UsersManager'])) {
    $default_sort_col = 2;
};

?>
<?php if (!isset($noscript) || $noscript !== true) : ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var buttonCommon = {
                exportOptions: {
                    format: {
                        body: function(data, row, column, node) {
                            var parsed = false;
                            $("textarea,input", node).not("input[type=radio]").each(function() {
                                data = $(this).val();
                                parsed = true;
                            })
                            $("input[type=radio]:checked", node).each(function() {
                                data = $(this).parent().text();
                                parsed = true;
                            })
                            return (parsed ? data : $(node).text()).trim();
                        }
                    }
                }
            };
            var buttonCsvExcelCommon = {
                exportOptions: {
                    format: {
                        body: function(data, row, column, node) {

                            var parsed = false;
                            $("textarea,input", node).not("input[type=radio]").each(function() {
                                data = $(this).val();
                                parsed = true;
                            })
                            $("input[type=radio]:checked", node).each(function() {
                                data = $(this).parent().text();
                                parsed = true;
                            })
                            if ($(node).data('type') === 'currency') {
                                data = data.replace(/[^\d,.]/g, '').replace(/,/g, '.')
                                parsed = true;
                            }
                            return (parsed ? data : $(node).text()).trim();
                        }
                    }
                }
            };
            var buttonPrintCommon = {
                exportOptions: {
                    format: {
                        body: function(data, row, column, node) {
                            $("textarea,input", node).not("input[type=radio]").each(function() {
                                data = $(this).val();
                            })
                            $("input[type=radio]:checked", node).each(function() {
                                data = $(this).parent().text();
                            })
                            return data;
                        }
                    }
                }
            };
            $.fn.dataTable.moment('DD.MM.YYYY');
            $("#dtable").DataTable({
                dom: "<'border-top pb-2'><'row'<'col'l><'col'B><'col'f>>rtip",
                buttons: [
                    $.extend(true, {}, buttonCommon, {
                        extend: 'copy',
                    }),
                    $.extend(true, {}, buttonCsvExcelCommon, {
                        extend: 'csv',
                    }),
                    $.extend(true, {}, buttonCsvExcelCommon, {
                        extend: 'excel',
                        title: null,
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'pdf',
                    }),
                    $.extend(true, {}, buttonPrintCommon, {
                        extend: 'print',
                    }),
                ],
                pageLength: 200,
                lengthMenu: [20, 50, 100, 150, 200, 250, 300, 500, 1000],
                stateSave: true,
                order: [
                    [<?php echo  $default_sort_col ?>, "asc"]
                ],
                language: {
                    "emptyTable": "Tabulka neobsahuje žádná data",
                    "info": "Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů",
                    "infoEmpty": "Zobrazuji 0 až 0 z 0 záznamů",
                    "infoFiltered": "(filtrováno z celkem _MAX_ záznamů)",
                    "loadingRecords": "Načítám...",
                    "zeroRecords": "Žádné záznamy nebyly nalezeny",
                    "paginate": {
                        "first": "První",
                        "last": "Poslední",
                        "next": "Další",
                        "previous": "Předchozí"
                    },
                    "searchBuilder": {
                        "add": "Přidat podmínku",
                        "clearAll": "Smazat vše",
                        "condition": "Podmínka",
                        "conditions": {
                            "date": {
                                "after": "po",
                                "before": "před",
                                "between": "mezi",
                                "empty": "prázdné",
                                "equals": "rovno",
                                "not": "není",
                                "notBetween": "není mezi",
                                "notEmpty": "není prázdné"
                            },
                            "number": {
                                "between": "mezi",
                                "empty": "prázdné",
                                "equals": "rovno",
                                "gt": "větší",
                                "gte": "rovno a větší",
                                "lt": "menší",
                                "lte": "rovno a menší",
                                "not": "není",
                                "notBetween": "není mezi",
                                "notEmpty": "není prázdné"
                            },
                            "string": {
                                "contains": "obsahuje",
                                "empty": "prázdné",
                                "endsWith": "končí na",
                                "equals": "rovno",
                                "not": "není",
                                "notEmpty": "není prázdné",
                                "startsWith": "začíná na"
                            },
                            "array": {
                                "equals": "rovno",
                                "empty": "prázdné",
                                "contains": "obsahuje",
                                "not": "není",
                                "notEmpty": "není prázdné",
                                "without": "neobsahuje"
                            }
                        },
                        "data": "Sloupec",
                        "logicAnd": "A",
                        "logicOr": "NEBO",
                        "title": {
                            "0": "Rozšířený filtr",
                            "_": "Rozšířený filtr (%d)"
                        },
                        "value": "Hodnota",
                        "button": {
                            "0": "Rozšířený filtr",
                            "_": "Rozšířený filtr (%d)"
                        },
                        "deleteTitle": "Smazat filtrovací pravidlo"
                    },
                    "autoFill": {
                        "cancel": "Zrušit",
                        "fill": "Vyplň všechny buňky textem <i>%d<i><\/i><\/i>",
                        "fillHorizontal": "Vyplň všechny buňky horizontálně",
                        "fillVertical": "Vyplň všechny buňky vertikálně"
                    },
                    "buttons": {
                        "collection": "Kolekce <span class=\"ui-button-icon-primary ui-icon ui-icon-triangle-1-s\"><\/span>",
                        "copy": "Kopírovat",
                        "copySuccess": {
                            "1": "Skopírován 1 řádek do schránky",
                            "_": "SKopírováno %d řádků do schránky"
                        },
                        "copyTitle": "Kopírovat do schránky",
                        "csv": "CSV",
                        "excel": "Excel",
                        "pageLength": {
                            "-1": "Zobrazit všechny řádky",
                            "_": "Zobrazit %d řádků"
                        },
                        "pdf": "PDF",
                        "print": "Tisknout",
                        "colvis": "Viditelnost sloupců",
                        "colvisRestore": "Resetovat sloupce",
                        "copyKeys": "Zmáčkněte ctrl or u2318 + C pro zkopírování dat.  Pro zrušení klikněte na tuto zprávu nebo zmáčkněte esc.."
                    },
                    "searchPanes": {
                        "clearMessage": "Smazat vše",
                        "collapse": {
                            "0": "Vyhledávací Panely",
                            "_": "Vyhledávací Panely (%d)"
                        },
                        "count": "{total}",
                        "countFiltered": "{shown} ({total})",
                        "emptyPanes": "Žádné Vyhledávací Panely",
                        "loadMessage": "Načítám Vyhledávací Panely",
                        "title": "Aktivních filtrů - %d"
                    },
                    "select": {
                        "cells": {
                            "1": "Vybrán 1 záznam",
                            "_": "Vybráno %d záznamů"
                        },
                        "columns": {
                            "1": "Vybrán 1 sloupec",
                            "_": "Vybráno %d sloupců"
                        }
                    },
                    "aria": {
                        "sortAscending": "Aktivujte pro seřazení vzestupně",
                        "sortDescending": "Aktivujte pro seřazení sestupně"
                    },
                    "infoThousands": ",",
                    "lengthMenu": "Zobrazit _MENU_ výsledků",
                    "processing": "Zpracovávání...",
                    "search": "Vyhledávání:",
                    "thousands": ",",
                    "datetime": {
                        "previous": "Předchozí",
                        "next": "Další",
                        "hours": "Hodiny",
                        "minutes": "Minuty",
                        "seconds": "Vteřiny",
                        "unknown": "-",
                        "amPm": [
                            "Dopoledne",
                            "Odpoledne"
                        ]
                    },
                    "editor": {
                        "close": "Zavřít",
                        "create": {
                            "button": "Nový",
                            "title": "Nový záznam",
                            "submit": "Vytvořit"
                        },
                        "edit": {
                            "button": "Změnit",
                            "title": "Změnit záznam"
                        }
                    }
                }
            });
        });
    </script>
<?php endif; ?>
