<?php

use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $settlement Settlement
 */

$settlement->loadLogs();
if (empty($settlement->settlement_logs)) {
    return;
}
?>

<div class="card mt-2">
    <h5 class="card-header">
        <?= __('Historie vyúčtování') ?>
    </h5>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Datum / Čas') ?></th>
                <th><?= __('Stav vyúčtování') ?></th>
                <th><?= __('Kdo provedl změnu') ?></th>
                <th><?= __('Komentář') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($settlement->settlement_logs as $log): ?>
                <tr>
                    <td><?= $log->id ?></td>
                    <td><?= $log->created->nice() ?></td>
                    <td><?= SettlementState::getLabel($log->settlement_state_id) ?></td>
                    <td><?= $log->user ? $log->user->email : ($log->executed_by_user_id === null ? __('Automaticky') : sprintf(__('Uživatel č. %d'), $log->executed_by_user_id)) ?></td>
                    <td><?= $log->economics_statement ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
