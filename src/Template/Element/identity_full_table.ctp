<?php

use App\Model\Entity\Identity;
use App\Model\Entity\Request;
use App\Model\Entity\User;
use App\View\AppView;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\Utility\Hash;
use App\Form\IdentityForm;
use Cake\I18n\Number;
use Cake\Routing\Router;

/**
 * @var $this AppView
 * @var $user User
 * @var $version int
 * @var $request Request
 * @var $brief bool If the table should display only verification pdf necessary info
 * @var $identityDetail bool True if used for /histories/identity_detail
 */

if (empty($user) || !($user instanceof User) || (empty($version) && empty($request->user_identity_version))) {
    return;
}

if (!isset($brief) || !is_bool($brief)) {
    $brief = false;
}

$request->loadIdentities();
$identity_attachments = $request->loadIdentityAttachments();

if (!isset($identityDetail) || !boolval($identityDetail)) {
    foreach ($identity_attachments as $key => $attachment) {
        if ($attachment->identity_attachment_type->order >= 1000) { // Remove attachments with type order > 1000 , see #501
            unset($identity_attachments[$key]);
        }
    }
}

$flattened = [];
foreach ($request->identities as $identity) {
    $flattened[$identity->name] = $identity->value;
}
$nested = Hash::expand($flattened);

$isPo = boolval(Hash::get($nested, Identity::IS_PO, 0));
$poRegistrationInformation = '';
if ($isPo) {
    $registeredWhen = Hash::get($nested, Identity::PO_REGISTRATION_SINCE);
    try {
        $registeredDate = FrozenDate::parseDate($registeredWhen, 'YYYY-MM-dd');
        $registeredWhen = ($registeredDate ? ', ' . $registeredDate->format('d. m. Y') : $registeredWhen);
    } catch (Throwable $e) {
        Log::debug($e);
        $registeredWhen = empty($registeredWhen) ? '' : ', ' . $registeredWhen;
    }
    $registeredWhere = Hash::get($nested, Identity::PO_REGISTRATION_DETAILS);
    $poRegistrationInformation = sprintf("%s%s", trim($registeredWhere), $registeredWhen);
}

$appView = ($this instanceof AppView) ? $this : new AppView($this->getRequest(), $this->getResponse(), $this->getEventManager());

$residenceCity = $appView->getCityName(intval(Hash::get($nested, Identity::RESIDENCE_ADDRESS_CITY, null)));
$residenceCityPart = $appView->getCityPartName(intval(Hash::get($nested, Identity::RESIDENCE_ADDRESS_CITY_PART, null)));
$legalFormName = $appView->getLegalFormName(intval(Hash::get($nested, Identity::PO_CORPORATE_TYPE, null)));

$residenceAddress = sprintf(
    "%s, %s %s, %s",
    Hash::get($nested, Identity::RESIDENCE_ADDRESS_STREET),
    $residenceCity,
    $residenceCityPart,
    Hash::get($nested, Identity::RESIDENCE_ADDRESS_ZIP)
);

$fullName = $fullName = $isPo ?
    Hash::get($nested, Identity::PO_FULLNAME) :
    sprintf(
        "%s %s %s %s",
        Hash::get($nested, Identity::FO_DEGREE_BEFORE),
        Hash::get($nested, Identity::FO_FIRSTNAME),
        Hash::get($nested, Identity::FO_SURNAME),
        Hash::get($nested, Identity::FO_DEGREE_AFTER)
    );
$fullNameVerified = strcmp($fullName, Hash::get($nested, Identity::ISDS_VERIFIED_BUSINES_NAME)) === 0;

$ico = Hash::get($nested, Identity::PO_BUSINESS_ID);
$verifiedIco = Hash::get($nested, Identity::ISDS_VERIFIED_BUSINESS_ID);
$icoIsVerified = !empty($verifiedIco) && strcmp($ico, $verifiedIco) === 0;

$dsId = Hash::get($nested, Identity::CONTACT_DATABOX) ?? Hash::get($nested, Identity::ISDS_VERIFIED_DATABOX_ID);
$dsIdVerified = !empty($dsId) && strcmp($dsId, Hash::get($nested, Identity::ISDS_VERIFIED_DATABOX_ID)) === 0;

$birthDate = Hash::get($nested, Identity::FO_DATE_OF_BIRTH);

?>
<table class="table table-striped">
    <tbody>
        <tr>
            <td><?= __('Celé jméno') ?></td>
            <td>
                <?= $fullName ?>
                <?php if ($fullNameVerified) {
                    echo '(ověřeno pomocí DS)';
                } ?>
            </td>
        </tr>
        <?php if (!$isPo) : ?>
            <tr>
                <td><?= __('Datum narození') ?></td>
                <td>
                    <?= $birthDate ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td><?= $isPo ? __('Sídlo právnické osoby') : __('Trvalá adresa') ?></td>
            <td><?= $residenceAddress ?></td>
        </tr>
        <?php if (!$brief) : ?>
            <tr>
                <td><?= __('Právní forma') ?></td>
                <td><?= $legalFormName ?></td>
            </tr>
        <?php endif; ?>
        <?php if ($isPo) : ?>
            <tr>
                <td><?= __('IČO') ?></td>
                <td>
                    <?= $ico ?>
                    <?php
                    if (!empty($verifiedIco)) {
                        echo '(';
                        if (!$icoIsVerified) {
                            // verified ico differs from provided
                            echo $verifiedIco . ' ';
                        }
                        echo __('ověřeno pomocí DS') . ')';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td><?= __('Registrace právnické osoby (kde, kdy)') ?></td>
                <td><?= $poRegistrationInformation ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <td><?= __('DIČ') ?></td>
            <td><?= Hash::get($nested, $isPo ? Identity::PO_VAT_ID : Identity::FO_VAT_ID) ?></td>
        </tr>
        <?php if (!$brief && $dsId) : ?>
            <tr>
                <td><?= __('ID Datové Schránky') ?></td>
                <td>
                    <?= $dsId ?>
                    <?php if ($dsIdVerified) {
                        echo '(ověřeno pomocí DS)';
                    } ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td><?= __('Kontaktní informace') ?></td>
            <td><?= Hash::get($nested, Identity::CONTACT_EMAIL) ?>
                , <?= Hash::get($nested, Identity::CONTACT_PHONE) ?></td>
        </tr>
        <?php if (!$brief && $isPo && Identity::mustProvideStatutory(Hash::get($nested, Identity::PO_CORPORATE_TYPE))) : ?>
            <tr>
                <td><?= __('Statutární orgán zastupující žadatele') ?></td>
                <td>
                    <?= sprintf(
                        "%s - %s, %s, %s",
                        Hash::get($nested, Identity::STATUTORY_FULLNAME),
                        Hash::get($nested, Identity::STATUTORY_REPRESENTATION_REASON),
                        Hash::get($nested, Identity::STATUTORY_PHONE),
                        Hash::get($nested, Identity::STATUTORY_EMAIL)
                    ) ?>
                    <?php
                    $statutoriesCount = 1;
                    foreach (Identity::extractPOStatutories($nested) as $interestData) {
                        $statutoriesCount++;
                        echo '<br/>';
                        echo sprintf(
                            "%s - %s, %s, %s",
                            $interestData[Identity::STATUTORY_X_FULLNAME],
                            $interestData[Identity::STATUTORY_X_REPRESENTATION_REASON],
                            $interestData[Identity::STATUTORY_X_PHONE],
                            $interestData[Identity::STATUTORY_X_EMAIL]
                        );
                    }
                    if ($statutoriesCount > 1) {
                        $actStandalone = boolval(Hash::get($nested, Identity::STATUTORIES_ACT_STANDALONE, false));
                        echo '<hr/>';
                        if ($actStandalone) {
                            echo __('Uvedení členové statutárního orgánu mohou jednat samostatně');
                        } else {
                            echo __('Uvedení členové statutárního orgánu jednají společně');
                        }
                    }
                    ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php if (!$brief) : ?>
            <tr>
                <td><?= __('Žadatel je plátcem DPH') ?></td>
                <td>
                    <?php echo Hash::get($nested, Identity::VAT) ? __('Ano') : __('Ne'); ?>
                </td>
            </tr>
            <tr>
                <td><?= __('Hlavní bankovní spojení (účet, na který bude vyplacena schválená dotace)') ?></td>
                <td>
                    <?= sprintf(
                        "%s / %s (%s)",
                        Hash::get($nested, Identity::BANK_NUMBER),
                        Hash::get($nested, Identity::BANK_CODE),
                        Hash::get($nested, Identity::BANK_NAME)
                    ) ?>
                </td>
            </tr>
            <?php if (trim(Hash::get($nested, Identity::BANK_NUMBER_2) . Hash::get($nested, Identity::BANK_CODE_2) . Hash::get($nested, Identity::BANK_NAME_2))) : ?>
                <tr>
                    <td><?= __('Doplňující bankovní spojení') ?></td>
                    <td>
                        <?= sprintf(
                            "%s / %s (%s)",
                            Hash::get($nested, Identity::BANK_NUMBER_2),
                            Hash::get($nested, Identity::BANK_CODE_2),
                            Hash::get($nested, Identity::BANK_NAME_2)
                        ) ?>
                    </td>
                </tr>
            <?php endif; ?>
            <?php $po_interests = Identity::extractPOInterests($nested); ?>
            <?php /*if (!empty($po_interests)) : */?>
            <?php if (false) : ?>
                <tr>
                    <td>
                        <?= __('Osoby s podílem v této právnické osobě') ?>
                    </td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><?= __('Velikost podílu') ?></th>
                                    <th><?= __('Vlastník podílu') ?></th>
                                    <th><?= __('IČO / Dat. narození') ?></th>
                                </tr>
                            </thead>
                            <?php foreach ($po_interests as $po_interest) : ?>
                                <tr>
                                    <td><?= Hash::get($po_interest, 'size') ?></td>
                                    <td><?= Hash::get($po_interest, 'owner') ?></td>
                                    <td><?= sprintf("%s / %s", Hash::get($po_interest, 'business_id'), Hash::get($po_interest, 'date_of_birth')) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>
                    <?php if (!Hash::get($nested, Identity::PO_ANOTHER_SHARE)) : ?>
                        <?= __('Žadatel vlastní podíl v další právnické osobě') ?>
                    <?php else : ?>
                        <?= __('Žadatel nevlastní podíl v další právnické osobě') ?>
                    <?php endif; ?>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <?php if (!Hash::get($nested, Identity::PO_ANOTHER_SHARE)) : ?>
                <tr>
                    <td>
                        <?= __('Právnické osoby v nichž má žadatel přímý podíl') ?>
                    </td>
                    <td>
                        <?php $owned_interests = Identity::extractOwnedInterests($nested); ?>
                        <?php if (!empty($owned_interests)) : ?>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= __('Velikost podílu') ?></th>
                                        <th><?= __('Název společnosti') ?></th>
                                        <th><?= __('IČO') ?></th>
                                    </tr>
                                </thead>
                                <?php foreach ($owned_interests as $owned_interest) : ?>
                                    <tr>
                                        <td><?= Hash::get($owned_interest, 'size') ?></td>
                                        <td><?= Hash::get($owned_interest, 'name') ?></td>
                                        <td><?= Hash::get($owned_interest, 'business_id') ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>
                    <?= __('Upřesnění informací o žadateli') ?>
                </td>
                <td>
                    <?= Hash::get($nested, Identity::NOTE) ?>
                </td>
            </tr>
            <?php if (!empty($identity_attachments)) : ?>
                <tr>
                    <td>
                        <?= __('Soubory přiložené k identitě žadatele') . ':' ?>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <table class="table table-stripped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th><?= __('Příloha č.') ?></th>
                                    <th><?= __('Název souboru') ?></th>
                                    <th><?= __('Popis souboru') ?></th>
                                    <th><?= __('Typ přílohy') ?></th>
                                    <th><?= __('Velikost') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($identity_attachments as $attachment) : ?>
                                    <tr>
                                        <td>
                                            <div class="ed"><?= $attachment->file_id ?></div>
                                        </td>
                                        <td>
                                            <div class="ed"><?= h($attachment->file->original_filename) ?>
                                                <?php
                                                echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['controller' => 'Users', 'action' => 'identityAttachmentFile', 'id' => $attachment->user_id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]);
                                                ?>
                                            </div>

                                        </td>
                                        <td>
                                            <div class="ed"><?= $attachment->description ?></div>
                                        </td>
                                        <td>
                                            <div class="ed"><?= $attachment->identity_attachment_type->type_name ?></div>
                                        </td>
                                        <td>
                                            <div class="ed"><?= Number::toReadableSize($attachment->file->filesize) ?></div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>
    </tbody>
</table>