<?php

/**
 * @var $this AppView
 */

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\Controller\ChangeRequestController;
use App\View\AppView;

$UstiException =  OrganizationSetting::isException(OrganizationSetting::USTI);
$admin_menu = "";

$pending_change_request_num = 0;

if ($this->isTeamFormalControlor()) {
    $chrc = new ChangeRequestController();
    $pending_change_request_num = $chrc->getNumPending();
    unset($chrc);
}

if ($this->isManager()) {
    $admin_menu .= $this->Html->link(__('Příručka správce dotačního portálu'), ['_name' => 'admin_docs'], ['class' => 'dropdown-item']);
    $admin_menu .= '<div class="dropdown-divider"></div>';
}

if ($this->isSystemsManager() || $this->isPortalsManager()) {
    $admin_menu .= $this->Html->link(__('Správa Organizací'), ['_name' => 'admin_organizations'], ['class' => 'dropdown-item']);
}
if ($this->isUsersManager()) {
    $admin_menu .= $this->Html->link(__('Správa Uživatelů'), ['_name' => 'admin_users'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Struktura Dotačního Úřadu'), ['_name' => 'admin_teams'], ['class' => 'dropdown-item']);
}
if ($this->isWikiManager()) {
    $admin_menu .= $this->Html->link(__('Správa nápovědy'), ['_name' => 'admin_wiki'], ['class' => 'dropdown-item']);
}
if ($this->isSystemsManager() || $this->isPortalsManager()) {
    $admin_menu .= $this->Html->link(__('Překlady'), ['_name' => 'admin_translations'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Vstoupit do žádosti'), ['_name' => 'locate_request'], ['class' => 'dropdown-item']);
    $admin_menu .= '<div class="dropdown-divider"></div>';
    $admin_menu .= $this->Html->link(__('Číselník zdrojů veřejné podpory'), ['_name' => 'admin_public_income_sources'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Číselník států'), ['_name' => 'csu_countries'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Číselník právních forem'), ['_name' => 'csu_legal_forms'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Číselník obcí'), ['_name' => 'csu_municipalities'], ['class' => 'dropdown-item']);
    $admin_menu .= $this->Html->link(__('Číselník částí obcí '), ['_name' => 'csu_municipality_parts'], ['class' => 'dropdown-item']);
}

$show_notifications_menu = User::userHasRoleInAnyProgram($this->getCurrentUser()->id, 'comments_programs');

?>

<ul class="navbar-nav">
    <?php if (!empty($admin_menu)) : ?>
        <li class="nav-item dropdown">
            <a href="#" id="dropdownAdministrator" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Administrátor') ?>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownAdministrator">
                <?= $admin_menu ?>
            </div>
        </li>
    <?php endif; ?>
    <?php if ($this->isGrantsManager()) : ?>
        <li class="nav-item dropdown">
            <a href="#" id="dropdownGrants" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Struktura Dotačních Fondů') ?>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownGrants">
                <?php
                echo $this->Html->link(__('Dotační Fondy'), ['_name' => 'admin_fonds'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Oblasti Podpory'), ['_name' => 'admin_realms'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Programy a Pod-programy'), ['_name' => 'admin_programs'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Výzvy k podání dotačních žádostí'), ['_name' => 'admin_appeals'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Hodnotící kritéria'), ['_name' => 'admin_evaluation_criteria'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Formuláře'), ['_name' => 'admin_forms'], ['class' => 'dropdown-item']);
                echo $this->Html->link(__('Datové sestavy / tabulky'), ['_name' => 'admin_reports'], ['class' => 'dropdown-item']);
                ?>
            </div>
        </li>
    <?php endif; ?>
    <?php if ($this->isManager() || $this->isTeamMember()) : ?>
        <li class="nav-item dropdown">
            <a href="#" id="dropdownHistory" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Historie') ?>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownHistory">
                <?= $this->Html->link(__('Historie žadatelů'), ['_name' => 'admin_history_identities'], ['class' => 'dropdown-item']); ?>
                <?php if (OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, false)) : ?>
                    <?= $this->Html->link(__('Historie ohlášení'), ['_name' => 'admin_notifications'], ['class' => 'dropdown-item']); ?>
                <?php endif; ?>
                <?php
                if ($this->getSiteSetting(OrganizationSetting::HAS_DSW) === true) {
                ?>
                    <div class="dropdown-divider"></div>
                <?php
                    echo $this->Html->link(__('DSW Archiv Žadatelů'), ['_name' => 'admin_history_dsw'], ['class' => 'dropdown-item']);
                    echo $this->Html->link(__('DSW Archiv Žádostí'), ['_name' => 'admin_history_dsw_zadosti'], ['class' => 'dropdown-item']);
                }
                ?>
            </div>
        </li>
    <?php endif; ?>
    <?php if ($this->isTeamMember()) : ?>
        <li class="nav-item dropdown">
            <a href="<?= $this->Url->build(['_name' => 'my_teams']) ?>" id="dropdownTeams" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Mé týmy') ?>
                <?php if ($this->isTeamFormalControlor() && $pending_change_request_num > 0) : ?>
                    <sup><i class="text-success fas fa-circle"></i></sup>
                <?php endif; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownTeams">
                <?= $this->Html->link(__('Přehled'), ['_name' => 'my_teams'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link(__('Zprávy'), ['_name' => 'my_teams_messages'], ['class' => 'dropdown-item']) ?>
                <div class="dropdown-divider"></div>
                <?= $this->isTeamFormalControlor() ? $this->Html->link(__('Formální kontrola'), ['_name' => 'team_formal_control_index'], ['class' => 'dropdown-item']) : '' ?>
                <?= $this->isTeamEvaluator() ? $this->Html->link(__('Hodnotitelé'), ['_name' => 'team_evaluators_index'], ['class' => 'dropdown-item']) : '' ?>
                <?= $this->isTeamProposals() ? $this->Html->link(__('Navrhovatelé'), ['_name' => 'team_proposals_index'], ['class' => 'dropdown-item']) : '' ?>
                <?= $this->isTeamApprovals() ? $this->Html->link(__('Schvalovatelé'), ['_name' => 'team_approvals_index'], ['class' => 'dropdown-item']) : '' ?>
                <?= $this->isTeamManagers() ? $this->Html->link(__('Finalizace'), ['_name' => 'team_managers_index'], ['class' => 'dropdown-item']) : '' ?>
                <?= $this->isTeamPreview() ? $this->Html->link(__('Náhled'), ['_name' => 'team_preview_index'], ['class' => 'dropdown-item']) : '' ?>
                <?php if ($UstiException) : ?>
                    <div class="dropdown-divider"></div>
                    <?= $this->isTeamFormalControlor() ? $this->Html->link(__('Žádosti o změnu') . " ($pending_change_request_num)", ['_name' => 'change_request_index'], ['class' => 'dropdown-item']) : '' ?>
                <?php endif; ?>
            </div>
        </li>
    <?php endif; ?>
    <?php if ($this->isEconomicsManager()) : ?>
        <li class="nav-item dropdown">
            <a href="#" id="dropdownEconomics" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Ekonomické oddělení') ?>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownEconomics">
                <?= $this->Html->link(__('Přehled'), ['_name' => 'my_economics'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link(__('Vyúčtování'), ['_name' => 'my_economic_settlements'], ['class' => 'dropdown-item']) ?>
            </div>
        </li>
    <?php endif; ?>
    <?= $this->Html->link(__('Moje žádosti'), ['_name' => 'user_requests'], ['class' => 'btn text-danger nav-item']) ?>
</ul>

<ul class="navbar-nav ml-auto">
    <li class="nav-item">
        <?= $this->Html->link(__('Nápověda'), ['_name' => 'public_wiki'], ['class' => 'btn btn-outline-success mr-2']) ?>
    </li>
    <li class="nav-item dropdown">
        <a href="#" id="dropdownUserSettings" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> <?= h($this->getUserEmail()) ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUserSettings">
            <?= $this->Html->link('<i class="fas fa-cogs mr-2"></i> ' . __('Moje informace'), ['_name' => 'update_self_info'], ['class' => 'dropdown-item', 'escapeTitle' => false]) ?>
            <?= $this->Html->link('<i class="fas fa-coins mr-2"></i> ' . __('Historie přijaté veřejné podpory'), ['_name' => 'update_self_history'], ['class' => 'dropdown-item', 'escapeTitle' => false]) ?>
            <div class="dropdown-divider"></div>
            <?= $this->isCurrentlyOriginUser() && $show_notifications_menu ? $this->Html->link('<i class="fas fa-bell mr-2"></i> ' . __('Nastavení oznámení'), ['_name' => 'notifications'], ['class' => 'dropdown-item', 'escapeTitle' => false]) : '' ?>
            <?= $this->isCurrentlyOriginUser() ? $this->Html->link('<i class="fas fa-share-square mr-2"></i> ' . __('Sdílet tento účet'), ['_name' => 'share_my_account'], ['class' => 'dropdown-item', 'escapeTitle' => false]) : '' ?>
            <?= $this->isCurrentlyOriginUser() ? $this->Html->link('<i class="fas fa-mail-bulk mr-2"></i> ' . __('Změnit e-mail'), ['_name' => 'change_email'], ['class' => 'dropdown-item', 'escapeTitle' => false]) : '' ?>
            <?= $this->isCurrentlyOriginUser() ? $this->Html->link('<i class="fas fa-key mr-2"></i> ' . __('Změnit heslo'), ['_name' => 'change_password'], ['class' => 'dropdown-item', 'escapeTitle' => false]) : '' ?>
            <?= $this->Html->link('<i class="fas fa-sign-out-alt mr-2"></i> ' . __('Odhlásit se'), ['_name' => 'logout'], ['class' => 'dropdown-item', 'escapeTitle' => false]) ?>
            <?php if ($this->canSwitchUsers()) : ?>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header"><?= __('Přepnout účet') ?></h6>
                <?php
                foreach ($this->getAllowedUsersForSwitch() as $allowedUsers) {
                    echo $this->Html->link($allowedUsers->email, ['_name' => 'switch_to_user', 'id' => $allowedUsers->id], ['class' => 'dropdown-item']);
                }
                ?>
            <?php endif; ?>
        </div>
    </li>
</ul>