<?php

use App\Model\Entity\Form;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form Form
 * @var $states array
 * @var $defaultStates array
 */
?>

<div class="btn-group btn-group-toggle">
  <?= $this->Form->button(__('Všechny stavy'), array(
    'id' => 'state_ids_btn_all',
    'type' => 'button',
    'onclick' => 'this.blur();stateIdsFilter(\'all\');',
    'class' => 'btn btn-outline-secondary btn-sm mb-3',
    'style' => 'margin-top:-0.6em;'
  ));
  ?>
  <?= isset($defaultStates) && count($defaultStates) > 0 ? $this->Form->button(__('Výchozí stavy'), array(
    'id' => 'state_ids_btn_default',
    'type' => 'button',
    'onclick' => 'this.blur();stateIdsFilter(\'default\');',
    'class' => 'btn btn-outline-secondary btn-sm mb-3',
    'style' => 'margin-top:-0.6em;'
  )) : '';
  ?>
  <?= $this->Form->button(__('Odebrat vše'), array(
    'id' => 'state_ids_btn_empty',
    'type' => 'button',
    'onclick' => 'this.blur();stateIdsFilter(\'empty\');',
    'class' => 'btn btn-outline-secondary btn-sm mb-3',
    'style' => 'margin-top:-0.6em;'
  ));
  ?>

  <script type="text/javascript">
    function stateIdsFilter(val) {
      switch (val) {
        case 'all':
          $("#state-ids").val([<?= implode(',', array_keys($states)) ?>]);
          break;
        case 'default':
          $("#state-ids").val([<?= implode(',', array_values(isset($defaultStates) && is_array($defaultStates) ? $defaultStates : [])) ?>]);
          break;
        case 'empty':
          $('#state-ids option').prop('selected', false);
          break;
      }
      $('#state-ids').change();
    }
  </script>
</div>