<?php

/**
 * @var $this AppView
 * @var $request Request
 * @var $merged_request Array
 */

use App\Model\Entity\OrganizationSetting;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestChange;
use App\Model\Entity\RequestBudgetChange;
use App\Model\Entity\SettlementState;
use App\View\AppView;
use Cake\I18n\Number;

$P4Exception = OrganizationSetting::isException(OrganizationSetting::P4);
$withNotifications = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::ALLOW_NOTIFICATIONS, true));

?>
<div class="col-xl-4 col-md-6 col-12 mt-2 pl-2"><!-- d-flex -->
    <div class="card flex-fill">
        <div class="card-header">

            <div class="row">
                <div class="col">
                    <h4 class="d-inline"><?= h($request->name) ?></h4>
                </div>
                <div class="mr-2">
                    č. <?= $request->id ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <?= sprintf("%s: %s", __('Výzva'), $request->appeal->name) ?> <br />
            <?= sprintf("%s: %s", __('Program podpory'), $request->program->name) ?> <br />

            <?php if ($request->canUserEditRequest() && !empty($request->lock_when)) : ?>
                <hr />
                <div class="alert alert-danger">
                    <?= __('Žádost bude automaticky uzavřena') ?>: <?= $request->lock_when->format('d.m.Y') ?>
                    <br />
                    <?= __('Důvod neschválení žádosti o dotaci') ?>:
                    <div>
                        <?= $request->lock_comment ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!$request->canUserEditRequest()) : ?>
                <?php if (!empty($request->reference_number)) : ?>
                    <?= __('Přidělené číslo jednací: ') . $request->reference_number ?>
                <?php endif; ?>
                <?php if (!in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])) : ?>
                    <hr />
                    <?php if ($request->request_state_id === RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS) : ?>
                        <div class="alert alert-danger">
                            <?= __('Vaše žádost je aktuálně v režimu porušení pravidel dotačního programu / rozpočtové kázně') ?>
                            <br />
                            <?= __('Pro více informací kontaktujte prosím poskytovatele dotace') ?>
                        </div>
                    <?php elseif ($request->request_state_id === RequestState::STATE_FORMAL_CHECK_REJECTED) : ?>
                        <div class="alert alert-danger">
                            <?= __('Vaše žádost nebyla schválena pro nesplnění formálních požadavků') ?>
                        </div>
                    <?php elseif ($request->request_state_id === RequestState::STATE_CLOSED_FINISHED && $request->final_subsidy_amount < 1) : ?>
                        <div class="alert alert-danger">
                            <?= __('Vaše žádost nebyla podpořena') ?>
                        </div>
                    <?php elseif (in_array($request->request_state_id, [
                        RequestState::STATE_READY_TO_SIGN_CONTRACT,
                        RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                        RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                        RequestState::STATE_SETTLEMENT_SUBMITTED,
                        RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
                        RequestState::STATE_CLOSED_FINISHED,
                    ], true)) : ?>
                        <div class="alert alert-success">
                            <?= __('Vaše žádost byla schválena') ?>
                            <ul>
                                <?php
                                if (in_array($request->request_state_id, [RequestState::STATE_READY_TO_SIGN_CONTRACT], true)) {
                                    echo sprintf("<li>%s</li>", __('Aktuálně se očekává podpis smlouvy o poskytnutí podpory'));
                                } else if ($request->final_subsidy_amount > 0) {
                                    echo sprintf("<li>%s</li>", __('Smlouva o podpoře byla podepsána'));
                                }
                                ?>
                                <?php if ($request->getPaymentsSum() > 0) : ?>
                                    <li><?= sprintf(
                                            "%s %s<br/>(%.2f%% %s)",
                                            __('Bylo vám vyplaceno'),
                                            Number::currency($request->getPaymentsSum(), 'CZK'),
                                            $request->getPaidTotalPercent(),
                                            __('poskytnuté podpory')
                                        )
                                        ?>
                                    </li>
                                <?php endif; ?>
                                <?php
                                if ($request->request_state_id === RequestState::STATE_PAID_READY_FOR_SETTLEMENT) {
                                    if ($request->hasSettlementWithRequestedChanges()) {
                                        $text = sprintf(
                                            '<span class=text-danger>%s</span>',
                                            __('Vaše vyúčtování nebylo přijato, prosím proveďte opravu a vyúčtování znovu podejte ke kontrole')
                                        );
                                    } else if ($request->hasSettlementWithRejected()) {
                                        $text = sprintf(
                                            '<span class=text-danger>%s</span>',
                                            __('Vyúčtování nevyhovuje podmínkám')
                                        );
                                    } else {
                                        $text = in_array($request->id, $merged_request)
                                            ? ''
                                            : __('Aktuálně se od Vás očekává vyúčtování přijaté podpory');
                                    }
                                } else if ($request->request_state_id === RequestState::STATE_SETTLEMENT_SUBMITTED) {
                                    $text = __('Probíhá zpracování podaného vyúčtování, prosím vyčkejte');
                                    if ($request->hasSettlementWithRejected()) {
                                        $text = sprintf(
                                            '<span class=text-danger>%s</span>',
                                            __('Vyúčtování nevyhovuje podmínkám')
                                        );
                                    } else if (is_array($request->settlements) && !empty($request->settlements)) {
                                        $most_recent_settlement = reset($request->settlements);
                                        if ($most_recent_settlement) { // See #424, but how can it become boolean ?
                                            $state_label = SettlementState::getLabel($most_recent_settlement->settlement_state_id);
                                            $text .= __(' (aktuální stav: ') . '<i>' . $state_label . '</i>).';
                                        }
                                    }
                                } else if ($request->request_state_id === RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT) {
                                    $text = __('Očekávejte výplatu podpory, dle příslušné veřejnoprávní smlouvy');
                                } else if ($request->request_state_id === RequestState::STATE_CLOSED_FINISHED) {
                                    $text = __('Tato žádost je ukončena a uzavřena');
                                } else if ($request->request_state_id === RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID) {
                                    $text = __('Očekáváme vrácení nevyčerpaných / nevyúčtovaných prostředků z poskytnuté podpory');
                                }
                                if (!empty($text)) {
                                    echo '<li>' . $text . '</li>';
                                }
                                ?>
                            </ul>
                        </div>
                    <?php elseif ($request->request_state_id === RequestState::STATE_SUBMIT_LOCK) : ?>
                        <div class="alert alert-info">
                            <?php if (!$P4Exception) : ?>
                                <?= __('Žádost je nyní zamčena a očekává se, že doručíte Potvrzení o elektronické evidenci žádosti na podatelnu') ?>
                                <br />
                                <?= __('Pokud chcete ještě před podáním provést úpravy, můžete žádost odemknout, nezapomeňte si však následně stáhnout nové Potvrzení, staré již nebude platné') ?>
                            <?php else : ?>
                                <?= __('Žádost je nyní zamčena a odeslána ke zpracování dotačnímu úřadu. Nyní se očekává, že doručíte:') ?>
                                <p>
                                <ol>
                                    <li><?= __('Podepsané “Potvrzení o elektronické evidenci žádosti”') ?></li>
                                    <li><?= __('Kompletně vyplněnou žádost, kterou získáte pomocí tlačítka "Stáhnout jako pdf"') ?></li>
                                    <li><?= __('Originály Vámi připravených povinných příloh') ?></li>
                                </ol>
                                </p>
                                <?= __('fyzicky na podatelnu MČ Praha 4 nebo poštou, aby mohlo dojít k dalšímu zpracování Vaší žádosti."') ?>
                            <?php endif; ?>
                        </div>
                    <?php else : ?>
                        <div class="alert alert-success">
                            <?= __('Žádost byla úspěšně odeslána a nyní probíhá její schvalování. O dalším postupu vás budeme informovat e-mailem a/nebo datovou schránkou') ?>
                            <br /><br />
                            <?= sprintf("%s: %s", __('Aktuální stav žádosti'), $request->getCurrentStateLabel()) ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="card-footer">

            <?php
            if ($this->isRequestOwner($request) && $request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                echo $this->Html->link(
                    __('Odeslat žádost Datovou schránkou'),
                    ['action' => 'requestSubmitDatabox', 'id' => $request->id],
                    ['class' => 'm-2 w-100 btn btn-info ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                );
            }

            if ($request->canUserLockOwnRequest()) {
                echo $this->Html->link(
                    __('Odeslat žádost jinak'),
                    ['action' => 'lockUnlockRequest', 'id' => $request->id],
                    ['class' => 'btn btn-info m-2 w-100 ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                );
            }

            if ($request->canUserDownloadOwnVerificationPdf()) {
                echo $this->Html->link(
                    __('Stáhnout Potvrzení o elektronické evidenci žádosti'),
                    ['action' => 'downloadRequestVerificationPdf', 'id' => $request->id],
                    ['class' => 'btn btn-success m-2 w-100'],
                );
            }

            if (!$P4Exception && $request->canUserUnlockOwnRequest()) {
                echo $this->Html->link(
                    __('Odemknout žádost k provádění úprav'),
                    ['action' => 'lockUnlockRequest', 'id' => $request->id, 'do' => 'unlock'],
                    ['class' => 'btn btn-warning m-2 w-100'],
                );
            }

            if ($withNotifications && (RequestState::canUserCreateNotification($request->request_state_id))) {
                $request->loadNotifications();
                if ($c = count($request->request_notifications)) {
                    $c_msg = ' (' . __('počet již evidovaných ohlášení: ') . $c . ')';
                } else {
                    $c_msg = '';
                }
                echo $this->Html->link(
                    __('Provést ohlášení akce / díla') . ' ' . $c_msg,
                    ['action' => 'notifications', 'id' => $request->id],
                    ['class' => ' btn btn-primary m-2 w-100']
                );
            }

            if ($request->canUserEditRequest()) {
                echo $this->Html->link(
                    __('Upravit žádost'),
                    ['action' => 'requestDetail', $request->id],
                    ['class' => 'btn btn-success m-2 w-100']
                );
            }

            if ($request->canUserDownloadPdf()) {
            ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <?= $this->Html->link(
                            __('Stáhnout jako PDF'),
                            ['action' => 'getPdf', $request->id],
                            ['class' => 'btn btn-success m-2 w-100', 'title' => __('Stáhnout žádost jako PDF')]
                        ) ?></div>
                    <div class="col-12 col-md-6">
                        <?= $this->Html->link(
                            __('Zobrazit jako PDF'),
                            ['action' => 'getPdfView', 'id' => $request->id],
                            ['class' => 'btn btn-success m-2 w-100', 'target' => '_blank', 'title' => __('Stáhnout žádost jako PDF')]
                        ) ?></div>
                </div>
            <?php
            }

            if ($request->canUserIssueRequestChange()) {
                echo '<hr />';
                echo $this->Html->link(
                    __('Vytvořit požadavek na změnu v podané žádosti'),
                    ['controller' => 'UserRequests', 'action' => 'requestChange', $request->id],
                    ['class' => 'btn btn-success m-2 w-100']
                );
            }

            // List of changes for "request" or "request_budget"
            $changeStatus = [
                RequestBudgetChange::CHANGE_ORIGINAL => __('originál'),
                RequestBudgetChange::CHANGE_ACCEPTED => __('byla schválena'),
                RequestBudgetChange::CHANGE_DECLINED => __('byla zamítnuta'),
                RequestBudgetChange::CHANGE_PENDING => __('čeká na schválení'),
            ];
            $request_changes = !empty($request->request_changes) ? array_filter($request->request_changes, function ($ch) use ($changeStatus) {
                return $ch->status > 0 && $ch->status < count($changeStatus);
            }) : [];
            $request_budgets_changes = !empty($request->request_budgets_changes) ? array_filter($request->request_budgets_changes, function ($ch) use ($changeStatus) {
                return $ch->status > 0 && $ch->status < count($changeStatus);
            }) : [];

            if (!empty($request_changes) || !empty($request_budgets_changes)) {
                echo '<hr /><div class="alert alert-info"><h5>' . __('Požadavky na změnu v podané žádosti') . ':</h5>';

                if (!empty($request_changes)) {
                    echo '<h6>' . __('Změny ve formuláři') . ':</h6><ul>';
                    foreach ($request_changes as $ch) {
                        echo '<li>' . __('Ze dne') . ' ' . $ch->created->i18nFormat() . ' - ' . $changeStatus[$ch->status];
                        echo $this->Html->link(__('Zobrazit změny'), ['controller' => 'ChangeRequest', 'action' => 'requestFormChangeView', $ch->id], ['class' => 'm-1 btn btn-secondary']);
                        echo '</li>';
                    }
                    echo '</ul>';
                }
                if (!empty($request_budgets_changes)) {
                    echo '<h6>' . __('Změny v rozpočtu') . ':</h6><ul>';
                    foreach ($request_budgets_changes as $ch) {
                        echo '<li>' . __('Ze dne') . ' ' . $ch->created->i18nFormat() . ' - ' . $changeStatus[$ch->status];
                        echo $this->Html->link(__('Zobrazit změny'), ['controller' => 'ChangeRequest', 'action' => 'requestBudgetChangeView', $ch->id], ['class' => 'm-1 btn btn-secondary']);
                        echo '</li>';
                    }
                    echo '</ul>';
                }
                echo '</div>';
            }

            if (
                $this->isRequestOwner($request)
                && in_array($request->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])
            ) {
                echo $this->Form->postLink(
                    __('Smazat žádost'),
                    ['action' => 'requestDelete', $request->id],
                    ['class' => 'btn btn-outline-danger m-2', 'confirm' => __('Opravdu chcete smazat tuto žádost?')]
                );
            }

            $currentSettlement = !empty($request->getPayments()) && isset($request->payments) && !empty($request->payments)
                ? end($request->payments)
                : null;

            if (
                $this->isRequestOwner($request) && $currentSettlement
            ) {
                echo '<hr />';
                echo $this->Html->link(
                    __('Vyúčtování žádosti'),
                    ['action' => 'settlementCreate', $request->id],
                    ['class' => 'btn btn-dark w-100 m-2']
                );
            }

            if (
                $request->canUserDownloadPdf() &&
                $currentSettlement &&
                isset($currentSettlement->settlement) &&
                isset($currentSettlement->settlement->settlement_state_id) &&
                $currentSettlement->settlement->settlement_state_id > SettlementState::STATE_NEW
            ) {
            ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <?= $this->Html->link(
                            __('Stáhnout jako PDF'),
                            ['controller' => 'UserRequests', 'action' => 'settlementPdf', $currentSettlement->settlement_id],
                            ['class' => 'btn btn-secondary m-2 w-100', 'title' => __('Stáhnout vyúčtování jako PDF')]
                        ) ?></div>
                    <div class="col-12 col-md-6">
                        <?= $this->Html->link(
                            __('Zobrazit jako PDF'),
                            ['controller' => 'UserRequests', 'action' => 'settlementPdfView', $currentSettlement->settlement_id],
                            ['class' => 'btn btn-secondary m-2 w-100', 'target' => '_blank', 'title' => __('Zobrazit vyúčtování jako PDF')]
                        ) ?></div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>