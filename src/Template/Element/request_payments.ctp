<?php

use App\Model\Entity\Request;
use App\Model\Entity\SettlementState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $request Request
 * @var $this AppView
 **/
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= __('Přehled plateb této žádosti') ?>
    </h2>
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <td><?= __('Datum provedení platby') ?></td>
                    <td><?= __('Částka') ?></td>
                    <td><?= __('Typ platby') ?></td>
                    <td><?= __('Bylo vyúčtováno?') ?></td>
                    <td><?= __('ID vyúčtování') ?></td>
                    <td><?= __('Akce') ?></td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($request->getPayments() as $payment) : ?>
                    <tr>
                        <td><?= $payment->created->format('d.m.Y') ?></td>
                        <td data-type="currency"><?= Number::currency($payment->amount_czk, 'CZK') ?></td>
                        <td>
                            <?= $payment->is_refund ? __('Refundace') : __('Výplata podpory') ?>
                            <?= $payment->in_addition
                                ? '<br />' . __('Dofinancování')
                                : '' ?>
                            <?php if (isset($payment->files) && count($payment->files) > 0) {
                                foreach ($payment->files as $attachment) {
                                    echo '<br />' . h($attachment->original_filename) . '&nbsp;';
                                    echo $this->Html->link(
                                        '<i class="fas fa-download ml-2 mr-1"></i>',
                                        ['action' => 'additionalFinancingDownloadFile', 'payment_id' => $payment->id, 'file_id' => $attachment->id],
                                        ['escapeTitle' => false, 'title' => __('Stáhnout')]
                                    );
                                    echo $this->Html->link(
                                        '<i class="fas fa-file ml-1 mr-1"></i>',
                                        ['action' => 'additionalFinancingViewFile', 'payment_id' => $payment->id, 'file_id' => $attachment->id],
                                        ['escapeTitle' => false, 'title' => __('Zobrazit'),  'target' => '_blank']
                                    );
                                }
                            }
                            ?>
                            <?= $payment->comment ? sprintf("%s", trim($payment->comment)) : '' ?>
                        </td>
                        <td>
                            <?php
                            if (empty($payment->settlement_id)) {
                                echo __('Vyúčtování chybí');
                            } else {
                                if ($payment->settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_APPROVED) {
                                    echo $this->Html->link(__('Vyúčtováno a schváleno'), ['action' => 'settlementDetail', $payment->settlement_id]);
                                } else {
                                    echo $this->Html->link(__('Vyúčtování existuje, ale zatím není schváleno'), ['action' => 'settlementDetail', $payment->settlement_id]);
                                }
                            }
                            ?>
                        </td>
                        <td data-type="currency">
                            <?= isset($payment->settlement_id) ? $payment->settlement_id : '<span class="text-muted">' . __('- není -') . '</span>' ?>
                        </td>
                        <td>
                            <?php
                            echo empty($payment->settlement_id)
                                ? $this->Html->link(
                                    __('Vymazat tuto platbu'),
                                    ['action' => 'deletePayment', 'request_id' => $payment->request_id, 'payment_id' => $payment->id],
                                    ['class' => 'text-danger', 'confirm' => __('Opravdu chcete vymazat tuto platbu?')]
                                ) :  '<span class="text-muted">' . __('- není -') . '</span>';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>