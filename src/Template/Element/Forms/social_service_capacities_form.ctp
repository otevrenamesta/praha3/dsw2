<?php

use App\Form\SocialServiceCapacitiesFormController;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $form SocialServiceCapacitiesFormController
 * @var $request Request
 * @var $enableInput bool
 */

// for tables in this template disable wrapping <input> and displaying labels/helpers/...
$this->Form->setTemplates([
    'formGroup' => '{{input}}',
    'inputContainer' => '{{content}}'
]);

$years = $form->getYears();
$showColumn4 = $form->getSettingValue($form::SETTING_SHOW_COLUMN_4, false);

$simple_table = $form->getTables();

?>

<?php foreach ($simple_table as $tableName => $table): ?>

    <h3><?= $table['title'] ?></h3>

    <table class="table table-bordered table-with-inputs">
        <colgroup>
            <col class="w-25">
        </colgroup>
        <thead class="thead-dark text-center">
        <tr>
            <th rowspan="2"><?= __('Jednotka') ?></th>
            <th colspan="2"><?= sprintf("%s %s", __('Skutečnost'), $years[0]) ?></th>
            <th colspan="2"><?= sprintf("%s %s", __('Skutečnost'), $years[1]) ?></th>
            <th colspan="2"><?= sprintf("%s %s", __('Plán'), $years[2]) ?></th>
            <?php if ($showColumn4): ?>
                <th colspan="2"><?= sprintf("%s %s", __('Plán'), $years[3]) ?></th>
            <?php endif; ?>
        </tr>
        <tr>
            <th><?= __('Celkem') ?></th>
            <th><?= OrgDomainsMiddleware::getCurrentOrganization()->name ?></th>
            <th><?= __('Celkem') ?></th>
            <th><?= OrgDomainsMiddleware::getCurrentOrganization()->name ?></th>
            <th><?= __('Celkem') ?></th>
            <th><?= OrgDomainsMiddleware::getCurrentOrganization()->name ?></th>
            <?php if ($showColumn4): ?>
                <th><?= __('Celkem') ?></th>
                <th><?= OrgDomainsMiddleware::getCurrentOrganization()->name ?></th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($table['rows'] as $rowId => $rowName): ?>
            <tr class="tr-inputs">
                <td class="text-center p-4 font-weight-bold"><?= $rowName ?></td>
                <?php for ($i = 0; $i < ($showColumn4 ? 8 : 6); $i++): ?>
                    <td><?= $form->fieldOrFormatted($this->Form, $tableName, $rowId, $i) ?></td>
                <?php endfor; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <hr/>

<?php endforeach; ?>

<style type="text/css">
    <?php if($enableInput): ?>

    .table-with-inputs * {
        border-radius: 0;
    }

    .tr-inputs td input {
        width: 100%;
        height: 100%;
        padding: 1.75rem;
        margin: 0;
        border-radius: 0;
        border: 0;
    }

    .tr-inputs .input-group-append, .tr-inputs .input-group-text {
        border-radius: 0;
        border: 0;
    }

    .tr-inputs td {
        padding: 0;
    }

    <?php endif; ?>

    .w-20 {
        width: 20% !important;
    }

    .w-30 {
        width: 30% !important;
    }

    .w-15 {
        width: 15% !important;
    }
</style>
