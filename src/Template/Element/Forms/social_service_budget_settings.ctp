<?php
/**
 * @var $this AppView
 * @var $form SocialServiceBudgetFormController
 */

use App\Form\SocialServiceBudgetFormController;
use App\View\AppView;

?>

<?= $this->asHtmlListRecursive([
    __('Rozpočet poskytované služby (projektu) a požadavek od MČ Praha podle nákladových položek'),
]); ?>

<hr/>

<?php
echo $this->Form->create($form->getFormDefinition());
?>
<h2><?= __('Nastavení sloupců tabulky') ?></h2>
<?php
echo $this->Form->control('form_settings.0.id', ['value' => $form->getSetting(SocialServiceBudgetFormController::SETTING_COLUMN_1_YEAR)->id]);
echo $this->Form->control('form_settings.0.value', ['type' => 'number', 'label' => __('Příští rok (pro jednoleté financování)')]);
echo $this->Form->control('form_settings.1.id', ['value' => $form->getSetting(SocialServiceBudgetFormController::SETTING_COLUMN_2_YEAR)->id]);
echo $this->Form->control('form_settings.1.value', ['type' => 'number', 'label' => __('Přespříští rok (pro dvouleté financování)')]);
echo $this->Form->control('form_settings.2.id', ['value' => $form->getSetting(SocialServiceBudgetFormController::SETTING_SHOW_YEAR_2)->id]);
echo $this->Form->control('form_settings.2.value', [
    'type' => 'checkbox',
    'label' => __('Zobrazit sloupec pro dvouleté financování'),
    'checked' => $form->getSettingValue(SocialServiceBudgetFormController::SETTING_SHOW_YEAR_2),
    'required' => false
]);
echo $this->Form->submit(__('Uložit nastavení'), ['class' => 'btn btn-success']);
echo $this->Form->end();

?>
