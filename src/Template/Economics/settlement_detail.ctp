<?php

use App\Model\Entity\Program;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\Model\Entity\SettlementAttachmentType;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $settlement Settlement
 * @var $program Program
 * @var $settlement_attachment SettlementAttachment
 */

?>

<div class="card m-2">
    <div class="card-header row">
        <div class="col-md">
            <h2><?= __('Akce ekonomického oddělení') ?></h2>
        </div>
        <div class="col-md text-right">
            <?= $this->Html->link(
                sprintf("%s #%d", __('Otevřít detail žádosti'), $settlement->getRequest()->id),
                $settlement->getRequest()->getAdminDetailLink(),
                ['class' => 'btn btn-primary', 'target' => '_blank']
            ) ?>
            <?php
            $filename = urlencode(trim($settlement->getRequest()->id));
            if ($settlement->settlement_state_id !== SettlementState::STATE_NEW) {
                echo $this->Html->link(
                    __('Stáhnout jako PDF'),
                    ['action' => 'settlementDownloadPdf', $settlement->id, 'download' => $filename . '.pdf'],
                    ['class' => 'btn btn-success', 'title' => __('Stáhnout')]
                ) . ' ';
                echo $this->Html->link(
                    __('Otevřít jako PDF'),
                    ['action' => 'settlementDownloadPdfView', $settlement->id, 'filename' => $filename],
                    ['class' => 'btn btn-success', 'title' => __('Zobrazit'), 'target' => '_blank']
                );
                echo ($settlement->settlement_attachments && count($settlement->settlement_attachments) > 0)
                    ?  ('<p>' . $this->Html->link(
                        __('Stáhnout přílohy jako PDF'),
                        ['action' => 'settlementPdfAttachments', $settlement->id,],
                        ['class' => 'btn btn-secondary mt-1', 'title' => __('Stáhnout přílohy vyúčtování')]
                    ) . '</p>')
                    : '';
            }
            ?>
            <?php
            if (in_array($settlement->settlement_state_id, [SettlementState::STATE_FILLED_COMPLETELY, SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES], true)) {
                echo $this->Html->link(__('Manuálně označit vyúčtování jako odeslané (došlé)'), ['action' => 'settlementManualSubmit', 'settlement_id' => $settlement->id], ['class' => 'btn btn-warning ml-2']);
            }
            if ($settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_APPROVED) {
                echo $this->Html->link(__('Vrátit zpět mezi neschválená vyúčtování'), ['action' => 'settlementUnapprove', 'settlement_id' => $settlement->id], ['class' => 'btn btn-warning ml-2']);
            }
            ?>
        </div>
    </div>
    <div class="card-body">
        <div class="alert alert-info">
            <?php
            switch ($settlement->settlement_state_id) {
                case SettlementState::STATE_NEW:
                    echo __('Vyúčtování je vytvořeno, ale není dokončeno, je potřeba počkat, až jej žadatel odešle ke kontrole');
                    break;
                case SettlementState::STATE_FILLED_COMPLETELY:
                    echo __('Vyúčtování je kompletně vyplněno, stačí jen odeslat nebo označit jako odeslané');
                    break;
                case SettlementState::STATE_SUBMITTED_READY_TO_REVIEW:
                    echo __('Vyúčtování je podáno, nyní je třeba se vyjádřit k jeho úplnosti a správnosti');
                    break;
                case SettlementState::STATE_ECONOMICS_APPROVED:
                    echo __('Vyúčtování je schváleno jako úplné a kompletní');
                    break;
                case SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES:
                    echo __('Vyúčtování bylo vráceno žadateli k opravě s následujícím komentářem') . ':' . '<hr/>' . $settlement->econom_statement;
                    if (!empty($settlement->repair_date)) {
                        echo sprintf(
                            "<br />%s: %s",
                            __('Datum, do kdy má žadatel čas vyúčtování opravit'),
                            $settlement->repair_date->nice()
                        );
                    }
                    break;
                case SettlementState::STATE_SETTLEMENT_REJECTED:
                    echo __('Vyúčtování nevyhovuje podmínkám');
                    if (!empty($settlement->rejected_statement)) {
                        echo '<hr/>' . $settlement->rejected_statement;
                    }
                    break;
            }
            ?>
        </div>
        <div class="row">
            <?php if (in_array($settlement->settlement_state_id, [SettlementState::STATE_SUBMITTED_READY_TO_REVIEW])) : ?>
                <div class="col-md">
                    <div class="card">
                        <h2 class="card-header"><?= __('Vyúčtování je v pořádku') ?></h2>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($settlement);
                            echo $this->Form->hidden('settlement_state_id', ['value' => SettlementState::STATE_ECONOMICS_APPROVED]);
                            //echo $this->Form->control('econom_statement', ['label' => __('Nepovinný komentář pro evidenci / další zpracování')]);
                            echo $this->Form->submit(__('Potvrdit správnost'), ['class' => 'btn btn-success']);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (in_array(
                $settlement->settlement_state_id,
                [SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES, SettlementState::STATE_SETTLEMENT_REJECTED]
            )) : ?>
                <div class="col-md">
                    <div class="card">
                        <h2 class="card-header"><?= __('Vyúčtování není v pořádku') ?></h2>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($settlement);
                            echo $this->Form->hidden('settlement_state_id', ['value' => SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES]);
                            echo $this->Form->control('econom_statement', ['label' => __('Vysvětlení, proč není vyúčtování v pořádku'), 'type' => 'textarea']);
                            echo $this->Form->control('repair_date', ['label' => __('Datum, do kdy má žadatel čas vyúčtování opravit'), 'type' => 'date', 'default' => date('Y-m-d', strtotime('+7 days')), 'class' => 'w-100']);
                            echo $this->Form->submit(__('Vrátit vyúčtování žadateli k opravě'), ['class' => 'btn btn-warning']);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (in_array($settlement->settlement_state_id, [SettlementState::STATE_SETTLEMENT_REJECTED])) : ?>
                    <div class="card mt-2">
                        <h2 class="card-header"><?= __('Vrátit vyúčtování ke kontrole úředníkem') ?></h2>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($settlement);
                            echo $this->Form->hidden('settlement_state_id', ['value' => SettlementState::STATE_SUBMITTED_READY_TO_REVIEW]);
                            echo $this->Form->submit(__('Vrátit vyúčtování ke kontrole úředníkem'), ['class' => 'btn btn-success']);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (in_array($settlement->settlement_state_id, [SettlementState::STATE_SUBMITTED_READY_TO_REVIEW])) : ?>
                    <div class="card mt-2">
                        <h2 class="card-header"><?= __('Vyúčtování nevyhovuje podmínkám') ?></h2>
                        <div class="card-body">
                            <?php
                            echo $this->Form->create($settlement);
                            echo $this->Form->hidden('settlement_state_id', ['value' => SettlementState::STATE_SETTLEMENT_REJECTED]);
                            echo $this->Form->control('rejected_statement', ['label' => __('Vysvětlení, proč vyúčtování nevyhovuje podmínkám'), 'type' => 'textarea']);
                            echo $this->Form->submit(__('Vyúčtování nevyhovuje podmínkám'), ['class' => 'btn btn-danger']);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= $this->render('/UserRequests/settlement_detail', false); ?>
<?= $this->element('settlement_logs', compact('settlement', 'program')) ?>