<?php

/**
 * @var $this AppView
 * @var $request Request
 * @var $payment Payment
 * @var $mergedRequest object
 */

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Payment;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

$this->assign('title', sprintf("%s #%d", __('Evidence plateb'), $request->id));
$additionalFinancing = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_ADDITIONAL_FINANCING);
$isMergedRequest = $mergedRequest && $mergedRequest->id > 0 && $mergedRequest->final_subsidy_amount > 0;
$mergedRequestAmount = $isMergedRequest ? (float)$mergedRequest->final_subsidy_amount : 0;

$lastStates = [
    RequestState::STATE_FORMAL_CHECK_REJECTED,
    RequestState::STATE_CLOSED_FINISHED,
    RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
];
$isLastState = in_array($request->request_state_id, $lastStates);

echo $this->Form->create($payment, ['type' => 'file']);
?>

<div class="card m-2">
    <h4 class="card-header">
        <?= sprintf("%s #%d", __('Evidovat platbu / Změnit stav žádosti'), $request->id) ?>
    </h4>
    <div class="card-body">
        <?php
        // Schválená podpora
        $undrawnSupport = $request->getFinalSubsidyAmount() - $request->getPaymentsSum() + $mergedRequestAmount;
        echo $this->Form->control('amount_czk', ['label' => __('Částka transakce (Kč)'), 'style' => 'max-width:450px', 'min' => 0, 'default' => max($request->getFinalSubsidyAmount() - $request->getPaymentsSum(), 0)]);
        echo '<ul class="pb-2"><li>Schválená podpora celkem: ' . Number::currency($request->getFinalSubsidyAmount() + $mergedRequestAmount, 'CZK');

        if (!$isMergedRequest && isset($request->original_subsidy_amount) && !empty($request->original_subsidy_amount)) {
            echo '<ul>';
            echo '<li>' . sprintf("%s: %s", __('Původní podpora celkem'), Number::currency($request->original_subsidy_amount, 'CZK')) . '</li>';
            echo '<li>' . sprintf("%s: %s", __('Dofinancování (navýšení původně schválené podpory)'), Number::currency($request->final_subsidy_amount - $request->original_subsidy_amount, 'CZK')) . '</li>';
            echo '</ul>';
        } else if ($isMergedRequest) {
            echo '<ul>';
            echo '<li>' . __('Schválená podpora "této" žádosti') . ': ' . Number::currency($request->final_subsidy_amount, 'CZK') . '</li>';
            echo '<li>' . __('Schválená podpora "propojené" žádosti') . ' (#' . $mergedRequest->id . ', ' . $mergedRequest->name . '): ' . Number::currency($mergedRequest->final_subsidy_amount, 'CZK') . '</li>';
            echo '</ul>';
        }
        echo '</li>
            <li>' . __('Vyplacená podpora') . ': ' . Number::currency($request->getPaymentsSum(), 'CZK') . '</li>
            <li>' . __('Nečerpaná podpora') . ': ' . Number::currency($undrawnSupport, 'CZK') .
            ($undrawnSupport < 0 ? '&nbsp; <span class="badge bg-danger text-white">' . __('Přečerpáno') . '</span>' : '') . '
            </li></ul>';
        echo $this->Form->control('is_refund', ['label' => __('Je tato platba refundace (vrácením peněz od žadatele) ?'), 'type' => 'checkbox']);

        echo $this->Form->control('created', ['label' => __('Datum provedení platby (nepovinné)'), 'style' => 'max-width:450px', 'default' => date('Y-m-d'), 'type' => 'date']);
        if ($additionalFinancing) {
            echo '<hr />';
            echo $this->Form->control('in_addition', ['label' => __('Je tato platba dofinancování (navýšení původně schválené podpory) ?'), 'type' => 'checkbox']);
            echo $this->Form->control('comment', ['type' => 'textarea', 'label' => __('Poznámka k dofinancování')]);
            echo $this->Form->control('files.0.filedata', ['type' => 'file', 'required' => false, 'label' => __('Příloha k dofinancování')]);
            echo '<hr />';
        }
        ?>
        <div class="pb-2"></div>
        <?php
        echo $this->Form->control('current_request_state_id', [
            'type' => 'text',
            'label' => __('Aktuální stav žádosti'),
            'default' => RequestState::getLabelByStateId($request->request_state_id),
            'value' => RequestState::getLabelByStateId($request->request_state_id),
            'disabled' => true,
            'style' => 'max-width:850px'
        ]);
        echo $this->Form->control('new_request_state_id', [
            'label' => __('Nový stav žádosti'),
            'options' => RequestState::getAllowedTransitionsLabels($request->request_state_id),
            'default' => $request->request_state_id,
            'style' => 'max-width:850px'
        ]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        $confirmMessage =  $isLastState ? __('Vyřízení žádosti již bylo ukončeno. Opravdu chcete provést nové změny?') : null;
        echo $this->Form->button(
            __('Evidovat platbu a/nebo nastavit stav žádosti'),
            ['type' => 'submit', 'class' => 'btn btn-success m-2', 'name' => 'SET_REQUEST_STATE', 'confirm' => $confirmMessage],
        );
        echo $this->Form->button(
            __('Pouze provést evidenci platby, ale neměnit stav žádosti'),
            ['type' => 'submit', 'class' => 'btn btn-info m-2', 'confirm' => $confirmMessage],
        );
        echo $this->Form->end();
        ?>
    </div>
</div><br />

<?= $this->element('request_payments', compact('request')) ?>
<hr />

<?php
echo $this->element('request_full_table', compact('request'));
?>
<div class="card m-2">
    <h5 class="card-header"><?= __('Nápověda') ?></h5>
    <div class="card-body">

        <div class="card m-2">
            <h2 class="card-header">Rozdělení vyúčtování na 1 a 2-leté dotace</h2>
            <div class="card-body">
                <div>
                    <strong>Rozdělení schválené podpory</strong>
                    <ul>
                        <li>schválená podpora lze rozdělit na více částí, každá část má své vyúčtování</li>
                        <li>přejděte do sekce: Ekonomické oddělení / Přehled</li>
                        <li>vyhledejte žádost u které bude evidována nová platba</li>
                        <li>zvolte u ní akci "Evidence plateb"</li>
                        <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
                    </ul>
                </div>
                <div>
                    <strong>Evidovat platbu / Změnit stav žádosti</strong>
                    <ul>
                        <li>v této sekci nastavte "částku transakce"</li>
                        <li>pokud je částka shodná se "schválenou podporou celkem", je podpora vyčerpána jednorázově</li>
                        <li>pokud je částka nižší než "schválenáu podpora celkem", umožní to rozdělit podporu na více částí</li>
                        <li>každá takto evidovaná výplata podpory (částka transakce) má své samostatné vyúčtování, které přijde žadateli k vyplnění</li>
                        <li>tímto způsobem lze "schválenou podporou celkem" rozdělit bez omezení na více částí, až do vyčerpání celkové přiznané podpory</li>
                        <li>rozdělení je časově neomezené a lze dopnit kdykoliv později a umožnit tak vyúčtování např. na 1 a 2-leté dotace</li>
                    </ul>
                </div>
                <div>
                    <strong>Přehledy vyplacených částek a částek k čerpání</strong>
                    <ul>
                        <li>pod formulářem "Evidovat platbu / Změnit stav žádosti" je v tabulce přehled všech uskutečněných platem "Přehled plateb této žádosti"</li>
                        <li>ve vyúčtování platby, na stránce "Vyúčtování dotace", je pak uveden přehled jaká byla "Schválená podpora celkem"</li>
                        <li>a částky "Vyplacená podpora" a/nebo "Nečerpaná podpora"</li>
                        <li>částka transakce pro vyúčtování ve "Vyúčtování dotace" se nazývá "Částka k čerpání v tomto vyúčtování"</li>
                    </ul>
                </div>
                <div>
                    <strong>Zrušení evidované platby a vyúčtování</strong>
                    <ul>
                        <li>každá platba je evidovaná samostatně a v případě chyby lze použít funkci "Vymazat tuto platbu"</li>
                        <li>po smazání platby se uvolní se částka pro nastavení správné "Výplaty podpory"</li>
                        <li>nelze vymazat platbu pokud k ní již existuje "Vyúčtování"</li>
                        <li>"Vyúčtování" může smazat pouze žadatel, který ho vyplňuje</li>
                    </ul>
                </div>
            </div>
        </div>
        <p><em>Více informací naleznete v sekci <a href="/admin/docs#vicelete-vyuctovani" target="_blank">Příručka správce dotačního portálu</a></em></p>
        <?php if ($additionalFinancing) : ?>
            <br />
            <div class="card m-2">
                <h2 class="card-header">Dofinancování žádosti</h2>
                <div class="card-body">
                    <div>
                        <strong>Dofinancování (navýšení původně schválené podpory)</strong>
                        <ul>
                            <li>dofinancování slouží pro mimořádné navýšení původně schválené podpory</li>
                            <li>zapnutí modulu "Dofinancování" se provede v sekci Administrator / Správa organizace v "Nastavení ekonomického oddělení"</li>
                            <li>zde zaškrtněte volbu "Povolit možnost dofinancování - navýšení původně schválené podpory"</li>
                            <li>pak přejděte do sekce: Ekonomické oddělení / Přehled</li>
                            <li>vyhledejte žádost, která bude dofinancována</li>
                            <li>zvolte u ní akci "Evidence plateb"</li>
                            <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Evidovat platbu / Změnit stav žádosti</strong>
                        <ul>
                            <li>v této sekci nastavte jako "Částku transakce" konkrétní částku pro dofinancování, o kterou se "chválená podpora" navyšuje</li>
                            <li>níže zaškrtněte volbu "Je tato platba dofinancování (navýšení původně schválené podpory)?"</li>
                            <li>dle potřeby přidejte "Poznámku k dofinancování"</li>
                            <li>dle potřeby přidejte "Přílohu k dofinancování"</li>
                            <li>pokud je to potřeba, nastavte "Nový stav žádosti"</li>
                            <li>konečné nastavení potvrdíte tlačítkem "Evidovat platbu a/nebo nastavit stav žádosti"</li>
                            <li>nastavené informace se zobrazí níže v tabulce "Přehled plateb této žádosti"</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Dofinancování v přehledech a vyúčtování</strong>
                        <ul>
                            <li>po nastavení "dofinancování" se v detailu evidence plateb a v žádostech nově uvádí tyto částky</li>
                            <li>1. Schválená podpora celkem</li>
                            <li>2. Původní podpora celkem</li>
                            <li>3. Dofinancování (navýšení původně schválené podpory)</li>
                            <li>tyto částky jsou zobrazeny i v PDF verzi vyúčtování pro tisk a v modulu Anonymizace</li>
                            <li>v seznamech se uvádí informace o "Dofinancování" ve sloupci "Podpora celkem", respektive "Typ vyúčtování" u vyúčtování</li>
                            <li>vypnutí modulu "Dofinancování", v sekci Administrator / Správa organizace, nemá vliv na již "dofinancované" žádosti</li>
                        </ul>
                    </div>
                    <div>
                        <strong>Vymazání platby pro "Dofinancování"</strong>
                        <ul>
                            <li>přejděte do sekce: Ekonomické oddělení / Přehled</li>
                            <li>vyhledejte žádost, která byla dofinancována</li>
                            <li>zvolte u ní akci "Evidence plateb"</li>
                            <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
                            <li>zde v tabulce "Přehled plateb této žádosti" zvolte platbu s typem "Dofinancování" a u ní akci "Vymazat tuto platbu"</li>
                            <li>"Dofinancování" nelze vymazat, pokud k němu již existuje "Vyúčtování"</li>
                        </ul>
                    </div>
                </div>
            </div>
            <p><em>Více informací naleznete v sekci <a href="/admin/docs#dofinancovaní" target="_blank">Příručka správce dotačního portálu</a></em></p>
        <?php endif; ?>
    </div>
</div>