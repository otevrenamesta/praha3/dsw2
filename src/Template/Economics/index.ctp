<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;
use App\Controller\OpenDataController;

/**
 * @var $this AppView
 * @var $appeals array
 * @var $states array
 * @var $defaultStates array
 * @var $requests Request[]
 * @var $withNotifications bool
 */

echo $this->element('simple_datatable');
echo $this->element('simple_select2');
$this->assign('title', __('Přehled'));

$actions = [
    RequestState::STATE_PAID_READY_FOR_SETTLEMENT => RequestState::getLabelByStateId(RequestState::STATE_PAID_READY_FOR_SETTLEMENT),
]
?>

<?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW, true) === true) : ?>
    <div class="alert alert-info">
        <?= __('V této sekci nejsou vidět vyúčtování pro úspěšné žádosti z předchozí verze Dotačního Software') ?>
        <br />
        <?= $this->Html->link(__('Přejít do sekce vyúčtování'), ['action' => 'settlements'], ['class' => 'btn btn-success']) ?>
    </div>
<?php endif; ?>

<div class="card mb-3" id="economics-filter">
    <div class="card-header">
        <h2><?= __('Přehled všech žádostí') ?></h2>
        <?= $this->Form->create(null, ['type' => 'get']) ?>
        <div class="row">
            <div class="col-md">
                <?= $this->Form->control(
                    'state_ids',
                    ['options' => $states, 'class' => 'select2', 'multiple' => true, 'label' => __('Stav žádosti'), 'empty' => true]
                ) ?>
                <?= $this->element('table_status_filter') ?>
                <?= $this->Form->submit(__('Filtrovat')) ?>
            </div>
            <div class="col-md">
                <?= $this->Form->control(
                    'appeal_id',
                    ['options' => $appeals, 'label' => __('Dotační výzva'), 'empty' => __('Všechny výzvy...')]
                ) ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<?= $this->Form->create(null, ['type' => 'post']) ?>
<div class="card mb-3">
    <div class="card-header pb-3">
        <h2 class='mt-1'><?= __('Hromadná změna stavu žádosti') ?></h2>
        <input type="checkbox" id="checkbox_all" />&nbsp;
        <label for="checkbox_all"><?= __('Vybrat/odebrat všechny výsledky na stránce') ?></label>
        <div class="mb-2">
            <?= $this->Form->control(
                'requests_action',
                [
                    'label' => __('Hromadná akce pro vybrané žádosti - změna stavu na') . ':',
                    'options' => $actions, 'multiple' => false, 'empty' => false, 'required' => false, 'style' => 'width:33%',
                ]
            ) ?>
        </div>
        <?= $this->Form->button(__('Nastavit'), [
            'type' => 'submit', 'class' => 'btn btn-info', 'escape' => false,
            'confirm' => __('Opravdu chcete změnit stav vybraných žádostí ?'),
        ]); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#checkbox_all').click(function() {
            $('#requests_table').find('input:checkbox').prop('checked', $(this).prop('checked'));
        });
    })
</script>

<table class="table" id="dtable">
    <?= $this->Form->unlockField('dtable_length'); ?>
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Vlastní identifikátor') ?></th>
            <th><?= __('Stav žádosti') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Program') ?></th>
            <th><?= __('Výzva') ?></th>
            <th><?= __('Oblast podpory') ?></th>
            <th><?= __('Podpora celkem') ?></th>
            <th><?= __('Vyplaceno celkem') ?></th>
            <?php if ($withNotifications) : ?><th><?= __('Ohlášeno?') ?></th><?php endif; ?>
            <th><?= __('Vyúčtováno?') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody id="requests_table">
        <?php foreach ($requests as $request) : ?>
            <tr>
                <td>
                    <?= $this->Form->control('request_checkbox', ['label' => $request->id, 'id' => 'requests_item' . (string)$request->id, 'name' => 'requests_item[]', 'value' => $request->id, 'type' => 'checkbox']) ?>
                </td>
                <td><?= $request->reference_number ?></td>
                <td title='<?= $request->request_state_id ?>'><?= $request->getCurrentStateLabel() ?></td>
                <td><?= $this->Html->link(h($request->name), ['_name' => 'my_teams_request_detail', 'id' => $request->id]) ?></td>
                <td><?= $this->getUserIdentity($request->user_id, $request->user_identity_version, 'default') ?></td>
                <td><?= $request->program->name ?></td>
                <td><?= (isset($appeals) && !empty($appeals[$request->appeal_id]) ? $appeals[$request->appeal_id] : '') ?></td>
                <td><?= $request->program->realm->name ?></td>
                <?php
                $request_sum = round($request instanceof Request ? $request->getPaymentsSum() : $request->konecna_castka, 2);
                $request_refund = round($request instanceof Request ? $request->getPaymentsRefundSum() : 0, 2);
                // old value: Number::currency($request->subsidy_paid, 'CZK')
                ?>
                <td data-type="currency">
                    <?= Number::currency($request->final_subsidy_amount ?? $request->request_budget->requested_amount ?? 0, 'CZK') ?>
                    <?= isset($request->original_subsidy_amount) && $request->original_subsidy_amount > 0 ? '<br />' . __('Dofinancováno') : '' ?>
                    <?= isset($request->merged_request) && $request->merged_request > 0
                        ? '<br />a ' . Number::currency($request_sum  - $request->final_subsidy_amount, 'CZK') . ' ' . __('(propojená žádost)')
                        : '' ?>
                </td>
                <td class="text-center" data-type="currency"><?= Number::currency($request_sum + $request_refund, 'CZK') ?></td>

                <?php if ($withNotifications) : ?>
                    <td class="text-center">
                        <?= $this->element(
                            'simple_color_indicator',
                            ['bool' => $request->hasNotifications(), 'tooltip' => 0]
                        ) ?></td>
                <?php endif; ?>
                <td class="text-center">
                    <?= $this->element(
                        'simple_color_indicator',
                        ['bool' => (!$request->hasUnsettledPayments() ? (count($request->getPayments()) > 0 ? true : false) : 3), 'tooltip' => 1]
                    ) ?></td>
                <td>
                    <?php
                    if (in_array($request->request_state_id, [
                        RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
                        RequestState::STATE_SETTLEMENT_SUBMITTED,
                        RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                        RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                        RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
                        RequestState::STATE_CLOSED_FINISHED,
                    ], true)) {
                        echo $this->Html->link(__('Evidence plateb'), ['action' => 'doPayout', $request->id], ['class' => 'btn btn-success']);
                    }
                    ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?= $this->Form->end(); ?>