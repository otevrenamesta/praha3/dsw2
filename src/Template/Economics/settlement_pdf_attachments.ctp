<?php

use App\Model\Entity\Request;
use App\Model\Entity\Settlement;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $settlement Settlement
 * @var $request Request
 */
echo '<!DOCTYPE html><html lang="cz"><head><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>';
?>

<div>
  <h3><?= __('Přílohy vyúčtování žádosti') . ' #' . $request->id ?></h3>
  <table cellspacing="0" cellpadding="5" border="1">
    <thead>
      <tr>
        <th><strong><?= __('Příloha č.') ?></strong></th>
        <th><strong><?= __('Název souboru') ?></strong></th>
        <th><strong><?= __('Typ') ?></strong></th>
        <th><strong><?= __('V tomto archivu') ?></strong></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($settlement->sortedAttachments() as $attachment) : ?>
        <tr>
          <td><?= $attachment->file_id ?></td>
          <td><?= strip_tags($attachment->file->original_filename) ?></td>
          <td><?= $attachment->settlement_attachment_type->type_name ?></td>
          <td><?= ($attachment->export ? __('Ano') : __('Ne (vytiskněte samostatně)')) ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<?php
echo '</body></html>';
