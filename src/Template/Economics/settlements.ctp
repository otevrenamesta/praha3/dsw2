<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\Model\Entity\SettlementType;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $states array
 * @var $defaultStates array
 * @var $settlements Settlement[]
 */

echo $this->element('simple_datatable');
echo $this->element('simple_select2');
$this->assign('title', __('Vyúčtování'));
?>
<div class="card" id="economics-filter">
    <div class="card-header">
        <h2><?= __('Přehled všech vyúčtování') ?></h2>
        <?= $this->Form->create(null, ['type' => 'get']) ?>
        <div class="row">
            <div class="col-md">
                <?= $this->Form->control(
                    'state_ids',
                    ['options' => $states, 'class' => 'select2', 'multiple' => true, 'label' => __('Stav vyúčtování'), 'empty' => true]
                ) ?>
                <?= $this->element('table_status_filter') ?>
                <?= $this->Form->submit(__('Filtrovat')) ?>
            </div>
            <div class="col-md">
                <?= $this->Form->control(
                    'appeal_id',
                    ['options' => $appeals, 'label' => __('Dotační výzva'), 'empty' => __('Všechny výzvy...')]
                ) ?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
    </div>

    <div class="card-body"><!-- table-responsive -->
        <table class="table table-bordered" id="dtable">
            <thead>
                <tr>
                    <!--<th><?= __('ID Vyúčtování') ?></th>-->
                    <th><?= __('ID Žádosti/ Vyúčtování') ?></th>
                    <th><?= __('Vlastní identifikátor') ?></th>
                    <th><?= __('Název žádosti') ?></th>
                    <th><?= __('Žadatel') ?></th>
                    <th><?= __('Částka k vyúčtování') ?></th>
                    <th><?= __('Částka vyúčtovaná') ?></th>
                    <th><?= __('Rozdíl') ?></th>
                    <th><?= __('Stav vyúčtování') ?></th>
                    <th><?= __('Typ vyúčtování') ?></th>
                    <th><?= __('Termín pro podání vyúčtování') ?></th>
                    <th><?= __('Datum podání') ?></th>
                    <th><?= __('Akce') ?></th>
                    <th><?= __('Zveřejnění') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($settlements as $settlement) : ?>
                    <?php
                    $request = false;
                    try {
                        $request = $settlement->getRequest();
                    } catch (Exception $e) {
                        //TODO: Log this
                    }
                    if (!$request || !$request->id) continue;
                    // payments
                    $request_sum = round(isset($request->konecna_castka) ? $request->konecna_castka : $request->getPaymentsSum(), 2);
                    $economics_payout = $request_sum;
                    $additionalFinancing = false;
                    if (!isset($request->konecna_castka)) {
                        foreach ($request->getPayments() as $key => $value) {
                            $economics_payout = $value->settlement_id === $settlement->id ? $value->amount_czk : $economics_payout;
                            if ($value->settlement_id === $settlement->id && $value->in_addition) {
                                $additionalFinancing = true;
                            }
                        }
                    }

                    // show 'expenses_total' field
                    $origItemsSum = 0;
                    $isExpensesTotal = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_EXPENSES_TOTAL_FIELD);
                    if ($isExpensesTotal) {
                        $economics_payout = $settlement->expenses_total ?? 0;
                        foreach ($settlement->settlement_items as $item) {
                            $origItemsSum += (float)$item->original_amount;
                        }
                    }

                    $request_amount = round($request instanceof Request ? $request->getFinalSubsidyAmount() : $request->konecna_castka, 2);
                    $request_refund = round($request instanceof Request ? $request->getPaymentsRefundSum() : 0, 2);
                    $settlement_sum = $isExpensesTotal ?  round($origItemsSum, 2) : round($settlement->getItemsSum(), 2);
                    $settlement_subt = $settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN ? 0 : $economics_payout - $settlement_sum;
                    ?>
                    <tr>
                        <!--<td><?= $settlement->id ?></td>-->
                        <td><a name="<?= $request->id ?>"></a><?= $request->id ?>/<?= $settlement->id ?></td>
                        <td><?= $settlement->getReferenceNumber() ?></td>
                        <td><?= $this->Html->link($settlement->getRequestName(), $settlement->getRequest()->getAdminDetailLink(), ['target' => '_blank']) ?></td>
                        <td><?= $settlement->getRequesterName() ?></td>
                        <td class="text-right" data-type="currency">
                            <?= Number::currency($economics_payout, 'CZK') ?>
                            <?= isset($request->merged_request) && $request->merged_request > 0
                                ? '(' . __('propojená žádost') . ' ' . Number::currency($economics_payout - $request->final_subsidy_amount, 'CZK') . ')'
                                : '' ?>
                        </td>
                        <td class="text-right" data-type="currency"><?= Number::currency($settlement_sum, 'CZK') ?></td>
                        <td class="text-right" data-type="currency"><?= Number::currency($settlement_subt, 'CZK') ?>
                            <?= $request_refund > 0 ? sprintf(" (%s: %s) ", __('vráceno'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
                            <?= $settlement_subt > 0 && $settlement_sum > 0 && (!$request_refund || (($request_refund > 0) && ($settlement_subt - ($request_amount - $request_sum)) > 0))
                                ? '<span class="badge bg-warning">' . __('Nedoúčtováno') .  '</span>'
                                : '' ?>
                            <?= $settlement_subt < 0
                                ? '<span class="badge bg-danger text-white">' . __('Přečerpáno') . '</span>'
                                : '' ?>
                            <?= $economics_payout > 0 && ((float)$settlement_subt === 0.0 || (($request_refund > 0) && ($settlement_subt - ($request_amount - $request_sum)) <= 0))
                                ? '<span class="badge bg-primary text-white">' . __('Vyúčtováno') . '</span>'
                                : '' ?>
                            <?= $economics_payout > 0 && (float)$settlement_sum === 0.0
                                ? '<span class="badge bg-info text-white">' .
                                ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN ? __('Vrácení') : __('Nevyúčtováno')) .
                                '</span>'
                                : '' ?>
                        </td>
                        <td><?= SettlementState::getLabel($settlement->settlement_state_id, true) ?></td>
                        <td>
                            <?= SettlementType::getLabel($settlement->settlement_type_id) ?>
                            <?= $additionalFinancing ? '<br />' . __('Dofinancování') : '' ?>
                        </td>
                        <td><?= $request->settlement_date ? date_format(date_create($request->settlement_date), 'd.m.Y') : '' ?></td>
                        <td><?= $settlement->submitted ? $settlement->submitted->format('d.m.Y') : "n/a" ?></td>
                        <td>
                            <?php
                            if ($settlement->settlement_state_id === SettlementState::STATE_SUBMITTED_READY_TO_REVIEW) {
                                echo $this->Html->link(__('Zkontrolovat vyúčtování'), ['action' => 'settlementDetail', 'settlement_id' => $settlement->id], ['class' => 'btn btn-success w-100']);
                            } else {
                                echo $this->Html->link(__('Prohlížet'), ['action' => 'settlementDetail', 'settlement_id' => $settlement->id], ['class' => 'btn btn-primary w-100']);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            $request = $settlement->getRequest();
                            if (get_class($request) == 'OldDsw\Model\Entity\Zadost') {
                                echo __('Vyúčtování z DSW1 nelze zveřejnit');
                            } else {
                                $pub = ($settlement->published == 0) ? '' : 'd-none ';
                                $unpub = ($settlement->published == 1) ? '' : 'd-none ';

                                echo $this->Html->link(__('Zveřejnit'), '/admin/opendata/' . $settlement->id . '/toggle', ['data-req-id' => $settlement->id, 'class' => $pub . 'pub-toggle-pub pub-toggle btn btn-primary btn-block']);
                                echo $this->Html->link(__('Zrušit'), '/admin/opendata/' . $settlement->id . '/toggle', ['data-req-id' => $settlement->id, 'class' => $unpub . 'pub-toggle-un pub-toggle btn btn-secondary btn-block']);
                                echo $this->Html->link(__('Anonymizace'),  '/admin/opendata/' . $settlement->id . '/anonymize', ['class' => 'btn btn-warning btn-block']);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $("a.pub-toggle").click(function(e) {
        e.preventDefault();
        let $reqId = $(this).attr('data-req-id');
        if ($(this).hasClass('pub-toggle-pub')) {
            $(this).next().removeClass('d-none');
            $(this).addClass('d-none');
        } else {
            $(this).prev().removeClass('d-none');
            $(this).addClass('d-none');
        }
        $.get("/admin/opendata/" + $reqId + "/toggle", function(data, status) {
            //console.log("Data: " + data + "\nStatus: " + status);
        });
    });
</script>