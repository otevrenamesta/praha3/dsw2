<?php

use App\Model\Entity\WikiCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $wikis WikiCategory[]
 */

$this->assign('title', __('Nápověda')) ?>
<h1><?= $this->fetch('title') ?></h1>
<div class="alert alert-info">
    <?= __('Kategorie i jednotlivé stránky v sekci nápovědy jsou řazeny podle abecedy. Konkrétní požadí můžete vynutit např. číslováním kategorií a stránek') ?>
</div>
<div class="alert alert-success">
    <?= sprintf(__('Zde vytvořené informace jsou dostupné na adrese %s i pro nepřihlášené uživatele'), $this->Html->link(['_name' => 'public_wiki', '_full' => true])) ?>
</div>

<h2><?= __('Seznam kategorií') ?></h2>
<?= $this->Html->link(__('Přidat novou kategorii'), ['action' => 'categoryAddModify'], ['class' => 'btn btn-success mb-2']) ?>
<table class="table">
    <thead>
    <tr>
        <th><?= __('Název kategorie') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($wikis as $category): ?>
        <tr>
            <td><?= $category->category_name ?></td>
            <td>
                <?= $this->Html->link(__('Upravit'), ['action' => 'categoryAddModify', 'id' => $category->id], ['class' => 'text-primary']) ?>
                ,
                <?= $this->Form->postLink(__('Smazat'), ['action' => 'categoryDelete', 'id' => $category->id], ['confirm' => __('Opravdu chcete smazat kategorii vč. obsažených stránek?'), 'class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<h2><?= __('Jednotlivé stránky podpory') ?></h2>
<?= $this->Html->link(__('Vytvořit novou stránku'), ['action' => 'addModify'], ['class' => 'btn btn-success mb-2']) ?>
<table class="table datatable">
    <thead>
    <tr>
        <th><?= __('Název stránky') ?></th>
        <th><?= __('Název kategorie') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($wikis as $category): ?>
        <?php foreach ($category->wikis as $page): ?>
            <tr>
                <td><?= $page->title ?></td>
                <td><?= $category->category_name ?></td>
                <td>
                    <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'id' => $page->id], ['class' => 'text-primary']) ?>
                    ,
                    <?= $this->Form->postLink(__('Smazat'), ['action' => 'delete', 'id' => $page->id], ['confirm' => __('Opravdu chcete smazat stránku?'), 'class' => 'text-danger']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>
