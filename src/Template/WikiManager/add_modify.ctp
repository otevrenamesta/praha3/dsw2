<?php
/**
 * @var $this AppView
 * @var $page Wiki
 * @var $allowed_categories array
 */

use App\Model\Entity\Wiki;
use App\View\AppView;

$this->assign('title', $page->isNew() ? __('Nová stránka podpory') : sprintf("%s: %s", __('Upravit'), $page->title));

echo $this->Form->create($page);
echo $this->Form->control('title', ['required' => 'required', 'maxlength' => 255]);
echo $this->Form->control('wiki_category_id', ['options' => $allowed_categories]);
echo $this->Form->control('contents', ['type' => 'textarea', 'rows' => 50]);
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();