<?php

use App\Model\Entity\WikiCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $category WikiCategory
 **/
$this->set('title', $category->isNew() ? __('Nová kategorie Wiki') : __('Upravit kategorii'));

echo $this->Form->create($category);
echo $this->Form->control('category_name');
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
