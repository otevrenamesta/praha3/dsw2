<?php

use App\Model\Entity\Request;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 */


$orgContactEmail = OrganizationSetting::getSetting(OrganizationSetting::CONTACT_EMAIL);
$orgEmailAllowed = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_EMAIL_SUBMIT_OF_REQUESTS, true);
$orgCodeRequired = OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, false);
$P4Exception =  OrganizationSetting::isException(OrganizationSetting::P4);
$P4ready = ($request->request_state_id == RequestState::STATE_READY_TO_SUBMIT);

if ($orgEmailAllowed) {
    $this->assign('title', __('Proces podání fyzicky na úřadě/emailem'));
} else {
    $this->assign('title', __('Proces podání fyzicky na úřadě'));
}

if ($orgCodeRequired) {
    $codeMsg = __('Úřad následně žádost přijme skrze kód uvedený na potvrzení.');
} else {
    $codeMsg = __('Úřad následně žádost na základě tohoto potvrzení přijme.');
}


?>
<div class="card m-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                <h2>
                    <?= $this->fetch('title') ?>
                </h2>
            </div>
            <div class="col-md text-right">
                <?php
                if ($request->canUserEditRequest()) {
                    echo $this->Html->link(
                        __('Otevřít žádost'),
                        ['action' => 'requestDetail', 'id' => $request->id],
                        ['class' => 'btn btn-success m-2']
                    );
                }
                ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="alert alert-info">
            <?php if ($P4Exception) : ?>
                <?php if ($P4ready) : ?>
                    <?= __('o odeslání žádosti se žádost uzamkne, dále ji nebude možné editovat a systém Vám umožní stáhnout:') ?>
                    <p>
                    <ol>
                        <li><?= __('“Potvrzení o elektronické evidenci žádosti”, které následně podepíšete') ?></li>
                        <li><?= __('Vámi kompletně vyplněnou žádost o dotaci, kterou následně podepíšete (na konci dokumentu je na to pole)') ?></li>

                    </ol>
                    </p>
                    <?= __('Tyto dva soubory vytisknete a společně s originály Vámi připravených povinných příloh dle pokynů daného dotačního programu/výzvy je doručíte fyzicky na podatelnu MČ Praha 4 (Antala Staška 2059/80b, 140 46 Praha 4), nebo prostřednictvím držitele poštovní licence, tak aby byla doručena nejpozději do lhůty pro podání žádostí, aby mohlo dojít k dalšímu zpracování Vaší žádosti.') ?>
                <?php else : ?>
                    <?= __('Žádost je nyní zamčena a odeslána ke zpracování dotačnímu úřadu. Nyní se očekává, že doručíte:') ?>
                    <p>
                    <ol>
                        <li><?= __('Podepsané “Potvrzení o elektronické evidenci žádosti”, které získáte pomocí tlačítka „Stáhnout Potvrzení o elektronické evidenci žádosti"') ?></li>
                        <li><?= __('Podepsanou kompletně vyplněnou žádost, kterou získáte pomocí tlačítka "Stáhnout žádost jako pdf“') ?></li>
                        <li><?= __('Originály Vámi připravených povinných příloh') ?></li>
                    </ol>
                    </p>
                    <?= __('fyzicky na podatelnu MČ Praha 4 (Antala Staška 2059/80b, 140 46 Praha 4), nebo prostřednictvím držitele poštovní licence, tak aby byla doručena nejpozději do lhůty pro podání žádostí, aby mohlo dojít k dalšímu zpracování Vaší žádosti.') ?>
                <?php endif ?>
            <?php else : ?>
                <?php if ($orgEmailAllowed) : ?>
                    <?=
                    __('Pokud nemáte datovou schránku, můžete tuto žádost podat pomocí Potvrzení o elektronické evidenci žádosti.') .
                        '<br/>' .
                        __('Uzamčením žádosti získáte Potvrzení. Toto potvrzení podepsané oprávněnou osobou je možné na úřad doručit dvěma způsoby:') .
                        '<p><ol>' .
                        '<li>' . __('Odevzdat potvrzení fyzicky na podatelně úřadu') .  '</li>' .
                        '<li>' . __('Naskenované podepsané potvrzení zaslat na email: ')  . '<a href="mailto:' . $orgContactEmail . '">' . $orgContactEmail . '</a></li>' .
                        '</ol></p>' .
                        $codeMsg .
                        '<br /><br /><p><em>' .
                        __('Pokud žádost odemknete, abyste provedli úpravy, nebo vám žádost bude vrácena pro formální nedostatky, musíte následně proces opakovat, a nové Potvrzení znovu odevzdat na podatelnu.') .
                        '</em></p>'

                    ?>
                <?php else : ?>
                    <?=
                    __('Pokud nemáte datovou schránku, můžete tuto žádost podat pomocí Potvrzení o elektronické evidenci žádosti.') .
                        '<br/>' .
                        __('Uzamčením žádosti získáte Potvrzení. Toto potvrzení podepsané oprávněnou osobou je možné na úřad doručit fyzicky.') . '<br/>' .
                        $codeMsg .
                        '<br /><br /><p><em>' .
                        __('Pokud žádost odemknete, abyste provedli úpravy, nebo vám žádost bude vrácena pro formální nedostatky, musíte následně proces opakovat, a nové Potvrzení znovu odevzdat na podatelnu.') .
                        '</em></p>'
                    ?>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md">
                <strong>
                    <?= __('Aktuální stav žádosti') ?>:
                </strong>
                <?= RequestState::getLabelByStateId($request->request_state_id) ?>
            </div>
        </div>
        <?php
        if ($P4Exception) {
            if ($request->canUserLockOwnRequest()) {
                echo $this->Html->link(
                    __('Odeslat žádost'),
                    ['do' => 'lock', 'id' => $request->id],
                    ['class' => 'btn btn-primary m-2'],
                );
            }
        } else {
            if ($request->canUserLockOwnRequest()) {
                echo $this->Html->link(
                    __('Uzamknout žádost a stáhnout Potvrzení o elektronické evidenci žádosti'),
                    ['do' => 'lockAndDownload', 'id' => $request->id],
                    ['class' => 'btn btn-info m-2']
                );
                echo $this->Html->link(
                    __('Pouze uzamknout žádost'),
                    ['do' => 'lock', 'id' => $request->id],
                    ['class' => 'btn btn-primary m-2'],
                );
            }
        }
        if ($request->canUserDownloadOwnVerificationPdf()) {
            echo $this->Html->link(
                __('Stáhnout Potvrzení o elektronické evidenci žádosti'),
                ['action' => 'downloadRequestVerificationPdf', 'id' => $request->id],
                ['class' => 'btn btn-success m-2']
            );
        }
        if ($P4Exception) {
            if ($request->canUserDownloadPdf() && $request->canUserUnlockOwnRequest()) {
                echo $this->Html->link(
                    __('Stáhnout žádost jako PDF'),
                    ['action' => 'getPdf', $request->id],
                    ['class' => 'btn btn-success m-2']
                );
            }
        } else {
            if ($request->canUserUnlockOwnRequest()) {
                echo $this->Html->link(
                    __('Odemknout žádost pro další úpravy'),
                    ['do' => 'unlock', 'id' => $request->id],
                    ['class' => 'btn btn-warning m-2'],
                );
            }
        }
        ?>
    </div>
</div>