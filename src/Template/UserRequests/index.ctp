<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\Settlement;
use App\View\AppView;
use OldDsw\Model\Entity\Zadost;

/**
 * @var $this AppView
 * @var $requests Request[]
 * @var $old_requests Zadost[]
 * @var $old_settlements Settlement[]
 * @var $shared_requests Request[] requests from accounts that share themself with current user
 */

$this->assign('title', __('Mé žádosti o podporu'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Vytvořit novou žádost o podporu'), ['action' => 'addModify'], ['class' => 'btn btn-success mb-2']) ?>
<hr />
<div class="row no-gutters">
    <?php
    foreach ($requests as $request) {
        echo $this->element('request_card', compact('request'));
    }
    ?>
</div>

<?php if (!empty($shared_requests)) : ?>
    <hr />
    <div class="card">
        <div class="card-header">
            <h3><?= __('Žádosti ze sdílených účtů') ?></h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><?= __('ID') ?></th>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Stav žádosti') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($shared_requests as $shared_request) : ?>
                        <tr>
                            <td><?= $shared_request->id ?></td>
                            <td><?= h($shared_request->name) ?></td>
                            <td><?= RequestState::getLabelByStateId($shared_request->request_state_id) ?></td>
                            <td>
                                <?= $this->Html->link(sprintf("%s (%s)", __('Přepnout účet'), $shared_request->user->email), ['_name' => 'switch_to_user', 'id' => $shared_request->user_id]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW) === true && !empty($old_requests)) : ?>
    <hr />
    <div class="card">
        <div class="card-header">
            <h3><?= __('Archiv žádostí') ?></h3>
        </div>
        <div class="card-body table-responsive">
            <?= $this->element('OldDsw.zadosti', ['zadosti' => $old_requests, 'vyuctovani' => $old_settlements]); ?>
        </div>
    </div>
<?php endif; ?>