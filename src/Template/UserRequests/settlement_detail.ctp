<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementAttachment;
use App\Model\Entity\SettlementAttachmentType;
use App\Model\Entity\SettlementItem;
use App\Model\Entity\SettlementState;
use App\Model\Entity\SettlementType;
use App\View\AppView;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use OldDsw\Model\Entity\Zadost;

/**
 * @var $this AppView
 * @var $attachmentTypes SettlementAttachmentType[]
 * @var $budget Array
 * @var $mergedRequest object
 * @var $program Program
 * @var $settlement Settlement
 * @var $settlement_attachment SettlementAttachment
 * @var $settlement_item SettlementItem
 */
/** @var Zadost|Request $request */

$request = $settlement->getRequest();

$this->assign('title', sprintf("%s #%d", $settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT
    ? __('Vyúčtování dotace')
    : __('Vrácení dotace'), $settlement->getRequest()->id));

// is economics/payout for this settlement/id?
$request_sum = round(isset($request->konecna_castka) ? $request->konecna_castka : $request->getPaymentsSum(), 2);
$economics_payout = $request_sum;
if (!isset($request->konecna_castka)) {
    foreach ($request->getPayments() as $key => $value) {
        $economics_payout = $value->settlement_id === $settlement->id ? $value->amount_czk : $economics_payout;
    }
}

// show 'expenses_total' field
$origItemsSum = 0;
$isExpensesTotal = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_EXPENSES_TOTAL_FIELD);
if ($isExpensesTotal) {
    $economics_payout = $settlement->expenses_total ?? 0;
    foreach ($settlement->settlement_items as $item) {
        $origItemsSum += (float)$item->original_amount;
    }
}

// payments
$request_amount = round($request instanceof Request ? $request->getFinalSubsidyAmount() : $request->konecna_castka, 2);
$request_refund = round($request instanceof Request ? $request->getPaymentsRefundSum() : 0, 2);
$settlement_sum = $isExpensesTotal ? round($origItemsSum, 2) : round($settlement->getItemsSum(), 2);
$settlement_subt = $economics_payout - $settlement_sum;

// budget settings
if (isset($budget) && !empty($budget)) {
    list(
        $budgetSectionsList,
        $budgetColumns,
        $budgetRequest,
        $budgetSectionsData,
        $budgetExceeded,
        $budgetExceededPartial,
        $budgetSectionsDataFull,
    ) = $budget;
}
$isBudget = (isset($budgetSectionsList) && !empty($budgetSectionsList));

// allowed actions
$notRequireAttachments = isset($program) ? boolval($program->not_require_tax_documents) : false;
$requestIsWaivedCompletely = $settlement->isRequestWaivedCompletely() || $notRequireAttachments;
$canEdit = $settlement->canUserEdit() &&
    $settlement->isSettlementOwner($this->getCurrentUser()) &&
    $this->getRequest()->getParam('controller') === 'UserRequests';
$settlementSum = ($settlement_sum > 0 && ($isExpensesTotal || $settlement_subt >= 0)) || $notRequireAttachments;
$canSubmit = !(isset($budgetExceeded) && boolval($budgetExceeded)) &&
    !(isset($budgetExceededPartial) && boolval($budgetExceededPartial)) &&
    (($settlementSum && $settlement->canUserSubmitSettlement($this->getCurrentUser())) ||
        ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN &&
            $settlement->isSettlementOwner($this->getCurrentUser())));
$canDelete = $settlement->canUserDelete() && $settlement->isSettlementOwner($this->getCurrentUser());

// validations
list($hasOneItem, $itemsHasRequiredAttachments, $hasFinalReport, $hasProofOfPublicity, $hasAgreementNo, $hasNoUnusedAttachments) = $settlement->getValidations();

// the approval
$approvalProcess = $this->getRequest()->getParam('controller') !== 'UserRequests' && $settlement->settlement_state_id === SettlementState::STATE_SUBMITTED_READY_TO_REVIEW;
$approvalProcessView = $this->getRequest()->getParam('controller') !== 'UserRequests' && $settlement->settlement_state_id !== SettlementState::STATE_ECONOMICS_APPROVED;
$repairProcess = $this->getRequest()->getParam('controller') === 'UserRequests' && $settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES;

// attachments
$invoiceAttachmentTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_1_INVOICES);
$proofTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_2_PROOF);
$invoiceAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_1_INVOICES, true);
$proofAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_2_PROOF, true);
$returnAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_2_PROOF, false);
$unusedAttachments = !empty($settlement->getUnusedAttachments());

// states
$states = SettlementState::getKnownStatesWithLabels();

// merged requests
$isMergedRequest = $mergedRequest && $mergedRequest->id > 0 && $mergedRequest->final_subsidy_amount > 0;
$mergedRequestAmount = $isMergedRequest ? (float)$mergedRequest->final_subsidy_amount : 0;

// additional columns if enabled in organization settings
$addColumns = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_ADDITIONAL_COLUMNS);

echo $this->element('filesize_check');
?>
<div class="card m-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h2><?= $this->fetch('title') ?></h2>
                <?php
                if ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN && $settlement->isSettlementOwner($this->getCurrentUser())) {
                    echo $this->Form->create($settlement);
                    echo $this->Form->hidden('settlement_type_id', ['value' => SettlementType::STANDARD_SETTLEMENT]);
                    echo $this->Form->hidden('return_reason', ['value' => '']);
                    echo $this->Form->submit(__('Vyúčtovat dotaci, místo vrácení celé částky'));
                    echo $this->Form->end();
                }
                ?>
            </div>
            <div class="col-md text-right">
                <?= sprintf("%s: %s", __('Aktuální stav vyúčtování'), $states[$settlement->settlement_state_id]) ?> <br />
                <?php
                if ($settlement->canUserGetPdf() && $settlement->isSettlementOwner($this->getCurrentUser())) {
                    echo $this->Html->link(__('Stáhnout PDF'), ['controller' => 'UserRequests', 'action' => 'settlementPdf', 'settlement_id' => $settlement->id], ['class' => 'm-2 btn btn-success']);
                }

                echo $this->Html->link(
                    __('Odeslat vyúčtování Datovou schránkou'),
                    ['controller' => 'UserRequests', 'action' => 'settlementSubmitDatabox', 'settlement_id' => $settlement->id],
                    ['disabled' => !$canSubmit, 'class' => 'm-2 btn btn-info ' . ($canSubmit ? '' : 'disabled')]
                );
                echo $this->getSiteSetting(OrganizationSetting::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND) ? '' : $this->Html->link(
                    __('Odeslat vyúčtování jinak'),
                    ['controller' => 'UserRequests', 'action' => 'settlementSubmitDirectly', 'settlement_id' => $settlement->id],
                    ['disabled' => !$canSubmit, 'class' => 'm-2 btn btn-info ' . ($canSubmit ? '' : 'disabled')]
                );

                echo $this->Html->link(__('Smazat vyúčtování'), [
                    'controller' => 'UserRequests',
                    'action' => 'settlementDelete',
                    'settlement_id' => $settlement->id
                ], [
                    'disabled' => !$canDelete,
                    'class' => 'ml-2 btn btn-danger ' . ($canDelete ? '' : 'disabled'),
                    'confirm' => __('Opravdu chcete nevratně smazat tuto vyúčtování?')
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?php
        if ($settlement->isSettlementOwner($this->getCurrentUser()) && in_array(
            $settlement->settlement_state_id,
            [SettlementState::STATE_SUBMITTED_READY_TO_REVIEW, SettlementState::STATE_ECONOMICS_APPROVED, SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES, SettlementState::STATE_SETTLEMENT_REJECTED]
        )) {
            $alert_color = $settlement->settlement_state_id == SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES || $settlement->settlement_state_id == SettlementState::STATE_SETTLEMENT_REJECTED ? 'danger' : 'success';
            $text = null;
            switch ($settlement->settlement_state_id) {
                case SettlementState::STATE_SUBMITTED_READY_TO_REVIEW:
                    $text = __('Vyúčtování bylo úspěšně podáno, vyčkejte prosím až bude zpracováno');
                    break;
                case SettlementState::STATE_ECONOMICS_APPROVED:
                    $text = __('Vyúčtování bylo schváleno');
                    if (!empty($settlement->econom_statement)) {
                        $text .= '<hr/>';
                        $text .= $settlement->econom_statement;
                    }
                    break;
                case SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES:
                    $text = __('Vyúčtování bylo vráceno k opravě');
                    $text .= '<hr/>';
                    $text .= $settlement->econom_statement;
                    if (!empty($settlement->repair_date)) {
                        $text .= sprintf(
                            "<br />%s: %s",
                            __('Datum, do kdy má žadatel čas vyúčtování opravit'),
                            $settlement->repair_date->nice()
                        );
                    }
                    break;
                case SettlementState::STATE_SETTLEMENT_REJECTED:
                    $text = __('Vyúčtování nevyhovuje podmínkám');
                    $text .= '<hr/>';
                    $text .= $settlement->rejected_statement;
                    break;
            }
        ?>
            <div class="alert alert-<?= $alert_color ?>">
                <?= $text ?>
            </div>
        <?php
        }
        ?>
        <?= sprintf("%s: <strong>%s</strong>", __('Název projektu'), ($request instanceof Request ? $request->name : $request->nazev)) ?><br />
        <?= isset($program) && isset($program->name) && !empty($program->name) ? sprintf("%s: %s", __('Program podpory'), $program->name) . '<br />' : '' ?>
        <?= sprintf("%s: %s", __('Žadatel'), $settlement->getRequesterName()) ?><br />
        <hr />
        <?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
            <?= sprintf("%s: %s", __('Schválená podpora celkem'), Number::currency($request_amount + $mergedRequestAmount, 'CZK')) ?><br />
            <?php
            if (!$isMergedRequest && isset($request->original_subsidy_amount) && !empty($request->original_subsidy_amount)) {
                echo '<ul>';
                echo '<li>' . sprintf("%s: %s", __('Původní podpora celkem'), Number::currency($request->original_subsidy_amount, 'CZK')) . '</li>';
                echo '<li>' . sprintf("%s: %s", __('Dofinancování (navýšení původně schválené podpory)'), Number::currency($request->final_subsidy_amount - $request->original_subsidy_amount, 'CZK')) . '</li>';
                echo '</ul>';
            } else if ($isMergedRequest) {
                echo '<ul>';
                echo '<li>' . __('Schválená podpora "této" žádosti') . ': ' . Number::currency($request->final_subsidy_amount, 'CZK') . '</li>';
                echo '<li>' . __('Schválená podpora "propojené" žádosti') . ' (#' . $mergedRequest->id . ', ' . $mergedRequest->name . '): ' . Number::currency($mergedRequest->final_subsidy_amount, 'CZK') . '</li>';
                echo '</ul>';
            }
            ?>
            <?= sprintf("%s: %s", __('Vyplacená podpora'), Number::currency($request_sum + $request_refund, 'CZK')) ?><br />
            <?= $request_refund > 0 ? sprintf("%s: %s", __('Vrácená podpora'), Number::currency($request_refund, 'CZK')) . '<br />' : '' ?>
            <?= sprintf("%s: %s", __('Nečerpaná podpora'), Number::currency($request_amount - $request_sum - $request_refund + $mergedRequestAmount, 'CZK')) ?>
            <?= $request_refund > 0 ? sprintf(" (%s: %s)", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?><br />
            <?= sprintf("%s: %s", __('Čerpaná podpora celkem'), Number::currency($request_sum, 'CZK')) ?><br />
            <hr />
            <?= sprintf("%s: %s", $isExpensesTotal ? __('Celková výše nákladů na projekt') : __('Částka k čerpání v tomto vyúčtování'), Number::currency($economics_payout, 'CZK')) ?><br />
            <?= sprintf("%s: %s", __('Částka vyúčtovaná'), Number::currency($settlement_sum, 'CZK')) ?><br />
            <?= sprintf("%s: %s", __('Částka nevyúčtovaná'), Number::currency($settlement_subt, 'CZK')) ?>
            <?= $request_refund > 0 ? sprintf(" (%s: %s) ", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
            &nbsp;
            <?= $settlement_subt > 0 && $settlement_sum > 0 && (!$request_refund || (($request_refund > 0) && ($settlement_subt - ($request_amount - $request_sum)) > 0))
                ? '<span class="badge bg-warning">' . __('Nedoúčtováno') . '</span>'
                : '' ?>
            <?= $settlement_subt < 0
                ? '<span class="badge bg-danger text-white">' . __('Přečerpáno') . '</span>'
                : '' ?>
            <?= $economics_payout > 0 && ((float)$settlement_subt === 0.0 || (($request_refund > 0) && ($settlement_subt - ($request_amount - $request_sum)) <= 0))
                ? '<span class="badge bg-primary text-white">' . __('Vyúčtováno') . '</span>'
                : '' ?>
            <?= $economics_payout > 0 && (float)$settlement_sum === 0.0
                ? '<span class="badge bg-info text-white">' .
                ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN ? __('Vrácení') : __('Nevyúčtováno/přečerpáno')) .
                '</span>'
                : '' ?>
        <?php else : ?>
            <?= sprintf("%s: %s", __('Celkem nevyčerpáno z dotace'), Number::currency($settlement->getRequestFinalAmount(), 'CZK')) ?>
        <?php endif; ?>
        <br />
        <?php
        if (isset($budgetExceeded) && boolval($budgetExceeded)) {
            echo '<hr />' . sprintf("%s: &nbsp; <span class='badge bg-danger text-white'>%s</span><br />", __('Vyúčtované náklady vzhledem k rozpočtu'), __('Přečerpáno'));
        }
        if ($canEdit) {
            $warning = OrganizationSetting::getSetting($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT
                ? OrganizationSetting::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS
                : OrganizationSetting::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY);
            echo $warning ? '<hr /><div class="alert alert-warning text-dark font-weight-bold">' . $warning . '</div>' : '';
        }
        ?>
        <?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
            <hr />
            <p><strong><?= __('Náležitosti vyúčtování ke kontrole') ?></strong><br /></p>
            <?= __('Povinné náležitosti vyúčtování') ?>:
            <ol>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $hasAgreementNo]) . __('Číslo veřejnoprávní smlouvy bylo vyplněno') ?></li>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => ($hasOneItem || $requestIsWaivedCompletely)]) .
                        ($requestIsWaivedCompletely ? __('Vyúčtování nemusí obsahovat ani 1 položku') : __('Vyúčtování obsahuje alespoň 1 položku')) ?></li>
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => ($itemsHasRequiredAttachments || $requestIsWaivedCompletely)]) .
                        ($requestIsWaivedCompletely ? __('Položky vyúčtování jsou v pořádku') : __('Každá položka obsahuje alespoň účetní nebo daňový doklad a výdajový pokladní doklad nebo výpis z bankovního účtu')) ?></li>
                <li>
                    <?= $this->element('simple_checkmark_indicator', ['bool' => $hasFinalReport]) . __('Vyúčtování obsahuje Závěrečnou zprávu') ?>
                    <?php
                    if (!$hasFinalReport) {
                        echo '<br/>' . $this->Html->link(__('Nahrát závěrečnou zprávu'), ['_name' => 'settlement_new_attachment_report', 'settlement_id' => $settlement->id], ['class' => 'btn btn-primary']);
                    }
                    ?>
                </li>
                <li>
                    <?= $this->element('simple_checkmark_indicator', ['bool' => $hasProofOfPublicity]) . __('Vyúčtování obsahuje alespoň 1 přílohu, doklad o publicitě projektu') ?>
                    <?php
                    if (!$hasProofOfPublicity) {
                        echo '<br/>' . $this->Html->link(__('Nahrát doklad o publicitě projektu'), ['_name' => 'settlement_new_attachment_publicity', 'settlement_id' => $settlement->id], ['class' => 'btn btn-primary']);
                    }
                    ?>
                </li>
            </ol>
            <?= __('Ostatní náležitosti') ?>:
            <ol start="6">
                <li><?= $this->element('simple_checkmark_indicator', ['bool' => $hasNoUnusedAttachments, 'warning' => $unusedAttachments]) .
                        __('Vyúčtování obsahuje nepoužité přílohy') ?>
                </li>
            </ol>
        <?php endif; ?>

        <?php
        if (!$canEdit && isset($settlement->agreement_no) && !empty($settlement->user_comment)) {
            echo '<div class="alert alert-primary"> ' .
                sprintf("%s:", __('Komentář žadatele k vyúčtování')) .
                '<ul><li>' . sprintf("%s", $settlement->user_comment) . '</li></ul>' .
                '</div>';
        } ?>
    </div>
    <div class="card-footer">
        <?php
        if ($canEdit) {
            echo $this->Form->create($settlement);
            echo $this->Form->control('agreement_no', ['label' => __('Číslo veřejnoprávní smlouvy'), 'placeholder' => __('Číslo veřejnoprávní smlouvy')]);
            echo $this->Form->control('user_comment', ['label' => __('Komentář žadatele k vyúčtování'), 'type' => 'textarea', 'data-noquilljs' => 'data-noquilljs']);
            echo ($isBudget || $isExpensesTotal)
                ? $this->Form->control('expenses_total', ['type' => 'number', 'step' => .01, 'min' => 0, 'label' => __('Náklady celkem – skutečně vynaložené celkové náklady na projekt'), 'default' => '0'])
                : '';
            echo $this->Form->button(__('Uložit'), ['type' => 'submit', 'class' => 'btn btn-success']);
            echo $this->Form->end();
        } else {
            echo sprintf("%s: %s", __('Číslo veřejnoprávní smlouvy'), $settlement->agreement_no);
        }
        ?>
    </div>
</div>

<?php if ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN) : ?>
    <div class="card m-2">
        <h2 class="card-header">
            <?= __('Důvod vrácení poskytnuté podpory v plné výši') ?>
        </h2>
        <div class="card-body">
            <?php
            if ($canEdit) {
                echo $this->Form->create($settlement);
                echo $this->Form->control('return_reason', ['label' => __('Důvod vrácení poskytnuté podpory v plné výši'), 'required' => true, 'data-noquilljs' => 'data-noquilljs']);
                echo $this->Form->submit(__('Uložit'));
                echo $this->Form->end();
            } else {
            ?>
                <p>
                    <?= $settlement->return_reason ?>
                </p>
            <?php } ?>
        </div>
        <div class="card-footer">
            <?php
            if ((!$canEdit) && count($returnAttachments)) {
                foreach ($returnAttachments as $attachment) {
                    echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]);
                    echo $settlement->printFormatAttachments([$attachment]);
                }
            }
            ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($canEdit) : ?>
    <div class="card m-2">
        <h2 class="card-header"><?= __('Odeslání vyúčtování') ?></h2>
        <div class="card-body">
            <div class="alert alert-warning text-dark font-weight-bold">
                <?= OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH, true) ?>
            </div>

            <?php echo $this->Html->link(
                __('Odeslat vyúčtování Datovou Schránkou'),
                ['controller' => 'UserRequests', 'action' => 'settlementSubmitDatabox', 'settlement_id' => $settlement->id],
                ['disabled' => !$canSubmit, 'class' => 'btn btn-info ' . ($canSubmit ? '' : 'disabled')]
            ) ?>
            <?php echo $this->getSiteSetting(OrganizationSetting::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND) ? '' : $this->Html->link(
                __('Odeslat vyúčtování jinak'),
                ['controller' => 'UserRequests', 'action' => 'settlementSubmitDirectly', 'settlement_id' => $settlement->id],
                ['disabled' => !$canSubmit, 'class' => 'btn btn-info ' . ($canSubmit ? '' : 'disabled')]
            ) ?>

            <?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
                <hr />

                <div class="alert alert-danger text-dark font-weight-bold">
                    <?= OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY, true) ?>
                </div>
                <?php echo $this->Html->link(__('Vrátit dotaci v plné výši'), ['controller' => 'UserRequests', 'action' => 'settlementReturn', 'settlement_id' => $settlement->id], ['class' => 'btn btn-danger']) ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($isBudget) : ?>
    <div class="card m-2 mt-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <h2><?= __('Položky rozpočtu') ?></h2>
                </div>
            </div>
        </div>
        <div class="card-body table-responsive pb-0">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Položky nákladů') ?></th>
                        <th class="text-right"><?= __('Částka požadovaná v žádosti') ?></th>
                        <th class="text-right"><?= __('Náklady vyúčtované') ?></th>
                        <th class="text-right"><?= __('Nevyúčtováno/přečerpáno') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($budgetSectionsDataFull as $item) : ?>
                        <tr>
                            <td><?= $item['title'] ?></td>
                            <td class="text-right"><?= Number::currency($item['total2'], 'CZK') ?></td>
                            <td class="text-right"><?= Number::currency($item['sum'], 'CZK') ?></td>
                            <td class="text-right<?= $item['diff'] < 0 ? ' text-danger' : '' ?>"><?= Number::currency($item['diff'], 'CZK') ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <hr />
        <?php if (!$canEdit) : ?>
            <div class="card-body table-responsive pb-0">
                <table class="table table-bordered table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th><?= __('Náklady celkem – skutečně vynaložené celkové náklady') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="pl-3"> <?php echo Number::currency($settlement->expenses_total, 'CZK') ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <?php foreach ($budgetColumns as $column) : ?>
                            <th class="text-right"><?= $column ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($budgetColumns as $column => $value) : ?>
                            <td class="text-right">
                                <?= isset($budgetRequest) && isset($budgetRequest[$column])
                                    ? (strpos($value, '%') ? Number::toPercentage($budgetRequest[$column]) : Number::currency($budgetRequest[$column], 'CZK'))
                                    : '' ?>
                                <?= ($column === 'diff_' && $budgetRequest['diff_'] > $budgetRequest['max_']) ||
                                    ($column === 'sum_' && $budgetRequest['sum_'] > $budgetRequest['requested_amount'])
                                    ? '<br /><span class="text-danger">' . __('Překročeno') . '</span>'
                                    : '' ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
<?= $approvalProcess ? $this->Form->create() . $this->Form->hidden('approvalProcess', ['value' => 'approvalProcess']) : '' ?>

<?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <h2><?= __('Položky vyúčtování') ?></h2>
                </div>
                <?php if ($canEdit) : ?>
                    <div class="col-md text-right">
                        <?= $this->Html->link(__('Vytvořit novou položku'), ['_name' => 'create_settlement_item', 'settlement_id' => $settlement->id], ['class' => 'btn btn-success']) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Položka č.') ?></th>
                        <?php if (!$canEdit) : ?>
                            <th><?= __('Technické č.') ?></th>
                        <?php endif; ?>
                        <th><?= __('Datum uhrazení') ?></th>
                        <th><?= __('Popis položky') ?></th>
                        <th><?= __('Účetní nebo daňový doklad') ?></th>
                        <th><?= __('Výpis z bankovního účtu / Výdajový pokladní doklad') ?></th>
                        <th><?= __('Ostatní přílohy') ?></th>
                        <th class="text-right"><?= __('Částka dle dokladu') ?></th>
                        <th class="text-right"><?= __('Z toho čerpáno z dotace') ?></th>
                        <?= $addColumns ? '<th class="text-right">' . __('Hrazeno z vlastních zdrojů') . '</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' . __('Částka, která není k dotaci vůbec uplatňována') . '</th>' : '' ?>
                        <?php if ($canEdit) : ?>
                            <th><?= __('Akce') ?></th>
                        <?php endif; ?>
                        <?php if ($approvalProcess || $repairProcess || $approvalProcessView) : ?>
                            <th><?= __('Schvalování') ?></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <?php
                $totalAmount = 0;
                $totalOrigAmount = 0;
                $totalOwnAmount = 0;
                ?>

                <tbody>
                    <?php foreach ($settlement->settlement_items as $item) : ?>
                        <tr>
                            <td><?= $item->order_number ?></td>
                            <?php if (!$canEdit) : ?>
                                <td><?= $item->id ?></td>
                            <?php endif; ?>
                            <td><?= $item->paid_when->format('d. m. Y') ?></td>
                            <td>
                                <?= isset($budgetSectionsList) && !empty($item->budget_item) && isset($budgetSectionsList[$item->budget_item]) ? $budgetSectionsList[$item->budget_item] . '<hr />' : '' ?>
                                <?= $item->description ?>
                                <?php if (!empty($item->note)) : ?>
                                    <hr />
                                    <?= $item->note ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_1_INVOICES) as $attachmentByType) {
                                    echo str_replace(' - ', '<br/>', $attachmentByType->getLabel());
                                    echo $this->Html->link(
                                        '<i class="fas fa-download ml-2 mr-1"></i>',
                                        ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        ['escapeTitle' => false, 'title' => __('Stáhnout')]
                                    );
                                    echo $this->Html->link(
                                        '<i class="fas fa-file ml-1 mr-2"></i>',
                                        ['action' => 'settlementViewFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        [
                                            'escapeTitle' => false,
                                            'title' => __('Zobrazit'),
                                            'target' => '_blank'
                                        ]
                                    );
                                    echo '<hr/>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_2_PROOF) as $attachmentByType) {
                                    echo $attachmentByType->getLabel();
                                    echo $this->Html->link(
                                        '<i class="fas fa-download ml-2 mr-1"></i>',
                                        ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        ['escapeTitle' => false, 'title' => __('Stáhnout')]
                                    );
                                    echo $this->Html->link(
                                        '<i class="fas fa-file ml-1 mr-1"></i>',
                                        ['action' => 'settlementViewFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        [
                                            'escapeTitle' => false,
                                            'title' => __('Zobrazit'),
                                            'target' => '_blank'
                                        ]
                                    );
                                    echo '<hr/>';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_3_OTHERS) as $attachmentByType) {
                                    echo $attachmentByType->getLabel();
                                    echo $this->Html->link(
                                        '<i class="fas fa-download ml-2 mr-1"></i>',
                                        ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        ['escapeTitle' => false, 'title' => __('Stáhnout')]
                                    );
                                    echo $this->Html->link(
                                        '<i class="fas fa-file ml-1 mr-1"></i>',
                                        ['action' => 'settlementViewFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id],
                                        [
                                            'escapeTitle' => false,
                                            'title' => __('Zobrazit'),
                                            'target' => '_blank'
                                        ]
                                    );
                                    echo '<hr/>';
                                }
                                ?>
                            </td>
                            <td class="text-right"><?= Number::currency($item->original_amount, 'CZK') ?></td>
                            <td class="text-right"><?= Number::currency($item->amount, 'CZK') ?></td>

                            <?= $addColumns ? '<td class="text-right">' . Number::currency($item->own_amount, 'CZK') . '</td>' : '' ?>
                            <?= $addColumns ? '<td class="text-right">' . Number::currency($item->original_amount - $item->amount - $item->own_amount, 'CZK') . '</td>' : '' ?>

                            <?php
                            $totalAmount += (float)$item->amount;
                            $totalOrigAmount += (float)$item->original_amount;
                            $totalOwnAmount += (float)$item->own_amount;
                            ?>
                            <?php if ($canEdit) : ?>
                                <td>
                                    <?= !$repairProcess || ($repairProcess && !$item->approved) ?
                                        $this->Html->link(
                                            __('Upravit'),
                                            ['controller' => 'UserRequests', 'action' => 'settlementItemAddModify', 'settlement_id' => $item->settlement_id, 'id' => $item->id]
                                        )
                                        : '' ?>
                                    <?= !$repairProcess // || ($repairProcess && !$item->approved)
                                        ? $this->Form->postLink(
                                            __('Smazat'),
                                            ['controller' => 'UserRequests', 'action' => 'settlementItemDelete', 'settlement_id' => $item->settlement_id, 'id' => $item->id],
                                            ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto položku')]
                                        )
                                        : '' ?>
                                </td>
                            <?php endif ?>
                            <?php if ($approvalProcess || $repairProcess || $approvalProcessView) : ?>
                                <td>
                                    <?php
                                    if ($approvalProcess) {
                                        echo $this->Form->control('approved[]', ['type' => 'checkbox', 'checked' => boolval($item->approved), 'value' => $item->id, 'label' => __('Příloha schválena')]);
                                        echo $this->Form->control('approved_note:' . $item->id, ['type' => 'text', 'value' => $item->approved_note ?? '', 'label' => __('Důvod neschválení'), 'maxlength' => 255]);
                                    } else if ($repairProcess || $approvalProcessView) {
                                        echo $item->approved
                                            ? '<span class="text-success">' . __('Schváleno') . '</span>' : '<span class="text-danger">' . __('Neschváleno') . '</span>';
                                        echo !$item->approved && !empty($item->approved_note)
                                            ? sprintf("<hr />%s", $item->approved_note) : '';
                                        echo $approvalProcessView && $item->approved && !empty($item->approved_note)
                                            ? sprintf("<hr /><em style='color:#aaa;'>%s</em>", $item->approved_note) : '';
                                    }
                                    ?>
                                </td>
                            <?php endif ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (empty($settlement->settlement_items)) : ?>
                        <tr>
                            <td colspan="9" class="text-center"><?= __('Vyúčtování neobsahuje žádné položky') ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="<?= $canEdit ? '6' : '7' ?>"><?= __('Celkem') ?></th>
                        <th class="text-right"><?= Number::currency($totalOrigAmount, 'CZK') ?></th>
                        <th class="text-right"><?= Number::currency($totalAmount, 'CZK') ?></th>
                        <?= $addColumns ? '<th class="text-right">' . Number::currency($totalOwnAmount, 'CZK') . '</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' . Number::currency($totalOrigAmount - $totalAmount - $totalOwnAmount, 'CZK') . '</th>' : '' ?>
                        <?= ($canEdit ? '<th />' : '') ?>
                        <?= ($approvalProcess || $repairProcess || $approvalProcessView ? '<th />' : '') ?>
                    </tr>
                    <tr class="thead-dark">
                        <?php if ($isExpensesTotal) {
                            $totalOrigAmount = $economics_payout;
                        } ?>
                        <th colspan="<?= $canEdit ? '6' : '7' ?>"><?= $isExpensesTotal ? __('Podíl z celkové výše nákladů na projekt ') . Number::currency($economics_payout, 'CZK') : __('Podíl') ?></th>
                        <th class="text-right"><?= $isExpensesTotal ? '' : (number_format(100, 2) . '%') ?></th>
                        <th class="text-right"><?= number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2) ?> %</th>
                        <?= $addColumns ? '<th class="text-right">' . number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2) . ' %</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' . number_format(
                            100 - number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2)
                                - number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2),
                            2
                        ) . ' %</th>' : '' ?>
                        <?= $canEdit ? '<th />' : '' ?>
                        <?= $repairProcess  ? '<th />' : '' ?>
                        <?= $approvalProcessView && !$approvalProcess  ? '<th />' : '' ?>
                        <?= $approvalProcess
                            ? '<th>' . $this->Form->submit(__('Uložit schvalování'), ['class' => 'btn btn-success float-right']) . '</th>'
                            : '' ?>
                    </tr>
                </tfoot>
            </table>
        </div>
        <?php if ($canEdit) : ?>
            <div class="card-footer" id="fileUpload">
                <strong><?= __('Přidat novou položku vyúčtování') ?></strong> <br />
                <div class="alert alert-info">
                    <?= __('Při platbě faktury v hotovosti je nutné doložit fakturu + výdajový pokladní doklad, v případě bezhotovostní platby je nutné doložit fakturu + výpis z bankovního účtu (případně potvrzení o provedení platby)') ?>
                </div>
                <?php
                echo $this->Form->create($settlement_item, ['type' => 'file', 'url' => ['_name' => 'create_settlement_item', 'settlement_id' => $settlement->id]]);
                ?>
                <?php if (isset($budgetSectionsList) && count($budgetSectionsList) > 0) : ?>
                    <div class="row">
                        <div class="col">
                            <?= $this->Form->control('budget_item', ['options' => $budgetSectionsList, 'label' => __('Vyberte prosím položku z rozpočtu'), 'required' => true, 'class' => 'attachment-type']) ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= $this->Form->control('description', ['label' => __('Popis položky')]); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $this->Form->control('original_amount', ['type' => 'number', 'step' => .01, 'min' => 0, 'label' => __('Částka dle dokladu (Kč)'), 'default' => '0']); ?>
                    </div>
                    <div class="col-md-2">
                        <?= $this->Form->control('amount', ['type' => 'number', 'step' => .01, 'label' => __('Z toho čerpáno z dotace (Kč)'), 'default' => '0']); ?>
                    </div>
                    <?= $addColumns
                        ? '<div class="col-md-2">' . $this->Form->control('own_amount', ['type' => 'number', 'step' => .01, 'min' => 0, 'label' => __('Hrazeno z vlastních zdrojů (Kč)'), 'default' => '0']) . '</div>'
                        : '<div class="col-md-2"></div>' ?>
                    <div class="col-md-2">
                        <?= $this->Form->control('paid_when', ['type' => 'date', 'label' => __('Datum uhrazení'), 'required' => true, 'default' => false]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="<?= count($invoiceAttachmentTypes) > 1 ? 'col-md-8' : 'col-md' ?> <?= empty($invoiceAttachments) ? ' pt-2rem' : '' ?>">
                        <div class="alert alert-info">
                            <?= __('Pokud obsahuje nahrávaný dokument více stran, je nutné všechny naskenované stránky daného dokumentu nejprve spojit do jednoho dokumentu a ten poté nahrát jako jeden soubor s více stranami.') ?>
                        </div>
                        <?= $this->Form->control('settlement_attachments.0.filedata', ['type' => 'file', 'label' => __('Účetní nebo daňový doklad / faktura / účtenka / paragon / mzdový doklad / smlouva'), 'required' => !$notRequireAttachments]); ?>
                        <?php
                        if (!empty($invoiceAttachments)) {
                            echo $this->Form->control('settlement_attachments.0.existing_id', ['type' => 'select', 'label' => __('Účetní nebo daňový doklad / faktura / účtenka / paragon / mzdový doklad / smlouva'), 'options' => $invoiceAttachments, 'class' => 'existing-attachments']);
                            echo $this->Form->control('settlement_attachments.0.select_existing', ['type' => 'checkbox', 'label' => __('Vybrat z již nahraných příloh'), 'class' => 'attachment-switch']);
                        }
                        ?>
                    </div>
                    <div class="col-md-4 <?= count($invoiceAttachmentTypes) > 1 ? '' : 'd-none' ?>">
                        <?= $this->Form->control('settlement_attachments.0.settlement_attachment_type_id', ['options' => $invoiceAttachmentTypes, 'label' => __('Vyberte prosím typ nahrávaného souboru'), 'required' => true, 'class' => 'attachment-type']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="<?= count($proofTypes) > 1 ? 'col-md-8' : 'col-md' ?> <?= empty($proofAttachments) ? ' pt-2rem' : '' ?>">
                        <?= $this->Form->control('settlement_attachments.1.filedata', ['type' => 'file', 'label' => __('Výpis z bankovního účtu / Výdajový pokladní doklad'), 'required' => !$notRequireAttachments]); ?>
                        <?php
                        if (!empty($proofAttachments)) {
                            echo $this->Form->control('settlement_attachments.1.existing_id', ['type' => 'select', 'label' => __('Výpis z bankovního účtu / Výdajový pokladní doklad'), 'options' => $proofAttachments, 'class' => 'existing-attachments']);
                            echo $this->Form->control('settlement_attachments.1.select_existing', ['type' => 'checkbox', 'label' => __('Vybrat z již nahraných příloh'), 'class' => 'attachment-switch']);
                        }
                        ?>
                    </div>
                    <div class="col-md-4 <?= count($proofTypes) > 1 ? '' : 'd-none' ?>">
                        <?= $this->Form->control('settlement_attachments.1.settlement_attachment_type_id', ['options' => $proofTypes, 'label' => __('Vyberte prosím typ nahrávaného souboru'), 'class' => 'attachment-type', 'required' => true]) ?>
                    </div>
                </div>
                <?php
                echo $this->Form->submit(__('Uložit'));
                echo $this->Form->end();
                ?>
                <script type="text/javascript">
                    $(function() {
                        $(".attachment-switch").change(function() {
                            let selected = $(this).is(':checked');
                            let $row = $(this).closest('.row');
                            <?= $notRequireAttachments ? '' : "$(\"input[type=file]\", $" . "row).prop('required', !selected).closest('.form-group').toggleClass('required', !selected).toggle(!selected);" ?>
                            $("select.existing-attachments", $row).prop('required', selected).closest('.form-group').toggleClass('required', selected).toggle(selected);
                            $("select.attachment-type", $row).prop('required', !selected).closest('.form-group').toggleClass('required', !selected).closest('.col-md-4').toggle(!selected);
                            $('.col-md-8, .col-md', $row).toggleClass('pt-2rem', !selected);
                        }).change();
                    });
                </script>
            </div>
        <?php endif; ?>
    </div>

    <div class="card m-2">
        <h2 class="card-header"><?= __('Přílohy vyúčtování') ?></h2>
        <div class="card-body table-responsive">
            <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Příloha č.') ?></th>
                        <th><?= __('Název souboru') ?></th>
                        <th><?= __('Typ') ?></th>
                        <th><?= __('Je příloha použita?') ?></th>
                        <th><?= __('Velikost') ?></th>
                        <?php if ($canEdit) : ?>
                            <th><?= __('Akce') ?></th>
                        <?php endif; ?>
                        <?php if ($approvalProcess || $repairProcess || $approvalProcessView) : ?>
                            <th><?= __('Schvalování') ?></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($settlement->sortedAttachments() as $attachment) :
                        // Hide used attachments with settlement_attachment_type [1, 2, 5]
                        if ($attachment->isUsed($settlement) && in_array($attachment->settlement_attachment_type_id, [1, 2, 5])) continue;
                    ?>
                        <tr>
                            <td>
                                <?= $attachment->file_id ?>
                            </td>
                            <td>
                                <?= h($attachment->file->original_filename) ?>
                                <?= $this->Html->link(
                                    '<i class="fas fa-download ml-2 mr-1"></i>',
                                    ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->file_id],
                                    ['escapeTitle' => false, 'title' => __('Stáhnout')]
                                ) ?>
                                <?= $this->Html->link(
                                    '<i class="fas fa-file ml-1 mr-1"></i>',
                                    ['action' => 'settlementViewFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->file_id],
                                    [
                                        'escapeTitle' => false,
                                        'title' => __('Zobrazit'),
                                        'target' => '_blank'
                                    ]
                                ) ?>
                            </td>
                            <td><?= $attachment->settlement_attachment_type->type_name ?></td>
                            <td><?= $this->element('simple_checkmark_indicator', ['bool' => $attachment->isUsed($settlement)]) ?></td>
                            <td><?= Number::toReadableSize($attachment->file->filesize) ?></td>
                            <?php if ($canEdit) : ?>
                                <td>
                                    <?= $this->Html->link(__('Upravit'), ['action' => 'settlementAddModifyFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->id]) ?>
                                    <?= ', ' . $this->Html->link(__('Stáhnout soubor'), ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->file_id], ['class' => 'text-success']) ?>
                                    <?= !$repairProcess
                                        ? ', ' . $this->Form->postLink(__('Smazat'), ['action' => 'settlementDeleteFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete přílohu vymazat?')])
                                        : '' ?>
                                </td>
                            <?php endif; ?>
                            <?php if ($approvalProcess || $repairProcess || $approvalProcessView) : ?>
                                <td>
                                    <?php
                                    if ($approvalProcess) {
                                        echo $this->Form->control('approved2[]', ['type' => 'checkbox', 'checked' => boolval($attachment->approved), 'value' => $attachment->id, 'label' => __('')]);
                                        echo $this->Form->control('approved2_note:' . $attachment->id, ['type' => 'text', 'value' => $attachment->approved_note ?? '', 'label' => __('')]);
                                    } else if ($repairProcess || $approvalProcessView) {
                                        echo $attachment->approved ? '<span class="text-success">' . __('Schváleno') . '</span>' : '<span class="text-danger">' . __('Neschváleno') . '</span>';
                                        echo !$attachment->approved && !empty($attachment->approved_note) ? sprintf("<hr />%s", $attachment->approved_note) : '';
                                    }
                                    ?>
                                </td>
                            <?php endif ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?= $approvalProcess
                ? '<div class="float-right p-1 pr-3">' . $this->Form->submit(__('Uložit schvalování'), ['class' => 'btn btn-success']) . '</div>'
                : '' ?>
        </div>
        <?php if ($canEdit) : ?>
            <div class="card-footer">
                <strong><?= __('Nahrát novou přílohu') ?></strong> <br />
                <?php
                echo $this->Form->create($settlement_attachment, ['type' => 'file', 'url' => ['action' => 'settlementAddModifyFile', 'settlement_id' => $settlement->id]]);
                ?>
                <div class="row">
                    <div class="col-md-8 pt-2rem">
                        <?= $this->Form->control('filedata', ['type' => 'file', 'label' => __('Vyberte soubor přílohy'), 'required' => true]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->control('settlement_attachment_type_id', ['options' => $attachmentTypes, 'label' => __('Vyberte prosím typ nahrávaného souboru'), 'required' => true]); ?>
                    </div>
                </div>
                <?php
                echo $this->Form->submit(__('Nahrát novou přílohu'));
                echo $this->Form->end();
                ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
<?= $approvalProcess  ? $this->Form->end() : '' ?>

<div class="card m-2">
    <h5 class="card-header"><?= __('Nápověda') ?></h5>
    <div class="card-body">
        <div>
            <strong>Položky vyúčtování</strong>
            <ul>
                <li>zde se dokladují vynaložené finance nárokované pro potřeby dotace</li>
                <li>skládá se ze tří souvisejících částí:</li>
                <li>1. název položky, částka z dokladu, čerpaná částka a datum</li>
                <li>2. soubor, ve kterém je účetní nebo daňový doklad, faktura, účtenka, paragon, apod.</li>
                <li>3. soubor, ve kterém je výpis z bankovního účtu nebo výdajový pokladní doklad</li>
                <li>tyto soubory lze případně pouze vybrat, pokud byli již dříve vloženy v sekci Přílohy vyúčtování</li>
            </ul>
        </div>

        <div>
            <strong>Přílohy vyúčtování</strong>
            <ul>
                <li>seznam všech příloh v tomto vyúčtování</li>
                <li>je zde i možnost: Nahrát novou přílohu</li>
                <li>a tu pak použít v sekci Položky vyúčtování</li>
            </ul>
        </div>

        <div>
            <strong>Co znamená: Je příloha použita?</strong>
            <ul>
                <li>označení pro konkrétní soubor v Přílohy vyúčtování zda je/není použit v sekci Položky vyúčtování</li>
                <li>a/nebo slouží jako soubor doladující "závěrečnou zprávu" nebo "doklad o publicitě projektu"</li>
            </ul>
        </div>

        <div>
            <strong>Co znamená: Vyúčtování obsahuje nepoužité přílohy</strong>
            <ul>
                <li>informuje, zda jsou všechny vložené přílohy použity k doložení nároků na dotaci</li>
                <li>seznam příloh najdete v sekci Přílohy vyúčtování</li>
                <li>pokud je ve sloupci "Je příloha použita?" červený křížek, jedná se o soubor, který není použit v sekci Položky vyúčtování</li>
                <li>některé úřady ale vyžadují dodatečné přílohy, takže pak je toto označení pouze informativní</li>
            </ul>
        </div>
        <?= $settlement->isSettlementOwner($this->getCurrentUser())
            ? '<p><em>Více informací k tomuto dotačnímu portálu naleznete v sekci <a href="/podpora" target="_blank">Nápověda</a></em></p>'
            : '<p><em>Více informací naleznete v sekci <a href="/admin/docs#vyuctovani-dotace" target="_blank">Příručka správce dotačního portálu</a></em></p>' ?>
    </div>
</div>