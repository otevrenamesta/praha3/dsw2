<?php

use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\View\AppView;
use App\Model\Entity\ProjectBudgetDesign;

/**
 * @var $this AppView
 * @var $request Request
 * @var $public_income_history array
 */

$P4Exception =  OrganizationSetting::isException(OrganizationSetting::P4);

$historyNotRequested = $this->getSiteSetting(OrganizationSetting::SUBSIDY_HISTORY_NOT_REQUESTED);
$historyYears = max(0, intval($this->getSiteSetting(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true)));
$this->assign('title', h($request->name));
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h1 class="d-inline"><?= h($request->name) ?></h1>
            </div>
            <div class="col text-right">
                <?= sprintf("%s %d", __('žádost č.'), $request->id) ?><br />
                <?= $this->Html->link(__('Nastavení'), ['_name' => 'request_settings', 'id' => $request->id], ['class' => 'btn btn-outline-success']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col text-right">
                <?php
                if ($request->canUserDownloadPdf()) {
                    echo $this->Html->link(
                        __('Stáhnout jako PDF'),
                        ['action' => 'getPdf', $request->id],
                        ['class' => 'btn btn-success m-2']
                    );
                    echo $this->Html->link(
                        __('Otevřít jako PDF'),
                        ['action' => 'getPdfView', 'id' => $request->id],
                        ['class' => 'btn btn-success m-2', 'target' => '_blank']
                    );
                }
                if ($this->isRequestOwner($request) && $request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                    echo $this->Html->link(
                        __('Odeslat žádost Datovou schránkou'),
                        ['action' => 'requestSubmitDatabox', 'id' => $request->id],
                        ['class' => 'm-2 btn btn-info ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                    );

                    if ($this->getSiteSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, false)) {
                        echo "<br />";
                        echo $this->Html->link(
                            __('Odeslat žádost jinak'),
                            ['action' => 'lockUnlockRequest', 'id' => $request->id],
                            ['class' => 'btn btn-info m-2 ' . ($request->appeal->canUserSubmitRequest($request) ? '' : 'disabled')]
                        );
                    }
                }
                if ($this->isRequestOwner($request) && $request->request_state_id === RequestState::STATE_READY_TO_SUBMIT) {
                }

                ?>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th><?= __('Požadavek') ?></th>
                    <th><?= __('Stav požadavku') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?= __('Vyplnit svou kompletní identitu') ?>
                    </td>
                    <td>
                        <?php
                        $identityMissingFields = $this->getCurrentUser()->getIdentityMissingFields($request->user_identity_version);
                        $identityCompleted = empty($identityMissingFields);

                        $class = $identityCompleted ? 'fa-check text-success' : 'fa-times text-danger';
                        $text = $identityCompleted ? __('Hotovo') : __('Nesplněno');
                        ?><i class="fas font-weight-bold <?= $class ?>"></i> <?= $text ?>
                        <?php
                        if (!empty($identityMissingFields)) {
                            $counter = 0;
                            foreach ($identityMissingFields as $missingFieldId) {
                                if ($counter < 3) {
                                    echo '<br/><span class="text-danger">' . sprintf("%s: %s", __('Chybí vyplnit'), Identity::getFieldLabel($missingFieldId)) . '</span>';
                                }
                                $counter++;
                            }
                            if ($counter >= 3) {
                                echo '<br/>' . sprintf(__('Celkem %d chyb'), $counter);
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?= $this->Html->link(__('Vyplnit identitu'), ['_name' => 'update_self_info'], ['class' => 'btn btn-success']) ?>
                    </td>
                </tr>

                <?php
                echo $historyNotRequested ? '' : '<tr>
                    <td>' . ($historyYears > 0
                    ? sprintf("%s %d až %d", __('Poskytnout informace o přijaté veřejné podpoře za roky'), intval(date('Y')) - $historyYears, date('Y'))
                    : sprintf("%s %d", __('Vyplnit informace o přijaté veřejné podpoře za rok'), date('Y'))) .
                    '</td>
                    <td>' . (empty($public_income_history)
                        ? ('<span class="text-warning"><i class="fas fa-exclamation-triangle"></i> ' . __('Nebyly nalezeny žádné historické záznamy') . '</span>')
                        : ('<span class="">' . __('Historie obsahuje alespoň jednu položku') . '</span>')) .
                    '</td>
                    <td>' .
                    $this->Html->link(__('Upravit historii'), ['_name' => 'update_self_history', 'id' => $request->id, 'name' => $request->name], ['class' => 'btn btn-success']) .
                    '</td>
                </tr>'; ?>
                <?php $no_budget = ($request->program->project_budget_design_id == ProjectBudgetDesign::DESIGN_NONE) ?>
                <?php if (!$no_budget) : ?>
                    <tr>
                        <td>
                            <?= __('Vyplnit rozpočet projektu') ?>
                        </td>
                        <td><?= RequestBudget::getValidationUserMessage($request->validateBudgetRequirements(), $request) ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit rozpočet'), ['_name' => 'request_budget', $request->id], ['class' => 'btn btn-success']) ?>
                        </td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td>
                            <?= __('Požadovaná částka') ?>
                        </td>
                        <td><?= RequestBudget::getValidationUserMessage($request->validateBudgetRequirements(), $request) ?></td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                <?php endif; ?>


                <?php
                $forms = $request->getForms();
                foreach ($forms as $form) {
                ?>
                    <tr>
                        <td><?= sprintf("%s: %s", __("Vyplnit Formulář"), !empty($form->name_displayed) ? $form->name_displayed : $form->name) ?></td>
                        <td><?php
                            $isComplete = $request->isFormCompleted($form->id);
                            $class = $isComplete ? 'fa-check text-success' : 'fa-times text-danger';
                            $text = $isComplete ? __('Hotovo') : __('Nesplněno');
                            ?><i class="fas font-weight-bold <?= $class ?>"></i> <?= $text ?>
                        </td>
                        <td>
                            <?php
                            $text = ($form->shared) ? __('Změnit ve všech žádostech') : __('Vyplnit formulář');
                            $btn_class = ($form->shared) ? 'btn-warning' : 'btn-success';
                            ?>
                            <?= $this->Html->link($text, ['action' => 'formFill', 'id' => $request->id, 'form_id' => $form->id], ['class' => 'btn ' . $btn_class]) ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
                <tr>
                    <td>
                        <?= __('Nahrát nepovinné přílohy') ?>
                    </td>
                    <td>
                        <p class="text-success"><i class="fas fa-check text-success"></i> <?= __('Nemusíte nic nahrávat') ?>
                        </p>
                        <?php
                        $attachments = array_filter($request->files, function ($v) {
                            return empty($v['file_type']);
                        });
                        foreach ($attachments ?? [] as $attachment) : ?>
                            <?= $this->Html->link($attachment->original_filename, ['action' => 'fileDownload', $request->id, $attachment->id], ['target' => '_blank']); ?>
                            <?= $this->Form->postLink('<i class="fas fa-times"></i>', ['action' => 'attachmentDelete', $request->id, $attachment->id], ['class' => 'text-danger', 'title' => __('Smazat'), 'escapeTitle' => false, 'confirm' => __('Opravdu chcete smazat tuto přílohu?')]) ?>
                            <br />
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?= $this->Html->link(__('Nahrát novou přílohu'), ['action' => 'attachmentAdd', $request->id], ['class' => 'btn btn-outline-success']) ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        <div class='pt-2'><?= __('Program') ?>:</div>
        <h5 class='pt-2'><?= $request->program->name ?></h5>
        <div class='pt-3'>
            <?= $request->program->description ?>
        </div>
        <hr />
        <div class='pt-2'><?= __('Dotační výzva') ?>:</div>
        <h5 class='pt-2'><?= $request->appeal->name ?></h5>
        <div class='pt-3'>
            <?= $request->appeal->description ?>
        </div>
    </div>
</div>