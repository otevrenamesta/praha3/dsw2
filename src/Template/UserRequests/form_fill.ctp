<?php

use App\Form\AbstractFormController;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $requestForm AbstractFormController
 */

$this->assign('title', $requestForm->getFormNameDisplayed());
$required_hint = $requestForm->containsRequiredFields();
$backbutton = "";
$resetButton = "";

$tc = isset($mode) && $mode == 'track_changes';
$vc = isset($mode) && $mode == 'view_changes';
$readOnly = isset($mode) && $mode == 'read_only';

if (!$readOnly) {
    $submit = $this->Form->button(__('Uložit vyplněný formulář'), ['class' => 'btn btn-success m-2 save-incomplete-form', 'type' => 'submit']);
    if (!($vc || $tc)) $backbutton = $this->Html->link(__('Neukládat a Vrátit se zpět k žádosti'), ['action' => 'requestDetail', $requestForm->getUserRequest()->id], ['class' => 'btn btn-warning m-2']);
    if (!($vc || $tc)) $resetButton = $this->Form->postLink(__('Vymazat data a začít znovu'), ['action' => 'formFillReset', 'form_id' => $requestForm->getFormDefinition()->id, 'id' => $requestForm->getUserRequest()->id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete vymazat všechna vyplněné informace?')]);
    if ($tc) {
        $submit = $this->Form->button(__('Požádat o změnu v tomto formuláři'), ['class' => 'btn btn-success m-2 save-incomplete-form', 'type' => 'submit']);
    }

    if ($vc) {
        //$submit = $this->Form->button(__('Akceptovat změny'), ['class' => 'btn btn-success m-2 save-incomplete-form', 'type' => 'submit']);
        //$resetButton = $this->Form->postLink(__('Zamítnout změny'), ['action' => 'formFillReset', 'form_id' => $requestForm->getFormDefinition()->id, 'id' => $requestForm->getUserRequest()->id], ['class' => 'btn btn-danger m-2', 'confirm' => __('Opravdu chcete vymazat všechna vyplněné informace?')]);
        $submit = $this->Html->link(__('Akceptovat'), ['action' => 'requestFormChangeAccept', 'id' => $change_request->id], ['class' => 'm-3 btn btn-success ']);
        $resetButton = $this->Html->link(__('Zamítnout'), ['action' => 'requestFormChangeDecline', 'id' => $change_request->id], ['class' => 'm-3 btn btn-danger ']);
    }
}

echo $this->Form->create($requestForm, ['type' => $requestForm->getHtmlHelperFormType()]);
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-7">
                <h1><?= $this->fetch('title') ?></h1>
            </div>
            <div class="col-md text-right">
                <?php
                if (!$readOnly) {
                    if (!$tc) echo $submit;
                    echo $backbutton;
                    echo $resetButton;
                }
                ?>
            </div>
        </div>
    </div>

    <?php if (!empty($sharedRequests) && count($sharedRequests) > 1) : ?>
        <div class="card">
            <div class="card-header form-group required">
                <i class="fas fa-exclamation-triangle text-warning"></i> <?= __("Tento formulář je sdílený pro více žádostí. Změny zde provedené se propíší do všech následujících žádostí:") ?> <i class="fas fa-exclamation-triangle text-warning"></i>
            </div>
            <div class="card-body">
                <ul>
                    <?php
                    foreach ($sharedRequests as $sharedRequest) {
                        echo '<li>(ID: ' . $sharedRequest->id . ') ' . $sharedRequest->name . '</li>';
                    }
                    ?>
                </ul>
            </div>
        </div>

    <?php endif; ?>

    <?php if ($required_hint) : ?>
        <div class="card-header form-group required">
            <label for="uname"> <?= __("Červená hvězdička označuje pole formuláře, která musíte povinně vyplnit, aby bylo možné žádost podat") ?></label>
        </div>
    <?php endif; ?>



    <div class="card-body">

        <?= $requestForm->render($this); ?>
    </div>
    <div class="card-footer text-right">
        <?php
        if (!$readOnly) {
            echo $submit;
            echo $backbutton;
            echo $this->Form->end();
            echo $resetButton;
        }
        ?>
    </div>
</div>
<?= $this->element('filesize_check'); ?>
<script type="text/javascript">
    $(function() {
        $(".save-incomplete-form").on('click submit', function() {
            //console.log('bind working');
            let $form = $(this).closest('form');
            $("[required=required]", $form).each(function() {
                //console.log($(this));
                $(this).prop('required', false);
            });
        });
    });
</script>
<?php if ($vc) : ?>
    <style>
        .altered {
            background-color: rgb(220, 53, 69, 0.5) !important;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            <?php foreach ($highlight as $h) : ?>
                $('label[for=<?= $h ?>]').addClass('altered')
                $('input[name=<?= $h ?>]').prev().addClass('altered')
                $('input[name^=<?= $h ?>]').closest('table').prev().addClass('altered')
            <?php endforeach; ?>
        });
    </script>

<?php endif; ?>