<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementAttachment;
use App\Model\Entity\SettlementAttachmentType;
use App\Model\Entity\SettlementItem;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $attachmentTypes array
 * @var $budget Array
 * @var $item SettlementItem
 * @var $program Program
 * @var $settlement Settlement
 */
$this->assign('title', $item->isNew() ? __('Nová položka vyúčtování') : sprintf("%s: %s", __('Položka'), $this->Text->truncate(h($item->description), 54)));

// budget settings
if (isset($budget) && !empty($budget)) list($budgetSectionsList,) = $budget;

// attachments
$invoiceAttachmentTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_1_INVOICES);
$proofTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_2_PROOF);
$invoiceAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_1_INVOICES, true);
$proofAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_2_PROOF, true);
$otherAttachmentTypes = SettlementAttachmentType::filter($attachmentTypes, [SettlementAttachmentType::TYPE_ACCOUNTING_INVOICE_DOCUMENT, SettlementAttachmentType::TYPE_PROOF_OF_PAYMENT, SettlementAttachmentType::TYPE_OTHERS]);
$allAttachmentsNotAssignedInThisItem = $settlement->getAllAttachments(true, $item);
$allAttachments = $settlement->getAllAttachments(true);
$maxOptionalAttachments = $this->getSiteSetting(OrganizationSetting::ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT, true);
$notRequireAttachments = isset($program) ? boolval($program->not_require_tax_documents) : false;

// additional columns if enabled in organization settings
$addColumns = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_ADDITIONAL_COLUMNS);

echo $this->Form->create($item, ['type' => 'file']);
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo isset($budgetSectionsList) && count($budgetSectionsList) > 0
            ? $this->Form->control(
                'budget_item',
                ['options' => $budgetSectionsList, 'label' => __('Vyberte prosím položku z rozpočtu'), 'required' => true]
            )
            : '';
        echo $this->Form->control('description', ['label' => __('Popis položky')]);
        echo $this->Form->control('note', ['label' => __('Nepovinná poznámka / vysvětlení k této položce')]);
        ?>
        <hr />
        <?php
        echo $this->Form->control('original_amount', ['label' => __('Částka dle dokladu (Kč)'), 'min' => 0, 'step' => .01, 'default' => '0']);
        echo $this->Form->control('amount', ['label' => __('Z toho čerpáno z dotace (Kč)'), 'step' => .01, 'default' => '0']);
        echo $addColumns ? $this->Form->control('own_amount', ['label' => __('Hrazeno z vlastních zdrojů (Kč)'), 'min' => 0, 'step' => .01]) : '';
        echo $this->Form->control('paid_when', ['label' => __('Datum uhrazení'), 'required' => true, 'default' => false, 'type' => 'date']);
        ?>
    </div>
    <h2 class="card-header"><?= __('Přílohy') ?></h2>
    <p class="alert alert-info">
        <?= __('Pokud obsahuje nahrávaný dokument více stran, je nutné všechny naskenované stránky daného dokumentu nejprve spojit do jednoho dokumentu a ten poté nahrát jako jeden soubor s více stranami.') ?>
    </p>
    <div class="card-body">
        <?php
        $nonEmptyAttachmentsCount = 0;
        for ($i = 0; $i < (2 + $maxOptionalAttachments); $i++) {
            $fieldNamePrefix = sprintf('settlement_attachments.%d.', $i);
            $typeOptions = $otherAttachmentTypes;
            $existingOptions = $allAttachmentsNotAssignedInThisItem;
            $required = false;
            $currentData = $item->getAttachment($i);
            $filedata_label = __('Příloha');
            $existing_label = __('Již nahrané přílohy');
            $hasData = $currentData instanceof SettlementAttachment && !$currentData->isNew();
            switch ($i) {
                case 0:
                    $filedata_label = __('Nahrát účetní nebo daňový doklad / fakturu / účtenku / paragon / mzdový doklad / smlouvu');
                    $existing_label = __('Účetní nebo daňový doklad / faktura / účtenka / paragon / mzdový doklad / smlouva');
                    $existingOptions = $invoiceAttachments;
                    $typeOptions = $invoiceAttachmentTypes;
                    $required = $notRequireAttachments ? false : true;
                    break;
                case 1:
                    $filedata_label = $existing_label = __('Výpis z bankovního účtu / Výdajový pokladní doklad');
                    $existingOptions = $proofAttachments;
                    $typeOptions = $proofTypes;
                    $required = $notRequireAttachments ? false : true;
                    break;
            }
            if ($hasData) {
                $nonEmptyAttachmentsCount++;
                $existingOptions = array_replace($existingOptions, [
                    $currentData->id => $settlement->getAttachmentsById($currentData->id)->getLabel()
                ]);
            }
        ?>
            <div class="row" data-filled="<?= json_encode($hasData) ?>" data-optional="<?= json_encode($i > 1) ?>">
                <div class="col-md-8">
                    <?php
                    echo $this->Form->control($fieldNamePrefix . 'id');
                    echo $this->Form->control($fieldNamePrefix . 'filedata', ['type' => 'file', 'label' => $filedata_label, 'required' => $required]);
                    echo $this->Form->control($fieldNamePrefix . 'existing_id', ['type' => 'select', 'label' => $existing_label, 'empty' => $required ? false : __('Vyberte tuto možnost, pokud chcete tuto přílohu ponechat prázdnou'), 'value' => $hasData ? $currentData->id : false, 'options' => $existingOptions, 'class' => 'existing-attachments']);
                    ?>
                    <?php
                    echo $this->Form->control($fieldNamePrefix . 'select_existing', ['type' => 'checkbox', 'label' => __('Vybrat z již nahraných příloh'), 'checked' => $hasData, 'data-is-required' => $required ? 'true' : 'false', 'class' => 'attachment-switch']);
                    ?>
                    <?php
                    if ($hasData) {
                        $fileData = $settlement->getFileById($currentData->file_id, false);
                        echo $this->element('fileinfo', [
                            'file' => $fileData,
                            'showWarning' => false,
                            'link' => ['action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $fileData->id]
                        ]);
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->control($fieldNamePrefix . 'settlement_attachment_type_id', ['options' => $typeOptions, 'empty' => !$required, 'label' => __('Vyberte prosím typ nahrávaného souboru'), 'required' => $required, 'class' => 'attachment-type']) ?>
                </div>
            </div>
            <?php
            if ($i === 1) {
                echo "<hr/>";
            }
            ?>
        <?php } ?>
    </div>
    <div class="card-footer">
        <?php
        echo $item->approved ? '' : $this->Form->submit(__('Uložit'));
        ?>
    </div>
</div>
<?php
echo $this->Form->end();
echo $this->element('filesize_check');
?>

<script type="text/javascript">
    const NUMBER_OF_PRESENT_ATTACHMENTS = <?= $nonEmptyAttachmentsCount ?>;
    $(function() {
        $(".attachment-switch").change(function() {
            let selected = $(this).is(':checked');
            let isRequired = $(this).data('is-required');
            let $row = $(this).closest('.row');
            $("input[type=file]", $row).prop('required', isRequired && !selected).closest('.form-group').toggleClass('required', isRequired && !selected).toggle(!selected);
            $("select.existing-attachments", $row).prop('required', isRequired && selected).closest('.form-group').toggleClass('required', isRequired && selected).toggle(selected);
            $("select.attachment-type", $row).prop('required', isRequired && !selected).closest('.form-group').toggleClass('required', isRequired && !selected).closest('.col-md-4').toggle(!selected);
            $('.col-md-8', $row).toggleClass('pt-2rem', !selected);
        }).change();
        $("div.row[data-optional=true][data-filled=true]").show();
        let $hiddenAttachments = $("div.row[data-filled=false][data-optional=true]");
        $hiddenAttachments.hide();
        $hiddenAttachments.first().show();
        $("div.row[data-optional=true] input[type=file], div.row[data-optional=true] select").change(function() {
            let $row = $(this).closest('div.row[data-optional=true]');
            let $selfEmpty = $.trim($(this).val()).length === 0;
            let $selfVisible = $row.is(":visible");
            let $shouldHideRow = $selfEmpty && $selfVisible;
            if ($shouldHideRow) {
                $("select, input[type=file]", $row).each(function() {
                    $shouldHideRow = $shouldHideRow && $.trim($(this).val()).length === 0;
                });
            }
            let $lastVisible = $("div.row[data-optional=true]:visible").last();
            let $firstHidden = $("div.row[data-optional=true]:hidden").first();
            if ($shouldHideRow) {
                //$lastVisible.hide();
            } else if ($lastVisible.is($row)) {
                $firstHidden.show();
            }
        });
    });
</script>