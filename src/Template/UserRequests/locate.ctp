<?php

use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $public_income_history array
 */



$this->assign('title', 'Vstoupit do žádosti');
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h1 class="d-inline"><?= __('Vstoupit do uživatele podle čísla žádosti') ?></h1>
            </div>
        </div>
    </div>
    <div class="card-body">
        <?php
        echo $this->Form->create(null, array('type' => 'get'));
        echo $this->Form->label('request_id', __("Zadejte ID žádosti:"));
        echo $this->Form->text('request_id', ['type' => 'number', 'id' => 'request-id']);
        echo "<br/>";
        echo $this->Html->link('<i class="fas fa-dungeon"></i> ' . __('Vstoupit do uživatele') . '<span id="user-email"></span>', '#', ['escapeTitle' => false, 'class' => 'btn btn-primary', 'id' => 'btn-enter']);
        echo $this->Form->end
        ?>
    </div>
    <div class="card-footer">
    </div>
</div>
<script>
    $('#btn-enter').attr('disabled', true).addClass('disabled');
    $('#request-id').on('keyup', function(event) {
        var val = $(this).val();
        $.getJSON('/admin/locate/' + val, function(data) {
            if (data.status == 'OK') {
                $('#btn-enter').attr('disabled', false).removeClass('disabled');
                $("#btn-enter").attr("href", '/admin/users/' + data.id +  '/login');
                $("#user-email").text(": " + data.email);
            } else {
                $('#btn-enter').attr('disabled', true).addClass('disabled');
                $("#user-email").text();
            }
        })
    });
</script>