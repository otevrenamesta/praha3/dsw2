<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Identities;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementAttachmentType;
use App\Model\Entity\SettlementState;
use App\Model\Entity\SettlementType;
use App\Model\Table\ProgramsTable;
use App\View\AppView;
use Cake\I18n\Number;
use OldDsw\Model\Entity\Zadost;

/**
 * @var $this AppView
 * @var $attachmentTypes SettlementAttachmentType[]
 * @var $budget Array
 * @var $identities Identities
 * @var $mergedRequest object
 * @var $program Program
 * @var $settlement Settlement
 */

/** @var Zadost|Request $request */
$request = $settlement->getRequest();
if ($request instanceof Request) {
    $request->loadUser()->user;
}
if (isset($identities) && !empty($identities)) {
    $request->identities = $identities;
}
// is economics/payout for this settlement/id?
$request_sum = round(isset($request->konecna_castka) ? $request->konecna_castka : $request->getPaymentsSum(), 2);
$economics_payout = $request_sum;
if (!isset($request->konecna_castka)) {
    foreach ($request->getPayments() as $key => $value) {
        $economics_payout = $value->settlement_id === $settlement->id ? $value->amount_czk : $economics_payout;
    }
}

// show 'expenses_total' field
$origItemsSum = 0;
$isExpensesTotal = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_EXPENSES_TOTAL_FIELD);
if ($isExpensesTotal) {
    $economics_payout = $settlement->expenses_total ?? 0;
    foreach ($settlement->settlement_items as $item) {
        $origItemsSum += (float)$item->original_amount;
    }
}

// budget settings
if (isset($budget) && !empty($budget)) {
    list(
        $budgetSectionsList,
        $budgetColumns,
        $budgetRequest,
        $budgetSectionsData,
        $budgetExceeded,
        $budgetExceededPartial,
        $budgetSectionsDataFull,
    ) = $budget;
}
$isBudget = (isset($budgetSectionsList) && !empty($budgetSectionsList));

// payments
$request_amount = round($request instanceof Request ? $request->getFinalSubsidyAmount() : $request->konecna_castka, 2);
$request_refund = round($request instanceof Request ? $request->getPaymentsRefundSum() : 0, 2);
$settlement_sum = $isExpensesTotal ? round($origItemsSum, 2) : round($settlement->getItemsSum(), 2);
$settlement_subt = $economics_payout - $settlement_sum;

$isMergedRequest = isset($mergedRequest) && $mergedRequest && $mergedRequest->id > 0 && $mergedRequest->final_subsidy_amount > 0;
$mergedRequestAmount = $isMergedRequest ? (float)$mergedRequest->final_subsidy_amount : 0;

$waived_amount = $settlement->getWaivedAmount();
$waived_explanation = $waived_amount > 0 ? trim(strip_tags(OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION) ?? '')) : null;

// additional columns if enabled in organization settings
$addColumns = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_ADDITIONAL_COLUMNS);
?>
<style type="text/css">
    @media print {
        * {
            page-break-inside: avoid;
            page-break-after: avoid;
            page-break-before: avoid;
        }

        .card-header {
            margin: 0;
            max-width: 98%;
            overflow: none;
        }

        .settlements table {
            width: 98%;
        }

        .alert h3 {
            font-size: 90%;
        }
    }

    table {
        page-break-inside: auto;
    }

    table tr {
        position: relative !important;
        float: none;
        page-break-inside: avoid;
        page-break-after: auto;
        page-break-before: auto;
    }

    thead {
        display: table-header-group
    }

    tfoot {
        display: table-footer-group
    }
</style>
<div class="card m-2">
    <div class="card-header">
        <span class="h2">
            <?= sprintf("%s #%d", $settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT ? __('Vyúčtování dotace') : __('Vrácení dotace'), $request->id) ?>
        </span>
        <p class="p1 text-right">
            <?= strip_tags(OrganizationSetting::getSetting(OrganizationSetting::CONTACT_ADDRESS)) ?><br />
        </p>
        <div class="p1">
            <div class="p1">
                <?= $request->reference_number ? sprintf("%s: %s", __('Vlastní identifikátor'), $request->reference_number) : '' ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: <strong>%s</strong>", __('Název projektu'), $settlement->getRequestName()) ?>
            </div>
            <div class="p1">
                <?= isset($program) && isset($program->name) && !empty($program->name) ? sprintf("%s: <strong>%s</strong>", __('Program podpory'), $program->name) : '' ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: <strong>%s</strong>", __('Číslo veřejnoprávní smlouvy'), $settlement->agreement_no) ?><br />
                <?= sprintf("%s: <strong>%s</strong>", __('Stav vyúčtování'), SettlementState::getLabel($settlement->settlement_state_id)) ?>
            </div>
        </div>
        <hr />
        <div class="p1">
            <div class="p1">
                <?= sprintf("%s: %s", __('Schválená podpora celkem'), Number::currency($request_amount + $mergedRequestAmount, 'CZK')) ?>
                <?php
                if (!$isMergedRequest && isset($request->original_subsidy_amount) && !empty($request->original_subsidy_amount)) {
                    echo '<ul>';
                    echo '<li>' . sprintf("%s: %s", __('Původní podpora celkem'), Number::currency($request->original_subsidy_amount, 'CZK')) . '</li>';
                    echo '<li>' . sprintf("%s: %s", __('Dofinancování (navýšení původně schválené podpory)'), Number::currency($request->final_subsidy_amount - $request->original_subsidy_amount, 'CZK')) . '</li>';
                    echo '</ul>';
                } else if ($isMergedRequest) {
                    echo '<ul>';
                    echo '<li>' . __('Schválená podpora "této" žádosti') . ': ' . Number::currency($request->final_subsidy_amount, 'CZK') . '</li>';
                    echo '<li>' . __('Schválená podpora "propojené" žádosti') . ' (#' . $mergedRequest->id . ', ' . $mergedRequest->name . '): ' . Number::currency($mergedRequest->final_subsidy_amount, 'CZK') . '</li>';
                    echo '</ul>';
                }
                ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: %s", __('Vyplacená podpora'), Number::currency($request_sum + $request_refund, 'CZK')) ?>
            </div>
            <div class="p1">
                <?= $request_refund > 0 ? sprintf("%s: %s", __('Vrácená podpora'), Number::currency($request_refund, 'CZK')) : '' ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: %s", __('Nečerpaná podpora'), Number::currency($request_amount - $request_sum - $request_refund + $mergedRequestAmount, 'CZK')) ?>
                <?= $request_refund > 0 ? sprintf(" (%s: %s)", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: %s", __('Čerpaná podpora celkem'), Number::currency($request_sum, 'CZK')) ?>
            </div>
            <hr />
            <div class="p1">
                <?= sprintf("%s: %s", $isExpensesTotal ? __('Celková výše nákladů na projekt') : __('Částka k čerpání v tomto vyúčtování'), Number::currency($economics_payout, 'CZK')) ?>
            </div>
            <div class="p1">
                <?= sprintf("%s: %s", __('Částka vyúčtovaná'), Number::currency($settlement_sum, 'CZK')) ?>
            </div>
            <div class="p1">
                <?= sprintf("%s %s: %s", !empty($waived_explanation)
                    ? '<span class="text-danger">*</span>'
                    : '', __('Částka nevyúčtovaná'), Number::currency($settlement_subt, 'CZK')) ?>
                <?= $request_refund > 0 ? sprintf(" (%s: %s) ", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
            </div>
            <div class='pl-2'>
                <?= (isset($budgetExceeded) && boolval($budgetExceeded))
                    ? '<hr />' . sprintf("%s: &nbsp; <span class='badge bg-danger text-white'>%s</span><br />", __('Vyúčtované náklady vzhledem k rozpočtu'), __('Přečerpáno'))
                    : '' ?>
            </div>
        </div>
        <?php
        $full_return_instructions = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY);
        $standard_return_instructions = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS);
        $warning = $settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN && !empty($full_return_instructions)
            ? $full_return_instructions
            : $standard_return_instructions;
        echo $warning ? '<hr /><div class="alert alert-warning text-dark text-center small">' . $warning . '</div>' : '';
        ?>
    </div>
    <?php
    if (!empty($waived_explanation)) :
    ?>
        <div class="card-footer">
            <span class="text-danger">*</span>
            <p><?= $waived_explanation ?></p>
        </div>
    <?php endif; ?>
</div>
<?php if ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN) : ?>
    <div class="card m-2">
        <h2 class="card-header">
            <?= __('Důvod vrácení poskytnuté podpory v plné výši') ?>
        </h2>
        <div class="card-body">
            <p>
                <?= $settlement->return_reason ?>
            </p>
        </div>
    </div>
<?php endif; ?>
<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Příjemce dotace') ?></h2>
        <?= __('Původní identifikace ze žádosti o dotaci') ?>
    </div>
    <div class="card-body">
        <?php
        if ($request instanceof Zadost) {
            echo $this->element('OldDsw.ucet', ['ucet' => $request->ucet, 'short' => false, 'public_only' => true]);
        } elseif ($request instanceof Request) {
            echo $this->element('identity_full_table', ['user' => $request->user, 'request' => $request]);
        }
        ?>
    </div>
</div>

<?php if ($isBudget) : ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <h2><?= __('Položky rozpočtu') ?></h2>
                </div>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Položky nákladů k vyúčtování') ?></th>
                        <th class="text-right"><?= __('Částka požadovaná v žádosti') ?></th>
                        <th class="text-right"><?= __('Náklady vyúčtované') ?></th>
                        <th class="text-right"><?= __('Nevyúčtováno/přečerpáno') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($budgetSectionsDataFull as $item) : ?>
                        <tr>
                            <td><?= $item['title'] ?></td>
                            <td class="text-right"><?= Number::currency($item['total2'], 'CZK') ?></td>
                            <td class="text-right"><?= Number::currency($item['sum'], 'CZK') ?></td>
                            <td class="text-right<?= $item['diff'] < 0 ? ' text-danger' : '' ?>"><?= Number::currency($item['diff'], 'CZK') ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <hr />
        <div class="card-body table-responsive pb-0">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Náklady celkem – skutečně vynaložené celkové náklady') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="pl-3"> <?php echo Number::currency($settlement->expenses_total, 'CZK') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <?php foreach ($budgetColumns as $column) : ?>
                            <th class="text-right"><?= $column ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($budgetColumns as $column => $value) : ?>
                            <td class="text-right">
                                <?= isset($budgetRequest) && isset($budgetRequest[$column])
                                    ? (strpos($value, '%') ? Number::toPercentage($budgetRequest[$column]) : Number::currency($budgetRequest[$column], 'CZK'))
                                    : '' ?>
                                <?= ($column === 'diff_' && $budgetRequest['diff_'] > $budgetRequest['max_']) ||
                                    ($column === 'sum_' && $budgetRequest['sum_'] > $budgetRequest['requested_amount'])
                                    ? '<br /><span class="text-danger">' . __('Překročeno') . '</span>'
                                    : '' ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
    <div class="card m-2">
        <div class="card-body settlements">
            <h2 class="card-header">
                <?= __('Položkový rozpis nákladů') ?>
            </h2>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><?= __('Položka č.') ?></th>
                        <th><?= __('Technické č. položky') ?></th>
                        <th><?= __('Datum uhrazení') ?></th>
                        <th><?= __('Popis položky') ?></th>
                        <th><?= __('Účetní nebo daňový doklad') ?></th>
                        <th><?= __('Výpis z bankovního účtu / Výdajový pokladní doklad') ?></th>
                        <th><?= __('Ostatní přílohy') ?></th>
                        <th class="text-right"><?= __('Částka dle dokladu') ?></th>
                        <th class="text-right"><?= __('Z toho čerpáno z dotace') ?></th>
                        <?= $addColumns ? '<th class="text-right">' . __('Hrazeno z vlastních zdrojů') . '</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' . __('Částka, která není k dotaci vůbec uplatňována') . '</th>' : '' ?>
                    </tr>
                </thead>
                <?php
                $totalAmount = 0;
                $totalOrigAmount = 0;
                $totalOwnAmount = 0;
                ?>

                <tbody>
                    <?php foreach ($settlement->settlement_items as $item) : ?>
                        <tr>
                            <td><?= $item->order_number ?></td>
                            <td><?= $item->id ?></td>
                            <td><?= $item->paid_when->nice() ?></td>
                            <td>
                                <?= isset($budgetSectionsList) && !empty($item->budget_item) && isset($budgetSectionsList[$item->budget_item]) ? $budgetSectionsList[$item->budget_item] . '<hr />' : '' ?>
                                <?= $item->description ?>
                            </td>
                            <td><?= $settlement->printFormatAttachments($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_1_INVOICES)) ?></td>
                            <td><?= $settlement->printFormatAttachments($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_2_PROOF)) ?></td>
                            <td><?= $settlement->printFormatAttachments($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_3_OTHERS)) ?></td>
                            <td class="text-right"><?= Number::currency($item->original_amount, 'CZK') ?></td>
                            <td class="text-right"><?= Number::currency($item->amount, 'CZK') ?></td>
                            <?= $addColumns ? '<td class="text-right">' . Number::currency($item->own_amount, 'CZK') . '</td>' : '' ?>
                            <?= $addColumns ? '<td class="text-right">' . Number::currency($item->original_amount - $item->amount - $item->own_amount, 'CZK') . '</td>' : '' ?>
                            <?php
                            $totalOrigAmount += (float)$item->original_amount;
                            $totalAmount += (float)$item->amount;
                            ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="7"><?= __('Celkem') ?></th>
                        <th class="text-right"><?= Number::currency($totalOrigAmount, 'CZK') ?></th>
                        <th class="text-right"><?= Number::currency($totalAmount, 'CZK') ?></th>
                        <?= $addColumns ? '<th class="text-right">' . Number::currency($totalOwnAmount, 'CZK') . '</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' . Number::currency($totalOrigAmount - $totalAmount - $totalOwnAmount, 'CZK') . '</th>' : '' ?>
                    </tr>
                    <tr class="thead-dark">
                        <?php if ($isExpensesTotal) {
                            $totalOrigAmount = $economics_payout;
                        } ?>
                        <th colspan="7"><?= $isExpensesTotal ? __('Podíl z celkové výše nákladů na projekt ') . Number::currency($economics_payout, 'CZK') : __('Podíl') ?></th>
                        <th class="text-right"><?= $isExpensesTotal ? '' : number_format(100, 2) . '%' ?></th>
                        <th class="text-right"><?= number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2) ?> %</th>
                        <?= $addColumns ? '<th class="text-right">' . number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2) . ' %</th>' : '' ?>
                        <?= $addColumns ? '<th class="text-right">' .
                            number_format(100 - number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2) - number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2), 2) .
                            ' %</th>' : '' ?>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="card m-2">
        <h2 class="card-header">
            <?= __('Rozpis příloh') ?>
        </h2>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><?= __('Příloha č.') ?></th>
                        <th><?= __('Název souboru') ?></th>
                        <th><?= __('Typ') ?></th>
                        <th><?= __('Velikost') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($settlement->sortedAttachments() as $attachment) :
                        // Hide used attachments with settlement_attachment_type [1, 2, 5]
                        if ($attachment->isUsed($settlement) && in_array($attachment->settlement_attachment_type_id, [1, 2, 5])) continue;
                    ?>
                        <tr>
                            <td><?= $attachment->file_id ?></td>
                            <td><?= h($attachment->file->original_filename) ?></td>
                            <td><?= $attachment->settlement_attachment_type->type_name ?></td>
                            <td><?= Number::toReadableSize($attachment->file->filesize) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php
    $consent = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH);
    if (!empty(trim(strip_tags($consent ?? '')))) :
    ?>
        <hr />
        <div class="alert alert-warning text-dark font-weight-bold small">
            <?= $consent ?>
        </div>
    <?php endif; ?>
<?php endif; ?>