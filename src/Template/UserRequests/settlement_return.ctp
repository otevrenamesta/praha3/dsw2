<?php

/**
 * @var $this AppView
 * @var $settlement Settlement
 * @var $canSubmitRightAway boolean
 */

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Settlement;
use App\View\AppView;

$this->assign('title', __('Vrátit dotaci v plné výši'));

if ($canSubmitRightAway) :
?>
    <div class="card m-2">
        <h2 class="card-header">
            <?= $this->Html->link(
                __('Odeslat vyúčtování Datovou schránkou'),
                ['controller' => 'UserRequests', 'action' => 'settlementSubmitDatabox', 'settlement_id' => $settlement->id],
                ['class' => 'm-2 btn btn-success']
            ); ?>
            <?= $this->getSiteSetting(OrganizationSetting::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND) ? '' : $this->Html->link(
                __('Odeslat vyúčtování jinak'),
                ['controller' => 'UserRequests', 'action' => 'settlementSubmitDirectly', 'settlement_id' => $settlement->id],
                ['class' => 'm-2 btn btn-success']
            ); ?>
        </h2>
    </div>
<?php
endif;
echo $this->Form->create($settlement, ['type' => 'file']);
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('return_reason', ['required' => true, 'label' => __('Důvod vrácení poskytnuté podpory v plné výši'), 'data-noquilljs' => 'data-noquilljs']);
        echo $this->Form->control('agreement_no', ['required' => true, 'label' => __('Číslo veřejnoprávní smlouvy')]);
        echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('Potvrzení o provedeném převodu zpět na účet dotačního úřadu'), 'required' => true]);
        echo $this->Form->hidden('settlement_attachment_type_id', ['default' => 2]);
        ?>
    </div>
    <div class="card-footer">
        <div class="alert alert-info">
            <?= $this->getSiteSetting(OrganizationSetting::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY, true) ?>
        </div>
        <?= $this->Form->submit(__('Odeslat'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
echo $this->Form->end();
