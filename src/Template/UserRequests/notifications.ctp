<?php

use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 **/

$this->assign('title', sprintf("%s #%d", __('Ohlášení k žádosti'), $request->id));

?>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-8">
                <h2><?= $this->fetch('title') ?></h2>
            </div>
            <div class="col-md text-right">
                <?= $this->Html->link(__('Provést nové ohlášení'), ['action' => 'notificationCreate', 'id' => $request->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <p>
            <?= sprintf("<strong>%s:</strong> %s", __('Žádost'), h($request->name)) ?><br />
            <?= sprintf("<strong>%s:</strong> %s", __('Žádatel'), h($request->getRequesterName())) ?>
        </p>
    </div>
</div>

<?php
foreach ($request->request_notifications as $notification) {
    echo $this->element('notification_card', compact('request', 'notification'));
}
