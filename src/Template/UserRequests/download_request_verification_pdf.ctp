<?php

use App\Model\Entity\Request;
use App\View\AppView;
use App\Model\Entity\OrganizationSetting;

/**
 * @var $this AppView
 * @var $request Request
 */
$appView = $this instanceof AppView ? $this : new AppView($this->getRequest(), $this->getResponse());
$orgCodeRequired = OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, false);

?>

<style type="text/css">
    @media print {
        * {
            page-break-inside: avoid;
            page-break-after: avoid;
            page-break-before: avoid;
        }
    }

    table {
        page-break-inside: auto;
    }

    table tr {
        position: relative !important;
        float: none;
        page-break-inside: avoid;
        page-break-after: auto;
        page-break-before: auto;
    }

    thead {
        display: table-header-group
    }

    tfoot {
        display: table-footer-group
    }
</style>

<div class="card">
    <h2 class="card-header">
        <?= __('Potvrzení o elektronické evidenci žádosti') ?>
    </h2>
    <div class="card-body">
        <table class="table table-striped">
            <tr>
                <?php if ($orgCodeRequired) : ?>
                    <td><strong><?= __('Kód pro příjem žádosti') ?></strong></td>
                    <td><?= $request->getVerificationCode() ?></td>
                <?php else : ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php endif ?>
                <td><strong><?= __('Datum uzamčení žádosti') ?></strong></td>
                <td><?= $request->getVerificationCodeTimestamp(true) ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="card mt-2">
    <h5 class="card-header"><?= __('Žádost') ?></h5>
    <div class="card-body">
        <table class="table table-striped">
            <tr>
                <td><strong><?= __('Název projektu') ?></strong></td>
                <td><?= $request->name ?></td>
                <td><strong><?= __('Identifikační číslo žádosti') ?></strong></td>
                <td>
                    <?= $request->id ?>
                    <?php if (!empty(trim($request->reference_number))) : ?>
                        ( <?= __('vlastní identifikátor') ?>: <?= $request->reference_number ?> )
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td><strong><?= __('Dotační program') ?></strong></td>
                <td><?= $request->program->name ?></td>
                <td><strong><?= __('Oblast podpory') ?></strong></td>
                <td><?= ($request->program->realm ?? $request->program->parent_program->realm)->name ?></td>
            </tr>
            <tr>
                <td><strong><?= __('Nadřízený dotační program') ?></strong></td>
                <td><?= $request->program->parent_program ? $request->program->parent_program->name : '' ?></td>
                <td><strong><?= __('Název dotační výzvy') ?></strong></td>
                <td><?= $request->appeal->name ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="card mt-2">
    <h5 class="card-header"><?= __('Žadatel') ?></h5>
    <div class="card-body">
        <?= $appView->element('identity_full_table', ['user' => $request->user, 'request' => $request, 'brief' => true]) ?>
    </div>
</div>

<?= $appView->element('pdf_signatures_table') ?>