<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use App\Model\Entity\RequestNotification;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals array
 * @var $states array
 * @var $requests Request[]
 */


echo $this->element('simple_datatable');
echo $this->element('simple_select2');
$this->assign('title', __('Přehled ohlášení'));
?>

<?= $this->Form->create(null, ['type' => 'get']) ?>

<div class="row">
    <div class="col-md">
        <?= $this->Form->control(
            'program_id',
            ['options' => $programs, 'label' => __('Program'), 'empty' => __('Všechny programy...'), 'default' => $request_program_id]
        ) ?>
        <?= $this->Form->submit(__('Filtrovat  podle programu')) ?>
        <br />
    </div>
</div>


<table class="table table-bordered" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID Žádosti') ?></th>
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Typ ohlášení') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Místo konání') ?></th>
            <th><?= __('Čerpaná částka') ?></th>
            <th><?= __('Datum a čas ohlášení') ?></th>
            <th><?= __('Program') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($requestNotifications as $notification) : ?>
            <tr>
                <td><?= $notification->request->id . $this->Html->link(' (' . __('Stránka ohlášení') .')', ['action' => 'notifications', $notification->request->id], []) ?></td>
                <td><?= $notification->request->name ?></td>
                <td><?= $notificationTypes[$notification->request_notification_type_id] ?></td>
                <td><?= $notification->getRequesterName(false) ?></td>
                <td><?= $notification->location ?></td>
                <td class="text-right" data-type="currency"><?= Number::currency($notification->costs_czk, 'CZK') ?></td>
                <td><?= $notification->created ?></td>
                <td><?= $programs[$notification->request->program_id] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
