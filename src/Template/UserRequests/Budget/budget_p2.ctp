<?php

use App\Budget\P2ProjectBudget;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\RequestBudgetChange;
use Cake\I18n\Number;


/**
 * @var $this AppView
 * @var $request Request
 * @var $budget RequestBudget
 * @var $design_id DesignID
 * @var $change_request_id Change Request ID
 */

$budgetHandler = new P2ProjectBudget($design_id);
$org_name = $budgetHandler->org_name;

$this->assign('title', __('Rozpočet projektu'));
$this->Html->css('table-with-inputs.css', ['block' => true]);


$budgetHandler->loadBudget($budget);

$budget_id = $budgetHandler->budget_id;
echo $this->Form->create($budget);



?>
<!-- modal popups -->
<div class="modal fade" id="budget-equal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nelze odeslat žádost</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?= __("Nelze odeslat žádost o změnu, protože rozpočet není vyrovaný."); ?></p>
                <p><?= __("Částky se liší o:"); ?> <span id='budget-gap'></span></p>
                <p><?= __("Opravte prosím rozpočet."); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Zavřít"); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="budget-balance" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nelze odeslat žádost</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?= __("Nelze odeslat žádost o změnu, protože výše požadované dotace v novém rozpočtu neodpovídá skutečně přidělené částce."); ?></p>
                <p><?= __("Částky se liší o:"); ?> <span id='budget-gap'></span></p>
                <p><?= __("Opravte prosím rozpočet."); ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Zavřít"); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- end modal popup -->

<div class="card mt-2">
    <div class="card-header">
        <h2 class="card-title"><?= $this->fetch('title') ?></h2>
    </div>
    <div class="card-header">
        <div class="font-weight-bold">
            <?php
            $labels = [
                'total_costs' => __('Celkové náklady/výdaje na projekt'),
                'total_income' => __('Ostatní finanční zdroje'),
                'requested_amount' => __('Výše požadované dotace od MČ  Praha 2'),
                'percentage' => __('Podíl dotace na celkových nákladech')
            ];
            echo $this->Form->control('total_costs', ['label' => $labels['total_costs'], 'required'=>false, 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_costs, 'CZK')]);
            echo $this->Form->control('requested_amount', ['label' => $labels['requested_amount'], 'required'=>false, 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->requested_amount, 'CZK')]);
            echo $this->Form->control('total_income', ['label' => $labels['total_income'], 'required'=>false, 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_other_subsidy, 'CZK')]);
            echo $this->Form->control('percentage', ['label' => $labels['percentage'], 'required'=>false, 'disabled' => true, 'size' => 3, 'type' => 'text', 'value' => '']);
            /*echo $this->Form->control('total_own_sources', ['label' => __('Součet financí z jiných zdrojů'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_own_sources, 'CZK')]);*/
            ?>
        </div>
        <?php
        $request_sum = round(isset($request->subsidy_paid) ? $request->subsidy_paid : $request->konecna_castka, 2);
        ?>
        <?php echo __('Částky jsou automaticky vypočteny vyplněním níže uvedených sekcí:'); ?>
    </div>
</div>


<?php foreach ($budgetHandler->getSections() as $sectionId => $sectionParams) : ?>
    <?php
    $sectionFilledRows = $budgetHandler->getSectionFilledRows($sectionId);
    $sectionType = $sectionParams['type'] ?? 'default';
    $sectionNoAdd = $sectionParams['noadd'] ?? false;
    $sectionTitle = $sectionParams['title'];
    $saveButton = $sectionParams['savebutton'] ?? false;
    $cardOpenTitle = $sectionParams['cardopen'] ?? false;
    $cardNoBody = $sectionParams['no_body'] ?? false;
    $cardClose = $sectionParams['cardclose'] ?? false;
    ?>

    <?php if ($cardOpenTitle) : ?>
        <div class="card mt-2">
            <div class="card-header">
                <h2><?= $cardOpenTitle ?></h2>
            </div>
            <div class="card-body table-responsive">
            <?php endif; ?>
            <div class="card mt-2">
                <?php if ($sectionTitle) : ?>
                    <div class="card-header">
                        <h5><?= $sectionTitle ?></h5>
                    </div>
                <?php else : ?>
                    <!-- no title -->
                <?php endif; ?>
                <?php if (!$cardNoBody) : ?>
                    <div class="card-body table-responsive">
                        <?php if ($sectionType == 'default') : ?>
                            <table class="table table-bordered with-inputs" id="section-<?= $sectionId ?>">
                                <colgroup>
                                    <col class="w-25">
                                    <col class="w-15">
                                    <col class="w-15">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <?php foreach ($budgetHandler->getSectionHeaders($sectionId) as $columnHeader) : ?>
                                            <th><?= $columnHeader ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for ($sectionRowCounter = 0; $sectionRowCounter < 50; $sectionRowCounter++) : ?>
                                        <tr class="<?= $sectionRowCounter >= $sectionFilledRows ? 'd-none' : 'visible' ?>">
                                            <?php
                                            foreach ($budgetHandler->getSectionColumns($sectionId) as $columnId => $columnExtra) : ?>
                                                <?php if (($prefilled_value = $budgetHandler->disabledInPrefills($sectionId, $sectionRowCounter, $columnId))) {
                                                    $columnExtra['readonly'] = true;
                                                } ?>
                                                <td class="<?= ($columnExtra['disabled'] ?? false) ? 'bg-dark' : '' ?>">
                                                    <?php
                                                    echo $this->Form->control(sprintf('%s.%d.%d.%d', $budget_id, $sectionId, $sectionRowCounter, $columnId), $columnExtra);
                                                    ?>
                                                </td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endfor; ?>
                                    <?php if (!$sectionNoAdd) : ?>
                                        <tr>
                                            <td class="p-0">
                                                <div class="row no-gutters">
                                                    <div class="col">
                                                        <a class="btn btn-success w-100 row-add" style="margin: 0; border: 0; color: white;">+</a>
                                                    </div>
                                                    <div class="col">
                                                        <a class="btn btn-danger w-100 row-remove" style="margin: 0; border: 0; color: white;">-</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                </tbody>
                                <tfoot>
                                    <tr class="thead-dark">
                                        <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes) : ?>
                                            <th <?php foreach ($attributes as $name => $value) {
                                                    echo sprintf('%s="%s"', $name, $value);
                                                } ?>><?= $attributes['text'] ?? '' ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                </tfoot>
                            </table>
                        <?php endif; ?>
                        <?php if ($sectionType == 'sum') : ?>
                            <table class="table table-bordered with-inputs" id="section-<?= $sectionId ?>">
                                <colgroup>
                                    <col class="w-25">
                                    <col class="w-15">
                                    <col class="w-15">
                                </colgroup>
                                <tr class="thead-dark">
                                    <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes) : ?>
                                        <th <?php foreach ($attributes as $name => $value) {
                                                echo sprintf('%s="%s"', $name, $value);
                                            } ?>><?= $attributes['text'] ?? '' ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </table>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if ($saveButton) : ?>
                    <div class="card-footer font-weight-bold">
                        <?php

                        echo $this->Form->submit(__('Uložit'));

                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if ($cardClose) : ?>
            </div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
<div>
    <style type="text/css">
        td.bg-dark input:disabled {
            background-color: transparent;
            color: white;
        }
    </style>
    <script type="text/javascript">
        function updateRequired() {
            $('tr.d-none input[required]').prop('required', false);
            $('tr.visible input[is-required=1]').prop('required', true);
        }

        function formatFloat(value, sumFunction) {
            value = parseInt(value)
            switch (sumFunction) {
                case 'currency':
                    var formatter = new Intl.NumberFormat('cs-CZ', {
                        style: 'currency',
                        currency: 'CZK',
                        maximumFractionDigits: 2
                    });
                    value = formatter.format(value);
                    break;
                case 'decimal':
                    value = value.toFixed(2);
                    break;
            }
            return value;
        }

        /**
         * Updates special rows where it's value is calculated as x*y=z, where x and y are cell sibblings to the left
         *
         */
        function updateMultiplies() {
            $('[data-multiply=true]').each(
                function() {
                    var id = $(this).attr('id');
                    var idParts = id.split('-');
                    var rowPrefix = '#' + idParts[0] + '-' + idParts[1] + '-' + idParts[2];
                    var idX = rowPrefix + '-' + (parseInt(idParts[3]) - 2);
                    var idY = rowPrefix + '-' + (parseInt(idParts[3]) - 1);
                    var z = parseInt($(idX).val()) * parseInt($(idY).val());
                    $(this).val(z)
                }
            )
        }

        function updateSums() {
            $('[data-fill=sum]').each(
                function() {
                    let $targetSum = 0;
                    let $targetId = $(this).attr('data-target-id');
                    $('[data-sum-to=' + $targetId + ']').not(':disabled').each(
                        function(i, el) {
                            let val = parseInt($(this).val());
                            if (!isNaN(val)) {
                                $targetSum += val;
                            }
                        }
                    )
                    $('[data-run-sum-to=' + $targetId + ']').not(':disabled').each(
                        function(i, el) {
                            let val = parseInt($(this).val());
                            if (!isNaN(val)) {
                                $targetSum += val;
                            }
                        }
                    )
                    $('[data-run2-sum-to=' + $targetId + ']').each(
                        function(i, el) {
                            console.log("summing");
                            let val = parseInt($(this).val());
                            if (!isNaN(val)) {
                                $targetSum += val;
                            }
                        }
                    )
                    $('[data-run3-sum-to=' + $targetId + ']').each(
                        function(i, el) {
                            console.log("summing");
                            let val = parseInt($(this).val());
                            if (!isNaN(val)) {
                                $targetSum += val;
                            }
                        }
                    )
                    $(this).html(formatFloat($targetSum, 'currency'))
                    $(this).data('rawsum', $targetSum)
                }
            )
            let $total_costs_raw = $('[data-target-id=expenses]').data('rawsum')
            let $total_costs = formatFloat($total_costs_raw, 'currency')
            let $total_income_raw = $('[data-target-id=income]').data('rawsum')
            let $total_income = formatFloat($total_income_raw, 'currency')
            let $requested_amount_raw = $('[data-target-id=requested]').data('rawsum')
            let $requested_amount = formatFloat($requested_amount_raw, 'currency')
            let $percentage = 0 + ' %'
            if ($total_costs_raw > 0) {
                $percentage = Math.ceil($requested_amount_raw / $total_costs_raw * 100) + ' %'
            }
            $('input[name=total_costs]').val($total_costs)
            $('input[name=total_income]').val($total_income)
            $('input[name=requested_amount]').val($requested_amount)
            //$('input[name=ulgrant]').val($requested_amount_raw) // Only in VZ15
            $('input[name=percentage]').val($percentage)
        }

        $(function() {
            updateSums();
            $('textarea').each(function() {
                this.setAttribute('style', 'height:' + (Math.max(this.scrollHeight, 36)) + 'px;overflow-y:hidden;');
            }).on('input', function() {
                this.style.height = 'auto';
                this.style.height = (Math.max(this.scrollHeight, 36)) + 'px';
            });

            $(".row-add").click(function() {
                let $target = $('tr.d-none', $(this).closest('table')).first();
                $target.toggleClass('d-none').toggleClass('visible');
                updateRequired();
            });
            $(".row-remove").click(function() {
                let $target = $('tr.visible', $(this).closest('table')).last();
                let $anyDisabled = false;
                $target.find("[readonly='readonly']").each(function(i, el) {
                    $anyDisabled = true
                })
                if (!$anyDisabled) {
                    $target.toggleClass('d-none').toggleClass('visible');
                    $("input", $target).val("").change();
                    updateRequired();
                }
            });
            $("input[type=number]:enabled").on('change keyup paste', function() {
                updateMultiplies();
                updateSums();
            });
            $("#change-request-submit").click(
                function(e) {
                    let $requested_amount_raw = $('[data-target-id=requested]').data('rawsum');
                    let $total_costs_raw = $('[data-target-id=expenses]').data('rawsum');
                    let $total_income_raw = $('[data-target-id=income]').data('rawsum');
                    if ($total_costs_raw != $total_income_raw) {
                        e.preventDefault();
                        let $gap = Math.abs($total_costs_raw - $total_income_raw);
                        $('#budget-gap').html('<strong>' + formatFloat($gap, 'currency') + '</strong>');
                        $("#budget-equal").modal('show');
                    } else {
                        if ($requested_amount_raw != <?= $request_sum ?>) {
                            e.preventDefault();
                            let $gap = $requested_amount_raw - <?= $request_sum ?>;
                            $('#budget-gap').html('<strong>' + formatFloat($gap, 'currency') + '</strong>');
                            $("#budget-balance").modal('show');
                        }
                    }
                });

        });
    </script>
    <style>
        .altered {
            background-color: rgb(220, 53, 69, 0.5) !important;
        }
    </style>
    <?= $this->Form->end(); ?>