<?php

use App\Budget\P14ProjectBudget;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $budget RequestBudget
 */

$this->assign('title', __('Rozpočet projektu'));

$this->Html->css('table-with-inputs.css', ['block' => true]);
$budgetHandler = new P14ProjectBudget();
$budgetHandler->loadBudget($budget);
echo $this->Form->create($budget);
?>

    <div class="card mt-2">
        <div class="card-header">
            <h2 class="card-title"><?= $this->fetch('title') ?></h2>
            <span><?= __('Vyplňte všechny relevantní položky. V případě potřeby přidejte další řádky v jednotlivých sekcích.') ?></span>
        </div>
        <div class="card-header">
            <div class="font-weight-bold">
                <?php
                echo $this->Form->control('requested_amount', ['label' => __('Požadovaná výše dotace (Kč)'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->requested_amount, 'CZK')]);
                echo $this->Form->control('total_own_sources', ['label' => __('Součet financí z jiných zdrojů'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_own_sources, 'CZK')]);
                echo $this->Form->control('total_costs', ['label' => __('Celkové náklady/výdaje na projekt'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_costs, 'CZK')]);
                ?>
            </div>
            <?php
            echo __('Částky jsou automaticky vypočteny vyplněním níže uvedených sekcí');
            ?>
        </div>
    </div>

<?php foreach ($budgetHandler->getSections() as $sectionId => $sectionTitle): ?>
    <?php
    $sectionFilledRows = $budgetHandler->getSectionFilledRows($sectionId);
    ?>
    <div class="card mt-2">
        <div class="card-header">
            <h2><?= $sectionTitle ?></h2>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered with-inputs" id="section-<?= $sectionId ?>">
                <colgroup>
                    <col class="w-25">
                    <col class="w-15">
                    <col class="w-15">
                    <col class="w-25">
                    <col class="w-20">
                </colgroup>
                <thead>
                <tr>
                    <?php foreach ($budgetHandler->getSectionHeaders($sectionId) as $columnHeader): ?>
                        <th><?= $columnHeader ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <?php for ($sectionRowCounter = 0; $sectionRowCounter < $budgetHandler->getSectionMaxRows($sectionId); $sectionRowCounter++): ?>
                    <tr class="<?= $sectionRowCounter >= $sectionFilledRows ? 'd-none' : 'visible' ?>">
                        <?php foreach ($budgetHandler->getSectionColumns($sectionId) as $columnId => $columnExtra): ?>
                            <td class="<?= ($columnExtra['disabled'] ?? false) ? 'bg-dark' : '' ?>">
                                <?= $this->Form->control(sprintf('%s.%d.%d.%d', P14ProjectBudget::BUDGET_ID, $sectionId, $sectionRowCounter, $columnId), $columnExtra) ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endfor; ?>
                <tr>
                    <td class="p-0">
                        <div class="row no-gutters">
                            <div class="col">
                                <a class="btn btn-success w-100 row-add"
                                   style="margin: 0; border: 0; color: white;">+</a>
                            </div>
                            <div class="col">
                                <a class="btn btn-danger w-100 row-remove"
                                   style="margin: 0; border: 0; color: white;">-</a>
                            </div>
                        </div>
                    </td>
                    <td colspan="4"></td>
                </tr>
                </tbody>
                <tfoot>
                <tr class="thead-dark">
                    <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes): ?>
                        <th <?php foreach ($attributes as $name => $value) {
                            echo sprintf('%s="%s"', $name, $value);
                        } ?>><?= $attributes['text'] ?? '' ?></th>
                    <?php endforeach; ?>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="card-footer font-weight-bold">
            <?php
            echo $this->Form->submit(__('Uložit'));
            ?>
        </div>
    </div>
<?php endforeach; ?>
    <style type="text/css">
        td.bg-dark input:disabled {
            background-color: transparent;
            color: white;
        }
    </style>
    <script type="text/javascript">
        function updateRequired() {
            $('tr.d-none input[required]').prop('required', false);
            $('tr.visible input[is-required=1]').prop('required', true);
        }

        function formatFloat(value, sumFunction) {
            switch (sumFunction) {
                case 'currency':
                    value = value.toFixed(2) + " Kč";
                    break;
                case 'decimal':
                    value = value.toFixed(2);
                    break;
            }
            return value;
        }

        $(function () {
            $('textarea').each(function () {
                this.setAttribute('style', 'height:' + (Math.max(this.scrollHeight, 36)) + 'px;overflow-y:hidden;');
            }).on('input', function () {
                this.style.height = 'auto';
                this.style.height = (Math.max(this.scrollHeight, 36)) + 'px';
            });

            $(".row-add").click(function () {
                let $target = $('tr.d-none', $(this).closest('table')).first();
                $target.toggleClass('d-none').toggleClass('visible');
                updateRequired();
            });
            $(".row-remove").click(function () {
                let $target = $('tr.visible', $(this).closest('table')).last();
                $target.toggleClass('d-none').toggleClass('visible');
                $("input", $target).val("").change();
                $("textarea", $target).val("").change();
                updateRequired();
            });
            $("input[type=number]:enabled").on('change keyup paste', function () {
                let $row = $(this).closest('tr');
                let $table = $(this).closest('table');
                let $sumType = $(this).attr('sum-type');
                let $rowSum = $("input[disabled=disabled][type=text]", $row);
                let $colSum = $("th.colsum[sum-type=" + $sumType + "]", $table);

                let _colsum = 0, _rowsum = 0;
                $("input[type=number][sum-type=" + $sumType + "]", $table).each(function () {
                    let val = parseInt($(this).val());
                    if (!isNaN(val)) {
                        _colsum += val;
                    }
                });
                $colSum.text(formatFloat(_colsum, 'currency'));
                $("input[type=number]", $row).not(':disabled').each(function () {
                    let val = parseInt($(this).val());
                    if (!isNaN(val)) {
                        _rowsum += val;
                    }
                });
                $rowSum.val(formatFloat(_rowsum, 'currency'));
                $rowSum.data('rawsum', _rowsum);

                let _tableSum = 0;
                $("input[disabled=disabled][type=text]", $table).each(function () {
                    let val = parseInt($(this).data('rawsum'));
                    if (!isNaN(val)) {
                        _tableSum += val;
                    }
                });
                $("th.colsum[sum-type=" + $rowSum.attr('sum-type') + "]", $table).text(formatFloat(_tableSum, 'currency'));
            });
        });
    </script>

<?= $this->Form->end(); ?>