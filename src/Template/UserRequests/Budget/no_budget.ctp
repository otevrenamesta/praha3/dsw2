<?php

use App\Model\Entity\BudgetItemType;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $budget RequestBudget
 */

 // This template should never be called as null budget (see #380) has no editable interface, 
 // but a placeholder doesn't cost us anything and is better than an error
 return;
 ?>