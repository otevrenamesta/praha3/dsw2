<?php

use App\Budget\P4ProjectBudget;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 * @var $budget RequestBudget
 * @var $design_id DesignID
 */

$this->assign('title', __('Rozpočet projektu'));

$this->Html->css('table-with-inputs.css', ['block' => true]);

$budgetHandler = new P4ProjectBudget($design_id);
$budgetHandler->loadBudget($budget);
$budget_id = $budgetHandler->budget_id;

echo $this->Form->create($budget);
?>
<div class="card mt-2">
    <div class="card-header">
        <h2 class="card-title"><?= $this->fetch('title') ?></h2>
        <span><?= __('Vyplňte všechny relevantní položky. V případě potřeby přidejte další řádky v jednotlivých sekcích.') ?></span>
    </div>
    <div class="card-header">
        <div class="font-weight-bold">
            <?php
            echo $this->Form->control('total_costs', ['label' => __('Celkové náklady na projekt'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_costs, 'CZK')]);
            echo $this->Form->control('total_own_sources', ['label' => __('Financování z vlastních zdrojů'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_own_sources, 'CZK')]);
            echo $this->Form->control('requested_amount', ['label' => __('Požadovaná dotace od Prahy 4'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->requested_amount, 'CZK')]);
            echo $this->Form->control('total_other_subsidy', ['label' => __('Financování z jiných zdrojů'), 'disabled' => true, 'type' => 'text', 'value' => Number::currency($budget->total_other_subsidy, 'CZK')]);
            echo $this->Form->control('percentage', ['label' => __('Podíl požadované dotace na celkových nákladech'), 'disabled' => true, 'size' => 3, 'type' => 'text', 'value' => '']);
            ?>
        </div>
        <?php
        echo __('Částky jsou automaticky vypočteny vyplněním níže uvedených sekcí');
        ?>
    </div>
</div>


<?php foreach ($budgetHandler->getSections() as $sectionId => $sectionParams) : ?>
    <?php
    $sectionFilledRows = $budgetHandler->getSectionFilledRows($sectionId);
    $sectionType = $sectionParams['type'] ?? 'default';
    $sectionTitle = $sectionParams['title'];
    $saveButton = $sectionParams['savebutton'] ?? false;
    $cardOpenTitle = $sectionParams['cardopen'] ?? false;
    $cardClose = $sectionParams['cardclose'] ?? false;
    ?>

    <?php if ($cardOpenTitle) : ?>
        <div class="card mt-2">
            <div class="card-header">
                <h2><?= $cardOpenTitle ?></h2>
            </div>
            <div class="card-body table-responsive">
            <?php endif; ?>
            <div class="card mt-2">
                <?php if ($sectionTitle) : ?>
                    <div class="card-header">
                        <h5><?= $sectionTitle ?></h5>
                    </div>
                <?php else : ?>
                    <!-- no title -->
                <?php endif; ?>
                <div class="card-body table-responsive">
                    <?php if ($sectionType == 'default') : ?>
                        <table class="table table-bordered with-inputs" id="section-<?= $sectionId ?>">
                            <colgroup>
                                <col class="w-25">
                                <col class="w-15">
                                <col class="w-15">
                            </colgroup>
                            <thead>
                                <tr>
                                    <?php foreach ($budgetHandler->getSectionHeaders($sectionId) as $columnHeader) : ?>
                                        <th><?= $columnHeader ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($sectionRowCounter = 0; $sectionRowCounter < $budgetHandler->getSectionMaxRows($sectionId); $sectionRowCounter++) : ?>
                                    <tr class="<?= $sectionRowCounter >= $sectionFilledRows ? 'd-none' : 'visible' ?>">
                                        <?php foreach ($budgetHandler->getSectionColumns($sectionId) as $columnId => $columnExtra) : ?>
                                            <?php if ($budgetHandler->disabledInPrefills($sectionId, $sectionRowCounter, $columnId)) {
                                                $columnExtra['readonly'] = true;
                                            } ?>
                                            <td class="<?= ($columnExtra['disabled'] ?? false) ? 'bg-dark' : '' ?>">
                                                <?= $this->Form->control(sprintf('%s.%d.%d.%d', $budget_id, $sectionId, $sectionRowCounter, $columnId), $columnExtra) ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endfor; ?>
                                <tr>
                                    <td class="p-0">
                                        <div class="row no-gutters">
                                            <div class="col">
                                                <a class="btn btn-success w-100 row-add" style="margin: 0; border: 0; color: white;">+</a>
                                            </div>
                                            <div class="col">
                                                <a class="btn btn-danger w-100 row-remove" style="margin: 0; border: 0; color: white;">-</a>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="thead-dark">
                                    <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes) : ?>
                                        <th <?php foreach ($attributes as $name => $value) {
                                                echo sprintf('%s="%s"', $name, $value);
                                            } ?>><?= $attributes['text'] ?? '' ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </tfoot>
                        </table>
                    <?php endif; ?>
                    <?php if ($sectionType == 'sum') : ?>
                        <table class="table table-bordered with-inputs" id="section-<?= $sectionId ?>">
                            <colgroup>
                                <col class="w-25">
                                <col class="w-15">
                                <col class="w-15">
                            </colgroup>
                            <tr class="thead-dark">
                                <?php foreach ($budgetHandler->getSectionFooters($sectionId) as $columnId => $attributes) : ?>
                                    <th <?php foreach ($attributes as $name => $value) {
                                            echo sprintf('%s="%s"', $name, $value);
                                        } ?>><?= $attributes['text'] ?? '' ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </table>
                    <?php endif; ?>
                </div>
                <?php if ($saveButton) : ?>
                    <div class="card-footer font-weight-bold">
                        <?php
                        echo $this->Form->submit(__('Uložit'));
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if ($cardClose) : ?>
            </div>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
<div>
    <style type="text/css">
        td.bg-dark input:disabled {
            background-color: transparent;
            color: white;
        }
    </style>
    <script type="text/javascript">
        function updateRequired() {
            $('tr.d-none input[required]').prop('required', false);
            $('tr.visible input[is-required=1]').prop('required', true);
        }

        function formatFloat(value, sumFunction) {
            value = parseInt(value)
            switch (sumFunction) {
                case 'currency':
                    var formatter = new Intl.NumberFormat('cs-CZ', {
                        style: 'currency',
                        currency: 'CZK',
                        maximumFractionDigits: 2
                    });
                    value = formatter.format(value); 
                    break;
                case 'decimal':
                    value = value.toFixed(2);
                    break;
            }
            return value;
        }

        function gVO(id) { // Get (integer) value of by id
            let val = parseInt($('#' + id).val());
            if (!isNaN(val)) {
                return val    
            } else {
                return 0 
            }
        }

        function updateSums() {
            $('[data-row-formula=P4custom]').each(
                function() {
                    var id = $(this).attr('id');
                    var p = id.split('-');
                    var result = gVO(p[0] + '-' + p[1] + '-' + p[2] + '-1') - gVO(p[0] + '-' + p[1] + '-' + p[2] + '-2') - gVO(p[0] + '-' + p[1] + '-' + p[2] + '-4') ; 
                    $(this).val(result);
                }
            )
            $('[data-fill=sum]').each(
                function() {
                    let $targetSum = 0;
                    let $targetId = $(this).attr('data-target-id');
                    $('[data-sum-to=' + $targetId + ']').not(':disabled').each(
                        function(i, el) {
                            let val = parseInt($(this).val());
                            if (!isNaN(val)) {
                                $targetSum += val;
                            }
                        }
                    )
                    $(this).html(formatFloat($targetSum, 'currency'))
                    $(this).data('rawsum', $targetSum)
                }
            )
            let $total_costs_raw = $('[data-target-id=expenses]').data('rawsum')
            let $total_costs = formatFloat($total_costs_raw,'currency')
            let $total_own_sources_raw = $('[data-target-id=own_sources]').data('rawsum')
            let $total_own_sources = formatFloat($total_own_sources_raw,'currency')
            let $requested_amount_raw = $('[data-target-id=requested]').data('rawsum')
            let $requested_amount = formatFloat($requested_amount_raw,'currency')
            let $total_other_subsidy_raw = $('[data-target-id=other_subsidy]').data('rawsum')
            let $total_other_subsidy = formatFloat($total_other_subsidy_raw,'currency')


            let $percentage = 0 + ' %'
            if ($total_costs_raw > 0) {
                $percentage = Math.ceil($requested_amount_raw / $total_costs_raw * 100) + ' %'
            }
            $('input[name=total_costs]').val($total_costs)
            $('input[name=total_own_sources]').val($total_own_sources)
            $('input[name=requested_amount]').val($requested_amount)
            $('input[name=percentage]').val($percentage)
            $('input[name=total_other_subsidy]').val($total_other_subsidy)
        }

        $(function() {
            updateSums();
            $('textarea').each(function() {
                this.setAttribute('style', 'height:' + (Math.max(this.scrollHeight, 36)) + 'px;overflow-y:hidden;');
            }).on('input', function() {
                this.style.height = 'auto';
                this.style.height = (Math.max(this.scrollHeight, 36)) + 'px';
            });

            $(".row-add").click(function() {
                let $target = $('tr.d-none', $(this).closest('table')).first();
                $target.toggleClass('d-none').toggleClass('visible');
                updateRequired();
            });
            $(".row-remove").click(function() {
                let $target = $('tr.visible', $(this).closest('table')).last();
                let $anyDisabled = false;
                $target.find("[readonly='readonly']").each(function(i, el) {
                    $anyDisabled = true
                })
                if (!$anyDisabled) {
                    $target.toggleClass('d-none').toggleClass('visible');
                    $("input", $target).val("").change();
                    updateRequired();
                }
            });
            $("input[type=number]:enabled").on('change keyup paste', function() {
                updateSums();
            });
        });
    </script>

    <?= $this->Form->end(); ?>