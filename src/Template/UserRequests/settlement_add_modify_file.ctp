<?php

use App\Model\Entity\Attachment;
use App\Model\Entity\Settlement;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $settlement Settlement
 * @var $attachment Attachment
 * @var $attachmentTypes array
 */
$this->assign('title', $attachment->isNew() ? __('Nová příloha') : sprintf("%s: %s", __('Příloha'), h($attachment->file->original_filename)));
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $attachment->isNew() ? $this->fetch('title') : h($attachment->file->original_filename) ?>
    </h2>
    <div class="card-body">
        <?php

        echo $this->Form->create($attachment, ['type' => 'file']);
        echo $this->Form->control('filedata', ['type' => 'file', 'label' => $attachment->file ? $attachment->file->original_filename : __('Vyberte soubor k nahrání')]);
        echo $this->element('fileinfo', ['file' => $attachment->file]);
        if ($attachment->isNew()) {
            echo $this->Form->control('settlement_attachment_type_id', ['options' => $attachmentTypes, 'label' => __('Vyberte typ přílohy')]);
        }
        echo $this->Form->submit(__('Uložit'));
        echo $this->Form->end();
        ?>
    </div>
</div>
