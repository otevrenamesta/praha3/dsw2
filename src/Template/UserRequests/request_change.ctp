<?php

use App\Model\Entity\Identity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestChange;
use App\Model\Entity\RequestBudgetChange;
use App\Model\Entity\RequestState;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $public_income_history array
 */

$P4Exception =  OrganizationSetting::isException(OrganizationSetting::P4);

$this->assign('title', h($request->name));
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h1 class="d-inline"><?= h($request->name) ?></h1>
            </div>
            <div class="col text-right">
                <?= sprintf("%s %d", __('žádost č.'), $request->id) ?><br />
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th><?= __('Požadavek') ?></th>
                    <th><?= __('Stav požadavku') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?= __('Zadat změnu v identitě') ?>
                    </td>
                    <td>
                        <?php

                        $class = 'fa-check text-success';
                        $text = __('Je možné zadat změny');
                        ?><i class="fas font-weight-bold <?= $class ?>"></i> <?= $text ?>
                    </td>
                    <td>
                        <?= $this->Html->link(__('Provést změny v identitě'), ['_name' => 'update_self_info'], ['class' => 'btn btn-success']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= __('Požádat o změnu v rozpočtu projektu') ?>
                    </td>
                    <td>
                        <?php
                        $out = '';
                        $disabled_class = '';
                        if (empty($request->request_budgets_changes)) {
                            $out = '<i class="fas font-weight-bold fa-check text-success"></i> ' . __('Je možné podat žádost o změnu');
                        } else {
                            $rbch = [];
                            // Sort by ID
                            foreach ($request->request_budgets_changes as $change) {
                                if ($change->status == RequestBudgetChange::CHANGE_ORIGINAL) continue;
                                $rbch[$change->id] = $change;
                            }
                            ksort($rbch);
                            foreach ($rbch as $id => $ch) {
                                if ($ch->status == RequestBudgetChange::CHANGE_ACCEPTED) {
                                    $out .= '<i class="fas font-weight-bold fa-check text-success"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' byla schválena.') . "<br />";
                                }
                                if ($ch->status == RequestBudgetChange::CHANGE_DECLINED) {
                                    $out .= '<i class="fas font-weight-bold fa-times text-danger"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' byla zamítnuta') . "<br />";
                                }
                                if ($ch->status == RequestBudgetChange::CHANGE_PENDING) {
                                    $out .= '<i class="fas fa-exclamation-triangle text-warning"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' čeká na schválení.') . '<br />' . __(' Dokud nebude schválena nebo zamítnuta, nelze podat další žádost.') . "<br />";
                                    $disabled_class = ' disabled';
                                }
                            }
                        }
                        echo $out;
                        ?>
                    </td>
                    <td>
                        <?= $this->Html->link(__('Zadat změnu v rozpočtu'), ['_name' => 'request_budget_change', $request->id], ['class' => 'btn btn-success' . $disabled_class]) ?>
                    </td>
                </tr>

                <?php
                foreach ($request->getForms() as $form) {
                ?>
                    <tr>
                        <td><?= sprintf("%s: %s", __("Požádat o změnu ve formuláři"), !empty($form->name_displayed) ? $form->name_displayed : $form->name) ?></td>
                        <td><?php
                            $isComplete = $request->isFormCompleted($form->id);
                            $class = 'fa-times text-danger';
                            if (!$form->allow_change) {
                                $out = '<i class="fas font-weight-bold fa-times text-danger"></i> ' . __('Nelze požádat o změnu');
                                $disabled_class = ' disabled';
                            } else {
                                $out = '';
                                $disabled_class = '';
                                $request_changes = [];
                                foreach ($request->request_changes as $change) {
                                    if ($change->form_id != $form->id) continue;
                                    $request_changes[] = $change;
                                }
                                if (empty($request_changes)) {
                                    $out = '<i class="fas font-weight-bold fa-check text-success"></i> ' . __('Je možné podat žádost o změnu');
                                } else {
                                    $rch = [];
                                    // Sort by ID
                                    foreach ($request_changes as $change) {
                                        if ($change->form_id != $form->id) continue;
                                        if ($change->status == RequestChange::CHANGE_ORIGINAL) continue;
                                        $rch[$change->id] = $change;
                                    }
                                    ksort($rch);
                                    foreach ($rch as $id => $ch) {
                                        if ($ch->status == RequestBudgetChange::CHANGE_ACCEPTED) {
                                            $out .= '<i class="fas font-weight-bold fa-check text-success"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' byla schválena.') . "<br />";
                                        }
                                        if ($ch->status == RequestBudgetChange::CHANGE_DECLINED) {
                                            $out .= '<i class="fas font-weight-bold fa-times text-danger"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' byla zamítnuta') . "<br />";
                                        }
                                        if ($ch->status == RequestBudgetChange::CHANGE_PENDING) {
                                            $out .= '<i class="fas fa-exclamation-triangle text-warning"></i> ' . __('Žádost o změnu z ') . $ch->created->i18nFormat() . __(' čeká na schválení.') . '<br />' . __(' Dokud nebude schválena nebo zamítnuta, nelze podat další žádost.') . "<br />";
                                            $disabled_class = ' disabled';
                                        }
                                    }
                                }
                            }
                            echo $out;
                            ?>
                        </td>
                        <td>
                            <?= $this->Html->link(__('Zadat změnu'), ['action' => 'formFillChange', 'id' => $request->id, 'form_id' => $form->id], ['class' => 'btn btn-success' . $disabled_class]) ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <strong><?= $request->program->name ?></strong>
        <div>
            <?= $request->program->description ?>
        </div>
        <strong><?= __('Dotační Výzva') ?>: <?= $request->appeal->name ?></strong>
        <div>
            <?= $request->appeal->description ?>
        </div>
    </div>
</div>