<?php

use App\Model\Entity\Request;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $request Request
 **/

$this->assign('title', __('Vyúčtovat dotaci'));
?>

<div class="card m-2">
    <div class="card-header">
        <h2><?= h($request->name) ?></h2>
        <strong>
            <?= __('Vyberte si prosím rozpracované vyúčtování nebo vytvořte vyúčtování k nevyúčtované částce') ?>
        </strong>
    </div>
    <div class="card-body">
        <?= __('Přehled plateb této žádosti') . ':' ?>
        <ul class="pb-2">
            <li>Schválená podpora celkem: <?= Number::currency($request->getFinalSubsidyAmount(), 'CZK') ?>
                <?php
                if (isset($request->original_subsidy_amount) && !empty($request->original_subsidy_amount)) {
                    echo '<ul>';
                    echo '<li>' . sprintf("%s: %s", __('Původní podpora celkem'), Number::currency($request->original_subsidy_amount, 'CZK')) . '</li>';
                    echo '<li>' . sprintf("%s: %s", __('Dofinancování (navýšení původně schválené podpory)'), Number::currency($request->final_subsidy_amount - $request->original_subsidy_amount, 'CZK')) . '</li>';
                    echo '</ul>';
                }
                ?>
            </li>
            <li>Vyplacená podpora: <?= Number::currency($request->getPaymentsSum(), 'CZK') ?></li>
            <li>Nečerpaná podpora: <?= Number::currency(max($request->getFinalSubsidyAmount() - $request->getPaymentsSum(), 0), 'CZK')  ?></li>
        </ul>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><?= __('ID') ?></th>
                    <th><?= __('Datum provedení / evidence platby') ?></th>
                    <th><?= __('Částka') ?></th>
                    <th><?= __('Typ platby') ?></th>
                    <th><?= __('Akce') ?></th>
                    <th><?= __('Ke stažení') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($request->getPayments() as $payment) :  ?>
                    <tr>
                        <td><?= !empty($payment->settlement_id) ? $payment->settlement_id : '' ?></td>
                        <td><?= $payment->created->format('d.m.Y') ?></td>
                        <td data-type="currency"><?= Number::currency($payment->amount_czk, 'CZK') ?></td>
                        <td>
                            <?= $payment->is_refund ? __('Vrácení částky z poskytnuté dotace') : __('Výplata podpory') ?>
                            <?= $payment->in_addition ? '<br />' . __('Dofinancování') : '' ?>
                        </td>
                        <td>
                            <?php
                            if (empty($payment->settlement_id)) {
                                if ($payment->is_refund) {
                                    echo __('Tuto platbu nelze vyúčtovat');
                                } else {
                                    echo $this->Html->link(__('Vyúčtovat tuto platbu'), ['action' => 'settlementCreate', 'id' => $request->id, 'payment_id' => $payment->id], ['class' => 'btn btn-success']);
                                }
                            } else {
                                $text = __('Otevřit detail vyúčtování');
                                switch ($payment->settlement->settlement_state_id) {
                                    case SettlementState::STATE_NEW:
                                    case SettlementState::STATE_FILLED_COMPLETELY:
                                        $text = __('Otevřít rozpracované vyúčtování');
                                        break;
                                    case SettlementState::STATE_SUBMITTED_READY_TO_REVIEW:
                                        $text = __('Vyúčtování bylo odesláno a čeká na zpracování');
                                        break;
                                    case SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES:
                                        $text = __('Vyúčtování bylo vráceno k opravě');
                                        break;
                                    case SettlementState::STATE_ECONOMICS_APPROVED:
                                        $text = __('Vyúčtování bylo odsouhlaseno');
                                        break;
                                    case SettlementState::STATE_SETTLEMENT_REJECTED:
                                        $text = __('Vyúčtování nevyhovuje podmínkám');
                                        break;
                                }
                                echo $this->Html->link($text, ['action' => 'settlementDetail', 'settlement_id' => $payment->settlement_id], ['class' => 'btn btn-primary']);
                            }
                            ?>
                        </td>
                        <td>
                            <?= !empty($payment->settlement_id) && $payment->settlement->settlement_state_id > SettlementState::STATE_NEW
                                ? $this->Html->link(
                                    __('Stáhnout PDF'),
                                    ['controller' => 'UserRequests', 'action' => 'settlementPdf', $payment->settlement_id],
                                    ['class' => 'btn btn-secondary']
                                )
                                : '' ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>