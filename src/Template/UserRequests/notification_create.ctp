<?php

use App\Model\Entity\Request;
use App\Model\Entity\RequestNotification;
use App\Model\Entity\RequestNotificationType;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $notification RequestNotification
 **/

$this->assign('title', __('Nové ohlášení'));

echo $this->Form->create($notification, ['type' => 'file']);
?>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= $this->fetch('title') ?></h2>
            <?= sprintf("%s: %s", __('Žádost'), h($request->name)) ?>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md">
                    <?php
                    echo $this->Form->control('request_notification_type_id', ['options' => RequestNotificationType::getTypesWithLabels(), 'label' => __('Typ ohlášení'), 'type' => 'radio', 'required' => true]);
                    ?>
                </div>
                <div class="col-md">
                    <?php
                    echo $this->Form->control('date_from', ['empty' => true, 'label' => sprintf("%s (%s)", __('Datum zahájení'), __('nepovinné, pokud se shoduje s datem ukončení'))]);
                    echo $this->Form->control('date_end', ['label' => __('Datum ukončení'), 'default' => date('Y-m-d')]);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <?php
                    echo $this->Form->control('costs_czk', ['type' => 'number', 'label' => __('Čerpaná částka'), 'max' => $request->getFinalSubsidyAmount(), 'min' => 0]);
                    ?>
                </div>
                <div class="col-md">
                    <?php
                    echo $this->Form->control('participants_count', ['type' => 'number', 'label' => __('Počet účastníků (předpokládaný)'), 'required' => false, 'default' => 0]);
                    ?>
                </div>
            </div>

            <?php
            echo $this->Form->control('location', ['label' => __('Místo konání'), 'data-noquilljs' => 'data-noquilljs']);
            ?>
            <div class="row">
                <div class="col-md">
                    <?php
                    echo $this->Form->control('garant_name', ['label' => __('Garant akce')]);
                    ?>
                </div>
                <div class="col-md">
                    <?php
                    echo $this->Form->control('garant_phone', ['label' => __('Tel. garanta akce')]);
                    ?>
                </div>
                <div class="col-md">
                    <?php
                    echo $this->Form->control('garant_email', ['label' => __('Email garanta akce'), 'type' => 'email']);
                    ?>
                </div>
            </div>
            <hr/>
            <?= $this->Form->control('files.0.filedata', ['type' => 'file', 'required' => false, 'label' => __('Nepovinná přiloha ohlášení, např. leták nebo fotografie z proběhlé události')]) ?>
            <hr/>
            <?php
            echo $this->Form->control('comment', ['label' => __('Poznámka k ohlášení'), 'data-noquilljs' => 'data-noquilljs', 'required' => false]);
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit() ?>
        </div>
    </div>
<?= $this->Form->end() ?>
<?= $this->element('filesize_check') ?>
