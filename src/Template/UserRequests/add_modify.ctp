<?php

use App\Budget\ProjectBudgetFactory;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $request Request
 * @var $appeals_programs array
 * @var $program_descriptions array
 * @var $programs_to_join_ids array
 * @var $requests_to_join_ids array
 */

$P4Exception =  OrganizationSetting::isException(OrganizationSetting::P4);
$mergingOfRequests = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MERGING_OF_REQUESTS);
$maxLength = 100; // See #413

// set different order - see #510
if ($request->appeal_id > 0 && !empty($appeals_programs)) {
    foreach ($appeals_programs as $key => $value) {
        if (strpos($key, "[#$request->appeal_id]") > 0) {
            unset($appeals_programs[$key]);
            $appeals_programs = [$key => $value] + $appeals_programs;
            break;
        }
    }
}

$this->assign('title', $request->isNew() ? __('Nová žádost o podporu') : __('Nastavení'));
echo $this->element('simple_chained_select', ['data' => $appeals_programs, 'selectedKey' => $request->program_id]);
?>
<div class="card">
    <h1 class="card-header"><?= $this->fetch('title') ?></h1>
    <div class="card-body">
        <?php
        echo $this->Form->create($request);
        echo $this->Form->control('program_id', [
            'class' => 'chainedSelect',
            'escape' => false,
            'label' => sprintf("%s<br/><small id='info-box' class='text-muted'>%s</small>", __('Vyberte si Dotační výzvu > Oblast podpory > Program, ve kterém chcete žádat'), __('Popis programu je uveden níže')),
            'options' => [],
            'required' => 'required',
            'type' => 'select',
        ]);
        echo $this->Form->control('name', [
            'escape' => false,
            'label' => sprintf("%s<br/><small class='text-muted'>%s</small>", __('Název projektu'), __('Maximální délka názvu je') . sprintf(" %s ", $maxLength) . __('znaků')),
            'maxlength' => $maxLength,
            'required' => true,
        ]);
        if (
            !$P4Exception &&
            (!($request->program instanceof Program) || ProjectBudgetFactory::getBudgetHandler($request->program->project_budget_design_id)->canUserEditRequestedAmount())
        ) {
            echo $this->Form->control(
                'request_budget.requested_amount',
                [
                    'default' => 0,
                    'escape' => false,
                    'label' => sprintf("%s<br/><small class='text-muted'>%s</small>", __('Požadovaná výše dotace (Kč)'), __('Částku můžete později upravit')),
                    'required' => false,
                    'type' => 'number',
                ]
            );
        }

        echo !$mergingOfRequests || !count($requests_to_join_ids ?? []) || !count($programs_to_join_ids ?? [])
            ? ''
            : '<div id="merged_request_box">' . $this->Form->control(
                'merged_request',
                [
                    'empty' => __('- nenavazovat -'),
                    'id' => 'merged_request',
                    'label' => __('Navázat vytvářenou žádost na již existující schválenou žádost'),
                    'options' => $requests_to_join_ids,
                    'type' =>  'select',
                ]
            ) . '</div>';
        echo $this->Form->button(
            '<i class="fas fa-check add-space"></i>' . ($request->isNew() ? __('Vytvořit novou žádost') : __('Uložit')),
            ['class' => 'btn btn-success', 'escape' => false, 'type' => 'submit']
        );
        echo $this->Form->control('appeal_id', ['value' => $request->appeal_id, 'type' => 'hidden']);
        $this->Form->unlockField('appeal_id');
        echo $this->Form->end();
        ?>
    </div>
    <div class="mt-3 pt-4 card-footer"></div>
</div>

<script type="text/javascript">
    const PROGRAM_DESCRIPTIONS = <?= json_encode($program_descriptions) ?>;
    const PROGRAM_IDS = <?= json_encode($programs_to_join_ids) ?>;

    function chained_select_callback(id) {
        let $target = $(".card-footer");
        if (id in PROGRAM_DESCRIPTIONS) {
            $target.html("<h4 id='popis-programu'><?= __('Popis programu') ?></h4><p>" + PROGRAM_DESCRIPTIONS[id] + "</p>");
            $("#info-box").show();
            var appeal = ($("#program-id").val()).match(/\[#(\d+)\]/);
            $("input[name='appeal_id']").val(Array.isArray(appeal) ? appeal.pop().toString() : '');
        } else {
            $target.html("<p class='alert alert-danger'><?= __('Není vybrán program') ?></p>");
            $("#info-box").hide();
        }

        if (PROGRAM_IDS.includes(Number(id))) {
            $("#merged_request_box").show();
        } else if (id) {
            $("#merged_request_box").hide();
            $("#merged_request").val('');
        }
    }

    $(document).ready(function() {
        $("#name").keyup(function() {
            $(this).val().length > 0 ? $(this).removeClass('is-invalid') : $(this).addClass('is-invalid');
        })
    })
</script>