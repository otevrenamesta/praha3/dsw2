<?php

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\HistoryIdentity;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $identity HistoryIdentity
 * @var $request Request
 * @var $this AppView
 */

$this->assign('title', h(!empty($identity->name) ? $identity->name : $identity->formatFirstLastName()));
if (isset($identity->histories) && is_array($identity->histories)) {
    array_multisort(array_column($identity->histories, 'year'), SORT_DESC, $identity->histories);
}
?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item mr-1" role="presentation" style="">
        <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
            <?= __('Souhrn') ?>
        </button>
    </li>
    <?php if ($request !== null) : ?>
        <li class="nav-item mr-1" role="presentation">
            <button class="nav-link" id="identity-tab" data-toggle="tab" data-target="#identity" type="button" role="tab" aria-controls="identity" aria-selected="false">
                <?= __('Identita žadatele') ?>
            </button>
        </li>
    <?php endif; ?>
    <li class="nav-item mr-1" role="presentation">
        <button class="nav-link" id="history-tab" data-toggle="tab" data-target="#history" type="button" role="tab" aria-controls="history" aria-selected="false">
            <?= __('Historie poskytnuté podpory') ?>
        </button>
    </li>
    <?php if ($request !== null) : ?>
        <li class="nav-item mr-1" role="presentation">
            <button class="nav-link" id="email-tab" data-toggle="tab" data-target="#email" type="button" role="tab" aria-controls="email" aria-selected="false">
                <?= __('Odeslat žadateli e-mail') ?>
            </button>
        </li>
    <?php endif; ?>
</ul>

<div class="tab-content" id="myTabContent">
    <div class="card tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="m-4">
            <h3><?= h(!empty($identity->name) ? $identity->name : $identity->formatFirstLastName()) ?></h3>
            <ul>
                <li><strong><?= __('IČO') ?> / <?= __('DIČ') ?>:</strong> <?= $identity->formatIcoDic() ?></li>
                <li><strong><?= __('ID datové schránky') ?>:</strong> <?= h($identity->databox_id) ?></li>
                <li><strong><?= __('E-mail') ?>:</strong> <?= h($identity->email) ?></li>
                <li><strong><?= __('Telefon') ?>:</strong> <?= h($identity->phone) ?></li>
                <li><strong><?= __('Jméno a příjmení') ?>:</strong> <?= h($identity->formatFirstLastName()) ?></li>
                <li><strong><?= __('Datum narození') ?>:</strong> <?= $identity->date_of_birth ? $identity->date_of_birth->nice() : '' ?></li>
            </ul>
        </div>


        <?php if (count($identity->histories)) : ?>
            <div class="m-4">
                <div class="row mb-2 mt-5">
                    <div class="col">
                        <h3><?= __('Historie poskytnuté podpory') ?></h3>
                    </div>
                    <div class="col text-right">
                        <?= (empty($identity->id) ? '' : $this->Html->link(
                            __('Přidat'),
                            ['action' => 'addModifyHistory', 'identity_id' => $identity->id],
                            ['class' => 'btn btn-success']
                        )) ?>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th><?= __('Rok') ?></th>
                            <th><?= __('Poskytovatel') ?></th>
                            <th class="w-25"><?= __('Název projektu') ?></th>
                            <th><?= __('Výše podpory') ?></th>
                            <th><?= __('Poznámka') ?></th>
                            <th><?= __('Vložil') ?></th>
                            <th><?= __('Stav žádosti') ?></th>
                            <th><?= __('Žádost') ?></th>
                            <th><?= __('Akce') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $hide = !OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES);
                        foreach ($identity->histories as $history) :
                        ?>
                            <tr class="<?= $hide && empty($history->public_income_source) ? '' : '' ?>">
                                <td><?= $history->year ?></td>
                                <td><?= (!empty($history->public_income_source) ? $history->public_income_source : OrgDomainsMiddleware::getCurrentOrganization()->name) ?></td>
                                <td><?= h($history->project_name) ?></td>
                                <td data-type="currency"><?= Number::currency($history->czk_amount, 'CZK') ?></td>
                                <td><?= $history->notes ?></td>
                                <td><?= (!empty($history->added_by) ? $history->added_by : __('úředník')) ?></td>
                                <td><?= (!empty($history->request_state) ? $history->request_state : '') ?></td>
                                <td><?= (!empty($history->request_id) ? $this->Html->link('Otevřít', ['_name' => 'my_teams_request_detail', 'id' => $history->request_id]) : '') ?></td>
                                <td>
                                    <?= (isset($history->no_edit) ? '' : $this->Html->link(
                                        __('Upravit'),
                                        ['action' => 'addModifyHistory', 'identity_id' => $identity->id, 'id' => $history->id]
                                    )) ?>
                                    <?= (isset($history->no_edit) ? '' : ', ' . $this->Html->link(
                                        __('Smazat'),
                                        ['action' => 'deleteHistory', 'identity_id' => $identity->id, 'id' => $history->id]
                                    )) ?>
                                    <?= (!isset($history->no_edit) ? '' : '<em>-' . __('není') . '-<em>') ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>

    <?php if ($request !== null) : ?>
        <div class="card tab-pane fade" id="identity" role="tabpanel" aria-labelledby="identity-tab">
            <div class="m-4">
                <h3 class="pb-2"><?= __('Identita žadatele') ?></h3>
                <?= $this->element('identity_full_table', ['user' => $request->user, 'request' => $request, 'identityDetail' => true]) ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="card tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
        <?= $this->element('simple_datatable'); ?>
        <div class="m-4">
            <h3 class="pb-2"><?= __('Historie poskytnuté podpory') ?></h3>
            <table class="table" id="dtable">
                <thead>
                    <tr>
                        <th><?= __('Rok') ?></th>
                        <th><?= __('Poskytovatel') ?></th>
                        <th class="w-25"><?= __('Název projektu') ?></th>
                        <th><?= __('Výše podpory') ?></th>
                        <th><?= __('Poznámka') ?></th>
                        <th><?= __('Vložil') ?></th>
                        <th><?= __('Stav žádosti') ?></th>
                        <th><?= __('Žádost') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $hide = !OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES);
                    foreach ($identity->histories as $history) :
                    ?>
                        <tr class="<?= $hide && empty($history->public_income_source) ? '' : '' ?>">
                            <td><?= $history->year ?></td>
                            <td><?= (!empty($history->public_income_source) ? $history->public_income_source : OrgDomainsMiddleware::getCurrentOrganization()->name) ?></td>
                            <td><?= h($history->project_name) ?></td>
                            <td data-type="currency"><?= Number::currency($history->czk_amount, 'CZK') ?></td>
                            <td><?= $history->notes ?></td>
                            <td><?= (!empty($history->added_by) ? $history->added_by : __('úředník')) ?></td>
                            <td><?= (!empty($history->request_state) ? $history->request_state : '') ?></td>
                            <td><?= (!empty($history->request_id) ? $this->Html->link('Otevřít', ['_name' => 'my_teams_request_detail', 'id' => $history->request_id]) : '') ?></td>
                            <td>
                                <?= (isset($history->no_edit) ? '' : $this->Html->link(
                                    __('Upravit'),
                                    ['action' => 'addModifyHistory', 'identity_id' => $identity->id, 'id' => $history->id]
                                )) ?>
                                <?= (isset($history->no_edit) ? '' : ', ' . $this->Html->link(
                                    __('Smazat'),
                                    ['action' => 'deleteHistory', 'identity_id' => $identity->id, 'id' => $history->id]
                                )) ?>
                                <?= (!isset($history->no_edit) ? '' : '<em>-' . __('není') . '-<em>') ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php if ($request !== null) : ?>
        <div class="card tab-pane fade" id="email" role="tabpanel" aria-labelledby="email-tab">
            <?= $this->Form->create(null) ?>
            <div class="m-4">
                <h3 class="pb-2"><?= __('Odeslat žadateli e-mail') ?></h3>
                <div class="row">
                    <div class="col-md">
                        <?php
                        echo $this->Form->control('_sender', [
                            'label' => __('Odesílatel'),
                            'disabled' => true,
                            'value' => $this->getSiteSetting(OrganizationSetting::EMAIL_FROM)
                        ]);
                        ?>
                    </div>
                    <div class="col-md">
                        <?php
                        echo $this->Form->control('_replyTo', [
                            'label' => __('Adresa pro odpověď na zasílaný e-mail'),
                            'disabled' => true,
                            'value' => OrgDomainsMiddleware::getCurrentOrganization()->getReplyToEmailAddress()

                        ]);
                        ?>
                    </div>
                </div>
                <?php
                echo $this->Form->control('_recipient', [
                    'label' => __('Příjemci'),
                    'disabled' => true,
                    'value' => join(", ", $request->user->getEmailRecipients($request->user_identity_version, true))
                ]);
                echo $this->Form->control('subject', [
                    'label' => __('Předmět e-mailu'),
                    'required' => true,
                    'default' => __('Upozornění k vaší žádosti o podporu')
                ]);
                echo $this->Form->control('contents', [
                    'label' => __('Kompletní text e-mailu'),
                    'required' => true,
                    'data-quilljs' => 'data-quilljs',
                    'rows' => 10,
                    'default' => sprintf(
                        __('Dobrý den') . ",<br/><br/><br/><br/>S pozdravem<br/>%s, v z. %s",
                        $this->getSiteSetting(OrganizationSetting::SITE_NAME),
                        $this->getCurrentUser()->email
                    )
                ]);
                ?>
            </div>
            <div class="m-4">
                <?= $this->Form->submit(__('Odeslat')) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    <?php endif; ?>
