<?php

use App\Model\Entity\HistoryIdentity;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $identities HistoryIdentity[]
 **/
$this->assign('title', __('Žadatelé (včetně historie)'));
echo $this->element('simple_datatable');
?>
<div class="row mb-2">
    <div class="col">
        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="col text-right">
        <?= $this->Html->link(__('Přidat nového žadatele'), ['action' => 'addModifyIdentity'], ['class' => 'btn btn-success mr-2']) ?>
        <?= $this->Html->link(__('Nahrát žadatele hromadně'), ['action' => 'uploadIdentities'], ['class' => 'btn btn-primary mr-2']) ?>
    </div>
</div>
<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Název žadatele') ?></th>
            <th><?= __('IČO') . '/' . __('DIČ') ?></th>
            <th><?= __('Datová schránka') ?></th>
            <th><?= __('E-mail') ?></th>
            <th><?= __('Telefon') ?></th>
            <th><?= __('Záznamů') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($identities as $identity) : ?>
            <tr>
                <td><?= (isset($identity->id_user_id) ? $identity->id_user_id : $identity->id) ?></td>
                <td><?= h(!empty($identity->name) ? $identity->name : $identity->formatFirstLastName()) ?></td>
                <td><?= $identity->formatIcoDic() . ($identity->date_of_birth ? ' nar. ' . $identity->date_of_birth->nice() : '') ?></td>
                <td><?= h($identity->databox_id) ?></td>
                <td><?= h($identity->email) ?></td>
                <td><?= h($identity->phone) ?></td>
                <td><?= count($identity->histories) ?></td>
                <td>
                    <?php
                    if (isset($identity->id) && !isset($identity->user_id)) {
                        echo $this->Html->link(__('Otevřít'), ['action' => 'detail', 'id' => $identity->id]);
                    } else {
                        echo $this->Html->link(__('Otevřít'), ['action' => 'detailFromUserId', 'user_id' => $identity->user_id]);
                    }
                    ?>
                    <?= isset($identity->no_edit) ? '' : ', ' . $this->Html->link(__('Upravit'), ['action' => 'addModifyIdentity', 'id' => $identity->id]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
