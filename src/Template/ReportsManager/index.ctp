<?php

use App\Model\Entity\Report;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $reports Report[]
 */

$this->assign('title', __('Sestavy'));
echo $this->element('simple_datatable');
?>

<div class="mb-2 alert alert-info">
    <?= __('V této sekci si můžete sestavit vlastní tabulky z dat v žádostech a v dotačním procesu') ?>
</div>

<div class="row mb-2">
    <div class="col-md">
        <h2><?= $this->fetch('title') ?></h2>
    </div>
    <div class="col-md text-right">
        <?= $this->Html->link(__('Nastavení Agendio sestav'), ['action' => 'agendioPresets'], ['class' => 'btn btn-primary m-2']) ?>
        <?= $this->Html->link(__('Vytvořit novou sestavu'), ['action' => 'addModify'], ['class' => 'btn btn-success m-2']) ?>
    </div>
</div>

<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __('ID') ?></th>
        <th><?= __('Název sestavy') ?></th>
        <th><?= __('Akce') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($reports as $report): ?>
        <tr>
            <td><?= $report->id ?></td>
            <td><?= $this->Html->link($report->name, ['action' => 'renderReport', 'id' => $report->id]) ?></td>
            <td>
                <?= $this->Html->link(__('Otevřít'), ['action' => 'renderReport', 'id' => $report->id]) ?>
                ,
                <?= $this->Html->link(__('Nastavení'), ['action' => 'addModify', 'id' => $report->id], ['class' => 'text-warning']) ?>
                ,
                <?= $this->Html->link(__('Smazat'), ['action' => 'delete', 'id' => $report->id], ['class' => 'text-danger']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
