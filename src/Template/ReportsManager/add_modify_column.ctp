<?php

use App\Model\Entity\Report;
use App\Model\Entity\ReportColumn;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $report Report
 * @var $column ReportColumn
 * @var $columnOrder array
 * @var $columnDataOptions array
 * @var $agendioColumnOptions array
 */

$this->assign('title', $report->isNew() ? __('Vytvořit nový sloupec') : __('Upravit sloupec'));
echo $this->element('simple_chained_select', ['data' => $columnDataOptions, 'selectedKey' => $column->path]);
echo $this->element('simple_chained_select', ['data' => $agendioColumnOptions, 'selectedKey' => $column->agendio_column_id, 'target' => '.chainedSelectAgendio']);
echo $this->Form->create($column);
?>
    <div class="m-2 alert alert-info">
    <?= __('Pokud vám v této sekci chybí nějaká data ze žádosti, identity žadatele nebo dotačního procesu, napište nám na dsw2@otevrenamesta.cz a my vám přidáme dle požadavku další data ke zobrazení v sestavách') ?>
    </div>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= $this->fetch('title') ?></h2>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('name', ['label' => __('Nadpis sloupce')]);
            echo $this->Form->control('path', ['label' => __('Data k zobrazení ve sloupci'), 'options' => [], 'class' => 'chainedSelect']);
            echo $this->Form->control('order', ['label' => __('Umístění sloupce v sestavě (sloupec za který je tento umístěn)'), 'options' => $columnOrder, 'empty' => __('Umístit jako první sloupec'), 'required' => false]);
            ?>
            <hr/>
            <?php
            echo $this->Form->control('agendio_column_id', ['label' => __('Pokud tento sloupec chcete použít v Agendio sestavě, vyberte odpovídající Agendio sestavu a konkrétní sloupec'), 'options' => [], 'class' => 'chainedSelectAgendio']);
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->button(__('Uložit sloupec'), ['class' => 'btn btn-success', 'type' => 'submit']) ?>
            <?php
            if (!$column->isNew()) {
                echo $this->Html->link(__('Vymazat tento sloupec'), ['action' => 'deleteColumn', 'id' => $column->report_id, 'column_id' => $column->id], ['class' => 'btn btn-danger']);
            }
            ?>
        </div>
    </div>
<?= $this->Form->end(); ?>