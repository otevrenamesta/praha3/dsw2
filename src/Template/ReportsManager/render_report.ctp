<?php

use App\Model\Entity\AgendioReport;
use App\Model\Entity\Program;
use App\Model\Entity\Report;
use App\Model\Entity\Request;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $agendio_presets array [string(preset storage key) => string(preset value, if set)]
 * @var $agendio_report null|AgendioReport
 * @var $appeals array
 * @var $programs Program[]
 * @var $report Report
 * @var $request_states array
 * @var $requests Request[]
 * @var $show_agendio_headers bool
 * @var $states array
 */

$this->assign('title', $report->name);
echo $this->element('simple_treeselect');

if (count($report->report_columns) > 0) {
    echo $this->element('simple_datatable');
}
echo $this->Form->create(null);
?>

<div class="card">
    <h2 class="card-header"><?= __('Filtrovat žádosti podle') ?></h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('program_ids', ['label' => __('Dotační oblasti, programy a pod-programy'), 'class' => 'treeselect', 'multiple' => true, 'options' => $programs]) ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('appeal_id', ['label' => __('Výzvy k podání žádostí o dotace'), 'options' => $appeals]) ?>
            </div>
            <div class="col-md-3">
                <?= $this->Form->control('request_state_id', ['label' => __('Stav žádosti'), 'options' => $states]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->button(__('Filtrovat'), ['class' => 'btn btn-success m-2', 'type' => 'submit']) ?>
        <?= $this->Form->button(__('Zobrazit jako Agendio Smlouva'), ['class' => 'btn btn-warning m-2', 'type' => 'submit', 'name' => 'agendio_report_id', 'value' => AgendioReport::AGENDIO_REPORT_SMLOUVA]) ?>
    </div>
</div>
<?php
echo $this->Form->end();
?>

<div class="alert alert-info mt-2">
    <?= __('Kliknutím na název sloupce můžete sloupec upravit nebo jej vymazat') ?>
</div>

<div class="card mt-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h2><?= $this->fetch('title') ?></h2>
            </div>
            <div class="col-md text-right">
                <?= $this->Html->link(__('Přidat sloupec'), ['action' => 'addModifyColumn', 'id' => $report->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <div class="card-body table-responsive">
        <table class="table table-bordered" id="dtable">
            <?php if ($agendio_report) : ?>
                <thead>
                    <tr>
                        <?php foreach ($agendio_report->agendio_columns as $agendio_column) : ?>
                            <th><?= $agendio_column->export_name ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
            <?php else : ?>
                <?php if (!empty($report->report_columns)) : ?>
                    <thead>
                        <tr>
                            <?php foreach ($report->report_columns as $column) : ?>
                                <?php foreach ($this->getColumnHeaders($column) as $columnHeader) : ?>
                                    <th><?= $this->Html->link($columnHeader, ['action' => 'addModifyColumn', 'id' => $column->report_id, 'column_id' => $column->id]) ?></th>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                <?php endif; ?>
            <?php endif; ?>

            <tbody>
                <?php foreach ($requests as $request) : ?>
                    <?php if ($agendio_report) : ?>
                        <tr>
                            <?php foreach ($agendio_report->agendio_columns as $agendio_column) : ?>
                                <td>
                                    <?= $this->extractReportColumnFirstValue($request, $report->getFirstAgendioColumn($agendio_column), $agendio_presets[$agendio_column->getPresetStorageKey()] ?? null) ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <?php foreach ($report->report_columns as $column) : ?>
                                <?php foreach ($this->extractReportColumn($request, $column) as $columnData) : ?>
                                    <td>
                                        <?= $columnData ?>
                                    </td>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
