<?php

use App\Model\Entity\Report;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $report Report
 */

$this->assign('title', $report->isNew() ? __('Vytvořit novou sestavu') : __('Upravit sestavu'));

echo $this->Form->create($report);
?>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= $this->fetch('title') ?></h2>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('name', ['label' => __('Název sestavy')]);
            echo $this->Form->control('is_archived', ['label' => __('Archivní, zaškrtněte pokud již sestava není aktuální'), 'type' => 'checkbox']);
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit(__('Uložit sestavu')) ?>
        </div>
    </div>
<?= $this->Form->end(); ?>