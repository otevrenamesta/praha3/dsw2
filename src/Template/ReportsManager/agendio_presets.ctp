<?php

use App\Model\Entity\AgendioReport;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $current_presets array [string(preset storage key) => string(current preset value)]
 * @var $reports AgendioReport[]
 */
$this->assign('title', __('Nastavení Agendio sloupců'));
echo $this->Form->create();
?>
<div class="alert alert-primary">
    <?= __('Jsou zobrazeny jen sloupce, pro které lze předvyplnit hodnotu, pokud vám zde chybí možnost nastavit konkrétní sloupec, napište nám na dsw2@otevrenamesta.cz') ?>
</div>

<?php foreach ($reports as $report) : ?>
    <div class="card">
        <div class="card-header">
            <h2><?= $report->label ?></h2>
        </div>
        <div class="card-body">
            <?php
            foreach ($report->agendio_columns as $column) {
                $storage_key = OrganizationSetting::getAgendioPresetKey($column->agendio_report_id, $column->export_name);
                $label = $column->label . '<small class="pl-2">[' . $column->export_name . ']</small>';
                echo $this->Form->control(
                    $storage_key,
                    [
                        'label' => $label,
                        'type' => 'text',
                        'default' => $current_presets[$storage_key] ?? '',
                        'escape' => false,
                    ]
                );
            }
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit(__('Uložit nastavení'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php endforeach; ?>

<?php
echo $this->Form->end();
?>
