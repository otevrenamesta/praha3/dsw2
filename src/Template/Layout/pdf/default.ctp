<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8" />
    <title>
    </title>
    <style type="text/css">
        <?= file_get_contents(WWW_ROOT . DS . 'css' . DS . 'bootstrap-4.4.1.min.css') ?>* {
            font-family: sans-serif;
        }

        .pdf-header {
            font-size: 80%;
            width: 90%;
        }

        .pdf-header-image {
            width: 70px;
        }

        .pdf-header-image img {
            max-height: 70px;
            width: auto;
        }

        .pdf-header-address p {
            margin: 0 1em;

        }

        .pdf-header-message {
            padding: 0.5em 0 1em 0;
        }
    </style>
</head>

<body>
    <div class="container-fluid clearfix">
        <?= $this->fetch('content') ?>
    </div>
</body>

</html>