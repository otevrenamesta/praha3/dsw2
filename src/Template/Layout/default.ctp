<?php

/**
 * @var $this AppView
 */

use App\Model\Entity\OrganizationSetting;
use App\View\AppView;
use Cake\Routing\Router;
use App\Middleware\OrgDomainsMiddleware;

?>
<!DOCTYPE html>
<html lang="cs">

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="referrer" content="origin-when-cross-origin">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon', OrganizationSetting::getFaviconFullPath());
    ?>

    <!-- detect es6 browser without features and es5 browsers only -->
    <script type="module" src="/js/es6.js"></script>
    <script nomodule src="/js/es5.js"></script>

    <?= $this->Html->script([
        'jquery-3.5.1.min.js',
        'popper-1.16.0.min.js',
        'bootstrap-4.4.1.min.js',
        'quill-1.3.7.min.js',
        'jquery.scrollTo-2.1.2.min.js',
        'font-awesome-5.13.0.all.min.js',
        'bs-custom-file-input-1.3.4.min.js',
        'jquery.textarea-auto-expand.js'
    ]) ?>

    <?= $this->Html->css([
        'bootstrap-4.4.1.min.css',
        'font-awesome-5.13.0.all.min.css',
        'quill-1.3.7.snow.css',
        'dsw2.css'
    ]) ?>

    <?= $this->Html->script('bootstrap-treeview.min') ?>
    <?= $this->Html->css('bootstrap-treeview.min') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <?php
    $custom_css = OrganizationSetting::getSetting(OrganizationSetting::CUSTOM_CSS);
    if (!empty(trim($custom_css))) :
    ?>
        <style type="text/css">
            <?= $custom_css ?>
        </style>
    <?php endif; ?>
    <style type="text/css">
        .testing-environment-banner {
            width: 100%;
            background-color: firebrick;
            color: white;
            text-align: center;
            font-weight: bold;
            padding: 3px;
        }
    </style>
</head>

<body>

    <nav class="navbar <?= ($this->isManager() || $this->isTeamMember()) ? 'navbar-expand-xl' : 'navbar-expand-lg' ?> navbar-light bg-light">
        <a class="navbar-brand mr-4" href="<?= $this->Url->build('/'); ?>">
            <?= $this->Html->image($this->getSiteLogo() ?: 'default.png', ['height' => 30, 'width' => 30, 'alt' => $this->getSiteName()]) ?>
            <?= $this->getSiteName() ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php
            if ($this->isUser()) {
                echo $this->element('main_menu_user');
            } else if ($this->isRequest()) {
                echo $this->element('main_menu_not_user');
            }
            ?>
        </div>
    </nav>
    <?php if (OrgDomainsMiddleware::isTestingEnvironment()) : ?>
        <div class="testing-environment-banner">         
        <i class="fa fa-american-sign-language-interpreting fa-lg" aria-hidden="true"></i> <?= __('PRACUJETE V TESTOVACÍM PROSTŘEDÍ'); ?>
        </div>
    <?php endif; ?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <?= $this->Html->link(__('Hlavní stránka'), '/') ?>
            </li>

            <?php if (isset($crumbs)) : ?>
                <?php foreach ($crumbs as $crumb_name => $crumb_path) : ?>
                    <li class="breadcrumb-item">
                        <?= $this->Html->link($crumb_name, (is_string($crumb_path) && Router::routeExists(['_name' => $crumb_path])) ? ['_name' => $crumb_path] : $crumb_path) ?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if ($this->getRequest() && $this->getRequest()->getRequestTarget() !== '/') : ?>
                <li class="breadcrumb-item active" aria-current="page">
                    <?= $this->Html->link($this->fetch('title'), $this->getRequest()->getRequestTarget()) ?>
                </li>
            <?php endif; ?>
        </ol>
    </nav>

    <div class="container-fluid clearfix mt-2">
        <?= $this->Flash->render() ?>
        <div id='main-content'>
            <?= $this->fetch('content') ?>
        </div>
    </div>

    <footer class="bg-light text-dark p-2 pt-3">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <?= OrganizationSetting::getContactInfo(' | ', true, true) ?>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    &copy; 2020-<?= date('Y', time()); ?> <a href="https://otevrenamesta.cz" class="text-dark">Otevřená města, z. s.</a>
                    | <a href="https://gitlab.com/otevrenamesta/praha3/dsw2" class="text-dark">GitLab</a>
                    <?php
                    $hide_about = OrganizationSetting::getSetting(OrganizationSetting::SITE_FOOTER_HIDE_ABOUT);
                    if (!$hide_about) { ?>
                        | <a href="<?= $this->Url->build('/about'); ?>" class="text-dark"><?= __('O projektu'); ?></a>
                    <?php } ?>
                    | <a href="<?= $this->Url->build('/gdpr'); ?>" class="text-dark"><?= __('GDPR'); ?></a>
                    | <a href="<?= $this->Url->build('/accessibility'); ?>" class="text-dark"><?= __('Prohlášení o přístupnosti'); ?></a>
                </div>
            </div>
        </div>
    </footer>

    <?= $this->Html->script('quill-textarea.js') ?>
    <?php
    $ga_id = OrganizationSetting::getSetting(OrganizationSetting::GA_TRACKING_ID);
    if (!empty(trim($ga_id))) :
    ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163606561-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', '<?= $ga_id ?>');
        </script>
    <?php endif; ?>
    <?php
    $custom_js = OrganizationSetting::getSetting(OrganizationSetting::CUSTOM_JS);
    if (!empty(trim($custom_js))) :
    ?>
        <script type="text/javascript">
            $(function() {
                <?= $custom_js ?>
            });
        </script>
    <?php endif; ?>
</body>

</html>