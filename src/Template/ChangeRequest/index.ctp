<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\RequestBudgetChange;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals array
 * @var $states array
 * @var $defaultStates array
 * @var $requests Request[]
 * @var $withNotifications bool
 */

echo $this->element('simple_datatable');
echo $this->element('simple_select2');
$this->assign('title', __('Přehled'));
$states = RequestBudgetChange::getLabels();
?>

<div class="card mb-3" id="change-requests-filter">
    <div class="card-header">
        <h2><?= __('Přehled všech žádostí o změnu') ?></h2>
        <?= $this->Form->create(null, ['type' => 'get']) ?>
        <div class="row">
            <div class="col-md">
                <?= $this->Form->control(
                    'state_ids',
                    ['options' => $states, 'class' => 'select2', 'multiple' => true, 'label' => __('Stav žádosti'), 'empty' => true]
                ) ?>
                <?= $this->element('table_status_filter') ?>
                <?= $this->Form->submit(__('Filtrovat')) ?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>

<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Datum a čas') ?></th>
            <th><?= __('Stav žádosti o změnu') ?></th>
            <th><?= __('ID žádosti o dotaci') ?></th>
            <th><?= __('Typ změny') ?></th>
            <th><?= __('Název žádosti o dotaci') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($change_requests as $chr) : ?>
            <tr>
                <td><?= $chr->id ?></td>
                <td><?= $chr->created->i18nFormat() ?></td>
                <td><?= $states[$chr->status] ?></td>
                <td><?= $chr->request_id ?></td>
                <td><?php
                    if ($chr->change_type == "BUDGET") echo __('Změna v rozpočtu');
                    if ($chr->change_type == "FORM") echo __('Změna ve formuláři');
                    ?>
                </td>
                <td><?= $chr->request->name ?></td>
                <td>
                    <?= $this->getUserIdentity($chr->request->user_id, $chr->request->user_identity_version, 'default') ?>
                </td>
                <td>
                    <?php
                    // TODO: Only if it's a budget change
                    if ($chr->status == RequestBudgetChange::CHANGE_ACCEPTED) $btn_class = 'btn-secondary';
                    if ($chr->status == RequestBudgetChange::CHANGE_DECLINED) $btn_class = 'btn-secondary';
                    if ($chr->status == RequestBudgetChange::CHANGE_PENDING) $btn_class = 'btn-success';
                    if ($chr->change_type == "BUDGET") {
                        echo $this->Html->link(__('Detail'), ['action' => 'requestBudgetChangeDetail', $chr->id], ['class' => 'btn ' . $btn_class]);
                    }
                    if ($chr->change_type == "FORM") {
                        echo $this->Html->link(__('Detail'), ['action' => 'requestFormChangeDetail', $chr->id], ['class' => 'btn ' . $btn_class]);
                    }

                    ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>