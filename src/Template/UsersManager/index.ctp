<?php

use App\Model\Entity\Form;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $users User[]
 * @var $organizations array [org_id => org_name]
 * @var $roles array [role_id => role_name]
 */
$this->assign('title', __('Uživatelé'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<div class="row">
    <div class="col-sm">
        <?= $this->Html->link(__('Pozvat nového uživatele'), ['_name' => 'admin_users_invite'], ['class' => 'btn btn-success mb-2']) ?>
    </div>
    <div class="col-sm">
        <strong><?= __('Filtr rolí') . ':' ?></strong>
        <?= $this->Form->radio(
            'role-filter',
            [
                ['value' => 1, 'checked' => true, 'text' => ' uživatel portálu', 'class' => 'ml-3 mt-2'],
                ['value' => 2, 'text' => ' pracovník úřadu', 'class' => 'ml-3 mt-2'],

            ]
        ) ?>
    </div>
    <div class="col-sm">
    </div>
</div>


<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Název') ?></th>
            <th><?= __('E-mail') ?></th>
            <th><?= __('Povolen') ?></th>
            <th><?= __('Organizace / Role') ?></th>
            <?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW, true) === true) : ?>
                <th><?= __('Staré účty') ?></th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user->id ?></td>
                <td><?= $user->name ?></td>
                <td><?= $this->Html->link($user->email, ['action' => 'edit', $user->id]) ?></td>
                <td><?= $user->is_enabled ? 'Povolen' : 'Zakázán' ?></td>
                <td>
                    <?php
                    foreach ($user->users_to_roles as $users_to_role) {
                        printf("%s%s<br/>", $roles[$users_to_role->user_role_id], $users_to_role->organization_id ? ' v ' . $organizations[$users_to_role->organization_id] : '');
                    }
                    ?>
                </td>
                <?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW, true) === true) : ?>
                    <td>
                        <?php
                        foreach ($user->stare_ucty as $stary_ucet) {
                            echo $stary_ucet->_getNameWithYear() . ', ';
                        }
                        ?>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
    var mainFilterRule = 'Správce|Super';
    $(document).ready(function() {
        // default
        var table = $('#dtable')
            .DataTable()
            .columns(4)
            .search('^(?!.*(' + mainFilterRule + '))', true, false)
            .draw();
        // on change
        $('input[type=radio][name=role-filter]').change(function() {
            var table = $('#dtable')
                .DataTable()
                .columns(4)
                .search(this.value == '1' ?
                    '^(?!.*(' + mainFilterRule + '))' :
                    '(' + mainFilterRule + ')', true, false)
                .draw();
        });
    });
</script>