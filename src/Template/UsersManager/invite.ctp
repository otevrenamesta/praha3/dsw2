<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 */
$this->assign('title', __('Pozvat nového uživatele'));
?>
    <h1><?= $this->fetch('title') ?></h1>
<div class="alert alert-info">
    <?= __('Na zadanou e-mailovou adresu přijde odkaz, na kterém si uživatel nastaví heslo pro přihlášení') ?>
</div>
<?php
echo $this->Form->create($user);
echo $this->Form->control('email');
echo $this->Form->submit(__('Odeslat pozvánku'));
echo $this->Form->end();