<?php
$this->assign('title', 'Prohlášení o přístupnosti');
?>
<div class="container">

  <h1>PROHLÁŠENÍ O PŘÍSTUPNOSTI</h1>

  <p>Otevřená města, z. s. se jako poskytovatel internetových stránek zavazuje ke zpřístupnění svých internetových
    stránek v souladu se zákonem č. 99/2019 Sb., o přístupnosti internetových stránek a mobilních aplikací subjektů
    veřejného sektoru.</p>

  <h2>Soulad s mezinárodními normami</h2>

  <p>Podle Rozhodnutí komise (EU) 2018/2048 ze dne 20. prosince 2018 o harmonizované normě pro webové stránky a mobilní
    aplikace jsou uvedené internetové stránky vytvářeny s ohledem na následující normy:</p>

  <p>Evropská harmonizovaná norma EN 301 549 V2.1.2 (2018-08) Požadavky na přístupnost výrobků a služeb v oblasti IKT
    Mezinárodní standard ISO 40500 Web content accessibility guidelines (WCAG) Dále jsou uvedené internetové stránky v
    souladu s pravidly pro přístupnost, jak je stanoví zákon č.99/2019 Sb., o přístupnosti internetových stránek a
    mobilních aplikací.</p>

  <h2>Stav souladu s požadavky na přístupnost</h2>

  <p>Uvedené webové stránky a mobilní aplikace jsou v souladu s požadavky na přístupnost stanovenými příslušnou
    směrnicí, zákonem a příslušnými mezinárodními normami, kromě níže uvedeného obsahu.</p>

  <p>Na některých stránkách/částech webových stránek jsou odkazy na další soubory a dokumenty, zejména formátu DOC,
    DOCX, XLS, XLSX a PDF, které jsou přístupné, ale jsou ve vlastním formátu, k jehož otevření jsou potřeba další
    programy. V některých webových prohlížečích je integrována aplikace, jež slouží k prohlížení výše uvedených formátů
    dokumentů. U některého mediálního obsahu, zejména u specifických prezentací jež jsou primárně vizuální, není možno
    detailně popsat v textové alternativě plně jejich obsah. Je však vždy uveden orientační popis, aby měl uživatel
    představu o vizuálně prezentovaných informacích.</p>

  <h2>Alternativní postupy pro získání informací</h2>

  <p>Pokud se na internetových stránkách nachází informace, jež není či nemůže být přístupná, poskytuje poskytovatel v
    souladu s § 4, odst. 7, zákona, náhradní způsob získání informací.</p>

  <p>Je možné kontaktovat správce obsahu. Správce obsahu se bude snažit příslušnou informaci uživateli předat v pro něj
    vhodné formě, bude-li to možné.</p>

  <h2>Vypracování prohlášení o přístupnosti</h2>

  <p>Toto prohlášení bylo vypracováno dne 14. října 2021.</p>

  <p>Toto prohlášení bylo vypracováno dle metodického pokynu MVČR k tomuto zákonu na základě vlastního posouzení.</p>

  <h2>Zpětná vazba a kontaktní údaje</h2>

  <p>Tyto internetové stránky spravuje spolek <a href="https://www.otevrenamesta.cz/">Otevřená města, z. s.</a> Zpětnou
    vazbu ohledně přístupnosti webových stránek a informací na nich uvedených, směřujte na e-mailovou adresu <a href="mailto:info@otevrenamesta.cz">info@otevrenamesta.cz</a>.</p>

</div>
