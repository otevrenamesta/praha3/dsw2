<?php
$this->assign('title', 'Prohlášení o zpracování osobních údajů');
?>
<div class="container">
    <h1>PROHLÁŠENÍ O ZPRACOVÁNÍ OSOBNÍCH ÚDAJŮ</h1>

    <p>Prohlášení o zpracování osobních údajů dle
        nařízení Evropského parlamentu a Rady (EU) 2016/679 o ochraně fyzických osob v souvislosti se zpracováním
        osobních
        údajů a poučení subjektů údajů (dále jen „GDPR“)
        a zákona č. 110/2019 Sb. o zpracování osobních údajů
    </p>
    <p>Spolek Otevřená města, z.s., Vás tímto v souladu s čl. 12 GDPR informuje o zpracování Vašich osobních údajů a o
        Vašich právech.</p>

    <h2>I. Správce a zpracovatel osobních údajů</h2>
    <p>Správcem zpracovávaných osobních údajů je příslušná obec, které je adresována žádost podaná prostřednictvím
        zpracovatele.</p>

    <p>Zpracovatelem je spolek Otevřená města z.s., se sídlem Malinovského náměstí 624/3 602 00 Brno, IČO: 05129061,
        zapsaný
        v obchodním rejstříku vedený Krajským soudem v Brně, spisová značka L 21748/KSBR.</p>

    <h2>II. Rozsah zpracování osobních údajů</h2>
    <p>Osobní údaje jsou zpracovány v rozsahu, v jakém je příslušný subjekt údajů správci poskytl, a to v souvislosti s
        uzavřením smluvního či jiného právního vztahu se správcem, nebo které správce či zpracovatel shromáždil jinak a
        zpracovává je v souladu s platnými právními předpisy či k plnění zákonných povinností správce.</p>

    <h2>III. Zdroje osobních údajů</h2>
    <p>Osobní údaje jsou získávány přímo od subjektů údajů (dotazník na webových stránkách, dotazník v písemné podobě,
        smlouva v písemné podobě, e-maily, telefon, chat, webové stránky, kontaktní formulář na webu, sociální sítě,
        vizitky
        aj.).
    </p>
    <h2>IV. Kategorie osobních údajů, které jsou předmětem zpracování</h2>
    <p>
        Zpracováváme o Vás tyto osobní údaje:
    </p>
    <ul>
        <li>Jméno, příjmení, titul, datum narození, IČO, DIČ, adresa trvalého pobytu, adresa sídla a další údaje
            umožňující
            kontakt se subjektem údajů,
        </li>
        <li>Kontaktní údaje – kontaktní adresa, číslo telefonu, číslo faxu, e-mailová adresa, ID datové schránky a jiné
            obdobné informace),
        </li>
        <li>Bankovní spojení (název banky, kód banky, číslo účtu),</li>
        <li>Další údaje nezbytné pro plnění smlouvy (Zejména jméno a příjmení statutárního zástupce právnické osoby,
            právní
            důvod zastoupení, státní příslušnost...),
        </li>
        <li>Případně údaje poskytnuté nad rámec příslušných zákonů zpracovávané v rámci uděleného souhlasu ze strany
            subjektu údajů, byl-li tento udělen.
        </li>
    </ul>

    <h2>V. Kategorie subjektů údajů</h2>
    <p>V rámci naší činnosti zpracováváme osobní údaje o žadatelích o dotace, kteří využívají systém DSW2.</p>

    <h2>VI. Kategorie příjemců osobních údajů</h2>
    <p>
        Vaše osobní údaje zpracováváme pro potřeby těchto příjemců:
    </p>
    <ul>
        <li>správce či jiný poskytovatel dotace (v rámci poskytovatele se k údajům dostanou osoby podílející se na
            rozhodnutí, a dále osoby vykonávající kontrolu),
            orgány státní správy v rámci výkonu jejich působnosti.
        </li>
    </ul>

    <h2>VII. Účel zpracování osobních údajů</h2>
    <p>Vaše osobní údaje zpracováváme za těmito účely:</p>
    <ul>
        <li>řízení o poskytnutí dotace v celém svém rozsahu, včetně navazujících úkonů (kontrola),</li>
        <li>statistické zpracování dat,</li>
        <li>jednání o smluvním vztahu, plnění smlouvy,</li>
        <li>ochrana práv správce, příjemce nebo jiných dotčených osob (např. vymáhání pohledávek správce),</li>
        <li>archivnictví vedené na základě zákona,</li>
        <li>plnění zákonných povinností ze strany správce,</li>
        <li>ochrana životně důležitých zájmů subjektu údajů,</li>
        <li>účely obsažené v rámci případného souhlasu subjektu údajů.</li>
    </ul>

    <h2>VIII. Způsob zpracování a ochrany osobních údajů</h2>
    <p>
        Zpracování osobních údajů provádí správce sám i prostřednictvím zpracovatele. Zpracování je prováděno v
        provozovnách
        zpracovatele a ve sídle správce jednotlivými pověřenými zaměstnanci. Ke zpracování dochází prostřednictvím
        výpočetní
        techniky, popř. i manuálním způsobem u osobních údajů v listinné podobě za dodržení všech bezpečnostních zásad
        pro
        správu a zpracování osobních údajů. Za tímto účelem přijali zpracovatel i správce technickoorganizační opatření
        k
        zajištění ochrany osobních údajů, zejména opatření, aby nemohlo dojít k neoprávněnému nebo nahodilému přístupu k
        osobním údajům, jejich změně, zničení či ztrátě, neoprávněným přenosům, k jejich neoprávněnému zpracování, jakož
        i k
        jinému zneužití osobních údajů. Veškeré subjekty, kterým mohou být osobní údaje zpřístupněny, respektují právo
        subjektů údajů na ochranu soukromí a jsou povinny postupovat dle platných právních předpisů týkajících se
        ochrany
        osobních údajů.
    </p>

    <h2>IX. Doba zpracování osobních údajů</h2>
    <p>
        Vaše osobní údaje zpracováváme a uchováváme po dobu nezbytně nutnou k dosažení účelu zpracování. Po delší dobu
        zpracováváme a uchováváme osobní údaje jedině tehdy, pokud jste nám k tomu udělili souhlas, nebo pokud taková
        povinnost plyne ze zákona.
    </p>

    <h2>X. Poučení o právní úpravě</h2>
    <p>Správce a zpracovatel zpracovává údaje buď se souhlasem subjektu údajů, nebo pokud zákon výslovně stanoví, že
        souhlas
        není potřeba. Podle čl. 6 odst. 1 GDPR může správce a zpracovatel i bez souhlasu subjektu údajů zpracovávat
        údaje
        zejména v těchto případech:</p>
    <ul>
        <li>zpracování je nezbytné pro splnění smlouvy, jejíž smluvní stranou je subjekt údajů, nebo pro provedení
            opatření
            přijatých před uzavřením smlouvy na žádost tohoto subjektu údajů,
        </li>
        <li>zpracování je nezbytné pro splnění právní povinnosti, která se na správce vztahuje,</li>
        <li>zpracování je nezbytné pro ochranu životně důležitých zájmů subjektu údajů nebo jiné fyzické osoby,</li>
        <li>zpracování je nezbytné pro splnění úkolu prováděného ve veřejném zájmu nebo při výkonu veřejné moci, kterým
            je
            pověřen správce,
        </li>
        <li>zpracování je nezbytné pro účely oprávněných zájmů příslušného správce či třetí strany, kromě případů, kdy
            před
            těmito zájmy mají přednost zájmy nebo základní práva a svobody subjektu údajů vyžadující ochranu osobních
            údajů.
        </li>
    </ul>

    <h2>XI. Práva subjektů údajů</h2>
    <p>V souladu se čl. 12 GDPR informuje správce na žádost subjektu údajů subjekt údajů o právu na přístup k osobním
        údajům
        a k následujícím informacím:</p>
    <ul>
        <li>účelu zpracování,</li>
        <li>kategorii dotčených osobních údajů,</li>
        <li>příjemci nebo kategorie příjemců, kterým osobní údaje byly nebo budou zpřístupněny,</li>
        <li>plánované době, po kterou budou osobní údaje uloženy,</li>
        <li>veškeré dostupné informace o zdroji osobních údajů,</li>
        <li>pokud nejsou získány od subjektu údajů, skutečnosti, zda dochází k automatizovanému rozhodování, včetně
            profilování.
        </li>
    </ul>
    <p>Správce má právo za poskytnutí informace požadovat přiměřenou úhradu nepřevyšující náklady nezbytné na poskytnutí
        informace.</p>

    <p>
        Každý subjekt údajů, který zjistí nebo se domnívá, že správce nebo zpracovatel provádí zpracování jeho osobních
        údajů, které je v rozporu s ochranou soukromého a osobního života subjektu údajů nebo v rozporu se zákonem,
        zejména
        jsou-li osobní údaje nepřesné s ohledem na účel jejich zpracování, může:
    </p>
    <ul>
        <li> Požádat správce o vysvětlení.</li>
        <li>Požadovat, aby správce odstranil takto vzniklý stav. Zejména se může jednat o blokování, provedení opravy,
            doplnění nebo vymazání osobních údajů.
        </li>
        <li>Je-li žádost subjektu údajů shledána oprávněnou, správce odstraní neprodleně závadný stav.</li>
        <li>
            Nevyhoví-li správce žádosti subjektu údajů, má subjekt údajů právo obrátit se přímo na dozorový úřad, tedy
            Úřad
            pro ochranu osobních údajů.
        </li>
        <li>Žádost o odstranění závadného stavu nevylučuje, aby se subjekt údajů obrátil se svým podnětem na dozorový
            úřad
            přímo.
        </li>
    </ul>

    <p>
        Toto prohlášení je veřejně přístupné na internetových stránkách zpracovatele a v místě sídla zpracovatele.
    </p>
</div>
