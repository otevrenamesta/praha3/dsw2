<style>
    body {
        font-family: 'acumin-pro-wide';
        line-height: 1.5;
    }

    .about-header {
        background: #fff;
        background-image: linear-gradient(rgba(212, 226, 255, .7) .1em, transparent .1em), linear-gradient(90deg, rgba(212, 226, 255, .7) .1em, transparent .1em);
        background-size: 8.315% 33.1%;
        padding-bottom: 3.3%;
        margin-bottom: 3.3%;
    }


    .dsw2-hero {
        width: 12vw;
    }

    a {
        color: #1C1696;
    }

    .dark-blue {
        color: #1C1696;
    }

    .bg-dark-blue {
        background-color: #1C1696;
        color: white;
    }

    .green {
        color: #00D48F;
    }

    .navbar-light .navbar-nav .nav-link {
        color: #00D48F !important;
        font-weight: bold;
        font-size: 0.9rem;
    }

    .navbar-light .navbar-nav .nav-link:hover {
        text-decoration: underline;
    }

    .title-column {
        position: relative
    }

    .main-title {
        position: absolute;
        bottom: 0;
        margin-bottom: -1vw;
        font-size: 1.5vw;
    }

    .table-responsive {
        overflow-y: hidden;
    }

    @media (max-width: 991.98px) {
        .main-title {
            font-size: 15px;
        }

    }

    .jumbotron {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }
</style>
<div class="container">
    <div class="about-header p-3">
        <div class="row">
            <div class="col">
                <img class="d-flex justify-content-center dsw2-hero" src="/images/dsw2-hero.svg">
            </div>
            <div class="col title-column d-flex justify-content-end">
                <h2 class="main-title float-right dark-blue text-5xl md:text-8xl leading-tight font-weight-bold p-3 w-2/3">
                    DOTAČNÍ SOFTWARE 2
                </h2>
            </div>
        </div>
    </div>
    <p class="green">DSW2 je open-source nástroj pro digitalizaci dotačních procesů určený především pro obce, městské části a kraje.</p>
    <p>Umožňuje pohodlnou správu celého dotačního procesu od samotné tvorby dotační výzvy a jejích programů, příjem žádostí, jejich zpracování, kontrolu, hodnocení a veškeré související akce až po kontrolu podaných vyúčtování poskytnuté dotace.</p>
    <p>Aplikace je velmi variabilní a může se přizpůsobit různě nastaveným procesům podle individuálních potřeb obce. Také míra využití jednotlivých funkcí aplikace je volitelná, tudíž zaleží na každé obce, které fáze dotačního procesu realizuje prostřednictvím aplikace, a které mimo ni. </p>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 w-50">
                <h3 class="green">Další výhody pro obec:</h3>
                <ul class="list-unstyled">
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Vše na jednom místě v digitální podobě.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Sjednocení formy přijatých žádostí.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Rychlejší vypisování dotačních výzev.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Tvorba vlastních formulářů pro žadatele.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Správa uživatelských účtů, a to jak zaměstnanců úřadů, tak i žadatelů.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost on-line asistence žadateli již ve fázi přípravy žádosti.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost příjmu žádostí přes datovou schránku.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Příjem a kontrola vyúčtování dotace.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Historie žádostí i dotačních programů.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost zveřejní seznamu všech poskytnutých dotací.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Automatická publikace otevřených dat.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Notifikace pro žadatele i úředníky.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost vrátit žádost žadateli k doplnění či opravě (včetně notifikace).
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 w-50">
                <h3 class="green">Výhody pro žadatele:</h3>
                <ul class="list-unstyled">
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost odeslat žádost datovou schránkou nebo e-mailem.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost doložení všech potřebných dokumentů pro poskytnutí i následné vyúčtování dotace prostřednictvím aplikace.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Trvalý přístup ke všem aktuálním i historickým, rozpracovaným i odeslaným žádostem.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Možnost úpravy osobních a kontaktních údajů žadatele.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Aktuální informace ke zveřejněným dotačním výzvám a programům na jednom místě.
                    </li>
                    <li class="mt-1"><span class="dark-blue h4"><i class="fas fa-hand-holding"></i></span> Notifikace o stavu zpracování žádosti (potvrzení o formální správnosti žádosti, potvrzení o schválení žádosti a potvrzení o odeslání schválených finančních prostředků).
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div id="about" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2 class="green">O projektu</h2>
        <p>
            Hlavním cílem DSW2 je nabídnout obcím, městským částem a krajům pohodlný nástroj pro správu celého dotačního procesu, a to počínaje vypsáním dotační výzvy a konkrétních dotačních programů přes příjem žádostí, jejich zpracování, kontrolu, hodnocení i ohlášení akcí, až po vyúčtování a zveřejnění udělené podpory v otevřených datech.
        </p>
        <p>
            Nastavení celého dotačního portálu je velmi variabilní. Aplikace se umí přizpůsobit zavedeným procesům v obci/kraji, není tady nutné, aby se uživatel přizpůsoboval aplikaci. Uživatel má možnost vytvořit si v aplikaci vlastní formuláře pro žadatele o dotaci, a to v takové podobě, na jakou je zvyklý. Může také spravovat jak uživatelské účty zaměstnanců dotačního úřadu, tak účty žadatelů.
        </p>
        <p>
            Projekt je poskytován ve formě open source software vč. dokumentace pro instalaci a nastavení, a to proto, že jedním z cílů projektu je umožnit obci/kraji nezávislost na IT poskytovatelích (eliminace rizika vendor lock-in).
        </p>
        <p>
            Jedinou otázkou, na kterou si budoucí uživatel (obec/kraj) musí odpovědět je, zda si open source software bude provozovat sám na své vlastní náklady nebo zda zvolí variantu dodání aplikace jako kompletní služby, která zahrnuje implementaci, správu, provozní a uživatelskou podporu a vývoj na míru, od neziskové organizace Otevřená města, z. s. .
        </p>
    </div>
</div>

<div id="get-started" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2 class="green">Jak si software vyzkoušet?</h2>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="fas fa-dot-circle dark-blue"></i>
            </div>
            <div class="col">
                Napište nám na e-mail <a href="mailto:dsw2@otevrenamesta.cz">dsw2@otevrenamesta.cz</a>
                požádejte o svůj vlastní portál ZDARMA.
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="fas fa-dot-circle dark-blue"></i>
            </div>
            <div class="col">
                Vyzkoušejte si administraci dotačního portálu, vytvoření formulářů, vyplnění a odeslání žádosti o dotaci.
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-md-1 text-center">
                <i class="fas fa-dot-circle dark-blue"></i>
            </div>
            <div class="col">
                Rozhodněte se dle <a href="#pricing">Ceníku</a> jak chcete software provozovat.
            </div>
        </div>
        Při testování vám samozřejmě poskytneme pomoc s nastavením a zodpovíme všechny otázky, abyste mohli udělat informované rozhodnutí.
    </div>
</div>

<div id="process" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2 class="green">Dotační proces</h2>
        <p>
            Abyste si mohli udělat představu, které všechny úkony s Dotačním Software lze provádět, zde je stručný popis
            dotačního procesu, jak jej implementujeme.
        </p>
        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="w-25">
                <col class="w-10">
                <col class="w-10">
                <col>
            </colgroup>
            <thead class="bg-dark-blue">
                <tr>
                    <th>Krok v dotačním procesu</th>
                    <th>Povinný?</th>
                    <th>Kompetence (Role / Tým)</th>
                    <th>Popis</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $processes = [
                    ['Přijetí žádosti', 'Povinný', 'Tým: Formální Kontrola', 'Přijetí může proběhnout úplně elektronicky nebo listinnou formou.'],
                    ['Formální kontrola', 'Povinný', 'Tým: Formální Kontrola', 'Každá elektronicky přijatá žádost musí být schválena formální kontrolou, případně může být žádost vrácena žadateli k opravě a v nejhorším případě vyřazena z dalšího zpracování. U listinných podání žádostí se předpokládá kontrola před elektronickou evidencí.'],
                    ['Hodnocení žádosti', 'Nepovinný', 'Tým: Hodnocení Žádostí', 'Pokud chcete, můžete v jednotlivých programech žádosti hodnotit dle stanovených bodových kritérií, výsledek hodnocení pak může být podkladem pro další jednání dotačních orgánů.'],
                    ['Návrh změny výše nebo účelu dotace', 'Nepovinný', 'Tým: Návrh výše dotace', 'V procesu zpracování lze provést návrhy na změnu výše podpory nebo účelu.'],
                    ['Uzavření a finalizace žádostí', 'Nepovinný', 'Role: Správce dotací', 'Před jednáním statutárních orgánů (rada, zastupitelstvo, ...) je možné ukončit hodnocení a návrhy změn výše nebo účelu (typicky provádí komise/výbory).'],
                    ['Nastavení výsledného stavu', 'Povinný', 'Tým: Schválení výše dotace', 'Po jednání statutárních orgánů je třeba provést evidenci výsledku jednání, zaslat vyrozumění o poskytnutí/neposkytnutí podpory a případně vyzvat žadatele k podpisu smlouvy.'],
                    ['Zvěřejnění smlouvy a vyplacení podpory', 'Povinný', 'Tým: Nastavení výsledného stavu žádosti / Role: Ekonom', 'Pro každou žádost je třeba s žadatelem uzavřít příslušnou smlouvu o veřejnoprávní podpoře a tuto zveřejnit v registru smluv. Portál umožňuje evidovat i situace kdy smlouva podepsána být nemůže, provést hlášení podpory de minimis a případně vyplatit i jen část schválené podpory (víceleté dotace).'],
                    ['Ohlášení činnosti / akce / díla', 'Nepovinný', 'Žadatel', 'Pokud vyžadujete od úspěšných žadatelů, aby vás informovali o průběhu čerpání (uspořádání akce, dokočení díla, naplánované činnosti), toto mohou žadatelé provést přímo v dotačním portálu.'],
                    ['Vyúčtování dotace', 'Povinný', 'Žadatel / Role: Ekonom', 'Vyúčtování dotace je nezbytné, portál umožňuje žadateli podat vyúčtování (jednotlivé položky) a tyto doložit příslušnými doklady (výpis z BÚ, faktura, smlouva, pokladní doklad). Ekonom dotační organizace pak může vyúčtování akceptovat, nebo jej vrátit k doplnění/opravě žadateli.'],
                    ['Veřejnoprávní kontrola', 'Nepovinný', 'Role: Ekonom', 'Skrze portál lze také evidovat situace kdy došlo k porušení veřejnoprávní smlouvy a musí být vrácena část nebo celá částka dotace a evidovat probíhající správní/trestní/soudní řízení.'],
                    ['Zveřejnění udělené podpory', 'Povinný', 'Role: Ekonom', 'Dle zákonných povinností, portál automaticky zveřejní po ukončení procesu žádosti, identifikaci úspěšného žadatele, částku podpory, účel a hodnocení a část vyúčtování (závěrečnou zprávu a seznam uznaných nákladových položek) ve formátu otevřených dat.']
                ];
                foreach ($processes as $process) :
                ?>
                    <tr>
                        <td class="font-weight-bold text-center"><?= $process[0] ?></td>
                        <td class="text-center <?= $process[1] === 'Povinný' ? 'text-light' : 'text-muted' ?>"><?= $process[1] ?></td>
                        <td class="text-center"><?= $process[2] ?></td>
                        <td><?= $process[3] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<div id="pricing" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container table-responsive">
        <h2 class="green">Ceník</h2>

        <p>Cena za používání / podporu při využívání projektu se odvíjí především od toho, zda je
            daná obec / městská část, členem spolku Otevřených Měst či nikoliv. Cena za využívání aplikace je navázána na velikost daňových příjmů obce nebo kraje. Cena je konstruována jako pásmová.
        </p>
        <p>Pokud vás zajímá členství, podmínky a
            případně další výhody, více informací naleznete zde:
            <a href="https://www.otevrenamesta.cz/collaboration/" target="_blank" class="font-weight-bold">https://www.otevrenamesta.cz/collaboration/</a>
        </p>

        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="w-30">
                <col class="w-20">
                <col class="w-50">
            </colgroup>
            <thead class="bg-dark-blue">
                <tr>
                    <th>Nákladová položka</th>
                    <th>Pro členy Otevřených Měst</th>
                    <th>Ostatní obce a organizace</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>Provoz v cloudu Otevřených Měst (SaaS)</strong>
                        <ul class="text-muted">
                            <li>Provoz a podporu zajišťují Otevřená Města na vlastní IT infrastruktuře.</li>
                            <li>Zahrnuje běžnou podporu v pracovní dny (8/5).</li>
                            <li>Zahrnuje školení pro správce dotačního fondu.</li>
                            <li>Zahrnuje asistenci s prvním nastavením dotačního portálu a technických parametrů.</li>
                            <li>Zdarma je samozřejmě oprava všech funkčních chyb i drobný vývoj.</li>
                        </ul>
                    </td>
                    <td class="font-weight-bold text-center h4">
                        Zdarma
                    </td>
                    <td>
                        <table class="table table-light">
                            <thead class="thead-light">
                                <tr>
                                    <th>Výše daňových příjmu obce (Kč/rok)</th>
                                    <th>Měsíční poplatek (Kč)</th>
                                    <th>Roční poplatek (Kč)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>do 200 mil.</td>
                                    <td>1.050 Kč</td>
                                    <td>12.600 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 400 mil.</td>
                                    <td>2.100 Kč</td>
                                    <td>25.200 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 600 mil.</td>
                                    <td>3.150 Kč</td>
                                    <td>37.800 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 800 mil.</td>
                                    <td>4.200 Kč</td>
                                    <td>50.400 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 1000 mil.</td>
                                    <td>5.250 Kč</td>
                                    <td>63.000 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 1200 mil.</td>
                                    <td>6.300 Kč</td>
                                    <td>75.600 Kč</td>
                                </tr>
                                <tr>
                                    <td>do 1400 mil.</td>
                                    <td>7.350 Kč</td>
                                    <td>88.200 Kč</td>
                                </tr>
                                <tr>
                                    <td>nad 1400 mil.</td>
                                    <td>8.400 Kč</td>
                                    <td>100.800 Kč</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Provoz na vašem serveru (On-Premise)</strong>
                        <ul class="text-muted">
                            <li>Dotační software bude provozován na vaší IT infrastruktuře.</li>
                            <li>Zahrnuje běžnou podporu v pracovní dny (8/5).</li>
                            <li>Zahrnuje školení pro správce dotačního fondu.</li>
                            <li>Zahrnuje asistenci s prvním nastavením dotačního portálu a technických parametrů.</li>
                            <li>Zdarma je samozřejmě oprava všech funkčních chyb i drobný vývoj.</li>
                        </ul>
                    </td>
                    <td colspan="2" class="text-center">
                        Kompletní Dotační Software si můžete zdarma stáhnout na
                        <a href="https://gitlab.com/otevrenamesta/praha3/dsw2" target="_blank" class="text-decoration-none">stránkách Gitlab</a>.
                        <br /><br />
                        Pokud si software nedokážete spustit sami, nebo nemáte potřebné kapacity, můžete se obrátit na
                        Otevřená Města a požádat nás o zajištění provozu a/nebo uživatelské podpory.
                        <br /><br />
                        Cenu bohužel nelze stanovit předem, kontaktujte nás prosím na e-mailu
                        <a class="text-decoration-none" href="mailto:dsw2@otevrenamesta.cz">dsw2@otevrenamesta.cz</a>
                        <hr class="bg-light" />
                        Nebo můžete požádat svého stávajícího poskytovatele/dodavatele webových stránek,
                        provoz projektu je velmi jednoduchý a nevyžaduje žádné speciální počítače, servery nebo licence.
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Vývoj na míru</strong>
                        <br />
                        <p class="text-muted">
                            Například vývoj formulářů na míru, nebo integrace s produkty třetí strany, například
                            spisovou službou, ekonomickým software, IdM a další.
                        </p>
                    </td>
                    <td colspan="2" class="font-weight-bold text-center">
                        Menší úpravy dle hodinové náročnosti s cenou 1500 Kč/hodina.
                        <br /><br />
                        Pro velké úpravy stanovíme, pokud to bude možné, fixní projektovou cenu.
                    </td>
                </tr>
            </tbody>
            <tr>
                <td colspan="3" class="text-center">
                    <i> Ceny platné od června 2022 </i>
                </td>
            </tr>
        </table>
    </div>
</div>

<div id="gallery" class="jumbotron bg-opaque">
    <div class="container">
        <h2 class="green">Fotogalerie</h2>
        <div class="row">
            <?php
            $images = [
                ['hlavni_stranka_springfield.png', 'Ukázkový dotační portál města Springfield'],
                ['nastaveni_organizace_zakladni.png', 'Nastavení dotačního portálu: Základní informace'],
                ['nastaveni_organizace_technicke.png', 'Nastavení dotačního portálu: Technické detaily'],
                ['nastaveni_organizace_emaily.png', 'Nastavení dotačního portálu: E-mailový server'],
                ['sprava_struktury_dotacniho_uradu.png', 'Správa struktury dotačního úřadu'],
                ['detail_spravy_uzivatele.png', 'Detail správy uživatele / oprávnění'],
                ['sprava_napovedy.png', 'Správa nápovědy (sekcí a stránek uživatelské podpory)'],
                ['sprava_prekladu.png', 'Správa překladů (úprava statických textů)'],
                ['sprava_formularu.png', 'Správa formulářů k vyplnění'],
                ['sprava_formularu_detail.png', 'Detail úpravy formuláře'],
                ['sprava_dotacnich_vyzev.png', 'Správa dotačních výzev'],
                ['sprava_dotacnich_vyzev_detail.png', 'Detail úpravy výzvy k podání žádostí o dotaci'],
                ['hodnotici_kriteria.png', 'Správa skupin hodotících kritérií'],
                ['sprava_historie_zadatelu.png', 'Správa historie žadatelů / poskytnuté podpory'],
                ['me_zadosti.png', 'Žadatel: Přehled žádostí'],
                ['detail_zadosti.png', 'Žadatel: Detail žádosti'],
                ['rozpocet_projektu.png', 'Žadatel: Rozpočet projektu (náklady, výnosy, vlastní zdroje, dotace z jiných zdrojů na stejný projekt)'],
                ['moje_identita_1.png', 'Žadatel: Moje identita'],
                ['moje_identita_2_po_podily.png', 'Žadatel: Evidence vlastnické struktury a vlastněných podílů v právnických osobách'],
                ['moje_identita_3.png', 'Žadatel: Moje identita - bankovní účet a údaje ověřené přihlášením do Datové schránky'],
                ['socialni_personalni_zajisteni_sluzby.png', 'Sociální služby: Personální zajištění služby (informace o zaměstnancích dotované organizace)'],
                ['socialni_kapacita_sluzby.png', 'Sociální služby: Poskytovaná kapacita služby (s výhledem do dalšího období)'],
                ['socialni_financni_rozvaha.png', 'Sociální služby: Finanční rozvaha k zajištění služby podle zdroje financí'],
                ['socialni_rozpocet_podle_nakladove_polozky.png', 'Sociální služby: Rozpočet podle nákladových položek'],
                ['rozpocet_projektu_vzP14.png', 'Rozpočet projektu vz. Praha 14 (oddělené nákladové sekce a automatický výpočet žádané částky)'],
                ['papirova_evidence_1.png', 'Elektronická evidence papírově přijaté žádosti'],
                ['reporty_1.png', 'Uživatelsky upravitelné datové sestavy (tabulky) z dat obsažených v žádosti a procesu zpracování'],
                ['reporty_2.png', 'Uživatelsky upravitelné datové sestavy (tabulky) z dat obsažených v žádosti a procesu zpracování']
            ];
            foreach ($images as $image) :
            ?>
                <div class="col-md-3 mt-2">
                    <a href="/about_assets/<?= $image[0] ?>" data-fancybox="previews" data-caption="<?= $image[1] ?>" class="w-100">
                        <img src="/about_assets/<?= $image[0] ?>" class="w-100 border-dark border" alt="<?= $image[1] ?>">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div id="partners" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2 class="green">Partneři projektu</h2>
        <p>
            Uvedené jsou městské části a obce, které již využívají nebo aktuálně testují využití Dotačního Software,
            a organizace, které se podílejí na vývoji nebo propagaci projektu.
        </p>
        <div class="row">
            <?php
            $partners = [
                [
                    'logo' => 'https://www.praha3.cz/_templates/project_frontend/images/logo.svg',
                    'name' => 'MČ Praha 3',
                    'link' => 'https://dotace.praha3.cz',
                    'linktext' => 'dotace.praha3.cz'
                ],
                [
                    'logo' => 'https://www.zvirevnouzi.cz/wp-content/uploads/sites/3/2019/08/Mestska-cast-Praha-4-znak.jpg',
                    'name' => 'MČ Praha 4',
                    'link' => 'https://www.praha4.cz/',
                    'linktext' => 'praha4.cz'
                ],
                [
                    'logo' => 'https://www.praha8.cz/image/Jns/logo-prvni-misto.jpg',
                    'name' => 'MČ Praha 8',
                    'link' => 'https://dotace.praha8.cz',
                    'linktext' => 'dotace.praha8.cz'
                ],
                [
                    'logo' => 'https://www.bezeckenadeje.cz/wp-content/uploads/2015/01/logo_barva_p12-1024x559.jpg',
                    'name' => 'MČ Praha 12',
                    'link' => 'https://dotace.praha12.cz',
                    'linktext' => 'dotace.praha12.cz'
                ],
                [
                    'logo' => 'https://www.top09.cz/files/photos/large/20120614182152.jpg',
                    'name' => 'MČ Praha 14',
                    'link' => 'https://praha14.dsw2.otevrenamesta.cz/',
                    'linktext' => 'dotace.praha14.cz'
                ],
                [
                    'logo' => '/img/usti-logo-zakladni verze-RGB-barva_medium.jpg',
                    'name' => 'Ústí nad Labem',
                    'link' => 'https://dotace.usti-nad-labem.cz/',
                    'linktext' => 'dotace.usti-nad-labem.cz'
                ],
                [
                    'logo' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSukHOIQjDfbZl0TIPDDT6tCI21uLElmhH2tA&usqp=CAU',
                    'name' => 'Operátor ICT',
                    'link' => 'https://operatorict.cz/',
                    'linktext' => 'operatorict.cz'
                ],
                [
                    'logo' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Prague_11_CoA_CZ.svg/1280px-Prague_11_CoA_CZ.svg.png',
                    'name' => 'Praha 11',
                    'link' => 'https://dotace.praha11.cz/',
                    'linktext' => 'dotace.praha11.cz'
                ]

            ];

            foreach ($partners as $partner) :
            ?>
                <div class="col-md-3 p-2 d-inline-block text-center">
                    <img src="<?= $partner['logo'] ?>" alt="<?= $partner['name'] ?>" class="w-100">
                    <a href="<?= $partner['link'] ?>" target="_blank" class="text-dark stretched-link"><?= $partner['linktext'] ?></a>
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>

<div id="links" class="jumbotron jumbotron-fluid bg-opaque">
    <div class="container">
        <h2 class="green">Další informace</h2>
        <div class="list-group">
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2" target="_blank" class="list-group-item list-group-item-action">Gitlab:
                Zdrojový kód</a>
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/tree/master/docs" target="_blank" class="list-group-item list-group-item-action">
                Gitlab: Dokumentace (instalace serveru, nastavení dotačního portálu,&nbsp;...)
            </a>
            <a href="https://otevrenamesta.cz/projects/2" target="_blank" class="list-group-item list-group-item-action">Otevřená
                Města: Projekt DSW</a>
        </div>

        <br />

        <h2 class="green">Napsali o nás</h2>
        <div class="list-group">
            <a href="https://praha.pirati.cz/roboti-misto-uredniku-praha-3-nasazuje-otevreny-software.html" target="_blank" class="list-group-item list-group-item-action">
                pirati.cz: Piráti začali první úředníky nahrazovat robotem. Dotační software Prahy 3 nyní volně k
                dispozici všem
            </a>
            <a href="https://www.lupa.cz/aktuality/praha-3-podle-slibu-zverejnila-prvni-vlastni-software-jako-open-source/" target="_blank" class="list-group-item list-group-item-action">
                lupa.cz: Praha 3 podle slibů zveřejnila první vlastní software jako open source
            </a>

        </div>
    </div>
</div>

<div id="contacts" class="jumbotron bg-opaque">
    <div class="container">
        <h2 class="green">Kontakty</h2>
        <div class="list-group">
            <a href="mailto:dsw2@otevrenamesta.cz" target="_blank" class="list-group-item list-group-item-action">
                Pokud máte zájem si projekt vyzkoušet, nebo máte dotaz právního/obchodního charakteru, použijte prosím
                email:
                <strong>dsw2@otevrenamesta.cz</strong>
            </a>
            <a href="https://www.otevrenamesta.cz/contact" target="_blank" class="list-group-item list-group-item-action">
                Kontakty na spolek Otevřená Města, <strong>https://www.otevrenamesta.cz/contact</strong>
            </a>
            <a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues" target="_blank" class="list-group-item list-group-item-action">
                Pokud máte technický problém, nebo se chcete podílet na vývoji, <strong>Gitlab Issues</strong>
            </a>
        </div>
    </div>
</div>