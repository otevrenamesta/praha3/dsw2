<?php

/**
 * @var $this AppView
 * @var $fond Fond
 * @var $realm Realm
 * @var $program Program
 * @var $appeal Appeal
 * @var $form Form
 * @var $request Request
 * @var $organization Organization
 */

use App\Model\Entity\Appeal;
use App\Model\Entity\Fond;
use App\Model\Entity\Form;
use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Realm;
use App\Model\Entity\Request;
use App\View\AppView;

$this->assign('title', 'Příručka správce dotačního portálu');
?>

<div class="card m-2">
    <h2 class="card-header">První kroky v novém dotačním portálu</h2>
    <div class="card-body">
        <p class="alert alert-info">
            Kroky zde uvedené je třeba provést v daném pořadí, jinak není možné některé kroky provést. <br />
            Například lze vytvořit formulář bez dotačního programu, ale ne program bez oblasti podpory nebo dotačního
            fondu. <br />
            Stejně tak nelze vytvořit žádost o podporu, bez vytvořeného programu, ale formuláře v programu vyžadované
            můžete upravit kdykoli
        </p>
        <ol>
            <li>
                <i class="fas <?= empty($this->getSiteSetting(OrganizationSetting::SITE_NAME)) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                Nastavit základní informace o organizaci v
                <?= $this->Html->link('Administrátor > Správa organizací', ['controller' => 'Organizations', 'action' => 'edit'], ['target' => '_blank']) ?>
                <br />Především pak
                <ul>
                    <li>v sekci "Základní nastavení organizace", kde lze vypnout registraci / přihlašování uživatel</li>
                    <li>v sekci "Nastavení dotačního procesu", která určuje základní předpoklady o průběhu procesu
                        podání a vyřízení žádostí
                    </li>
                    <li>v sekci "Technické údaje" a "Nastavení e-mailů" lze portál spojit s Datovou Schránkou, vlastním
                        e-mailovým serverem a přidat logo dotačního portálu
                    </li>
                </ul>
            </li>
            <li>
                <i class="fas <?= empty($fond) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit dotační fond',
                    ['controller' => 'GrantsManager'] + ($fond ? ['action' => 'indexFonds'] : ['action' => 'fondAddModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
            <li>
                <i class="fas <?= empty($realm) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit oblast(i) podpory',
                    ['controller' => 'GrantsManager'] + ($realm ? ['action' => 'indexRealms'] : ['action' => 'realmAddModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
            <li>
                <i class="fas <?= empty($program) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit alespoň 1 program, ve kterém poskytujete podporu',
                    ['controller' => 'GrantsManager'] + ($program ? ['action' => 'indexPrograms'] : ['action' => 'programAddModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
            <li>
                <i class="fas <?= empty($form) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit 1 formulář, který budete vyžadovat od žadatelů vyplnit, při podání žádosti',
                    ['controller' => 'Forms'] + ($form ? ['action' => 'index'] : ['action' => 'addModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
            <li>
                <i class="fas <?= empty($appeal) ? "fa-times text-danger" : "fa-check text-success" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit výzvu k podání žádostí o dotace, abyste si mohli proces otestovat',
                    ['controller' => 'GrantsManager'] + ($appeal ? ['action' => 'indexAppeals'] : ['action' => 'appealAddModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
            <li>
                <i class="fas <?= $this->isUser() ? (empty($request) ? "fa-times text-danger" : "fa-check text-success") : "fa-question text-warning" ?>"></i>
                <?= $this->Html->link(
                    'Vytvořit žádost ve vytvořeném programu, a otestovat jak probíhá proces vyplnění',
                    ['controller' => 'UserRequests'] + ($request ? ['action' => 'index'] : ['action' => 'addModify']),
                    ['target' => '_blank']
                ) ?>
            </li>
        </ol>
    </div>
</div>

<div class="card m-2" id="users">
    <h2 class="card-header">Práce s uživateli</h2>
    <div class="card-body">
        <div>
            Dotační portál všem registrovaným uživatelům, dovoluje vytvářet žádosti o dotace a spravovat vlastní
            uživatelský účet, tj.
            <ul>
                <li>upravovat svoji identitu</li>
                <li>spravovat historii přijaté veřejné podpory</li>
                <li>sdílet svůj účet s dalšími uživateli</li>
            </ul>
        </div>
        <div>
            Nad rámec těchto, lze uživatelům přiřadit mimořádná oprávnění, a to ve dvou kategoriích, viz. níže
            <ol>
                <li><a href="#user-roles">Role: Oprávnění ke správě části nebo celého dotačního portálu</a></li>
                <li><a href="#user-teams">Týmy: Tedy příslušnost k jedné nebo více skupinám / organizačním jednotkám
                        dotačního úřadu</a></li>
            </ol>
        </div>
        <div>
            Ověřené informace v identitě uživatele se získávají přihlášením uživatele do datové schránky. Ověřené údaje jsou:
            <ul>
                <li>název subjektu</li>
                <li>IČO subjektu</li>
                <li>ID datové schránky</li>
                <li>typ datové schránky</li>
            </ul>
        </div>
        <p>
            Pro správce portálu je pak důležité, že mohou vstoupit do jiného registrovaného uživatele, jako forma
            uživatelské podpory. Toto samozřejmě může provést jen oprávněná role.
        </p>
        <p>
            Žádná role nebo tým v systému, nemůže přímo upravovat podané žádosti, to může vždy jen uživatel. <br />
            Vyjímku tvoří Správce dotačního portálu a Správce uživatel, který může upravit žádost před jejím podáním, ne
            však už ve chvíli, kdy je žádost odeslána (elektronicky nebo papírově).
        </p>
        <p class="font-weight-bold">
            Po úpravě rolí nebo týmů daného uživatele, se musí daný uživatel odhlásit a znovu přihlásit, aby se změny
            projevily.
        </p>
        <div>
            Sdílení účtu:
            <ul>
                <li>funkce sdílení účtu je dostupná v menu uživatele pod názvem "<a href="/user/share_my_account" target="_blank">Sdílet tento účet</a>"</li>
                <li>sdílet svůj účet lze pouze s dalšími (již vytvořenými) uživateli systému DSW2</li>
                <li>uživatel sdíleného účtu má v sekci "<a href="/user/requests" target="_blank">Mé žádosti o podporu</a>" dostupný seznam "Žádosti ze sdílených účtů"</li>
                <li>u jednotlivé žádosti je možné přepnutí na sdílený účet</li>
                <li>po přepnutí na sdílený účet má uživatel dostupné funkce, jako by byl ke sdílenému účtu přímo přihlášený</li>
                <li>funkce přepnutí na sdílený účet/účty je také dostupná v menu uživatele pod názvem "Přepnout účet"</li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="user-roles">
    <h2 class="card-header">Uživatelské role</h2>
    <div class="card-body">
        <p>
            Systém obsahuje několik málo rolí, pro základní rozdělení kompetencí.
        </p>
        <p>
            Pouze Správce dotačního portálu nebo Správce uživatel může jiným uživatelům udělovat/odebírat role nebo
            upravovat jejich členství v týmech.
        </p>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Název role</th>
                    <th>Popis kompetencí</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-nowrap">Správce Dotačního Portálu</td>
                    <td>
                        <ul>
                            <li>Má kompletní kontrolu nad dotačním portálem</li>
                            <li>Může provádět vše co dělají ostatní role</li>
                            <li>Pokud není členem konkrétního týmu, nevidí týmové rozhraní a nemůže zasahovat do zpracování
                                podaných žádostí (oprávnění si ale může sám přidat)
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Správce Uživatel</td>
                    <td>
                        <ul>
                            <li>Může zvát do portálu další uživatele</li>
                            <li>Může uživatelům přidělovat role, kromě role správce dotačního portálu</li>
                            <li>Může se přihlásit za jiného uživatele (z detailu uživatele funkcí "Vstoupit do uživatele")
                            </li>
                            <li>Může spravovat jen uživatele, kteří se někdy přihlásili v aktuálním dotačním portálu
                                (případně uživatele, které do portálu pozval)
                            </li>
                            <li>Může spravovat týmy a uživatele do týmů zařazovat nebo je z nich odebírat</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Správce dotací</td>
                    <td>
                        <ul>
                            <li>Může spravovat Fondy, Oblasti podpory a Programy/Pod-programy</li>
                            <li>Může spravovat Hodnotící kritéria, a jejich zařazení</li>
                            <li>Může spravovat formuláře a jejich podobu/nastavení</li>
                            <li>Může spravovat výzvy k podání žádostí o dotaci</li>
                            <li>Může vytvořit dotační žádost ve výzvě, která je nastavena jako neaktivní (při testování)
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Správce historie žadatelů</td>
                    <td>
                        <ul>
                            <li>Spravuje jen sekci "Žadatelé (včetně historie)", tj. seznam identit historických žadatelů a jejich žádostí
                            </li>
                            <li>Sekce "Žadatelé (včetně historie)" je nezávislá na historii žádostí podaných v dotačním software,
                                slouží k importu dat o žadatelích, které organizace shromáždila v předchozích letech, kdy
                                dotační proces prováděla papírově nebo v jiném elektronickém systému
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Správce Wiki / Podpory</td>
                    <td>
                        <ul>
                            <li>Může jen spravovat kategorie a jednotlivé stránky uživatelské podpory, které jsou dostupné
                                pod tlačítkem "Nápověda" vpravo v horním navigačním menu
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Super Ekonom</td>
                    <td>
                        <ul>
                            <li>Vykonává veřejnoprávní a ekonomickou kontrolu nad celým dotačním portálem</li>
                            <li>V <a href="/about#process" target="_blank">Dotačním Procesu</a> od fáze "Zvěřejnění smlouvy
                                a vyplacení podpory" včetně, dohlíží nad plněním podmínek veřejnoprávní smlouvy
                            </li>
                            <li>
                                Eviduje že byla vyplacena podpora (a výše, tj. zda bylo vyplaceno 100% nebo méně, dle
                                příslušné smlouvy)
                            </li>
                            <li>
                                Kontroluje podaná vyúčtování udělené podpory a případně s žadateli elektronicky řeší nápravu
                            </li>
                            <li>V případě že je povoleno papírové podání žádosti, může pro účely evidence a zveřejnění
                                otevřených dat, manuálně evidovat fyzicky přijatá vyúčtování a závěrečnou zprávu
                            </li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card m-2" id="user-teams">
    <h2 class="card-header">Týmy / Struktura dotačního úřadu</h2>
    <div class="card-body">
        <p>
            Týmy, také "organizační jednotky dotačního úřadu", jsou způsobem, jak rozdělit a organizovat práci
            zaměstnanců dotačního úřadu, při zpracování podaných žádostí
        </p>
        <p>
            Narozdíl od rolí, které jsou statické v rámci Dotačního Software, týmy jsou zcela v kompetenci dotačního
            úřadu. <br />
            Typicky slouží k určení jednotek, jako jsou komise a výbory, příslušnost k odborům které poskytují dotace
            nebo nad dotačním procesem dohlíží.
        </p>
        <p>
            Členství v týmu uživateli samo o sobě nedává žádné pravomoci, je nutné, aby týmu byly přiřazeny kompetence v
            konkrétním dotačním programu nebo pod-programu. Vjímkou je náhled na historii žadatelů (výsledek správy v
            sekci "Žadatelé (včetně historie)"), každý člen týmu, může vidět historii žadatelů a žádostí, které úřad zpracoval v
            předchozích letech.
        </p>
        <p>
            Tým může být zodpovědný za jednu nebo více činností, které jsou spojené s procesem zpracování podaných
            žádostí, činnosti (kompetence) musí být rozděleny v každém dotačním programu a jsou rozdělené následovně
        </p>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Kompetence týmu</th>
                    <th>Povolené úkony</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-nowrap">Formální kontrola</td>
                    <td>
                        <ul>
                            <li>Manuální označení žádosti úspěšně vyplněné, jako "odeslané" (pokud v nastavení organizace je
                                povoleno)
                            </li>
                            <li>Označení žádosti jako formálně správné, nebo zamínuté</li>
                            <li>Pokud je žádost zamítnuta, žadateli udělí termín do kterého musí náležitosti opravit a
                                datum, pokud žadatel do daného data žádost neupraví a znovu neodešlě (neprovede papírové
                                podání na úřadu), žádost je automaticky nastavena do stavu "zamítnuto formální kontrolou"
                            </li>
                            <li>
                                Formální kontrola nemůže žádosti upravovat, jen je prohlížet a případně posouvat do dalších
                                stavů v rámci <a href="/about#process" target="_blank">Dotačního procesu</a>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Návrh výše dotace</td>
                    <td>
                        <ul>
                            <li>U žádosti může upravit návrh na výši poskytnuté podpory a popis účelu, na který je dotace
                                poskytnuta
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Hodnocení dotace</td>
                    <td>
                        <ul>
                            <li>
                                Pokud má daný dotační program určena hodnotící kritéria, může člen týmu provést své osobní
                                hodnocení, které se pak stane součástí průměru hodnocení
                            </li>
                            <li>
                                Člen týmu nevidí hodnocení ostatních hodnotitelů nebo výsledný průměr, dokud není žádost vč.
                                hodnocení a popisu účelu schválena
                            </li>
                            <li>
                                Součástí hodnocení je i textová poznámka, kterou si hodnotitel může vyplnit pro svoji
                                vlastní potřebu, a která není součástí dalšího dotačního procesu. To se může hodit, pokud
                                hodnocení uděluje dlouho před zasedáním kompetentního výboru/komise
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Schválení výše dotace</td>
                    <td>
                        <ul>
                            <li>U žádosti může upravit finální návrh na výši poskytnuté podpory a popis účelu a dosažené
                                hodnocení, na který je dotace poskytnuta
                            </li>
                            <li>
                                Pokud jsou v daném programu určena hodnotící kritéria, vidí výsledný průměr hodnocení
                                žádosti, ne však jednotlivá hodnocení nebo kdo hodnocení provedl, pouze počet započtených
                                hodnocení a průměr dosažený v jednotlivých kritériích
                            </li>
                            <li>
                                Schvalovatel, typicky předseda nebo tajemník komise/výboru, pak může určit hodnocení odlišné
                                od toho, které je výsledkem zpracování jednotlivých hodnotitelů, jelikož závazné pro další
                                proces je typicky usnesení takového výboru/komise, ne jednotlivá hodnocení, předcházející
                                jednání
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Nastavení výsledného stavu žádosti</td>
                    <td>
                        <ul>
                            <li>Poté co je žádost schválena a je pro ni schválena navržená podpora, slovní formulace účelu
                                (pokud byla určena) a bodové hodnocení, je žádost typicky schválena statutárními orgány
                                (rada, zastupitelstvo, ...), výsledek jednání odpovědného (statutárního) orgánu pak člen
                                tohoto týmu zanese zpět do dotačního software
                            </li>
                            <li>
                                Může dotaci nastavit do stavu "Připraveno k podpisu smlouvy", případně žádost uzavřít, pokud
                                podpora nebyla schválena
                            </li>
                            <li>
                                Může dotaci nastavit do stavu "Smlouva podepsána, připraveno k vyplacení", případně žádost
                                uzavřít, pokud žadatel odmítl nebo nebyl schopen smlouvu o veřejné podpoře podepsat
                                (například pro nesplnění jiných formálních podmínek dotačního procesu)
                            </li>
                            <li>
                                Může ohlásit udělení podpory do Registru de minimis, tj. registru veřejné podpory malého
                                rozsahu, který provozuje Ministerstvo Zemědělství. Tento krok pak může udělat i uživatel s
                                rolí "Ekonom", čímž daná žádost přechází do kompetence Ekonomů, kteří dotační proces vedou
                                dále až k vypořádání, ukončení dané žádosti a zveřejnění v otevřených datech
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap">Náhled na žádosti</td>
                    <td>
                        <ul>
                            <li>V určených programech vidí všechny žádosti bez ohledu na jejich aktuální stav, včetně
                                informací o dosavadním průběhu zpracování žádosti, navržené příp. schválené finanční
                                podpoře, bodového a slovního hodnocení, upravéno účelu poskytnutí finančních prostředků a
                                vyúčtování, pokud již toto bylo schváleno ekonomickým oddělením
                            </li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="font-weight-bold">
            Stejně jako u Rolí, pokud se změní zařazení uživatele, musí se uživatel odhlásit a znovu přihlásit, aby se
            změny projevily
        </p>
    </div>
</div>

<div class="card m-2" id="grants-history">
    <h2 class="card-header">Historie poskytnuté podpory</h2>
    <div class="card-body">
        <div>Uživatel:
            <ul>
                <li>uživatel vyplňuje historii přijaté veřejné podpory ve svém profilu</li>
                <li>historie je automaticky doplněna systémem o čerpanou podporu z tohoto systému DSW2</li>
                <li>pokud je ve "Správa organizace" zapnuta volba "Zobrazení historie podpory žadatelů z archivních záznamů" je historie doplněna i o archivní údaje doplňované úředem v sekci "Žadatelé (včetně historie)"</li>
                <li>tato historie podpory uživatele se pak zobrazuje jako součást přehledu žádosti v sekci "Historie přijaté veřejné podpory" a to za požadované období uvedené v nastavení "Správa organizace"</li>
            </ul>
        </div>
        <div>Úředník:
            <ul>
                <li>kompletní přehled přijaté veřejné podpory žadatele je dostupný v sekci "Žadatelé (včetně historie)"</li>
                <li>archivní záznamy lze doplnit funkcí "Přidat nového žadatele" nebo "Nahrát žadatele hromadně" v této sekci</li>
                <li>úředník zde má úplný přehled o historii čerpané podpory žadatele včetně podrobností, možnosti správy, nebo otevření žádostí</li>
                <li>sloupec "Vložil" v sekci "Žadatelé (včetně historie)" rozlišuje, kdo záznam do historie přidal:
                    <ul>
                        <li>uživatel - historii přijaté veřejné podpory vyplňuje ve svém profilu</li>
                        <li>systém - automatické doplnění systémem o čerpanou podporu z tohoto systému DSW2</li>
                        <li>úředník - archivní záznamy doplněné úřadem přes sekci "Žadatelé (včetně historie)"</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="admin_section">
    <h2 class="card-header">Sekce pro úředníky</h2>
    <div class="card-body">
        <div>Sekce pro úředníky:
            <ul>
                <li>sekce pro úředníky slouží k nastavení úředních informací</li>
                <li>změny může provádět uživatel s oprávněním pro "formální kontrolu"</li>
                <li>úpravy se provádějí v samostatné sekci, která je dostupná po stiskutí tlačítka "Sekce pro úředníky"</li>
                <li>tlačítko je dostupné v žádostech v sekcích "Mé týmy" a v "Ekonomickém oddělení"</li>
                <li>jednotlivé části jsou: vlastní identifikátor, podpora de minimis, veřejnosprávní kontrola a poznámka k žádosti</li>
            </ul>
        </div>
        <div>Úřední informace:
            <ul>
                <li>vyplněné údaje v sekci pro úředníky se zobrazují v žádosti v části "Úřední informace"</li>
                <li>obsahují:
                    <ul>
                        <li>vlastní identifikátor žádosti (např. pořadové číslo, spisová značka, ...)</li>
                        <li>podpora de minimis - to je podpora, která není de iure veřejnou podporou</li>
                        <li>informace o veřejnosprávní kontrole</li>
                        <li>libovolná poznámka k žádosti, včetně přílohy</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="users">
    <h2 class="card-header">Změny ve "Struktuře dotačních fondů"</h2>
    <div class="card-body">
        <div>Úprava a mazání formulářů:
            <ul>
                <li>formulář není možné upravit, pokud je používán v odeslaných žádostech</li>
                <li>není možné smazat formulář, který je používán v programech</li>
                <li>nelze smazat formulář, který byl již vyplněn a nelze smazat položku formuláře, která již byla vyplněna</li>
                <li>v detailu formuláře získáte informaci v jakých žádostech je vyplněn a v jakých se používá programech</li>
            </ul>
        </div>
        <div>Mazání programů:
            <ul>
                <li>nelze smazat program, který je součástí existující výzvy</li>
                <li>nelze smazat program, ve kterém jsou již rozpracované žádosti o dotaci</li>
            </ul>
        </div>
        <div>Podprogramy:
            <ul>
                <li>pokud je vybraný program "podprogram" a nelze k němu již přidávat další programy</li>
            </ul>
        </div>
        <div>Smazání oblastí podpory:
            <ul>
                <li>nelze smazat oblast podpory, pokud obsahuje podřízené programy/podprogramy</li>
                <li>nelze smazat oblast podpory s programy aktivními ve výzvách</li>
                <li>nelze smazat oblast podpory s programy, které obsahují rozpracované žádosti</li>
            </ul>
        </div>
        <div>Smazání výzvy:
            <ul>
                <li>nelze smazat výzvu pokud obsahuje žádosti o podporu</li>
            </ul>
        </div>
        <div>Smazání fondu:
            <ul>
                <li>před smazáním fondu se přesvědčte, že na něj nejsou navázány oblasti podpory</li>
            </ul>
        </div>
        <div>Vytvoření kopie fondu:
            <ul>
                <li>v rámci kopie fondu se vytváří také kopie oblastí podpory, programů a podprogramů</li>
                <li>ke kopiím programů a podprogramů nejsou připojeny formuláře</li>
            </ul>
        </div>
        <div>Hodnotící kritéria a hodnocení:
            <ul>
                <li>nelze smazat hodnotící kritéria, kterými již byly hodnoceny žádosti</li>
                <li>pokud již proběhla uzávěrka hodnocení, není možné vkládat nová hodnocení nebo upravovat existující</li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="vyuctovani-dotace">
    <h2 class="card-header"> Ekonomické oddělení / Vyúčtování - Vyúčtování dotace</h2>
    <div class="card-body">
        <div>
            <strong>Položky vyúčtování</strong>
            <ul>
                <li>zde se dokladují vynaložené finance nárokované pro potřeby dotace</li>
                <li>skládá se ze tří souvisejících částí:</li>
                <li>1. název položky, částka z dokladu, čerpaná částka a datum</li>
                <li>2. soubor, ve kterém je účetní nebo daňový doklad, faktura, účtenka, paragon, apod.</li>
                <li>3. soubor, ve kterém je výpis z bankovního účtu nebo výdajový pokladní doklad</li>
                <li>tyto soubory lze případně pouze vybrat, pokud byli již dříve vloženy v sekci Přílohy vyúčtování</li>
            </ul>
        </div>

        <div>
            <strong>Přílohy vyúčtování</strong>
            <ul>
                <li>seznam všech příloh v tomto vyúčtování</li>
                <li>je zde i možnost: Nahrát novou přílohu</li>
                <li>a tu pak použít v sekci Položky vyúčtování</li>
            </ul>
        </div>

        <div>
            <strong>Co znamená: Je příloha použita?</strong>
            <ul>
                <li>označení pro konkrétní soubor v Přílohy vyúčtování zda je/není použit v sekci Položky vyúčtování</li>
                <li>a/nebo slouží jako soubor doladující "závěrečnou zprávu" nebo "doklad o publicitě projektu"</li>
            </ul>
        </div>

        <div>
            <strong>Co znamená: Vyúčtování obsahuje nepoužité přílohy</strong>
            <ul>
                <li>informuje, zda jsou všechny vložené přílohy použity k doložení nároků na dotaci</li>
                <li>seznam příloh najdete v sekci Přílohy vyúčtování</li>
                <li>pokud je ve sloupci "Je příloha použita?" červený křížek, jedná se o soubor, který není použit v sekci Položky vyúčtování</li>
                <li>některé úřady ale vyžadují dodatečné přílohy, takže pak je toto označení pouze informativní</li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="vicelete-vyuctovani">
    <h2 class="card-header">Ekonomické oddělení / Přehled - Rozdělení vyúčtování na 1 a 2-leté dotace</h2>
    <div class="card-body">
        <div>
            <strong>Rozdělení schválené podpory</strong>
            <ul>
                <li>schválená podpora lze rozdělit na více částí, každá část má své vyúčtování</li>
                <li>přejděte do sekce: Ekonomické oddělení / Přehled</li>
                <li>vyhledejte žádost u které bude evidována nová platba</li>
                <li>zvolte u ní akci "Evidence plateb"</li>
                <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
            </ul>
        </div>
        <div>
            <strong>Evidovat platbu / Změnit stav žádosti</strong>
            <ul>
                <li>v této sekci nastavte "částku transakce"</li>
                <li>pokud je částka shodná se "schválenou podporou celkem", je podpora vyčerpána jednorázově</li>
                <li>pokud je částka nižší než "schválenáu podpora celkem", umožní to rozdělit podporu na více částí</li>
                <li>každá takto evidovaná výplata podpory (částka transakce) má své samostatné vyúčtování, které přijde žadateli k vyplnění</li>
                <li>tímto způsobem lze "schválenou podporou celkem" rozdělit bez omezení na více částí, až do vyčerpání celkové přiznané podpory</li>
                <li>rozdělení je časově neomezené a lze dopnit kdykoliv později a umožnit tak vyúčtování např. na 1 a 2-leté dotace</li>
            </ul>
        </div>
        <div>
            <strong>Přehledy vyplacených částek a částek k čerpání</strong>
            <ul>
                <li>pod formulářem "Evidovat platbu / Změnit stav žádosti" je v tabulce přehled všech uskutečněných platem "Přehled plateb této žádosti"</li>
                <li>ve vyúčtování platby, na stránce "Vyúčtování dotace", je pak uveden přehled jaká byla "Schválená podpora celkem"</li>
                <li>a částky "Vyplacená podpora" a/nebo "Nečerpaná podpora"</li>
                <li>částka transakce pro vyúčtování ve "Vyúčtování dotace" se nazývá "Částka k čerpání v tomto vyúčtování"</li>
            </ul>
        </div>
        <div>
            <strong>Zrušení evidované platby a vyúčtování</strong>
            <ul>
                <li>každá platba je evidovaná samostatně a v případě chyby lze použít funkci "Vymazat tuto platbu"</li>
                <li>po smazání platby se uvolní se částka pro nastavení správné "Výplaty podpory"</li>
                <li>nelze vymazat platbu pokud k ní již existuje "Vyúčtování"</li>
                <li>"Vyúčtování" může smazat pouze žadatel, který ho vyplňuje</li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="dofinancovaní">
    <h2 class="card-header">Ekonomické oddělení / Přehled - Dofinancování žádosti</h2>
    <div class="card-body">
        <div>
            <strong>Dofinancování (navýšení původně schválené podpory)</strong>
            <ul>
                <li>dofinancování slouží pro mimořádné navýšení původně schválené podpory</li>
                <li>zapnutí modulu "Dofinancování" se provede v sekci Administrator / Správa organizace v "Nastavení ekonomického oddělení"</li>
                <li>zde zaškrtněte volbu "Povolit možnost dofinancování - navýšení původně schválené podpory"</li>
                <li>pak přejděte do sekce: Ekonomické oddělení / Přehled</li>
                <li>vyhledejte žádost, která bude dofinancována</li>
                <li>zvolte u ní akci "Evidence plateb"</li>
                <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
            </ul>
        </div>
        <div>
            <strong>Evidovat platbu / Změnit stav žádosti</strong>
            <ul>
                <li>v této sekci nastavte jako "Částku transakce" konkrétní částku pro dofinancování, o kterou se "chválená podpora" navyšuje</li>
                <li>níže zaškrtněte volbu "Je tato platba dofinancování (navýšení původně schválené podpory)?"</li>
                <li>dle potřeby přidejte "Poznámku k dofinancování"</li>
                <li>dle potřeby přidejte "Přílohu k dofinancování"</li>
                <li>pokud je to potřeba, nastavte "Nový stav žádosti"</li>
                <li>konečné nastavení potvrdíte tlačítkem "Evidovat platbu a/nebo nastavit stav žádosti"</li>
                <li>nastavené informace se zobrazí níže v tabulce "Přehled plateb této žádosti"</li>
            </ul>
        </div>
        <div>
            <strong>Dofinancování v přehledech a vyúčtování</strong>
            <ul>
                <li>po nastavení "dofinancování" se v detailu evidence plateb a v žádostech nově uvádí tyto částky</li>
                <li>1. Schválená podpora celkem</li>
                <li>2. Původní podpora celkem</li>
                <li>3. Dofinancování (navýšení původně schválené podpory)</li>
                <li>tyto částky jsou zobrazeny i v PDF verzi vyúčtování pro tisk a v modulu Anonymizace</li>
                <li>v seznamech se uvádí informace o "Dofinancování" ve sloupci "Podpora celkem", respektive "Typ vyúčtování" u vyúčtování</li>
                <li>vypnutí modulu "Dofinancování", v sekci Administrator / Správa organizace, nemá vliv na již "dofinancované" žádosti</li>
            </ul>
        </div>
        <div>
            <strong>Vymazání platby pro "Dofinancování"</strong>
            <ul>
                <li>přejděte do sekce: Ekonomické oddělení / Přehled</li>
                <li>vyhledejte žádost, která byla dofinancována</li>
                <li>zvolte u ní akci "Evidence plateb"</li>
                <li>otevře se stránka žádosti "Evidovat platbu / Změnit stav žádosti"</li>
                <li>zde v tabulce "Přehled plateb této žádosti" zvolte platbu s typem "Dofinancování" a u ní akci "Vymazat tuto platbu"</li>
                <li>"Dofinancování" nelze vymazat, pokud k němu již existuje "Vyúčtování"</li>
            </ul>
        </div>
    </div>
</div>

<div class="card m-2" id="notifikace">
    <h2 class="card-header">Notifikace a historie změn žádosti/vyúčtování/ohlášení:</h2>
    <div class="card-body">
        <h4>Historie/notifikace u žádosti:</h4>
        <div>
            <strong>Zaznamenává se</strong>
            <ul>
                <li>datum a čas změny</li>
                <li>aktální stav žádosti</li>
                <li>kdo provedl změnu (email)</li>
                <li>komentář, nebo jiná informace ke změně</li>
            </ul>
        </div>
        <div>
            <strong>Změny, které se zaznamenávají</strong>
            <ul>
                <li>záznam nastane vždy při změně stavu žádosti</li>
                <li>nebo pokud není změna stavu žádosti, ale je posun mezi týmy</li>
                <li>záznam emailu je pouze v případě, že byl odeslán navíc</li>
                <li>nebo s komentářem úředníka ke stavu žádosti</li>
                <li>pokud v sekci "Navrhovatelé" dojde ke změně v samotném textu</li>
                <li>(<a href="https://gitlab.com/otevrenamesta/praha3/dsw2/-/blob/master/docs/zivotni-cyklus-diagram/DSW2_Flow_1.png" target="_blank">schéma životního cyklu žádosti</a>)</li>
            </ul>
        </div>
        <div>
            <strong>Notifikace na email žadatele</strong>
            <ul>
                <li>při každé změně stavu žádosti</li>
                <li>při přímém odesální emailu úředníkem z DSW2</li>
                <li>pouze při posunu žádosti mezi týmy "Hodnotitel" a "Navrhovatelé" se notifikace neodesílají</li>
                <li>k odeslání notifikace pak dojde až v týmu "Schvalovatelé" pokud je žádost schválena odbornou kontrolou</li>
            </ul>
        </div>
        <div>
            <strong>Texty odesílaných emailů</strong>
            <ul>
                <li>základní text emailu: Vaše žádost {NÁZEV} č. {ČÍSLO} (výzva: {VÝZVA}) je nyní ve stavu: {STAV}</li>
                <li>text při vrácení: Vaše žádost {NÁZEV} neprošla formální kontrolou. Na opravu žádosti máte čas do {DATUM}. Pokud žádost neopravíte a znovu neodešlete, bude pravděpodobně vyřazena z dalšího zpracování. Odůvodnění {TEXT}</li>
                <li>text při vyřazení: Vaše žádost {NÁZEV} byla vyřazena pro nesplnění podmínek. Odůvodnění: {TEXT}</li>
            </ul>
        </div>
        <div>
            <strong>Notifikace na e-mail úřadu</strong>
            <ul>
                <li>pokud jsou povoleny volby ve "Správě organizace", zasílá se notifikace:</li>
                <li>když přijde nová žádost</li>
                <li>a/nebo když přijde opravená žádost</li>
                <li>text zprávy: informujeme Vás o nové/opravené žádosti {NÁZEV} č. {ČÍSLO} na dotačním portále</li>
            </ul>
        </div>
        <br />

        <h4>Historie/notifikace u vyúčtování:</h4>
        <div>
            <strong>Zaznamenává se</strong>
            <ul>
                <li>datum a čas změny</li>
                <li>aktální stav vyúčtování</li>
                <li>kdo provedl změnu (email)</li>
                <li>komentář</li>
            </ul>
        </div>
        <div>
            <strong>Stavy ve vyúčtování, které se zaznamenávají</strong>
            <ul>
                <li>rozpracované</li>
                <li>kompletně vyplněno</li>
                <li>odesláno ke kontrole</li>
                <li>schváleno</li>
                <li>vráceno k opravě</li>
                <li>nevyhovuje podmínkám</li>
            </ul>
        </div>
        <div>
            <strong>Notifikace na email žadatele</strong>
            <ul>
                <li>při každé změně stavu vyúčtování</li>
            </ul>
        </div>
        <div>
            <strong>Texty odesílaných emailů</strong>
            <ul>
                <li>základní text emailu: Vaše vyúčtování č. {ID} žádosti {NÁZEV} je nyní ve stavu {STAV}</li>
                <li>text při vrácení: Vaše vyúčtování č. {ID} žádosti {NÁZEV} je nyní ve stavu{STAV}. Vaše vyúčtování bylo vráceno k opravě s následujícím odůvodněním {TEXT}</li>
            </ul>
        </div>
        <div>
            <strong>Notifikace na e-mail úřadu</strong>
            <ul>
                <li>pokud jsou povoleny volby ve "Správě organizace", zasílá se notifikace:</li>
                <li>když přijde nové vyúčtování</li>
                <li>a/nebo když přijde opravené vyúčtování</li>
                <li>text zprávy: informujeme Vás o novém/opraveném vyúčtování k žádosti {NÁZEV} č. {ČÍSLO} na dotačním portále</li>
            </ul>
        </div>
        <br />

        <h4>Notifikace u výzev:</h4>
        <div>
            <strong>Notifikace v sekci "Výzvy k podání dotačních žádostí"</strong>
            <ul>
                <li>v přehledu výzev lze ve sloupi "Akce" použít následující hromadné notifikace:</li>
                <li>"Odeslat informaci o nové výzvě"<br />popis: Hromadné odeslání informace o nové výzvě na e-maily všech uživatelů dotačního portálu</li>
                <li>"Odeslat informaci o končící lhůtě"<br />popis: Hromadné odeslání informace o končící lhůtě pro podání žádostí v této výzvě na e-maily všech uživatelů dotačního portálu</li>
            </ul>
        </div>
        <br />

        <h4>Historie/notifikace u ohlášení:</h4>
        <div>
            <strong>Zaznamenává se</strong>
            <ul>
                <li>každé přijaté ohlášení</li>
            </ul>
            <p>
                V případě, že organizace umožňuje žadatelům ohlašovat díla, činnosti a v nastavení organizace též zaškrtne <em>Povolit zasílání notifikací o nových ohlášeních hodnotitelům emailem</em>, zasílají se notifikacve o nově přijatých ohlášeních emailem všem hodnotitlům v daném programu. Jednotliví hodnotitelé mohou zasílání emailů na vlastní emailovou adresu potlačit v sekci Moje Identita nastavení <em>Nezaslat notifikace o provedených ohlášeních</em>
            </p>
            <p>
                Historii všech ohlášení mohou členové všech týmů též zobrazit v sekci Historie / Historie ohlášení, odkud se lze také podívat na seznam ohlášení pro konkrétní žádost a zobrazit přiložené soubory.
            </p>
        </div>
    </div>
</div>

<div class="card m-2" id="agendio">
    <h2 class="card-header">Agendio sestavy, smlouvy:</h2>
    <div class="card-body">
        <div>
            <strong>Agendio sestavy</strong>
            <ul>
                <li>dostupné v sekci Struktura dotačních fondů - Datové sestavy / tabulky</li>
                <li>zde si můžete sestavit vlastní tabulky z dat v žádostech a v dotačním procesu, které DSW2 umožní převést do formátu Agendio sestav</li>
            </ul>
        </div>
        <div>
            <strong>Nastavení Agendio sestav</strong>
            <ul>
                <li>v sestavách, vedle možnosti vytvořit novou sestavu, je tlačítko pro vstup do sekce nastavení Agendio sestav</li>
                <li>nastavení Agendio sestav umožňuje přednastavení Agendio sloupců požadovanými hodnotami</li>
                <li>nastavené hodnoty uložíte níže tlačítkem Uložit nastavení</li>
                <li>v Agendio sestavách se pak tyto hodnoty předvyplňují v požadovaných sloupcích</li>
            </ul>
        </div>
        <div>
            <strong>Nastavení sloupců</strong>
            <ul>
                <li>při vytváření sestavy v kroku "Přidat/upravit sloupec" je možné přidat nastavení sloupce pro Agendio</li>
                <li>pod volbou "Pokud tento sloupec chcete použít v Agendio sestavě, vyberte odpovídající Agendio sestavu a konkrétní sloupec"</li>
                <li>zvolte požadovaný sloupec z rozbalovacího seznamu pod Agendio smlouva a tím ho přiřadíte do Agendio sestav</li>
            </ul>
        </div>
        <div>
            <strong>Zobrazit jako Agendio smlouva</strong>
            <ul>
                <li>v každé sestavě je možnost Zobrazit jako Agendio smlouva</li>
                <li>toto zobrazení spojí formát Agendio se sloupci sestav a naplní je hodnotami podle "Nastavení sloupců" (viz výše)</li>
                <li>export Agendio sestavy se pak provede kliknutím na tlačítko Kopírovat</li>
                <li>v tomto případě se celá sestava zkopíruje do schránky a uživatel si jí pak vloží do libovolného tabulkového programu (Excel, OpenOffice)</li>
                <li>nebo kliknutím na tlačítko Excel se provede export a automatické stažení do formátu .xlsx</li>
            </ul>
        </div>
    </div>
</div>