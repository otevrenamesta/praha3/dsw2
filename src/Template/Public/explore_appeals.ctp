<?php

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeals Appeal[]
 * @var $threadedPrograms Program[] threaded query including all programs in the organization
 */

$this->assign('title', __('Dotační Výzvy'));
$withTime = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::APPEALS_OPEN_TO_WITH_TIME));
?>

<h1><?= $this->fetch('title') ?></h1>
<?php foreach ($appeals as $appeal) : ?>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= $appeal->name ?></h2>
            <?= sprintf(
                "%s %s %s %s",
                __('Žádosti lze podávat od'),
                $appeal->open_from->format('d.m'),
                __('do'),
                $appeal->open_to->i18nFormat($withTime ? 'dd.MM. YYYY HH:mm' : 'dd.MM. YYYY')
            ) ?>
        </div>
        <div class="card-body">
            <div class="card-text">
                <?= $appeal->description ?>
            </div>
            <div class="card-text">
                <?= __('Tato výzva se týká následujících dotačních programů a pod-programů:') ?>
                <ul>
                    <?= $this->threadedAsHtmlList($threadedPrograms, $this->getAllowedPrograms($appeal, true)) ?>
                </ul>
            </div>
            <?php if (!empty($appeal->link)) : ?>
                <div class="card-text">
                    <?= __('Více informací na:') ?>
                    <?= $this->Html->link($appeal->link, $appeal->link, ['class' => 'link']) ?>
                    <?php if (!empty($appeal->link2)) : ?>
                        <?= __('a') ?>
                        <?= $this->Html->link($appeal->link2, $appeal->link2, ['class' => 'link']) ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="card-footer">
            <small class="text-muted">
                <?= __('Publikováno:') ?> <?= $appeal->created->nice() ?>
                <?php
                if ($appeal->modified->greaterThan($appeal->created)) {
                    printf(", %s: %s", __('Upraveno'), $appeal->modified->nice());
                }
                ?>
            </small>
        </div>
    </div>
<?php endforeach; ?>
