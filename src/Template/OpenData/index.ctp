<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeals array
 * @var $states array
 * @var $requests Request[]
 */


echo $this->element('simple_datatable');
echo $this->element('simple_select2');
$this->assign('title', __('Přehled'));
?>

<?php if ($this->getSiteSetting(OrganizationSetting::HAS_DSW, true) === true) : ?>
    <div class="alert alert-info">
        <?= __('Nejsou zde vidět žádosti z předchozí verze Dotačního Software') ?>
        <br />
    </div>
<?php endif; ?>

<?= $this->Form->create(null, ['type' => 'get']) ?>

<div class="row">
    <div class="col-md">
        <?= $this->Form->control(
            'appeal_id',
            ['options' => $appeals, 'label' => __('Dotační výzva'), 'empty' => __('Všechny výzvy...')]
        ) ?>
        <?= $this->Form->submit(__('Filtrovat  podle výzvy')) ?>
        <br />
    </div>
</div>


<table class="table table-bordered" id="dtable">
    <thead>
        <tr>
            <!--<th><?= __('ID Vyúčtování') ?></th>-->
            <!--<th><?= __('ID Žádosti') ?></th>-->
            <!--<th><?= __('Vlastní identifikátor') ?></th>-->
            <th><?= __('Název žádosti') ?></th>
            <th><?= __('Dotační výzva') ?></th>
            <th><?= __('Žadatel') ?></th>
            <th><?= __('Částka k vyúčtování') ?></th>
            <th><?= __('Částka vyúčtovaná') ?></th>
            <!--<th><?= __('Datum poslední změny') ?></th>-->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($settlements as $settlement) : ?>
            <?php
            /** @var $request App\Model\Entity\Request; */
            $request = $settlement->getRequest();
            if (method_exists($request, 'extractColumn')) { // There is perhaps a better way to diferentiate between old dsw 
                $appeal_name = $request->extractColumn($request, 'appeal.name');
                $appeal_name = $appeal_name[0] ?? '';
            } else {
                $appeal_name = '';
            }
            ?>
            <?php if ($settlement->published) : ?>
                <tr>
                    <!--<td><?= $settlement->id ?></td>-->
                    <!--<td><?= $settlement->getRequest()->id ?></td>-->
                    <!--<td><?= $settlement->getRequest()->id ?></td>-->
                    <!--<td><?= $settlement->getReferenceNumber() ?></td>-->
                    <td><?= $this->Html->link($settlement->getRequestName(), ['action' => 'openDataDetail', $settlement->id], ['target' => '_blank']) ?></td>
                    <td><?= $appeal_name ?></td>
                    <td><?= $settlement->getRequesterName(false) ?></td>
                    <td class="text-right" data-type="currency"><?= Number::currency($settlement->getRequestFinalAmount(), 'CZK') ?></td>
                    <td class="text-right" data-type="currency"><?= Number::currency($settlement->getItemsSum(), 'CZK') ?></td>
                    <!--<td><?= $settlement->modified->format('d.m.Y') ?></td>-->
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>