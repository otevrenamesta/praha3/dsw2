<?php

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\SettlementAttachmentType;
use App\Model\Entity\SettlementType;
use Cake\I18n\Number;
use OldDsw\Model\Entity\Zadost;
use App\View\Helper\AnonymizeHelper;

/**
 * @var $this AppView
 * @var $attachmentTypes SettlementAttachmentType[]
 * @var $mergedRequest object
 * @var $mode
 * @var $P3Exception
 * @var $program Program
 * @var $settlement Settlement
 * @var $settlement_attachment SettlementAttachment
 * @var $settlement_item SettlementItem
 */
/** @var Zadost|Request $request */

$request = $settlement->getRequest();
$add_reference_number = (isset($request->reference_number) && !empty($request->reference_number) ? (' (' . $request->reference_number . ')') : '');
if (!isset($title_assigned)) {
    $this->assign('title', sprintf("%s #%d", $settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT
        ? __('Anonymizace vyúčtování dotace')
        : __('Anonymizace vrácení dotace'), $settlement->getRequest()->id) . $add_reference_number);
}
$request_name = $request instanceof Request ? $request->name : $request->nazev;
$request_amount = $request->final_subsidy_amount;
// is economics/payout for this settlement/id?
$request_sum = round(isset($request->konecna_castka) ? $request->konecna_castka : $request->getPaymentsSum(), 2);
$economics_payout = $request_sum;
if (!isset($request->konecna_castka)) {
    foreach ($request->getPayments() as $key => $value) {
        $economics_payout = $value->settlement_id === $settlement->id ? $value->amount_czk : $economics_payout;
    }
}

// show 'expenses_total' field
$origItemsSum = 0;
$isExpensesTotal = OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_EXPENSES_TOTAL_FIELD);
if ($isExpensesTotal) {
    $economics_payout = $settlement->expenses_total ?? 0;
    foreach ($settlement->settlement_items as $item) {
        $origItemsSum += (float)$item->original_amount;
    }
}

// budget settings
if (isset($budget) && !empty($budget)) {
    list(
        $budgetSectionsList,
        $budgetColumns,
        $budgetRequest,
        $budgetSectionsData,
        $budgetExceeded,
        $budgetExceededPartial,
        $budgetSectionsDataFull,
    ) = $budget;
}
$isBudget = (isset($budgetSectionsList) && !empty($budgetSectionsList));

// payments
$request_amount = round($request instanceof Request ? $request->getFinalSubsidyAmount() : $request->konecna_castka, 2);
$request_refund = round($request instanceof Request ? $request->getPaymentsRefundSum() : 0, 2);
$settlement_sum = $isExpensesTotal ?  round($origItemsSum, 2) : round($settlement->getItemsSum(), 2);
$settlement_subt = $economics_payout - $settlement_sum;

$requestIsWaivedCompletely = $settlement->isRequestWaivedCompletely();

// validations
list($hasOneItem, $itemsHasRequiredAttachments, $hasFinalReport, $hasProofOfPublicity, $hasAgreementNo, $hasNoUnusedAttachments) = $settlement->getValidations();

$invoiceAttachmentTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_1_INVOICES);
$proofTypes = SettlementAttachmentType::filter($attachmentTypes, SettlementAttachmentType::TYPE_2_PROOF);
$invoiceAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_1_INVOICES, true);
$proofAttachments = $settlement->getAttachmentsByType(SettlementAttachmentType::TYPE_2_PROOF, true);

$isMergedRequest = $mergedRequest && $mergedRequest->id > 0 && $mergedRequest->final_subsidy_amount > 0;
$mergedRequestAmount = $isMergedRequest ? (float)$mergedRequest->final_subsidy_amount : 0;

# See issue #396
$P3HideAttachmentName = $P3Exception && ($mode == AnonymizeHelper::ANONYMIZE_VIEW);


?>
<div class="card m-2">
    <div class="card-body">
        <h2>
            <div class="ed"><?= sprintf("%s: %s", __('Název projektu'), $request_name) ?></div>
        </h2>
        <?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
            <div class="ed"><?= sprintf("%s: %s", __('Program podpory'), $program->name) ?></div>
            <div class="ed"><?= sprintf("%s: %s", __('Žadatel'), $settlement->getRequesterName($mode != AnonymizeHelper::ANONYMIZE_VIEW)) ?></div>
            <hr />
            <div class="ed"><?= sprintf("%s: %s", __('Schválená podpora celkem'), Number::currency($request_amount + $mergedRequestAmount, 'CZK')) ?></div>
            <?php
            if (!$isMergedRequest && isset($request->original_subsidy_amount) && !empty($request->original_subsidy_amount)) {
                echo '<ul>';
                echo '<li><div class="ed">' . sprintf("%s: %s", __('Původní podpora celkem'), Number::currency($request->original_subsidy_amount, 'CZK')) . '</div></li>';
                echo '<li><div class="ed">' . sprintf("%s: %s", __('Dofinancování (navýšení původně schválené podpory)'), Number::currency($request->final_subsidy_amount - $request->original_subsidy_amount, 'CZK')) . '</div></li>';
                echo '</ul>';
            } else if ($isMergedRequest) {
                echo '<ul>';
                echo '<li><div class="ed">' . __('Schválená podpora "této" žádosti') . ': ' . Number::currency($request->final_subsidy_amount, 'CZK') . '</div></li>';
                echo '<li><div class="ed">' . __('Schválená podpora "propojené" žádosti') . ' (#' . $mergedRequest->id . ', ' . $mergedRequest->name . '): ' . Number::currency($mergedRequest->final_subsidy_amount, 'CZK') . '</div></li>';
                echo '</ul>';
            }
            ?>
            <div class="ed"><?= sprintf("%s: %s", __('Vyplacená podpora'), Number::currency($request_sum + $request_refund, 'CZK')) ?></div>
            <div class="ed"><?= $request_refund > 0 ? sprintf("%s: %s", __('Vrácená podpora'), Number::currency($request_refund, 'CZK')) : '' ?></div>
            <div class="ed">
                <?= sprintf("%s: %s", __('Nečerpaná celkem'), Number::currency($request_amount - $request_sum - $request_refund + $mergedRequestAmount, 'CZK')) ?>
                <?= $request_refund > 0 ? sprintf(" (%s: %s)", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
            </div>
            <div class="ed"><?= sprintf("%s: %s", __('Čerpaná podpora celkem'), Number::currency($request_sum, 'CZK')) ?></div>
            <hr />
            <div class="ed"><?= sprintf("%s: %s", $isExpensesTotal ? __('Celková výše nákladů na projekt') :  __('Částka k čerpání v tomto vyúčtování'), Number::currency($economics_payout, 'CZK')) ?></div>
            <div class="ed"><?= sprintf("%s: %s", __('Částka vyúčtovaná'), Number::currency($settlement_sum, 'CZK')) ?></div>
            <div class="ed">
                <?= sprintf("%s: %s", __('Částka nevyúčtovaná'), Number::currency($settlement_subt, 'CZK')) ?>
                <?= $request_refund > 0 ? sprintf(" (%s: %s) ", __('včetně vrácené podpory'), Number::currency($request_amount - $request_sum, 'CZK')) : '' ?>
            </div>
            <div class='ed'>
                <?= (isset($budgetExceeded) && boolval($budgetExceeded))
                    ? '<hr />' . sprintf("%s: &nbsp; %s<br />", __('Vyúčtované náklady vzhledem k rozpočtu'), __('Přečerpáno'))
                    : '' ?>
            </div>
        <?php else : ?>
            <div class="ed"><?= sprintf("%s: %s", __('Celkem nevyčerpáno z dotace'), Number::currency($settlement->getRequestFinalAmount(), 'CZK')) ?></div>
        <?php endif; ?>
    </div>
    <div class="card-footer">
        <div class="ed"><?= sprintf("%s: %s", __('Číslo veřejnoprávní smlouvy'), $settlement->agreement_no) ?></div>
    </div>
</div>

<?php if ($settlement->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN) : ?>
    <div class="card m-2">
        <h2 class="card-header">
            <?= __('Důvod vrácení poskytnuté podpory v plné výši') ?>
        </h2>
        <div class="card-body">
            <div class="ed"><?= $settlement->return_reason ?></div>
        </div>
    </div>
<?php endif; ?>

<?php if ($isBudget) : ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <h2><?= __('Položky rozpočtu') ?></h2>
                </div>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Položky nákladů k vyúčtování') ?></th>
                        <th class="text-right"><?= __('Částka požadovaná v žádosti') ?></th>
                        <th class="text-right"><?= __('Náklady vyúčtované') ?></th>
                        <th class="text-right"><?= __('Nevyúčtováno/přečerpáno') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($budgetSectionsDataFull as $item) : ?>
                        <tr>
                            <td><?= $item['title'] ?></td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item['total2'], 'CZK') ?></div>
                            </td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item['sum'], 'CZK') ?></div>
                            </td>
                            <td class="text-right<?= $item['diff'] < 0 ? ' text-danger' : '' ?>">
                                <div class="ed"><?= Number::currency($item['diff'], 'CZK') ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <hr />
        <div class="card-body table-responsive pb-0">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Náklady celkem – skutečně vynaložené celkové náklady') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="pl-3">
                            <div class="ed"><?php echo Number::currency($settlement->expenses_total, 'CZK') ?></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <?php foreach ($budgetColumns as $column) : ?>
                            <th class="text-right"><?= $column ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($budgetColumns as $column => $value) : ?>
                            <td class="text-right">
                                <div class="ed"><?= isset($budgetRequest) && isset($budgetRequest[$column])
                                                    ? (strpos($value, '%') ? Number::toPercentage($budgetRequest[$column]) : Number::currency($budgetRequest[$column], 'CZK'))
                                                    : '' ?></div>
                                <?= ($column === 'diff_' && $budgetRequest['diff_'] > $budgetRequest['max_']) ||
                                    ($column === 'sum_' && $budgetRequest['sum_'] > $budgetRequest['requested_amount'])
                                    ? '<br /><span class="text-danger"><div class="ed">' . __('Překročeno') . '</div></span>'
                                    : '' ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>


<?php if ($settlement->settlement_type_id === SettlementType::STANDARD_SETTLEMENT) : ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="row">
                <div class="col-md">
                    <h2><?= __('Položky vyúčtování') ?></h2>
                </div>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Položka č.') ?></th>
                        <th><?= __('Datum uhrazení') ?></th>
                        <th><?= __('Popis položky') ?></th>
                        <th><?= __('Účetní nebo daňový doklad') ?></th>
                        <th><?= __('Výpis z bankovního účtu / Výdajový pokladní doklad') ?></th>
                        <th><?= __('Ostatní přílohy') ?></th>
                        <th class="text-right"><?= __('Částka dle dokladu') ?></th>
                        <th class="text-right"><?= __('Z toho čerpáno z dotace') ?></th>
                        <th class="text-right"><?= __('Hrazeno z vlastních zdrojů') ?></th>
                        <th class="text-right"><?= __('Částka, která není k dotaci vůbec uplatňována') ?></th>
                    </tr>
                </thead>
                <?php
                $totalAmount = 0;
                $totalOrigAmount = 0;
                $totalOwnAmount = 0;
                ?>

                <tbody>
                    <?php foreach ($settlement->settlement_items as $item) : ?>
                        <tr>
                            <td>
                                <div class="ed"><?= $item->order_number ?></div>
                            </td>
                            <td>
                                <div class="ed"><?= $item->paid_when->nice() ?></div>
                            </td>
                            <td>
                                <?= isset($budgetSectionsList) && !empty($item->budget_item) && isset($budgetSectionsList[$item->budget_item])
                                    ? '<div class="ed">' . $budgetSectionsList[$item->budget_item] . '</div><hr />'
                                    : '' ?>
                                <div class="ed"><?= $item->description ?></div>
                                <?php if (!empty($item->note)) : ?>
                                    <hr />
                                    <div class="ed"><?= $item->note ?></div>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_1_INVOICES) as $attachmentByType) {
                                    $lines = explode(' - ', $attachmentByType->getLabel($P3HideAttachmentName));
                                    foreach ($lines as $line) {
                                        echo "<div class=\"ed\">$line</div>";
                                    }
                                    if ($mode == AnonymizeHelper::ANONYMIZE_EDIT) {
                                        echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['controller' => 'Economics', 'action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id], ['escapeTitle' => false]);
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_2_PROOF) as $attachmentByType) {

                                    $lines = explode(' - ', $attachmentByType->getLabel($P3HideAttachmentName));
                                    foreach ($lines as $line) {
                                        echo "<div class=\"ed\">$line</div>";
                                    }
                                    if ($mode == AnonymizeHelper::ANONYMIZE_EDIT) {
                                        echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['controller' => 'Economics', 'action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id], ['escapeTitle' => false]);
                                    }
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach ($settlement->getAttachmentsByItem($item->id, SettlementAttachmentType::TYPE_3_OTHERS) as $attachmentByType) {
                                    $lines = explode(' - ', $attachmentByType->getLabel($P3HideAttachmentName));
                                    foreach ($lines as $line) {
                                        echo "<div class=\"ed\">$line</div>";
                                    }
                                    if ($mode == AnonymizeHelper::ANONYMIZE_EDIT) {
                                        echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['controller' => 'Economics', 'action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachmentByType->file_id], ['escapeTitle' => false]);
                                    }
                                }
                                ?>
                            </td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item->original_amount, 'CZK') ?></div>
                            </td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item->amount, 'CZK') ?></div>
                            </td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item->own_amount, 'CZK') ?></div>
                            </td>
                            <td class="text-right">
                                <div class="ed"><?= Number::currency($item->original_amount - $item->amount - $item->own_amount, 'CZK') ?></div>
                            </td>
                            <?php
                            $totalAmount += (float)$item->amount;
                            $totalOrigAmount += (float)$item->original_amount;
                            $totalOwnAmount += (float)$item->own_amount;
                            ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (empty($settlement->settlement_items)) : ?>
                        <tr>
                            <td colspan="9" class="text-center">
                                <div class="ed"><?= __('Vyúčtování neobsahuje žádné položky') ?></div>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="6"><?= __('Celkem') ?></th>
                        <th class="text-right">
                            <div class="ed"><?= Number::currency($totalOrigAmount, 'CZK') ?></div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= Number::currency($totalAmount, 'CZK') ?></div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= Number::currency($totalOwnAmount, 'CZK') ?></div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= Number::currency($totalOrigAmount - $totalAmount - $totalOwnAmount, 'CZK') ?></div>
                        </th>
                    </tr>
                    <tr class="thead-dark">
                        <?php if ($isExpensesTotal) {
                            $totalOrigAmount = $economics_payout;
                        } ?>
                        <th colspan="6">
                            <div class="ed"><?= $isExpensesTotal ? __('Podíl z celkové výše nákladů na projekt ') . Number::currency($economics_payout, 'CZK') : __('Podíl') ?>
                            </div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= $isExpensesTotal ? '' : number_format(100, 2) . '%' ?></div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2) ?> %</div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2) ?> %</div>
                        </th>
                        <th class="text-right">
                            <div class="ed"><?= number_format(100 - number_format(!$totalOrigAmount ? 0 : (100 * $totalAmount / $totalOrigAmount), 2) - number_format(!$totalOrigAmount ? 0 : (100 * $totalOwnAmount / $totalOrigAmount), 2), 2) ?> %</div>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="card m-2">
        <h2 class="card-header"><?= __('Přílohy vyúčtování') ?></h2>
        <div class="card-body table-responsive">
            <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th><?= __('Příloha č.') ?></th>
                        <th><?= __('Název souboru') ?></th>
                        <th><?= __('Typ') ?></th>
                        <th><?= __('Velikost') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($settlement->sortedAttachments() as $attachment) :
                        // Hide used attachments with settlement_attachment_type [1, 2, 5]
                        if ($attachment->isUsed($settlement) && in_array($attachment->settlement_attachment_type_id, [1, 2, 5])) continue;
                    ?>
                        <?php if ($mode == AnonymizeHelper::ANONYMIZE_EDIT && $attachment->settlement_attachment_type->id == AnonymizeHelper::TYPE_ANONYMIZED) continue; ?>
                        <tr>
                            <td>
                                <div class="ed"><?= $attachment->file_id ?></div>
                            </td>
                            <?php if ((!$P3HideAttachmentName) || ($mode == AnonymizeHelper::ANONYMIZE_VIEW && $attachment->settlement_attachment_type->id == AnonymizeHelper::TYPE_ANONYMIZED)) {
                                $file_name = h($attachment->file->original_filename);
                            } else {
                                $file_name = __("Skrytý název souboru");
                            } ?>
                            <td>
                                <div class="ed"><?= $file_name ?>
                                    <?php
                                    if ($mode == AnonymizeHelper::ANONYMIZE_VIEW && $attachment->settlement_attachment_type->id == AnonymizeHelper::TYPE_ANONYMIZED) {
                                        echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['action' => 'openDataPublicFile', 'id' => $settlement->id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]);
                                    } elseif ($mode == AnonymizeHelper::ANONYMIZE_EDIT) {
                                        echo $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['controller' => 'Economics', 'action' => 'settlementDownloadFile', 'settlement_id' => $settlement->id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]);
                                    }
                                    ?>
                                </div>
                            </td>

                            <td>
                                <div class="ed"><?= $attachment->settlement_attachment_type->type_name ?></div>
                            </td>
                            <td>
                                <div class="ed"><?= Number::toReadableSize($attachment->file->filesize) ?></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>