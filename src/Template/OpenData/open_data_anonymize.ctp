<?php

use App\View\Helper\AnonymizeHelper;
use Cake\I18n\Number;

$csrfToken = $this->request->getParam('_csrfToken');
$this->assign('title', __('Anonymizace vyúčtování žádosti'));

$mode = AnonymizeHelper::ANONYMIZE_EDIT;
// Capture the output of the template

ob_start();
include "open_data_template.ctp";
$content = ob_get_clean();


echo $this->Anonymize->processReplacements($content, $settlement->anynonym_mask ? $settlement->anynonym_mask : "{}", AnonymizeHelper::ANONYMIZE_EDIT);

$canEdit = true;
?>

<div class="card m-2">
    <h2 class="card-header"><?= __('Anonymizované přílohy') ?></h2>
    <div class="card-body table-responsive">
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th><?= __('Příloha č.') ?></th>
                    <th><?= __('Název souboru') ?></th>
                    <th><?= __('Typ') ?></th>
                    <th><?= __('Velikost') ?></th>
                    <?php if ($canEdit) : ?>
                        <th><?= __('Akce') ?></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($settlement->sortedAttachments() as $attachment) : ?>
                    <?php if ($mode == AnonymizeHelper::ANONYMIZE_EDIT && $attachment->settlement_attachment_type->id != AnonymizeHelper::TYPE_ANONYMIZED) continue; ?>
                    <tr>
                        <td>
                            <?= $attachment->file_id ?>
                        </td>
                        <td>
                            <?= h($attachment->file->original_filename) ?>
                            <?= $this->Html->link('<i class="fas fa-download ml-2 mr-2"></i>', ['action' => 'openDataDownloadFile', 'id' => $settlement->id, 'file_id' => $attachment->file_id], ['escapeTitle' => false]) ?>
                        </td>
                        <td><?= $attachment->settlement_attachment_type->type_name ?></td>
                        <td><?= Number::toReadableSize($attachment->file->filesize) ?></td>
                        <?php if ($canEdit) : ?>
                            <td>
                                <?= $this->Html->link(__('Upravit'), ['action' => 'openDataAddModifyFile', 'id' => $settlement->id, 'file_id' => $attachment->id]) ?>
                                ,
                                <?= $this->Html->link(__('Stáhnout soubor'), ['action' => 'openDataDownloadFile', 'id' => $settlement->id, 'file_id' => $attachment->file_id], ['class' => 'text-success']) ?>
                                ,
                                <?= $this->Form->postLink(__('Smazat'), ['action' => 'openDataDeleteFile', 'id' => $settlement->id, 'file_id' => $attachment->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete přílohu vymazat?')]) ?>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php if ($canEdit) : ?>
        <div class="card-footer">
            <strong><?= __('Nahrát novou přílohu') ?></strong> <br />
            <?php
            echo $this->Form->create($settlement_attachment, ['type' => 'file', 'url' => ['action' => 'openDataAddModifyFile', 'id' => $settlement->id]]);
            ?>
            <div class="row">
                <div class="col-md-8 pt-2rem">
                    <?= $this->Form->control('filedata', ['type' => 'file', 'label' => __('Vyberte soubor přílohy'), 'required' => true]); ?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->hidden('settlement_attachment_type_id', ['default' => AnonymizeHelper::TYPE_ANONYMIZED]); ?>
                </div>
            </div>
            <?php
            echo $this->Form->submit(__('Nahrát novou přílohu'));
            echo $this->Form->end();
            ?>
        </div>
    <?php endif; ?>
</div>

<div class="card m-2">
    <h2 class="card-header"><?= __('Zveřejnění') ?></h2>
    <div class="card-body">
        <?= $this->Html->link(__('Uložit a zveřejnit'), ['action' => 'openDataSaveAndPublish', 'id' => $settlement->id], ['class' => 'btn btn-primary mb-2']) ?>
        <?= $this->Html->link(__('Jen uložit'), ['action' => 'openDataSaveOnly', 'id' => $settlement->id], ['class' => 'btn btn-success mb-2']) ?>
        <?= $this->Html->link(__('Vymazat všechny anonymizace'), ['action' => 'openDataDiscard', 'id' => $settlement->id], ['class' => 'btn btn-danger mb-2',  'confirm' => __('Opravdu chcete vymazat všechny anonymizace ?')]) ?>

    </div>
</div>


<style type="text/css">
    .edited {
        background-color: rgb(255, 251, 204, 0.8);
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('#main-content').domEdit({
            editorClass: 'editor',
            onSetEditorStyle: function($editor, $editingElement) {
                $editor.css('border-style', 'dotted');
                $editor.css('border-width', '1px');
                $editor.css('outline', 'none');
            }
        });
    });
    (function($) {
        var editorId = 'dom-edit-' + Date.now();
        var editorHTML = '<textarea id="' + editorId + '"></textarea>';
        var $editor = $(editorHTML);
        var $currentTargetElement = null;

        function preventDefaultEvents(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        function getTargetElementBoundingRect($aTargetElement) {
            var offset = $aTargetElement.offset();
            return {
                left: offset.left,
                top: offset.top,
                width: $aTargetElement.width(),
                height: $aTargetElement.height()
            };
        }


        function closeDomEditor(escape) {
            $editor.remove();
            if (escape) return;

            if ($currentTargetElement) {
                if ($currentTargetElement.attr('data-orig')) { // Has been editted before
                    $orig = $currentTargetElement.attr('data-orig');
                } else {
                    $orig = $currentTargetElement.html();
                }
                $new = $editor.val();
                $new_strip = replaceNbsps($new).trim()
                $orig_strip = replaceNbsps($orig).trim()
                if (strcmp($new_strip, $orig_strip) != 0) {
                    if (!$currentTargetElement.attr('data-orig')) {
                        $currentTargetElement.attr('data-orig', $orig);
                        $currentTargetElement.attr('data-toggle', "tooltip");
                        $currentTargetElement.attr('data-placement', "bottom");
                        $currentTargetElement.attr('title', $orig);
                        $currentTargetElement.addClass('edited');
                    }
                    $currentTargetElement.html($editor.val());
                    $currentTargetElement.addClass('ed');
                    uploadEdits();
                }
            }

            $currentTargetElement = null;
            //$(document).off('click', closeDomEditor);
        }

        function editorClick(e) {
            preventDefaultEvents(e);
        }

        function setEditorStyle($element, opts) {
            $editor.css(getTargetElementBoundingRect($element));
            $editor.css('font-size', $element.css('font-size'));
            $editor.css('font-weight', $element.css('font-weight'));
            $editor.css('text-align', $element.css('text-align'));
            $editor.css('font-family', $element.css('font-family'));
            $editor.css('padding', $element.css('padding'));
            $editor.css('position', 'absolute');
            $editor.css('margin-top', '0px');
            $editor.css('padding', '0px');

            if (opts && opts.onSetEditorStyle) {
                opts.onSetEditorStyle($editor, $element);
            }
        }

        function setEditorState($element) {
            var text = jQuery('<div>').html($element.html()).text()
            text = text.trim();
            text = text.replace(/\s\s+/g, ' ');
            $editor.val(text);
            $editor.select();
            $editor.focus();
            $editor.click(editorClick);
            $editor.blur(closeDomEditor);
            $editor.on('keyup', function(e) {
                if (e.key == "Escape") {
                    closeDomEditor(true)
                }
                if (e.key == "Enter") {
                    closeDomEditor()
                }
            });

        }

        function uploadEdits() {
            var data = []
            $("*[data-orig]").each(function(index) {
                var search = replaceNbsps($(this).attr("data-orig"))
                var replace = replaceNbsps($(this).text())
                data.push([search, replace])
            });

            $.ajax({
                url: '/admin/opendata/<?= $settlement->id ?>/anonymize/upload_edits',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', '<?= $csrfToken ?>');
                },
                type: "POST",
                data: {
                    anonym_mask: JSON.stringify(data),
                }
            });
        }

        function replaceNbsps(str) {
            var re = new RegExp(String.fromCharCode(160), "g");
            out = str.replace(re, " ");
            out = out.replaceAll("&nbsp;", " ");
            return out.trim()
        }

        function strcmp(a, b) {
            a = a.toString(), b = b.toString();
            for (var i = 0, n = Math.max(a.length, b.length); i < n && a.charAt(i) === b.charAt(i); ++i);
            if (i === n) return 0;
            return a.charAt(i) > b.charAt(i) ? -1 : 1;
        }

        $.fn.domEdit = function(options) {
            var defaultOptions = {
                editorClass: ''
            }

            var opts = $.extend(defaultOptions, options);
            $editor.addClass(opts.editorClass);

            return this.each(function(idx, element) {
                $(element).dblclick(function(e) {
                    preventDefaultEvents(e);
                    var target = e.target;
                    var $body = $(document.body);

                    if (target === $editor[0] || target === document.body || !$body.has(target)) return;

                    var $element = $(target);
                    if (!$element.hasClass("ed")) return
                    if (!$editor.parent().length) {
                        $body.append($editor);
                    }

                    setEditorStyle($element, opts);
                    setEditorState($element);
                    //$(document).on('click', closeDomEditor);
                    $currentTargetElement = $element;
                });
            });
        }
    })(jQuery);
</script>