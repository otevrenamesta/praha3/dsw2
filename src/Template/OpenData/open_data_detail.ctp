<?php
use App\View\Helper\AnonymizeHelper;

$this->assign('title', __('Vyúčtování dotace'));
$title_assigned = true;
// Capture the output of the template
$mode = AnonymizeHelper::ANONYMIZE_VIEW;
ob_start();
include "open_data_template.ctp";
$output = ob_get_contents();
ob_end_clean();

// Perform anonymisation replacements 
echo $this->Anonymize->processReplacements($output,$settlement->anynonym_mask ? $settlement->anynonym_mask:"{}" , AnonymizeHelper::ANONYMIZE_VIEW);
