<?php

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $realms array
 * @var $filters array
 * @var $appeals Appeal[]
 */

$this->assign('title', __('Dotační Výzvy'));
$withTime = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::APPEALS_OPEN_TO_WITH_TIME));
?>

<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link(__('Vytvořit novou výzvu'), ['action' => 'appealAddModify'], ['class' => 'btn btn-success m-2']) ?>

<?= $this->Form->create(null, ['type' => 'get']) ?>
<div class="card mt-2 mb-2">
    <h2 class="card-header"><?= __('Filtrovat') ?></h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('realm_id', ['options' => $realms, 'value' => ($filters['realm_id'] ?? 0), 'label' => __('Oblasti podpory v programech')]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Filtrovat'), ['class' => 'btn btn-secondary']) ?>
    </div>
</div>
<?= $this->Form->end() ?>

<table class="table dataTable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Název') ?></th>
            <th><?= __('Programy') ?></th>
            <th><?= __('Platnost') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($appeals as $appeal) :

            if (!empty($filters['realm_id'])) {
                $found_programs = [];
                foreach ($appeal->programs as $program) {
                    if ($filters['realm_id'] == $program->realm_id) {
                        $found_programs[] = $program;
                    }
                }
                if (count($found_programs) <= 0) {
                    continue;
                }
            }

        ?>
            <tr>
                <td><?= $appeal->id ?></td>
                <td><?= $this->Html->link($appeal->name, ['action' => 'appealAddModify', $appeal->id]) ?></td>
                <td>
                    <?php foreach ($appeal->programs as $program) :
                    ?>
                        <?= $this->Html->link($program->name, ['action' => 'programAddModify', $program->id]) ?>
                        <br />
                    <?php endforeach; ?>
                </td>
                <td>
                    <?= $appeal->open_from->nice() ?> - <?= $appeal->open_to->i18nFormat($withTime ? 'dd.MM. YYYY HH:mm' : 'dd.MM. YYYY') ?> <br /><br />
                    <?= $appeal->is_active ? __('Aktivní') : __('Neaktivní') ?> <br />
                    <?= $this->Html->link(
                        $appeal->is_active
                            ? sprintf('(%s)', __('Deaktivovat'))
                            : sprintf("(%s)", __('Aktivovat')),
                        ['action' => 'appealToggle', $appeal->id],
                        ['class' => ($appeal->is_active ? 'text-danger' : 'text-sucess')]
                    ) ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Upravit'), ['action' => 'appealAddModify', $appeal->id]) ?>,
                    <?= $this->Form->postLink(
                        __('Smazat'),
                        ['action' => 'appealDelete', $appeal->id],
                        ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto výzvu?')]
                    ) ?>,<br />
                    <?= $this->Html->link(
                        __('Nastavení financí a termínů'),
                        ['action' => 'appealDetailSettings', $appeal->id],
                        ['class' => 'text-success']
                    ) ?>,<br />
                    <?= $this->Html->link(
                        __('Odeslat informaci o nové výzvě'),
                        ['action' => 'appealIsBeginning', $appeal->id],
                        ['class' =>  empty($appeal->begin_sent) ? 'text-info' : 'text-secondary']
                    ) ?>,<br />
                    <?= $this->Html->link(
                        __('Odeslat informaci o končící lhůtě'),
                        ['action' => 'appealIsEnding', $appeal->id],
                        ['class' => empty($appeal->end_sent) ? 'text-info' : 'text-secondary']
                    ) ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2">
                    <?= $appeal->description ?>
                </td>
                <td colspan="2">
                    <?= $this->Html->link($appeal->link, $appeal->link, ['target' => '_blank']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>