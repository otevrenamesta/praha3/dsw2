<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeal Appeal
 */
$this->assign('title', __('Odeslat informaci o nové výzvě'));
?>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-8">
                <h2><?= $this->fetch('title') ?></h2>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Hromadné odeslání informace o nové výzvě na e-maily všech uživatelů dotačního portálu') ?>
        </div>
        <?php
        echo empty($appeal->begin_sent) ? '' : '<div class="alert alert-warning"><strong>' . __('Upozornění!') . '</strong> ' .
            __('Hromadné odeslání již proběhlo dne') . ': ' . $appeal->begin_sent->nice() . '</div><br />';

        echo $this->Form->create(null);

        echo $this->Form->control('subject', [
            'label' => __('Předmět e-mailu'),
            'required' => true,
            'default' => __('Upozornění na novou výzvu')
        ]);

        echo $this->Form->control('contents', [
            'label' => __('Kompletní text e-mailu'),
            'required' => true,
            'data-quilljs' => 'data-quilljs',
            'rows' => 10,
            'default' => sprintf(
                __('Dobrý den') . ",<br/><br/>" .
                    __('na dotačním portále je vypsána nová výzva') . ":<br />" .
                    "%s<br /><br />%s<br /><br />S pozdravem<br/>%s, v z. %s",
                $appeal->name,
                $appeal->description,
                $this->getSiteSetting(OrganizationSetting::SITE_NAME),
                $this->getCurrentUser()->email
            )
        ]);

        echo $this->Form->control('type', ['value' => 'beginning', 'type' => 'hidden']);
        echo $this->Form->submit(__('Odeslat'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>