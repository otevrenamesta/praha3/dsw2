<?php

use App\Model\Entity\Program;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $fonds array
 * @var $realms array
 * @var $filters array
 * @var $programs Program[]
 */

$this->assign('title', __('Dotační Programy'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<h6><?= __('Jsou zobrazeny pouze programy, pod-programy můžete spravovat otevřením skupiny') ?></h6>
<?= $this->Html->link(__('Přidat nový Program'), ['action' => 'programAddModify'], ['class' => 'btn btn-success m-2']) ?>

<?= $this->Form->create(null, ['type' => 'get']) ?>
<div class="card mt-2 mb-2">
    <h2 class="card-header"><?= __('Filtrovat') ?></h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('realm_id', ['options' => $realms, 'value' => ($filters['realm_id'] ?? 0), 'label' => __('Oblasti podpory')]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->control('fond_id', ['options' => $fonds, 'value' => ($filters['fond_id'] ?? 0), 'label' => __('Fondy')]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Filtrovat'), ['class' => 'btn btn-secondary']) ?>
    </div>
</div>
<?= $this->Form->end() ?>

<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Název programu') ?></th>
            <th><?= __('Formuláře programu') ?></th>
            <th><?= __('Podprogramy') ?></th>
            <th><?= __('Oblast Podpory') ?></th>
            <th><?= __('Fond') ?></th>
            <th><?= __('Hodnotící kritéria') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($programs as $program) :
            if (!empty($filters['realm_id']) && $filters['realm_id'] !== $program->realm_id) {
                continue;
            }
            if (!empty($filters['fond_id']) && $filters['fond_id'] !== $program->realm->fond_id) {
                continue;
            }
        ?>
            <tr>
                <td>
                    <?= $program->id ?>
                </td>
                <td>
                    <?= $this->Html->link($program->name, ['action' => 'programAddModify', $program->id],  ['title' => __('Program') . ' #' . $program->id]) ?>
                </td>
                <td>
                    <?php if (!empty($program->forms)) : ?>
                        <?php foreach ($program->forms as $key => $form) : ?>
                            <?= $key !== 0 ? '<hr/>' : '' ?>
                            <?= $this->Html->link($form->name, ['controller' => 'Forms', 'action' => 'formDetail', $form->id], ['target' => '_blank', 'title' => __('Formulář') . ' #' . $form->id]) ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </td>
                <td>
                    <?php foreach ($program->child_programs as $key => $child_program) : ?>
                        <?= $key !== 0 ? '<hr/>' : '' ?>
                        <?= $this->Html->link(h($child_program->name), ['action' => 'programAddModify', $child_program->id], ['title' => __('Podprogram') . ' #' . $child_program->id]) ?>
                    <?php endforeach; ?>
                </td>
                <td><?= $this->Html->link($program->realm->name, ['action' => 'realmAddModify', $program->realm_id], ['title' => __('Oblast') . ' #' . $program->realm_id]) ?></td>
                <td><?= $this->Html->link($program->realm->fond->name, ['action' => 'fondAddModify', $program->realm->fond_id], ['title' => __('Fond') . ' #' . $program->realm->fond_id]) ?></td>
                <td><?= $program->evaluation_criterium ? $this->Html->link($program->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $program->evaluation_criteria_id], ['title' => __('Kritéria') . ' #' . $program->evaluation_criteria_id]) : '' ?></td>
                <td>
                    <?= $this->Html->link(__('Otevřít'), ['action' => 'programAddModify', $program->id]) ?>,
                    <?= $this->Form->postLink(__('Smazat program'), ['action' => 'programDelete', $program->id], ['confirm' => __('Opravdu chcete smazat tento program, včetně jeho nastavení a podprogramů (pokud jsou nastaveny)?'), 'class' => 'text-danger text-nowrap']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>