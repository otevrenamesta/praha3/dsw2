<?php

use App\Model\Entity\Realm;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $fonds array
 * @var $filters array
 * @var $realms Realm[]
 */
$this->assign('title', __('Oblasti Podpory'));
echo $this->element('simple_datatable');
?>
<h1><?= $this->fetch('title') ?></h1>
<?= $this->Html->link('Vytvořit novou oblast podpory', ['action' => 'realmAddModify'], ['class' => 'btn btn-success m-2']) ?>

<?= $this->Form->create(null, ['type' => 'get']) ?>
<div class="card mt-2 mb-2">
    <h2 class="card-header"><?= __('Filtrovat') ?></h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->control('fond_id', ['options' => $fonds, 'value' => ($filters['fond_id'] ?? 0), 'label' => __('Fondy')]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Filtrovat'), ['class' => 'btn btn-secondary']) ?>
    </div>
</div>
<?= $this->Form->end() ?>

<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Váha') ?></th>
            <th><?= __('Název Oblasti') ?></th>
            <th><?= __('Dotační Fond') ?></th>
            <th><?= __('Stav') ?></th>
            <th><?= __('Podřízené Programy') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($realms as $realm) :
            if (!empty($filters['fond_id']) && $filters['fond_id'] !== $realm->fond_id) {
                continue;
            }
        ?>
            <tr>
                <td><?= $realm->id ?></td>
                <td><?= $realm->weight ?></td>
                <td><?= $this->Html->link($realm->name, ['action' => 'realmAddModify', $realm->id]) ?></td>
                <td><?= $this->Html->link($realm->fond->name, ['action' => 'fondAddModify', $realm->fond_id]) ?></td>
                <td>
                    <?= $realm->is_enabled ? 'Povolen' : 'Zakázán' ?> <br />
                    <?= $this->Html->link($realm->is_enabled ? '(Zakázat)' : '(Povolit)', ['action' => 'realmToggle', $realm->id], ['class' => $realm->is_enabled ? 'text-danger' : 'text-success']) ?>
                </td>
                <td>
                    <ul style="margin-left:-1.5em;">
                        <?php
                        $listed = array();
                        foreach ($realm->programs as $program) {
                            if ($program->parent_id <= 0) {
                                echo  '<li>' . $this->Html->link($program->name, ['action' => 'programAddModify', $program->id]);
                                $listed[] = $program->id;
                                $subprograms = '';
                                foreach ($realm->programs as $subprogram) {
                                    if ($program->id === $subprogram->parent_id) {
                                        $subprograms .=  '<li>' . $this->Html->link($subprogram->name, ['action' => 'programAddModify', $subprogram->id]) . '</li>';
                                        $listed[] = $subprogram->id;
                                    }
                                }
                                echo  $subprograms  ? '<ul>' . $subprograms . '</ul></li>' : '</li>';
                            }
                        }
                        foreach ($realm->programs as $subprogram) {
                            if (!in_array($subprogram->id, $listed)) {
                                echo '<li>' . $this->Html->link($subprogram->name, ['action' => 'programAddModify', $subprogram->id]) .
                                    ' <small class="text-danger"> (' . __('podprogram, který je v jiné oblasti podpory, než nadřazený program') . ')</small> </li>';
                            }
                        }
                        ?>
                    </ul>
                    <?= $this->Html->link('<i class="fas fa-plus-circle"></i> Přidat nový podřízený program', ['action' => 'programAddModify', 'realm_id' => $realm->id], ['escapeTitle' => false, 'class' => 'text-success']) ?>
                </td>
                <td>
                    <?= $this->Html->link('Upravit', ['action' => 'realmAddModify', $realm->id]) ?>,
                    <?= $this->Form->postLink('Smazat', ['action' => 'realmDelete', $realm->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto oblast podpory?')]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>