<?php

use App\Model\Entity\Fond;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organizations array
 * @var $fond Fond
 */

$this->assign('title', $fond->id > 0 ? __('Upravit Fond') : __('Vytvořit nový Fond'))
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
echo $this->Form->create($fond);
echo $this->Form->control('name', ['label' => __('Jméno')]);
echo $this->Form->control('description', ['label' => __('Popis')]);
echo $this->Form->control('is_enabled', ['type' => 'checkbox', 'label' => __('Aktivní?')]);
echo $this->Form->control('is_hidden', ['type' => 'checkbox', 'label' => __('Skrýt ve veřejné struktuře dotačních fondů?')]);
if ($this->isSystemsManager()) {
    echo $this->Form->control('organization_id', ['options' => $organizations]);
}
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
echo $this->Form->end();
echo $this->Html->link(__('Zahodit změny a vrátit se na seznam fondů'), ['action' => 'indexFonds'], ['class' => 'btn btn-warning mt-2']);