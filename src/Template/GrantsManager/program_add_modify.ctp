<?php

use App\Model\Entity\EvaluationCriterium;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\ProjectBudgetDesign;
use App\Model\Entity\Realm;
use App\Model\Entity\Team;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $allowed_programs Program[]
 * @var $allowed_realms Realm[]
 * @var $allowed_teams Team[]
 * @var $disableFormUpdate boolean
 * @var $usedInRequests array
 * @var $evaluation_criteria EvaluationCriterium[]
 * @var $forms array
 * @var $program Program
 * @var $projectBudgetDesigns array
 */


$UstiException =  OrganizationSetting::isException(OrganizationSetting::USTI);
$mergingOfRequests = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MERGING_OF_REQUESTS);

echo $this->element('simple_select2');
$this->assign('title', $program->id > 0
    ? ($program->parent_id > 0 ? __('Upravit Pod-program') : __('Upravit Program'))
    : __('Vytvořit nový Program nebo Pod-Program'));

echo $this->Form->create($program);
$submitButton = $this->Form->button(__('Uložit'), ['class' => 'btn btn-success m-2', 'type' => 'submit']);
?>
<div class="card mt-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h1><?= $this->fetch('title') ?></h1>
            </div>
            <div class="col-md-1 text-right">
                <?= $submitButton ?>
            </div>
            <?php
            if ($program->parent_id) {
            ?>
                <div class="col-md-3 text-right">
                    <?= $this->Html->link(__('Zahodit změny a vrátit se k nadřazenému programu'), ['action' => 'programAddModify', $program->parent_id], ['class' => 'btn btn-warning m-2']); ?>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="card-body">
        <?php
        $isSubprogram = $program->parent_id > 0 ? true : false;
        $listOfRequests = [];
        if (isset($usedInRequests) && !empty($usedInRequests)) {
            foreach ($usedInRequests as $key => $value) {
                if (isset($value->id) && !in_array($value->id, $listOfRequests)) {
                    $listOfRequests[] = $value->id;
                }
            }
        }
        echo count($listOfRequests) > 0 ? '<div class="alert alert-dismissible alert-warning">' .
            __('Upozornění: K tomuto programu/podprogramu již existují vyplněné žádosti s ID') . ': ' . implode(', ', $listOfRequests) .
            '</div><br />' : '';
        echo $this->Form->control('name', ['label' => __('Název'), 'type' => 'text', 'maxlength' => 255]);
        echo $this->Form->control('description', ['label' => __('Popis')]);
        echo $this->Form->control('parent_id', ['options' => $allowed_programs, 'disabled' => $isSubprogram,  'empty' => __('Program není podřízený'), 'label' => __('Nadřízený Program')]);
        echo $this->Form->control('realm_id', ['options' => $allowed_realms, 'disabled' => $isSubprogram, 'empty' => false, 'label' => __('Oblast Podpory'), 'escape' => false]);
        echo $this->Form->control('evaluation_criteria_id', ['options' => $evaluation_criteria, 'empty' => __('Ponechte prázdné, pokud se žádosti v tomto programu nehodnotí podle kritérií'), 'label' => __('Hodnotící kritéria')]);
        echo $this->Form->control('forms._ids', [
            'options' => $forms,
            'empty' => true,
            'label' => __('Formuláře, které musí <strong>žadatel vyplnit před odevzdáním žádosti</strong>'),
            'class' => 'select2',
            'escape' => false
        ]);
        echo !$disableFormUpdate ? '' : '<div class="alert alert-dismissible alert-warning">' . __('Formuláře jsou již vyplněny v odeslaných žádostech. Nelze provádět jejich úpravy.') . '</div><br />';
        echo $this->Form->control('paper_forms._ids', [
            'options' => $forms,
            'empty' => true,
            'label' => __('Formuláře, které <strong>úřad musí opsat z papírově podané žádosti</strong>'),
            'class' => 'select2',
            'escape' => false
        ]);
        echo !$disableFormUpdate ? '' : '<div class="alert alert-dismissible alert-warning">' . __('Formuláře jsou již vyplněny v odeslaných žádostech. Nelze provádět jejich úpravy.') . '</div><br />';
        echo $this->Form->control('weight', ['label' => __('Váha (pro řazení)'), 'description' => __('Čím "težší", tím níže v pořadí. Je dobré zvolit třeba krok 10 (10, 20, 30, atd...)')]);
        ?>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>

<div class="card mt-2">
    <h2 class="card-header"><?= __('Nastavení dotačního procesu') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('project_budget_design_id', ['label' => __('Forma rozpočtu projektu'), 'options' => $projectBudgetDesigns, 'empty' => false, 'default' => ProjectBudgetDesign::DESIGN_P3]);
        ?>
        <div class="project-budget-design">
            <?php
            echo $this->Form->control('requires_balance_sheet', ['label' => __('Povolit sekci "Ekonomická rozvaha" v rozpočtu projektu'), 'type' => 'checkbox', 'class' => 'pbd-1']);
            echo $this->Form->control('budget_hide_earnings', ['label' => __('Schovat v ekonomické rozvaze sekci "Výnosy"'), 'type' => 'checkbox', 'class' => 'pbd-1']);
            echo $this->Form->control('requires_budget', ['label' => __('Povolit sekci "Vlastní zdroje a dotace z jiných zdrojů" v rozpočtu projektu'), 'type' => 'checkbox', 'class' => 'pbd-1']);
            echo $this->Form->control('requires_extended_budget', ['label' => __('V sekci rozpočtu, požadovat rozepsat nákladové položky jako náklad celkem a požadovaná částka s nepovinným komentářem'), 'type' => 'checkbox', 'class' => 'pbd-1']);
            ?>
            <script type="text/javascript">
                $(function() {
                    $("#project-budget-design-id").on('change', function() {
                        let allowedClass = 'pbd-' + parseInt($(this).val());
                        console.log('allowed ' + allowedClass);
                        let $container = $(".project-budget-design");
                        $("input, select", $container).each(function() {
                            $(this).closest('.form-group').toggle($(this).hasClass(allowedClass));
                        });
                    }).change();
                });
            </script>
        </div>
        <hr />
        <?php
        if ($UstiException) {
            echo $this->Form->control('requires_pre_evaluation', ['label' => __('Povolit hodnocení v rámci formální kontroly'), 'type' => 'checkbox']);
        }
        echo $this->Form->control('not_require_tax_documents', ['label' => __('Nevyžadovat ve vyúčtování účetní nebo daňový doklad a výpis BÚ'), 'type' => 'checkbox']);
        echo !$mergingOfRequests ? '' : $this->Form->control('merging_of_requests', ['label' => __('Zobrazovat možnost propojení dvou žádostí'), 'type' => 'checkbox']);
        echo $this->Form->control('settlement_date', ['type' => 'date', 'label' => __('Termín pro podání vyúčtování'), 'empty' => true]);

        ?>
        <?php
        // Disabled per request https://gitlab.com/otevrenamesta/praha3/dsw2/-/issues/169
        /*
        echo $this->Form->control('formal_check_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se formální kontrola neprovádí'), 'label' => __('Kdo provádí Formální kontrolu?')]);
        echo $this->Form->control('price_proposal_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se výše dotace nenavrhuje dopředu'), 'label' => __('Kdo navrhuje výši dotace?')]);
        echo $this->Form->control('price_approval_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se výše dotace neschvaluje před jednáním zastupitelstva/rady'), 'label' => __('Kdo schvaluje výši dotace?')]);
        echo $this->Form->control('comments_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud se neprovádí hodnocení dle hodnotících kritérií'), 'label' => __('Kdo provádí hodnocení dotace?')]);
        echo $this->Form->control('request_manager_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud vše bude provádět správce dotačního portálu'), 'label' => __('Kdo může nastavit výsledný status dotace?')]);
        echo $this->Form->control('preview_team_id', ['options' => $allowed_teams, 'empty' => __('Ponechte prázdné, pokud nemáte nikoho kdo na žádosti jen nahlíží'), 'label' => __('Kdo nahlíží na žádosti v tomto programu, bez možnosti zasahovat do dotačního procesu?')]);
        */

        ?>
    </div>
    <div class="card-footer">
        <?= $submitButton ?>
    </div>
</div>
<?php
echo $this->Form->end();
if (!$program->isNew() && (empty($program->parent_id) || !empty($program->child_programs))) : ?>

    <div class="card mt-2">
        <div class="card-header">
            <h2><?= __('Podprogramy') ?></h2>
            <?= $this->Html->link(__('Přidat nový podprogram'), ['action' => 'programAddModify', 'parent_id' => $program->id], ['class' => 'btn btn-success mb-2']) ?>
        </div>
        <div class="card-body">
            <table id="dtable" class="table">
                <thead>
                    <tr>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Oblast') ?></th>
                        <th><?= __('Formuláře') ?></th>
                        <th><?= __('Hodnotící kritéria') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($program->child_programs as $childProgram) :
                        $realmName = !empty($childProgram->realm_id) && !empty($allowed_realms) && isset($allowed_realms[$childProgram->realm_id])
                            ? $allowed_realms[$childProgram->realm_id]
                            : '';
                    ?>
                        <tr>
                            <td><?= $this->Html->link($childProgram->name, ['action' => 'programAddModify', $childProgram->id]) ?></td>
                            <td><?= $realmName ? $this->Html->link($realmName, ['action' => 'realmAddModify', $childProgram->realm_id]) : '' ?></td>
                            <td>
                                <?php foreach (empty($childProgram->forms) ? $program->forms : $childProgram->forms as $form) {
                                    echo sprintf("%s,<br/>", $this->Html->link($form->name, ['controller' => 'Forms', 'action' => 'formDetail', $form->id], ['target' => '_blank']));
                                } ?>
                            </td>
                            <td>
                                <?php
                                if ($childProgram->evaluation_criterium) {
                                    echo $this->Html->link($childProgram->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $childProgram->evaluation_criteria_id]);
                                } else if ($program->evaluation_criterium) {
                                    echo $this->Html->link($program->evaluation_criterium->name, ['action' => 'criteriumAddModify', 'controller' => 'EvaluationCriteria', $program->evaluation_criterium->id]);
                                }
                                ?>
                            </td>
                            <td>
                                <?= $this->Html->link(__('Otevřít'), ['action' => 'programAddModify', $childProgram->id]) ?>
                                ,
                                <?= $this->Form->postLink(__('Smazat Podprogram'), ['action' => 'programDelete', $childProgram->id], ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat podprogram?')]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(function() {
        $("#requires-balance-sheet").change(function() {
            $("#requires-extended-budget").prop('disabled', !$(this).is(':checked'));
        }).change();
    });
</script>