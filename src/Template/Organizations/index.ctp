<?php

/**
 * @var $this AppView
 * @var $organizations Organization[]
 */

use App\Model\Entity\Organization;
use App\View\AppView;

$this->assign('title', __('Správa Organizací'));
echo $this->element('simple_datatable');
?>
<h1>
    <?= $this->fetch('title') ?>
</h1>
<?php if ($this->isSystemsManager()): ?>
    <?= $this->Html->link(__('Vytvořit novou organizaci'), ['action' => 'create'], ['class' => 'btn btn-success mb-2']) ?>
<?php endif; ?>
<table class="table" id="dtable">
    <thead>
        <tr>
            <th><?= __('ID') ?></th>
            <th><?= __('Jméno') ?></th>
            <th><?= __('Domény') ?></th>
            <th><?= __('Akce') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($organizations as $org): ?>
            <tr>
                <td><?= $org->id ?></td>
                <td><?= $this->Html->link(h($org->name), ['action' => 'edit', $org->id]) ?></td>
                <td>
                    <?php
                    foreach ($org->domains as $domain) {
                        if (strstr($domain, 'solr.cz')) continue;
                        echo (h($domain->domain));
                        echo $this->Html->link($domain->is_enabled ? sprintf(" (%s)", __('Zakázat')) : sprintf(" (%s)", __('Povolit')), ['action' => 'domainTrigger', $org->id, $domain->id], ['class' => $domain->is_enabled ? 'text-danger' : 'text-success']);
                        echo $this->Html->link(' (' . __('Smazat') . ')', ['action' => 'domainDelete', $org->id, $domain->id], ['class' => 'text-danger']);
                        echo '<br/>';
                    }
                    echo $this->Html->link('<i class="fas fa-plus-circle"></i> ' . __('Přidat doménové jméno'), ['action' => 'domainAdd', $org->id], ['class' => 'text-success', 'escapeTitle' => false]);
                    if ($org->test_id) {
                        echo '<br /><i class="fa fa-american-sign-language-interpreting text-info" aria-hidden="true"></i> Má přiřazeno testovací prostředí (<code>ID: ' . $org->test_id . ')</code>';
                    }
                    ?>
                </td>
                <td>
                    <?= $this->Html->link(__('Upravit'), ['action' => 'edit', $org->id]) ?>,
                    <?= $this->Html->link(__('Smazat'), ['action' => 'delete', $org->id], ['class' => 'text-danger']) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>