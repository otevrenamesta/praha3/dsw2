<?php

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $user User
 */

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

$this->assign('title', __('Organizace') . ' - ' . h($organization->name));

$sections = [
    [
        'name' => __('Základní nastavení Organizace'),
        'id' => 'basics',
        'settings' => OrganizationSetting::ALL_BASIC,
        'collapsed' => false,
    ],
    [
        'name' => __('Hlavní stránka portálu'),
        'id' => 'mainpage',
        'settings' => OrganizationSetting::ALL_INDEX_HERO,
        'collapsed' => false,
    ],
    [
        'name' => __('Nastavení dotačního procesu'),
        'id' => 'subsidy-process',
        'settings' => OrganizationSetting::ALL_SUBSIDY_PROCESS,
        'collapsed' => false,
    ],
    [
        'name' => __('Nastavení ekonomického oddělení'),
        'id' => 'economics-settings',
        'settings' => OrganizationSetting::ALL_ECONOMICS,
        'collapsed' => false,
    ],
    [
        'name' => __('Technické údaje'),
        'id' => 'technical',
        'settings' => OrganizationSetting::ALL_TECHNICAL,
        'collapsed' => true,
    ],
    [
        'name' => __('Nastavení E-mailů'),
        'id' => 'emails',
        'settings' => OrganizationSetting::ALL_EMAIL,
        'collapsed' => true,
    ],
];

echo $this->Form->create($organization);

echo '<nav class="navbar navbar-light bg-light sticky-top rounded-bottom">';
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success m-1 ml-auto p-1 pl-3 pr-3']);
echo '</nav>';
?>

<div class="card mt-2">
    <div class="card-header">
        <?php
        echo $this->Form->control('name', ['label' => __('Název Organizace')]);
        ?>
    </div>
</div>

<?php foreach ($sections as $_section) : ?>
    <div class="card mt-2 <?= $_section['border'] ?? '' ?>">
        <div class="card-header font-weight-bold">
            <a href="#<?= $_section['id'] ?>" class="text-dark " role="button" data-toggle="collapse" aria-controls="<?= $_section['id'] ?>" aria-expanded="false">
                <?= $_section['name'] ?></a>
        </div>
        <div class="card-body <?= ($_section['collapsed'] ?? false) ? 'collapse' : '' ?>" id="<?= $_section['id'] ?>">
            <?php
            foreach ($_section['settings'] as $field) {
                if (
                    $field === OrganizationSetting::ALLOW_ADDITIONAL_FORM_CHANGES &&
                    (!$user->email || strpos($user->email, '@otevrenamesta.cz') === false)
                ) {
                    // change the ALLOW_ADDITIONAL_FORM_CHANGES field is allowed for OM only
                    continue;
                }
                switch ($field) {
                    case 'ds.cert': {
            ?>
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Column 1 -->
                                    <div class="col-md-6 col-sm-12 pl-0">
                                        <?php renderOrgSettingField($this, $field, $organization); ?>
                                    </div>
                                    <!-- Column 2 -->
                                    <div class="col-md-6 col-sm-12 pl-0">
                                        <br />
                                        <div class="card">
                                            <div class="card-header">
                                                <?= __('Informace o certifikátu') ?>
                                            </div>
                                            <div class="card-body">
                                                <?php
                                                $cert = $organization->getSettingValue($field, true);
                                                $out =  OrganizationSetting::extractCertificateInfo($cert);
                                                ?>
                                                <?php if ($out['err_msg']) : ?>
                                                    <span class="text-danger"><i class="fas fa-times"></i> <?= __('Při čtení certifikátu došlo k chybě:') ?></span>
                                                    <p>
                                                        <?= $out['err_msg']; ?>
                                                    </p>
                                                <?php else : ?>
                                                    <ul>
                                                        <li><strong><?= __('Subjekt:') ?></strong> <?= $out['subject_cn'] ?></li>
                                                        <li><strong><?= __('Vydavatel:') ?></strong> <?= $out['issuer_cn'] ?></li>
                                                        <li><strong><?= __('Sériové číslo:') ?></strong> <?= $out['serial'] ?></li>
                                                        <li><strong><?= __('Platnost od:') ?></strong> <?= date("j.n. Y", $out['notBefore']) ?></li>
                                                        <?php if ($out['notAfter'] > (time() + 2.678e+6)) : ?>
                                                            <li><strong><?= __('Datum expirace:') ?></strong> <span class="text-success"><i class="fas fa-check"></i> <?= date("j. n. Y", $out['notAfter']) ?></span></li>
                                                        <?php endif ?>
                                                        <?php if ($out['notAfter'] > time() &&  ($out['notAfter'] < (time() + 2.678e+6))) : ?>
                                                            <li><strong><?= __('Datum expirace:') ?></strong> <span class="text-warning"><i class="fas fa-exclamation-triangle"></i> <?= date("j. n. Y", $out['notAfter']) ?></span></li>
                                                        <?php endif ?>
                                                        <?php if ($out['notAfter'] < time()) : ?>
                                                            <li><strong><?= __('Datum expirace:') ?></strong> <span class="text-danger"><i class="fas fa-times"></i> <?= date("j. n. Y", $out['notAfter']) ?></span></li>
                                                        <?php endif ?>

                                                    <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php break;
                        }
                    case 'ds.certkey': {

                        ?>
                            <p>&nbsp;</p>
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Column 1 -->
                                    <div class="col-md-6 col-sm-12 pl-0">
                                        <?php renderOrgSettingField($this, $field, $organization); ?>
                                    </div>
                                    <!-- Column 2 -->
                                    <div class="col-md-6 col-sm-12 pl-0">
                                        <br />
                                        <div class="card">
                                            <div class="card-header">
                                                <?= __('Test privátního klíče:') ?>
                                            </div>
                                            <div class="card-body">
                                                <?php
                                                $certificate = $organization->getSettingValue('ds.cert', true);
                                                $private_key = $organization->getSettingValue($field, true);
                                                ?>
                                                <?php if (OrganizationSetting::compareKeyVSCert($private_key, $certificate)) : ?>
                                                    <span class="text-success"><i class="fas fa-check"></i> <?= __('Privátní klíč se shoduje s certifikátem.') ?></span>
                                                <?php else : ?>
                                                    <span class="text-danger"><i class="fas fa-times"></i> <?= __('Privátní klíč se neshoduje.') ?></span>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid ml-0 pl-0">
                                <div class="row">
                                    <div class="col">
                                    <button id="isds_test" type="button" class="btn btn-light mt-3 mb-3 mr-3"><?= __('Otestovat spojení s odesílací bránou ...')?></button>
                                    <span id="isds_result"></span>
                                    </div>
                                </div>
                            </div>
            <?php
                            break;
                        }
                    default: {
                            renderOrgSettingField($this, $field, $organization);
                        }
                }
            }
            ?>
        </div>
    </div>
<?php endforeach;

echo '<nav class="navbar navbar-light bg-light sticky-top rounded-bottom">';
echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success m-1 ml-auto p-1 pl-3 pr-3']);
echo '</nav>';

echo $this->Form->end();

function renderOrgSettingField($me, $field, $organization)
{
    echo $me->Form->control('settings.' . $field, array_merge([
        'label' => OrganizationSetting::getSettingLabel($field),
        OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
        'type' => OrganizationSetting::getSettingDataType($field),
        'escape' => false,
    ], OrganizationSetting::getExtraFormControlOptions($field, $organization->id)));
}

?>

<script>
  $(document).ready(function(){
    // Listen for a click event on the button with ID 'isds_test'
    $('#isds_test').click(function() {
      // Make the AJAX call
      $.ajax({
        url: '/user/isds_test', // Endpoint to which the request is sent
        type: 'GET', // The HTTP method to use for the request (GET or POST)

        success: function(response) {
          // This function is called if the request succeeds
          // Check if the 'result' property exists in the response
          if(response.result !== undefined) {
            // Display the 'result' property in the 'isds_result' span
            if (response.result == 'OK') {
                $('#isds_result').html('<span class="text-success"><i class="fas fa-check"></i> Spojení je v pořádku </span>');
            } else {
                $('#isds_result').html('<span class="text-danger"><i class="fas fa-times"></i> Spojení se nezdařilo </span>');
            }
            
          } else {
            // If 'result' isn't a property, display an error message or default text
            $('#isds_result').text("No result found.");
          }
        },
        error: function(xhr, status, error) {
          // This function is called if the request fails
          // 'xhr' is the XMLHttpRequest object
          // 'status' is a string describing the type of error that occurred
          // 'error' is an optional exception object, if one occurred
          // Display an error message in the 'isds_result' span
          $('#isds_result').text("Error: " + error);
        }
      });
    });
  });
</script>