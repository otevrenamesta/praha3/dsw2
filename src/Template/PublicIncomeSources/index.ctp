<?php
/**
 * @var $this AppView
 * @var $sources PublicIncomeSource[]
 */

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\PublicIncomeSource;
use App\View\AppView;

$this->assign('title', __('Zdroje veřejné podpory'));

echo $this->element('simple_datatable');
?>
<div class="card m-2">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>
        <?= $this->Html->link(__('Přidat nový zdroj veřejné podpory'), ['action' => 'addModify'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="card-body">
        <table id="dtable" class="table table-striped">
            <thead>
            <tr>
                <th><?= __('Název zdroje') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($sources as $source): ?>
                <tr>
                    <td><?= $source->source_name ?></td>
                    <td>
                        <?php
                        if ($source->organization_id === OrgDomainsMiddleware::getCurrentOrganizationId()) {
                            echo $this->Html->link(__('Upravit'), ['action' => 'addModify', 'id' => $source->id]);
                            echo ', ';
                            echo $this->Html->link(__('Smazat'), ['action' => 'delete', 'id' => $source->id]);
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
