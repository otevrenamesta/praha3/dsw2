<?php
/**
 * @var $this AppView
 * @var $source PublicIncomeSource
 */

use App\Model\Entity\PublicIncomeSource;
use App\View\AppView;

$this->assign('title', $source->isNew() ? __('Vytvořit nový záznam') : __('Upravit stávající'));

echo $this->Form->create($source);
?>
<div class="card m-2">
    <div class="card-header">
        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="card-body">
        <?= $this->Form->control('source_name', ['label' => __('Název zdroje veřejné podpory')]); ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit() ?>
    </div>
</div>
<?= $this->Form->end(); ?>
