<?php
use App\Model\Entity\RequestBudgetChange;

/**
 * @var AppView $this
 * @var Request $request
 * @var ChangeRequest $change_request
 * @var string $orgName
 * @var string $title
 * @var string $text
 */
$this->assign('title', $title);
?>
<?= trim(str_replace("<br>", PHP_EOL, strip_tags($text, '<br>'))) ?>