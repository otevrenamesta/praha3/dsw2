<?php

/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

echo __d('email', 'Dobrý den') . ',<br />' . PHP_EOL . PHP_EOL;

echo sprintf(__d('email', 'Vaše žádost "%s" č. %d (výzva: %s) byla vyřazena pro nesplnění podmínek'), $request->name, $request->id, $request->appeal->name) . PHP_EOL . PHP_EOL;

if ($request->lock_comment) {
    echo __d('email', 'Odůvodnění:') . PHP_EOL;
    echo trim(str_replace("<br>", PHP_EOL, strip_tags($request->lock_comment, '<br />')));
}
