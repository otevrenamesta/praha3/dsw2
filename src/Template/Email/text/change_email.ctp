<?php

/**
 * @var AppView $this
 * @var int $user_id
 * @var string $code
 * @var string $orgName
 * @var string $new_email
 */

use App\View\AppView; ?>
<?= __d('email', 'Ověření e-mailové adresy') ?>

-----------------------------

<?php
$link = $this->Url->build([
  '_name' => 'new_email',
  'token' => $code,
  'requestId' => $user_id
], ['fullBase' => true]);
?>
<?= __d('email', 'Dobrý den') ?>,

<?= __d('email', 'pro aktivaci nové e-mailové adresy') ?> <?= h($new_email) ?>, <?= __d('email', 'otevřete následující odkaz') ?>

<?= $link ?>
