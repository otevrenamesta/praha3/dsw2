<?php
/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 * @var string $text
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

if (empty($text)):
    ?>
    <?= __d('email', 'Dobrý den') . ',<br />' ?>

    <?= sprintf(__d('email', 'Vaše žádost "%s" č. %d (výzva: %s) je nyní ve stavu: %s'), $request->name, $request->id, $request->appeal->name, $request->getCurrentStateLabel()) ?>
<?php else: ?>
    <?= trim(str_replace("<br>", PHP_EOL, strip_tags($text, '<br>'))) ?>
<?php endif; ?>
