<?php

/**
 * @var AppView $this
 * @var Settlement $settlement
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\View\AppView;

?>
<p><?= __d('email', 'Dobrý den') ?>,</p>
<p>
    <?= sprintf(__d('email', 'Vaše vyúčtování č. %d žádosti "%s" je nyní ve stavu: %s'), $settlement->id, $settlement->getRequestName() ?? '', SettlementState::getLabel($settlement->settlement_state_id)) ?>
</p>
<?php if ($settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES && !empty($settlement->econom_statement)) : ?>
    <p>
        <?= sprintf(__d('email', 'Vaše vyúčtování č. %d žádosti "%s" bylo vráceno k opravě s následujícím odůvodněním:'), $settlement->id, $settlement->getRequestName() ?? '') ?>
    </p>
    <p>
        <?= str_replace('<p><br></p>', '', $settlement->econom_statement) ?>
    </p>
<?php endif; ?>
