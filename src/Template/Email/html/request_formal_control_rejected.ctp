<?php

/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

?>
<p><?= __d('email', 'Dobrý den') ?>,</p>
<p>
    <?= sprintf(__d('email', 'Vaše žádost "%s" č. %d (výzva: %s) byla vyřazena pro nesplnění podmínek'), $request->name, $request->id, $request->appeal->name) ?>
</p>
<?php if ($request->lock_comment) : ?>
    <p>
        <strong><?= __d('email', 'Odůvodnění:') ?></strong>
    </p>
    <p>
        <?= str_replace('<p><br></p>', '', $request->lock_comment) ?>
    </p>
<?php endif; ?>
