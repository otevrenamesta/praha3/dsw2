<?php
use App\Model\Entity\RequestBudgetChange;

/**
 * @var AppView $this
 * @var Request $request
 * @var ChangeRequest $change_request
 * @var string $orgName
 * @var string $title
 * @var string $text
 */
$this->assign('title', $title);

if (empty($text)):
    ?>
    <?= __d('email', 'Dobrý den') ?>,
    <?php if ($change_request->status == RequestBudgetChange::CHANGE_ACCEPTED): ?>
        <?= sprintf(__d('email', 'Váš požadavek na změnu rozpočtu v žádosti %s z %s byl akceptován.'), $request->name, $change_request->created->i18nFormat()) ?>
    <?php else: ?>
        <?= sprintf(__d('email', 'Váš požadavek na změnu rozpočtu v žádosti %s z %s byl zamítnut.'), $request->name, $change_request->created->i18nFormat()) ?>
    <?php endif; ?>
<?php else: ?>
    <?= trim(str_replace("<br>", PHP_EOL, strip_tags($text, '<br>'))) ?>
<?php endif; ?>