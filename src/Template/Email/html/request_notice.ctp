<?php

/**
 * @var AppView $this
 * @var Request $request
 * @var string $orgName
 * @var string $title
 * @var string $text override text for this email template
 */
$this->assign('title', $title);

use App\Model\Entity\Request;
use App\View\AppView;

if (empty($text)) :
?>
    <p><?= __d('email', 'Dobrý den') ?>,</p>
    <p>
        <?= sprintf(__d('email', 'Vaše žádost "%s" č. %d (výzva: %s) je nyní ve stavu: %s'), $request->name, $request->id, $request->appeal->name, $request->getCurrentStateLabel()) ?>
    </p>
<?php else : ?>
    <?= $text ?>
<?php endif; ?>
