<?php
/**
 * @var $this AppView
 * @var $content string
 * @var $title ?string
 */

use App\View\AppView;

$this->assign('title', isset($title) ? $title ?? '' : '');

$content = explode("\n", $content);

foreach ($content as $line) :
    echo '<p> ' . $line . "</p>\n";
endforeach;
