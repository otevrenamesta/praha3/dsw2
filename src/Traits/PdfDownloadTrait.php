<?php

declare(strict_types=1);

namespace App\Traits;

use Cake\Core\Configure;

trait PdfDownloadTrait
{

    protected function renderAsPdf(string $filename, bool $forceDownload = true, string $orientation = 'landscape')
    {
        Configure::write(
            'CakePdf',
            [
                //'engine' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'App.WeasyPrint',
                'orientation' => $orientation,
                'margin' => [
                    'bottom' => 6,
                    'left' => 3,
                    'right' => 3,
                    'top' => 6,
                ],
            ]
        );
        if ($forceDownload) {
            $this->viewBuilder()->setOptions(
                [
                    'pdfConfig' => [
                        'download' => $forceDownload,
                        'filename' => $filename
                    ],
                ]
            );
        }
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
    }
}
