<?php

namespace App\Traits;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Exception;

trait EmailThrowableTrait
{
    /**
     * @inheritDoc
     */
    protected function _appSpecificSkipEmail($throwable)
    {
        if (empty($throwable)) {
            return true;
        }
        if (Configure::read('debug')) {
            return true;
        }

        return false;
    }

    /**
     * Check if we should rate limit the throwable. This is done by determining if we've
     * already seen the throwable in the last x minutes determined by the rateLimit config variable.
     * Throwables are determined to be unique by looking at throwable class + throwable message + throwable code
     *
     * @param  Exception $throwable Throwable instance.
     * @return bool true if email should be skipped due to throttling, false if it shouldn't
     */
    protected function _throttle($throwable)
    {
        switch (true) {
            // Check the config first to see if we should even try to throttle
            case empty(Configure::read('ErrorEmail.throttle')):
                // Break omitted intentionally
                // Check the throttle skip list to see if we should skip throttling
            case $this->_inSkipList('ErrorEmail.skipThrottle', $throwable):
                // Break omitted intentionally
                // If the throwable should skip throttling for any other reason
            case $this->_appSpecificSkipThrottle($throwable):
                return false;
            default:
                // This throwable shouldn't skip throttling if we made it this far so check the cache to see if we need to throttle.
                // The cache key is a composite of the throwable class, message, and code
                $cacheKey = preg_replace("/[^A-Za-z0-9]/", '', get_class($throwable) . $throwable->getFile() . $throwable->getLine() . $throwable->getCode());
                if (Cache::read($cacheKey, Configure::read('ErrorEmail.throttleCache')) !== false) {
                    return true;
                }
                // The throwable wasn't in the cache add it to the cache now
                Cache::add($cacheKey, true, Configure::read('ErrorEmail.throttleCache'));
                // Since it wasn't in the cache don't throttle it
                return false;
        }
    }
}
