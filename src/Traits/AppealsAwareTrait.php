<?php


namespace App\Traits;


use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Appeal;
use App\Model\Entity\Fond;

trait AppealsAwareTrait
{

    private array $_allowed_programs = [];
    private array $_program_descriptions = [];
    private array $_program_id_to_appeal_id = [];

    function hasSelectableItems($data): bool
    {
        $rtn = false;
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $rtn = true;
            }
            if (is_array($value)) {
                $rtn = $rtn || $this->hasSelectableItems($value);
            }
        }
        return $rtn;
    }

    public function getAppealsToPrograms(): array
    {
        $tree = [];

        $appeals_conditions = [
            'Appeals.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
        ];
        if (!$this->getCurrentUser()->isGrantsManager()) {
            $appeals_conditions['Appeals.is_active'] = true;
        }

        /** @var Appeal[] $appeals */
        $appeals = $this->Requests->Appeals->find('all', [
            'conditions' => $appeals_conditions,
            'contain' => [
                'Programs',
            ],
        ])->toArray();

        foreach ($appeals as $appeal) {
            $allowed_programs = [];
            foreach ($appeal->programs as $program) {
                $allowed_programs[] = $program->id;
                $this->_allowed_programs[] = $program->id;
                $this->_program_descriptions[$program->id] = $program->description ?? '-';
                $this->_program_id_to_appeal_id[$program->id] = $appeal->id;
            }

            /** @var Fond[] $fonds */
            $fonds = $this->Fonds->find('all', [
                'conditions' => [
                    'Fonds.is_enabled' => true,
                    'Fonds.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
                'contain' => [
                    'Realms.Programs.ChildPrograms',
                ],
            ])->toArray();

            foreach ($fonds as $fond) {
                $fond_data = [];
                foreach ($fond->realms as $realm) {
                    $realm_data = [];
                    foreach ($realm->programs as $program) {
                        if (!empty($program->child_programs)) {
                            $realm_data[$program->name] = [];
                            foreach ($program->child_programs as $child_program) {
                                if (in_array($child_program->id, $allowed_programs, true) || in_array($program->id, $allowed_programs, true)) {
                                    $realm_data[$program->name][$child_program->id] = $child_program->name;
                                }
                            }
                        } elseif (empty($program->parent_id)) {
                            if (in_array($program->id, $allowed_programs, true)) {
                                $realm_data[$program->id] = $program->name;
                            }
                        }
                    }
                    if (!empty($realm_data) && $this->hasSelectableItems($realm_data)) {
                        if ($realm->is_hidden) {
                            $fond_data = $fond_data + $realm_data;
                        } else {
                            $fond_data[$realm->name] = $realm_data;
                        }
                    }
                }
                if (!empty($fond_data) && $this->hasSelectableItems($fond_data)) {
                    if ($fond->is_hidden) {
                        $appeal_data = $fond_data;
                    } else {
                        $appeal_data[$fond->name] = $fond_data;
                    }
                }
            }

            if (!empty($appeal_data)) {
                $tree[$appeal->name . " [#$appeal->id]"] = $appeal_data;
            }
        }

        // strip appeal name if only one is open
        if (count($tree) === 1) {
            $tree = array_pop($tree);
        }

        // strip single fond name if only one is contained in appeal
        if (count($tree) === 1) {
            $tree = array_pop($tree);
        }

        // strip empty branches
        foreach ($tree as $key => $value) {
            foreach ($value as $k => $v) {
                if (is_array($v) && (count($v) <= 0)) {
                    unset($tree[$key][$k]);
                }
            }
        }

        return $tree;
    }
}
