<?php
declare(strict_types=1);

namespace App\Traits;

use App\Controller\AppController;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Form;
use App\Model\Entity\ReportColumn;
use App\Model\InstanceReportInterface;
use App\Model\Table\RequestsTable;
use Exception;

/**
 * @property-read RequestsTable $Requests
 */
trait ReportColumnsAwareTrait
{

    /**
     * @return array
     * @throws Exception
     */
    private function getReportColumnsDataOptions(): array
    {
        if (!($this instanceof AppController)) {
            throw new Exception('Invalid Trait usage, use in Controller classes only');
        }

        // better be safe here
        $this->loadModel('Requests');

        $columnDataOptions = [];
        foreach (ReportColumn::ENTITIES_WITH_REPORT_INTERFACE as $entityClass) {
            $class = ReportColumn::getEntityByClassName($entityClass);
            $columnDataOptions[ReportColumn::getEntityColumnsLabel($entityClass)] = ReportColumn::prefixColumnOptions($class::getReportColumns(), $entityClass);
        }
        $columnDataOptions['Formuláře'] = [];
        /** @var Form[] $organizationForms */
        $organizationForms = $this->Requests->RequestFilledFields->Forms->find('all', [
            'conditions' => [
                'Forms.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => [
                'FormFields'
            ]
        ]);

        foreach ($organizationForms as $organizationForm) {
            $formController = $organizationForm->getFormController();
            $displayName = sprintf("%d - %s", $organizationForm->id, $formController->getFormName());

            if ($formController instanceof InstanceReportInterface) {
                $columns = $formController->getReportColumns();
                $columnDataOptions['Formuláře'][$displayName] = ReportColumn::prefixColumnOptions($columns, Form::class);
            }

        }
        return $columnDataOptions;
    }

}