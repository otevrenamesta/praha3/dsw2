<?php
declare(strict_types=1);

namespace App\Traits;

trait StreetPartsAwareTrait {

    public static int $STREET_PART_NAME = 1,
        $STREET_PART_POPISNE = 2,
        $STREET_PART_ORIENTACNI = 3,
        $STREET_PART_PISMENO = 4;

    public static string $STREET_REGEX = '/^([^\d]+)(\d+)?\/?(\d+)?([^\d\W])?.*$/i';


    public static function getStreetPart(?string $street, int $part): string
    {
        if (empty($street)) {
            return "";
        }

        $parts = [];
        preg_match(self::$STREET_REGEX, $street, $parts);

        return $parts[$part] ?? "";
    }


}