<?php

namespace App\Traits;

use App\Model\Entity\CsuMunicipality;
use App\Model\Entity\CsuMunicipalityPart;
use App\Model\Table\CsuMunicipalitiesTable;
use App\Model\Table\CsuMunicipalityPartsTable;

trait CsuInfoTrait {

    public function getCityName(?int $municipalityNumber): ?string
    {
        if (empty($municipalityNumber)) {
            return null;
        }

        /** @var CsuMunicipalitiesTable $csuMunicipalities */
        $csuMunicipalities = $this->getTableLocator()->get('CsuMunicipalities');

        /** @var CsuMunicipality $municipalityByNumber */
        $municipalityByNumber = $csuMunicipalities->find('all', [
            'conditions' => [
                'number' => $municipalityNumber,
            ],
        ])->first();

        return $municipalityByNumber ? $municipalityByNumber->name : null;
    }

    public function getCityPartName(?int $municipalityPartNumber): ?string
    {
        if (empty($municipalityPartNumber)) {
            return null;
        }

        /** @var CsuMunicipalityPartsTable $csuMunicipalityParts */
        $csuMunicipalityParts = $this->getTableLocator()->get('CsuMunicipalityParts');

        /** @var CsuMunicipalityPart $municipalityPartByNumber */
        $municipalityPartByNumber = $csuMunicipalityParts->find('all', [
            'conditions' => [
                'number' => $municipalityPartNumber,
            ],
        ])->first();

        return $municipalityPartByNumber ? $municipalityPartByNumber->name : null;
    }

}