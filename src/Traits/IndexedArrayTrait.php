<?php

namespace App\Traits;

trait IndexedArrayTrait
{

    /**
     * Transforms array of objects to array indexed by objects ids (ORM id field) instead of automatically indexed by results order
     * @param array $array
     */
    public function _indexArrayByElementIds(array &$array)
    {
        $newarr = [];
        foreach ($array as $key => $val) {
            if (!empty($val)) {
                if (isset($val->id)) {
                    $newarr[$val->id] = $val;
                } elseif (isset($val['id'])) {
                    $newarr[$val['id']] = $val;
                }
            }
        }
        $array = $newarr;
    }

}