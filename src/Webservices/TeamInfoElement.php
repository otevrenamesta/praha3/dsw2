<?php
declare(strict_types=1);

namespace App\Webservices;


class TeamInfoElement
{
    /**
     * @var int
     */
    public $id;
    /**
     * Team name
     *
     * @var string
     */
    public $name;
    /**
     * Formal Check
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $formal_check_programs = [];
    /**
     * Managers (Finalizace)
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $request_manager_programs = [];
    /**
     * Comments (Hodnocení)
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $comments_programs = [];
    /**
     * Price Proposal (Navrhovatelé)
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $price_proposal_programs = [];
    /**
     * Price Approval (Schvalovatelé)
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $price_approval_programs = [];
    /**
     * Preview (Náhled)
     *
     * @var \App\Webservices\KeyValueElement[]
     */
    public $preview_programs = [];

}