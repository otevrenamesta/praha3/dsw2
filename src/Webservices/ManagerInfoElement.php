<?php
declare(strict_types=1);

namespace App\Webservices;


class ManagerInfoElement
{

    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $email;
    /**
     * @var \App\Webservices\KeyValueElement[]
     */
    public $teams = [];
    /**
     * @var \App\Webservices\KeyValueElement[]
     */
    public $roles = [];

    public function __construct(int $id, string $email, array $teams, array $roles)
    {
        $this->id = $id;
        $this->email = $email;
        foreach ($teams as $key => $value) {
            $this->teams[] = new KeyValueElement(strval($key), $value);
        }
        foreach ($roles as $key => $value) {
            $this->roles[] = new KeyValueElement(strval($key), $value);
        }
    }

}