<?php
declare(strict_types=1);

namespace App\Webservices;


use App\Controller\ApiController;
use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Team;
use App\Model\Entity\User;
use App\Model\Entity\UserRole;
use App\Model\Table\TeamsTable;
use Cake\Utility\Hash;

class PublicService
{

    private ApiController $controller;

    public function __construct(ApiController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Get list of current manager users of this portal, along with assigned teams and roles
     *
     * @return \App\Webservices\ManagerInfoElement[]
     */
    public function managers(): array
    {
        // TODO: This should reflect the change of storage for roles in programs, now when it's possible to have multiple teams having the same role in a program, see TeamsTable::beforeMarshal
        /** @var User[] $managers */
        $managers = $this->controller->Users->find('all', [
            'contain' => [
                'Teams' => TeamsTable::ALL_TEAMS,
                'UsersToRoles.UserRoles'
            ]
        ])->filter(function (User $user) {
            return !$user->isSystemsManager() && ($user->isManager() || $user->hasAtLeastOneTeam());
        });

        $overview = [];
        foreach ($managers as $manager) {
            $manager_teams = $manager->getAllTeamIds(true);
            $manager_roles = $manager->getAllRoleIds(true);
            if (empty($manager_teams) && count($manager_roles) === 1 && $manager_roles[0] === UserRole::USER_PORTAL_MEMBER) {
                // user who has no team and is only user of this portal is not a manager
                continue;
            }
            $overview[] = new ManagerInfoElement($manager->id, $manager->email, $manager_teams, $manager_roles);
        }

        return $overview;
    }

    /**
     * Get list of system static roles, that can be assigned to user
     *
     * @return \App\Webservices\KeyValueElement[]
     */
    public function roles(): array
    {
        return $this->controller->UserRoles->find('list')
            ->filter(function ($value, $key) {
                return !in_array(intval($key), [UserRole::MANAGER_SYSTEMS, UserRole::USER_PORTAL_MEMBER]);
            })
            ->map(
                function ($value, $key): KeyValueElement {
                    return new KeyValueElement(strval($key), $value);
                }
            )->toArray(false);
    }

    /**
     * Get current portal (organization) configured teams and their capabilities
     *
     * @return \App\Webservices\TeamInfoElement[]
     */
    public function teams(): array {
        /** @var Team[] $teams */
        $teams = $this->controller->Teams->find('all', [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => TeamsTable::ALL_TEAMS
        ]);

        $overview = [];
        foreach ($teams as $team) {
            $_team = new TeamInfoElement();
            $_team->name = $team->name;
            $_team->id = $team->id;
            foreach (Team::ALL_TEAMS as $team_field) {
                if (!empty($team->{$team_field})) {
                    $_team->{$team_field} = KeyValueElement::fromArray(Hash::combine($team->{$team_field}, '{n}.id', '{n}.name'));
                }
            }
            $overview[] = $_team;
        }

        return $overview;
    }

}