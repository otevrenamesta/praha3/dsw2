<?php


namespace App\Webservices;


class KeyValueElement
{

    /**
     * @var string
     */
    public $key;
    /**
     * @var string
     */
    public $value;

    public function __construct(string $key, string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * Convert associative array to KeyValueElement[]
     *
     * @param array $keyValues
     * @return KeyValueElement[]
     */
    public static function fromArray(array $keyValues): array
    {
        $rtn = [];
        foreach ($keyValues as $key => $value) {
            $rtn[] = new KeyValueElement(strval($key), strval($value));
        }
        return $rtn;
    }

}