<?php

namespace App\Model\Table;

use App\Model\Entity\RequestsToSettlement;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestsToSettlements Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property SettlementsTable&BelongsTo $Settlements
 *
 * @method RequestsToSettlement get($primaryKey, $options = [])
 * @method RequestsToSettlement newEntity($data = null, array $options = [])
 * @method RequestsToSettlement[] newEntities(array $data, array $options = [])
 * @method RequestsToSettlement|false save(EntityInterface $entity, $options = [])
 * @method RequestsToSettlement saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestsToSettlement patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestsToSettlement[] patchEntities($entities, array $data, array $options = [])
 * @method RequestsToSettlement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestsToSettlementsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requests_to_settlements');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Settlements', [
            'foreignKey' => 'settlement_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['settlement_id'], 'Settlements'));

        return $rules;
    }
}
