<?php

namespace App\Model\Table;

use App\Model\Entity\IdentityAttachmentType;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\Validation\Validator;

/**
 * IdentityAttachmentTypes Model
 *
 * @property IdentityAttachmentsTable&HasMany $IdentityAttachments
 *
 * @method IdentityAttachmentType get($primaryKey, $options = [])
 * @method IdentityAttachmentType newEntity($data = null, array $options = [])
 * @method IdentityAttachmentType[] newEntities(array $data, array $options = [])
 * @method IdentityAttachmentType|false save(EntityInterface $entity, $options = [])
 * @method IdentityAttachmentType saveOrFail(EntityInterface $entity, $options = [])
 * @method IdentityAttachmentType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method IdentityAttachmentType[] patchEntities($entities, array $data, array $options = [])
 * @method IdentityAttachmentType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class IdentityAttachmentTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('identity_attachment_types');
        $this->setDisplayField('type_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('IdentityAttachments', [
            'foreignKey' => 'identity_attachment_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }

    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !count($order)) {
            $query->order([$this->getAlias() . '.order' => 'ASC']);
        }
    }
}
