<?php

namespace App\Model\Table;

use App\Model\Entity\BudgetItem;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * BudgetItems Model
 *
 * @property RequestBudgetsTable&BelongsTo $RequestBudgets
 *
 * @method BudgetItem get($primaryKey, $options = [])
 * @method BudgetItem newEntity($data = null, array $options = [])
 * @method BudgetItem[] newEntities(array $data, array $options = [])
 * @method BudgetItem|false save(EntityInterface $entity, $options = [])
 * @method BudgetItem saveOrFail(EntityInterface $entity, $options = [])
 * @method BudgetItem patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method BudgetItem[] patchEntities($entities, array $data, array $options = [])
 * @method BudgetItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class BudgetItemsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('budget_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RequestBudgets', [
            'foreignKey' => 'request_budget_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('BudgetItemTypes');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('amount')
            ->notEmptyString('amount')
            ->greaterThan('amount', 0);

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_budget_id'], 'RequestBudgets'));
        $rules->add($rules->existsIn(['budget_item_type_id'], 'BudgetItemTypes'));

        return $rules;
    }
}
