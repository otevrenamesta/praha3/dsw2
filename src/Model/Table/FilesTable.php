<?php

namespace App\Model\Table;

use App\Model\Entity\File;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Files Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property AttachmentsTable&HasMany $Attachments
 * @property RequestNotificationsTable&BelongsToMany $RequestNotifications
 * @property PaymentsTable&BelongsToMany $Payments
 *
 * @method File get($primaryKey, $options = [])
 * @method File newEntity($data = null, array $options = [])
 * @method File[] newEntities(array $data, array $options = [])
 * @method File|false save(EntityInterface $entity, $options = [])
 * @method File saveOrFail(EntityInterface $entity, $options = [])
 * @method File patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method File[] patchEntities($entities, array $data, array $options = [])
 * @method File findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class FilesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('files');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Requests',
            [
                'through' => 'Attachments',
            ]
        );
        $this->belongsToMany(
            'RequestNotifications',
            [
                'through' => 'RequestNotificationsToFiles'
            ]
        );
        $this->belongsToMany(
            'Payments',
            [
                'through' => 'PaymentsToFiles'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('filepath')
            ->maxLength('filepath', 255)
            ->requirePresence('filepath', 'create')
            ->notEmptyFile('filepath');

        $validator
            ->integer('filesize')
            ->requirePresence('filesize', 'create')
            ->greaterThan('filesize', 0)
            ->lessThan('filesize', getMaximumFileUploadSize())
            ->notEmptyFile('filesize');

        $validator
            ->scalar('original_filename')
            ->maxLength('original_filename', 255)
            ->requirePresence('original_filename', 'create')
            ->notEmptyFile('original_filename');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
