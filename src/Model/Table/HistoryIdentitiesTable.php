<?php

namespace App\Model\Table;

use App\Model\Entity\HistoryIdentity;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * HistoryIdentities Model
 *
 * @property HistoriesTable&HasMany $Histories
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method HistoryIdentity get($primaryKey, $options = [])
 * @method HistoryIdentity newEntity($data = null, array $options = [])
 * @method HistoryIdentity[] newEntities(array $data, array $options = [])
 * @method HistoryIdentity|false save(EntityInterface $entity, $options = [])
 * @method HistoryIdentity saveOrFail(EntityInterface $entity, $options = [])
 * @method HistoryIdentity patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method HistoryIdentity[] patchEntities($entities, array $data, array $options = [])
 * @method HistoryIdentity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class HistoryIdentitiesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('history_identities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Histories', [
            'foreignKey' => 'history_identity_id',
        ]);
        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255);

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->allowEmptyString('first_name', null, function ($context) {
                return !isset($context['data']['date_of_birth']) || empty($context['data']['date_of_birth']);
            });

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->allowEmptyString('last_name', null, function ($context) {
                return !isset($context['data']['date_of_birth']) || empty($context['data']['date_of_birth']);
            });

        $validator
            ->date('date_of_birth')
            ->allowEmptyDate('date_of_birth');

        $validator
            ->scalar('databox_id')
            ->maxLength('databox_id', 10, __('Max length is 10'))
            ->allowEmptyString('databox_id');

        $validator
            ->scalar('ico')
            ->maxLength('ico', 9)
            ->allowEmptyString('ico');

        $validator
            ->scalar('dic')
            ->maxLength('dic', 13)
            ->allowEmptyString('dic');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['ico', 'organization_id']));
        $rules->add($rules->isUnique(['dic', 'organization_id']));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (empty($entity->get('name'))) {
            $entity->set('name', sprintf("%s %s", $entity->get('first_name'), $entity->get('last_name')));
        }
        if (!empty($entity->get('ico')) && !empty($entity->get('dic'))) {
            $entity->unsetProperty('first_name');
            $entity->unsetProperty('last_name');
            $entity->unsetProperty('date_of_birth');
        }
        $empty_to_null = ['first_name', 'last_name', 'ico', 'dic', 'databox_id'];
        foreach ($empty_to_null as $field) {
            if ($entity->has($field) && empty($entity->get($field))) {
                $entity->set($field, null);
            }
        }
    }
}
