<?php

namespace App\Model\Table;

use App\Model\Entity\TeamsMessage;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * TeamsMessages Model
 *
 * @property UsersTable&BelongsTo $Users
 *
 * @method TeamsMessage get($primaryKey, $options = [])
 * @method TeamsMessage newEntity($data = null, array $options = [])
 * @method TeamsMessage[] newEntities(array $data, array $options = [])
 * @method TeamsMessage|false save(EntityInterface $entity, $options = [])
 * @method TeamsMessage saveOrFail(EntityInterface $entity, $options = [])
 * @method TeamsMessage patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method TeamsMessage[] patchEntities($entities, array $data, array $options = [])
 * @method TeamsMessage findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TeamsMessagesTable extends AppTable
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('teams_messages');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

    $this->belongsTo('Users', [
      'foreignKey' => 'user_id',
      'joinType' => 'INNER',
    ]);

    $this->belongsTo('Users', [
      'foreignKey' => 'recipient_user_id',
      'joinType' => 'INNER',
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param Validator $validator Validator instance.
   * @return Validator
   */
  public function validationDefault(Validator $validator)
  {
    $validator
      ->integer('id')
      ->allowEmptyString('id', null, 'create');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param RulesChecker $rules The rules object to be modified.
   * @return RulesChecker
   */
  public function buildRules(RulesChecker $rules)
  {
    $rules->add($rules->existsIn(['user_id'], 'Users'));
    $rules->add($rules->existsIn(['recipient_user_id'], 'Users'));

    return $rules;
  }
}
