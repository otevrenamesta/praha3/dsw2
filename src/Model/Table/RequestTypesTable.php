<?php

namespace App\Model\Table;

use App\Model\Entity\RequestType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * RequestTypes Model
 *
 * @property RequestsTable&HasMany $Requests
 *
 * @method RequestType get($primaryKey, $options = [])
 * @method RequestType newEntity($data = null, array $options = [])
 * @method RequestType[] newEntities(array $data, array $options = [])
 * @method RequestType|false save(EntityInterface $entity, $options = [])
 * @method RequestType saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestType[] patchEntities($entities, array $data, array $options = [])
 * @method RequestType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Requests', [
            'foreignKey' => 'request_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }
}
