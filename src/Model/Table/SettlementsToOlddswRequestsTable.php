<?php

namespace App\Model\Table;

use App\Model\Entity\SettlementsToOlddswRequest;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use OldDsw\Model\Table\ZadostiTable;

/**
 * SettlementsToOlddswRequests Model
 *
 * @property SettlementsTable&BelongsTo $Settlements
 * @property ZadostiTable&BelongsTo $Zadosti
 *
 * @method SettlementsToOlddswRequest get($primaryKey, $options = [])
 * @method SettlementsToOlddswRequest newEntity($data = null, array $options = [])
 * @method SettlementsToOlddswRequest[] newEntities(array $data, array $options = [])
 * @method SettlementsToOlddswRequest|false save(EntityInterface $entity, $options = [])
 * @method SettlementsToOlddswRequest saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementsToOlddswRequest patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementsToOlddswRequest[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementsToOlddswRequest findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementsToOlddswRequestsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlements_to_olddsw_requests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Settlements', [
            'foreignKey' => 'settlement_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Zadosti', [
            'foreignKey' => 'zadost_id',
            'className' => 'OldDsw.Zadosti',
            'strategy' => 'select',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['settlement_id'], 'Settlements'));
        $rules->add($rules->existsIn(['zadost_id'], 'Zadosti'));

        return $rules;
    }
}
