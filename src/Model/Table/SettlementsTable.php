<?php

namespace App\Model\Table;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementType;
use App\Model\Entity\User;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Settlements Model
 *
 * @property SettlementStatesTable&BelongsTo $SettlementStates
 * @property RequestsToSettlementsTable&HasMany $RequestsToSettlements
 * @property SettlementAttachmentsTable&HasMany $SettlementAttachments
 * @property SettlementItemsTable&HasMany $SettlementItems
 * @property SettlementsToOlddswRequestsTable&HasMany $SettlementsToOlddswRequests
 * @property SettlementTypesTable&BelongsTo $SettlementTypes
 * @property SettlementLogsTable&HasMany $SettlementLogs
 * @property PaymentsTable&HasMany $Payments
 *
 * @method Settlement get($primaryKey, $options = [])
 * @method Settlement newEntity($data = null, array $options = [])
 * @method Settlement[] newEntities(array $data, array $options = [])
 * @method Settlement|false save(EntityInterface $entity, $options = [])
 * @method Settlement saveOrFail(EntityInterface $entity, $options = [])
 * @method Settlement patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Settlement[] patchEntities($entities, array $data, array $options = [])
 * @method Settlement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementsTable extends AppTable
{
    public const ALL_CONTAINS = [
        'RequestsToSettlements.Requests',
        'SettlementsToOlddswRequests',
        'SettlementItems',
        'SettlementAttachments',
        'SettlementAttachments.Files',
        'SettlementAttachments.SettlementAttachmentTypes',
        'SettlementAttachments.SettlementItems',
        'SettlementStates',
        'Payments',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlements');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SettlementStates', [
            'foreignKey' => 'settlement_state_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('RequestsToSettlements', [
            'foreignKey' => 'settlement_id',
        ]);
        $this->hasMany('SettlementAttachments', [
            'foreignKey' => 'settlement_id',
        ]);
        $this->hasMany('SettlementItems', [
            'foreignKey' => 'settlement_id',
        ]);
        $this->hasMany('SettlementsToOlddswRequests', [
            'foreignKey' => 'settlement_id',
        ]);
        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id'
        ]);
        $this->belongsTo('SettlementTypes', [
            'foreignKey' => 'settlement_type_id'
        ]);
        $this->hasMany('SettlementLogs', [
            'foreignKey' => 'settlement_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'settlement_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('organization_id')
            ->requirePresence('organization_id', 'create');

        $validator
            ->allowEmptyString('._no', null, 'create')
            ->scalar('agreement_no')
            ->maxLength('agreement_no', 50);

        $validator
            ->date('repair_date')
            ->allowEmptyDate('REPAIR_DATE');

        $validator
            ->scalar('return_reason')
            ->maxLength('return_reason', 65535)
            ->minLength('return_reason', 5)
            ->allowEmptyString('return_reason', null, function ($context) {
                return !isset($context['data']['settlement_type_id'])
                    || intval($context['data']['settlement_type_id']) !== SettlementType::FULL_SUBSIDY_RETURN;
            });

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['settlement_state_id'], 'SettlementStates'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['settlement_type_id'], 'SettlementTypes'));

        return $rules;
    }

    public function getWithPermissionCheck(int $settlement_id, User $userAccount, bool $checkOldDsw)
    {
        $settlement = $this->get($settlement_id, [
            'conditions' => [
                'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => self::ALL_CONTAINS
        ]);
        foreach ($settlement->requests_to_settlements as $newLink) {
            if ($newLink->request->user_id === $userAccount->id) {
                return $settlement;
            }
        }
        if ($checkOldDsw) {
            $stareUctyIds = $userAccount->getStareUctyIds();
            foreach ($settlement->settlements_to_olddsw_requests as $oldLink) {
                $oldLink->zadosti = $this->SettlementsToOlddswRequests->Zadosti->get($oldLink->zadost_id);
                if (in_array($oldLink->zadosti->ucet_id, $stareUctyIds, true)) {
                    $oldLink->zadosti->ucet = $this->SettlementsToOlddswRequests->Zadosti->Ucet->get($oldLink->zadosti->ucet_id);
                    return $settlement;
                }
            }
        }
        throw new NotFoundException();
    }

    public function findAllMatchingOldDsw(): Query
    {
        return $this->find('all', [
            'conditions' => [
                'Settlements.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
            ],
            'contain' => [
                'SettlementsToOlddswRequests',
            ],
        ])->matching('SettlementsToOlddswRequests');
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof Settlement) {
            if (!empty($entity->return_reason)) {
                $entity->return_reason = strip_tags(trim($entity->return_reason));
            }

            if (!empty($entity->rejected_statement)) {
                $entity->rejected_statement = strip_tags(trim($entity->rejected_statement));
            }

            if (empty($entity->return_reason) && $entity->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN) {
                $entity->setError('return_reason', __('Toto pole je při vrácení podpory v plné výši povinné'));
                $event->stopPropagation();
            }

            return;
        }
    }
}
