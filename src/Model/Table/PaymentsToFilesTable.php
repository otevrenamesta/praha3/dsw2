<?php

namespace App\Model\Table;

use App\Model\Entity\PaymentsToFile;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * PaymentsToFiles Model
 *
 * @property PaymentsTable&BelongsTo $Payments
 * @property FilesTable&BelongsTo $Files
 *
 * @method PaymentsToFile get($primaryKey, $options = [])
 * @method PaymentsToFile newEntity($data = null, array $options = [])
 * @method PaymentsToFile[] newEntities(array $data, array $options = [])
 * @method PaymentsToFile|false save(EntityInterface $entity, $options = [])
 * @method PaymentsToFile saveOrFail(EntityInterface $entity, $options = [])
 * @method PaymentsToFile patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PaymentsToFile[] patchEntities($entities, array $data, array $options = [])
 * @method PaymentsToFile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PaymentsToFilesTable extends AppTable
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('payments_to_files');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');

    $this->addBehavior('Timestamp');

    $this->belongsTo('Payments', [
      'foreignKey' => 'payment_id',
      'joinType' => 'INNER',
    ]);
    $this->belongsTo('Files', [
      'foreignKey' => 'file_id',
      'joinType' => 'INNER',
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param Validator $validator Validator instance.
   * @return Validator
   */
  public function validationDefault(Validator $validator)
  {
    $validator
      ->integer('id')
      ->allowEmptyString('id', null, 'create');

    return $validator;
  }

  /**
   * Returns a rules checker object that will be used for validating
   * application integrity.
   *
   * @param RulesChecker $rules The rules object to be modified.
   * @return RulesChecker
   */
  public function buildRules(RulesChecker $rules)
  {
    $rules->add($rules->existsIn(['payment_id'], 'Payments'));
    $rules->add($rules->existsIn(['file_id'], 'Files'));

    return $rules;
  }
}
