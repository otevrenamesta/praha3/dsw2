<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\PublicIncomeSource;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * PublicIncomeSources Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property PublicIncomeHistoriesTable&HasMany $PublicIncomeHistories
 *
 * @method PublicIncomeSource get($primaryKey, $options = [])
 * @method PublicIncomeSource newEntity($data = null, array $options = [])
 * @method PublicIncomeSource[] newEntities(array $data, array $options = [])
 * @method PublicIncomeSource|false save(EntityInterface $entity, $options = [])
 * @method PublicIncomeSource saveOrFail(EntityInterface $entity, $options = [])
 * @method PublicIncomeSource patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PublicIncomeSource[] patchEntities($entities, array $data, array $options = [])
 * @method PublicIncomeSource findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PublicIncomeSourcesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('public_income_sources');
        $this->setDisplayField('source_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('PublicIncomeHistories', [
            'foreignKey' => 'public_income_source_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('source_name')
            ->maxLength('source_name', 255)
            ->requirePresence('source_name', 'create')
            ->notEmptyString('source_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $this->addDeleteLinkConstraintRule($rules, 'PublicIncomeHistories', 'source_name', __('Tento zdroj veřejné podpory je již použit žadatelem, nelze jej smazat'));

        return $rules;
    }

    public function fkCheck(AppEntity $entity, array $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $hasAssociations = $this->PublicIncomeHistories->find('all', [
            'conditions' => [
                'PublicIncomeHistories.public_income_source_id' => $entity->id,
            ]
        ])->count();

        if ($hasAssociations > 0) {
            return __('Tento zdroj veřejné podpory je již použit žadatelem, nelze jej smazat');
        }

        return true;
    }
}
