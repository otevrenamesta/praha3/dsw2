<?php

namespace App\Model\Table;

use App\Model\Entity\RequestNotification;
use ArrayObject;
use Cake\Chronos\ChronosInterface;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestNotifications Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property RequestNotificationTypesTable&BelongsTo $RequestNotificationTypes
 * @property RequestNotificationsToFilesTable&HasMany $RequestNotificationsToFiles
 * @property FilesTable&BelongsToMany $Files
 *
 * @method RequestNotification get($primaryKey, $options = [])
 * @method RequestNotification newEntity($data = null, array $options = [])
 * @method RequestNotification[] newEntities(array $data, array $options = [])
 * @method RequestNotification|false save(EntityInterface $entity, $options = [])
 * @method RequestNotification saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestNotification patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestNotification[] patchEntities($entities, array $data, array $options = [])
 * @method RequestNotification findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestNotificationsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_notifications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RequestNotificationTypes', [
            'foreignKey' => 'request_notification_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('RequestNotificationsToFiles', [
            'foreignKey' => 'request_notification_id',
        ]);
        $this->belongsToMany('Files', [
            'through' => 'RequestNotificationsToFiles',
            'className' => 'Files',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('date_from')
            ->allowEmptyDate('date_from');

        $validator
            ->date('date_end')
            ->requirePresence('date_end', 'create')
            ->notEmptyDate('date_end');

        $validator
            ->integer('costs_czk')
            ->notEmptyString('costs_czk')
            ->greaterThanOrEqual('costs_czk', 0);

        $validator
            ->integer('participants_count')
            ->notEmptyString('participants_count');

        $validator
            ->scalar('location')
            ->requirePresence('location', 'create')
            ->notEmptyString('location');

        $validator
            ->scalar('garant_name')
            ->maxLength('garant_name', 255)
            ->requirePresence('garant_name', 'create')
            ->notEmptyString('garant_name');

        $validator
            ->scalar('garant_phone')
            ->maxLength('garant_phone', 50)
            ->minLength('garant_phone', 9, __('Telefonní číslo není ve správném formátu'))
            ->requirePresence('garant_phone', 'create')
            ->notEmptyString('garant_phone', __('Telefonní číslo není ve správném formátu'));

        $validator
            ->scalar('garant_email')
            ->email('garant_email')
            ->maxLength('garant_email', 100)
            ->requirePresence('garant_email', 'create')
            ->notEmptyString('garant_email');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['request_notification_type_id'], 'RequestNotificationTypes'));
        $rules->add(function (EntityInterface $entity) {
            $start = $entity->get('date_from');
            $end = $entity->get('date_end');
            if ($start instanceof ChronosInterface and $end instanceof ChronosInterface) {
                return $end->greaterThanOrEquals($start);
            }
            return true;
        }, 'startBeforeEnd');
        $rules->add(function (EntityInterface $entity) {
            $data = $entity->extract($this->getSchema()->columns(), true);
            $validator = $this->getValidator();
            $errors = $validator->validate($data, $entity->isNew());
            $valid = empty($errors);
            if (!$valid) {
                $entity->setErrors($errors);
            }
            return $valid;
        }, 'noErrorsBeforeSaving');

        return $rules;
    }

    public function beforeRules(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof RequestNotification) {
            $entity->garant_phone = strip_tags(trim($entity->garant_phone));
            // remove non-numerical and replace multi-whitespace with single-whitespace
            $entity->garant_phone = preg_replace('/[\s]+/', ' ', preg_replace('/[^0-9+ ]/', '', $entity->garant_phone ?? ''));
            $entity->garant_email = strip_tags(trim($entity->garant_email));
            $entity->garant_name = strip_tags(trim($entity->garant_name));

            $entity->location = strip_tags(trim($entity->location));
            $entity->comment = strip_tags(trim($entity->comment));
        }
        return $entity;
    }
}
