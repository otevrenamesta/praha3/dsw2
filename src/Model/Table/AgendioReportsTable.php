<?php

namespace App\Model\Table;

use App\Model\Entity\AgendioReport;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * AgendioReports Model
 *
 * @property AgendioColumnsTable&HasMany $AgendioColumns
 *
 * @method AgendioReport get($primaryKey, $options = [])
 * @method AgendioReport newEntity($data = null, array $options = [])
 * @method AgendioReport[] newEntities(array $data, array $options = [])
 * @method AgendioReport|false save(EntityInterface $entity, $options = [])
 * @method AgendioReport saveOrFail(EntityInterface $entity, $options = [])
 * @method AgendioReport patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method AgendioReport[] patchEntities($entities, array $data, array $options = [])
 * @method AgendioReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AgendioReportsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agendio_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('AgendioColumns', [
            'foreignKey' => 'agendio_report_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->requirePresence('label', 'create')
            ->notEmptyString('label');

        return $validator;
    }
}
