<?php

namespace App\Model\Table;

use App\Model\Entity\SettlementLog;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * SettlementLogs Model
 *
 * @property SettlementsTable&BelongsTo $Settlements
 * @property SettlementStatesTable&BelongsTo $SettlementStates
 * @property UsersTable&BelongsTo $Users
 *
 * @method SettlementLog get($primaryKey, $options = [])
 * @method SettlementLog newEntity($data = null, array $options = [])
 * @method SettlementLog[] newEntities(array $data, array $options = [])
 * @method SettlementLog|false save(EntityInterface $entity, $options = [])
 * @method SettlementLog saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementLog patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementLog[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementLogsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlement_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Settlements', [
            'foreignKey' => 'settlement_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SettlementStates', [
            'foreignKey' => 'settlement_state_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'executed_by_user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('return_reason')
            ->allowEmptyString('return_reason');

        $validator
            ->scalar('economics_statement')
            ->allowEmptyString('economics_statement');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['settlement_id'], 'Settlements'));
        $rules->add($rules->existsIn(['settlement_state_id'], 'SettlementStates'));
        $rules->add($rules->existsIn(['executed_by_user_id'], 'Users'));

        return $rules;
    }
}
