<?php

namespace App\Model\Table;

use App\Model\Entity\BudgetItemType;
use App\Model\Entity\RequestBudget;
use App\Model\Entity\RequestFilledFieldChange;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestBudgets Model
 *
 * @property RequestsTable&BelongsTo $Requests
 *
 * @method RequestBudget get($primaryKey, $options = [])
 * @method RequestBudget newEntity($data = null, array $options = [])
 * @method RequestBudget[] newEntities(array $data, array $options = [])
 * @method RequestBudget|false save(EntityInterface $entity, $options = [])
 * @method RequestBudget saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestBudget patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestBudget[] patchEntities($entities, array $data, array $options = [])
 * @method RequestBudget findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestChangesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_changes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'RequestFilledFieldChanges',
            [
            'foreignKey' => 'change_id',
            'joinType' => 'INNER',
            ]
        );

        $this->belongsTo(
            'Requests',
            [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            ]
        );
        
        $this->belongsTo(
            'Forms',
            [
            'foreignKey' => 'form_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['form_id'], 'Forms'));

        return $rules;
    }
}
