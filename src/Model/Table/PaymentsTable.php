<?php

namespace App\Model\Table;

use App\Model\Entity\Payment;
use App\Model\Entity\Request;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property SettlementsTable&BelongsTo $Settlements
 * @property PaymentsToFilesTable&HasMany $PaymentsToFiles
 * @property FilesTable&BelongsToMany $Files
 *
 * @method Payment get($primaryKey, $options = [])
 * @method Payment newEntity($data = null, array $options = [])
 * @method Payment[] newEntities(array $data, array $options = [])
 * @method Payment|false save(EntityInterface $entity, $options = [])
 * @method Payment saveOrFail(EntityInterface $entity, $options = [])
 * @method Payment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Payment[] patchEntities($entities, array $data, array $options = [])
 * @method Payment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PaymentsTable extends AppTable
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('CounterCache', [
            'Requests' => [
                'subsidy_paid' => function (Event $event, Payment $entity, Table $table, $original) {
                    if (intval($entity->request_id) < 1) {
                        return 0;
                    }
                    $requestsTable = $this->getTableLocator()->get('Requests');
                    /** @var Request $request */
                    $request = $requestsTable->get($entity->request_id, [
                        'contain' => [
                            'Payments'
                        ]
                    ]);
                    return $request->getPaymentsSum();
                }
            ]
        ]);

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Settlements', [
            'foreignKey' => 'settlement_id',
        ]);
        $this->hasMany('PaymentsToFiles', [
            'foreignKey' => 'payment_id',
        ]);
        $this->belongsToMany('Files', [
            'through' => 'PaymentsToFiles',
            'className' => 'Files',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('amount_czk')
            ->requirePresence('amount_czk', 'create')
            ->notEmptyString('amount_czk')
            ->greaterThan('amount_czk', 0, __('Platba nemůže být záporná'));

        $validator
            ->boolean('is_refund')
            ->notEmptyString('is_refund');
/*
        $validator
            ->boolean('in_addition')
            ->allowEmptyString('in_addition');
*/
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['settlement_id'], 'Settlements'));

        return $rules;
    }
}
