<?php
namespace App\Model\Table;

use App\Model\Entity\ProjectBudgetDesign;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * ProjectBudgetDesigns Model
 *
 * @property ProgramsTable&HasMany $Programs
 *
 * @method ProjectBudgetDesign get($primaryKey, $options = [])
 * @method ProjectBudgetDesign newEntity($data = null, array $options = [])
 * @method ProjectBudgetDesign[] newEntities(array $data, array $options = [])
 * @method ProjectBudgetDesign|false save(EntityInterface $entity, $options = [])
 * @method ProjectBudgetDesign saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectBudgetDesign patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectBudgetDesign[] patchEntities($entities, array $data, array $options = [])
 * @method ProjectBudgetDesign findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectBudgetDesignsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('project_budget_designs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Programs', [
            'foreignKey' => 'project_budget_design_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
