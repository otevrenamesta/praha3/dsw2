<?php
namespace App\Model\Table;

use App\Model\Entity\CsuLegalForm;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * CsuLegalForms Model
 *
 * @method CsuLegalForm get($primaryKey, $options = [])
 * @method CsuLegalForm newEntity($data = null, array $options = [])
 * @method CsuLegalForm[] newEntities(array $data, array $options = [])
 * @method CsuLegalForm|false save(EntityInterface $entity, $options = [])
 * @method CsuLegalForm saveOrFail(EntityInterface $entity, $options = [])
 * @method CsuLegalForm patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CsuLegalForm[] patchEntities($entities, array $data, array $options = [])
 * @method CsuLegalForm findOrCreate($search, callable $callback = null, $options = [])
 */
class CsuLegalFormsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('csu_legal_forms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('number')
            ->maxLength('number', 38)
            ->requirePresence('number', 'create')
            ->notEmptyString('number')
            ->add('number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 38)
            ->requirePresence('short_name', 'create')
            ->notEmptyString('short_name');

        $validator
            ->scalar('name')
            ->maxLength('name', 110)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['number']));

        return $rules;
    }
}
