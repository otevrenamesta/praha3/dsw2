<?php

namespace App\Model\Table;

use App\Model\Entity\History;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Histories Model
 *
 * @property HistoryIdentitiesTable&BelongsTo $HistoryIdentities
 *
 * @method History get($primaryKey, $options = [])
 * @method History newEntity($data = null, array $options = [])
 * @method History[] newEntities(array $data, array $options = [])
 * @method History|false save(EntityInterface $entity, $options = [])
 * @method History saveOrFail(EntityInterface $entity, $options = [])
 * @method History patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method History[] patchEntities($entities, array $data, array $options = [])
 * @method History findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class HistoriesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('HistoryIdentities', [
            'foreignKey' => 'history_identity_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('czk_amount')
            ->requirePresence('czk_amount', 'create')
            ->notEmptyString('czk_amount');

        $validator
            ->scalar('project_name')
            ->maxLength('project_name', 255)
            ->requirePresence('project_name', 'create')
            ->notEmptyString('project_name');

        $validator
            ->scalar('notes')
            ->allowEmptyString('notes');

        $validator
            ->scalar('year')
            ->notEmptyString('year')
            ->requirePresence('year', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['history_identity_id'], 'HistoryIdentities'));

        return $rules;
    }
}
