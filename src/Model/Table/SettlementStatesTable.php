<?php
namespace App\Model\Table;

use App\Model\Entity\SettlementState;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * SettlementStates Model
 *
 * @property SettlementsTable&HasMany $Settlements
 *
 * @method SettlementState get($primaryKey, $options = [])
 * @method SettlementState newEntity($data = null, array $options = [])
 * @method SettlementState[] newEntities(array $data, array $options = [])
 * @method SettlementState|false save(EntityInterface $entity, $options = [])
 * @method SettlementState saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementState patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementState[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementState findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementStatesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlement_states');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Settlements', [
            'foreignKey' => 'settlement_state_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
