<?php

namespace App\Model\Table;

use App\Model\Entity\SettlementItem;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * SettlementItems Model
 *
 * @property SettlementsTable&BelongsTo $Settlements
 * @property SettlementAttachmentsTable&HasMany $SettlementAttachments
 * @property SettlementItemsToSettlementAttachmentsTable&HasMany $SettlementItemsToSettlementAttachments
 *
 * @method SettlementItem get($primaryKey, $options = [])
 * @method SettlementItem newEntity($data = null, array $options = [])
 * @method SettlementItem[] newEntities(array $data, array $options = [])
 * @method SettlementItem|false save(EntityInterface $entity, $options = [])
 * @method SettlementItem saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementItem patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementItem[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementItemsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlement_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Settlements', [
            'foreignKey' => 'settlement_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('SettlementAttachments', [
            'through' => 'SettlementItemsToSettlementAttachments'
        ]);
        $this->hasMany('SettlementItemsToSettlementAttachments', [
            'foreignKey' => 'settlement_item_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->integer('order_number')
            ->allowEmptyString('order_number');

        $validator
            ->decimal('amount')
            ->greaterThanOrEqual('amount', 0)
            ->lessThanOrEqualToField('amount', 'original_amount', __('Částka čerpaná z dotace musí být vždy rovna, nebo nižší, než částka dle dokladu'))
            ->requirePresence('amount', 'create');

        $validator
            ->decimal('original_amount')
            ->greaterThan('original_amount', 0)
            ->requirePresence('original_amount', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['settlement_id'], 'Settlements'));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof SettlementItem) {
            if (!empty($entity->note)) {
                $entity->note = strip_tags(trim($entity->note));
            }

            if (!empty($entity->description)) {
                $entity->description = strip_tags(trim($entity->description));
            }

            return;
        }
    }
}
