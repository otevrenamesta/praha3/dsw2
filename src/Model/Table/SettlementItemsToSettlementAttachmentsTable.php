<?php
namespace App\Model\Table;

use App\Model\Entity\SettlementItemsToSettlementAttachment;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * SettlementItemsToSettlementAttachments Model
 *
 * @property SettlementItemsTable&BelongsTo $SettlementItems
 * @property SettlementAttachmentsTable&BelongsTo $SettlementAttachments
 *
 * @method SettlementItemsToSettlementAttachment get($primaryKey, $options = [])
 * @method SettlementItemsToSettlementAttachment newEntity($data = null, array $options = [])
 * @method SettlementItemsToSettlementAttachment[] newEntities(array $data, array $options = [])
 * @method SettlementItemsToSettlementAttachment|false save(EntityInterface $entity, $options = [])
 * @method SettlementItemsToSettlementAttachment saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementItemsToSettlementAttachment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementItemsToSettlementAttachment[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementItemsToSettlementAttachment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementItemsToSettlementAttachmentsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlement_items_to_settlement_attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SettlementItems', [
            'foreignKey' => 'settlement_item_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SettlementAttachments', [
            'foreignKey' => 'settlement_attachment_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['settlement_item_id'], 'SettlementItems'));
        $rules->add($rules->existsIn(['settlement_attachment_id'], 'SettlementAttachments'));

        return $rules;
    }
}
