<?php

namespace App\Model\Table;

use App\Model\Entity\Report;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Reports Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property ReportColumnsTable&HasMany $ReportColumns
 *
 * @method Report get($primaryKey, $options = [])
 * @method Report newEntity($data = null, array $options = [])
 * @method Report[] newEntities(array $data, array $options = [])
 * @method Report|false save(EntityInterface $entity, $options = [])
 * @method Report saveOrFail(EntityInterface $entity, $options = [])
 * @method Report patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Report[] patchEntities($entities, array $data, array $options = [])
 * @method Report findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ReportsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reports');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ReportColumns', [
            'foreignKey' => 'report_id',
            'sort' => [
                'ReportColumns.order' => 'ASC'
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('is_archived')
            ->notEmptyString('is_archived');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }

    public function findWithOrganization(int $organization_id): Query
    {
        return $this->find('all', [
            'conditions' => [
                'Reports.organization_id' => $organization_id,
            ],
        ]);
    }

    public function getWithOrganization(int $report_id, int $organization_id, array $contain = [])
    {
        return $this->get($report_id, [
            'conditions' => [
                'Reports.organization_id' => $organization_id,
            ],
            'contain' => $contain,
        ]);
    }
}
