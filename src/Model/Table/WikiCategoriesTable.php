<?php
namespace App\Model\Table;

use App\Model\Entity\WikiCategory;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * WikiCategories Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property WikisTable&HasMany $Wikis
 *
 * @method WikiCategory get($primaryKey, $options = [])
 * @method WikiCategory newEntity($data = null, array $options = [])
 * @method WikiCategory[] newEntities(array $data, array $options = [])
 * @method WikiCategory|false save(EntityInterface $entity, $options = [])
 * @method WikiCategory saveOrFail(EntityInterface $entity, $options = [])
 * @method WikiCategory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method WikiCategory[] patchEntities($entities, array $data, array $options = [])
 * @method WikiCategory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class WikiCategoriesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wiki_categories');
        $this->setDisplayField('category_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Wikis', [
            'foreignKey' => 'wiki_category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('category_name')
            ->maxLength('category_name', 255)
            ->requirePresence('category_name', 'create')
            ->notEmptyString('category_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
}
