<?php

namespace App\Model\Table;

use App\Model\Entity\ReportColumn;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * ReportColumns Model
 *
 * @property ReportsTable&BelongsTo $Reports
 * @property AgendioColumnsTable&BelongsTo $AgendioColumns
 *
 * @method ReportColumn get($primaryKey, $options = [])
 * @method ReportColumn newEntity($data = null, array $options = [])
 * @method ReportColumn[] newEntities(array $data, array $options = [])
 * @method ReportColumn|false save(EntityInterface $entity, $options = [])
 * @method ReportColumn saveOrFail(EntityInterface $entity, $options = [])
 * @method ReportColumn patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ReportColumn[] patchEntities($entities, array $data, array $options = [])
 * @method ReportColumn findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ReportColumnsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('report_columns');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Reports', [
            'foreignKey' => 'report_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('AgendioColumns', [
            'foreignKey' => 'agendio_column_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('path')
            ->requirePresence('path', 'create')
            ->notEmptyString('path');

        $validator
            ->integer('order')
            ->requirePresence('order', 'create')
            ->notEmptyString('order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['report_id'], 'Reports'));
        $rules->add($rules->existsIn(['agendio_column_id'], 'AgendioColumns'));

        return $rules;
    }

    public function getWithReport(int $column_id, int $report_id, array $contain = [])
    {
        return $this->get($column_id, [
            'conditions' => [
                'ReportColumns.report_id' => $report_id,
            ],
            'contain' => $contain,
        ]);
    }
}
