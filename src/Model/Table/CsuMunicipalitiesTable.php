<?php
namespace App\Model\Table;

use App\Model\Entity\CsuMunicipality;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * CsuMunicipalities Model
 *
 * @method CsuMunicipality get($primaryKey, $options = [])
 * @method CsuMunicipality newEntity($data = null, array $options = [])
 * @method CsuMunicipality[] newEntities(array $data, array $options = [])
 * @method CsuMunicipality|false save(EntityInterface $entity, $options = [])
 * @method CsuMunicipality saveOrFail(EntityInterface $entity, $options = [])
 * @method CsuMunicipality patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CsuMunicipality[] patchEntities($entities, array $data, array $options = [])
 * @method CsuMunicipality findOrCreate($search, callable $callback = null, $options = [])
 */
class CsuMunicipalitiesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('csu_municipalities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany(
            'CsuMunicipalitiesToCsuMunicipalityParts',
            [
            'foreignKey' => 'csu_municipalities_number',
            'bindingKey' => 'number',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('number')
            ->requirePresence('number', 'create')
            ->add('number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 25)
            ->requirePresence('short_name', 'create')
            ->notEmptyString('short_name');

        $validator
            ->scalar('name')
            ->maxLength('name', 33)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['number']));

        return $rules;
    }
}
