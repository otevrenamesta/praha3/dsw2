<?php

namespace App\Model\Table;

use App\Model\Entity\UserRole;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * TeamsRolesInProgram Model
 *
 *
 * @mixin TimestampBehavior
 */
class TeamsRolesInProgramTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams_roles_in_programs');
        $this->setDisplayField('program_role_id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->belongsTo('Programs')
        ->setForeignKey('id')
        ->setProperty('program_id');
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        /*
        $validator
            ->scalar('role_name')
            ->maxLength('role_name', 255)
            ->requirePresence('role_name', 'create')
            ->notEmptyString('role_name')
            ->add('role_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
        */

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        /*
        $rules->add($rules->isUnique(['role_name']));
        */

        return $rules;
    }
}
