<?php

namespace App\Model\Table;

use App\Model\Entity\Team;
use App\Controller\TeamsManagerController;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * Teams Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property UsersTable&BelongsToMany $Users
 *
 * @method Team get($primaryKey, $options = [])
 * @method Team newEntity($data = null, array $options = [])
 * @method Team[] newEntities(array $data, array $options = [])
 * @method Team|false save(EntityInterface $entity, $options = [])
 * @method Team saveOrFail(EntityInterface $entity, $options = [])
 * @method Team patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Team[] patchEntities($entities, array $data, array $options = [])
 * @method Team findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TeamsTable extends AppTable
{
    // TODO: check where else apart  from the programs this is used
    public const ALL_TEAMS = [
        /*
        'FormalCheckPrograms', // formální kontrola
        'PriceProposalPrograms', // navrhovatelé
        'PriceApprovalPrograms', // schvalovatelé
        'CommentsPrograms', // hodnotitelé
        'RequestManagerPrograms', // finalizace
        'PreviewPrograms', // náhled
        */
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Users',
            [
                'through' => 'TeamsToUsers',
            ]
        );
        
        $this->hasMany('TeamsRolesInPrograms', [
            'foreignKey' => 'team_id'
        ]);

        $this->belongsToMany('Programs', [
            'foreignKey' => 'team_id',
            'targetForeignKey' => 'program_id',
            'through' => 'TeamsRolesInPrograms'
        ]);

        $this->hasMany(
            'TeamsRolesInProgram',
            [
                'foreignKey' => 'team_id',
            ]
        );
        /*
        $this->hasMany(
            'FormalCheckPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'formal_check_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        $this->hasMany(
            'PriceProposalPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'price_proposal_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        $this->hasMany(
            'PriceApprovalPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'price_approval_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        $this->hasMany(
            'PreviewPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'preview_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        $this->hasMany(
            'CommentsPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'comments_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        $this->hasMany(
            'RequestManagerPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'request_manager_team_id',
                'saveStrategy' => 'replace',
            ]
        );
        */
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
    
    /**
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     * 
     * @return [type]
     * 
     * Saves mutiple team/role/program associations (to allow more teams have a role in a program)
     */

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // Get this team ID from the request
        if (!is_numeric($team_id = Router::getRequest()->getParam('id'))) return;
        $roles = TeamsManagerController::getTeamsRoles(); 
        $teams_roles_in_program = TableRegistry::getTableLocator()->get('TeamsRolesInProgram');
        $new_roles = [];
        // Based on the $data which holds values from the submitted form, rebuild roles in programs for this team
        foreach ($roles as $role_id => $role) {
            if (is_array($program_ids = $data[$role]['_ids'])) {
                foreach ($program_ids as $program_id) {
                    // Create record team $team_id, program $program_id, program_role_id $role_id");
                    $new_role = $teams_roles_in_program->newEntity();
                    $new_role->team_id = $team_id;
                    $new_role->program_id = $program_id;
                    $new_role->program_role_id = $role_id;
                    $new_roles[] = $new_role;
                }
            }
        }
        // Delete all previous roles for this team
        $teams_roles_in_program->deleteAll(['team_id' => $team_id]);
        // And create new ones
        $teams_roles_in_program->saveMany($new_roles);
    }
}
