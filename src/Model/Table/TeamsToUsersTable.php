<?php
namespace App\Model\Table;

use App\Model\Entity\TeamsToUser;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * TeamsToUsers Model
 *
 * @property TeamsTable&BelongsTo $Teams
 * @property UsersTable&BelongsTo $Users
 *
 * @method TeamsToUser get($primaryKey, $options = [])
 * @method TeamsToUser newEntity($data = null, array $options = [])
 * @method TeamsToUser[] newEntities(array $data, array $options = [])
 * @method TeamsToUser|false save(EntityInterface $entity, $options = [])
 * @method TeamsToUser saveOrFail(EntityInterface $entity, $options = [])
 * @method TeamsToUser patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method TeamsToUser[] patchEntities($entities, array $data, array $options = [])
 * @method TeamsToUser findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class TeamsToUsersTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('teams_to_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Teams', [
            'foreignKey' => 'team_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        
        $this->belongsTo('TeamsRolesInPrograms', [
            'foreignKey' => 'team_id',
            'bindingKey' => 'team_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['team_id'], 'Teams'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
