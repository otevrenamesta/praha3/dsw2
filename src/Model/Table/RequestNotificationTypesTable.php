<?php

namespace App\Model\Table;

use App\Model\Entity\RequestNotificationType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestNotificationTypes Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method RequestNotificationType get($primaryKey, $options = [])
 * @method RequestNotificationType newEntity($data = null, array $options = [])
 * @method RequestNotificationType[] newEntities(array $data, array $options = [])
 * @method RequestNotificationType|false save(EntityInterface $entity, $options = [])
 * @method RequestNotificationType saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestNotificationType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestNotificationType[] patchEntities($entities, array $data, array $options = [])
 * @method RequestNotificationType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestNotificationTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_notification_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }
}
