<?php

namespace App\Model\Table;

use App\Model\Entity\AgendioColumn;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * AgendioColumns Model
 *
 * @property AgendioReportsTable&BelongsTo $AgendioReports
 *
 * @method AgendioColumn get($primaryKey, $options = [])
 * @method AgendioColumn newEntity($data = null, array $options = [])
 * @method AgendioColumn[] newEntities(array $data, array $options = [])
 * @method AgendioColumn|false save(EntityInterface $entity, $options = [])
 * @method AgendioColumn saveOrFail(EntityInterface $entity, $options = [])
 * @method AgendioColumn patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method AgendioColumn[] patchEntities($entities, array $data, array $options = [])
 * @method AgendioColumn findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AgendioColumnsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agendio_columns');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AgendioReports', [
            'foreignKey' => 'agendio_report_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('required')
            ->notEmptyString('required');

        $validator
            ->scalar('label')
            ->maxLength('label', 255)
            ->requirePresence('label', 'create')
            ->notEmptyString('label');

        $validator
            ->scalar('export_name')
            ->maxLength('export_name', 255)
            ->requirePresence('export_name', 'create')
            ->notEmptyString('export_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['agendio_report_id'], 'AgendioReports'));

        return $rules;
    }
}
