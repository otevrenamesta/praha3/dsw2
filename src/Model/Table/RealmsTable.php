<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Realm;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Realms Model
 *
 * @property FondsTable&BelongsTo $Fonds
 * @property ProgramsTable&HasMany $Programs
 *
 * @method Realm get($primaryKey, $options = [])
 * @method Realm newEntity($data = null, array $options = [])
 * @method Realm[] newEntities(array $data, array $options = [])
 * @method Realm|false save(EntityInterface $entity, $options = [])
 * @method Realm saveOrFail(EntityInterface $entity, $options = [])
 * @method Realm patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Realm[] patchEntities($entities, array $data, array $options = [])
 * @method Realm findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RealmsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('realms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Fonds',
            [
                'foreignKey' => 'fond_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Programs',
            [
                'foreignKey' => 'realm_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fond_id'], 'Fonds'));
        $this->addDeleteLinkConstraintRule(
            $rules,
            'Programs',
            'fks',
            __('Nelze smazat kvůli existujícím vazbám. Oblast podpory obsahuje podřízené programy/podprogramy.')
        );

        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function fkCheck(AppEntity $entity, $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get(
            $entity->id,
            [
                'contain' => [
                    'Programs.Appeals.Requests',
                    'Programs.Requests',
                ],
            ]
        );

        foreach ($withAssociations->programs as $program) {
            if (!empty($program->appeals)) {
                return __('Nelze smazat oblast podpory s programy aktivními ve výzvách');
            }
            if (!empty($program->requests)) {
                return __('Nelze smazat oblast podpory s programy které obsahují rozpracované žádosti');
            }
        }

        return true;
    }

    /**
     * Relams ordering by weight first, then by name.
     * NOTE: Not sure where the default order is used, if anywhere, but this should be the default order
     *
     *
     */

    public function beforeFind($event, $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !count($order)) {
            $alias = $this->getAlias();
            $query->order([$alias . '.weight' => 'asc', $alias . '.name' => 'asc']);
        }
    }
}
