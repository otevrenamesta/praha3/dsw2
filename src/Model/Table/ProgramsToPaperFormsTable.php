<?php
namespace App\Model\Table;

use App\Model\Entity\ProgramsToPaperForm;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * ProgramsToPaperForms Model
 *
 * @property ProgramsTable&BelongsTo $Programs
 * @property FormsTable&BelongsTo $Forms
 *
 * @method ProgramsToPaperForm get($primaryKey, $options = [])
 * @method ProgramsToPaperForm newEntity($data = null, array $options = [])
 * @method ProgramsToPaperForm[] newEntities(array $data, array $options = [])
 * @method ProgramsToPaperForm|false save(EntityInterface $entity, $options = [])
 * @method ProgramsToPaperForm saveOrFail(EntityInterface $entity, $options = [])
 * @method ProgramsToPaperForm patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProgramsToPaperForm[] patchEntities($entities, array $data, array $options = [])
 * @method ProgramsToPaperForm findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProgramsToPaperFormsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('programs_to_paper_forms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Forms', [
            'foreignKey' => 'form_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        $rules->add($rules->existsIn(['form_id'], 'Forms'));

        return $rules;
    }
}
