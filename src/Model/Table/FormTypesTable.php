<?php

namespace App\Model\Table;

use App\Model\Entity\FormType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * FormTypes Model
 *
 * @method FormType get($primaryKey, $options = [])
 * @method FormType newEntity($data = null, array $options = [])
 * @method FormType[] newEntities(array $data, array $options = [])
 * @method FormType|false save(EntityInterface $entity, $options = [])
 * @method FormType saveOrFail(EntityInterface $entity, $options = [])
 * @method FormType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method FormType[] patchEntities($entities, array $data, array $options = [])
 * @method FormType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class FormTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('form_types');
        $this->setDisplayField('type_name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }
}
