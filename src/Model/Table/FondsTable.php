<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Fond;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Fonds Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property RealmsTable&HasMany $Realms
 *
 * @method Fond get($primaryKey, $options = [])
 * @method Fond newEntity($data = null, array $options = [])
 * @method Fond[] newEntities(array $data, array $options = [])
 * @method Fond|false save(EntityInterface $entity, $options = [])
 * @method Fond saveOrFail(EntityInterface $entity, $options = [])
 * @method Fond patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Fond[] patchEntities($entities, array $data, array $options = [])
 * @method Fond findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class FondsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fonds');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Realms',
            [
                'foreignKey' => 'fond_id',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $this->addDeleteLinkConstraintRule(
            $rules,
            'Realms',
            'fks',
            __('Nelze smazat kvůli existujícím vazbám. Dotační fond obsahuje oblasti podpory.')
        );

        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function fkCheck(AppEntity $entity, $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get(
            $entity->id,
            ['contain' => [
                'Realms.Programs.Appeals',
            ]]
        );

        foreach ($withAssociations->realms as $realm) {
            foreach ($realm->programs as $program) {
                if (!empty($program->appeals)) {
                    return __('Nelze smazat kvůli existujícím vazbám. Dotační fond obsahuje oblasti podpory.');
                }
            }
        }

        return true;
    }
}
