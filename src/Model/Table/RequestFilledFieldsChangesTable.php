<?php
namespace App\Model\Table;

use App\Model\Entity\RequestFilledFieldChange;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestFilledFieldsChanges Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property FormsTable&BelongsTo $Forms
 * @property FormFieldsTable&BelongsTo $FormFields
 *
 * @method RequestFilledField get($primaryKey, $options = [])
 * @method RequestFilledField newEntity($data = null, array $options = [])
 * @method RequestFilledField[] newEntities(array $data, array $options = [])
 * @method RequestFilledField|false save(EntityInterface $entity, $options = [])
 * @method RequestFilledField saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestFilledField patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestFilledField[] patchEntities($entities, array $data, array $options = [])
 * @method RequestFilledField findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestFilledFieldsChangesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_filled_fields_changes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RequestChanges', [
            'foreignKey' => 'change_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Forms', [
            'foreignKey' => 'form_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FormFields', [
            'foreignKey' => 'form_field_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('value')
            ->allowEmptyString('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['change_id'], 'RequestChanges'));
        $rules->add($rules->existsIn(['form_id'], 'Forms'));
        $rules->add($rules->existsIn(['form_field_id'], 'FormFields'));

        return $rules;
    }
}
