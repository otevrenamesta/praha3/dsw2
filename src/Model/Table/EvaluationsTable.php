<?php
namespace App\Model\Table;

use App\Model\Entity\Evaluation;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Evaluations Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property EvaluationCriteriaTable&BelongsTo $EvaluationCriteria
 * @property UsersTable&BelongsTo $Users
 *
 * @method Evaluation get($primaryKey, $options = [])
 * @method Evaluation newEntity($data = null, array $options = [])
 * @method Evaluation[] newEntities(array $data, array $options = [])
 * @method Evaluation|false save(EntityInterface $entity, $options = [])
 * @method Evaluation saveOrFail(EntityInterface $entity, $options = [])
 * @method Evaluation patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Evaluation[] patchEntities($entities, array $data, array $options = [])
 * @method Evaluation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class EvaluationsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('evaluations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('EvaluationCriteria', [
            'foreignKey' => 'evaluation_criteria_id',
            'joinType' => 'INNER',
            'propertyName' => 'evaluation_criterium'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('responses')
            ->requirePresence('responses', 'create')
            ->notEmptyString('responses');

        $validator
            ->decimal('points')
            ->requirePresence('points', 'create')
            ->notEmptyString('points');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['evaluation_criteria_id'], 'EvaluationCriteria'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
