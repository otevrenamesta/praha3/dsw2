<?php

namespace App\Model\Table;

use App\Model\Entity\Wiki;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Wikis Model
 *
 * @property WikiCategoriesTable&BelongsTo $WikiCategories
 *
 * @method Wiki get($primaryKey, $options = [])
 * @method Wiki newEntity($data = null, array $options = [])
 * @method Wiki[] newEntities(array $data, array $options = [])
 * @method Wiki|false save(EntityInterface $entity, $options = [])
 * @method Wiki saveOrFail(EntityInterface $entity, $options = [])
 * @method Wiki patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Wiki[] patchEntities($entities, array $data, array $options = [])
 * @method Wiki findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class WikisTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('wikis');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('WikiCategories', [
            'foreignKey' => 'wiki_category_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('contents')
            ->requirePresence('contents', 'create')
            ->notEmptyString('contents');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['wiki_category_id'], 'WikiCategories'));

        return $rules;
    }
}
