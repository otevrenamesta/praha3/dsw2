<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Form;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Forms Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property FormFieldsTable&HasMany $FormFields
 * @property ProgramsTable&BelongsToMany $Programs
 * @property ProgramsTable&BelongsToMany $PaperPrograms
 * @property RequestFilledFieldsTable&HasMany $RequestFilledFields
 * @property FormTypesTable&BelongsTo $FormTypes
 * @property FormSettingsTable&HasMany $FormSettings
 *
 * @method Form get($primaryKey, $options = [])
 * @method Form newEntity($data = null, array $options = [])
 * @method Form[] newEntities(array $data, array $options = [])
 * @method Form|false save(EntityInterface $entity, $options = [])
 * @method Form saveOrFail(EntityInterface $entity, $options = [])
 * @method Form patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Form[] patchEntities($entities, array $data, array $options = [])
 * @method Form findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class FormsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('forms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'FormTypes',
            [
                'foreignKey' => 'form_type_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'FormFields',
            [
                'foreignKey' => 'form_id',
            ]
        );
        $this->belongsToMany(
            'Programs',
            [
                'through' => 'programs_to_forms',
            ]
        );
        $this->belongsToMany(
            'PaperPrograms',
            [
                'through' => 'programs_to_paper_forms',
                'targetForeignKey' => 'program_id',
                'className' => 'Programs',
            ]
        );
        $this->hasMany('FormSettings', [
            'foreignKey' => 'form_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $this->addDeleteLinkConstraintRule(
            $rules,
            'FormFields',
            'fks',
            __('Nelze smazat formulář, který není prázdný')
        );

        return $rules;
    }

    public function fkCheck(AppEntity $entity, array $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get($entity->id, [
            'contain' => [
                'FormFields.RequestFilledFields',
            ],
        ]);

        foreach ($withAssociations->form_fields as $form_field) {
            if (!empty($form_field->request_filled_fields)) {
                return __('Nelze smazat formulář, který byl již vyplněn');
            }
        }

        return true;
    }

    /**
     * Forms ordering by weight first, then by id.
     * NOTE: Not sure where the default order is used, if anywhere, but this should be the default order
     *
     * TODO: ALTER TABLE `forms` ADD `weight` INT NOT NULL DEFAULT '0' COMMENT 'Váha pro řazení formulářů' AFTER `organization_id`;
     */

    public function beforeFind($event, $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !count($order)) {
            $alias = $this->getAlias();
            $query->order([$alias . '.weight' => 'asc', $alias . '.id' => 'asc']);
        }
    }
}
