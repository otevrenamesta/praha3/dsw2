<?php

namespace App\Model\Table;

use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Association\HasOne;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Requests Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property UsersTable&BelongsTo $Users
 * @property UsersTable&BelongsTo $PaperEvidencingUsers
 * @property ProgramsTable&BelongsTo $Programs
 * @property AppealsTable&BelongsTo $Appeals
 * @property EvaluationsTable&HasMany $Evaluations
 * @property AttachmentsTable&HasMany $Attachments
 * @property FilesTable&BelongsToMany $Files
 * @property RequestBudgetsTable&HasOne $RequestBudgets
 * @property RequestBudgetsTable&HasOne $RequestBudgetsChanges
 * @property RequestLogsTable&HasMany $RequestLogs
 * @property IdentitiesTable&HasMany $Identities
 * @property RequestFilledFieldsTable&HasMany $RequestFilledFields
 * @property RequestStatesTable&BelongsTo $RequestStates
 * @property PaymentsTable&HasMany $Payments
 * @property RequestNotificationsTable&HasMany $RequestNotifications
 *
 * @method Request get($primaryKey, $options = [])
 * @method Request newEntity($data = null, array $options = [])
 * @method Request[] newEntities(array $data, array $options = [])
 * @method Request|false save(EntityInterface $entity, $options = [])
 * @method Request saveOrFail(EntityInterface $entity, $options = [])
 * @method Request patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Request[] patchEntities($entities, array $data, array $options = [])
 * @method Request findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestsTable extends AppTable
{
    const CONTAIN_DEFAULT = [
        'Programs',
        'Programs.EvaluationCriteria',
        'Programs.ParentPrograms',
        'Programs.ParentPrograms.EvaluationCriteria',
        'Programs.Realms',
        'Programs.ParentPrograms.Realms',
        'Programs.Realms.Fonds',
        'Programs.ParentPrograms.Realms.Fonds',
        'Users',
        'RequestBudgets',
        'RequestBudgetsChanges',
        'RequestChanges',
        'Evaluations',
        'Evaluations.Users',
        'Appeals',
        'Files',
        'Payments',
        'Evaluations.EvaluationCriteria',
    ];
    const CONTAIN_EXTENDED = [
        'Programs' => [
            'Forms.FormFields',
            'PaperForms.FormFields',
            'ParentPrograms.Forms.FormFields',
            'ParentPrograms.PaperForms.FormFields',
        ],
        'Programs.EvaluationCriteria',
        'Programs.ParentPrograms',
        'Programs.ParentPrograms.EvaluationCriteria',
        'Programs.Realms',
        'Programs.ParentPrograms.Realms',
        'Programs.Realms.Fonds',
        'Programs.ParentPrograms.Realms.Fonds',
        'Users' => [
            'PublicIncomeHistories',
            'PublicIncomeHistories.PublicIncomeSources',
        ],
        'RequestBudgets' => [
            'OwnSources',
            'Incomes',
            'Costs',
            'OtherSubsidies',
        ],
        'RequestBudgetsChanges' => [
            'OwnSources',
            'Incomes',
            'Costs',
            'OtherSubsidies',
        ],
        'RequestChanges',
        'Evaluations',
        'Evaluations.Users',
        'Appeals',
        'Files',
        'Evaluations.EvaluationCriteria',
        'Payments' => [
            'Settlements'
        ],
        'Payments.Files',
        'RequestNotifications.Files',
        'RequestNotifications.RequestNotificationTypes',
        'Identities',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PaperEvidencingUsers', [
            'foreignKey' => 'paper_evidenced_by_user_id',
            'className' => 'Users',
        ]);
        $this->belongsTo('RequestStates', [
            'foreignKey' => 'request_state_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RequestTypes', [
            'foreignKey' => 'request_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Appeals', [
            'foreignKey' => 'appeal_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Evaluations', [
            'foreignKey' => 'request_id',
            'dependent' => true,
        ]);
        $this->belongsToMany('Files', [
            'className' => 'Files',
            'through' => 'Attachments',
        ]);
        $this->hasOne('RequestBudgets');
        $this->hasMany('RequestChanges');
        $this->hasMany('RequestBudgetsChanges');
        $this->hasMany('RequestLogs');
        $this->hasMany('Identities', [
            'foreignKey' => [
                'user_id',
                'version',
            ],
            'bindingKey' => [
                'user_id',
                'user_identity_version',
            ],
        ]);
        $this->hasMany('RequestFilledFields', [
            'foreignKey' => 'request_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'request_id'
        ]);
        $this->hasMany('RequestNotifications', [
            'foreignKey' => 'request_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('is_locked')
            ->notEmptyString('is_locked');

        $validator
            ->date('lock_when')
            ->allowEmptyDate('lock_when');

        $validator
            ->boolean('is_reported')
            ->notEmptyString('is_reported');

        $validator
            ->boolean('is_settled')
            ->notEmptyString('is_settled');

        $validator
            ->integer('final_subsidy_amount')
            ->allowEmptyString('final_subsidy_amount');

        $validator
            ->integer('user_identity_version')
            ->requirePresence('user_identity_version', 'create')
            ->notEquals('user_identity_version', 0, null, function ($context) {
                return !RequestState::canUserEditRequest(intval($context['data']['request_state_id'] ?? RequestState::STATE_NEW));
            });

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['program_id'], 'Programs'));
        $rules->add($rules->existsIn(['appeal_id'], 'Appeals'));
        $rules->add($rules->existsIn(['paper_evidenced_by_user_id'], 'PaperEvidencingUsers'));

        return $rules;
    }
}
