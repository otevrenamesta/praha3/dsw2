<?php

namespace App\Model\Table;

use App\Model\Entity\RequestNotificationsToFile;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * RequestNotificationsToFiles Model
 *
 * @property RequestNotificationsTable&BelongsTo $RequestNotifications
 * @property FilesTable&BelongsTo $Files
 *
 * @method RequestNotificationsToFile get($primaryKey, $options = [])
 * @method RequestNotificationsToFile newEntity($data = null, array $options = [])
 * @method RequestNotificationsToFile[] newEntities(array $data, array $options = [])
 * @method RequestNotificationsToFile|false save(EntityInterface $entity, $options = [])
 * @method RequestNotificationsToFile saveOrFail(EntityInterface $entity, $options = [])
 * @method RequestNotificationsToFile patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RequestNotificationsToFile[] patchEntities($entities, array $data, array $options = [])
 * @method RequestNotificationsToFile findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class RequestNotificationsToFilesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('request_notifications_to_files');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RequestNotifications', [
            'foreignKey' => 'request_notification_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Files', [
            'foreignKey' => 'file_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_notification_id'], 'RequestNotifications'));
        $rules->add($rules->existsIn(['file_id'], 'Files'));

        return $rules;
    }
}
