<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\EvaluationCriterium;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EvaluationCriteria Model
 *
 * @property EvaluationCriteriaTable&BelongsTo $ParentEvaluationCriteria
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property EvaluationCriteriaTable&HasMany $ChildEvaluationCriteria
 * @property EvaluationsTable&HasMany $Evaluations
 *
 * @method EvaluationCriterium get($primaryKey, $options = [])
 * @method EvaluationCriterium newEntity($data = null, array $options = [])
 * @method EvaluationCriterium[] newEntities(array $data, array $options = [])
 * @method EvaluationCriterium|false save(EntityInterface $entity, $options = [])
 * @method EvaluationCriterium saveOrFail(EntityInterface $entity, $options = [])
 * @method EvaluationCriterium patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method EvaluationCriterium[] patchEntities($entities, array $data, array $options = [])
 * @method EvaluationCriterium findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class EvaluationCriteriaTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('evaluation_criteria');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->setEntityClass(EvaluationCriterium::class);

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'ParentEvaluationCriteria',
            [
                'className' => 'EvaluationCriteria',
                'foreignKey' => 'parent_id',
                'propertyName' => 'parent_evaluation_criterium'
            ]
        );
        $this->addBehavior(
            'CounterCache',
            [
                'ParentEvaluationCriteria' => [
                    'max_points' => function ($event, $entity, Table $table, $original) {
                        $result = $table->find(
                            'all',
                            [
                                'conditions' => [
                                    'parent_id' => $entity->parent_id,
                                ],
                                'fields' => [
                                    'sum' => 'SUM(max_points)',
                                ],
                            ]
                        )->first();
                        return $result->sum ?? 0;
                    },
                ],
            ]
        );

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'ChildEvaluationCriteria',
            [
                'className' => 'EvaluationCriteria',
                'foreignKey' => 'parent_id',
                'propertyName' => 'child_evaluation_criteria'
            ]
        );
        $this->hasMany('Evaluations', [
            'className' => 'Evaluations',
            'foreignKey' => 'evaluation_criteria_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('parent_id')
            ->notEqualToField('parent_id', 'id', null, 'update')
            ->notEqualToField('id', 'parent_id', null, 'create')
            ->allowEmptyString('parent_id');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('min_points')
            ->requirePresence('min_points', 'create')
            ->notEmptyString('min_points')
            ->notBlank('min_points')
            ->add('min_points', 'custom', [
                'rule' => function ($value, $context) {
                    return $value <= $context['data']['max_points'];
                },
                'message' => 'Minimální počet bodů nemůže být vyšší než maximální počet bodů'
            ]);

        $validator
            ->integer('max_points')
            ->requirePresence('max_points', 'create')
            ->notEmptyString('max_points')
            ->notBlank('max_points');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentEvaluationCriteria'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $this->addDeleteLinkConstraintRule($rules, 'Evaluations', null, __('Nelze smazat hodnotící kritéria, kterými již byly hodnoceny žádosti'));

        return $rules;
    }

    public function fkCheck(AppEntity $entity, array $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $evaluations = $this->Evaluations->find('all', [
            'conditions' => [
                'Evaluations.evaluation_criteria_id' => $entity->get('parent_id') ?? $entity->id,
            ]
        ])->count();

        if ($evaluations < 1) {
            return true;
        }

        return __('Nelze smazat hodnotící kritéria, kterými již byly hodnoceny žádosti');
    }
}
