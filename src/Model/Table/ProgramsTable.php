<?php

namespace App\Model\Table;

use App\Model\Entity\AppEntity;
use App\Model\Entity\Program;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Programs Model
 *
 * @property ProgramsTable&BelongsTo $ParentPrograms
 * @property RealmsTable&BelongsTo $Realms
 * @property AppealsTable&BelongsToMany $Appeals
 * @property ProgramsTable&HasMany $ChildPrograms
 * @property EvaluationCriteriaTable&BelongsTo $EvaluationCriteria
 * @property FormsTable&BelongsToMany $Forms
 * @property RequestsTable&HasMany $Requests
 * @property ProjectBudgetDesignsTable&BelongsTo $ProjectBudgetDesigns
 *
 * @method Program get($primaryKey, $options = [])
 * @method Program newEntity($data = null, array $options = [])
 * @method Program[] newEntities(array $data, array $options = [])
 * @method Program|false save(EntityInterface $entity, $options = [])
 * @method Program saveOrFail(EntityInterface $entity, $options = [])
 * @method Program patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Program[] patchEntities($entities, array $data, array $options = [])
 * @method Program findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProgramsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('programs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'ProjectBudgetDesigns',
            [
                'joinType' => 'INNER',
                'foreignKey' => 'project_budget_design_id',
            ]
        );
        $this->belongsTo(
            'ParentPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->belongsTo(
            'Realms',
            [
                'foreignKey' => 'realm_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Appeals',
            [
                'through' => 'AppealsToPrograms',
            ]
        );
        $this->hasMany(
            'ChildPrograms',
            [
                'className' => 'Programs',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->hasMany(
            'TeamsRolesInProgram',
            [
                'foreignKey' => 'program_id',
            ]
        );
        $this->belongsTo(
            'EvaluationCriteria',
            [
                'foreignKey' => 'evaluation_criteria_id',
                'propertyName' => 'evaluation_criterium'
            ]
        );
        $this->belongsToMany(
            'Forms',
            [
                'through' => 'programs_to_forms',
            ]
        );
        $this->belongsToMany(
            'PaperForms',
            [
                'through' => 'programs_to_paper_forms',
                'targetForeignKey' => 'form_id',
                'className' => 'Forms'
            ]
        );
        $this->belongsTo(
            'PreviewTeams',
            [
                'className' => 'Teams',
                'foreignKey' => 'preview_team_id',
            ]
        );
        $this->hasMany('Requests', [
            'foreignKey' => 'program_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('evaluation_criteria_id')
            ->allowEmptyString('evaluation_criteria_id');

        $validator
            ->scalar('parent_id')
            ->allowEmptyString('parent_id')
            ->notEqualToField('parent_id', 'id', null, 'update')
            ->notEqualToField('id', 'parent_id', null, 'create');

        $allowIfChildProgram = function ($context) {
            return empty($context['parent_id']);
        };

        $validator
            ->allowEmptyString('realm_id', null, $allowIfChildProgram);
        /*
        $validator
            ->allowEmptyString('formal_check_team_id');
        $validator
            ->allowEmptyString('price_proposal_team_id');
        $validator
            ->allowEmptyString('price_approval_team_id');
        $validator
            ->allowEmptyString('comments_team_id');
        $validator
            ->allowEmptyString('request_manager_team_id');
        $validator
            ->allowEmptyString('preview_team_id');
        */

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentPrograms'));
        $rules->add($rules->existsIn(['realm_id'], 'Realms'));
        $rules->add($rules->existsIn(['evaluation_criteria_id'], 'EvaluationCriteria'));
        /*
        $rules->add($rules->existsIn(['formal_check_team_id'], 'FormalCheckTeams'));
        $rules->add($rules->existsIn(['price_proposal_team_id'], 'PriceProposalTeams'));
        $rules->add($rules->existsIn(['price_approval_team_id'], 'PriceApprovalTeams'));
        $rules->add($rules->existsIn(['preview_team_id'], 'PriceApprovalTeams'));
        $rules->add($rules->existsIn(['comments_team_id'], 'CommentsTeams'));
        $rules->add($rules->existsIn(['request_manager_team_id'], 'RequestManagerTeams'));
        */

        $this->addDeleteLinkConstraintRule($rules, 'Appeals', null, __('Nelze smazat Program, protože je součástí výzvy'));
        $this->addDeleteLinkConstraintRule($rules, 'Requests', null, __('Nelze smazat Program, ve kterém již jsou vytvořené žádosti o dotaci'));

        return $rules;
    }

    /**
     * @inheritDoc
     */
    public function fkCheck(AppEntity $entity, $options): bool
    {
        if ($entity->isNew()) {
            return true;
        }

        $withAssociations = $this->get(
            $entity->id,
            ['contains' => [
                'Appeals',
                'Requests',
            ],
            ]
        );

        if (!empty($withAssociations->appeals)) {
            return __('Nelze smazat program, který je součástí existující výzvy');
        }

        if (!empty($withAssociations->requests)) {
            return __('Nelze smazat program, ve kterém již jsou rozpracované žádosti o dotaci');
        }

        return true;
    }


/**
     * Programs ordering by weight first, then by name. 
     * NOTE: Not sure where the default order is used, if anywhere, but this should be the default order 
     * 
     * 
     */

    public function beforeFind($event, $query, $options, $primary)
    {
        $order = $query->clause('order');
        if ($order === null || !count($order)) {
            $alias = $this->getAlias();
            $query->order([$alias . '.weight' => 'asc', $alias . '.name' => 'asc'] );
        }
    }

}