<?php

namespace App\Model\Table;

use App\Model\Entity\CsuMunicipalityPart;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * CsuMunicipalityParts Model
 *
 * @property HasMany&CsuMunicipalitiesToCsuMunicipalityPartsTable $CsuMunicipalitiesToCsuMunicipalityParts
 *
 * @method CsuMunicipalityPart get($primaryKey, $options = [])
 * @method CsuMunicipalityPart newEntity($data = null, array $options = [])
 * @method CsuMunicipalityPart[] newEntities(array $data, array $options = [])
 * @method CsuMunicipalityPart|false save(EntityInterface $entity, $options = [])
 * @method CsuMunicipalityPart saveOrFail(EntityInterface $entity, $options = [])
 * @method CsuMunicipalityPart patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CsuMunicipalityPart[] patchEntities($entities, array $data, array $options = [])
 * @method CsuMunicipalityPart findOrCreate($search, callable $callback = null, $options = [])
 */
class CsuMunicipalityPartsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('csu_municipality_parts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany(
            'CsuMunicipalitiesToCsuMunicipalityParts',
            [
            'foreignKey' => 'csu_municipality_parts_number',
            'bindingKey' => 'number',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param  Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('number')
            ->requirePresence('number', 'create')
            ->add('number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 38)
            ->requirePresence('short_name', 'create')
            ->notEmptyString('short_name');

        $validator
            ->scalar('name')
            ->maxLength('name', 38)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['number']));

        return $rules;
    }
}
