<?php

namespace App\Model\Table;

use App\Model\Entity\FormFieldType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\Validation\Validator;

/**
 * FormFieldTypes Model
 *
 * @property FormFieldsTable&HasMany $FormFields
 *
 * @method FormFieldType get($primaryKey, $options = [])
 * @method FormFieldType newEntity($data = null, array $options = [])
 * @method FormFieldType[] newEntities(array $data, array $options = [])
 * @method FormFieldType|false save(EntityInterface $entity, $options = [])
 * @method FormFieldType saveOrFail(EntityInterface $entity, $options = [])
 * @method FormFieldType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method FormFieldType[] patchEntities($entities, array $data, array $options = [])
 * @method FormFieldType findOrCreate($search, callable $callback = null, $options = [])
 */
class FormFieldTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('form_field_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('FormFields', [
            'foreignKey' => 'form_field_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
