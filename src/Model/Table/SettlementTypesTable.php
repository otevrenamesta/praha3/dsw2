<?php
namespace App\Model\Table;

use App\Model\Entity\SettlementType;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\Validation\Validator;

/**
 * SettlementTypes Model
 *
 * @property SettlementsTable&HasMany $Settlements
 *
 * @method SettlementType get($primaryKey, $options = [])
 * @method SettlementType newEntity($data = null, array $options = [])
 * @method SettlementType[] newEntities(array $data, array $options = [])
 * @method SettlementType|false save(EntityInterface $entity, $options = [])
 * @method SettlementType saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementType[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class SettlementTypesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settlement_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Settlements', [
            'foreignKey' => 'settlement_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type_name')
            ->maxLength('type_name', 255)
            ->requirePresence('type_name', 'create')
            ->notEmptyString('type_name');

        return $validator;
    }
}
