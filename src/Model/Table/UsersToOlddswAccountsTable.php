<?php
namespace App\Model\Table;

use App\Model\Entity\UsersToOlddswAccount;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * UsersToOlddswAccounts Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property UctyTable&BelongsTo $Ucty
 *
 * @method UsersToOlddswAccount get($primaryKey, $options = [])
 * @method UsersToOlddswAccount newEntity($data = null, array $options = [])
 * @method UsersToOlddswAccount[] newEntities(array $data, array $options = [])
 * @method UsersToOlddswAccount|false save(EntityInterface $entity, $options = [])
 * @method UsersToOlddswAccount saveOrFail(EntityInterface $entity, $options = [])
 * @method UsersToOlddswAccount patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UsersToOlddswAccount[] patchEntities($entities, array $data, array $options = [])
 * @method UsersToOlddswAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UsersToOlddswAccountsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dsw2.users_to_olddsw_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Ucty', [
            'foreignKey' => 'ucet_id',
            'className' => 'OldDsw.Ucty',
            'joinType' => 'LEFT',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['ucet_id'], 'Ucty'));

        return $rules;
    }
}
