<?php
namespace App\Model\Table;

use App\Model\Entity\PublicIncomeHistory;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * PublicIncomeHistories Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property PublicIncomeSourcesTable&BelongsTo $PublicIncomeSources
 *
 * @method PublicIncomeHistory get($primaryKey, $options = [])
 * @method PublicIncomeHistory newEntity($data = null, array $options = [])
 * @method PublicIncomeHistory[] newEntities(array $data, array $options = [])
 * @method PublicIncomeHistory|false save(EntityInterface $entity, $options = [])
 * @method PublicIncomeHistory saveOrFail(EntityInterface $entity, $options = [])
 * @method PublicIncomeHistory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PublicIncomeHistory[] patchEntities($entities, array $data, array $options = [])
 * @method PublicIncomeHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PublicIncomeHistoriesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('public_income_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PublicIncomeSources', [
            'foreignKey' => 'public_income_source_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('amount_czk')
            ->requirePresence('amount_czk', 'create')
            ->notEmptyString('amount_czk');

        $validator
            ->scalar('fiscal_year')
            ->requirePresence('fiscal_year', 'create')
            ->notEmptyString('fiscal_year');

        $validator
            ->scalar('project_name')
            ->maxLength('project_name', 255)
            ->requirePresence('project_name', 'create')
            ->notEmptyString('project_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['public_income_source_id'], 'PublicIncomeSources'));

        return $rules;
    }
}
