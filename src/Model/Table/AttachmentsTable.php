<?php
namespace App\Model\Table;

use App\Model\Entity\Attachment;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * Attachments Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property FilesTable&BelongsTo $Files
 *
 * @method Attachment get($primaryKey, $options = [])
 * @method Attachment newEntity($data = null, array $options = [])
 * @method Attachment[] newEntities(array $data, array $options = [])
 * @method Attachment|false save(EntityInterface $entity, $options = [])
 * @method Attachment saveOrFail(EntityInterface $entity, $options = [])
 * @method Attachment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Attachment[] patchEntities($entities, array $data, array $options = [])
 * @method Attachment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AttachmentsTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Files', [
            'foreignKey' => 'file_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));
        $rules->add($rules->existsIn(['file_id'], 'Files'));

        return $rules;
    }
}
