<?php
namespace App\Model\Table;

use App\Model\Entity\CsuCountry;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * CsuCountries Model
 *
 * @method CsuCountry get($primaryKey, $options = [])
 * @method CsuCountry newEntity($data = null, array $options = [])
 * @method CsuCountry[] newEntities(array $data, array $options = [])
 * @method CsuCountry|false save(EntityInterface $entity, $options = [])
 * @method CsuCountry saveOrFail(EntityInterface $entity, $options = [])
 * @method CsuCountry patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CsuCountry[] patchEntities($entities, array $data, array $options = [])
 * @method CsuCountry findOrCreate($search, callable $callback = null, $options = [])
 */
class CsuCountriesTable extends AppTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('csu_countries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('number')
            ->maxLength('number', 38)
            ->requirePresence('number', 'create')
            ->notEmptyString('number')
            ->add('number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 42)
            ->requirePresence('short_name', 'create')
            ->notEmptyString('short_name');

        $validator
            ->scalar('name')
            ->maxLength('name', 64)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['number']));

        return $rules;
    }
}
