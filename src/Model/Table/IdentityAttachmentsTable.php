<?php

namespace App\Model\Table;

use App\Model\Entity\File;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * IdentityAttachments Model
 *
 * @property IdentityAttachmentTypesTable&BelongsTo $IdentityAttachmentTypes
 * @property FilesTable&BelongsTo $Files
 *
 * @method SettlementAttachment get($primaryKey, $options = [])
 * @method SettlementAttachment newEntity($data = null, array $options = [])
 * @method SettlementAttachment[] newEntities(array $data, array $options = [])
 * @method SettlementAttachment|false save(EntityInterface $entity, $options = [])
 * @method SettlementAttachment saveOrFail(EntityInterface $entity, $options = [])
 * @method SettlementAttachment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method SettlementAttachment[] patchEntities($entities, array $data, array $options = [])
 * @method SettlementAttachment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin TimestampBehavior
 */
class IdentityAttachmentsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('identity_attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('IdentityAttachmentTypes', [
            'foreignKey' => 'identity_attachment_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Files', [
            'foreignKey' => 'file_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_archived')
            ->notEmptyString('is_archived');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['identity_attachment_type_id'], 'IdentityAttachmentTypes'));
        $rules->add($rules->existsIn(['file_id'], 'Files'));

        return $rules;
    }

    public function beforeDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity instanceof SettlementAttachment) {
            if (!($entity->file instanceof File)) {
                $this->loadInto($entity, ['Files']);
            }
            if ($entity->file instanceof File) {
                $entity->file->deletePhysically();
            }
        }
    }
}
