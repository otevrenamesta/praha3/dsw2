<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Request;

interface ClassReportInterface
{

    /**
     * @return array [{column-id} => {column human readable label}]
     */
    public static function getReportColumns(): array;

    /**
     * Extract the column from Request or associated entities, return array of table column values (td's)
     *
     * @param Request $request
     * @param string $column
     * @return string[]
     */
    public static function extractColumn(Request $request, string $column): ?array;

    /**
     * Whether single added column renders as multiple table columns
     *
     * @param string $column
     * @return bool
     */
    public static function columnRendersAsMultiple(string $column): bool;

    /**
     * In case the column is rendered as multiple table columns, return header names for each rendered table column
     *
     * @param string $path
     * @param string $columnName
     * @return array
     */
    public static function getColumnHeaders(string $path, string $columnName): array;

}