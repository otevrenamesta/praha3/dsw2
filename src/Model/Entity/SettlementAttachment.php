<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * SettlementAttachment Entity
 *
 * @property int $id
 * @property int $settlement_id
 * @property int $settlement_attachment_type_id
 * @property bool $is_public
 * @property int $approved
 * @property null|string $approved_note
 * @property int $file_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Settlement $settlement
 * @property SettlementItem[] $settlement_items
 * @property SettlementAttachmentType $settlement_attachment_type
 * @property File $file
 */
class SettlementAttachment extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'settlement_id' => true,
        'settlement_attachment_type_id' => true,
        'is_public' => true,
        'approved' => true,
        'approved_note' => true,
        'file_id' => true,
        'modified' => true,
        'created' => true,
        'settlement' => true,
        'settlement_items' => true,
        'settlement_attachment_type' => true,
        'file' => true,
    ];

    public function getLabel($anonymize = false): string
    {
        $basic = __('Příloha č.') . $this->file_id;
        if ($anonymize || !($this->file instanceof File)) {
            return $basic;
        }
        return $basic . ' ' . $this->file->getLabel() . ($this->settlement_attachment_type instanceof SettlementAttachmentType ? ' - ' . $this->settlement_attachment_type->type_name : '');
    }

    /**
     * @param Settlement|null $settlement
     * @param bool $considerAutomaticallyUsed return is used only if it's paired with settlement item(s)
     * @return bool
     */
    public function isUsed(?Settlement $settlement = null, bool $considerAutomaticallyUsed = true): bool
    {
        if (!empty($this->settlement_items)) {
            return true;
        }
        if ($settlement instanceof Settlement) {
            foreach ($settlement->settlement_items ?? [] as $settlementItem) {
                foreach ($settlement->getAttachmentsByItem($settlementItem->id) as $itemAttachment) {
                    if ($itemAttachment->id === $this->id) {
                        return true;
                    }
                }
            }
        }
        if ($considerAutomaticallyUsed && in_array($this->settlement_attachment_type_id, SettlementAttachmentType::TYPES_AUTOMATICALLY_USED, true)) {
            return true;
        }
        return false;
    }
}
