<?php

namespace App\Model\Entity;

use App\Form\AbstractFormController;
use App\Form\FormControllerInterface;
use App\Form\SocialServiceBalanceSheetFormController;
use App\Form\SocialServiceBudgetFormController;
use App\Form\SocialServiceCapacitiesFormController;
use App\Form\SocialServicesStaffingFormController;
use App\Form\SocialServiceUsersInDistrictFormController;
use App\Form\StandardRequestFormController;
use App\Form\StandardRequestFormChangeController;
use App\Model\ClassReportInterface;
use App\Model\InstanceReportInterface;
use App\Model\Table\FormsTable;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Form Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_displayed
 * @property int $form_type_id
 * @property int $organization_id
 * @property int $allow_change
 * @property int $shared
 * @property int $weight
 * @property FrozenTime|null $modified
 *
 * @property Organization $organization
 * @property FormField[] $form_fields
 * @property Program[] $programs
 * @property Program[] $paper_programs
 * @property FormSetting[] $form_settings
 */
class Form extends AppEntity implements ClassReportInterface
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_displayed' => true,
        'organization_id' => true,
        'weight' => true,
        'allow_change' => true,
        'shared' => true,
        'modified' => true,
        'organization' => true,
        'form_fields' => true,
        'form_type_id' => true,
        'programs' => true,
        'form_settings' => true,
        'paper_programs' => true,
    ];

    public function getFormController(Request $userRequest = null, $change = false): AbstractFormController
    {
        if ($this->form_settings === null) {
            // ensure settings are loaded
            /** @var FormsTable $formsTable */
            $formsTable = $this->getTableLocator()->get('Forms');
            $formsTable->loadInto($this, ['FormSettings']);
        }
        // TODO: Implement form change requests for non standard forms 
        if ($change  && $this->form_type_id == FormType::FORM_TYPE_STANDARD) {
            return new StandardRequestFormChangeController($this, $userRequest);
        }
        switch ($this->form_type_id) {
            default:
            case FormType::FORM_TYPE_STANDARD:
                return new StandardRequestFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_SERVICE_EMPLOYEES:
                return new SocialServicesStaffingFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_SERVICE_CAPACITIES:
                return new SocialServiceCapacitiesFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_BALANCE_SHEET:
                return new SocialServiceBalanceSheetFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_BUDGET:
                return new SocialServiceBudgetFormController($this, $userRequest);
            case FormType::FORM_TYPE_SOCIAL_USERS_COUNT:
                return new SocialServiceUsersInDistrictFormController($this, $userRequest);
        }
    }

    public static function getReportColumns(): array
    {
        // not used
        return [];
    }

    /**
     * [(form id) => (form controller filled with request and settings)]
     * @var array
     */
    static array $_formControllerCache = [];
    public static function extractColumn(Request $request, string $column): ?array
    {
        list ($ignore, $form_id) = explode(".", $column);
        $form_id = intval($form_id);
        $request->loadFilledFields();
        $requestForm = $request->getFormById($form_id);
        if ($requestForm) {
            $formController = self::$_formControllerCache[$form_id] ?? $requestForm->getFormController();
            self::$_formControllerCache[$form_id] = $formController;
            if ($formController instanceof FormControllerInterface) {
                $formController->prefill($request);
            }
            if ($formController instanceof InstanceReportInterface) {
                return $formController->extractColumn($request, $column);
            }
        }
        return null;
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        return [];
    }
}
