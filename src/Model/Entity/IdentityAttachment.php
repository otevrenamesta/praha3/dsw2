<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * IdentityAttachment Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $identity_attachment_type_id
 * @property bool $is_archived
 * @property int $file_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property identity $identity
 * @property identityItem[] $identity_items
 * @property identityAttachmentType $identity_attachment_type
 * @property File $file
 */
class IdentityAttachment extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'identity_attachment_type_id' => true,
        'is_archived' => true,
        'file_id' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'identity' => true,
        'identity_attachment_type' => true,
        'file' => true,
    ];

    public function getLabel(): string
    {
        $basic = __('Příloha č.') . $this->file_id;
        if (!($this->file instanceof File)) {
            return $basic;
        }
        return $basic . ' ' . $this->file->getLabel() . ($this->identity_attachment_type instanceof identityAttachmentType ? ' - ' . $this->identity_attachment_type->type_name : '');
    }

    /**
     * @param identity|null $identity
     * @param bool $considerAutomaticallyUsed return is used only if it's paired with identity item(s)
     * @return bool
     */
    /*
    public function isUsed(?identity $identity = null, bool $considerAutomaticallyUsed = true): bool
    {
        if (!empty($this->identity_items)) {
            return true;
        }
        if ($identity instanceof Identity) {
            foreach ($identity->identity_items ?? [] as $identityItem) {
                foreach ($identity->getAttachmentsByItem($identityItem->id) as $itemAttachment) {
                    if ($itemAttachment->id === $this->id) {
                        return true;
                    }
                }
            }
        }
        if ($considerAutomaticallyUsed && in_array($this->Identity_attachment_type_id, IdentityAttachmentType::TYPES_AUTOMATICALLY_USED, true)) {
            return true;
        }
        return false;
    }
    */
}
