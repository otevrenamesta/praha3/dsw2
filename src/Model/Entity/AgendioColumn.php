<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * AgendioColumn Entity
 *
 * @property int $id
 * @property int $agendio_report_id
 * @property bool $can_have_organization_preset
 * @property bool $required
 * @property string $label
 * @property string $export_name
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property AgendioReport $agendio_report
 */
class AgendioColumn extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'agendio_report_id' => true,
        'can_have_organization_preset' => true,
        'required' => true,
        'label' => true,
        'export_name' => true,
        'created' => true,
        'modified' => true,
        'agendio_report' => true,
    ];

    public function getPresetStorageKey(): ?string
    {
        if (!$this->can_have_organization_preset) {
            return null;
        }
        return OrganizationSetting::getAgendioPresetKey($this->agendio_report_id, $this->export_name);
    }
}
