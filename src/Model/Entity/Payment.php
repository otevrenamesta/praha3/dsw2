<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * Payment Entity
 *
 * @property int $id
 * @property int $request_id
 * @property float $amount_czk
 * @property int|null $settlement_id
 * @property bool $is_refund
 * @property bool $in_addition
 * @property string $comment
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property Settlement $settlement
 * @property PaymentsToFile[] $payments_to_files
 * @property File[] $files
 */
class Payment extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'amount_czk' => true,
        'settlement_id' => true,
        'is_refund' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'settlement' => true,
        'in_addition' => true,
        'comment' => true,
        'payments_to_files' => true,
        'files' => true,
    ];
}
