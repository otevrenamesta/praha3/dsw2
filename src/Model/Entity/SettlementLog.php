<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * SettlementLog Entity
 *
 * @property int $id
 * @property int $settlement_id
 * @property int $settlement_state_id
 * @property int|null $executed_by_user_id
 * @property string|null $return_reason
 * @property string|null $economics_statement
 * @property FrozenTime $modified
 * @property FrozenTime|null $created
 *
 * @property Settlement $settlement
 * @property SettlementState $settlement_state
 * @property User $user
 */
class SettlementLog extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'settlement_id' => true,
        'settlement_state_id' => true,
        'executed_by_user_id' => true,
        'return_reason' => true,
        'economics_statement' => true,
        'modified' => true,
        'created' => true,
        'settlement' => true,
        'settlement_state' => true,
        'user' => true,
    ];
}
