<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * RequestNotificationsToFile Entity
 *
 * @property int $id
 * @property int $request_notification_id
 * @property int $file_id
 * @property FrozenTime|null $modified
 *
 * @property RequestNotification $request_notification
 * @property File $file
 */
class RequestNotificationsToFile extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_notification_id' => true,
        'file_id' => true,
        'modified' => true,
        'request_notification' => true,
        'file' => true,
    ];
}
