<?php

namespace App\Model\Entity;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Entity\OrganizationSetting;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * Appeal Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $link
 * @property string|null $link2
 * @property bool $is_active
 * @property int $organization_id
 * @property FrozenDate $open_from
 * @property FrozenDate $open_to
 * @property FrozenDate $begin_sent
 * @property FrozenDate $end_sent
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Program[] $programs
 * @property Appeal[] $appeals
 * @property AppealsToProgram[] $appeals_to_programs
 */
class Appeal extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'link' => true,
        'link2' => true,
        'is_active' => true,
        'open_from' => true,
        'open_to' => true,
        'begin_sent' => true,
        'end_sent' => true,
        'modified' => true,
        'created' => true,
        'programs' => true,
        'organization_id' => true,
        'appeals' => true,
        'appeals_to_programs' => true,
    ];

    public function _getDescriptiveName(): string
    {
        return sprintf("%s (%s - %s)", $this->name, $this->open_from, $this->open_to);
    }

    public function canUserSubmitRequest(Request $request): bool
    {


        if (empty($this->open_from) || empty($this->open_to)) {
            return false;
        }
        $withTime = boolval(OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::APPEALS_OPEN_TO_WITH_TIME));
        $now = $withTime ? FrozenTime::now() : FrozenTime::now()->hour(0)->minute(0)->second(0);
        // add zero time, because the user can on/off the settings at any time
        // Issue #265 - openFrom is a date (not a datetime) in the database, make it CakePHP Time with a correct timezone
        //$openFrom = $withTime ? $this->open_from : $this->open_from->hour(0)->minute(0)->second(0);
        $openFrom = new FrozenTime($this->open_from->format('Y-m-d 0:0:0'));
        $openTo = $withTime ? $this->open_to : $this->open_to->hour(0)->minute(0)->second(0);

        return $openFrom->lessThanOrEquals($now) &&
            ($openTo->greaterThanOrEquals($now) || ($request->lock_when instanceof FrozenDate && $request->lock_when->greaterThanOrEquals($now)));
    }

    public function getMaxRequestBudget(int $program_id): ?float
    {

        foreach ($this->appeals_to_programs ?? [] as $program_settings) {
            if ($program_settings->program_id === $program_id) {
                return $program_settings->max_request_budget;
            }
        }

        return null;
    }

    public function getMinRequestBudget(int $program_id): ?float
    {

        foreach ($this->appeals_to_programs ?? [] as $program_settings) {
            if ($program_settings->program_id === $program_id) {
                return $program_settings->min_request_budget;
            }
        }

        return null;
    }

    public function getMaxSupportPercentage(int $program_id): ?float
    {

        foreach ($this->appeals_to_programs ?? [] as $program_settings) {
            if ($program_settings->program_id === $program_id) {
                return $program_settings->max_support;
            }
        }
        return null;
    }


    public function getProgramBudget(int $program_id): ?float
    {
        foreach ($this->appeals_to_programs ?? [] as $program_settings) {
            if ($program_settings->program_id === $program_id) {
                return $program_settings->budget_size;
            }
        }

        return null;
    }

    public function checkRequestLimit(float $requested_amount, int $program_id): bool
    {

        $max_program_budget = $this->getMaxRequestBudget($program_id);
        if (!$max_program_budget) {
            return true;
        }

        return $requested_amount <= $max_program_budget;
    }

    public function checkMinRequestLimit(float $requested_amount, int $program_id): bool
    {
        $min_program_budget = $this->getMinRequestBudget($program_id);
        if (!$min_program_budget) {
            return true;
        }

        return $requested_amount >= $min_program_budget;
    }

    public function checkMaxSupport(float $requested_percentage, int $program_id): bool
    {
        $max_support = $this->getMaxSupportPercentage($program_id);
        if (!$max_support) {
            return true;
        }
        return $requested_percentage <= $max_support;
    }

    public function canCommentsSubmitEvaluation(Request $request): bool
    {
        foreach ($this->appeals_to_programs ?? [] as $programLink) {
            if ($programLink->program_id === $request->program_id) {
                if (!empty($programLink->evaluation_deadline)) {
                    return $programLink->evaluation_deadline->greaterThanOrEquals(FrozenDate::now());
                }
            }
        }
        return true;
    }
}
