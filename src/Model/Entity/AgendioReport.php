<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * AgendioReport Entity
 *
 * @property int $id
 * @property string $label
 * @property FrozenTime|null $created
 * @property FrozenTime|null $modified
 *
 * @property AgendioColumn[] $agendio_columns
 */
class AgendioReport extends AppEntity
{
    public const AGENDIO_REPORT_SMLOUVA = 1;

    public const KNOWN_REPORTS = [
        self::AGENDIO_REPORT_SMLOUVA,
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'label' => true,
        'created' => true,
        'modified' => true,
        'agendio_columns' => true,
    ];
}
