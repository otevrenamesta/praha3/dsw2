<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * RequestNotificationType Entity
 *
 * @property int $id
 * @property int|null $organization_id
 * @property string $type_name
 * @property FrozenTime|null $modified
 *
 * @property Organization $organization
 */
class RequestNotificationType extends AppEntity
{
    public const TYPE_ACTIVITY = 1,
        TYPE_WORK = 2,
        TYPE_YEARLONG_ACTIVITY = 3;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'type_name' => true,
        'modified' => true,
        'organization' => true,
    ];

    public static function getTypesWithLabels()
    {
        return [
            self::TYPE_ACTIVITY => __('Akce'),
            self::TYPE_WORK => __('Dílo'),
            self::TYPE_YEARLONG_ACTIVITY => __('Celoroční činnost')
        ];
    }
}
