<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * ProjectBudgetDesign Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Program[] $programs
 */
class ProjectBudgetDesign extends AppEntity
{
    public const DESIGN_P3 = 1,
        DESIGN_P14 = 2,
        DESIGN_USTI_V1 = 3,
        DESIGN_USTI_V2 = 4,
        DESIGN_USTI_V3 = 5,
        DESIGN_USTI_V4 = 6,
        DESIGN_USTI_V5 = 7,
        DESIGN_USTI_V6 = 8,
        DESIGN_USTI_V7 = 9,
        DESIGN_USTI_V8 = 10,
        DESIGN_USTI_V9 = 11,
        DESIGN_USTI_V10 = 12,
        DESIGN_P4 = 13,
        DESIGN_USTI_V11 = 14,
        DESIGN_USTI_V12 = 15,
        DESIGN_USTI_V11_CHANGE = 16,
        DESIGN_USTI_V12_CHANGE = 17,
        DESIGN_USTI_V13 = 18,
        DESIGN_USTI_V14 = 19,
        DESIGN_USTI_V15 = 20,
        DESIGN_NONE = 21,
        DESIGN_USTI_V16 = 22,
        DESIGN_USTI_V17 = 23,
        DESIGN_USTI_V18 = 24,
        DESIGN_TISNOV = 25,
        DESIGN_P2A  = 26,
        DESIGN_P2B = 27;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'programs' => true,
    ];
}
