<?php

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
use Cake\View\Helper\UrlHelper;
use Cake\View\View;
use App\Middleware\OrgDomainsMiddleware;

/**
 * OrganizationSetting Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property string $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 */
class OrganizationSetting extends AppEntity
{
    public const SITE_NAME = 'site.name',
        SITE_LOGO = 'site.logo',
        SITE_FAVICON = 'site.favicon';

    public const SITE_FOOTER_HIDE_ABOUT = 'site.footer.hide_about',
        SITE_HP_HIDE_GRANT_FUNDS = 'site.hp.hide_grant_funds';

    public const DS_ATS_ID = 'ds.ats_id',
        DS_ID = 'ds.id',
        DS_CERTIFICATE = 'ds.cert', DS_CERTKEY = 'ds.certkey',
        DS_TEST_ENVIRONMENT = 'ds.test';

    public const EAGRI_RDM_TEST_ENVIRONMENT = 'eagri.rdm.test';

    public const HAS_DSW = 'p3.dsw.enable';

    public const INDEX_HERO_TITLE = 'index.hero.title',
        INDEX_HERO_SUBTITLE = 'index.hero.subtitle',
        INDEX_HERO_TEXT = 'index.hero.text',
        INDEX_HERO = 'index.hero';

    public const EMAIL_SERVER = 'EmailTransport.default.host',
        EMAIL_PORT = 'EmailTransport.default.port',
        EMAIL_TLS = 'EmailTransport.default.tls',
        EMAIL_SERVER_USERNAME = 'EmailTransport.default.username',
        EMAIL_SERVER_PASSWORD = 'EmailTransport.default.password',
        EMAIL_FROM = 'Email.default.from',
        EMAIL_REPLYTO = 'Email.default.replyTo';

    public const CONTACT_EMAIL = 'site.contact.email',
        CONTACT_PHONE = 'site.contact.phone',
        CONTACT_ADDRESS = 'site.contact.address';

    public const ENABLE_REGISTRATION = 'site.registration.enable',
        ENABLE_LOGIN_USERS = 'site.login.users.enable';

    public const ENABLE_LOGIN_NIA = 'site.login.nia.enable';

    public const GA_TRACKING_ID = 'site.ga.id';

    public const CUSTOM_CSS = 'site.custom.css',
        CUSTOM_JS = 'site.custom.js';

    public const IDENTITY_REQUIRE_DS = 'identity.require.ds',
        IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS = 'identity.require.registration_info',
        IDENTITY_FO_MINIMUM_AGE = 'identity.minimum_age';

    public const SUBSIDY_HISTORY_YEARS = 'identity.history.years',
        SUBSIDY_HISTORY_SHOW_HISTORIES = 'identity.history.show_histories',
        SUBSIDY_HISTORY_NOT_REQUESTED = 'identity.history.not_requested';

    public const PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL = 'preview.request_detail.hide_sendmail',
        APPEALS_OPEN_TO_WITH_TIME = 'appeals.open_to.with_time';

    public const SHOW_RATINGS_TEAM_PROPOSALS = 'show_ratings.team_proposals',
        SHOW_RATINGS_TEAM_APPROVALS = 'show_ratings.team_approvals',
        SHOW_RATINGS_SHOW_PRIVATE_COMMENTS = 'show_ratings.show_private_comments';

    public const RATINGS_AMOUNT_FROM_CRITERIA_SUM = 'ratings.amount_from_criteria_sum';

    public const ALLOW_MANUAL_SUBMIT_OF_REQUESTS = 'formal_control.allow_manual',
        ALLOW_EMAIL_SUBMIT_OF_REQUESTS = 'formal_control.allow_email',
        MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS = 'formal_control.manual_submit_append_signature_fields',
        MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE = 'formal_control.manual_submit_require_verification_code';

    public const REPLY_TO_ADMIN_WHEN_NOTIFYING_USER = 'communication.reply_to_admin_who_executed_change';

    public const ALLOW_NOTIFICATIONS = 'notification.allow';
    public const ALLOW_NOTIFICATIONS_ALERTS = 'notification_alerts.allow';
    public const ALLOW_MERGING_OF_REQUESTS = 'merging_of_requests.allow';

    public const ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS = 'economics.text.settlement_return_instructions',
        ECONOMICS_TEXT_RETURN_FULL_SUBSIDY = 'economics.text.return_full_subsidy',
        ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH = 'economics.text.consent_with_publish',
        ECONOMICS_AMOUNT_NOT_REQUIRED_TO_BE_SETTLED = 'economics.waived_amount',
        ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION = 'economics.text.waived_amount_explanation',
        ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT = 'economics.settlement.max_optional_attachments',
        ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND = 'economics.settlement.disable_option_for_alt_send',
        ECONOMICS_ADDITIONAL_COLUMNS = 'economics.economics_additional_columns',
        ECONOMICS_ADDITIONAL_FINANCING = 'economics.economics_additional_financing',
        ECONOMICS_EXPENSES_TOTAL_FIELD = 'economics.economics_expenses_total_field';

    public const AGENDIO_PRESET_TEMPLATE = "agendio_preset.%d.%s";

    public const EXPORT_SHOW_PDF_HEADER = 'export.show_pdf_header',
        EXPORT_ADDRESS_FOR_PDF_HEADER = 'export.address_for_pdf_header',
        EXPORT_MESSAGE_FOR_PDF_HEADER = 'export.message_for_pdf_header';

    public const ALLOW_ADDITIONAL_FORM_CHANGES = 'forms.allow_additional_changes';

    public const EMAIL_MESSAGE_NEW_REQUEST = 'notification.email_message_new_request';
    public const EMAIL_MESSAGE_NEW_SETTLEMENT  = 'notification.email_message_new_settlement';
    public const EMAIL_MESSAGE_UPDATED_REQUEST  = 'notification.email_message_updated_request';
    public const EMAIL_MESSAGE_UPDATED_SETTLEMENT  = 'notification.email_message_updated_settlement';

    public const BOOLEANS = [
        self::EMAIL_TLS,
        self::HAS_DSW,
        self::ENABLE_REGISTRATION,
        self::ENABLE_LOGIN_USERS,
        self::ENABLE_LOGIN_NIA,
        self::DS_TEST_ENVIRONMENT,
        self::EAGRI_RDM_TEST_ENVIRONMENT,
        self::IDENTITY_REQUIRE_DS,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS,
        self::ALLOW_EMAIL_SUBMIT_OF_REQUESTS,
        self::MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS,
        self::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS,
        self::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS,
        self::SHOW_RATINGS_TEAM_APPROVALS,
        self::SHOW_RATINGS_TEAM_PROPOSALS,
        self::RATINGS_AMOUNT_FROM_CRITERIA_SUM,
        self::REPLY_TO_ADMIN_WHEN_NOTIFYING_USER,
        self::SUBSIDY_HISTORY_SHOW_HISTORIES,
        self::SUBSIDY_HISTORY_NOT_REQUESTED,
        self::PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL,
        self::APPEALS_OPEN_TO_WITH_TIME,
        self::SITE_FOOTER_HIDE_ABOUT,
        self::SITE_HP_HIDE_GRANT_FUNDS,
        self::EXPORT_SHOW_PDF_HEADER,
        self::ALLOW_ADDITIONAL_FORM_CHANGES,
        self::ALLOW_MERGING_OF_REQUESTS,
        self::ALLOW_NOTIFICATIONS_ALERTS,
        self::ALLOW_NOTIFICATIONS,
        self::ECONOMICS_ADDITIONAL_COLUMNS,
        self::ECONOMICS_ADDITIONAL_FINANCING,
        self::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND,
        self::ECONOMICS_EXPENSES_TOTAL_FIELD,
        self::EMAIL_MESSAGE_NEW_REQUEST,
        self::EMAIL_MESSAGE_NEW_SETTLEMENT,
        self::EMAIL_MESSAGE_UPDATED_REQUEST,
        self::EMAIL_MESSAGE_UPDATED_SETTLEMENT,
    ];

    public const INTEGERS = [
        self::EMAIL_PORT,
        self::SUBSIDY_HISTORY_YEARS,
        self::IDENTITY_FO_MINIMUM_AGE,
        self::ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT,
    ];

    public const DOUBLES = [];

    public const EMAILS = [
        self::EMAIL_FROM,
        self::EMAIL_SERVER_USERNAME,
        self::CONTACT_EMAIL,
        self::EMAIL_REPLYTO,
    ];

    public const TEXTEAREAS = [
        self::DS_CERTIFICATE,
        self::DS_CERTKEY,
        self::INDEX_HERO_TEXT,
        self::CONTACT_ADDRESS,
        self::CUSTOM_JS,
        self::CUSTOM_CSS,
        self::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS,
        self::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY,
        self::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH,
        self::ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION,
        self::EXPORT_ADDRESS_FOR_PDF_HEADER,
    ];

    public const ALL_TECHNICAL = [
        self::GA_TRACKING_ID,
        self::SITE_LOGO,
        self::SITE_FAVICON,
        self::EAGRI_RDM_TEST_ENVIRONMENT,
        self::DS_TEST_ENVIRONMENT,
        self::DS_ATS_ID,
        self::DS_ID,
        self::DS_CERTIFICATE,
        self::DS_CERTKEY,
        self::CUSTOM_CSS,
        self::CUSTOM_JS,
        self::SITE_HP_HIDE_GRANT_FUNDS,
        self::SITE_FOOTER_HIDE_ABOUT,
    ];

    public const ALL_BASIC = [
        self::SITE_NAME,
        self::CONTACT_EMAIL,
        self::CONTACT_PHONE,
        self::CONTACT_ADDRESS,
        self::ENABLE_REGISTRATION,
        self::ENABLE_LOGIN_USERS,
        self::ENABLE_LOGIN_NIA,
        self::EXPORT_SHOW_PDF_HEADER,
        self::EXPORT_ADDRESS_FOR_PDF_HEADER,
        self::EXPORT_MESSAGE_FOR_PDF_HEADER,
        self::ALLOW_ADDITIONAL_FORM_CHANGES
    ];

    public const ALL_SUBSIDY_PROCESS = [
        self::IDENTITY_REQUIRE_DS,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS,
        self::IDENTITY_FO_MINIMUM_AGE,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS,
        self::ALLOW_EMAIL_SUBMIT_OF_REQUESTS,
        self::MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS,
        self::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE,
        self::SUBSIDY_HISTORY_YEARS,
        self::SUBSIDY_HISTORY_SHOW_HISTORIES,
        self::SUBSIDY_HISTORY_NOT_REQUESTED,
        self::SHOW_RATINGS_TEAM_APPROVALS,
        self::SHOW_RATINGS_TEAM_PROPOSALS,
        self::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS,
        self::RATINGS_AMOUNT_FROM_CRITERIA_SUM,
        self::REPLY_TO_ADMIN_WHEN_NOTIFYING_USER,
        self::PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL,
        self::APPEALS_OPEN_TO_WITH_TIME,
        self::ALLOW_NOTIFICATIONS,
        self::ALLOW_NOTIFICATIONS_ALERTS,
        self::ALLOW_MERGING_OF_REQUESTS,
        self::EMAIL_MESSAGE_NEW_REQUEST,
        self::EMAIL_MESSAGE_UPDATED_REQUEST,
    ];

    public const ALL_ECONOMICS = [
        self::ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT,
        self::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS,
        self::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY,
        self::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH,
        self::ECONOMICS_AMOUNT_NOT_REQUIRED_TO_BE_SETTLED,
        self::ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION,
        self::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND,
        self::ECONOMICS_ADDITIONAL_FINANCING,
        self::EMAIL_MESSAGE_NEW_SETTLEMENT,
        self::EMAIL_MESSAGE_UPDATED_SETTLEMENT,
        self::ECONOMICS_ADDITIONAL_COLUMNS,
        self::ECONOMICS_EXPENSES_TOTAL_FIELD,
    ];

    public const ALL_INDEX_HERO = [
        self::INDEX_HERO_TITLE,
        self::INDEX_HERO_SUBTITLE,
        self::INDEX_HERO_TEXT,
    ];
    public const ALL_EMAIL = [
        self::EMAIL_SERVER,
        self::EMAIL_PORT,
        self::EMAIL_TLS,
        self::EMAIL_SERVER_USERNAME,
        self::EMAIL_SERVER_PASSWORD,
        self::EMAIL_FROM,
        self::EMAIL_REPLYTO,
    ];
    public const APP_GLOBAL_SETTINGS_GROUPS = [
        self::ALL_EMAIL,
    ];
    public const DEFAULT_VALUES = [
        self::HAS_DSW => false,
        self::EMAIL_TLS => false,
        self::ENABLE_LOGIN_USERS => true,
        self::ENABLE_LOGIN_NIA => false,
        self::ENABLE_REGISTRATION => true,
        self::IDENTITY_REQUIRE_DS => true,
        self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS => false,
        self::ALLOW_EMAIL_SUBMIT_OF_REQUESTS => false,
        self::MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS => false,
        self::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE => false,
        self::SUBSIDY_HISTORY_YEARS => 2,
        self::SUBSIDY_HISTORY_SHOW_HISTORIES => false,
        self::SUBSIDY_HISTORY_NOT_REQUESTED => false,
        self::PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL => false,
        self::APPEALS_OPEN_TO_WITH_TIME => false,
        self::ALLOW_NOTIFICATIONS => true,
        self::ALLOW_NOTIFICATIONS_ALERTS => false,
        self::ALLOW_MERGING_OF_REQUESTS => false,
        self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS => false,
        self::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS => 'Nevyúčtovanou (nevyčerpanou) část dotace, ' .
            'dle příslušné veřejnoprávní smlouvy, musíte vrátit na bankovní účet obce XXX-XXX/XXXX ' .
            'do 1.1. roku následujícího po podpisu smlouvy',
        self::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY => 'Pokud jste dotaci nevyužili a vracíte celou přijatou částku, ' .
            'nemusíte vytvářet žádnou položku vyúčtování ani nahrávat závěrečnou zprávu nebo doklad o publicitě',
        self::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH => 'Odesláním vyúčtování souhlasíte se zveřejněním položek vyúčtování a ' .
            'závěrečné zprávy v otevřených datech',
        self::ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION => 'V případě využití možnosti nevyúčtování XXXX Kč může být ' .
            'nevyúčtovaná částka až do výše XXXX Kč bez nutnosti vrácení dotace. V takovém případě je nezbytné vrátit ' .
            'pouze částku nad rámec XXXX Kč. V případě nevyužití možnosti nevyúčtování XXXX Kč je nezbytné vrátit celou' .
            ' výši nevyúčtované dotace podle předchozího odstavce.',
        self::SHOW_RATINGS_TEAM_APPROVALS => false,
        self::SHOW_RATINGS_TEAM_PROPOSALS => true,
        self::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS => false,
        self::RATINGS_AMOUNT_FROM_CRITERIA_SUM => false,
        self::IDENTITY_FO_MINIMUM_AGE => 18,
        self::ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT => 5,
        self::REPLY_TO_ADMIN_WHEN_NOTIFYING_USER => false,
        self::EAGRI_RDM_TEST_ENVIRONMENT => true,
        self::SITE_FOOTER_HIDE_ABOUT => false,
        self::SITE_HP_HIDE_GRANT_FUNDS => false,
        self::EXPORT_SHOW_PDF_HEADER => false,
        self::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND => false,
        self::ALLOW_ADDITIONAL_FORM_CHANGES => false,
        self::ECONOMICS_ADDITIONAL_FINANCING => false,
        self::ECONOMICS_ADDITIONAL_COLUMNS => false,
        self::ECONOMICS_EXPENSES_TOTAL_FIELD => false,
        self::EMAIL_MESSAGE_NEW_REQUEST => false,
        self::EMAIL_MESSAGE_NEW_SETTLEMENT  => false,
        self::EMAIL_MESSAGE_UPDATED_REQUEST  => false,
        self::EMAIL_MESSAGE_UPDATED_SETTLEMENT  => false,
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
    ];

    /**
     * Necessary for labels i18n translation
     *
     * @return array
     */
    public static function getLabels(): array
    {
        return [
            self::SITE_LOGO => __('Logo (název souboru nebo URL adresa)'),
            self::SITE_FAVICON => __('Favicon (název souboru nebo URL adresa)'),
            self::DS_ATS_ID => __('Datová Schránka ID Odesílací Brány / Autentizační Služby'),
            self::DS_ID => __('Datová Schránka ID'),
            self::DS_CERTIFICATE => __('Datová schránka - veřejný certifikát pro odesílací bránu - formát PEM (začíná -----BEGIN CERTIFICATE----- a končí -----END CERTIFICATE-----)'),
            self::DS_CERTKEY => __('Datová schránka - privátní klíč k certifikátu - formát PEM PKCS#8 unencrypted (začíná -----BEGIN PRIVATE KEY----- a končí -----END PRIVATE KEY-----)'),
            self::DS_TEST_ENVIRONMENT => __('Použít testovací prostředí Datových Schránek (czebox.cz) ?'),

            self::SITE_NAME => __('Název Portálu'),
            self::ENABLE_LOGIN_USERS => __('Povolit přihlašování uživatelů (občanů, netýká se administrátorů)'),
            self::ENABLE_LOGIN_NIA => __('Povolit přihlašování uživatelů pomocí NIA (Národní bod pro identifikaci a autentizaci)'),
            self::ENABLE_REGISTRATION => __('Povolit registraci uživatel (netýká se uživatel, pozvaných skrze sekci Správa Uživatelů'),

            self::IDENTITY_REQUIRE_DS => __('Vyžadovat vyplnění ID Datové Schránky v identitě žadatele'),
            self::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS => __('Vyžadovat vyplnění informací o registraci právnické osoby v identitě žadatele'),
            self::IDENTITY_FO_MINIMUM_AGE => __('Minimální věk žadatele, fyzické osoby, který může žádat o dotaci'),

            self::INDEX_HERO_TITLE => __('Nadpis'),
            self::INDEX_HERO_SUBTITLE => __('Podnadpis'),
            self::INDEX_HERO_TEXT => __('Text'),

            self::EMAIL_SERVER => __('E-mailový Server'),
            self::EMAIL_PORT => __('Port'),
            self::EMAIL_TLS => __('TLS Zabezpečení zapnuto?'),
            self::EMAIL_SERVER_USERNAME => __('Uživatelské jméno'),
            self::EMAIL_SERVER_PASSWORD => __('Heslo'),
            self::EMAIL_FROM => __('Odesílatel'),
            self::EMAIL_REPLYTO => __('Adresa, kam se mají doručovat odpovědi na odeslané e-maily'),

            self::CONTACT_EMAIL => __('Kontaktní email (veřejný)'),
            self::CONTACT_PHONE => __('Kontaktní telefon (veřejný)'),
            self::CONTACT_ADDRESS => __('Kontaktní adresa (veřejná)'),

            self::GA_TRACKING_ID => __('Google Analytics trackovací kód (typicky začíná "UA-")'),
            self::CUSTOM_JS => __('Vlastní Javascript kód, který bude aplikován na každou stránku dotačního portálu'),
            self::CUSTOM_CSS => __('Vlastní CSS styl, který bude aplikován na každou stránku dotačního portálu'),

            self::ALLOW_MANUAL_SUBMIT_OF_REQUESTS => __('Povolit možnost fyzického podání žádosti (stáhnout PDF, vytisknout, poodat jinak než datovou schránkou), vč. evidence papírově došlých žádostí při formální kontrole'),
            self::ALLOW_EMAIL_SUBMIT_OF_REQUESTS => __('Povolit možnost podání žádosti e-mailem'),
            self::MANUAL_SUBMIT_PDF_APPEND_SIGNATURE_FIELDS => __('V případě tisku PDF žádosti/vyúčtování pro fyzické podání na dotačním úřadu, připojit na konec žádosti pole pro datum a podpis žadatele'),
            self::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE => __('Při fyzickém podání žádosti vystavit Potvrzení o elektronické evidenci žádosti s ověřovacím kódem pro tým formální kontroly'),

            self::SUBSIDY_HISTORY_YEARS =>  sprintf(
                "%s<br/><small class='text-muted'>%s</small>",
                __('Počet let, za které by měl žadatel uvést historii své přijaté veřejné podpory (omezuje také výpis podpory v žádostech a sestavách)'),
                __('(0 = letošní rok, 1 = jeden rok nazpět, 2 = dva roky nazpět, apod.)')
            ),
            self::SUBSIDY_HISTORY_SHOW_HISTORIES => __('Zobrazení historie podpory žadatelů z archivních záznamů'),
            self::SUBSIDY_HISTORY_NOT_REQUESTED => __('Nepožadovat po žadateli žádné informace o přijaté veřejné podpoře'),

            self::PREVIEW_HIDE_SENDMAIL_IN_REQUEST_DETAIL => __('Skrýt možnost "Odeslat žadateli e-mail" pro uživatele, který má povolenu pouze roli "Mé tymy / Náhled"'),
            self::APPEALS_OPEN_TO_WITH_TIME => __('Nastavovat termín ukončení příjmu žádostí včetně času'),
            self::ALLOW_NOTIFICATIONS => __('Povolit žadatelům ohlašování událostí (činnosti / akcí / dokončení děl)'),
            self::ALLOW_NOTIFICATIONS_ALERTS => __('Povolit zasílání notifikací o nových ohlášeních hodnotitelům emailem'),
            self::ALLOW_MERGING_OF_REQUESTS => __('Zobrazovat v programech možnost propojení dvou žádostí'),

            self::ECONOMICS_TEXT_SETTLEMENT_RETURN_INSTRUCTIONS => __('Instrukce pro žadatele, při vyúčtování dotace, o vrácení nevyčerpané části poskytnutých peněz'),
            self::ECONOMICS_TEXT_RETURN_FULL_SUBSIDY => __('Instrukce pro žadatele, který vrací podporu v celé výši'),
            self::ECONOMICS_TEXT_SUBMIT_CONSENT_WITH_PUBLISH => __('Upozornění / souhlas se zveřejněním vyúčtování v otevřených datech'),
            self::ECONOMICS_AMOUNT_NOT_REQUIRED_TO_BE_SETTLED => __('Částka, kterou úspěšný žadatel nemusí vyúčtovat'),
            self::ECONOMICS_TEXT_WAIVED_AMOUNT_EXPLANATION => __('Vysvětlení k možnosti nevyúčtovat celou částku (v PDF vyúčtování popis "částky nevyúčtované")'),
            self::ECONOMICS_MAX_NUMBER_OF_OPTIONAL_ATTACHMENTS_TO_SETTLEMENT => __('Maximální počet nepovinných příloh k položce vyúčtování dotace'),
            self::ECONOMICS_DISABLE_OPTION_FOR_ALT_SEND => __('Zákaz možnosti "Odeslat vyúčtování jinak"'),
            self::ECONOMICS_ADDITIONAL_FINANCING => __('Povolit možnost dofinancování - navýšení původně schválené podpory'),
            self::ECONOMICS_ADDITIONAL_COLUMNS => __('Přílohy vyúčtování včetně sloupců "Hrazeno z vlastních zdrojů" a "Částka, která není k dotaci vůbec uplatňována"'),
            self::ECONOMICS_EXPENSES_TOTAL_FIELD => __('Zobrazit ve vyúčtování pole "Celková výše nákladů na projekt", ze kterého budou počítány celkové náklady vyúčtování'),

            self::SHOW_RATINGS_TEAM_PROPOSALS => __('Zobrazit jednotlivá hodnocení pro tým Navrhovatelů'),
            self::SHOW_RATINGS_TEAM_APPROVALS => __('Zobrazit jednotlivá hodnocení pro tým Schvalovatelů'),
            self::SHOW_RATINGS_SHOW_PRIVATE_COMMENTS => __('Zobrazit soukromé komentáře hodnotitelů spolu s jednotlivými hodnoceními'),
            self::RATINGS_AMOUNT_FROM_CRITERIA_SUM => __('V sekci Navrhovatelé je výsledná částka podpory vypočtená na základě bodového výsledku'),

            self::REPLY_TO_ADMIN_WHEN_NOTIFYING_USER => __('Při odesílání e-mailu žadateli uvést v Reply-To (adresu pro odpověď) e-mail uživatele, který provedl akci (např. vrácení žádosti ve formální kontrole nebo upozornění k vyúčtování)'),

            self::EAGRI_RDM_TEST_ENVIRONMENT => __('Použít testovací prostředí eAgri RDM (eagritest.cz) ?'),

            self::SITE_FOOTER_HIDE_ABOUT => __('V zápatí nezobrazovat odkaz na stránku "O projektu"'),
            self::SITE_HP_HIDE_GRANT_FUNDS => __('Na úvodní stránce nezobrazovat sekci "Dotační fond"'),

            self::EXPORT_SHOW_PDF_HEADER => __('Export žádosti do PDF - zobrazit s hlavičkou'),
            self::EXPORT_ADDRESS_FOR_PDF_HEADER => __('Export žádosti do PDF - adresa pro hlavičku'),
            self::EXPORT_MESSAGE_FOR_PDF_HEADER => __('Export žádosti do PDF - text pod hlavičkou'),

            self::ALLOW_ADDITIONAL_FORM_CHANGES => __('Jsou povoleny dodatečné změny formulářů i v případě odeslaných žádostí'),

            self::EMAIL_MESSAGE_NEW_REQUEST => __('Povolit notifikace úřadu, když přijde nová žádost'),
            self::EMAIL_MESSAGE_NEW_SETTLEMENT => __('Povolit notifikace úřadu, když přijde nové vyúčtování'),
            self::EMAIL_MESSAGE_UPDATED_REQUEST => __('Povolit notifikace úřadu, když přijde opravená žádost'),
            self::EMAIL_MESSAGE_UPDATED_SETTLEMENT => __('Povolit notifikace úřadu, když přijde opravené vyúčtování'),
        ];
    }

    /**
     * @param string $field whether the setting is app global, or portal specific
     * @return bool
     */
    public static function isAppGlobalSetting(string $field): bool
    {
        foreach (self::APP_GLOBAL_SETTINGS_GROUPS as $group) {
            if (in_array($field, $group)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $field string id of OrganizationSetting, see constants
     * @return array|false[]|mixed|null
     */
    public static function getDefaultValue(string $field)
    {
        if (self::isAppGlobalSetting($field)) {
            return Configure::read($field);
        }
        if (in_array($field, array_keys(self::DEFAULT_VALUES))) {
            return self::DEFAULT_VALUES[$field];
        }

        return null;
    }

    /**
     * @param string $separator separator of contact fields
     * @param bool $links make links active?
     * @param bool $address_plain strip html tags from address (value is provided from WYSIWYG editor)
     * @return string
     */
    public static function getContactInfo(string $separator = " - ", bool $links = false, bool $address_plain = false): string
    {
        $mail = self::getSetting(self::CONTACT_EMAIL);
        $phone = self::getSetting(self::CONTACT_PHONE);
        $address = self::getSetting(self::CONTACT_ADDRESS);

        if (empty($mail) && empty($phone) && empty($address)) {
            return "";
        }

        $mail = strip_tags(trim($mail));
        $phone = strip_tags(trim($phone));
        $address = trim($address_plain ? strip_tags($address, '<a>') : $address_plain);

        if ($links) {
            if (mb_strlen($phone) > 0) {
                $phone = '<a href="tel:' . str_replace(' ', '', $phone) . '">' . $phone . '</a>';
            }
            if (mb_strlen($mail) > 0) {
                $mail = '<a href="mailto:' . $mail . '">' . $mail . '</a>';
            }
        }

        return sprintf("%s%s%s%s%s", $mail, mb_strlen($mail) > 0 ? $separator : '', $phone, mb_strlen($phone) > 0 ? $separator : '', $address);
    }

    /**
     * @return void
     */
    public function storeConfiguration(): void
    {
        Configure::write(sprintf('%s%s', self::isAppGlobalSetting($this->name) ? '' : 'Organization.', $this->name), $this->deserialize());
    }

    /**
     * get current setting value
     *
     * @param string $name setting id, see constants
     * @param bool $returnDefaultValueIfNotConfigured
     * @return mixed
     */
    public static function getSetting(string $name, bool $returnDefaultValueIfNotConfigured = false)
    {
        $configured = Configure::read(sprintf('%s%s', self::isAppGlobalSetting($name) ? '' : 'Organization.', $name));
        return ($configured === null && $returnDefaultValueIfNotConfigured) ? self::getDefaultValue($name) : $configured;
    }

    /**
     * @param string $field setting id, see constants
     * @return string|null
     */
    public static function getSettingLabel(string $field): ?string
    {
        if (in_array($field, array_keys(self::getLabels()))) {
            return self::getLabels()[$field];
        }

        return null;
    }

    /**
     * @return bool|float|int|string
     */
    public function deserialize()
    {
        return self::deserializeValue($this->name, $this->value);
    }

    /**
     * @param string $field setting id
     * @param string $value serialized, string, value
     * @return bool|float|int|string
     */
    public static function deserializeValue(string $field, string $value)
    {
        if (in_array($field, self::BOOLEANS)) {
            return boolval($value);
        }

        if (in_array($field, self::INTEGERS)) {
            return intval($value);
        }

        if (in_array($field, self::DOUBLES)) {
            return floatval($value);
        }

        return $value;
    }

    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingDataType(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checkbox';
        }

        if (in_array($field, self::INTEGERS)) {
            return 'integer';
        }

        if (in_array($field, self::DOUBLES)) {
            return 'decimal';
        }

        if (in_array($field, self::EMAILS)) {
            return 'email';
        }

        if (in_array($field, self::TEXTEAREAS)) {
            return 'textarea';
        }

        return 'string';
    }

    /**
     * @param string $field setting id, see constants
     * @return array|string[]
     */
    public static function getExtraFormControlOptions(string $field, int $organization_id = null): array
    {
        if (in_array($field, [self::DS_CERTIFICATE, self::DS_CERTKEY, self::CUSTOM_CSS, self::CUSTOM_JS])) {
            return ['data-noquilljs' => 'data-noquilljs'];
        }

        if ($field == self::ENABLE_LOGIN_NIA) {
            $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http";
            $domain = $_SERVER['HTTP_HOST'];
            $url = $protocol . "://" . $domain . '/nia/' . $organization_id . '/login';
            $headers = get_headers($url);
            $response_code = substr($headers[0], 9, 1);
            // Check if NIA is set up correctly (correct cert and key deployed under
            // /var/lib/nodeapps/nia-auth-server/conf/). We should get a redirect.
            if ($response_code != 3) {
                return ['disabled' => 'disabled']; // Disable this setting if not
            }
        }
        return [];
    }


    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingFormOptionName(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checked';
        }

        return 'value';
    }

    /**
     * @return string
     */
    public static function getFaviconFullPath(): string
    {
        $favicon_path = self::getSetting(self::SITE_FAVICON) ?: 'default.ico';
        $is_full_url = isset(parse_url($favicon_path)['scheme']);

        return $is_full_url ? $favicon_path : (new UrlHelper(new View()))->image($favicon_path, ['fullBase' => true]);
    }

    public static function getAgendioPresetKey(int $agendio_report_id, string $agendio_column_path): string
    {
        return sprintf(self::AGENDIO_PRESET_TEMPLATE, $agendio_report_id, $agendio_column_path);
    }


    //TODO: Unfortunately for now we define these exceptions - shopuld be moved to "organisation supersettings"
    public const P4 = 14;
    public const P3 = 1;
    public const USTI = 13;


    public static function isException(int $organization_id)
    {

        $currentID = OrgDomainsMiddleware::getCurrentOrganizationId();
        if ($organization_id == $currentID) return true;

        // Additional check to see if we are operating in the testing environment attached to the $organization_id
        $organizationsTable = TableRegistry::getTableLocator()->get('Organizations');
        $organizations = $organizationsTable->find('all')
            ->where([
                'id' => $organization_id,
                'test_id' => $currentID
            ])
            ->toArray();
        return (!empty($organizations));
    }

    /**
     * Find the correct binary path using 'which' command
     *
     * @param mixed $binary_name
     *
     * @return [type]
     */

    public static function getBinaryPath($binary_name)
    {
        $output = array();
        $return_var = 0;
        exec("which $binary_name", $output, $return_var);
        $binary_path = $output[0] ?? false;
        if (!$binary_path || !is_executable($binary_path)) {
            return false;
            //return '/run/current-system/sw/bin/openssl';
            //throw new Exception(sprintf('openssl binary is not found or not executable: %s', $binary));
        }
        return $binary_path;
    }

    public static function extractCertificateInfo($certificate)
    {
        $out = ['err_msg' => null];
        if (empty(trim($certificate))) {
            $out['err_msg'] = 'No certificate present';
            return $out;
        }
        $binary_path = OrganizationSetting::getBinaryPath('openssl');
        if (!$binary_path) {
            $out = ['err_msg' => 'openssl binary not found!'];
            return $out;
        }

        // Command to get the issuer of the certificate
        $cmd = $binary_path . ' x509 -noout -issuer -subject -enddate -startdate -serial';
        $cmd_result = OrganizationSetting::runCommandWithSTDIN($cmd, $certificate);
        if ($cmd_result['err_msg'] != null) {
            $out['err_msg'] = $cmd_result['err_msg'];
            return $out;
        }

        $result = $cmd_result['output'];


        $patterns = [
            'issuer_cn' => '/issuer=.*?CN\s*=\s*([^,\n]+)/',
            'subject_cn' => '/subject=.*?CN\s*=\s*([^,\n]+)/',
            'notAfter' => '/notAfter=([A-Za-z0-9\s:]+)\sGMT/',
            'notBefore' => '/notBefore=([A-Za-z0-9\s:]+)\sGMT/',
            'serial' => '/serial=([A-Z0-9]+)/'
        ];

        // Extract data using each pattern
        foreach ($patterns as $key => $pattern) {
            preg_match($pattern, $result, $matches);
            // Assign the matched value to the corresponding key in the $data array
            if (array_key_exists(1, $matches)) {
                $out[$key] = OrganizationSetting::decodeHexToUtf8($matches[1]) ?? null;
            } else {
                $out[$key] = null;
            }
        }

        $out['notBefore'] = strtotime($out['notBefore']);
        $out['notAfter'] = strtotime($out['notAfter']);
        return $out;
    }

    /**
     * Check if the given key corresponds to the given certificate
     *
     * @param mixed $private_key
     * @param mixed $certificate
     *
     * @return bool
     */

    public static function compareKeyVSCert($private_key, $certificate): bool
    {
        //openssl x509 -in certificate.pem -noout -modulus | openssl md5
        //openssl rsa -in privatekey.pem -noout -modulus | openssl md5
        $binary_path = OrganizationSetting::getBinaryPath('openssl');
        if (!$binary_path) {
            return false; // TODO: Should throw an error message
        }
        $cmd1 = $binary_path . ' x509 -noout -modulus | ' . $binary_path . ' md5';
        $cmd1_result = OrganizationSetting::runCommandWithSTDIN($cmd1, $certificate);
        $cmd2 = $binary_path . ' rsa -noout -modulus | ' . $binary_path . ' md5';
        $cmd2_result = OrganizationSetting::runCommandWithSTDIN($cmd2, $private_key);
        return ($cmd1_result == $cmd2_result);
    }

    private static function decodeHexToUtf8($input)
    {
        // Regular expression to match the \C3\A1 format
        $regex = '/\\\\([0-9A-Fa-f]{2})/';

        // Callback function to convert hex to bytes and then to UTF-8
        $callback = function ($matches) {
            // Convert the hexadecimal number to a decimal
            $byte = hexdec($matches[1]);
            // Convert the byte to a character using chr
            $char = chr($byte);
            // Return the character
            return $char;
        };

        // Decode the string by replacing each escape sequence
        $decodedString = preg_replace_callback($regex, $callback, $input);

        // Ensure the resulting string is treated as UTF-8
        return mb_convert_encoding($decodedString, 'UTF-8', 'UTF-8');
    }

    private static function runCommandWithSTDIN($cmd, $input)
    {
        $out['err_msg'] = null;
        // Descriptor array: stdin, stdout, stderr
        $descriptorspec = [
            0 => ["pipe", "r"],  // stdin is a pipe that the child will read from
            1 => ["pipe", "w"],  // stdout is a pipe that the child will write to
            2 => ["pipe", "w"]   // stderr is a pipe that the child will write to
        ];

        // Open the process
        $process = proc_open($cmd, $descriptorspec, $pipes);

        if (is_resource($process)) {
            // Pass the certificate to the command via stdin
            fwrite($pipes[0], $input);
            fclose($pipes[0]);

            // Read the command's output
            $out['output'] = stream_get_contents($pipes[1]);
            fclose($pipes[1]);

            // (Optional) Read from stderr if you need to check for errors
            $errorOutput = stream_get_contents($pipes[2]);
            fclose($pipes[2]);

            // Close the process
            $out['return_value'] = proc_close($process);

            // Output the result
            if ($errorOutput) {
                $out['err_msg'] = "Error: " . $errorOutput;
            }
        } else {
            $out['err_msg'] = 'Failed to launch the openssl process';
        }
        return $out;
    }
}
