<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * SettlementItemsToSettlementAttachment Entity
 *
 * @property int $id
 * @property int $settlement_item_id
 * @property int $settlement_attachment_id
 * @property FrozenTime|null $created
 *
 * @property SettlementItem $settlement_item
 * @property SettlementAttachment $settlement_attachment
 */
class SettlementItemsToSettlementAttachment extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'settlement_item_id' => true,
        'settlement_attachment_id' => true,
        'created' => true,
        'settlement_item' => true,
        'settlement_attachment' => true,
    ];
}
