<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use OldDsw\Model\Entity\Zadost;

/**
 * SettlementsToOlddswRequest Entity
 *
 * @property int $id
 * @property int $settlement_id
 * @property int $zadost_id
 * @property FrozenTime|null $modified
 *
 * @property Settlement $settlement
 * @property Zadost $zadosti
 */
class SettlementsToOlddswRequest extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'settlement_id' => true,
        'zadost_id' => true,
        'modified' => true,
        'settlement' => true,
        'zadosti' => true,
    ];
}
