<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * RequestsToSettlement Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $settlement_id
 * @property FrozenTime|null $modified
 *
 * @property Request $request
 * @property Settlement $settlement
 */
class RequestsToSettlement extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'settlement_id' => true,
        'modified' => true,
        'request' => true,
        'settlement' => true,
    ];
}
