<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * Report Entity
 *
 * @property int $id
 * @property string $name
 * @property int $organization_id
 * @property bool $is_archived
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 * @property ReportColumn[] $report_columns
 */
class Report extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'organization_id' => true,
        'is_archived' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
        'report_columns' => true,
    ];

    public function getFirstAgendioColumn(AgendioColumn $column): ?ReportColumn
    {
        foreach ($this->report_columns ?? [] as $this_column) {
            if ($this_column->agendio_column_id === $column->id) {
                return $this_column;
            }
        }
        return null;
    }
}
