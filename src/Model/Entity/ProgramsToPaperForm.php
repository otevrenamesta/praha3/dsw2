<?php
namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * ProgramsToPaperForm Entity
 *
 * @property int $id
 * @property int $program_id
 * @property int $form_id
 * @property FrozenTime|null $modified
 *
 * @property Program $program
 * @property Form $form
 */
class ProgramsToPaperForm extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'program_id' => true,
        'form_id' => true,
        'modified' => true,
        'program' => true,
        'form' => true,
    ];
}
