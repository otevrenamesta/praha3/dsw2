<?php

namespace App\Model\Entity;

use App\Budget\ProjectBudgetFactory;
use App\Model\Entity\Request;
use App\Model\Table\AppealsToProgramsTable;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;
use Exception;
use OldDsw\Model\Entity\Zadost;

/**
 * Settlement Entity
 *
 * @property int $id
 * @property int $settlement_state_id
 * @property int $settlement_type_id
 * @property null|string $econom_statement Economics formal control comment (reason for requesting changes)
 * @property null|string $rejected_statement
 * @property null|string $agreement_no
 * @property null|string $return_reason User reasoning, why the subsidy is being returned in full
 * @property FrozenDate|null $repair_date
 * @property FrozenTime|null $submitted
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int|null $additional_financing
 * @property null|string $additional_comment
 * @property int|null $settlement_date_email
 * @property float|null $expenses_total
 * @property float|null $income_total
 *
 * @property SettlementState $settlement_state
 * @property RequestsToSettlement[] $requests_to_settlements
 * @property SettlementAttachment[] $settlement_attachments
 * @property SettlementItem[] $settlement_items
 * @property SettlementsToOlddswRequest[] $settlements_to_olddsw_requests
 * @property SettlementLog[] $settlement_logs
 */
class Settlement extends AppEntity
{
    use LocatorAwareTrait;

    public const FIELD_SETTLEMENT_STATE_ID = 'settlement_state_id';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'modified' => true,
        'created' => true,
        'submitted' => true,
        'requests_to_settlements' => true,
        'settlement_attachments' => true,
        'settlement_items' => true,
        'settlements_to_olddsw_requests' => true,
        'econom_statement' => true,
        'rejected_statement' => true,
        'user_comment' => true,
        'agreement_no' => true,
        'organization_id' => true,
        'settlement_type_id' => true,
        'return_reason' => true,
        'repair_date' => true,
        'settlement_logs' => true,
        'published' => true,
        'anynonym_mask' => true,
        'additional_financing' => true,
        'additional_comment' => true,
        'settlement_date_email' => true,
        'expenses_total' => true,
        'income_total' => true,
    ];

    /**
     * @return Zadost|Request
     * @throws Exception
     */
    public function getRequest(): EntityInterface
    {
        foreach ($this->requests_to_settlements as $newLink) {
            return $newLink->request;
        }
        if (OrganizationSetting::getSetting(OrganizationSetting::HAS_DSW)) {
            foreach ($this->settlements_to_olddsw_requests as $oldLink) {
                return $oldLink->zadosti;
            }
        }
        throw new Exception('Unattached Settlement id ' . $this->id);
    }

    public function getReferenceNumber(): ?string
    {
        $request = $this->getRequest();
        if ($request instanceof Request) {
            return $request->reference_number;
        }
        return null;
    }

    public function getRequestName(): ?string
    {
        $request = $this->getRequest();
        if ($request instanceof Request) {
            return $request->name;
        } elseif ($request instanceof Zadost) {
            return $request->nazev;
        }
        return null;
    }

    public function getRequestFinalAmount(): float
    {
        $request = $this->getRequest();
        if ($request instanceof Request) {
            return 0 + $request->final_subsidy_amount;
        } elseif ($request instanceof Zadost) {
            return 0 + $request->konecna_castka;
        }
        return 0;
    }

    public function getRequesterContactEmails(): array
    {
        $rtn = [];
        $request = $this->getRequest();
        if ($request instanceof Zadost) {
            if (!empty($request->ucet)) {
                $rtn[] = $request->ucet->email;
            }
        } elseif ($request instanceof Request) {
            /*TODO: expand on all contact email addresses*/
            $identity = $request->getFlatIdentities();
            if (array_key_exists(Identity::CONTACT_EMAIL, $identity)) {
                $rtn[] = $identity[Identity::CONTACT_EMAIL];
            }
        }
        return array_filter($rtn);
    }

    public function getRequesterName(bool $withEmail = true): string
    {
        $request = $this->getRequest();
        $name = "";
        if ($request instanceof Request) {
            $flatIdentity = $request->getFlatIdentities();
            $name .= Identity::getIdentityFullName($flatIdentity);
            if ($withEmail) {
                $emails = $this->getRequesterContactEmails();
                if (!empty($emails)) {
                    $name .= ', ' . reset($emails);
                }
            }
        } elseif ($request instanceof Zadost) {
            if ($request->ucet) {
                $name .= $request->ucet->nazev;
                if ($withEmail) {
                    $name .= ', ' . $request->ucet->email;
                }
            }
        }
        return $name;
    }

    /**
     * @return bool
     */
    public function validateAllItemsAttachments(): bool
    {
        if (empty($this->settlement_items)) {
            return false;
        }
        foreach ($this->settlement_items as $settlement_item) {
            $hasInvoice = $this->getAttachmentsByItem($settlement_item->id, SettlementAttachmentType::TYPE_1_INVOICES);
            $hasProof = $this->getAttachmentsByItem($settlement_item->id, SettlementAttachmentType::TYPE_2_PROOF);
            if (!$hasInvoice || !$hasProof) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param int $item_id
     * @param array $whitelistAttachmentTypes
     * @return SettlementAttachment[]
     */
    public function getAttachmentsByItem(int $item_id, array $whitelistAttachmentTypes = []): array
    {
        $attachments = [];
        foreach ($this->settlement_attachments ?? [] as $attachment) {
            if (empty($whitelistAttachmentTypes) || in_array($attachment->settlement_attachment_type_id, $whitelistAttachmentTypes, true)) {
                foreach ($attachment->settlement_items as $settlement_item) {
                    if ($settlement_item->id === $item_id) {
                        $attachments[] = $attachment;
                    }
                }
            }
        }
        return $attachments;
    }

    /**
     * @return SettlementAttachment|null
     */
    public function getFinalReport(): ?SettlementAttachment
    {
        $attachments = $this->getAttachmentsByType([SettlementAttachmentType::TYPE_FINAL_REPORT, SettlementAttachmentType::TYPE_FINAL_REPORT_WITH_PUBLICITY]);
        return empty($attachments) ? null : reset($attachments);
    }

    /**
     * @param int[] $whitelistedAttachmentTypes
     * @param bool $returnIdNameArray
     * @return SettlementAttachment[]
     */
    public function getAttachmentsByType(array $whitelistedAttachmentTypes, bool $returnIdNameArray = false): iterable
    {
        if (empty($whitelistedAttachmentTypes)) {
            return $this->settlement_attachments;
        }
        $attachments = [];
        foreach ($this->settlement_attachments as $attachment) {
            if (in_array($attachment->settlement_attachment_type_id, $whitelistedAttachmentTypes, true)) {
                $attachments[$attachment->id] = $returnIdNameArray ? $attachment->getLabel() : $attachment;
            }
        }
        return $attachments;
    }

    /**
     * @return float
     */
    public function getItemsSum(): float
    {
        $sum = 0;
        foreach ($this->settlement_items as $settlement_item) {
            $sum += $settlement_item->amount;
        }
        return $sum;
    }

    /**
     * @param int $item_id
     * @param bool $throwIfNotFound
     * @return SettlementItem|null
     */
    public function getItemById(int $item_id, bool $throwIfNotFound): ?SettlementItem
    {
        foreach ($this->settlement_items as $settlement_item) {
            if ($settlement_item->id === $item_id) {
                return $settlement_item;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException();
        }
        return null;
    }

    /**
     * @return SettlementAttachment[]
     */
    public function sortedAttachments(): iterable
    {
        usort($this->settlement_attachments, function (SettlementAttachment $a, SettlementAttachment $b) {
            return $a->id - $b->id;
        });
        return $this->settlement_attachments;
    }

    /**
     * @param int $attachment_id
     * @param bool $throwIfNotFound
     * @return SettlementAttachment|null
     */
    public function getAttachmentsById(int $attachment_id, bool $throwIfNotFound = false): ?SettlementAttachment
    {
        foreach ($this->settlement_attachments as $attachment) {
            if ($attachment->id === $attachment_id) {
                return $attachment;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException();
        }
        return null;
    }

    /**
     * @param int $file_id
     * @param bool $throwIfNotFound
     * @return File|null
     */
    public function getFileById(int $file_id, bool $throwIfNotFound = false): ?File
    {
        foreach ($this->settlement_attachments as $attachment) {
            if ($attachment->file->id === $file_id) {
                return $attachment->file;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException();
        }
        return null;
    }

    public function canUserEdit(): bool
    {
        if (!empty($this->repair_date) && $this->repair_date instanceof FrozenDate) {
            return $this->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES && FrozenTime::now()->lessThan(new FrozenTime($this->repair_date->addDays(1)->format('Y-m-d')));
        }

        return SettlementState::canUserEdit($this->settlement_state_id);
    }

    public function getFirstAttachmentByType(array $whitelistAttachmentTypes = [], ?int $settlement_item_id = null): ?SettlementAttachment
    {
        foreach ($this->settlement_attachments as $attachment) {
            if (in_array($attachment->settlement_attachment_type_id, $whitelistAttachmentTypes, true)) {
                if ($settlement_item_id < 1) {
                    return $attachment;
                }
                foreach ($attachment->settlement_items as $item) {
                    if ($item->id === $settlement_item_id) {
                        return $attachment;
                    }
                }
            }
        }
        return null;
    }

    public function getAllAttachments(bool $simpleList = false, ?SettlementItem $excludeByItem = null): array
    {
        if (!$simpleList) {
            return $this->settlement_attachments ?? [];
        }
        $list = [];
        foreach ($this->settlement_attachments as $attachment) {
            if ($excludeByItem) {
                foreach ($attachment->settlement_items as $item) {
                    if ($item->id === $excludeByItem->id) {
                        continue 2;
                    }
                }
            }
            $list[$attachment->id] = $attachment->getLabel();
        }
        return $list;
    }

    public function isSettlementOwner(User $user)
    {
        $request = $this->getRequest();
        if ($request instanceof Request) {
            return $request->user_id === $user->id;
        }
        return in_array($request->ucet_id, $user->getStareUctyIds(), true);
    }

    public function getWaivedAmount(): int
    {
        return intval(OrganizationSetting::getSetting(OrganizationSetting::ECONOMICS_AMOUNT_NOT_REQUIRED_TO_BE_SETTLED));
    }

    public function isRequestWaivedCompletely(): bool
    {
        $waivedAmount = $this->getWaivedAmount();
        return $waivedAmount > 0 && $waivedAmount >= $this->getRequestFinalAmount() && empty($this->settlement_items);
    }

    public function getValidationsReduced(): bool
    {
        return array_reduce($this->getValidations(), function ($carry, $item) {
            return $carry && $item;
        }, true);
    }

    public function getValidations(): array
    {
        $requestIsWaivedCompletely = $this->isRequestWaivedCompletely();
        if ($this->settlement_type_id === SettlementType::FULL_SUBSIDY_RETURN) {
            return [
                true,
                true,
                true,
                true,
                !empty($this->agreement_no),
                true,
            ];
        }

        return [
            // has at least one item
            (boolval($this->notRequireAttachments) ? true : ($requestIsWaivedCompletely || !empty($this->settlement_items))),
            // all items has required attachments
            (boolval($this->notRequireAttachments) ? true : ($requestIsWaivedCompletely || $this->validateAllItemsAttachments())),
            // has final report
            !empty($this->getFinalReport()),
            // has at least one proof of publicity
            !empty($this->getAttachmentsByType(SettlementAttachmentType::TYPE_4_PUBLICITY)),
            // public agreement number is set
            !empty($this->agreement_no),
            // has no unused attachments
            true // old: empty($this->getUnusedAttachments()), new: always true
        ];
    }

    /**
     * @param SettlementAttachment[] $attachments
     * @return string
     */
    public function printFormatAttachments(array $attachments = [])
    {
        $rtn = '';
        foreach ($attachments as $attachment) {
            $rtn .= sprintf("%s%d - %s<br/>", __('Příloha č.'), $attachment->file_id, $attachment->settlement_attachment_type->getShortTypeName());
        }
        return $rtn;
    }

    public function canUserGetPdf(): bool
    {
        # If organization allows submitting PDFs manually and the settlement is completed
        # or if the settlement is already submitted (user cannot edit)
        return (OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, true) && $this->settlement_state_id === SettlementState::STATE_FILLED_COMPLETELY)
            || !$this->canUserEdit();
    }

    public function canUserSubmitSettlement(User $user): bool
    {
        $isRequestOwner = false;
        $request = $this->getRequest();
        if ($request instanceof Request) {
            $isRequestOwner = ($request->user_id === $user->id);
        } elseif ($request instanceof Zadost) {
            $isRequestOwner = in_array($request->ucet_id, $user->getStareUctyIds(), true);
        }

        return $isRequestOwner &&
            in_array($this->settlement_state_id, [SettlementState::STATE_FILLED_COMPLETELY, SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES]) &&
            $this->canUserEdit();
    }

    public function fixItemsOrderNumber(): self
    {
        $counter = 1;
        usort($this->settlement_items, function (SettlementItem $a, SettlementItem $b) {
            return $a->id - $b->id;
        });
        foreach ($this->settlement_items ?? [] as $settlementItem) {
            if ($settlementItem->order_number !== $counter) {
                $settlementItem->set('order_number', $counter);
                $settlementItem->setDirty('order_number');
                $this->setDirty('settlement_items');
            }
            $counter++;
        }
        return $this;
    }

    /**
     * @return SettlementAttachment[]
     */
    public function getUnusedAttachments(): array
    {
        $rtn = [];
        foreach ($this->settlement_attachments ?? [] as $attachment) {
            if (!$attachment->isUsed($this)) {
                $rtn[$attachment->id] = $attachment;
            }
        }
        return $rtn;
    }

    public function loadLogs(): self
    {
        // null => not loaded, [] => loaded and empty
        if ($this->settlement_logs === null) {
            $requestsTable = $this->getTableLocator()->get('Settlements');
            $requestsTable->loadInto($this, ['SettlementLogs.Users']);
        }

        if (is_array($this->settlement_logs)) {
            usort($this->settlement_logs, function (SettlementLog $a, SettlementLog $b) {
                return $b->created->timestamp - $a->created->timestamp;
            });
        }

        return $this;
    }

    private function addLog(?int $executed_by_user_id = null, bool $force = false): bool
    {
        if (!$force && !$this->isDirty() && $this->settlement_state_id === $this->getOriginal('settlement_state_id')) {
            // do not add log if nothing changed or it's not forced by function call
            return false;
        }
        $this->loadLogs();
        /** @var SettlementLog $newLog */
        $newLog = $this->getTableLocator()->get('SettlementLogs')->newEntity();
        // record current state in the log
        $newLog->settlement_state_id = $this->settlement_state_id;
        $newLog->settlement_id = $this->id;
        $newLog->economics_statement = $this->econom_statement;
        $newLog->executed_by_user_id = $executed_by_user_id;
        $newLog->return_reason = $this->return_reason;
        // add and enforce association to be persisted
        $this->settlement_logs[] = $newLog;
        $this->setDirty('settlement_logs');

        return true;
    }

    public function canTransitionTo(int $new_request_state_id): bool
    {
        $original_state_id = $this->getOriginal(self::FIELD_SETTLEMENT_STATE_ID);
        if ($new_request_state_id === $original_state_id) {
            // no transition here, allow updating other fields if request state does not change
            return true;
        }

        return in_array($new_request_state_id, SettlementState::ALLOWED_TRANSITIONS[$original_state_id] ?? [], true);
    }

    public function setCurrentStatus(int $new_state_id, ?int $currentUserId = null, bool $setError = true): bool
    {
        if ($new_state_id === $this->settlement_state_id) {
            // ignore duplicate status changes
            return true;
        }

        // check if new state is known
        if (!in_array($new_state_id, SettlementState::KNOWN_STATES, true)) {
            $this->addError(self::FIELD_SETTLEMENT_STATE_ID, 'Nový stav vyúčtování není znám', $setError);

            return false;
        }

        // check if the transition is allowed from original persisted state
        if (!$this->canTransitionTo($new_state_id)) {
            $this->addError(self::FIELD_SETTLEMENT_STATE_ID, 'Toto není povolený stav, do kterého žádost může aktuálně přejít', $setError);

            return false;
        }

        // make transition
        if ($new_state_id === SettlementState::STATE_SUBMITTED_READY_TO_REVIEW) {
            $this->submitted = time();
            $this->econom_statement = null;
        }
        $this->settlement_state_id = $new_state_id;

        // record the transition consequences in logs
        $this->addLog($currentUserId);

        return true;
    }

    private function addError(string $field, string $error, bool $reallySetError = true)
    {
        if ($reallySetError) {
            $this->setError($field, $error);
        }
    }

    public function canUserDelete(): bool
    {
        return SettlementState::canUserDelete($this->settlement_state_id);
    }

    public static function budgetInSettlement(Program $program, Settlement $settlement)
    {
        // Exception for allowed budget Usti/V15
        $allowedBudgets = [ProjectBudgetDesign::DESIGN_USTI_V15];
        if (!empty($program->project_budget_design_id) && !in_array($program->project_budget_design_id, $allowedBudgets)) {
            return [];
        }

        // Budget settings
        $request = $settlement->getRequest();
        $request->loadBudget();
        $request->loadAppeal();

        $appealsToPrograms = new AppealsToProgramsTable();
        $request->appeal->appeals_to_programs = $appealsToPrograms->find(
            'all',
            [
                'conditions' => [
                    'AppealsToPrograms.appeal_id' => $request->appeal->id,
                    'AppealsToPrograms.program_id' => $request->program_id,
                ]
            ]
        )->first();
        $maxSupport = !empty($request->appeal->appeals_to_programs) ? $request->appeal->appeals_to_programs['max_support'] : 100;

        $budgetRequest = $request->request_budget;
        $budgetRequest['max_'] = $maxSupport;

        $budgetColumns = array_reverse(RequestBudget::getReportColumns());
        unset($budgetColumns['total_other_subsidy'], $budgetColumns['total_own_sources'], $budgetColumns['total_costs']);
        $budgetColumns['requested_amount'] = __('Poskytnutá dotace');
        $budgetColumns['sum_'] = __('Součet skutečně vyúčtovaných nákladů');
        $budgetColumns['max_'] = __('Maximální podpora (%)');
        $budgetColumns['diff_'] = __('Podíl dotace na celkových nákladech (%)');

        $budgetData = !empty($request->request_budget->extra_data) ? unserialize($request->request_budget->extra_data) : [];
        $budgetSettings = ProjectBudgetFactory::getBudgetHandler($program->project_budget_design_id);
        $budgetSections = $budgetSettings->definitions['SECTIONS'] ?? [];
        $budgetExceededPartial = false;

        // List of main budget sections
        $budgetSectionsData = [];
        $budgetSectionsDataFull = [];
        $budgetSectionsTitles = [];
        $cardopen = $savebutton = '';
        foreach ($budgetSections as $key => $value) {
            $savebutton = isset($value['savebutton']) && !empty($value['savebutton']) ? $value['savebutton'] : $savebutton;
            $cardopen = isset($value['cardopen']) && !empty($value['cardopen']) ? $value['cardopen'] : $cardopen;
            $title = isset($value['title']) && !empty($value['title']) ? $value['title'] : null;
            $cardopenTitle = (isset($value['cardopen']) && !empty($value['cardopen'])) || $title ? implode(' - ', !$value['title'] ? [$cardopen] : (!$cardopen ? [$title] : [$cardopen, $title])) : null;
            if ($savebutton) {
                continue;
            }
            if (isset($value['cardclose']) && boolval($value['cardclose'])) {
                $cardopen = '';
            }
            if ($cardopenTitle) {
                $budgetSectionsTitles[$key] = substr(trim($cardopenTitle), -1) === ':' ? $cardopenTitle : $cardopenTitle . ':';
            }
        }

        // Combination of budget sections and user's data
        $budgetSectionsList = [];
        foreach ($budgetData as $key => $value) {
            if (!isset($budgetSectionsTitles[$key])) continue;

            foreach ($value as $k => $v) {
                $section = implode('|', ['section', $key, $k]);

                if (!isset($budgetSectionsData[$key])) {
                    $budgetSectionsData[$key] = [
                        'title' => $budgetSectionsTitles[$key],
                        'total1' => 0,
                        'total2' => 0,
                        'sum' => 0,
                        'diff' => 0,
                    ];
                }
                if (!isset($budgetSectionsDataFull[$key])) {
                    $budgetSectionsDataFull[$section] = [
                        'title' => sprintf('%s %s', $budgetSectionsTitles[$key], isset($v[0]) ? (string)$v[0] : ''),
                        'total1' => 0,
                        'total2' => 0,
                        'sum' => 0,
                        'diff' => 0,
                    ];
                }
                $budgetSectionsData[$key]['total1'] += isset($v[1]) ? (float)$v[1] : 0;

                // if (!isset($v[2]) || $v[2] <= 0) continue;
                $budgetSectionsData[$key]['total2'] += isset($v[2]) ? (float)$v[2] : 0;
                $budgetSectionsList[$section] = sprintf('%s %s', $budgetSectionsTitles[$key], isset($v[0]) ? (string)$v[0] : '');

                $budgetSectionsDataFull[$section]['total1'] += isset($v[1]) ? (float)$v[1] : 0;
                $budgetSectionsDataFull[$section]['total2'] += isset($v[2]) ? (float)$v[2] : 0;
            }
        }
        $budgetSectionsData = array_filter($budgetSectionsData, function ($v) {
            return $v['total2'] > 0;
        });

        $sum_request = 0;
        $sum_costs = 0;
        foreach ($settlement->settlement_items as $item) {
            if (!empty($item->budget_item)) {
                $bID = explode('|', $item->budget_item);
                if (isset($bID[1]) && isset($budgetSectionsData[$bID[1]])) {
                    $budgetSectionsData[$bID[1]]['sum'] += (float)$item->amount;
                    $budgetSectionsData[$bID[1]]['diff'] = $budgetSectionsData[$bID[1]]['total2'] - $budgetSectionsData[$bID[1]]['sum'];

                    $sum_request += $budgetSectionsData[$bID[1]]['total2'];
                    $sum_costs += (float)$item->amount;

                    $budgetSectionsDataFull[$item->budget_item]['sum'] += (float)$item->amount;
                    $budgetSectionsDataFull[$item->budget_item]['diff'] = $budgetSectionsDataFull[$item->budget_item]['total2'] - $budgetSectionsDataFull[$item->budget_item]['sum'];
                }
            }
        }
        $sum_costs = round($sum_costs, 2);

        foreach ($budgetSectionsDataFull as $value) {
            if ($value['diff'] < 0) {
                $budgetExceededPartial = false;
            }
        }

        $budgetRequest['sum_'] = $sum_costs;
        // cancelled: $budgetRequest['diff_'] = $request->request_budget->total_costs > 0 ? round($sum_costs / $request->request_budget->total_costs * 100, 1) : 0;
        // newly:
        $budgetRequest['requested_amount'] = round($request instanceof Request ? $request->getFinalSubsidyAmount() : $request->konecna_castka, 2);
        $budgetRequest['diff_'] = (float)$settlement->expenses_total > 0 ? round($sum_costs / (float)$settlement->expenses_total * 100, 2) : 0;
        $budgetExceeded = ($budgetRequest['diff_'] > $budgetRequest['max_']) || ($budgetRequest['sum_'] > $budgetRequest['requested_amount']);

        return [
            $budgetSectionsList,
            $budgetColumns,
            $budgetRequest,
            $budgetSectionsData,
            $budgetExceeded,
            $budgetExceededPartial,
            $budgetSectionsDataFull,
        ];
    }
}
