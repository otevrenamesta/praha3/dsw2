<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Exception;

/**
 * SettlementState Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Settlement[] $settlements
 */
class SettlementState extends AppEntity
{
    public const STATE_NEW = 1,
        STATE_FILLED_COMPLETELY = 2,
        STATE_SUBMITTED_READY_TO_REVIEW = 3,
        STATE_ECONOMICS_APPROVED = 4,
        STATE_ECONOMICS_REQUESTED_CHANGES = 5,
        STATE_SETTLEMENT_REJECTED = 6;

    public const KNOWN_STATES = [
        self::STATE_NEW,
        self::STATE_FILLED_COMPLETELY,
        self::STATE_SUBMITTED_READY_TO_REVIEW,
        self::STATE_ECONOMICS_REQUESTED_CHANGES,
        self::STATE_ECONOMICS_APPROVED,
        self::STATE_SETTLEMENT_REJECTED,
    ];

    public const ALLOWED_TRANSITIONS = [
        null => [
            self::STATE_NEW,
        ],
        0 => [
            self::STATE_NEW,
        ],
        self::STATE_NEW => [
            self::STATE_FILLED_COMPLETELY,
        ],
        self::STATE_FILLED_COMPLETELY => [
            self::STATE_NEW,
            self::STATE_SUBMITTED_READY_TO_REVIEW,
        ],
        self::STATE_SUBMITTED_READY_TO_REVIEW => [
            self::STATE_ECONOMICS_APPROVED,
            self::STATE_ECONOMICS_REQUESTED_CHANGES,
            self::STATE_SETTLEMENT_REJECTED,
        ],
        self::STATE_ECONOMICS_REQUESTED_CHANGES => [
            self::STATE_SUBMITTED_READY_TO_REVIEW,
            self::STATE_SETTLEMENT_REJECTED,
        ],
        self::STATE_ECONOMICS_APPROVED => [
            // for reverting previous Economics approval decision
            self::STATE_SUBMITTED_READY_TO_REVIEW,
        ],
        self::STATE_SETTLEMENT_REJECTED => [
            self::STATE_ECONOMICS_REQUESTED_CHANGES,
            // there was a request to return the "settlement" to this state
            self::STATE_SUBMITTED_READY_TO_REVIEW,
        ],
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'settlements' => true,
    ];

    public static function canUserEdit(int $settlement_state_id): bool
    {
        return in_array($settlement_state_id, [self::STATE_NEW, self::STATE_FILLED_COMPLETELY, self::STATE_ECONOMICS_REQUESTED_CHANGES], true);
    }

    public static function canUserDelete(int $settlement_state_id): bool
    {
        return in_array($settlement_state_id, [self::STATE_NEW, self::STATE_FILLED_COMPLETELY, self::STATE_ECONOMICS_REQUESTED_CHANGES]);
    }

    /**
     * @param int $settlement_state_id
     * @return string gettext string (translated according to current set locale)
     * @throws Exception in case state is not known
     */
    public static function getLabel(int $settlement_state_id, bool $style = false): string
    {
        switch ($settlement_state_id) {
            case self::STATE_NEW:
                return ($style ? '<span class="text-warning">' : '') .
                    __('Rozpracované') .
                    ($style ? '</span>' : '');
            case self::STATE_FILLED_COMPLETELY:
                return ($style ? '<span class="text-secondary">' : '') .
                    __('Kompletně vyplněno') .
                    ($style ? '</span>' : '');
            case self::STATE_SUBMITTED_READY_TO_REVIEW:
                return ($style ? '<span class="text-info">' : '') .
                    __('Odesláno ke kontrole') .
                    ($style ? '</span>' : '');
            case self::STATE_ECONOMICS_APPROVED:
                return ($style ? '<span class="text-success">' : '') .
                    __('Schváleno') .
                    ($style ? '</span>' : '');
            case self::STATE_ECONOMICS_REQUESTED_CHANGES:
                return ($style ? '<span class="text-danger">' : '') .
                    __('Vráceno k opravě') .
                    ($style ? '</span>' : '');
            case self::STATE_SETTLEMENT_REJECTED:
                return ($style ? '<span class="text-danger">' : '') .
                    __('Nevyhovuje podmínkám') .
                    ($style ? '</span>' : '');
        }
        throw new Exception('Settlement State not known ' . $settlement_state_id);
    }

    public static function getKnownStatesWithLabels(): array
    {
        $rtn = [];
        foreach (self::KNOWN_STATES as $KNOWN_STATE) {
            $rtn[$KNOWN_STATE] = self::getLabel($KNOWN_STATE);
        }
        return $rtn;
    }
}
