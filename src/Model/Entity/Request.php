<?php

namespace App\Model\Entity;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\ClassReportInterface;
use App\Model\Table\RequestsTable;
use App\Model\Entity\RequestState;
use App\Traits\CsuInfoTrait;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use App\Model\Table\HistoryIdentitiesTable;
use App\Form\StandardRequestFormChangeController;

/**
 * Request Entity
 *
 * @property int $id
 * @property int $organization_id
 *
 * @property int $request_state_id
 *
 * @property string|null $reference_number spisová značka
 * @property int $user_id
 * @property int|null $paper_evidenced_by_user_id
 * @property int $user_identity_version
 * @property int $program_id
 * @property string $name
 * @property string|null $verification_code paper submission - lock verification code, form "{lock code alpha}|{lock timestamp}"
 * @property bool $is_locked
 * @property FrozenDate|null $lock_when
 * @property string|null $lock_comment
 * @property bool $is_reported
 * @property int $request_type_id
 * @property bool $is_settled
 * @property int|null $final_subsidy_amount
 * @property int|null $original_subsidy_amount
 * @property float $subsidy_paid
 * @property string|null $comment
 * @property string|null $purpose
 * @property string|null $final_evaluation
 * @property string|null $pre_evaluation
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $appeal_id
 * @property int $merged_request
 *
 * @property bool|null $de_minimis
 * @property string|null $de_minimis_comment
 * @property bool|null $public_control
 * @property bool|null $public_control_ok
 * @property string|null $public_control_comment
 * @property string|null $request_comment
 * @property string|null $settlement_date
 *
 * @property Organization $organization
 * @property User $user
 * @property Program $program
 * @property Appeal $appeal
 * @property Evaluation[] $evaluations
 * @property File[] $files
 * @property RequestBudget $request_budget
 * @property RequestLog[] $request_logs
 * @property Identity[] $identities
 * @property RequestFilledField[] $request_filled_fields
 * @property User $paper_evidencing_user
 * @property Payment[] $payments
 * @property RequestNotification[] $request_notifications
 * @property HistoryIdentitiesTable $HistoryIdentities
 *
 * Private properties not persisted in ORM
 *
 * _final_criterium structure:
 * [ {criterium_id_1} => {reached mean rating}, {criterium_id_2} ... , "sum" => {final reached rating sum, optionally}]
 * @property array $_final_criterium
 * _single_ratings structure:
 * [ {user_id} => [ {criterium_id} => {points}, 'comment' => {comment if provided}, 'label' => {label of evaluator} ] ]
 * @property array $_single_ratings
 * _flat_identities structure:
 * [ {identity key, eg. "is.po"} => {serialized, not formatted, value} ]
 * @property array $_flat_identities
 */
class Request extends AppEntity implements ClassReportInterface
{
    use LocatorAwareTrait;
    use CsuInfoTrait;

    public const FIELD_REQUEST_STATE_ID = 'request_state_id';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'comment' => true,
        'purpose' => true,
        'organization_id' => true,
        'user_id' => true,
        'program_id' => true,
        'name' => true,
        'is_locked' => true,
        'lock_when' => true,
        'is_reported' => true,
        'is_settled' => true,
        'final_subsidy_amount' => true,
        'original_subsidy_amount' => true,
        'modified' => true,
        'created' => true,
        'appeal_id' => true,
        'organization' => true,
        'user' => true,
        'program' => true,
        'appeal' => true,
        'evaluations' => true,
        'files' => true,
        'request_budget' => true,
        'reference_number' => true,
        'lock_comment' => true,
        'user_identity_version' => true,
        'request_logs' => true,
        'identities' => true,
        'request_type_id' => true,
        'paper_evidenced_by_user_id' => true,
        'paper_evidencing_user' => true,
        'request_state_id' => true,
        'de_minimis' => true,
        'de_minimis_comment' => true,
        'public_control' => true,
        'public_control_ok' => true,
        'public_control_comment' => true,
        'request_comment' => true,
        'settlement_date' => true,
        'pre_evaluation' => true,
        'merged_request' => true,
    ];

    public function canTransitionTo(int $new_request_state_id): bool
    {
        $original_state_id = $this->getOriginal(self::FIELD_REQUEST_STATE_ID);
        if ($new_request_state_id === $original_state_id) {
            // no transition here, allow updating other fields if request state does not change
            return true;
        }

        return in_array($new_request_state_id, RequestState::ALLOWED_TRANSITIONS[$original_state_id] ?? [], true);
    }

    public function setCurrentStatus(int $new_request_state_id, ?int $currentUserId = null, bool $setError = true, bool $changed = true): bool
    {
        if ($new_request_state_id === $this->request_state_id && !$changed) {
            // ignore duplicate status changes
            return true;
        }

        // check if new state is known
        if (!in_array($new_request_state_id, RequestState::KNOWN_STATUSES, true)) {
            $this->addError(self::FIELD_REQUEST_STATE_ID, 'Nový stav žádosti není znám', $setError);

            return false;
        }

        // check if the transition is allowed from original persisted state
        if (!$this->canTransitionTo($new_request_state_id)) {
            $this->addError(self::FIELD_REQUEST_STATE_ID, 'Toto není povolený stav, do kterého žádost může aktuálně přejít', $setError);

            return false;
        }

        // make transition
        $this->request_state_id = $new_request_state_id;
        $this->is_locked = !RequestState::canUserEditRequest($new_request_state_id);
        if ($this->is_locked === true) {
            // reset the lock timeout if already locked
            $this->lock_when = null;
            $this->lock_comment = null;
        } else {
            // verification code should not be available if user can edit request
            $this->verification_code = null;
        }

        if ($new_request_state_id === RequestState::STATE_SUBMIT_LOCK && $this->isDirty('request_state_id')) {
            $this->ensureVerificationCodePresent();
        }

        if (in_array($new_request_state_id, [RequestState::STATE_SUBMITTED, RequestState::STATE_SUBMIT_LOCK], true)) {
            // current identity locked
            $this->loadUser();
            if ($this->request_type_id !== RequestType::PAPER_REQUEST) {
                $this->set('user_identity_version', $this->user->getLastIdentityVersion(false));
                $this->loadIdentities(true);
            }
            $this->setCurrentIdentityLocked(true);
        } elseif (RequestState::canUserEditRequest($new_request_state_id)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsWithSameIdentity = $requestsTable->find('all', [
                'conditions' => [
                    'user_id' => $this->user_id,
                    'user_identity_version' => $this->user_identity_version,
                ],
            ]);
            // check if there is any request with same identity level
            // if there is none, unlock the identity as well
            $canUnlockIdentity = true;
            /** @var Request $request */
            foreach ($requestsWithSameIdentity as $request) {
                if ($request->id !== $this->id) {
                    $canUnlockIdentity = false;
                }
            }
            if ($canUnlockIdentity) {
                $this->setCurrentIdentityLocked(false);
            }
        }

        // record the transition consequences in logs
        $this->addLog($currentUserId);

        return true;
    }

    public function setCurrentIdentityLocked(bool $isLocked): self
    {
        $this->loadIdentities();
        $dirty = false;
        foreach ($this->identities as $identity) {
            if ($identity->is_locked !== $isLocked) {
                $identity->is_locked = $isLocked;
                $dirty = true;
            }
        }
        $this->setDirty('identities', $dirty);

        return $this;
    }

    public function canUserSubmitRequest(): bool
    {
        if ($this->request_type_id === RequestType::PAPER_REQUEST) {
            // paper requests can be marked as submitted by formal control only
            return false;
        }

        $this->loadAppeal();
        return $this->appeal->canUserSubmitRequest($this);
    }

    /**
     * Determines if the user is allowed to issue a request change, also used to decide whether to report changes in identity
     *
     * @return bool
     *
     */

    public function canUserIssueRequestChange(): bool
    {
        //TODO: Does the user own this request ?
        return (OrganizationSetting::isException(OrganizationSetting::USTI) &&
            (!in_array($this->request_state_id, [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT])) &&
            (in_array($this->request_state_id, [
                RequestState::STATE_READY_TO_SIGN_CONTRACT,
                RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                RequestState::STATE_SETTLEMENT_SUBMITTED,
                RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
            ], true)));
    }

    public function loadUser(): self
    {
        if ($this->user === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Users']);
        }

        return $this;
    }

    public function loadPayments(): self
    {
        if ($this->payments === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Payments.Settlements']);
        }

        return $this;
    }

    public function loadIdentities(bool $force = false): self
    {
        if ($this->identities === null || $force === true) {
            if ($force) {
                $this->unsetProperty('identities');
            }
            /** @var RequestsTable $requestsTable */
            $requestsTable = $this->getTableLocator()->get('Requests');
            if ($this->isDirty('user_identity_version')) {
                // if user_identity_version is dirty, loadInto would load incorrect data, based on current persisted state
                $this->identities = $requestsTable->Identities->find('all', [
                    'conditions' => [
                        'Identities.user_id' => $this->user_id,
                        'Identities.version' => $this->user_identity_version,
                    ]
                ])->toArray();
            } else {
                $requestsTable->loadInto($this, ['Identities']);
            }
        }

        return $this;
    }

    /**
     * Load Identity attachments
     */

    public function loadIdentityAttachments(): array
    {
        /** @var identityAttachmentsTable $IdentityAttachments */
        $identityAttachmentsTable = $this->getTableLocator()->get('IdentityAttachments');
        $attachments = $identityAttachmentsTable->find('all', [
            'conditions' => [
                'IdentityAttachments.user_id' => $this->user_id,
                'IdentityAttachments.is_archived' => '0',
            ],
            'contain' => ['IdentityAttachmentTypes', 'Files']
        ])->order(['IdentityAttachments.modified' => 'DESC'])->toArray();
        return $attachments;
    }

    /** Load Settlements for this request
     * @return self
     */

    public function loadSettlements(): self
    {
        // Get the table locator instance
        $tableLocator = $this->getTableLocator();

        // Get the Settlements table instance
        /** @var \Cake\ORM\Table $Settlements */
        $Settlements = $tableLocator->get('Settlements');

        // Fetch settlements for the current request
        $settlements = $Settlements->find('all', [
            'join' => [
                'table' => 'requests_to_settlements',
                'alias' => 'RTS',
                'type' => 'INNER',
                'conditions' => 'RTS.settlement_id = Settlements.id'
            ],
            'conditions' => [
                'RTS.request_id' => $this->id,  // Assuming the current entity has an 'id' property representing the request ID
            ],
        ])->order(['RTS.settlement_id' => 'DESC'])->toArray();
        $this->settlements = $settlements;

        return $this;
    }

    /** Load UserIncomeHistory for this request
     * @return self
     */

    public function loadUserIncomeHistory(): self
    {
        if ($this->user === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Users.PublicIncomeHistories', 'Users.PublicIncomeHistories.PublicIncomeSources']);

            if (OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_SHOW_HISTORIES)) {
                $userIdentities = array_fill_keys([
                    'HistoryIdentities.ico IN',
                    'HistoryIdentities.dic IN'
                ], []);

                foreach ([Identity::FO_VAT_ID, Identity::PO_BUSINESS_ID, Identity::PO_VAT_ID] as $v) {
                    if (!isset($this->_flat_identities[$v]) || empty($this->_flat_identities[$v])) {
                        continue;
                    }

                    switch ($v) {
                        case Identity::PO_BUSINESS_ID:
                            $userIdentities['HistoryIdentities.ico IN'][] = $this->_flat_identities[$v];
                            break;

                        case Identity::FO_VAT_ID:
                        case Identity::PO_VAT_ID:
                            $userIdentities['HistoryIdentities.dic IN'][] = $this->_flat_identities[$v];
                            break;
                    }
                }
            }

            if (isset($userIdentities) && !empty(array_filter($userIdentities))) {
                $historyIdentities = $this->getTableLocator()->get('HistoryIdentities');
                $archive = $historyIdentities->find('all', [
                    'conditions' => [
                        'HistoryIdentities.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                        'OR' => array_filter($userIdentities)
                    ],
                    'contain' => [
                        'Histories',
                    ],
                ])->toArray();

                if (!isset($this->user->public_income_histories)) {
                    $this->user->public_income_histories = array();
                }

                $orgRange = (int)OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true);
                $yearsRange = range((int)date('Y'), (int)date('Y') - max(0, $orgRange));

                if (isset($archive[0]) && isset($archive[0]->histories)) {
                    foreach ($yearsRange as $year) {
                        foreach ($archive[0]->histories as $v) {
                            if ((int)$v['year'] === (int)$year) {
                                $this->user->public_income_histories[] = (object)
                                [
                                    'amount_czk' => $v['czk_amount'],
                                    'fiscal_year' => $v['year'],
                                    'public_income_source' => (object)['organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()],
                                ];
                            }
                        }
                    }
                }

                // join with requests
                $allowedStates = [
                    RequestState::STATE_CLOSED_FINISHED,
                    RequestState::STATE_CONTRACT_SIGNED_READY_TO_PAYOUT,
                    RequestState::STATE_PAID_READY_FOR_SETTLEMENT,
                    RequestState::STATE_SETTLEMENT_SUBMITTED,
                    RequestState::STATE_WAITING_FOR_SETTLEMENT_TO_BE_PAID,
                ];
                $requests = empty($this->user_id) ? [] : $requestsTable->find(
                    'all',
                    [
                        'conditions' => [
                            'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                            'Requests.user_id' =>  $this->user_id,
                            'Requests.request_state_id IN (' . implode(',', $allowedStates) . ')',
                        ],
                        'contain' => [
                            'RequestLogs',
                        ],
                    ]
                )->order(['created' => 'DESC'])
                    ->toArray();

                foreach ($requests as $k => $v) {
                    $validLogs = array_filter($v->request_logs ?? [], function ($val) {
                        return (int)$val['request_state_id'] === RequestState::STATE_PAID_READY_FOR_SETTLEMENT;
                    });
                    $lastLog = count($validLogs) > 0 ? reset($validLogs) : [];
                    $logDate = !empty($lastLog) && isset($lastLog['created']) ? substr($lastLog['created']->year, 0, 4) : '';

                    $fiscal_year = !empty($logDate) ? $logDate : substr($v['created']->year, 0, 4);
                    foreach ($yearsRange as $year) {
                        if ($v['final_subsidy_amount'] > 0 && (int)$fiscal_year === (int)$year) {
                            $this->user->public_income_histories[] = (object)
                            [
                                'added_by' => __('systém'),
                                'added_by_system' => true,
                                'amount_czk' => $v['final_subsidy_amount'],
                                'fiscal_year' => $fiscal_year,
                                'public_income_source' => (object)['organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId()],
                            ];
                        }
                    }
                }
            }
        }

        return $this;
    }

    public function loadLogs(): self
    {
        // null => not loaded, [] => loaded and empty
        if ($this->request_logs === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['RequestLogs.Users']);
        }

        usort($this->request_logs, function (RequestLog $a, RequestLog $b) {
            return isset($b->created->timestamp)
                ?  $b->created->timestamp - $a->created->timestamp
                : 0;
        });

        return $this;
    }

    public function loadNotifications(): self
    {
        if ($this->request_notifications === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['RequestNotifications.Files', 'RequestNotifications.RequestNotificationTypes']);
        }

        return $this;
    }

    private function addLog(?int $executed_by_user_id = null, bool $force = false, ?string $overrideLockComment = null): bool
    {
        if (!$force && !$this->isDirty() && $this->request_state_id === $this->getOriginal(self::FIELD_REQUEST_STATE_ID)) {
            // do not add log if nothing changed or it's not forced by function call
            return false;
        }
        $this->loadLogs();
        /** @var RequestLog $newLog */
        $newLog = $this->getTableLocator()->get('RequestLogs')->newEntity();
        // record current state in the log
        $newLog->request_state_id = $this->request_state_id;
        $newLog->request_id = $this->id;
        $newLog->lock_comment = $overrideLockComment ?? $this->lock_comment;
        $newLog->is_locked = $this->is_locked;
        $newLog->comment = $this->comment;
        $newLog->purpose = $this->purpose;
        $newLog->executed_by_user_id = $executed_by_user_id;
        // add and enforce association to be persisted
        $this->request_logs[] = $newLog;
        $this->setDirty('request_logs');

        return true;
    }

    private function addError(string $field, string $error, bool $reallySetError = true)
    {
        if ($reallySetError) {
            $this->setError($field, $error);
        }
    }

    /**
     * @param int $form_id Forms.id of form associated with request
     * @return bool
     */
    public function isFormCompleted(int $form_id): bool
    {
        $form = $this->getFormById($form_id);
        if (empty($form)) {
            return false;
        }

        return $form->getFormController($this)->isFormFilledCompletely();
    }

    /**
     * @return Form[]
     */
    public function getForms(): iterable
    {
        $this->loadPrograms();


        $rtn = [];
        $out = [];
        $programFormsField = RequestType::STANDARD_REQUEST === $this->request_type_id ? 'forms' : 'paper_forms';

        foreach ((!empty($this->program) && !empty($this->program->{$programFormsField})) ? $this->program->{$programFormsField} : [] as $form) {
            $rtn[$form->id] = $form;
        }
        if (!empty($this->program->parent_program)) {
            foreach ($this->program->parent_program->{$programFormsField} ?? [] as $form) {
                $rtn[$form->id] = $form;
            }
        }

        // Sort by weight (primary) and id (secondary)
        if (!empty($rtn)) {
            usort($rtn, fn ($a, $b) => (((int)$a->weight <=> (int)$b->weight) === 0 ? (int)$a->id <=> (int)$b->id : (int)$a->weight <=> (int)$b->weight));
            foreach ($rtn as $form) {
                $out += [$form->id => $form];
            }
        }

        return $out;
    }

    /**
     * @param int $attachment_id find attachment by it's DB id, matching the user-owner of this request, unless $skip_ownership_check is set to  true
     * @return File|null
     */
    public function findAttachment(int $attachment_id, $skip_ownership_check = false): ?File
    {

        foreach ($this->files ?? [] as $attachment) {
            if ($skip_ownership_check) {
                $grant = true;
            } else {
                $grant = ($this->user_id === $attachment->user_id);
            }
            if ($attachment->id === $attachment_id && $grant) {
                return $attachment;
            }
        }


        $this->loadNotifications();
        foreach ($this->request_notifications ?? [] as $notification) {
            foreach ($notification->files ?? [] as $notification_attachment) {
                if ($skip_ownership_check) {
                    $grant = true;
                } else {
                    $grant = ($this->user_id === $notification_attachment->user_id);
                }
                if ($notification_attachment->id === $attachment_id && $grant) {
                    return $notification_attachment;
                }
            }
        }

        return null;
    }

    public function getRequesterContactEmails(): array
    {
        $rtn = [];
        $request = $this;
        if ($request instanceof Zadost) {
            if (!empty($request->ucet)) {
                $rtn[] = $request->ucet->email;
            }
        } elseif ($request instanceof Request) {
            /*TODO: expand on all contact email addresses*/
            $identity = $request->getFlatIdentities();
            if (array_key_exists(Identity::CONTACT_EMAIL, $identity)) {
                $rtn[] = $identity[Identity::CONTACT_EMAIL];
            }
        }
        return array_filter($rtn);
    }

    public function getRequesterName(bool $withEmail = true): string
    {
        $request = $this;
        $name = "";
        if ($request instanceof Request) {
            $flatIdentity = $request->getFlatIdentities();
            $name .= Identity::getIdentityFullName($flatIdentity);
            if ($withEmail) {
                $emails = $this->getRequesterContactEmails();
                if (!empty($emails)) {
                    $name .= ', ' . reset($emails);
                }
            }
        } elseif ($request instanceof Zadost) {
            if ($request->ucet) {
                $name .= $request->ucet->nazev;
                if ($withEmail) {
                    $name .= ', ' . $request->ucet->email;
                }
            }
        }
        return $name;
    }

    /**
     * @param int $form_id Forms.id of form associated with this request
     * @return Form|null
     */
    public function getFormById(int $form_id): ?Form
    {
        foreach ($this->getForms() as $form) {
            if ($form->id === $form_id) {
                return $form;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCurrentStateLabel(): string
    {
        return RequestState::getLabelByStateId($this->request_state_id);
    }

    /**
     * Returns budget validation code from @RequestBudget constants
     *
     * @return int
     */
    public function validateBudgetRequirements(): int
    {
        $this->loadBudget();
        if ($this->request_budget instanceof RequestBudget) {
            return $this->request_budget->validateBudgetRequirements($this->appeal, $this->program);
        }

        return RequestBudget::BUDGET_MISSING;
    }

    public function canUserSubmitSettlement(): bool
    {
        return RequestState::canUserSubmitSettlement($this->request_state_id);
    }

    public function getAdminDetailLink(): array
    {
        return ['_name' => 'my_teams_request_detail', 'id' => $this->id];
    }

    public function prefillSingleRatings(): array
    {
        if (!empty($this->_single_ratings) && is_array($this->_single_ratings)) {
            return $this->_single_ratings;
        }
        $this->_single_ratings = [];

        $this->loadEvaluations();
        foreach ($this->evaluations as $evaluation) {
            $evaluation_overview = [];
            foreach (unserialize($evaluation->responses) ?? [] as $criterium_id => $points) {
                $evaluation_overview[$criterium_id] = intval($points);
            }
            $evaluation_overview['comment'] = $evaluation->comment;
            $evaluation_overview['label'] = $evaluation->user ? $evaluation->user->email : sprintf('Uživatel č. %d', $evaluation->user_id);
            $this->_single_ratings[$evaluation->user_id] = $evaluation_overview;
        }

        return $this->_single_ratings;
    }

    public function getMaximumCriteriumSum(): float
    {
        $this->loadPrograms();
        $criterium = $this->getEvaluationCriteria();
        return $criterium ? $criterium->max_points : 0;
    }

    public function getMinimumCriteriumSum(): float
    {
        $this->loadPrograms();
        $criterium = $this->getEvaluationCriteria();
        return $criterium ? $criterium->min_points : 0;
    }

    /**
     * Prefill and return final criterium
     * If final_evaluation is already present, fill from it and only check consistence
     *
     * @return array
     */
    public function prefillFinalCriterium(): array
    {
        if (!empty($this->final_evaluation)) {
            $this->_final_criterium = @unserialize($this->final_evaluation);
        }
        if (is_array($this->_final_criterium) && !empty($this->_final_criterium)) {
            foreach ($this->_final_criterium as $criterium_id => $points) {
                $this->_final_criterium[$criterium_id] = round($points, 2);
            }
            return $this->_final_criterium;
        } else {
            $this->_final_criterium = [];
        }

        if (isset($this->_final_criterium['sum'])) {
            unset($this->_final_criterium['sum']);
        }

        $this->loadEvaluations();
        foreach ($this->evaluations as $evaluation) {
            $evaluation_overview = [];
            foreach (unserialize($evaluation->responses) ?? [] as $criterium_id => $points) {
                if (!isset($this->_final_criterium[$criterium_id]) || !is_array($this->_final_criterium[$criterium_id])) {
                    // [0, 0] => [points sum, number of evaluations]
                    $this->_final_criterium[$criterium_id] = [0, 0];
                }
                $this->_final_criterium[$criterium_id][0] += intval($points);
                $this->_final_criterium[$criterium_id][1]++;
                $evaluation_overview[$criterium_id] = intval($points);
            }
            $evaluation_overview['comment'] = $evaluation->comment;
            $evaluation_overview['label'] = $evaluation->user ? $evaluation->user->email : sprintf('Uživatel č. %d', $evaluation->user_id);
            $this->_single_ratings[$evaluation->user_id] = $evaluation_overview;
        }

        $sum = 0;
        foreach ($this->_final_criterium as $criterium_id => $stats) {
            if (!is_array($stats)) {
                # criterium has no evaluations at this point
                continue;
            }
            $this->_final_criterium[$criterium_id] = round($stats[1] > 0 ? ($stats[0] / $stats[1]) : 0, 2);
            $sum += $this->_final_criterium[$criterium_id];
        }

        $this->_final_criterium['sum'] = $sum;

        return $this->_final_criterium;
    }

    public function getFinalCriteriumMean(?int $criterium_id = null): float
    {
        $this->prefillFinalCriterium();

        return round(floatval($this->_final_criterium[$criterium_id] ?? 0.0), 2);
    }

    /**
     * Get the set of requests in the same program by the same user
     * @return array
     */

    public function getRequestsSet()
    {
        $user_id = $this->user_id;
        $program_id = $this->program_id;
        //TODO: throw an error if $user_id or $program_id are empty
        $requestsTable = $this->getTableLocator()->get('Requests');
        $requests_set = $requestsTable->find(
            'all',
            [
                'conditions' => [
                    'Requests.user_id' => $user_id,
                    'Requests.program_id' => $program_id,
                    'Requests.organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                ],
            ]
        )->order(['Requests.modified' => 'DESC'])->toArray();
        /* TODO: this may need to be added ?
        ->notMatching('PaperEvidencingUsers', function (Query $query) {
            return $query->where(['PaperEvidencingUsers.id' => $this->getCurrentUserId()]);
        })
        */
        return $requests_set;
    }


    /**
     * Like getForms, but only return the forms that are shared
     *
     * @return array
     */

    public function getSharedForms(): array
    {
        $out = [];
        $forms = $this->getForms();
        foreach ($forms as $form) {
            if ($form->shared) {
                $out += [$form->id => $form];
            }
        }
        return $out;
    }

    /**
     * Upon request creation, we auto fill values of shared forms from some already existing previous request
     * @return boolean true if sucessful
     */
    public function fillFromPeers()
    {

        // Find a suitable sibbling from which we can copy shared forms data
        $requests = $this->getRequestsSet();
        $request_sibling = null;
        foreach ($requests as $request) {
            if ($request->id == $this->id) continue;
            $request_sibling = $request;
            break;
        }
        // If no sibbling is found, this request is the first of it's kind and we don't have to do anything
        if (!$request_sibling) return true;

        $sharedForms = $this->getSharedForms();
        $sibblingFilledFields = $this->getFilledFieldsForSharedForms($sharedForms, $request_sibling);
        $myFilledFields = $this->getFilledFieldsForSharedForms($sharedForms, $this);
        // Now we need to copy the values - TODO: we rely on the same order of fields, maybe adittional checks would be safer
        foreach ($sibblingFilledFields as $key => $filledField) {
            $myFilledFields[$key]->value = $filledField->value;
        };

        // Now save them
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        if ($filledFieldsTable->saveMany($myFilledFields)) {
            return true;
        }
    }

    private function getFilledFieldsForSharedForms(array $sharedForms, $request): array
    {
        /** @var RequestFilledFieldsTable $filledFieldsTable */
        $filledFieldsTable = TableRegistry::getTableLocator()->get('RequestFilledFields');
        $sharedForms = $this->getSharedForms();
        $filledFields = [];
        // There can be more than one shared form
        foreach ($sharedForms as $shared_form) {
            /** @var \App\Model\Entity\Form $shared_form */
            $shared_form_controller = $shared_form->getFormController($request);
            // Traverse all fields
            foreach ($shared_form_controller->getFieldsInOrder() as $field) {
                $filledField = $filledFieldsTable->findOrCreate(
                    [
                        'form_id' => $shared_form_controller->getFormDefinition()->id,
                        'request_id' => $request->id,
                        'form_field_id' => $field->id,
                    ]
                );
                if ($filledField) {
                    $filledFields[] = $filledField;
                }
            }
        }
        return $filledFields;
    }

    public function getFinalCriteriaSum(): float
    {
        $this->prefillFinalCriterium();

        if (isset($this->_final_criterium['sum'])) {
            return $this->_final_criterium['sum'];
        }

        $countable = array_filter($this->_final_criterium, function ($key) {
            return is_numeric($key);
        }, ARRAY_FILTER_USE_KEY);

        return round(array_sum($countable), 2);
    }

    /**
     * @return EvaluationCriterium|null
     */
    public function getEvaluationCriteria(): ?Entity
    {
        $this->loadPrograms();
        if (!empty($this->program->evaluation_criteria_id)) {
            return $this->program->evaluation_criterium;
        }
        if (!empty($this->program->parent_program) && !empty($this->program->parent_program->evaluation_criterium)) {
            return $this->program->parent_program->evaluation_criterium;
        }
        return null;
    }

    public function getEvaluationCriteriumId(): ?int
    {
        $criteria = $this->getEvaluationCriteria();
        return $criteria ? $criteria->id : null;
    }

    public function getSingleRatings(): array
    {
        $this->prefillSingleRatings();

        return $this->_single_ratings;
    }

    public static function getReportColumns(): array
    {
        return [
            'id' => __('Číslo žádosti'),
            'reference_number' => __('Vlastní identifikátor žádosti (např. pořadové číslo, spisová značka, ...)'),
            'de_minimis' => __('Podpora de minimis'),
            'public_control' => __('Veřejnosprávní kontrola uskutečněna'),
            'public_control_ok' => __('Výsledek kontroly v pořádku'),
            'name' => __('Název žádosti / projektu'),
            'appeal.name' => __('Název Dotační Výzvy'),
            'request_state_id' => __('Aktuální stav výzvy v Dotačním Software'),
            'request_type_id' => __('Typ žádosti (elektronická nebo papírová evidence)'),
            'comment' => __('Slovní hodnocení žádosti'),
            'purpose' => __('Slovní formulace účelu, na který bude dotace poskytnuta (dle aktuálního stavu žádosti, může jít o návrh (Navrhovatelé), schválený návrh (Schvalovatelé) nebo záměr schválený zastupitelstvem (Finalizace))'),
            'program.name' => __('Název dotačního programu (pod-programu)'),
            'evaluations.count' => __('Počet provedených hodnocení (hodnotitelé)'),
            'final_evaluation.each' => __('Body za jednotlivá kritéria'),
            'final_evaluation.sum' => __('Celkový počet získaných bodů'),
            'final_subsidy_amount' => __('Schválená (schvalovaná) vyše dotace'),
        ];
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return in_array($column, ['final_evaluation.each'], true);
    }

    private static int $MAX_CRITERIA = -1;

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        switch ($path) {
            default:
                return [];
            case 'final_evaluation.each':
                if (self::$MAX_CRITERIA === -1) {
                    $evaluationCriteriaTable = TableRegistry::getTableLocator()->get('EvaluationCriteria');
                    $all_evaluation_criteria = $evaluationCriteriaTable->find('all', [
                        'conditions' => [
                            'organization_id' => OrgDomainsMiddleware::getCurrentOrganizationId(),
                            'parent_id IS NOT' => null
                        ],
                    ]);
                    $stats = [];
                    foreach ($all_evaluation_criteria as $single_criterium) {
                        $stats[$single_criterium->parent_id] = ($stats[$single_criterium->parent_id] ?? 0) + 1;
                    }
                    self::$MAX_CRITERIA = empty($stats) ? 0 : max($stats);
                }
                $headers = [];
                foreach (empty($stats) ? [__('Nebyla definována žádná hodnotící kritéria')] : range(1, max($stats)) as $header) {
                    $headers[] = is_numeric($header) ? sprintf(__('Kritérium č.%d - %s'), $header, $columnName) : $header;
                }
                return $headers;
        }
    }

    public static function extractColumn(Request $request, string $column): array
    {
        switch ($column) {
            case 'appeal.name':
                $request->loadAppeal();
                return [strval(Hash::get($request, $column))];
            case 'program.name':
                $request->loadPrograms();
                return [strval(Hash::get($request, $column))];
            case 'evaluations.count':
                $request->loadEvaluations();
                return [strval(count($request->evaluations))];
            case 'final_evaluation.each':
                $request->loadEvaluations();
                $request->prefillFinalCriterium();
                $rtn = [];
                foreach ($request->_final_criterium as $criterium_id => $criterium_points) {
                    if (is_numeric($criterium_id)) {
                        $rtn[] = strval($criterium_points);
                    }
                }
                while (count($rtn) < self::$MAX_CRITERIA) {
                    $rtn[] = '';
                }
                return $rtn;
            case 'final_evaluation.sum':
                return [strval($request->getFinalCriteriaSum())];
            default:
                return [strval(Hash::get($request, $column))];
            case 'request_state_id':
                return [RequestState::getLabelByStateId($request->request_state_id)];
            case 'request_type_id':
                return [RequestType::getLabel($request->request_type_id)];
            case 'final_subsidy_amount':
                return [$request->getFinalSubsidyAmount()];
            case 'de_minimis':
            case 'public_control_ok':
                return [Hash::get($request, $column) === null ? '' : (boolval(Hash::get($request, $column)) ? __('ano') : __('ne'))];
            case 'public_control':
                return [boolval(Hash::get($request, $column)) ? __('ano') : __('ne')];
        }
    }

    public function loadBudget(): self
    {
        if (!($this->request_budget instanceof RequestBudget)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['RequestBudgets']);
        }
        return $this;
    }

    public function loadAppeal(): self
    {
        if (!($this->appeal instanceof Appeal)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Appeals']);
        }
        return $this;
    }

    public function loadEvaluations(): self
    {
        if (!is_array($this->evaluations)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Evaluations']);
        }
        return $this;
    }

    public function loadPrograms(): self
    {
        if (!($this->program instanceof Program)) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['Programs', 'Programs.ParentPrograms', 'Programs.Forms.FormFields', 'Programs.PaperForms.FormFields']);
        }
        return $this;
    }

    public function loadFilledFields(): self
    {
        if ($this->request_filled_fields === null) {
            $requestsTable = $this->getTableLocator()->get('Requests');
            $requestsTable->loadInto($this, ['RequestFilledFields']);
        }
        return $this;
    }

    public function getFlatIdentities(): array
    {
        if (!empty($this->_flat_identities)) {
            return $this->_flat_identities;
        }
        $this->loadIdentities();
        $this->_flat_identities = Hash::combine($this->identities, '{n}.name', '{n}.value');
        return $this->_flat_identities;
    }

    public function canUserEditRequest(): bool
    {
        return RequestState::canUserEditRequest($this->request_state_id) && RequestType::canUserEditRequest($this->request_type_id);
    }

    /**
     * Whether user can download request PDF copy
     *
     * @return bool
     */
    public function canUserDownloadPdf(): bool
    {


        if ($this->request_type_id === RequestType::PAPER_REQUEST) {
            return false;
        }

        // Even more exprimental, see issue #195 which raises the question why should user NOT be able to dowload PDF
        return true;

        if (OrganizationSetting::isException(OrganizationSetting::P4)) {
            return true;
        }


        // Experimental, see issue #103
        if (RequestState::canUserEditRequest($this->request_state_id)) {
            return true;
        }


        if (!RequestState::canUserEditRequest($this->request_state_id) && $this->request_state_id !== RequestState::STATE_SUBMIT_LOCK) {
            // allow when request is not user editable AND not currently locked, when you can download only verification PDF
            return true;
        }

        if (OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, true)) {
            if (!OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, true)) {
                // request pdf only when manual submit does not require verification code and request is in correct state
                return $this->request_state_id === RequestState::STATE_READY_TO_SUBMIT;
            }
        }

        return false;
    }

    public function canUserLockOwnRequest(): bool
    {
        $preconditions = OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, true) &&
            //OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, true) &&
            $this->request_state_id === RequestState::STATE_READY_TO_SUBMIT;

        if ($preconditions) {
            $this->loadAppeal();
            return $this->appeal->canUserSubmitRequest($this);
        }

        return $preconditions;
    }

    public function canUserDownloadOwnVerificationPdf(): bool
    {
        return OrganizationSetting::getSetting(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS, true) &&
            //OrganizationSetting::getSetting(OrganizationSetting::MANUAL_SUBMIT_REQUIRE_VERIFICATION_CODE, true) &&
            $this->request_state_id === RequestState::STATE_SUBMIT_LOCK;
    }

    public function canUserUnlockOwnRequest(): bool
    {
        // same conditions, for now
        return $this->canUserDownloadOwnVerificationPdf();
    }

    public function getFinalSubsidyAmount(): int
    {
        if ($this->final_subsidy_amount === null) {
            if ($this->loadBudget()->request_budget) {
                return $this->request_budget->requested_amount;
            }
        }
        return max(intval($this->final_subsidy_amount), 0);
    }

    /**
     * Get's payments sorted by date of transaction effective date
     *
     * @return Payment[]
     */
    public function getPayments(): array
    {
        $this->loadPayments();
        $payments = $this->payments ?? [];
        usort($payments, function (Payment $a, Payment $b) {
            return $b->created->timestamp - $a->created->timestamp;
        });
        return $payments;
    }

    public function getPaymentsSum(): int
    {
        $sum = 0;
        foreach ($this->getPayments() as $payment) {
            if ($payment->is_refund) {
                $sum -= $payment->amount_czk;
            } else {
                $sum += $payment->amount_czk;
            }
        }
        return $sum;
    }

    public function getPaymentsRefundSum(): int
    {
        $sum = 0;
        foreach ($this->getPayments() as $payment) {
            if ($payment->is_refund) {
                $sum += $payment->amount_czk;
            }
        }
        return $sum;
    }

    public function hasUnsettledPayments(): bool
    {
        return array_reduce($this->getPayments(), function (bool $carry, Payment $p): bool {
            if (empty($p->settlement_id)) {
                return true;
            }
            if ($p->settlement && $p->settlement->settlement_state_id !== SettlementState::STATE_ECONOMICS_APPROVED) {
                return true;
            }
            return $carry;
        }, false);
    }

    public function hasNotifications(): bool
    {
        $this->loadNotifications();
        return $this->is_reported || !empty($this->request_notifications);
    }

    public function getPaymentById(int $payment_id): ?Payment
    {
        foreach ($this->getPayments() as $payment) {
            if ($payment_id === $payment->id) {
                return $payment;
            }
        }
        return null;
    }

    public function getPaidTotalPercent(): float
    {
        $paid_sum = $this->getPaymentsSum();
        return $paid_sum > 0 ? round($paid_sum / $this->getFinalSubsidyAmount(), 2) * 100 : 0;
    }

    public function hasSettlementWithRequestedChanges(): bool
    {
        $this->loadPayments();
        foreach ($this->payments as $payment) {
            if ($payment->settlement && $payment->settlement->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES) {
                return true;
            }
        }
        return false;
    }

    public function hasSettlementWithRejected(): bool
    {
        $this->loadPayments();
        foreach ($this->payments as $payment) {
            if ($payment->settlement && $payment->settlement->settlement_state_id === SettlementState::STATE_SETTLEMENT_REJECTED) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int $form_id
     * @return RequestFilledField[]
     */
    public function getFilledFields(int $form_id): iterable
    {
        $fields = [];

        $this->loadFilledFields();
        foreach ($this->request_filled_fields as $filled_field) {
            if ($filled_field->form_id === $form_id) {
                $fields[] = $filled_field;
            }
        }

        return $fields;
    }

    public function addMailLog(int $executed_by_user_id, string $subject, string $text)
    {
        $this->addLog(
            $executed_by_user_id,
            true,
            sprintf("Byl odeslán následující e-mail<br/><br/>Předmět: %s <br/><br/> %s", $subject, $text)
        );
    }

    public function addStatusLog(int $executed_by_user_id, string $text = null)
    {
        if ($text) {
            $this->addLog($executed_by_user_id, true, $text);
        } else {
            $this->addLog($executed_by_user_id, true);
        }
    }

    public function ensureVerificationCodePresent(): bool
    {
        if ($this->request_state_id === RequestState::STATE_SUBMIT_LOCK) {
            if (empty($this->verification_code)) {
                $this->verification_code = sprintf(
                    "%s|%d",
                    mb_strtoupper(random_str('alpha', 10)),
                    (new \DateTime())->getTimestamp()
                );
                $requestsTable = $this->getTableLocator()->get('Requests');
                return $requestsTable->save($this) !== false;
            }
            return true;
        }
        return false;
    }

    public function getVerificationCode(): string
    {
        $this->ensureVerificationCodePresent();
        return explode('|', $this->verification_code)[0];
    }

    /**
     * Return timestamp, as int or as formatted string
     *
     * @param bool $formatted if the timestamp should get formatted
     * @return int|string
     */
    public function getVerificationCodeTimestamp(bool $formatted = false)
    {
        $this->ensureVerificationCodePresent();
        $timestamp = explode('|', $this->verification_code)[1];
        return $formatted ? (new \DateTime())->setTimestamp(intval($timestamp))->format('d.m.Y H:i:s') : intval($timestamp);
    }

    public function unlockWithVerificationCode(string $verificationCode, int $currentUserId): bool
    {
        if (empty($this->verification_code)) {
            return false;
        }
        $code = trim(mb_strtoupper($this->getVerificationCode()));
        $verificationCode = trim(mb_strtoupper($verificationCode));
        if (strcmp($code, $verificationCode) === 0) {
            $this->setCurrentStatus(RequestState::STATE_SUBMITTED, $currentUserId);
            return true;
        }
        return false;
    }

    public function unlockWithoutVerificationCode(int $currentUserId): bool
    {
        $this->setCurrentStatus(RequestState::STATE_SUBMITTED, $currentUserId);
        return true;
    }
}
