<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Error;

/**
 * SettlementType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Settlement[] $settlements
 */
class SettlementType extends AppEntity
{

    public const STANDARD_SETTLEMENT = 1,
        FULL_SUBSIDY_RETURN = 2;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'modified' => true,
        'created' => true,
        'settlements' => true,
    ];

    public static function getLabel(int $settlement_type_id): string
    {
        switch($settlement_type_id) {
            case self::STANDARD_SETTLEMENT:
                return __('Standardní');
            case self::FULL_SUBSIDY_RETURN:
                return __('Vrácení celé podpory');
        }
        throw new Error('Typ vyúčtování neznámý');
    }
}
