<?php

namespace App\Model\Entity;

use App\Form\IdentityForm;
use App\Model\ClassReportInterface;
use App\Traits\CsuInfoTrait;
use App\Traits\StreetPartsAwareTrait;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use InvalidArgumentException;

/**
 * Identity Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $version
 * @property bool $is_locked
 *
 * @property User $user
 */
class Identity extends AppEntity implements ClassReportInterface
{
    use LocatorAwareTrait;
    use CsuInfoTrait;
    use StreetPartsAwareTrait;

    // indicate that identity is Corporate / Pravnicka Osoba
    public const IS_PO = 'is.po';

    public const FO_FIRSTNAME = 'fo.firstname',
        FO_SURNAME = 'fo.surname',
        FO_DEGREE_AFTER = 'fo.degree.after',
        FO_DEGREE_BEFORE = 'fo.degree.before',
        FO_DATE_OF_BIRTH = 'fo.date_of_birth',
        FO_VAT_ID = 'fo.vat_id',
        FO_FULLNAME = 'fo.fullname';

    public const PO_FULLNAME = 'po.fullname',
        PO_BUSINESS_ID = 'po.business_id',
        PO_CORPORATE_TYPE = 'po.corporate_type',
        PO_VAT_ID = 'po.vat_id',
        PO_REGISTRATION_DETAILS = 'po.registration.details',
        PO_REGISTRATION_SINCE = 'po.registration.since',
        PO_ANOTHER_SHARE = 'po.another_share';

    public const RESIDENCE_ADDRESS_CITY = 'address.residence.city',
        RESIDENCE_ADDRESS_CITY_PART = 'address.residence.city_part',
        RESIDENCE_ADDRESS_STREET = 'address.residence.street',
        RESIDENCE_ADDRESS_ZIP = 'address.residence.zip';

    public const POSTAL_ADDRESS_CITY = 'address.postal.city',
        POSTAL_ADDRESS_CITY_PART = 'address.postal.city_part',
        POSTAL_ADDRESS_STREET = 'address.postal.street',
        POSTAL_ADDRESS_ZIP = 'address.postal.zip';

    public const BANK_NAME = 'bank.name',
        BANK_NUMBER = 'bank.account_number',
        BANK_CODE = 'bank.code';

    public const BANK_NAME_2 = 'bank2.name',
        BANK_NUMBER_2 = 'bank2.account_number',
        BANK_CODE_2 = 'bank2.code';

    public const NO_NOTIFICATIONS = 'nonotifications';

    public const VAT = 'vat.registered';

    public const NOTE = 'note';

    public const CONTACT_PHONE = 'contact.phone',
        CONTACT_EMAIL = 'contact.email',
        CONTACT_DATABOX = 'contact.databox',
        CONTACT_EMAIL2 = 'contact.email2',
        CONTACT_EMAIL3 = 'contact.email3';

    public const ISDS_VERIFIED_DATABOX_ID = 'isds.verified.databox.id',
        ISDS_VERIFIED_DATABOX_TYPE = 'isds.verified.databox.type',
        ISDS_VERIFIED_BUSINESS_ID = 'isds.verified.business_id',
        ISDS_VERIFIED_BUSINES_NAME = 'isds.verified.business_name',
        ISDS_VERIFIED_FO_FIRST_NAME = 'isds.verified.fo.first_name',
        ISDS_VERIFIED_FO_LAST_NAME = 'isds.verified.fo.last_name',
        ISDS_VERIFIED_FO_FULL_NAME = 'isds.verified.fo.full_name',
        ISDS_VERIFIED_FO_DATE_OF_BIRTH = 'isds.verified.fo.date_of_birth',
        ISDS_VERIFIED_FO_ADDRESS_ZIP = 'isds.verified.fo.address.zip',
        ISDS_VERIFIED_FO_ADDRESS_STREET = 'isds.verified.fo.address.street',
        ISDS_VERIFIED_FO_ADDRESS_DISTRICT = 'isds.verified.fo.address.district',
        ISDS_VERIFIED_FO_ADDRESS_CITY = 'isds.verified.fo.city',
        ISDS_VERIFIED_FO_ADDRESS_FULL = 'isds.verified.fo.address_full',
        ISDS_VERIFIED_FO_ROB_IDENT = 'isds.verified.fo.rob_ident';

    public const ALL_VERIFIED_INFO_FO_BASIC = [
        self::ISDS_VERIFIED_FO_FIRST_NAME,
        self::ISDS_VERIFIED_FO_LAST_NAME,
        self::ISDS_VERIFIED_FO_FULL_NAME,
        self::ISDS_VERIFIED_FO_DATE_OF_BIRTH,
        self::ISDS_VERIFIED_FO_ROB_IDENT,
    ];

    public const ALL_VERIFIED_INFO_FO_ADDRESS = [
        self::ISDS_VERIFIED_FO_ADDRESS_STREET,
        self::ISDS_VERIFIED_FO_ADDRESS_CITY,
        self::ISDS_VERIFIED_FO_ADDRESS_ZIP,
        self::ISDS_VERIFIED_FO_ADDRESS_FULL,
    ];

    public const ALL_VERIFIED_INFO_CONTACTS = [
        self::ISDS_VERIFIED_DATABOX_ID,
        self::ISDS_VERIFIED_DATABOX_TYPE,
        self::ISDS_VERIFIED_BUSINES_NAME,
        self::ISDS_VERIFIED_BUSINESS_ID,
    ];

    public const ALL_VERIFIED_INFO = [
        self::ISDS_VERIFIED_DATABOX_ID,
        self::ISDS_VERIFIED_DATABOX_TYPE,
        self::ISDS_VERIFIED_BUSINESS_ID,
        self::ISDS_VERIFIED_BUSINES_NAME,
        self::ISDS_VERIFIED_FO_FIRST_NAME,
        self::ISDS_VERIFIED_FO_LAST_NAME,
        self::ISDS_VERIFIED_FO_FULL_NAME,
        self::ISDS_VERIFIED_FO_DATE_OF_BIRTH,
        self::ISDS_VERIFIED_FO_ADDRESS_ZIP,
        self::ISDS_VERIFIED_FO_ADDRESS_STREET,
        self::ISDS_VERIFIED_FO_ADDRESS_CITY,
        self::ISDS_VERIFIED_FO_ADDRESS_FULL,
        self::ISDS_VERIFIED_FO_ROB_IDENT,
    ];

    public const STATUTORY_FULLNAME = 'statutory.fullname',
        STATUTORY_REPRESENTATION_REASON = 'statutory.representation_reason',
        STATUTORY_PHONE = 'statutory.phone',
        STATUTORY_EMAIL = 'statutory.email',
        STATUTORY_SEND_EMAIL_NOTIFICATIONS = 'statutory.notify',
        STATUTORIES_ACT_STANDALONE = 'statutory.acting_standalone';

    public const ALL_STATUTORY = [
        self::STATUTORY_FULLNAME,
        self::STATUTORY_REPRESENTATION_REASON,
        self::STATUTORY_PHONE,
        self::STATUTORY_EMAIL,
    ];

    public const STATUTORY_X_FULLNAME = 'statutories.%d.fullname',
        STATUTORY_X_REPRESENTATION_REASON = 'statutories.%d.representation_reason',
        STATUTORY_X_PHONE = 'statutories.%d.phone',
        STATUTORY_X_EMAIL = 'statutories.%d.email',
        STATUTORY_X_SEND_EMAILS = 'statutories.%d.notify';

    public const FULL_STATUTORY_TEMPLATE = [
        self::STATUTORY_X_FULLNAME,
        self::STATUTORY_X_REPRESENTATION_REASON,
        self::STATUTORY_X_PHONE,
        self::STATUTORY_X_EMAIL,
        self::STATUTORY_X_SEND_EMAILS,
    ];

    public const INTEREST_X_SIZE = 'interest.%d.size',
        INTEREST_X_OWNER = 'interest.%d.owner',
        INTEREST_X_BUSINESS_ID = 'interest.%d.business_id',
        INTEREST_X_DATE_OF_BIRTH = 'interest.%d.date_of_birth',
        INTEREST_X_COUNTRY = 'interest.%d.country';

    public const OWNED_X_SIZE = 'owned.%d.size',
        OWNED_X_BUSINESS_ID = 'owned.%d.business_id',
        OWNED_X_NAME = 'owned.%d.name',
        OWNED_X_COUNTRY = 'owned.%d.country';

    public const FULL_INTEREST_TEMPLATE = [
        self::INTEREST_X_OWNER,
        self::INTEREST_X_BUSINESS_ID,
        self::INTEREST_X_DATE_OF_BIRTH,
        self::INTEREST_X_COUNTRY,
        self::INTEREST_X_SIZE,
    ];

    public const FULL_OWNED_INTERESTS_TEMPLATE = [
        self::OWNED_X_SIZE,
        self::OWNED_X_BUSINESS_ID,
        self::OWNED_X_NAME,
        self::OWNED_X_COUNTRY,
    ];

    public const BOOLEANS = [
        self::IS_PO,
        self::STATUTORY_SEND_EMAIL_NOTIFICATIONS,
        self::STATUTORIES_ACT_STANDALONE,
        self::STATUTORY_X_SEND_EMAILS,
    ];

    public const INTEGERS = [];

    public const DOUBLES = [];

    public const EMAILS = [
        self::STATUTORY_EMAIL,
        self::CONTACT_EMAIL,
        self::CONTACT_EMAIL2,
        self::CONTACT_EMAIL3,
        self::STATUTORY_X_EMAIL,
    ];

    public const REQUIRED_FIELDS = [
        self::BANK_NUMBER,
        self::BANK_NAME,
        self::BANK_CODE,
        self::CONTACT_PHONE,
        self::CONTACT_EMAIL,
        self::RESIDENCE_ADDRESS_CITY,
        self::RESIDENCE_ADDRESS_ZIP,
        self::RESIDENCE_ADDRESS_STREET,
    ];

    public const PO_REQUIRED_FIELDS = [
        self::PO_BUSINESS_ID,
        self::PO_CORPORATE_TYPE,
        self::PO_FULLNAME,
    ];

    public const FO_REQUIRED_FIELDS = [
        self::FO_DATE_OF_BIRTH,
        self::FO_SURNAME,
        self::FO_FIRSTNAME,
    ];

    public const TEXTEAREAS = [
        self::NOTE,
    ];

    public const SELECTS = [
        self::INTEREST_X_COUNTRY,
        self::OWNED_X_COUNTRY,
        self::POSTAL_ADDRESS_CITY,
        self::POSTAL_ADDRESS_CITY_PART,
        self::RESIDENCE_ADDRESS_CITY_PART,
        self::RESIDENCE_ADDRESS_CITY,
        self::PO_CORPORATE_TYPE,
    ];

    public const DATES = [
        self::FO_DATE_OF_BIRTH,
        self::INTEREST_X_DATE_OF_BIRTH,
        self::PO_REGISTRATION_SINCE,
    ];

    /**
     * @return array
     */
    public static function getLabels(): array
    {
        return [
            self::IS_PO => __('Mám IČO, jsem podnikatel nebo obchodní korporace'),

            self::FO_FIRSTNAME => __('Křestní jméno/jména'),
            self::FO_SURNAME => __('Příjmení'),
            self::FO_DEGREE_AFTER => __('Tituly za jménem'),
            self::FO_DEGREE_BEFORE => __('Tituly před jménem'),
            self::FO_DATE_OF_BIRTH => __('Datum narození'),
            self::FO_VAT_ID => __('DIČ'),

            self::PO_VAT_ID => __('DIČ'),
            self::PO_FULLNAME => __('Název společnosti'),
            self::PO_BUSINESS_ID => __('IČO'),
            self::PO_CORPORATE_TYPE => __('Právní forma'),
            self::PO_ANOTHER_SHARE => __('Žadatel nevlastní podíl v další právnické osobě'),

            self::VAT => __('Žadatel je plátcem DPH'),

            self::CONTACT_EMAIL => __('E-mailová adresa'),
            self::CONTACT_EMAIL2 => __('Další e-mailová adresa pro zasílání upozornění'),
            self::CONTACT_EMAIL3 => __('Další e-mailová adresa pro zasílání upozornění'),
            self::CONTACT_PHONE => __('Telefonní číslo'),
            self::CONTACT_DATABOX => __('ID Datové Schránky'),

            self::ISDS_VERIFIED_DATABOX_ID => __('Ověřené ID Datové Schránky'),
            self::ISDS_VERIFIED_DATABOX_TYPE => __('Ověřený Typ Datové Schránky'),
            self::ISDS_VERIFIED_BUSINES_NAME => __('Ověřený Název subjektu'),
            self::ISDS_VERIFIED_BUSINESS_ID => __('Ověřené IČO subjektu'),
            self::ISDS_VERIFIED_FO_LAST_NAME => __('Ověřené Příjmení'),
            self::ISDS_VERIFIED_FO_FIRST_NAME => __('Ověřené Křestní jméno'),
            self::ISDS_VERIFIED_FO_DATE_OF_BIRTH => __('Ověřené Datum narození'),
            self::ISDS_VERIFIED_FO_ADDRESS_STREET => __('Ověřená Ulice vč. čísla popisného a orientačního'),
            self::ISDS_VERIFIED_FO_ADDRESS_CITY => __('Ověřená Obec/Město'),
            self::ISDS_VERIFIED_FO_ADDRESS_ZIP => __('Ověřené PSČ'),
            self::ISDS_VERIFIED_FO_ADDRESS_DISTRICT => __('Ověřená Část obce / Městská část'),
            self::ISDS_VERIFIED_FO_ADDRESS_FULL => __('Ověřená adresa (kompletní)'),
            self::ISDS_VERIFIED_FO_ROB_IDENT => __('Ověřená existence v rejstříku obyvatel (ROB)'),
            self::ISDS_VERIFIED_FO_FULL_NAME => __('Ověřené celé jméno'),

            self::NOTE => false,

            self::BANK_NAME => __('Název banky'),
            self::BANK_CODE => __('Kód banky'),
            self::BANK_NUMBER => __('Číslo účtu'),

            self::BANK_NAME_2 => __('Název banky'),
            self::BANK_CODE_2 => __('Kód banky'),
            self::BANK_NUMBER_2 => __('Číslo účtu'),
            self::NO_NOTIFICATIONS => __('Nezaslat notifikace o provedených ohlášeních'),

            self::RESIDENCE_ADDRESS_CITY => __('Obec / Město'),
            self::RESIDENCE_ADDRESS_CITY_PART => __('Část obce / Městská část'),
            self::RESIDENCE_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
            self::RESIDENCE_ADDRESS_ZIP => __('PSČ'),

            self::POSTAL_ADDRESS_CITY => __('Obec / Město'),
            self::POSTAL_ADDRESS_CITY_PART => __('Část obce / Městská část'),
            self::POSTAL_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
            self::POSTAL_ADDRESS_ZIP => __('PSČ'),

            self::STATUTORY_EMAIL => __('E-mailová adresa'),
            self::STATUTORY_X_EMAIL => __('E-mailová adresa'),
            self::STATUTORY_FULLNAME => __('Celé jméno statutárního zástupce'),
            self::STATUTORY_X_FULLNAME => __('Celé jméno statutárního zástupce'),
            self::STATUTORY_PHONE => __('Telefonní číslo'),
            self::STATUTORY_X_PHONE => __('Telefonní číslo'),
            self::STATUTORY_REPRESENTATION_REASON => __('Právní důvod zastoupení'),
            self::STATUTORY_X_REPRESENTATION_REASON => __('Právní důvod zastoupení'),
            self::STATUTORY_SEND_EMAIL_NOTIFICATIONS => __('Zasílat na e-mail statutárního zástupce upozornění o podaných žádostech o dotaci'),
            self::STATUTORY_X_SEND_EMAILS => __('Zasílat na e-mail statutárního zástupce upozornění o podaných žádostech o dotaci'),
            self::STATUTORIES_ACT_STANDALONE => __('Uvedení členové statutárního orgánu mohou jednat samostatně'),

            self::INTEREST_X_SIZE => __('Velikost podílu (procenta nebo zlomek)'),
            self::INTEREST_X_OWNER => __('Plné jméno vlastníka podílu'),
            self::INTEREST_X_BUSINESS_ID => __('IČO'),
            self::INTEREST_X_DATE_OF_BIRTH => __('Datum narození vlastníka podílu'),
            self::INTEREST_X_COUNTRY => __('Státní příslušnost'),

            self::OWNED_X_SIZE => __('Velikost podílu (procenta nebo zlomek)'),
            self::OWNED_X_BUSINESS_ID => __('IČO'),
            self::OWNED_X_NAME => __('Název právnické osoby'),

            self::PO_REGISTRATION_SINCE => __('Datum registrace / vzniku právnické osoby'),
            self::PO_REGISTRATION_DETAILS => __('Spisová značka / Živnostenský úřad'),

            sprintf(self::INTEREST_X_SIZE, 0) => __('Osoby s podílem v této PO / skuteční majitelé')
        ];
    }

    public static function getPaperLabel(string $fieldId): ?string
    {
        switch ($fieldId) {
            default:
                return self::getFieldLabel($fieldId);
            case self::IS_PO:
                return __('Je žadatel právnická osoba (má IČO) ?');
        }
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string|null
     */
    public static function getFieldLabel(string $fieldId): ?string
    {
        $labels = self::getLabels();

        return isset($labels[$fieldId]) ? $labels[$fieldId] : null;
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string
     */
    public static function getFieldFormControlType(string $fieldId): string
    {
        if (in_array($fieldId, self::EMAILS)) {
            return 'email';
        }
        if (in_array($fieldId, self::SELECTS)) {
            return 'select';
        }
        if (in_array($fieldId, self::INTEGERS)) {
            return 'number';
        }
        if (in_array($fieldId, self::DOUBLES)) {
            return 'number';
        }
        if (in_array($fieldId, self::DATES)) {
            return 'date';
        }
        if (in_array($fieldId, self::TEXTEAREAS)) {
            return 'textarea';
        }
        if (in_array($fieldId, self::BOOLEANS)) {
            return 'checkbox';
        }

        return 'text';
    }

    /**
     * @return array
     */
    public static function getPlaceholders(): array
    {
        return [
            self::FO_DATE_OF_BIRTH => __('Nepovinné pro právnickou osobu'),
            self::FO_VAT_ID => __('Vyplňte DIČ, pokud vám bylo přiděleno'),
            self::FO_SURNAME => __('Příjmení'),
            self::FO_FIRSTNAME => __('Křestí jméno/jména'),

            self::RESIDENCE_ADDRESS_STREET => __('například Koněvova 1853/129'),
            self::RESIDENCE_ADDRESS_ZIP => __('například 293 01'),
            self::RESIDENCE_ADDRESS_CITY_PART => __('například Mladá Boleslav II'),
            self::RESIDENCE_ADDRESS_CITY => __('například Mladá Boleslav'),

            self::POSTAL_ADDRESS_STREET => __('například Koněvova 1853/129'),
            self::POSTAL_ADDRESS_ZIP => __('například 293 01'),
            self::POSTAL_ADDRESS_CITY_PART => __('například Mladá Boleslav II'),
            self::POSTAL_ADDRESS_CITY => __('například Mladá Boleslav'),

            self::CONTACT_PHONE => __('například +420 608 123 456'),
            self::STATUTORY_PHONE => __('například +420 608 123 456'),

            self::BANK_CODE => __('například 0100'),
            self::BANK_NAME => __('například Komerční Banka'),
            self::BANK_NUMBER => __('například 86-199488014'),

            self::NOTE => __('Nepovinné, vyplňte jen pokud je třeba něco dodat k vyplněné identifikaci / identitě žadatele'),

            self::PO_REGISTRATION_DETAILS => __('Např. Městský soud v Praze, C 123456'),
        ];
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return string|null
     */
    public static function getFieldFormPlaceholder(string $fieldId): ?string
    {
        $placeholders = self::getPlaceholders();

        return isset($placeholders[$fieldId]) ? $placeholders[$fieldId] : null;
    }

    /**
     * @return array
     */
    public static function getDefaultValues(): array
    {
        return [
            self::FO_DATE_OF_BIRTH => null,
            self::INTEREST_X_DATE_OF_BIRTH => null,
            self::STATUTORY_SEND_EMAIL_NOTIFICATIONS => false,
        ];
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return mixed|null
     */
    public static function getFieldFormDefaultValue(string $fieldId)
    {
        return self::getDefaultValues()[$fieldId] ?? null;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'version' => true,
        'is_locked' => true,
        'user' => true,
    ];

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @return bool|string|null
     */
    public static function getFieldFormEmpty(string $fieldId)
    {
        if (in_array($fieldId, self::SELECTS)) {
            return __('Prosím vyberte si z možností');
        }
        switch ($fieldId) {
            default:
                return false;
            case self::INTEREST_X_DATE_OF_BIRTH:
            case self::FO_DATE_OF_BIRTH:
                return true;
        }
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @param mixed $data user-provided
     * @return string
     */
    public static function serialize(string $fieldId, $data): string
    {
        if (in_array($fieldId, self::BOOLEANS)) {
            return boolval($data) ? '1' : '0';
        }

        return $data;
    }

    /**
     * @param string $fieldId id of identity field, from Identity constants
     * @param bool $isPO whether current identity is business corporation
     * @param int|null $poFormKod business corporation form id from ARES/CSU
     * @return bool
     */
    public static function getFieldFormIsRequired(string $fieldId, bool $isPO, ?int $poFormKod): bool
    {
        if (in_array($fieldId, self::getRequiredFieldIds($isPO, $poFormKod), true)) {
            return true;
        }

        return false;
    }

    public static function mustProvideStatutory(?int $poFormType = null): bool
    {
        return empty($poFormType) || $poFormType < 100 || $poFormType > 110;
    }

    /**
     * @param bool $isPO whether current identity is business corporation
     * @param int|null $poFormaKod business corporation form id from ARES/CSU
     * @param int|null $request_type_id consider required fields with regards to request type, ie. paper request does not require all the fields
     * @return array of identity fields, from Identity constants
     */
    public static function getRequiredFieldIds(bool $isPO, ?int $poFormaKod, ?int $request_type_id = null): array
    {
        $required = self::REQUIRED_FIELDS;
        $required = array_merge($required, $isPO ? self::PO_REQUIRED_FIELDS : self::FO_REQUIRED_FIELDS);
        if ($isPO && self::mustProvideStatutory($poFormaKod)) {
            if (empty($request_type_id) || !in_array($request_type_id, [RequestType::PAPER_REQUEST])) {
                $required = array_merge($required, self::ALL_STATUTORY);
                //$required[] = sprintf(self::INTEREST_X_SIZE, 0);
            }
        }

        return $required;
    }

    public static function getIdentityFullName(array $flatData): string
    {
        $isPO = boolval($flatData[self::IS_PO] ?? 0);
        if ($isPO) {
            return sprintf("%s (%s)", $flatData[self::PO_FULLNAME] ?? 'N/A', $flatData[self::PO_BUSINESS_ID] ?? 'N/A');
        }

        return sprintf(
            "%s %s %s %s",
            $flatData[self::FO_DEGREE_BEFORE] ?? '',
            $flatData[self::FO_FIRSTNAME] ?? '',
            $flatData[self::FO_SURNAME] ?? '',
            $flatData[self::FO_DEGREE_AFTER] ?? ''
        );
    }

    public static function format(array $identity_data, string $format = 'default'): string
    {
        switch ($format) {
            case 'default':
                return self::getIdentityFullName($identity_data);
        }
        throw new InvalidArgumentException('incorrect identity format provided');
    }

    public static function getFormattedKey(string $raw_key, int $counter): string
    {
        return sprintf($raw_key, $counter);
    }

    public static function extractPOInterests(array $flattened): array
    {
        $flattened = Hash::flatten($flattened);
        $non_empty_interests = [];

        $requiredInterestFields = [
            //Identity::INTEREST_X_SIZE,
            Identity::INTEREST_X_OWNER,
        ];

        for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) {
            $identity = [];
            foreach (Identity::FULL_INTEREST_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $value = $flattened[$flatKey] ?? null;
                $part = explode('.', $flatKey);
                $part = end($part);
                if (in_array($interestField, $requiredInterestFields, true) && empty($value)) {
                    continue 2;
                }
                $identity[$part] = $value;
            }
            $non_empty_interests[] = $identity;
        }

        return $non_empty_interests;
    }

    public static function extractOwnedInterests(array $flattened): array
    {
        $flattened = Hash::flatten($flattened);
        $non_empty_interests = [];

        for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_OWNED_INTERESTS; $i++) {
            $identity = [];
            foreach (Identity::FULL_OWNED_INTERESTS_TEMPLATE as $interestField) {
                $flatKey = sprintf($interestField, $i);
                $value = $flattened[$flatKey] ?? null;
                $part = explode('.', $flatKey);
                $part = end($part);
                if (empty($value)) {
                    continue 2;
                }
                $identity[$part] = $value;
            }
            $non_empty_interests[] = $identity;
        }

        return $non_empty_interests;
    }

    public static function extractPOStatutories(array $expanded): array
    {
        $flattened = Hash::flatten($expanded);
        $non_empty_statutories = [];

        for ($i = 0; $i < IdentityForm::MAX_NUMBER_OF_STATUTORY_MEMBERS; $i++) {
            $identity = [];
            foreach (Identity::FULL_STATUTORY_TEMPLATE as $statutoryField) {
                $flatKey = sprintf($statutoryField, $i);
                $value = $flattened[$flatKey] ?? null;
                $identity[$statutoryField] = $value;
            }
            if (empty(join("", $identity))) {
                continue;
            }
            $non_empty_statutories[] = $identity;
        }

        return Hash::expand($non_empty_statutories);
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public static function getReportColumns(): array
    {
        return [
            self::IS_PO => __('Je žadatel právnická osoba?'),
            'common.fullname' => __('Název žadatele (celé jméno nebo název právnické osoby)'),
            self::CONTACT_DATABOX => __('ID Datové schránky'),
            'common.legal_form' => __('Právní forma žadatele (zkratka PO, FO nebo FOP)'),
            self::PO_BUSINESS_ID => self::getFieldLabel(self::PO_BUSINESS_ID),
            self::PO_VAT_ID => self::getFieldLabel(self::PO_VAT_ID) . __(' (PO nebo FOP)'),
            self::FO_VAT_ID => self::getFieldLabel(self::FO_VAT_ID) . __(' (FO)'),
            __('Právnická Osoba') => [
                self::PO_FULLNAME => self::getFieldLabel(self::PO_FULLNAME),
                self::PO_REGISTRATION_SINCE => self::getFieldLabel(self::PO_REGISTRATION_SINCE),
                self::PO_REGISTRATION_DETAILS => self::getFieldLabel(self::PO_REGISTRATION_DETAILS),
                self::PO_CORPORATE_TYPE => self::getFieldLabel(self::PO_CORPORATE_TYPE),
                __('Statutární orgán zastupující právnickou osobu žadatele') => [
                    self::STATUTORY_FULLNAME => __('Celé jméno statutárního zástupce'),
                    self::STATUTORY_PHONE => __('Telefonní číslo'),
                    self::STATUTORY_EMAIL => __('E-mailová adresa'),
                    self::STATUTORY_REPRESENTATION_REASON => __('Právní důvod zastoupení')
                ]
            ],
            __('Fyzická osoba') => [
                self::FO_DEGREE_BEFORE => self::getFieldLabel(self::FO_DEGREE_BEFORE),
                self::FO_FIRSTNAME => self::getFieldLabel(self::FO_FIRSTNAME),
                self::FO_SURNAME => self::getFieldLabel(self::FO_SURNAME),
                self::FO_DEGREE_AFTER => self::getFieldLabel(self::FO_DEGREE_AFTER),
                self::FO_DATE_OF_BIRTH => self::getFieldLabel(self::FO_DATE_OF_BIRTH)
            ],
            __('Kontaktní informace') => [
                self::CONTACT_PHONE => __('Telefonní číslo'),
                self::CONTACT_EMAIL => __('E-mailová adresa'),
                __('Sídlo / Trvalá adresa') => [
                    self::RESIDENCE_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
                    self::RESIDENCE_ADDRESS_CITY => __('Obec / Město'),
                    self::RESIDENCE_ADDRESS_CITY_PART => __('Část obce / Městská část'),
                    self::RESIDENCE_ADDRESS_ZIP => __('PSČ'),
                    'address.residence.full' => __('Celá adresa (ulice, město, psč)'),
                    __('Části ulice') => [
                        'address.residence.street.name' => __('Ulice (bez dalších čísel nebo písmen'),
                        'address.residence.street.popisne' => __('Číslo popisné / evidenční / domovní'),
                        'address.residence.street.orientacni' => __('Číslo orientační'),
                        'address.residence.street.pismeno' => __('Písmeno čísla orientačního'),
                    ]
                ],
                __('Doručovací adresa') => [
                    self::POSTAL_ADDRESS_STREET => __('Ulice vč. čísla popisného a orientačního'),
                    self::POSTAL_ADDRESS_CITY => __('Obec / Město'),
                    self::POSTAL_ADDRESS_CITY_PART => __('Část obce / Městská část'),
                    self::POSTAL_ADDRESS_ZIP => __('PSČ'),
                    'address.postal.full' => __('Celá adresa (ulice, město, psč)'),
                    __('Části ulice') => [
                        'address.postal.street.name' => __('Ulice (bez dalších čísel nebo písmen'),
                        'address.postal.street.popisne' => __('Číslo popisné / evidenční / domovní'),
                        'address.postal.street.orientacni' => __('Číslo orientační'),
                        'address.postal.street.pismeno' => __('Písmeno čísla orientačního'),
                    ]
                ],
            ],
            __('Bankovní spojení') => [
                'bank.full' => __('Číslo účtu vč. kódu banky'),
                self::BANK_NUMBER => __('Číslo účtu'),
                self::BANK_CODE => __('Kód banky'),
                self::BANK_NAME => __('Název banky')
            ],
            __('Historie přijaté veřejné podpory') => [
                'history.sum.full' => __('Součet přijaté podpory - celkem'),
                'history.sum.this_year' => __('Součet přijaté podpory - za tento rok'),
                'history.sum.last_year' => __('Součet přijaté podpory - za minulý rok'),
                'history.sum.year_minus_2' => __('Součet přijaté podpory - před 2 roky'),
                'history.sum.year_minus_3' => __('Součet přijaté podpory - před 3 roky'),
                'history.sum.year_minus_4' => __('Součet přijaté podpory - před 4 roky'),
                // current organisation only
                'history.org_sum.full' => __('Součet přijaté podpory od této organizace - celkem'),
                'history.org_sum.this_year' => __('Součet přijaté podpory od této organizace - za tento rok'),
                'history.org_sum.last_year' => __('Součet přijaté podpory od této organizace - za minulý rok'),
                'history.org_sum.year_minus_2' => __('Součet přijaté podpory od této organizace - před 2 roky'),
                'history.org_sum.year_minus_3' => __('Součet přijaté podpory od této organizace - před 3 roky'),
                'history.org_sum.year_minus_4' => __('Součet přijaté podpory od této organizace - před 4 roky'),
                // system only
                'history.system_sum.full' => __('Součet přijaté podpory - zdroj Systém - celkem'),
                'history.system_sum.this_year' => __('Součet přijaté podpory - zdroj Systém - za tento rok'),
                'history.system_sum.last_year' => __('Součet přijaté podpory - zdroj Systém - za minulý rok'),
                'history.system_sum.year_minus_2' => __('Součet přijaté podpory - zdroj Systém - před 2 roky'),
                'history.system_sum.year_minus_3' => __('Součet přijaté podpory - zdroj Systém - před 3 roky'),
                'history.system_sum.year_minus_4' => __('Součet přijaté podpory - zdroj Systém - před 4 roky'),
            ]
        ];
    }

    public static function extractColumn(Request $request, string $column): array
    {
        $flatIdentity = $request->getFlatIdentities();
        $orgSum = strpos($column, '.org_sum.') > 0 ? intval($request->organization_id) : null;
        $systemSum = strpos($column, '.system_sum.') > 0;

        switch ($column) {
            case 'common.fullname':
                return [Identity::getIdentityFullName($flatIdentity)];
            case 'common.legal_form':
                $is_po = $flatIdentity[Identity::IS_PO] ?? false;
                $po_code = intval($flatIdentity[Identity::PO_CORPORATE_TYPE] ?? 0);
                $legal_form = $is_po ? 'PO' : 'FO';
                if ($is_po && $po_code >= 100 && $po_code < 110) {
                    $legal_form = 'FOP';
                }
                return [$legal_form];
            case self::RESIDENCE_ADDRESS_CITY:
            case self::POSTAL_ADDRESS_CITY:
                return [
                    $request->getCityName(intval($flatIdentity[$column] ?? null))
                ];
            case self::RESIDENCE_ADDRESS_CITY_PART:
            case self::POSTAL_ADDRESS_CITY_PART:
                return [
                    $request->getCityPartName(intval($flatIdentity[$column] ?? null))
                ];
            case 'address.residence.full':
                $residenceCity = $request->getCityName(intval($flatIdentity[Identity::RESIDENCE_ADDRESS_CITY] ?? null));
                $residenceCityPart = $request->getCityPartName(intval($flatIdentity[Identity::RESIDENCE_ADDRESS_CITY_PART] ?? null));
                return [sprintf(
                    "%s, %s %s, %s",
                    $flatIdentity[Identity::RESIDENCE_ADDRESS_STREET] ?? '',
                    $residenceCity,
                    $residenceCityPart,
                    $flatIdentity[Identity::RESIDENCE_ADDRESS_ZIP] ?? '',
                )];
            case 'address.postal.full':
                $residenceCity = $request->getCityName(intval($flatIdentity[Identity::POSTAL_ADDRESS_CITY] ?? null));
                $residenceCityPart = $request->getCityPartName(intval($flatIdentity[Identity::POSTAL_ADDRESS_CITY_PART] ?? null));
                return [sprintf(
                    "%s, %s %s, %s",
                    $flatIdentity[Identity::POSTAL_ADDRESS_STREET] ?? '',
                    $residenceCity,
                    $residenceCityPart,
                    $flatIdentity[Identity::POSTAL_ADDRESS_ZIP] ?? '',
                )];
            case 'address.residence.street.name':
                return [
                    self::getStreetPart($flatIdentity[self::RESIDENCE_ADDRESS_STREET] ?? '', self::$STREET_PART_NAME)
                ];
            case 'address.residence.street.popisne':
                return [
                    self::getStreetPart($flatIdentity[self::RESIDENCE_ADDRESS_STREET] ?? '', self::$STREET_PART_POPISNE)
                ];
            case 'address.residence.street.orientacni':
                return [
                    self::getStreetPart($flatIdentity[self::RESIDENCE_ADDRESS_STREET] ?? '', self::$STREET_PART_ORIENTACNI)
                ];
            case 'address.residence.street.pismeno':
                return [
                    self::getStreetPart($flatIdentity[self::RESIDENCE_ADDRESS_STREET] ?? '', self::$STREET_PART_PISMENO)
                ];
            case 'address.postal.street.name':
                return [
                    self::getStreetPart($flatIdentity[self::POSTAL_ADDRESS_STREET] ?? '', self::$STREET_PART_NAME)
                ];
            case 'address.postal.street.popisne':
                return [
                    self::getStreetPart($flatIdentity[self::POSTAL_ADDRESS_STREET] ?? '', self::$STREET_PART_POPISNE)
                ];
            case 'address.postal.street.orientacni':
                return [
                    self::getStreetPart($flatIdentity[self::POSTAL_ADDRESS_STREET] ?? '', self::$STREET_PART_ORIENTACNI)
                ];
            case 'address.postal.street.pismeno':
                return [
                    self::getStreetPart($flatIdentity[self::POSTAL_ADDRESS_STREET] ?? '', self::$STREET_PART_PISMENO)
                ];
            case 'bank.full':
                return [
                    sprintf('%s/%s', $flatIdentity[self::BANK_NUMBER], $flatIdentity[self::BANK_CODE])
                ];
            case 'history.sum.full':
            case 'history.org_sum.full':
            case 'history.system_sum.full':
                return [
                    $request->loadUserIncomeHistory()->user->getPublicIncomeHistorySum('all', $orgSum, $systemSum)
                ];
            case 'history.sum.this_year':
            case 'history.org_sum.this_year':
            case 'history.system_sum.this_year':
                return [
                    $request->loadUserIncomeHistory()->user->getPublicIncomeHistorySum(date('Y'), $orgSum, $systemSum)
                ];
            case 'history.sum.last_year':
            case 'history.sum.year_minus_2':
            case 'history.sum.year_minus_3':
            case 'history.sum.year_minus_4':
                // org_sum
            case 'history.org_sum.last_year':
            case 'history.org_sum.year_minus_2':
            case 'history.org_sum.year_minus_3':
            case 'history.org_sum.year_minus_4':
                // system_sum
            case 'history.system_sum.last_year':
            case 'history.system_sum.year_minus_2':
            case 'history.system_sum.year_minus_3':
            case 'history.system_sum.year_minus_4':
                $minusYear = intval(strval(substr($column, -1))) > 1 ? strval(substr($column, -1)) : 1;
                return [
                    $request->loadUserIncomeHistory()->user->getPublicIncomeHistorySum(date('Y', strtotime('-' . $minusYear . ' year')), $orgSum, $systemSum)
                ];
            default:
                return [strval($flatIdentity[$column] ?? '')];
        }
    }

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        return [];
    }

    public static function isFieldValueEmpty(string $fieldId, $value): bool
    {
        if (in_array($fieldId, self::BOOLEANS)) {
            return !in_array(strval($value), ['0', '1'], true);
        }
        return empty($value);
    }
}
