<?php

namespace App\Model\Entity;

use App\Budget\ProjectBudgetFactory;
use App\Model\ClassReportInterface;
use App\Model\Table\BudgetItemsTable;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * RequestBudget Entity
 *
 * @property int $id
 * @property int $request_id
 * @property float $total_income
 * @property float $total_costs
 * @property float $total_own_sources
 * @property float $total_other_subsidy
 * @property float $requested_amount
 * @property null|string $extra_data
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property BudgetItem[] $incomes
 * @property BudgetItem[] $costs
 * @property BudgetItem[] $own_sources
 * @property BudgetItem[] $other_subsidies
 */
class RequestBudgetChange extends AppEntity implements ClassReportInterface
{
    public const CHANGE_ORIGINAL = 0,
        CHANGE_PENDING = 1,
        CHANGE_ACCEPTED = 2,
        CHANGE_DECLINED = 3;

    public const KNOWN_STATUSES = [
        self::CHANGE_ORIGINAL,
        self::CHANGE_PENDING,
        self::CHANGE_ACCEPTED,
        self::CHANGE_DECLINED
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'total_income' => true,
        'total_costs' => true,
        'total_own_sources' => true,
        'total_other_subsidy' => true,
        'requested_amount' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'incomes' => true,
        'costs' => true,
        'status' => true,
        'own_sources' => true,
        'other_subsidies' => true,
        'extra_data' => true,
    ];

    static function getLabels()
    {
        return [
            self::CHANGE_PENDING => __('Čeká na schválení'),
            self::CHANGE_ACCEPTED => __('Změna akceptována'),
            self::CHANGE_DECLINED => __('Změna zamítnuta')
        ];
    }

    public function deleteRemovedItems(BudgetItemsTable $table)
    {
        foreach ($this->_itemsToDelete as $index => $item) {
            if (!empty($item['id'])) {
                //Log::debug(json_encode($item));
                $raw = $table->get(intval($item['id']), [
                    'conditions' => [
                        'request_budget_id' => $this->id,
                    ],
                ]);
                if (!empty($raw)) {
                    $table->delete($raw);
                }
            }
            unset($this->_itemsToDelete[$index]);
        }
    }

    /**
     * @return void
     */
    public function recountItems(): void
    {
        $this->total_costs = array_reduce(
            $this->costs ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_income = array_reduce(
            $this->incomes ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_other_subsidy = array_reduce(
            $this->other_subsidies ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_own_sources = array_reduce(
            $this->own_sources ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
    }

    /**
     * @param array $data POST/Request data to be cleaned
     * @return array cleaned data
     */
    public function cleanPostData(array $data): array
    {
        foreach (['incomes', 'costs', 'own_sources', 'other_subsidies'] as $group) {
            if (!isset($data[$group]) || !is_array($data[$group])) {
                continue;
            }
            foreach ($data[$group] as $index => $budget_item) {
                if (empty($budget_item['description']) || empty($budget_item['amount'])) {
                    if (!empty($budget_item['id'])) {
                        $this->_itemsToDelete[] = $budget_item;
                    }
                    unset($data[$group][$index]);
                }
            }
        }

        return $data;
    }


    public static function getReportColumns(): array
    {
        return [
            'requested_amount' => __('Žádaná částka'),
            'total_other_subsidy' => __('Jiné dotace na projekt celkem'),
            'total_own_sources' => __('Vlastní zdroje celkem'),
            'total_costs' => __('Náklady celkem'),
            'total_income' => __('Příjmy celkem'),
        ];
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public static function extractColumn(Request $request, string $column): array
    {
        $request->loadBudget();
        return [$request->request_budget ? strval(Hash::get($request->request_budget, $column)) : ''];
    }

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        return [];
    }
}
