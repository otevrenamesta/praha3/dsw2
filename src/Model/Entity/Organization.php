<?php

namespace App\Model\Entity;

use App\Controller\Component\IsdsComponent;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Email;
use Cake\Mailer\Transport\MailTransport;
use Cake\Mailer\TransportFactory;
use Cake\Utility\Hash;

/**
 * Organization Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Domain[] $domains
 * @property OrganizationSetting[] $organization_settings
 */
class Organization extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'test_id' => true,
        'modified' => true,
        'created' => true,
        'domains' => true,
        'organization_settings' => true,
    ];

    /**
     * @return void
     */
    public function storeConfiguration(): void
    {
        foreach ($this->organization_settings as $organization_setting) {
            $organization_setting->storeConfiguration();
        }
        $this->reconfigureEmails();
    }

    /**
     * @return void
     */
    public function reconfigureEmails(): void
    {
        foreach (Configure::read('EmailTransport') as $configName => $config) {
            if (!in_array($configName, TransportFactory::configured())) {
                if (empty($config['host'])) {
                    $config['className'] = MailTransport::class;
                }
                TransportFactory::setConfig($configName, $config);
            }
        }
        foreach (Configure::read('Email') as $configName => $config) {
            if (!in_array($configName, Email::configured())) {
                Email::setConfig($configName, $config);
            }
        }
    }

    /**
     * @param string $name setting ID, from OrganizationSetting constants
     * @param bool $return_default_value whether to return default value or null
     * @return bool|float|int|string|null
     */
    public function getSettingValue(string $name, bool $return_default_value = false)
    {
        $setting = $this->getSetting($name);

        return $setting === null ? ($return_default_value ? OrganizationSetting::getDefaultValue($name) : null) : $setting->deserialize();
    }

    /**
     * @param string $name setting ID, from OrganizationSetting constants
     * @return OrganizationSetting|null
     */
    public function getSetting(string $name): ?OrganizationSetting
    {
        foreach ($this->organization_settings as $setting) {
            if ($setting->name === $name) {
                return $setting;
            }
        }

        return null;
    }

    /**
     * Returns:
     * Reply-To address if configured
     * Current manager/admin user email, if override configured
     * Contact email otherwise
     *
     * Does not validate the returned email formal validity (filter email)
     *
     * @return string
     */
    public function getReplyToEmailAddress(): string
    {
        $isReplyToOverrideEnabled = $this->getSettingValue(OrganizationSetting::REPLY_TO_ADMIN_WHEN_NOTIFYING_USER, true);
        if ($isReplyToOverrideEnabled) {
            return Hash::get($_SESSION, 'Auth.User.email');
        }

        $configuredReplyTo = $this->getSettingValue(OrganizationSetting::EMAIL_REPLYTO, true);
        if (!empty(trim($configuredReplyTo))) {
            return $configuredReplyTo;
        }

        return $this->getSettingValue(OrganizationSetting::CONTACT_EMAIL, true) ?? '';
    }

    public function validateSettings(IsdsComponent $isds_component): array
    {
        $found_errors = [
            // field => error message
        ];

        $allowPaper = $this->getSettingValue(OrganizationSetting::ALLOW_MANUAL_SUBMIT_OF_REQUESTS);

        if (!$allowPaper && empty($this->getSettingValue(OrganizationSetting::DS_ATS_ID))) {
            $found_errors[OrganizationSetting::DS_ATS_ID] = __('ID Odesílací Brány / Autentizační služby - není nastaveno');
        }

        $ds_cert = $this->getSettingValue(OrganizationSetting::DS_CERTIFICATE);
        $ds_pkey = $this->getSettingValue(OrganizationSetting::DS_CERTKEY);
        if (!$allowPaper && empty($ds_cert)) {
            $found_errors[OrganizationSetting::DS_CERTIFICATE] = __('Certifikát pro ISDS není nastaven');
        } elseif (!empty($ds_cert)) {
            $ds_cert_filtered = $isds_component->filter_pem_certificate($ds_cert);
            if (!$ds_cert_filtered) {
                $found_errors[OrganizationSetting::DS_CERTIFICATE] = __('Certifikát pro ISDS není v pořádku, musí být ve formátu PEM (začínat -----BEGIN CERTIFICATE----- a končit -----END CERTIFICATE-----)');
            } else {
                $cert_parsed = openssl_x509_parse($ds_cert_filtered);
                $validFromTimestamp = intval(Hash::get($cert_parsed, 'validFrom_time_t', 0));
                $validToTimestamp = intval(Hash::get($cert_parsed, 'validTo_time_t', 0));
                if (!empty($validToTimestamp) && !empty($validFromTimestamp)) {
                    if ($validFromTimestamp > time()) {
                        $found_errors[OrganizationSetting::DS_CERTIFICATE] = __('Platnost certifikátu pro ISDS začíná v budoucnosti');
                    }
                    if ($validToTimestamp < time()) {
                        $found_errors[OrganizationSetting::DS_CERTIFICATE] = __('Platnost certifikátu pro ISDS již vypršela');
                    }
                }
            }
        }

        $ds_cert_ok = !isset($found_errors[OrganizationSetting::DS_CERTIFICATE]);
        if (!$allowPaper && empty($ds_pkey)) {
            $found_errors[OrganizationSetting::DS_CERTKEY] = __('Privátní klíč k certifikátu pro ISDS není nastaven');
        } else if ($ds_cert_ok && !empty($ds_pkey)) {
            $ds_pkey_filtered = $isds_component->filter_pem_privatekey($ds_pkey, $ds_cert);
            if (!$ds_pkey_filtered) {
                $found_errors[OrganizationSetting::DS_CERTKEY] = __('Privátní klíč k certifikátu pro ISDS není v pořádku. Je možné že nejde o privátní klíč k poskytnutému certifikátu, nebo není ve formátu PEM PKCS#8 unencrypted (začíná -----BEGIN PRIVATE KEY----- a končí -----END PRIVATE KEY-----)');
            }
        }

        return $found_errors;
    }
}
