<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Log\Log;
use InvalidArgumentException;

/**
 * File Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $filepath
 * @property int $filesize
 * @property string $original_filename
 * @property string|null $file_type
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property User $user
 * @property Attachment[] $attachments
 */
class File extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'filepath' => true,
        'filesize' => true,
        'original_filename' => true,
        'file_type' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'attachments' => true,
    ];

    /**
     * Moves file into /var/www/html/files/$user_id/`year`/`month`/`timestamp`
     *
     * @param array|null $filedata filedata as provided in Request->getData()
     * @param int $user_id id of user uploading the file
     * @return bool|string false if operation failed, filepath relative to folder files otherwise
     */
    public function fileUploadMove(?array $filedata, int $user_id)
    {
        if (empty($filedata) || $filedata['error'] !== UPLOAD_ERR_OK || $filedata['size'] <= 1 || $filedata['size'] > getMaximumFileUploadSize()) {
            throw new InvalidArgumentException();
        }
        $year = date('Y');
        $month = date('n');
        $timestamp = time();
        $targetDirectory = $this->getFileStoragePath() . $user_id . DS . $year . DS . $month . DS;
        $targetFile = $targetDirectory . $timestamp;
        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory, 0770, true);
        }

        // prevent simultaneous uploads from single form overwriting each other
        while (file_exists($targetFile)) {
            $targetFile = $targetDirectory . (++$timestamp);
        }

        if (move_uploaded_file($filedata['tmp_name'], $targetFile)) {
            return str_replace($this->getFileStoragePath(), '', $targetFile);
        }

        return false;
    }

    public function getExtendedDescription(): string
    {
        return sprintf("%s: %s, %s: %s",
            __('Název přílohy'),
            h($this->original_filename),
            __('Velikost'),
            Number::toReadableSize($this->filesize)
        );
    }

    /**
     * @return bool
     */
    public function deletePhysically(): bool
    {
        if (!empty($this->filepath)) {
            $filename = $this->getRealPath();
            if (is_file($filename)) {
                Log::debug('deletePhysically ' . $filename);
                return unlink($filename);
            }
        }

        return false;
    }

    public function getFileStoragePath(): string
    {
        return ROOT . DS . 'files' . DS;
    }

    public function getRealPath(): string
    {
        return $this->getFileStoragePath() . $this->filepath;
    }

    public function getLabel(): string
    {
        return sprintf("\"%s\"", h($this->original_filename));
    }

    public function isFilled(): bool
    {
        if ($this->isNew()) {
            return false;
        }
        if (empty($this->filepath) || $this->filesize < 1 || empty($this->original_filename)) {
            return false;
        }
        if (!is_file($this->getRealPath())) {
            Log::debug(sprintf("not valid %s", $this->getRealPath()));
        }
        return is_file($this->getRealPath());
    }
}
