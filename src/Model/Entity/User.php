<?php

namespace App\Model\Entity;

use App\Middleware\OrgDomainsMiddleware;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\UsersTable;
use Cake\Datasource\EntityInterface;
use Cake\Http\ServerRequest;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use InvalidArgumentException;
use OldDsw\Model\Entity\Ucet;
use App\Controller\TeamsManagerController;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string $mail_verification_code
 * @property bool $is_enabled
 * @property string $new_email
 * @property string $new_verification_code
 * @property string $original_email
 * @property string $nia_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property UserRole[] $user_roles
 * @property UsersToRole[] $users_to_roles
 * @property Team[] $teams
 * @property Ucet[] $stare_ucty
 * @property PublicIncomeHistory[] $public_income_histories
 * @property UsersToUser[] $allowed_users
 * @property UsersToUser[] $allowed_to_users
 * @property Request[] $requests
 */
class User extends AppEntity
{
    use LocatorAwareTrait;

    public const SESSION_ORIGIN_IDENTITY = 'origin_identity';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'salt' => true,
        'is_enabled' => true,
        'mail_verification_code' => true,
        'original_email' => true,
        'nia_id' => true,
        'modified' => true,
        'created' => true,
        'teams' => true,
        'users_to_roles' => true,
        'stare_ucty' => true,
        'allowed_users' => true,
        'allowed_to_users' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    /**
     * @param bool $setEmpty set null instead of generating random new one?
     * @return void
     */
    public function regenerateEmailToken(bool $setEmpty = false): void
    {
        $this->mail_verification_code = $setEmpty ? null : random_str('alphanum', 32);
        $this->setDirty('mail_verification_code');
        $this->getTableLocator()->get('Users')->save($this);
    }

    /**
     * @return void
     */
    public function regenerateSalt(): void
    {
        $this->salt = random_str('alphanum', 64);
        $this->setDirty('salt');
    }

    /**
     * @return bool
     */
    public function isSystemsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_SYSTEMS);
    }

    /**
     * @return bool
     */
    public function isPortalsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_PORTALS, OrgDomainsMiddleware::getCurrentOrganizationId());
    }

    /**
     * @return bool
     */
    public function isUsersManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_USERS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isGrantsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_GRANTS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isHistoryManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_HISTORIES, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isWikiManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_WIKIS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isEconomicsManager(): bool
    {
        return $this->hasRole(UserRole::MANAGER_ECONOMICS, OrgDomainsMiddleware::getCurrentOrganizationId()) || $this->isPortalsManager() || $this->isSystemsManager();
    }

    /**
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->hasRoles(UserRole::ALL_MANAGER, OrgDomainsMiddleware::getCurrentOrganizationId(), true) || $this->isSystemsManager();
    }

    /**
     * @param int $requested_role_id role id
     * @param int|null $organization_context id of organization, if the role must not be global
     * @return bool
     */
    public function hasRole(int $requested_role_id, ?int $organization_context = null): bool
    {
        if (!is_array($this->users_to_roles)) {
            return false;
        }
        foreach ($this->users_to_roles as $role_map) {
            if ($role_map->user_role_id != $requested_role_id) {
                continue;
            }
            if (empty($organization_context)) {
                return true;
            }
            if ($role_map->organization_id === $organization_context) {
                return true;
            }
            if ($requested_role_id === UserRole::MANAGER_SYSTEMS && empty($role_map->organization_id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $roles_ids id of roles that must be present
     * @param int|null $organization_context id of organization, if the roles must not be global
     * @param bool $atLeastOneRolePresent presence one of the requested roles suffices to return true
     * @return bool
     */
    public function hasRoles(array $roles_ids, ?int $organization_context = null, bool $atLeastOneRolePresent = false): bool
    {
        $rtn = true;
        foreach ($roles_ids as $roles_id) {
            $hasThisRole = $this->hasRole($roles_id, $organization_context);
            if ($atLeastOneRolePresent && $hasThisRole) {
                return true;
            }
            $rtn = $rtn && $hasThisRole;
        }

        return $rtn;
    }

    /**
     * @param int $requested_role_id id of requested role
     * @param bool $includeGlobal whether to include global roles (organization_id is null in such case)
     * @return array
     */
    public function getOrganizationIdsWhereUserHasRole(int $requested_role_id = UserRole::MANAGER_PORTALS, bool $includeGlobal = false): array
    {
        $orgs = [];
        foreach ($this->users_to_roles as $role_map) {
            if ($role_map->user_role_id === $requested_role_id && ($includeGlobal || !empty($role_map->organization_id))) {
                $orgs[] = $role_map->organization_id;
            }
        }

        return $orgs;
    }

    /**
     * @param int $requested_role_id id of requested role
     * @param bool $includeEmpty whether to include global roles (organization_id is null in such case)
     * @param array $fallbackRoles if requested role is not found, search for organizations with one of the fallback roles
     * @return array
     */
    public function getOrganizationIdsWhereUserHasRoleWithFallbacks(int $requested_role_id = UserRole::MANAGER_PORTALS, bool $includeEmpty = false, array $fallbackRoles = []): array
    {
        $orgs = $this->getOrganizationIdsWhereUserHasRole($requested_role_id, $includeEmpty);
        foreach ($fallbackRoles as $fallbackRole) {
            if (!empty($orgs)) {
                return $orgs;
            }
            $orgs = $this->getOrganizationIdsWhereUserHasRole($fallbackRole, $includeEmpty);
        }

        return $orgs;
    }

    /**
     * @param int $organization_id within which organization the portal-member role should be created
     * @return void
     */
    public function createMemberRoleIfNotExists(int $organization_id): void
    {
        if (!$this->hasRole(UserRole::USER_PORTAL_MEMBER, $organization_id)) {
            /** @var UsersToRole $role */
            $role = TableRegistry::getTableLocator()->get('UsersToRoles')->newEntity();
            if (!$this->isNew()) {
                $role->user_id = $this->id;
            }
            $role->organization_id = $organization_id;
            $role->user_role_id = UserRole::USER_PORTAL_MEMBER;
            if (!is_array($this->users_to_roles)) {
                $this->users_to_roles = [];
            }
            $this->users_to_roles[] = $role;
            $this->setDirty('users_to_roles');
        }
    }

    /**
     * @param string $newPassword plaintext password
     * @return void
     */
    public function setNewPassword(string $newPassword): void
    {
        while (empty($this->salt)) {
            $this->regenerateSalt();
        }
        $this->password = password_hash($newPassword . $this->salt, PASSWORD_ARGON2ID);
        $this->setDirty('password');
    }

    /**
     * @return int[]
     */
    public function getStareUctyIds(): array
    {
        $rtn = [];
        if (!empty($this->stare_ucty)) {
            foreach ($this->stare_ucty as $ucet) {
                $rtn[] = $ucet->id;
            }
        }

        return $rtn;
    }

    /**
     * Checks if in the current portal user has at least one team role assigned
     *
     * @return bool
     */
    public function hasAtLeastOneTeam(): bool
    {
        foreach ($this->teams ?? [] as $team) {
            if ($team->organization_id === OrgDomainsMiddleware::getCurrentOrganizationId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a particular user has a specific (or any) team role in any program (in any organisation)
     *
     * @param int $userId
     * @param string $role_name - aka comments_programs
     *
     * @return [type]
     */

    public static function userHasRoleInAnyProgram($userId, $role_name = null)
    {
        // Get an instance of the TeamsToUsers table.
        $locator = new TableLocator();
        $teamsToUsersTable = $locator->get('TeamsToUsers');

        // If role_name is null, just check if the user has any role in any program.
        if ($role_name === null) {
            $count = $teamsToUsersTable->find()
                ->where(['TeamsToUsers.user_id' => $userId])
                ->count();
            return ($count > 0);
        }

        $role_fields = TeamsManagerController::getTeamsRoles();
        $role = str_replace('_programs', '', $role_name);
        $roleId = array_search($role, $role_fields);

        // Perform the query for a specific role.
        $count = $teamsToUsersTable->find()
            ->matching('TeamsRolesInPrograms', function ($q) use ($roleId) {
                return $q->where(['TeamsRolesInPrograms.program_role_id' => $roleId]);
            })
            ->where(['TeamsToUsers.user_id' => $userId])
            ->count();

        return ($count > 0);
    }

    /**
     * @param mixed $team_id
     *
     * @return array
     *
     * Get a list of records from TeamsRolesInProgram table for a particular team
     */
    private function getTeamRolesInPrograms($team_id, $role_id = null): array
    {
        $teams_roles_in_program = TableRegistry::getTableLocator()->get('TeamsRolesInProgram');
        $conditions = [];
        $conditions['team_id'] = $team_id;
        if ($role_id) $conditions['program_role_id'] = $role_id;
        return $teams_roles_in_program->find('all', [
            'conditions' =>  $conditions
        ])->toArray();
    }

    private function fieldNameToRoleID($field_name)
    {
        $role_fields = TeamsManagerController::getTeamsRoles();
        $role = str_replace('_programs', '', $field_name);
        $role_id = array_search($role, $role_fields);
        return $role_id;
    }


    /**
     * @param string $team_field name of team role, such as formal_check_programs, see Entity\Team properties
     * @return Team[]
     */
    public function findTeamsWithProgramsIn(string $team_field): array
    {
        $teams = [];
        $role_id = $this->fieldNameToRoleID($team_field);
        if (!$role_id > 0 || !is_array($this->teams)) return $teams;
        foreach ($this->teams as $team) {
            if (OrgDomainsMiddleware::getCurrentOrganizationId() !== $team->organization_id) {
                continue;
            }
            if (!empty($this->getTeamRolesInPrograms($team->id, $role_id))) {
                $teams[] = $team;
            }
        }

        return $teams;
    }

    /**
     * @return bool
     */
    public function hasFormalControlTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('formal_check_programs'));
    }

    /**
     * @param string $field get ids of programs by team role name, such as formal_check_programs, see Entity\Teams properties
     * @return array
     */
    public function getProgramIdsForTeam(string $field)
    {
        $rtn = [];
        $teams = $this->findTeamsWithProgramsIn($field);
        foreach ($teams as $team) {
            $role_id = $this->fieldNameToRoleID($field);
            $results = $this->getTeamRolesInPrograms($team->id, $role_id);
            foreach ($results as $result) {
                $rtn[] = $result->program_id;
            }
        }
        array_unique($rtn);
        return array_unique($rtn);
    }

    /**
     * @return array
     */
    public function getAllTeamProgramIds()
    {
        $rtn = [];
        $role_fields = TeamsManagerController::getTeamsRoles();
        $old_field_names = [];
        foreach ($role_fields as $f) {
            $old_field_names[] = $f . '_programs';
        }
        foreach ($old_field_names as $team_field) {
            $rtn = array_merge($rtn, $this->getProgramIdsForTeam($team_field));
        }

        return array_unique($rtn);
    }

    /**
     * @return array
     */
    public function getProgramIdsForFormalControl(): array
    {
        return $this->getProgramIdsForTeam('formal_check_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdsForComments(): array
    {
        return $this->getProgramIdsForTeam('comments_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdsForProposals(): array
    {
        return $this->getProgramIdsForTeam('price_proposal_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdsForApprovals(): array
    {
        return $this->getProgramIdsForTeam('price_approval_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdsForManagers(): array
    {
        return $this->getProgramIdsForTeam('request_manager_programs');
    }

    /**
     * @return array
     */
    public function getProgramIdesForPreview(): array
    {
        return $this->getProgramIdsForTeam('preview_programs');
    }

    /**
     * @return bool
     */
    public function hasCommentsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('comments_programs'));
    }

    /**
     * @return bool
     */
    public function hasProposalsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('price_proposal_programs'));
    }

    /**
     * @return bool
     */
    public function hasApprovalsTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('price_approval_programs'));
    }

    /**
     * @return bool
     */
    public function hasManagersTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('request_manager_programs'));
    }

    /**
     * @return bool
     */
    public function hasPreviewTeam(): bool
    {
        return !empty($this->findTeamsWithProgramsIn('preview_programs'));
    }

    /**
     * @param bool $unlockedOnly return max version of unlocked identities only
     * @return int
     */
    public function getLastIdentityVersion(bool $unlockedOnly = true): int
    {
        if ($unlockedOnly === true) {
            $allLast = $this->getAllIdentity(null, true);
        } else {
            /** @var IdentitiesTable $identitiesTable */
            $identitiesTable = TableRegistry::getTableLocator()->get('Identities');
            $allLast = $identitiesTable->find('all', [
                'conditions' => [
                    'Identities.user_id' => $this->id,
                ],
            ]);
        }

        $maxVersion = 0;
        foreach ($allLast as $identity) {
            if ($identity->version > $maxVersion) {
                $maxVersion = $identity->version;
            }
        }

        return $maxVersion;
    }

    /**
     * @param int|null $version version of identity, as listed in appropriate link
     * @param bool $lastUnlockedVersion if provided $version is null or zero, this must be set to true
     * @return Identity[]|Query
     */
    public function getAllIdentity(?int $version = null, bool $lastUnlockedVersion = false): Query
    {
        if (empty($version) && $lastUnlockedVersion === false) {
            throw new InvalidArgumentException('if no version provided, you must request last unlocked version');
        }

        /** @var IdentitiesTable $identitiesTable */
        $identitiesTable = TableRegistry::getTableLocator()->get('Identities');

        $conditions = [
            'Identities.user_id' => $this->id,
        ];

        if ($lastUnlockedVersion === true) {
            $conditions['Identities.is_locked'] = false;
        } else {
            $conditions['Identities.version'] = $version;
        }

        /** @var Identity[] $identities */
        return $identitiesTable->find(
            'all',
            [
                'conditions' => $conditions,
            ]
        );
    }

    public function getIdentityMissingFields(?int $identity_version = null, ?int $request_type_id = null): ?array
    {
        $currentIdentityVersion = $identity_version ?? $this->getLastIdentityVersion(false);
        $identities = $identity_version === 0 ? [] : $this->getAllIdentity($currentIdentityVersion, empty($currentIdentityVersion));
        $flattened = [];
        $missing_fields = [];
        foreach ($identities as $identity) {
            $flattened[$identity->name] = $identity->value;
        }

        $requiredFields = Identity::getRequiredFieldIds(boolval($flattened[Identity::IS_PO] ?? false), intval($flattened[Identity::PO_CORPORATE_TYPE] ?? 0), $request_type_id);
        foreach ($requiredFields as $requiredFieldId) {
            if (!isset($flattened[$requiredFieldId]) || empty(trim($flattened[$requiredFieldId]))) {
                $missing_fields[] = $requiredFieldId;
            }
        }

        if (OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_REQUIRE_DS)) {
            if (empty($flattened[Identity::CONTACT_DATABOX])) {
                $missing_fields[] = Identity::CONTACT_DATABOX;
            }
        }

        if ($flattened[Identity::IS_PO] ?? false && OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_REQUIRE_PO_REGISTRATION_DETAILS)) {
            foreach ([Identity::PO_REGISTRATION_SINCE, Identity::PO_REGISTRATION_DETAILS] as $poRegistrationDetailField) {
                if (empty($flattened[$poRegistrationDetailField])) {
                    $missing_fields[] = $poRegistrationDetailField;
                }
            }
        }

        // mark invalid fields as missing
        if (!($flattened[Identity::IS_PO] ?? true)) {
            $ageIsValid = false;
            $minimumAge = OrganizationSetting::getSetting(OrganizationSetting::IDENTITY_FO_MINIMUM_AGE, true);
            $configuredAge = $flattened[Identity::FO_DATE_OF_BIRTH] ?? null;
            if ($configuredAge) {
                $parsedDate = FrozenDate::parseDate($configuredAge, 'Y-m-d');
                if ($parsedDate->diffInYears(FrozenDate::now(), false) >= $minimumAge) {
                    $ageIsValid = true;
                }
            }
            if (!$ageIsValid) {
                $missing_fields[] = Identity::FO_DATE_OF_BIRTH;
            }
        }

        return $missing_fields;
    }

    /**
     * @param int|null $identity_version
     * @return bool
     */
    public function isIdentityCompleted(?int $identity_version = null): bool
    {
        return empty($this->getIdentityMissingFields($identity_version));
    }

    public function getOriginIdentity(ServerRequest $request): User
    {
        $origin_data = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);

        return $origin_data instanceof User ? $origin_data : $this;
    }

    /**
     * returns false, if there is already origin identity set
     * returns true, if no origin identity found or if it matches the one passed in arguments
     *
     * @param ServerRequest $request
     * @param User|EntityInterface $originIdentity
     * @return bool
     */
    public function setOriginIdentity(ServerRequest $request, User $originIdentity): bool
    {
        $origin_data = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);
        if (($origin_data instanceof User && $origin_data->id === $originIdentity->id) || empty($origin_data)) {
            $request->getSession()->write(self::SESSION_ORIGIN_IDENTITY, $originIdentity);

            return true;
        }

        return false;
    }

    /**
     * Checks whether
     *
     * @param ServerRequest $request
     * @param int $user_id
     * @return bool
     */
    public function canSwitchToUser(ServerRequest $request, int $user_id): bool
    {
        $identity = $request->getSession()->read(self::SESSION_ORIGIN_IDENTITY);
        if (!($identity instanceof User)) {
            // if no origin identity set, retrieve the session one
            $identity = $request->getSession()->read('Auth.User');
        } elseif ($identity->id === $user_id) {
            // if origin identity matches the requested one
            return true;
        }
        $identity_object = json_decode(json_encode($identity), FALSE);
        foreach ($identity_object->allowed_users ?? [] as $allowed_user) {
            if ($allowed_user->user_id === $user_id) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ServerRequest $request
     * @param User|EntityInterface $target_user
     * @return bool
     */
    public function switchToUser(ServerRequest $request, User $target_user): bool
    {
        $origin = $this->getOriginIdentity($request);
        $hasAccess = $origin->canSwitchToUser($request, $target_user->id);
        if (!$hasAccess) {
            return false;
        }

        if ($this->setOriginIdentity($request, $origin)) {
            $request->getSession()->write('Auth.User', $target_user);

            return true;
        }

        return false;
    }

    /**
     * Patches allowed_to_users list for saving with association,
     * returns array of UsersToUser entities, meant to be removed if patch saving proceeds
     *
     * @param array $newEmails
     * @param UsersTable $usersTable
     * @param string $formField
     * @return array
     */
    public function patchAllowedToUsers(array $newEmails, UsersTable $usersTable, string $formField = 'allowed_users_emails'): array
    {
        $this->setErrors([$formField => []], true);
        $userEntities = [];
        $links = [];
        $toRemove = [];
        // filter and push validation errors on each invalid newEmails value
        foreach ($newEmails as $email) {
            $email = trim($email);
            if (empty(filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE))) {
                $this->setErrors([$formField => sprintf(__('"%s" Není platná e-mailová adresa'), h($email))]);
                break;
            } elseif ($email === $this->email) {
                $this->setErrors([$formField => __('Nemůžete sdílet účet sami se sebou')]);
                break;
            }
            $user_by_email = $usersTable->find('all', ['conditions' => ['Users.email' => $email]])->first();
            if (empty($user_by_email)) {
                $this->setErrors([$formField => sprintf(__('Uživatel s e-mailem %s nebyl nalezen'), $email)]);
                break;
            }
            $userEntities[$user_by_email->id] = $user_by_email;
            $links[$user_by_email->id] = false;
        }
        // check if the current links are still valid or meant to be removed
        foreach ($this->allowed_to_users ?? [] as $index => $existing_link) {
            if ($existing_link->user_id !== $this->id) {
                $links[$existing_link->allowed_user_id] = true;
            } elseif (in_array($existing_link->allowed_user_id, array_keys($links), true)) {
                $links[$existing_link->allowed_user_id] = true;
            } else {
                $toRemove[] = $existing_link;
                unset($this->allowed_to_users[$index]);
            }
        }
        // patch local allowed_to_users field to contain new allowed values
        foreach ($links as $allowed_user_id => $isAlreadySet) {
            if (!$isAlreadySet) {
                $newLink = $usersTable->AllowedUsers->newEntity([
                    'user_id' => $this->id,
                    'allowed_user_id' => $allowed_user_id,
                ]);
                $newLink->allowed_user = $userEntities[$allowed_user_id];
                $this->allowed_to_users[] = $newLink;
            }
        }
        $this->setDirty('allowed_to_users');

        return $toRemove;
    }

    public function loadRequests(): self
    {
        if (!is_array($this->requests)) {
            $usersTable = $this->getTableLocator()->get('Users');
            $usersTable->loadInto($this, ['Requests']);
        }

        return $this;
    }

    public function getMailVerificationCode(): string
    {
        if (empty($this->mail_verification_code)) {
            $this->regenerateEmailToken();
        }
        return $this->mail_verification_code;
    }

    public function getPublicIncomeHistorySum(string $year_spec = 'all', int $org_sum = null, bool $system_sum = false): int
    {
        $sum = 0;
        $target_year = ($year_spec === 'all' ? null : intval($year_spec));
        $orgRange = (int)OrgDomainsMiddleware::getCurrentOrganization()->getSettingValue(OrganizationSetting::SUBSIDY_HISTORY_YEARS, true);
        $minYear = (int)date('Y') - max(0, $orgRange);

        foreach ($this->public_income_histories ?? [] as $publicIncomeHistory) {
            if ($system_sum && !$publicIncomeHistory->added_by_system) {
                // continue if record is not from a system source
                continue;
            }

            $fiscal_year = isset($publicIncomeHistory->fiscal_year) ? (int)$publicIncomeHistory->fiscal_year : null;
            if (!$fiscal_year || $fiscal_year < $minYear || $fiscal_year > (int)date('Y')) {
                // out of range of required support years
                continue;
            }

            $org_id = isset($publicIncomeHistory->public_income_source->organization_id)
                ? intval($publicIncomeHistory->public_income_source->organization_id)
                : null;
            if ($target_year === null || $target_year === $fiscal_year) {
                $sum += ((!$org_sum || ($org_sum && $org_sum === $org_id))
                    ? $publicIncomeHistory->amount_czk
                    : 0);
            }
        }

        return $sum;
    }

    public function canSwitchUsers(ServerRequest $request): bool
    {
        return !empty($this->getOriginIdentity($request)->allowed_users);
    }

    /**
     * @param ServerRequest $request
     * @return int[]
     */
    public function getUserIdsForSwitch(ServerRequest $request): array
    {  // pr( $this->getOriginIdentity($request)->allowed_users );
        $rtn = [];
        foreach ($this->getOriginIdentity($request)->allowed_users ?? [] as $share_link) {
            $rtn[] = $share_link->user_id;
        }
        return $rtn;
    }

    public function getEmailRecipients(?int $identity_version = null, bool $justCC = true): array
    {
        $emails = [];

        if (!$justCC) {
            $emails[$this->email] = true;
        }

        $identity_flat = $this->getAllIdentity($identity_version)->combine('name', 'value')->toArray();

        if (
            boolval($identity_flat[Identity::STATUTORY_SEND_EMAIL_NOTIFICATIONS] ?? false) &&
            filter_var($identity_flat[Identity::STATUTORY_EMAIL] ?? null, FILTER_VALIDATE_EMAIL)
        ) {
            $emails[$identity_flat[Identity::STATUTORY_EMAIL]] = true;
        }

        if ($justCC && filter_var($identity_flat[Identity::CONTACT_EMAIL] ?? null, FILTER_VALIDATE_EMAIL)) {
            $emails[$identity_flat[Identity::CONTACT_EMAIL]] = true;
        }

        if (filter_var($identity_flat[Identity::CONTACT_EMAIL2] ?? null, FILTER_VALIDATE_EMAIL)) {
            $emails[$identity_flat[Identity::CONTACT_EMAIL2]] = true;
        }

        if (filter_var($identity_flat[Identity::CONTACT_EMAIL3] ?? null, FILTER_VALIDATE_EMAIL)) {
            $emails[$identity_flat[Identity::CONTACT_EMAIL3]] = true;
        }

        return array_keys($emails);
    }

    public function getAllTeamIds(bool $include_names = false): array
    {
        $team_ids = [];
        $role_fields = TeamsManagerController::getTeamsRoles();
        $old_field_names = [];
        foreach ($role_fields as $f) {
            $old_field_names[] = $f . '_programs';
        }
        foreach ($old_field_names as $team_field) {
            foreach ($this->findTeamsWithProgramsIn($team_field) as $team) {
                $team_ids[$team->id] = $team->name;
            }
        }

        return $include_names ? $team_ids : array_keys($team_ids);
    }

    public function getAllRoleIds(bool $include_names = false): array
    {
        $roles_ids = [];
        foreach ($this->users_to_roles as $users_to_role) {
            if ($users_to_role->organization_id === OrgDomainsMiddleware::getCurrentOrganizationId()) {
                $roles_ids[$users_to_role->user_role_id] = $users_to_role->user_role->role_name ?? 'N/A';
            }
        }
        return $include_names ? $roles_ids : array_keys($roles_ids);
    }
}
