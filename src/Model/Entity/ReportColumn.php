<?php

namespace App\Model\Entity;

use App\Model\ClassReportInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenTime;

/**
 * ReportColumn Entity
 *
 * @property int $id
 * @property int $report_id
 * @property int $agendio_column_id
 * @property string $name
 * @property string $path
 * @property int $order
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Report $report
 */
class ReportColumn extends AppEntity
{

    public const ENTITIES_WITH_REPORT_INTERFACE = [
        Identity::class,
        Request::class,
        RequestBudget::class,
        // this is not getting listed, but implements the interface Form::class,
    ];

    public static function getEntityColumnsLabel($class): string
    {
        switch ($class) {
            case Identity::class:
                return __('Identita žadatele');
            case Request::class:
                return __('Žádost');
            case RequestBudget::class:
                return __('Rozpočet žádosti');
            case Form::class:
                // not used
                return 'N/A';
        }
        throw new NotFoundException('getEntityColumnsLabel failed');
    }

    public static function getOptionPrefix($class): string
    {
        switch ($class) {
            case Identity::class:
                return 'identity';
            case Request::class:
                return 'request';
            case RequestBudget::class:
                return 'request_budget';
            case Form::class:
                return 'form';
        }
        throw new NotFoundException('getOptionPrefix failed');
    }

    public static function getEntityByPrefix(string $path): ClassReportInterface
    {
        $prefix = explode('|', $path);
        $prefix = $prefix[0];
        switch ($prefix) {
            case 'identity':
                return self::getEntityByClassName(Identity::class);
            case 'request':
                return self::getEntityByClassName(Request::class);
            case 'request_budget':
                return self::getEntityByClassName(RequestBudget::class);
            case 'form':
                return self::getEntityByClassName(Form::class);
        }
        throw new NotFoundException('getEntityByPrefix failed');
    }

    public static function extractPrefix(string $path): array
    {
        return explode('|', $path);
    }

    public static function getEntityByClassName($name): ClassReportInterface
    {
        switch ($name) {
            case Identity::class:
                return new Identity;
            case Request::class:
                return new Request;
            case RequestBudget::class:
                return new RequestBudget;
            case Form::class:
                return new Form;
        }
        throw new NotFoundException("getEntityByClassName failed");
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'report_id' => true,
        'agendio_column_id' => true,
        'name' => true,
        'path' => true,
        'order' => true,
        'modified' => true,
        'created' => true,
        'report' => true,
    ];

    public static function prefixColumnOptions(array $reportColumnsDefinition, string $entityClassName): array
    {
        $classPrefix = self::getOptionPrefix($entityClassName);
        foreach ($reportColumnsDefinition as $key => $value) {
            if (is_string($value)) {
                $reportColumnsDefinition[$classPrefix . '|' . $key] = $value;
                unset($reportColumnsDefinition[$key]);
            } else if (is_array($value)) {
                $reportColumnsDefinition[$key] = self::prefixColumnOptions($value, $entityClassName);
            }
        }
        return $reportColumnsDefinition;
    }
}
