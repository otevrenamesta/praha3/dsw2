<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * Program Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $realm_id
 * @property null|int $evaluation_criteria_id
 * @property null|int $formal_check_team_id
 * @property null|int $preview_team_id
 * @property null|int $price_proposal_team_id
 * @property null|int $price_approval_team_id
 * @property int $project_budget_design_id
 * @property bool $requires_budget
 * @property bool $requires_balance_sheet
 * @property bool $requires_extended_budget
 * @property bool $requires_pre_evaluation
 * @property bool $budget_hide_earnings
 * @property bool $not_require_tax_documents
 * @property bool $merging_of_requests
 * @property string $name
 * @property string|null $description
 * @property weight $weight
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property string|null $settlement_date
 *
 * @property Program $parent_program
 * @property Realm $realm
 * @property Appeal[] $appeals
 * @property Program[] $child_programs
 * @property EvaluationCriterium $evaluation_criterium
 * @property Form[] $forms
 * @property Form[] $paper_forms
 * @property Request[] $requests

 */
class Program extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'realm_id' => true,
        'name' => true,
        'description' => true,
        'weight' => true,
        'modified' => true,
        'created' => true,
        'parent_program' => true,
        'realm' => true,
        'appeals' => true,
        'child_programs' => true,
        'evaluation_criteria_id' => true,
        'forms' => true,
        'requires_budget' => true,
        'requires_balance_sheet' => true,
        'requires_extended_budget' => true,
        'requires_pre_evaluation' => true,
        'not_require_tax_documents' => true,
        'merging_of_requests' => true,
        'project_budget_design_id' => true,
        'paper_forms' => true,
        'budget_hide_earnings' => true,
        'settlement_date' => true,
    ];

    public function getDataSection($separator = '||')
    {
        $titleFond = '  [' . __('fond') . ', #' . ($this->realm->fond ? $this->realm->fond->id : '')  . ']';
        $titleRealm = '  [' . __('oblast') . ', #' . ($this->realm ? $this->realm->id : '')  . ']';
        $titleProgram = '  [' . __('program') . ', #' . ($this->parent_program ? $this->parent_program->id : '')  . ']';

        $realm = $this->realm ? $this->realm->name . $titleRealm : null;
        $fond = null;
        if ($realm) {
            $fond = $this->realm->fond ? $this->realm->fond->name . $titleFond . $separator : null;
        }
        $parent = $this->parent_program ? $this->parent_program->name . $titleProgram : null;

        return trim($fond) . trim($realm) . ($parent ? $separator . trim($parent) : null);
    }
}
