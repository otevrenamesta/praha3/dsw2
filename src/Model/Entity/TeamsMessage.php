<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * TeamsMessage Entity
 *
 * @property int $id
 * @property string $message
 * @property int $organization_id
 * @property int $recipient_user_id
 * @property int $user_id
 * @property int $response_to_id
 * @property FrozenTime|null $readed
 * @property FrozenTime|null $created
 *
 * @property User $user
 */
class TeamsMessage extends AppEntity
{
    /**
     * @var array
     */
    protected $_accessible = [
        'message' => true,
        'organization_id' => true,
        'recipient_user_id' => true,
        'user_id' => true,
        'response_to_id' => true,
        'readed' => true,
        'created' => true,
    ];
}
