<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;

/**
 * RequestType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request[] $requests
 */
class RequestType extends AppEntity
{
    public const STANDARD_REQUEST = 1,
        PAPER_REQUEST = 2;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'modified' => true,
        'created' => true,
        'requests' => true,
    ];

    public static function canUserEditRequest(int $request_type_id): bool
    {
        switch ($request_type_id) {
            default:
                return false;
            case self::STANDARD_REQUEST:
                return true;
        }
    }

    public static function getLabel(int $request_type_id, bool $short = false): string
    {
        switch ($request_type_id) {
            default:
                return $short ? __('Neznámá') : __('Neznámý typ žádosti');
            case self::STANDARD_REQUEST:
                return $short ? __('Elektronická') : __('Standardní elektronická žádost (plná elektronická evidence)');
            case self::PAPER_REQUEST:
                return $short ? __('Papírová') : __('Papírová žádost (částečná elektronická evidence)');
        }
    }
}
