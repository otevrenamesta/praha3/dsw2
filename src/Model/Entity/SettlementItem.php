<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * SettlementItem Entity
 *
 * @property int $id
 * @property int $settlement_id
 * @property FrozenDate $paid_when
 * @property float $amount
 * @property float $original_amount
 * @property float $own_amount
 * @property string $description - název položky vyúčtování
 * @property string $note - vysvětlivka / poznámka k položce vyúčtování
 * @property int|null $order_number
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 * @property int $approved
 * @property null|string $approved_note
 * @property null|string budget_item
 *
 * @property Settlement $settlement
 * @property SettlementAttachment[] $settlement_attachments
 */
class SettlementItem extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'settlement_id' => true,
        'description' => true,
        'order_number' => true,
        'modified' => true,
        'created' => true,
        'settlement' => true,
        'settlement_attachments' => true,
        'amount' => true,
        'original_amount' => true,
        'own_amount' => true,
        'paid_when' => true,
        'note' => true,
        'approved' => true,
        'approved_note' => true,
        'budget_item' => true,
    ];

    public function sortAttachments(): array
    {
        $sorted = [];
        $touched = [];
        $originals = $this->settlement_attachments ?? [];

        // first invoice
        foreach ($originals as $attachment) {
            if (in_array($attachment->settlement_attachment_type_id, SettlementAttachmentType::TYPE_1_INVOICES, true)) {
                $sorted[0] = $attachment;
                $touched[] = $attachment->id;
                break;
            }
        }
        // then proof
        foreach ($originals as $attachment) {
            if (in_array($attachment->settlement_attachment_type_id, SettlementAttachmentType::TYPE_2_PROOF, true)) {
                $sorted[1] = $attachment;
                $touched[] = $attachment->id;
                break;
            }
        }
        // then all the others
        foreach ($originals as $attachment) {
            if (!in_array($attachment->id, $touched, true)) {
                $sorted[] = $attachment;
            }
        }

        $this->settlement_attachments = $sorted;
        return $sorted;
    }

    public function getAttachment(int $order_number): ?SettlementAttachment
    {
        $this->sortAttachments();
        return $this->settlement_attachments[$order_number] ?? null;
    }
}
