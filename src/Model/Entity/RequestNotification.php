<?php

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;

/**
 * RequestNotification Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $request_notification_type_id
 * @property FrozenDate|null $date_from
 * @property FrozenDate $date_end
 * @property int $costs_czk
 * @property int $participants_count
 * @property string $location
 * @property string $garant_name
 * @property string $garant_phone
 * @property string $garant_email
 * @property string|null $comment
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property RequestNotificationType $request_notification_type
 * @property RequestNotificationsToFile[] $request_notifications_to_files
 * @property File[] $files
 */
class RequestNotification extends AppEntity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'request_notification_type_id' => true,
        'date_from' => true,
        'date_end' => true,
        'costs_czk' => true,
        'participants_count' => true,
        'location' => true,
        'garant_name' => true,
        'garant_phone' => true,
        'garant_email' => true,
        'comment' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'request_notification_type' => true,
        'request_notifications_to_files' => true,
        'files' => true,
    ];
    
    public function getRequest() {
        
        return $this->request;
    }

    public function getRequesterName(): string
    {
        $request = $this->getRequest();
        $name = "";
        if ($request instanceof Request) {
            $flatIdentity = $request->getFlatIdentities();
            $name .= Identity::getIdentityFullName($flatIdentity);
        } elseif ($request instanceof Zadost) {
            if ($request->ucet) {
                $name .= $request->ucet->nazev;
            }
        }
        return $name;
    }
}