<?php

namespace App\Model\Entity;

use Cake\Database\Type;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\Utility\Hash;
use DateTimeInterface;
use Exception;

/**
 * FormFieldType Entity
 *
 * @property int $id
 * @property string $name
 *
 * @property FormField[] $form_fields
 */
class FormFieldType extends AppEntity
{
    public const FIELD_TEXT = 1,
        FIELD_VARCHAR = 2,
        FIELD_INTEGER = 3,
        FIELD_DOUBLE = 4,
        FIELD_YES_NO = 5,
        FIELD_CHECKBOX = 6,
        FIELD_DATE = 7,
        FIELD_DATETIME = 8,
        FIELD_FILE = 9,
        FIELD_FILE_MULTIPLE = 16,
        FIELD_TITLE_NOT_INTERACTIVE = 10,
        FIELD_TEXT_NOT_INTERACTIVE = 11,
        FIELD_CHOICES = 12,
        FIELD_TABLE = 13,
        FIELD_CHOICES_MULTIPLE = 14,
        FIELD_CHOICES_DROPDOWN = 15,
        FIELD_YES_NO_ATTACHMENT = 17;

    public const RADIO_YES_NO_KEYS = [
        'yes',
        'no',
    ];

    public const TABLE_COLUMN_TYPES = [
        0 => 'Sloupec nezobrazovat',
        1 => 'Text',
        2 => 'Celé číslo',
        3 => 'Číslo s desetinnými místy',
    ];
    public const TABLE_COLUMN_TYPES_FORM_CONTROL_TYPE = [
        1 => 'string',
        2 => 'number',
        3 => 'number',
    ];

    public const DATETIME_FORMAT = 'Y-m-d H:i:s';
    public const DATETIME_FORMAT_00 = 'Y-m-d H:i:00';
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'form_fields' => true,
    ];

    /**
     * @param int $form_field_type_id field type id
     * @param string|null $value serialized string value of the field
     * @param FormField|null $fieldDefinition relevant field definition
     * @return bool|FrozenDate|FrozenTime|float|int|string|null
     */
    public static function parseValue(int $form_field_type_id, ?string $value, ?FormField $fieldDefinition = null)
    {
        switch ($form_field_type_id) {
            default:
                return 'unknown';
            case self::FIELD_TEXT:
                return $value;
            case self::FIELD_VARCHAR:
                return mb_substr($value ?? '', 0, 255);
            case self::FIELD_INTEGER:
                // for file return the file id
            case self::FIELD_YES_NO_ATTACHMENT:
            case self::FIELD_FILE:
                return intval($value);
            case self::FIELD_FILE_MULTIPLE:
                if (!$value) return [];
                try {
                    $out = unserialize($value);
                } catch (Exception $e) {
                    return [];
                }
                return $out;
            case self::FIELD_DOUBLE:
                return round(floatval($value), 2);
            case self::FIELD_CHECKBOX:
                return boolval($value);
            case self::FIELD_YES_NO:
                return in_array($value, self::RADIO_YES_NO_KEYS) ? $value : null;
            case self::FIELD_DATETIME:
                try {
                    return FrozenTime::createFromFormat(self::DATETIME_FORMAT, $value);
                } catch (Exception $e) {
                    Log::error($e);
                }

                return null;
            case self::FIELD_DATE:
                try {
                    return FrozenDate::createFromFormat(self::DATE_FORMAT, $value);
                } catch (Exception $e) {
                    Log::error($e);
                }

                return null;
            case self::FIELD_CHOICES_DROPDOWN:
            case self::FIELD_CHOICES:
                if (!empty($fieldDefinition) && in_array($value, $fieldDefinition->getChoices(), true)) {
                    return array_search($value, $fieldDefinition->getChoices(), true);
                }

                return null;
            case self::FIELD_TABLE:
            case self::FIELD_CHOICES_MULTIPLE:
                try {
                    return unserialize($value);
                } catch (Exception $e) {
                    // Log::error($e);
                }

                return null;
        }
    }

    /**
     * @param int $form_field_type_id field type id
     * @param mixed $value provided by user by filling out HTML form
     * @param FormField|null $fieldDefinition relevant field definition
     * @return bool|int|mixed|string|null
     */
    public static function serializeValue(int $form_field_type_id, $value, ?FormField $fieldDefinition = null)
    {
        switch ($form_field_type_id) {
            default:
                return 'unknown';
            case self::FIELD_TEXT:
            case self::FIELD_VARCHAR:
                return $value;
            case self::FIELD_INTEGER:
                return strval(intval($value));
            case self::FIELD_DOUBLE:
                return strval(round(floatval($value), 2));
            case self::FIELD_CHECKBOX:
                return boolval($value);
            case self::FIELD_YES_NO:
                return in_array($value, self::RADIO_YES_NO_KEYS) ? $value : null;
            case self::FIELD_YES_NO_ATTACHMENT:
            case self::FIELD_FILE:
                if (is_array($value)) {
                    return null;
                }
                if (strval(intval($value)) === strval($value)) {
                    return intval($value) > 0 ? intval($value) : null;
                }
                return null;
            case self::FIELD_FILE_MULTIPLE:
                if (!is_array($value)) {
                    return null;
                }
                return serialize($value);
            case self::FIELD_DATETIME:
                $parsed = Type::build('datetime')->marshal($value);

                return $parsed instanceof DateTimeInterface
                    ? $parsed->format(self::DATETIME_FORMAT)
                    : (is_string($parsed) ? date(self::DATETIME_FORMAT_00, strtotime($parsed)) : null);
            case self::FIELD_DATE:
                $parsed = Type::build('date')->marshal($value);

                return $parsed instanceof DateTimeInterface ? $parsed->format(self::DATE_FORMAT) : null;
            case self::FIELD_CHOICES_DROPDOWN:
            case self::FIELD_CHOICES:
                if (!empty($fieldDefinition)) {
                    if (($key = array_search($value, $fieldDefinition->getChoices(), true)) !== false) {
                        return $fieldDefinition->getChoices()[$key];
                    }
                    if (in_array(intval($value), array_keys($fieldDefinition->getChoices()), true)) {
                        return $fieldDefinition->getChoices()[intval($value)];
                    }
                }

                return null;
            case self::FIELD_TABLE:
            case self::FIELD_CHOICES_MULTIPLE:
                if (is_array($value)) {
                    return serialize($value);
                }

                return null;
        }
    }

    /**
     * @param int $form_field_type_id form field type
     * @param string|null $value value of the field
     * @param FormField|null $definitionField relevant definition
     * @param array $options ie. 'file' for uploaded file, expects Entity/File object
     * @return bool
     */
    public static function isFilled(int $form_field_type_id, ?string $value, ?FormField $definitionField = null, array $options = [])
    {
        $parsedValue = self::parseValue($form_field_type_id, $value, $definitionField);
        $serializedParsedValue = self::serializeValue($form_field_type_id, $parsedValue, $definitionField);

        if ($value === null) {
            return false;
        }

        switch ($form_field_type_id) {
            default:
                return false;
            case self::FIELD_TEXT:
            case self::FIELD_VARCHAR:
                return mb_strlen(trim($value)) !== 0;
            case self::FIELD_INTEGER:
            case self::FIELD_DOUBLE:
                return $value === $serializedParsedValue;
            case self::FIELD_CHECKBOX:
                //return in_array($value, [self::serializeValue($form_field_type_id, true), self::serializeValue($form_field_type_id, false)]);
                return in_array($value, [self::serializeValue($form_field_type_id, true)]);
            case self::FIELD_YES_NO:
                return in_array($value, self::RADIO_YES_NO_KEYS);
            case self::FIELD_YES_NO_ATTACHMENT:
            case self::FIELD_FILE:
                // also validate that uploaded file exists
                $assignedFile = $options['file'] ?? null;
                return $parsedValue > 0 && $assignedFile instanceof File && $assignedFile->isFilled();
            case self::FIELD_FILE_MULTIPLE:
                return is_array($parsedValue) && !empty($parsedValue);
            case self::FIELD_DATETIME:
            case self::FIELD_DATE:
            case self::FIELD_CHOICES:
            case self::FIELD_CHOICES_DROPDOWN:
            case self::FIELD_CHOICES_MULTIPLE:
                return $serializedParsedValue === $value;
            case self::FIELD_TABLE:
                return is_array($parsedValue) && !empty($parsedValue);
        }
    }

    /**
     * @param int $form_field_type_id form field type
     * @return bool
     */
    public static function canBeFilled(int $form_field_type_id)
    {
        switch ($form_field_type_id) {
            default:
                // this will fail form filled validation on not-implemented field types
                return true;
            case self::FIELD_TEXT:
            case self::FIELD_VARCHAR:
            case self::FIELD_INTEGER:
            case self::FIELD_DOUBLE:
            case self::FIELD_CHECKBOX:
            case self::FIELD_YES_NO:
            case self::FIELD_YES_NO_ATTACHMENT:
            case self::FIELD_FILE:
            case self::FIELD_FILE_MULTIPLE:
            case self::FIELD_DATETIME:
            case self::FIELD_DATE:
            case self::FIELD_CHOICES:
            case self::FIELD_CHOICES_DROPDOWN:
            case self::FIELD_CHOICES_MULTIPLE:
            case self::FIELD_TABLE:
                return true;
            case self::FIELD_TEXT_NOT_INTERACTIVE:
            case self::FIELD_TITLE_NOT_INTERACTIVE:
                return false;
        }
    }

    /**
     * @return string
     */
    public function getSelfFormHelperType(): string
    {
        return self::getFormHelperType($this->id);
    }

    /**
     * @param int $form_field_type_id form field type
     * @return string
     */
    public static function getFormHelperType(int $form_field_type_id): string
    {
        switch ($form_field_type_id) {
            default:
                return 'unknown';
            case self::FIELD_TEXT:
                return 'textarea';
            case self::FIELD_VARCHAR:
                return 'text';
            case self::FIELD_INTEGER:
            case self::FIELD_DOUBLE:
                return 'number';
            case self::FIELD_CHECKBOX:
                return 'checkbox';
            case self::FIELD_YES_NO:
            case self::FIELD_CHOICES:
                return 'radio';
            case self::FIELD_YES_NO_ATTACHMENT:
                return 'file';
            case self::FIELD_FILE:
                return 'file';
            case self::FIELD_DATETIME:
                return 'datetime';
            case self::FIELD_DATE:
                return 'date';
            case self::FIELD_CHOICES_MULTIPLE:
                return 'select';
            case self::FIELD_CHOICES_DROPDOWN:
                return 'select';
        }
    }

    public static function configureTableField(FormField $field, ?array $data = null): FormField
    {
        if ($field->form_field_type_id !== self::FIELD_TABLE) {
            $field->table_settings = null;

            return $field;
        }

        $rowsLabels = [];
        $rowLabel = strtok($data['table_rows_labels'] ?? '', PHP_EOL);
        while ($rowLabel !== false) {
            if (!empty(trim($rowLabel))) {
                $rowsLabels[] = trim($rowLabel);
            }
            $rowLabel = strtok(PHP_EOL);
        }
        $rowCount = count($rowsLabels) > 0 ? count($rowsLabels) : intval($data['table_rows_count']);

        $columnsFlat = [];
        $validColumnsCounter = 0;
        for ($i = 0; $i < 10; $i++) {
            $titleKey = sprintf('columns.%d.title', $i);
            $typeKey = sprintf('columns.%d.type', $i);
            $titleValue = Hash::get($data, $titleKey);
            $typeValue = Hash::get($data, $typeKey);
            if (empty(trim($titleValue)) || intval($typeValue) === 0) {
                continue;
            }
            $columnsFlat[sprintf('columns.%d.title', $validColumnsCounter)] = $titleValue;
            $columnsFlat[sprintf('columns.%d.type', $validColumnsCounter)] = $typeValue;
            $validColumnsCounter++;
        }

        $tableSettings = Hash::expand($columnsFlat);
        $tableSettings['table_rows_count'] = $rowCount;
        $tableSettings['table_rows_labels'] = $rowsLabels;
        $tableSettings['table_show_sums'] = boolval(Hash::get($data, 'table_show_sums')) === true;
        $tableSettings['table_show_sums_label'] = $tableSettings['table_show_sums'] === true ? $data['table_show_sums_label'] : null;
        $tableSettings['table_rows_header'] = count($rowsLabels) > 0 ? mb_substr($data['table_rows_header'] ?? '', 0, 255) : null;
        //dd($tableSettings);

        $field->table_settings = serialize($tableSettings);

        if ($rowCount < 1) {
            $field->setError('table_rows_count', __('Musíte zadat počet řádků nebo popisky řádků'));
        }
        if ($validColumnsCounter < 1) {
            $field->setError('columns', __('Tabulka musí obsahovat alespoň 1 sloupec'));
        }

        return $field;
    }

    public static function extractTableSettings(FormField $form_field, bool $raw = false)
    {
        if ($form_field->form_field_type_id !== FormFieldType::FIELD_TABLE) {
            return;
        }
        $settings = unserialize($form_field->table_settings);
        if (is_array($settings)) {
            foreach ($settings as $key => $val) {
                switch ($key) {
                    default:
                        $form_field->set($key, $val);
                        break;
                    case 'table_rows_labels':
                        $form_field->set($key, $raw === false && is_array($val) ? join(PHP_EOL, $val) : $val);
                        break;
                }
            }
        }
    }

    public static function extractChoices(FormField $form_field): void
    {
        if (!in_array(
            $form_field->form_field_type_id,
            [FormFieldType::FIELD_CHOICES, FormFieldType::FIELD_CHOICES_DROPDOWN, FormFieldType::FIELD_CHOICES_MULTIPLE],
            true
        )) {
            return;
        }

        if (!is_string($form_field->choices)) {
            return;
        }

        $form_field->choices = unserialize($form_field->choices);
        if (!is_array($form_field->choices)) {
            $form_field->choices = [];
        }

        foreach ($form_field->choices as $key => $value) {
            $form_field->choices[$value] = $value;
            unset($form_field->choices[$key]);
        }
    }

    public static function patchChoices(FormField $form_field, ?array $requestData): void
    {
        if (!in_array(
            $form_field->form_field_type_id,
            [FormFieldType::FIELD_CHOICES, FormFieldType::FIELD_CHOICES_DROPDOWN, FormFieldType::FIELD_CHOICES_MULTIPLE],
            true
        )) {
            $form_field->choices = null;
            return;
        }
        $originalChoices = $form_field->getOriginal('choices');
        if (is_string($originalChoices)) {
            $originalChoices = unserialize($originalChoices);
        }
        if (!is_array($originalChoices)) {
            $originalChoices = [];
        }

        $requestChoicesData = Hash::get($requestData, 'choices');
        if (is_array($requestChoicesData)) {
            $form_field->choices = serialize($requestChoicesData);
        }
    }
}
