<?php

namespace App\Model\Entity;

use App\Budget\ProjectBudgetFactory;
use App\Model\ClassReportInterface;
use App\Model\Table\BudgetItemsTable;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * RequestBudget Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $form_id
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 */
class RequestChange extends AppEntity implements ClassReportInterface
{
    public const CHANGE_ORIGINAL = 0,
        CHANGE_PENDING = 1,
        CHANGE_ACCEPTED = 2,
        CHANGE_DECLINED = 3;

    public const KNOWN_STATUSES = [
        self::CHANGE_ORIGINAL,
        self::CHANGE_PENDING,
        self::CHANGE_ACCEPTED,
        self::CHANGE_DECLINED
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'form_id' => true,
        'modified' => true,
        'created' => true,
    ];

    public static function getReportColumns(): array
    {
        return [
            'request_id' => __('ID žádosti')
        ];
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public static function extractColumn(Request $request, string $column): array
    {
        return [];
    }

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        return [];
    }
}
