<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\I18n\FrozenTime;

/**
 * IdentityAttachmentType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property int $order
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property IdentityAttachment[] $identity_attachments
 */
class IdentityAttachmentType extends AppEntity
{
    public const TYPE_GENERAL_ATTACHMENT = 1;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'modified' => true,
        'created' => true,
        'identity_attachments' => true,
        'order' => true,
    ];

    
    public static function getShortName(int $id): ?string
    {
        switch ($id) {
            default:
                return null;
            case self::TYPE_GENERAL_ATTACHMENT:
                return __('Obecná příloha');
        }
    }
}
