<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\I18n\FrozenTime;

/**
 * SettlementAttachmentType Entity
 *
 * @property int $id
 * @property string $type_name
 * @property int $order
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property SettlementAttachment[] $settlement_attachments
 */
class SettlementAttachmentType extends AppEntity
{
    public const TYPE_ACCOUNTING_INVOICE_DOCUMENT = 1,
        TYPE_PROOF_OF_PAYMENT = 2,
        TYPE_FINAL_REPORT = 3,
        TYPE_PUBLICITY_PROOF = 4,
        TYPE_OTHERS = 5,
        TYPE_FINAL_REPORT_WITH_PUBLICITY = 6,
        TYPE_ANONYMIZED_DOCUMENT = 7,
        TYPE_SETTLEMENT_ADDITIONAL_FINANCING = 8;

    public const TYPE_1_INVOICES = [
        self::TYPE_ACCOUNTING_INVOICE_DOCUMENT
    ];

    public const TYPE_2_PROOF = [
        self::TYPE_PROOF_OF_PAYMENT
    ];

    public const TYPE_3_OTHERS = [
        self::TYPE_OTHERS,
    ];

    public const TYPE_4_PUBLICITY = [
        self::TYPE_PUBLICITY_PROOF,
        self::TYPE_FINAL_REPORT_WITH_PUBLICITY,
    ];

    public const TYPES_AUTOMATICALLY_USED = [
        self::TYPE_PUBLICITY_PROOF,
        self::TYPE_FINAL_REPORT_WITH_PUBLICITY,
        self::TYPE_FINAL_REPORT,
    ];

    public const TYPE_ANONYMIZED = [
        self::TYPE_ANONYMIZED_DOCUMENT
    ];

    public const TYPE_ADDITIONAL_FINANCING = [
        self::TYPE_SETTLEMENT_ADDITIONAL_FINANCING
    ];

    public static function filter($listing, array $allowedTypes): array
    {
        return (new Collection($listing))->filter(function ($value, ?int $key = null) use ($allowedTypes) {
            if ($value instanceof SettlementAttachmentType) {
                return in_array($value->id, $allowedTypes, true);
            }
            return in_array($key, $allowedTypes, true);
        })->toArray();
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_name' => true,
        'modified' => true,
        'created' => true,
        'settlement_attachments' => true,
        'order' => true,
    ];

    public function getShortTypeName(): string
    {
        return self::getShortName($this->id);
    }

    public static function getShortName(int $id): ?string
    {
        switch ($id) {
            default:
                return null;
            case self::TYPE_ACCOUNTING_INVOICE_DOCUMENT:
                return __('Účetní nebo daňový doklad');
            case self::TYPE_PROOF_OF_PAYMENT:
                return __('Doklad o uhrazení');
            case self::TYPE_PUBLICITY_PROOF:
                return __('Doklad o publicitě');
            case self::TYPE_FINAL_REPORT:
                return __('Závěrečná zpráva bez dokladu o publicitě');
            case self::TYPE_OTHERS:
                return __('Ostatní přílohy');
            case self::TYPE_FINAL_REPORT_WITH_PUBLICITY:
                return __('Závěrečná zpráva s dokladem o publicitě');
        }
    }
}
