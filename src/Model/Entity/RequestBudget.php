<?php

namespace App\Model\Entity;

use App\Budget\ProjectBudgetFactory;
use App\Model\ClassReportInterface;
use App\Model\Table\BudgetItemsTable;
use Cake\I18n\FrozenTime;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * RequestBudget Entity
 *
 * @property int $id
 * @property int $request_id
 * @property float $total_income
 * @property float $total_costs
 * @property float $total_own_sources
 * @property float $total_other_subsidy
 * @property float $requested_amount
 * @property null|string $extra_data
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Request $request
 * @property BudgetItem[] $incomes
 * @property BudgetItem[] $costs
 * @property BudgetItem[] $own_sources
 * @property BudgetItem[] $other_subsidies
 */
class RequestBudget extends AppEntity implements ClassReportInterface
{
    public const BUDGET_OK = 1,
        BUDGET_MISSING = 2,
        BUDGET_MISSING_BUDGET = 3,
        BUDGET_MISSING_BALANCE_SHEET = 4,
        BUDGET_MISSING_REQUESTED_AMOUNT = 5,
        BUDGET_REQUESTED_HIGHER_THAN_ALLOWED = 6,
        BUDGET_REQUESTED_LOWER_THAN_ALLOWED = 7,
        BUDGET_INCORRECT_BALANCE = 8,
        BUDGET_MAX_SUPPORT_EXCEEDED = 9,
        BUDGET_CUSTOM_VALIDATION_FAILED = 10;


    public static function getValidationUserMessage(int $budget_validation_result, Request $request, bool $includeIcons = true): string
    {
        $rtn = '';
        switch ($budget_validation_result) {
            default:
            case RequestBudget::BUDGET_MISSING:
            case RequestBudget::BUDGET_MISSING_REQUESTED_AMOUNT:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-times text-danger"></i> ';
                }
                $rtn .= __('Rozpočet nebyl vyplněn');
                break;
            case RequestBudget::BUDGET_MISSING_BALANCE_SHEET:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-exclamation-triangle text-warning"></i> ';
                }
                $rtn .= __('V rozpočtu projektu nejsou žádné položky (výnosy / náklady)');
                break;
            case RequestBudget::BUDGET_MISSING_BUDGET:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-exclamation-triangle text-warning"></i> ';
                }
                $rtn .= __('V rozpočtu projektu nejsou žádné položky (vlastní zdroje / jiné dotace)');
                break;
            case RequestBudget::BUDGET_OK:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-check text-success"></i> ';
                }
                $no_budget = ($request->program->project_budget_design_id == ProjectBudgetDesign::DESIGN_NONE);
                if (!$no_budget) {
                $rtn .= sprintf(__('Rozpočet je vyplněn, žádáte o %s'), Number::currency($request->request_budget->requested_amount, 'CZK'));
                } else {
                    $rtn .= sprintf(__('Žádáte o %s'), Number::currency($request->request_budget->requested_amount, 'CZK'));
                }
                break;
            case RequestBudget::BUDGET_REQUESTED_HIGHER_THAN_ALLOWED:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-times text-danger"></i> ';
                }
                $rtn .= sprintf("%s %s", __('Žádáte o částku vyšší než je možné, maximum v tomto programu je'), Number::currency($request->appeal->getMaxRequestBudget($request->program_id), 'CZK'));
                break;
            case RequestBudget::BUDGET_REQUESTED_LOWER_THAN_ALLOWED:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-times text-danger"></i> ';
                }
                $rtn .= sprintf("%s %s", __('Žádáte o částku nižší než je možné, minimum v tomto programu je'), Number::currency($request->appeal->getMinRequestBudget($request->program_id), 'CZK'));
                break;
            case RequestBudget::BUDGET_MAX_SUPPORT_EXCEEDED:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-times text-danger"></i> ';
                }
                $rtn .= sprintf('percentage', sprintf("%s %s", __('Překročen poměr nákladů a dotace, maximum je '), Number::toPercentage($request->appeal->getMaxSupportPercentage($request->program_id))));
                break;
            case RequestBudget::BUDGET_INCORRECT_BALANCE:
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-exclamation-triangle text-warning"></i> ';
                }
                $rtn .= __('Rozpočet projektu musí být vyrovnaný (dotace + příjmy = náklady)');
                break;
            case RequestBudget::BUDGET_CUSTOM_VALIDATION_FAILED:
                $budgetHandler = ProjectBudgetFactory::getBudgetHandler($request->program->project_budget_design_id);
                if ($includeIcons) {
                    $rtn .= '<i class="fas fa-exclamation-triangle text-warning"></i> ';
                }
                $rtn .= $budgetHandler->definitions['VALIDATION']['_message'];
                break;

        }
        return $rtn;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'total_income' => true,
        'total_costs' => true,
        'total_own_sources' => true,
        'total_other_subsidy' => true,
        'requested_amount' => true,
        'modified' => true,
        'created' => true,
        'request' => true,
        'incomes' => true,
        'costs' => true,
        'own_sources' => true,
        'other_subsidies' => true,
        'extra_data' => true,
    ];

    private array $_itemsToDelete = [];

    public function deleteRemovedItems(BudgetItemsTable $table)
    {
        foreach ($this->_itemsToDelete as $index => $item) {
            if (!empty($item['id'])) {
                //Log::debug(json_encode($item));
                $raw = $table->get(intval($item['id']), [
                    'conditions' => [
                        'request_budget_id' => $this->id,
                    ],
                ]);
                if (!empty($raw)) {
                    $table->delete($raw);
                }
            }
            unset($this->_itemsToDelete[$index]);
        }
    }

    /**
     * @return void
     */
    public function recountItems(): void
    {
        $this->total_costs = array_reduce(
            $this->costs ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_income = array_reduce(
            $this->incomes ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_other_subsidy = array_reduce(
            $this->other_subsidies ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
        $this->total_own_sources = array_reduce(
            $this->own_sources ?? [],
            function ($carry, $item) {
                return $carry + floatval($item->amount);
            }
        ) ?? 0;
    }

    /**
     * @param array $data POST/Request data to be cleaned
     * @return array cleaned data
     */
    public function cleanPostData(array $data): array
    {
        foreach (['incomes', 'costs', 'own_sources', 'other_subsidies'] as $group) {
            if (!isset($data[$group]) || !is_array($data[$group])) {
                continue;
            }
            foreach ($data[$group] as $index => $budget_item) {
                if (empty($budget_item['description']) || empty($budget_item['amount'])) {
                    if (!empty($budget_item['id'])) {
                        $this->_itemsToDelete[] = $budget_item;
                    }
                    unset($data[$group][$index]);
                }
            }
        }

        return $data;
    }

    /**
     * @return bool
     */
    public function hasNoBudgetItems(): bool
    {
        return empty($this->incomes) && empty($this->costs) && empty($this->own_sources) && empty($this->total_other_subsidy);
    }

    public function validateBudgetRequirements(Appeal $appeal, Program $program): int
    {
        $budgetHandler = ProjectBudgetFactory::getBudgetHandler($program->project_budget_design_id);
        return $budgetHandler->validateBudgetRequirements($this, $appeal, $program);
    }

    public static function getReportColumns(): array
    {
        return [
            'requested_amount' => __('Žádaná částka'),
            'total_other_subsidy' => __('Jiné dotace na projekt celkem'),
            'total_own_sources' => __('Vlastní zdroje celkem'),
            'total_costs' => __('Náklady celkem'),
            'total_income' => __('Příjmy celkem'),
        ];
    }

    public static function columnRendersAsMultiple(string $column): bool
    {
        return false;
    }

    public static function extractColumn(Request $request, string $column): array
    {
        $request->loadBudget();
        return [$request->request_budget ? strval(Hash::get($request->request_budget, $column)) : ''];
    }

    public static function getColumnHeaders(string $path, string $columnName): array
    {
        return [];
    }
}
