<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App;

use App\Middleware\ErrorHandlerMiddleware;
use App\Middleware\OrgDomainsMiddleware;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Datasource\ConnectionManager;
use Cake\Http\BaseApplication;
use Cake\Http\MiddlewareQueue;
use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use DebugKit\Plugin;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication
{
    /**
     * {@inheritDoc}
     */
    public function bootstrap()
    {
        // Call parent to load bootstrap from files.
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        }

        // Load more plugins here
        if (!in_array(Configure::read('ErrorEmail.emailDeliveryProfile'), Email::configured())) {
            TransportFactory::setConfig('error', Configure::read('EmailTransport.error'));
            Email::setConfig('error', Configure::read('Email.error'));
        }
        $this->addPlugin('ErrorEmail');

        if (in_array('dsw', ConnectionManager::configured())) {
            $this->addPlugin('OldDsw');
        }
        Configure::write('CakePdf', [
            'engine' => 'App.Pdf/Engine.WeasyPrint',
        ]);
        
        $this->addPlugin('CakePdf');
        

        $debugAddresses = [
        ];
        $debugKitAddress = '/debug-kit';
        $enableDebug = PHP_SAPI === 'cli';

        foreach ($debugAddresses as $debugAddressPrefix) {
            if ($enableDebug) {
                break;
            }
            if (startsWith($_SERVER['REQUEST_URI'], $debugAddressPrefix)) {
                $enableDebug = true;
            }
            if (
                startsWith($_SERVER['REQUEST_URI'], $debugKitAddress)
                && (str_contains($_SERVER['HTTP_REFERER'], $debugAddressPrefix)
                    || str_contains($_SERVER['HTTP_REFERER'], '/debug-kit/toolbar/')
                )
            ) {
                $enableDebug = true;
            }
        }
        $enableDebug = false;
        if ($enableDebug) {
            Configure::write('Error.errorLevel', E_ALL);
            Configure::write('debug', true);
            Configure::write('DebugKit.safeTld', ['eu', 'cz']);
            Configure::write('DebugKit.ignoreAuthorization', true);
            Configure::write('DebugKit.forceEnable', true);
            $this->addPlugin(Plugin::class);
        }
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param MiddlewareQueue $middleware The middleware queue to setup.
     * @return MiddlewareQueue The updated middleware queue.
     */
    public function middleware($middleware): MiddlewareQueue
    {
        $middleware
            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(null, Configure::read('Error')))
            ->add(new OrgDomainsMiddleware($this))
            // Handle plugin/theme assets like CakePHP normally does.
            ->add(
                new AssetMiddleware(
                    [
                        'cacheTime' => Configure::read('Asset.cacheTime'),
                    ]
                )
            )
            ->add(new RoutingMiddleware($this));

        return $middleware;
    }

    /**
     * @return void
     */
    protected function bootstrapCli()
    {
        try {
            $this->addPlugin('Bake');
        } catch (MissingPluginException $e) {
            // Do not halt if the plugin is missing
        }

        $this->addPlugin('Migrations');
        $this->addPlugin('ADmad/I18n');

        // Load more plugins here
    }
}
