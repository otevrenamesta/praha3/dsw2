<?php

namespace App\Command;

use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Program;
use App\Model\Entity\Request;
use App\Model\Entity\RequestState;
use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\Model\Table\IdentitiesTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProgramsTable;
use App\Model\Table\RequestsTable;
use App\Model\Table\SettlementsTable;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;
use Cake\Mailer\Email;
use Cake\Mailer\Transport\MailTransport;
use Cake\Mailer\TransportFactory;
use Throwable;

/**
 * @property-read RequestsTable $Requests
 * @property-read IdentitiesTable $Identities
 * @property-read SettlementsTable $Settlements
 */
class CronCommand extends Command
{

    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $finishedOK = true;

        $io->success('Running SettlementsEmail');
        $result = $this->settlementsEmail($io);
        if ($result === null) {
            $io->success('SettlementsEmail OK');
        } else {
            $finishedOK = false;
            $io->error('SettlementsEmail error code ' . $result);
        }

        $io->success('Running RejectSettlements');
        $result = $this->rejectSettlements($io);
        if ($result === null) {
            $io->success('RejectSettlements OK');
        } else {
            $finishedOK = false;
            $io->error('RejectSettlements error code ' . $result);
        }

        $io->success('Running RejectRequests');
        $result = $this->rejectRequests($io);
        if ($result === null) {
            $io->success('RejectRequests OK');
        } else {
            $finishedOK = false;
            $io->error('RejectRequests error code ' . $result);
        }

        $io->success('Running LockRequests');
        $result = $this->lockRequests($io);
        if ($result === null) {
            $io->success('LockRequests OK');
        } else {
            $finishedOK = false;
            $io->error('LockRequests error code ' . $result);
        }

        $io->success('Running LockIdentities');
        $result = $this->lockIdentities($io);
        if ($result === null) {
            $io->success('LockIdentities OK');
        } else {
            $finishedOK = false;
            $io->error('LockIdentities error code ' . $result);
        }

        return $finishedOK ? null : 1;
    }

    private function lockIdentities(ConsoleIo $io)
    {
        $this->loadModel('Requests');
        $this->loadModel('Identities');

        $requestsWithLockedIdentities = $this->Requests->find('all', [
            'conditions' => [
                'Requests.request_state_id NOT IN' => [
                    RequestState::STATE_NEW,
                    RequestState::STATE_READY_TO_SUBMIT,
                ],
            ],
        ]);

        /** @var Request $request */
        foreach ($requestsWithLockedIdentities as $request) {
            $incorrectUnlockedIdentities = $this->Identities->find('all', [
                'conditions' => [
                    'Identities.version' => $request->user_identity_version,
                    'Identities.user_id' => $request->user_id,
                    'Identities.is_locked' => false,
                ],
            ]);
            $count = $incorrectUnlockedIdentities->count();
            if ($count > 0) {
                $io->warning(sprintf('USER %d VERSION %d UNLOCKED %d', $request->user_id, $request->user_identity_version, $count));
            }

            if ($count > 0) {
                $io->success(sprintf("LOCKED ROWS %d", $this->Identities->updateAll([
                    'is_locked' => true,
                ], [
                    'Identities.version' => $request->user_identity_version,
                    'Identities.user_id' => $request->user_id,
                ])));
            }
        }

        // null return is success
        return null;
    }

    private function lockRequests(ConsoleIo $io): ?int
    {
        $this->loadModel('Requests');

        $requestsNotLocked = $this->Requests->find('all', [
            'conditions' => [
                'request_state_id >=' => RequestState::STATE_READY_TO_SUBMIT,
                'is_locked' => false,
            ],
        ]);

        /** @var Request $request */
        foreach ($requestsNotLocked as $request) {
            $shouldBeLocked = !RequestState::canUserEditRequest($request->request_state_id);
            if ($request->is_locked !== $shouldBeLocked) {
                $request->set('is_locked', $shouldBeLocked);
                $io->info(sprintf('Request %d is unlocked in state %d, correcting', $request->id, $request->request_state_id));
            }
            if (!$this->Requests->save($request)) {
                $io->error(sprintf('Could not correct request %d reason %s', $request->id, json_encode($request->getErrors())));
            }
        }

        $requestsToBeLockedAutomatically = $this->Requests->find(
            'all',
            [
                'conditions' => [
                    'request_state_id IN' => [RequestState::STATE_NEW, RequestState::STATE_READY_TO_SUBMIT],
                    'lock_when <' => date('Y-m-d'),
                ],
            ]
        );

        $io->info(sprintf('Requests to be locked %d', $requestsToBeLockedAutomatically->count()));

        foreach ($requestsToBeLockedAutomatically as $request) {
            /**
             * @var Request $request
             */
            $request->setCurrentStatus(RequestState::STATE_FORMAL_CHECK_REJECTED);

            try {
                if ($this->Requests->save($request) === false) {
                    $io->error(sprintf('Request %d cannot be modified, reasons: ', $request->id) . json_encode($request->getErrors()));

                    return 2;
                }
            } catch (Throwable $t) {
                $io->error($t->getMessage());
                $io->error($t->getTraceAsString());

                return 1;
            }
            $io->success(sprintf('Request ID:%d automatically transitioned to state REJECTED', $request->id));
        }

        $io->success('All done successfully');

        return null;
    }

    private function rejectSettlements(ConsoleIo $io): ?int
    {
        $this->loadModel('Settlements');

        $settlementsNotRejected = $this->Settlements->find('all', [
            'conditions' => [
                'settlement_state_id =' => SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES,
                'repair_date IS NOT' => NULL,
                'repair_date <' => date('Y-m-d'),
            ],
        ]);

        $io->info(sprintf('Settlements to be rejected %d', $settlementsNotRejected->count()));

        foreach ($settlementsNotRejected as $settlement) {
            /**
             * @var Settlement $settlement
             */
            $settlement->setCurrentStatus(SettlementState::STATE_SETTLEMENT_REJECTED);

            try {
                if ($this->Settlements->save($settlement) === false) {
                    $io->error(sprintf('Settlement %d cannot be modified, reasons: ', $settlement->id) . json_encode($settlement->getErrors()));

                    return 2;
                }
            } catch (Throwable $t) {
                $io->error($t->getMessage());
                $io->error($t->getTraceAsString());

                return 1;
            }
            $io->success(sprintf('Settlement ID:%d automatically transitioned to state STATE_SETTLEMENT_REJECTED', $settlement->id));
        }

        $io->success('All done successfully');

        return null;
    }

    private function rejectRequests(ConsoleIo $io): ?int
    {
        $this->loadModel('Settlements');
        $this->loadModel('Requests');

        /** @var Settlement[] $settlements */
        $settlements = $this->Settlements->find('all', [
            'conditions' => [
                'settlement_state_id =' => SettlementState::STATE_SETTLEMENT_REJECTED,
            ],
            'contain' => [
                'RequestsToSettlements.Requests',
            ]
        ])->toArray();

        // Get all related Requests
        $listOfRequests = [];
        foreach ($settlements as $settlement) {
            foreach ($settlement->requests_to_settlements as $requests) {
                $listOfRequests[] =  $requests->request_id;
            }
        }
        $io->info(sprintf('Requests found %d', count($listOfRequests)));

        if (count($listOfRequests) > 0) {
            $requestsNotRejected = $this->Requests->find('all', [
                'conditions' => [
                    'request_state_id !=' => RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS,
                    'id IN' => $listOfRequests,
                ],
            ]);
            $io->info(sprintf('Requests to be modified %d', $requestsNotRejected->count()));

            foreach ($requestsNotRejected as $request) {
                /**
                 * @var Request $request
                 */
                $request->setCurrentStatus(RequestState::STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS);

                try {
                    if ($this->Requests->save($request) === false) {
                        $io->error(sprintf('Request %d cannot be modified, reasons: ', $request->id) . json_encode($request->getErrors()));

                        return 2;
                    }
                } catch (Throwable $t) {
                    $io->error($t->getMessage());
                    $io->error($t->getTraceAsString());

                    return 1;
                }
                $io->success(sprintf('Request ID:%d automatically transitioned to state STATE_CONTRACT_BREACH_CRIMINAL_PROCEEDINGS', $request->id));
            }
        }

        $io->success('All done successfully');

        return null;
    }

    private function settlementsEmail(ConsoleIo $io): ?int
    {
        foreach (Configure::read('EmailTransport') as $configName => $config) {
            if (!in_array($configName, TransportFactory::configured())) {
                if (empty($config['host'])) {
                    $config['className'] = MailTransport::class;
                }
                TransportFactory::setConfig($configName, $config);
            }
        }
        foreach (Configure::read('Email') as $configName => $config) {
            if (!in_array($configName, Email::configured())) {
                Email::setConfig($configName, $config);
            }
        }

        $this->loadModel('Settlements');
        $this->loadModel('Requests');
        $this->loadModel('Programs');
        $this->loadModel('Organizations');

        $date = date('Y-m-d', strtotime('+15 days'));
        // pr($date );

        $programs = $this->Programs->find('all', [
            'conditions' => [
                'settlement_date IS NOT' => NULL,
                'settlement_date =' => $date,
            ],
            'fields' => array('id'),
        ])->toArray();

        $programs_ids = count($programs) > 0
            ? array_reduce($programs, function ($result, $value) {
                $result[] = $value['id'];
                return $result;
            })
            : [];

        $requests = $this->Requests->find('all', [
            $conditions['OR'] = [
                'settlement_date =' => $date,
                'program_id IN' =>  $programs_ids,
            ],
            'contain' => [
                'Users'
            ]
        ])->toArray();

        foreach ($requests as $request) {
            $d = $request->settlement_date ? $request->settlement_date->format('Y-m-d') : null;

            if ($d === $date || (!$request->settlement_date && in_array($request->program_id, $programs_ids))) {

                $request->loadSettlements();
                foreach ($request->settlements as $settlement) {
                    if (!$settlement->settlement_date_email) {
                        $organization = $this->Organizations->get(
                            $settlement->organization_id,
                            [
                                'contain' => [
                                    'OrganizationSettings',
                                ],
                            ]
                        )->toArray();

                        $mail_key = array_search('Email.default.from', array_column($organization['organization_settings'], 'name'));
                        $mail = $organization['organization_settings'][$mail_key]['value'];

                        $site_key = array_search('site.name', array_column($organization['organization_settings'], 'name'));
                        $site = $organization['organization_settings'][$site_key]['value'];

                        $title = sprintf('Oznámení o blížícím se termínu podání vyúčtování - %s', $site);
                        $content = 'Dobrý den,

                        do termínu podání Vašeho vyúčtování č. %s k žádosti "%s" zbývá 15 dní.

                        V případě, že již bylo vyúčtování podáno/odesláno datovou schránkou, neberte na tuto zprávu zřetel. Děkujeme.

                        S pozdravem, %s

                        E-mail Vám byl zaslán, protože na tuto adresu je registrován uživatel v dotačním systému.
                        ';

                        $email = new Email('default');
                        $isSend = $email
                            ->template('default')
                            ->emailFormat('both')
                            ->to($request->user->email)
                            ->from($request->user->email)
                            ->setViewVars(['content' =>  sprintf($content,  $settlement->id, $request->name, $site), 'title' =>  $title])
                            ->subject($title)
                            ->send();

                        $io->info(sprintf('Sending an e-mail to the user #%d, deadline notice for settlement #%d', $request->user->id, $settlement->id));

                        if (!$isSend) {
                            $io->error(sprintf('Unable to send email for %d, settlement #%d - reason: %s', $request->user->email, $settlement->id, json_encode($request->getErrors())));
                        } else {
                            $settlement->set('settlement_date_email', 1);
                            $this->Settlements->save($settlement);
                        }
                    }
                }
            }
        }

        $io->success('All done successfully');

        return null;
    }
}
