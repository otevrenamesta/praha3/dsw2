FROM wyveo/nginx-php-fpm:php74

# Set www-data UID to first non-root UID
# This should make file permissions inside container working out of the box
# for most of developers.
RUN usermod -u 1000 www-data

# Install PHP extensions and other dependencies
RUN apt-get update \
	&& apt-get install -y \
	    libzip-dev \
	    locales \
	    libicu-dev \
            php7.4-soap \
        && apt-get dist-upgrade -y -o Dpkg::Options::="--force-confnew" \
	&& rm -rf /var/lib/apt/lists/*

RUN composer self-update --2

COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www/
COPY . .
RUN composer install

CMD ["./docker/dsw2-start.sh"]
