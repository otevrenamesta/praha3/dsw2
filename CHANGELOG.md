### 2021-12
- Název žadatele do sekce Administrátor/Správa uživatelů (#157)
- Oprava přepínání `překladů textů` pro organizace - problém s cache systémen na serveru (#136)
- Opraven `disabled` stav pro Odeslání žádosti (#158)

### 2021-11
- Rozšíření `ukončení výzvy` o čas (volitelná funkce) (#154)
- `Historie podpory` - propojení archivu s historií v žádosti (volitelná funkce) (#152)
- Nový systém pro `překlady textů` s individuálním ukládáním pro každou organizaci (#136)
- Personifikované texty organizací doplněny do DB (i18n_messages), použití v kódu je zajištěno použitím _d() (#136)
- `Schváleno formální kontrolou` je nově nastaveno pro 4 i 5 krok stavu žádosti (#137)
- Odeslání emailu o změně stavu schvalování je přesunuto o krok dále (#137)
- Update `de minimis` - zákaz editace v dalších krocích po uložení (#21)
- Přesun a validace pole `Žadatel nevlastní podíl v další právnické osobě` v Identitě (#71)
- Rozšíření `Mé týmy/Náhled` o sloupec Slovní hodnocení (#142)
- Změna v datových sestavách pro přidávání sloupců u IČO a DIČ (#132)
- Velikost fontů `šablony žádostí` pro export do pdf zvětšena z .75 na .90 (#141)
- Filtry pro lepší ovládání stavů v sekci `Mé týmy/Náhled` (#124)
- E-mailové notifikace pro stav `formálního schválení` a `před podpisem` (#137)
- Podpora `de minimis` v hodnocení a datových sestavách (#21)
- Po kontrole dotační žádostí vracet pozici prohlížeče na řádek žádosti (#95)
- Filtry v ekonomickém oddělení pro výběry stavů (#124)
- Sloupec `Vlastní identifikátor` v ekonomickém oddělení (#123)
- Vyjádření v identitě že žadatel nevlastní podíl v další PO (#71)
- Pdf export identity žadatele - opravena chyba u statutárního orgánu (#139)
- Filtr rolí `uživatel portálu` a `pracovník úřadu` v tabulce uživatelů (#140)
- Možnost v patičce oddělat sekci `O projektu` (#135)
- Možnost vypnutí karty `Dotační fond` na hompage (#131)

### 2021-10
- Nový typ formuláře `Vícepoložkový výběr` (#109)
- Úprava patičky šablony e-mailu (#90)
- Doplnění údajů formulářů pro generování do PDF (#118 a #109)
- Souhlas s GDPR při registraci (#106)
- Nová stránka `Prohlášení o přístupnosti` (#106)
- Změna patičky aplikace (#119)
- Individuální název pro formuláře jako `Zobrazovaný název` (#118)
- Oprava pro - špatné exporty pdf, dělené tabulky (#127)
