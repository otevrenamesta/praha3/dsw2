<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\DefiniceProjektu;

/**
 * DefiniceProjektu Model
 *
 * @method DefiniceProjektu get($primaryKey, $options = [])
 * @method DefiniceProjektu newEntity($data = null, array $options = [])
 * @method DefiniceProjektu[] newEntities(array $data, array $options = [])
 * @method DefiniceProjektu|false save(EntityInterface $entity, $options = [])
 * @method DefiniceProjektu saveOrFail(EntityInterface $entity, $options = [])
 * @method DefiniceProjektu patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method DefiniceProjektu[] patchEntities($entities, array $data, array $options = [])
 * @method DefiniceProjektu findOrCreate($search, callable $callback = null, $options = [])
 */
class DefiniceProjektuTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('definice_projektu');
        $this->setDisplayField('zadost_id');
        $this->setPrimaryKey('zadost_id');
        $this->setEntityClass('OldDsw.DefiniceProjektu');

        $this->hasOne('Terminy', [
            'className' => 'OldDsw.DefiniceProjektuTerminy',
            'bindingKey' => 'zadost_id',
            'foreignKey' => 'zadost_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('zadost_id')
            ->allowEmptyString('zadost_id', null, 'create');

        $validator
            ->scalar('ucel')
            ->allowEmptyString('ucel');

        $validator
            ->scalar('cilova_skupina')
            ->allowEmptyString('cilova_skupina');

        $validator
            ->scalar('vztah_k_Praha_3')
            ->allowEmptyString('vztah_k_Praha_3');

        $validator
            ->date('termin_zahajeni')
            ->allowEmptyDate('termin_zahajeni');

        $validator
            ->scalar('misto_realizace')
            ->maxLength('misto_realizace', 255)
            ->allowEmptyString('misto_realizace');

        $validator
            ->scalar('dosavadni_cinnost')
            ->allowEmptyString('dosavadni_cinnost');

        $validator
            ->scalar('pouziti_financi')
            ->allowEmptyString('pouziti_financi');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
