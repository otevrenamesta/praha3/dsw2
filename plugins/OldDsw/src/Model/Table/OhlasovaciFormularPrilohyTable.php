<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\OhlasovaciFormularPriloha;

/**
 * OhlasovaciFormularPrilohy Model
 *
 * @method OhlasovaciFormularPriloha get($primaryKey, $options = [])
 * @method OhlasovaciFormularPriloha newEntity($data = null, array $options = [])
 * @method OhlasovaciFormularPriloha[] newEntities(array $data, array $options = [])
 * @method OhlasovaciFormularPriloha|false save(EntityInterface $entity, $options = [])
 * @method OhlasovaciFormularPriloha saveOrFail(EntityInterface $entity, $options = [])
 * @method OhlasovaciFormularPriloha patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method OhlasovaciFormularPriloha[] patchEntities($entities, array $data, array $options = [])
 * @method OhlasovaciFormularPriloha findOrCreate($search, callable $callback = null, $options = [])
 */
class OhlasovaciFormularPrilohyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ohlasovaci_formular_prilohy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.OhlasovaciFormularPriloha');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->notEmptyFile('filename');

        $validator
            ->scalar('orig_filename')
            ->maxLength('orig_filename', 255)
            ->notEmptyFile('orig_filename');

        $validator
            ->numeric('filesize')
            ->notEmptyFile('filesize');

        $validator
            ->dateTime('added_when')
            ->notEmptyDateTime('added_when');

        $validator
            ->scalar('mime')
            ->maxLength('mime', 255)
            ->requirePresence('mime', 'create')
            ->notEmptyString('mime');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
