<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Uzivatel;

/**
 * Uzivatele Model
 *
 * @property UctyTable&BelongsTo $Ucets
 *
 * @method Uzivatel get($primaryKey, $options = [])
 * @method Uzivatel newEntity($data = null, array $options = [])
 * @method Uzivatel[] newEntities(array $data, array $options = [])
 * @method Uzivatel|false save(EntityInterface $entity, $options = [])
 * @method Uzivatel saveOrFail(EntityInterface $entity, $options = [])
 * @method Uzivatel patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Uzivatel[] patchEntities($entities, array $data, array $options = [])
 * @method Uzivatel findOrCreate($search, callable $callback = null, $options = [])
 */
class UzivateleTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uzivatele');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Uzivatel');

        $this->belongsTo('Ucets', [
            'foreignKey' => 'ucet_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Ucets',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('jmeno')
            ->maxLength('jmeno', 45)
            ->notEmptyString('jmeno');

        $validator
            ->scalar('prijmeni')
            ->maxLength('prijmeni', 45)
            ->notEmptyString('prijmeni');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('telefon')
            ->maxLength('telefon', 45)
            ->allowEmptyString('telefon');

        $validator
            ->boolean('aktivni')
            ->notEmptyString('aktivni');

        $validator
            ->dateTime('added_when')
            ->notEmptyDateTime('added_when');

        $validator
            ->boolean('is_superuser')
            ->notEmptyString('is_superuser');

        $validator
            ->scalar('login')
            ->maxLength('login', 45)
            ->notEmptyString('login');

        $validator
            ->scalar('password')
            ->maxLength('password', 45)
            ->notEmptyString('password');

        $validator
            ->boolean('smazano')
            ->notEmptyString('smazano');

        $validator
            ->dateTime('smazano_kdy')
            ->allowEmptyDateTime('smazano_kdy');

        $validator
            ->integer('smazano_kym')
            ->allowEmptyString('smazano_kym');

        $validator
            ->scalar('restart_hash')
            ->maxLength('restart_hash', 255)
            ->allowEmptyString('restart_hash');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['login']));
        $rules->add($rules->existsIn(['ucet_id'], 'Ucty'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
