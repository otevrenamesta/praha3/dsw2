<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\PrilohyFormulareProhlaseniOVerejnePodpore;

/**
 * PrilohyFormulareProhlaseniOVerejnePodpore Model
 *
 * @method PrilohyFormulareProhlaseniOVerejnePodpore get($primaryKey, $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore newEntity($data = null, array $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore[] newEntities(array $data, array $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore|false save(EntityInterface $entity, $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore saveOrFail(EntityInterface $entity, $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore[] patchEntities($entities, array $data, array $options = [])
 * @method PrilohyFormulareProhlaseniOVerejnePodpore findOrCreate($search, callable $callback = null, $options = [])
 */
class PrilohyFormulareProhlaseniOVerejnePodporeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prilohy_formulare_prohlaseni_o_verejne_podpore');
        $this->setDisplayField('zadost_id');
        $this->setPrimaryKey('zadost_id');
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('zadost_id')
            ->allowEmptyString('zadost_id', null, 'create');

        $validator
            ->nonNegativeInteger('poskytnuti_verejne_podpory')
            ->notEmptyString('poskytnuti_verejne_podpory');

        $validator
            ->nonNegativeInteger('rezim_podpory')
            ->notEmptyString('rezim_podpory');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
