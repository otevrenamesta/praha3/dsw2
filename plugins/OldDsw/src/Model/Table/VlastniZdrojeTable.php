<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\VlastniZdroje;

/**
 * VlastniZdroje Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property RozpoctyProjektuTable&BelongsToMany $RozpoctyProjektu
 *
 * @method VlastniZdroje get($primaryKey, $options = [])
 * @method VlastniZdroje newEntity($data = null, array $options = [])
 * @method VlastniZdroje[] newEntities(array $data, array $options = [])
 * @method VlastniZdroje|false save(EntityInterface $entity, $options = [])
 * @method VlastniZdroje saveOrFail(EntityInterface $entity, $options = [])
 * @method VlastniZdroje patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method VlastniZdroje[] patchEntities($entities, array $data, array $options = [])
 * @method VlastniZdroje findOrCreate($search, callable $callback = null, $options = [])
 */
class VlastniZdrojeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vlastni_zdroje');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Requests',
        ]);
        $this->belongsToMany('RozpoctyProjektu', [
            'foreignKey' => 'vlastni_zdroje_id',
            'targetForeignKey' => 'rozpocty_projektu_id',
            'joinTable' => 'rozpocty_projektu_vlastni_zdroje',
            'className' => 'OldDsw.RozpoctyProjektu',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('project_name')
            ->maxLength('project_name', 255)
            ->notEmptyString('project_name');

        $validator
            ->numeric('amount')
            ->notEmptyString('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
