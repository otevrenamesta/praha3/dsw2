<?php
namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\RozpoctyProjektuNaklady;

/**
 * RozpoctyProjektuNaklady Model
 *
 * @property ZadostsTable&BelongsTo $Zadosts
 *
 * @method RozpoctyProjektuNaklady get($primaryKey, $options = [])
 * @method RozpoctyProjektuNaklady newEntity($data = null, array $options = [])
 * @method RozpoctyProjektuNaklady[] newEntities(array $data, array $options = [])
 * @method RozpoctyProjektuNaklady|false save(EntityInterface $entity, $options = [])
 * @method RozpoctyProjektuNaklady saveOrFail(EntityInterface $entity, $options = [])
 * @method RozpoctyProjektuNaklady patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method RozpoctyProjektuNaklady[] patchEntities($entities, array $data, array $options = [])
 * @method RozpoctyProjektuNaklady findOrCreate($search, callable $callback = null, $options = [])
 */
class RozpoctyProjektuNakladyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rozpocty_projektu_naklady');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zadosts', [
            'foreignKey' => 'zadost_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Zadosts',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('nazev')
            ->maxLength('nazev', 255)
            ->notEmptyString('nazev');

        $validator
            ->numeric('castka')
            ->notEmptyString('castka');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['zadost_id'], 'Zadosts'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
