<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\FyzickaOsoba;

/**
 * FyzickeOsoby Model
 *
 * @method FyzickaOsoba get($primaryKey, $options = [])
 * @method FyzickaOsoba newEntity($data = null, array $options = [])
 * @method FyzickaOsoba[] newEntities(array $data, array $options = [])
 * @method FyzickaOsoba|false save(EntityInterface $entity, $options = [])
 * @method FyzickaOsoba saveOrFail(EntityInterface $entity, $options = [])
 * @method FyzickaOsoba patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method FyzickaOsoba[] patchEntities($entities, array $data, array $options = [])
 * @method FyzickaOsoba findOrCreate($search, callable $callback = null, $options = [])
 */
class FyzickeOsobyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fyzicke_osoby');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.FyzickaOsoba');

        $this->belongsToMany('Ucty', [
            'foreignKey' => 'fyzicke_osoby_id',
            'targetForeignKey' => 'ucty_id',
            'joinTable' => 'ucty_fyzicke_osoby',
            'className' => 'OldDsw.Ucty',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('funkce')
            ->maxLength('funkce', 45)
            ->allowEmptyString('funkce');

        $validator
            ->scalar('titul')
            ->maxLength('titul', 45)
            ->allowEmptyString('titul');

        $validator
            ->scalar('jmeno')
            ->maxLength('jmeno', 45)
            ->notEmptyString('jmeno');

        $validator
            ->scalar('prijmeni')
            ->maxLength('prijmeni', 45)
            ->notEmptyString('prijmeni');

        $validator
            ->scalar('datum_narozeni')
            ->maxLength('datum_narozeni', 45)
            ->allowEmptyString('datum_narozeni');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('cislo_OP')
            ->maxLength('cislo_OP', 45)
            ->allowEmptyString('cislo_OP');

        $validator
            ->scalar('podil')
            ->maxLength('podil', 5)
            ->allowEmptyString('podil');

        $validator
            ->scalar('podil_splaceno_procent')
            ->maxLength('podil_splaceno_procent', 45)
            ->allowEmptyString('podil_splaceno_procent');

        $validator
            ->scalar('podil_vkladu')
            ->maxLength('podil_vkladu', 45)
            ->allowEmptyString('podil_vkladu');

        $validator
            ->scalar('obec')
            ->maxLength('obec', 45)
            ->allowEmptyString('obec');

        $validator
            ->scalar('cast_obce')
            ->maxLength('cast_obce', 45)
            ->allowEmptyString('cast_obce');

        $validator
            ->scalar('ulice')
            ->maxLength('ulice', 45)
            ->allowEmptyString('ulice');

        $validator
            ->scalar('cislo_popisne')
            ->maxLength('cislo_popisne', 45)
            ->allowEmptyString('cislo_popisne');

        $validator
            ->scalar('cislo_orientacni')
            ->maxLength('cislo_orientacni', 45)
            ->allowEmptyString('cislo_orientacni');

        $validator
            ->scalar('psc')
            ->maxLength('psc', 45)
            ->allowEmptyString('psc');

        $validator
            ->scalar('stat')
            ->maxLength('stat', 45)
            ->allowEmptyString('stat');

        $validator
            ->scalar('telefon')
            ->maxLength('telefon', 255)
            ->allowEmptyString('telefon');

        $validator
            ->scalar('rodne_cislo')
            ->maxLength('rodne_cislo', 45)
            ->allowEmptyString('rodne_cislo');

        $validator
            ->scalar('titul_za_jmenem')
            ->maxLength('titul_za_jmenem', 45)
            ->allowEmptyString('titul_za_jmenem');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
