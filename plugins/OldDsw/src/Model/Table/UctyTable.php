<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Ucet;

/**
 * Accounts Model
 *
 * @property FyzickeOsobyTable&BelongsToMany $FyzickeOsoby
 *
 * @method Ucet get($primaryKey, $options = [])
 * @method Ucet newEntity($data = null, array $options = [])
 * @method Ucet[] newEntities(array $data, array $options = [])
 * @method Ucet|false save(EntityInterface $entity, $options = [])
 * @method Ucet saveOrFail(EntityInterface $entity, $options = [])
 * @method Ucet patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Ucet[] patchEntities($entities, array $data, array $options = [])
 * @method Ucet findOrCreate($search, callable $callback = null, $options = [])
 */
class UctyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dsw.ucty');
        $this->setDisplayField('nazev');
        $this->setPrimaryKey('id');
        $this->setEntityClass('OldDsw.Ucet');

        $this->belongsToMany('FyzickeOsoby', [
            'foreignKey' => 'ucty_id',
            'targetForeignKey' => 'fyzicke_osoby_id',
            'joinTable' => 'ucty_fyzicke_osoby',
            'className' => 'OldDsw.FyzickeOsoby',
        ]);

        $this->hasMany('Zadosti', [
            'foreignKey' => 'ucet_id',
            'className' => 'OldDsw.Zadosti'
        ]);

        $this->belongsToMany('StatutarniOrgan', [
            'joinTable' => 'statutarni_organ_fyzicke_osoby',
            'foreignKey' => 'account_id',
            'targetForeignKey' => 'fyzicke_osoby_id',
            'className' => 'OldDsw.FyzickeOsoby'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 45)
            ->notEmptyString('identifier');

        $validator
            ->scalar('nazev')
            ->maxLength('nazev', 255)
            ->notEmptyString('nazev');

        $validator
            ->scalar('pravni_forma')
            ->maxLength('pravni_forma', 85)
            ->notEmptyString('pravni_forma');

        $validator
            ->scalar('ico')
            ->maxLength('ico', 45)
            ->allowEmptyString('ico');

        $validator
            ->scalar('dic')
            ->maxLength('dic', 45)
            ->allowEmptyString('dic');

        $validator
            ->scalar('ulice')
            ->maxLength('ulice', 255)
            ->notEmptyString('ulice');

        $validator
            ->scalar('obec')
            ->maxLength('obec', 255)
            ->notEmptyString('obec');

        $validator
            ->scalar('psc')
            ->maxLength('psc', 45)
            ->notEmptyString('psc');

        $validator
            ->scalar('nazev_banky')
            ->maxLength('nazev_banky', 45)
            ->notEmptyString('nazev_banky');

        $validator
            ->scalar('cislo_uctu')
            ->maxLength('cislo_uctu', 45)
            ->notEmptyString('cislo_uctu');

        $validator
            ->scalar('kod_banky')
            ->maxLength('kod_banky', 45)
            ->notEmptyString('kod_banky');

        $validator
            ->scalar('typ_uctu')
            ->maxLength('typ_uctu', 1)
            ->notEmptyString('typ_uctu');

        $validator
            ->dateTime('added_when')
            ->allowEmptyDateTime('added_when');

        $validator
            ->scalar('cast_obce')
            ->maxLength('cast_obce', 105)
            ->allowEmptyString('cast_obce');

        $validator
            ->scalar('cislo_popisne')
            ->maxLength('cislo_popisne', 45)
            ->notEmptyString('cislo_popisne');

        $validator
            ->scalar('cislo_orientacni')
            ->maxLength('cislo_orientacni', 45)
            ->allowEmptyString('cislo_orientacni');

        $validator
            ->scalar('obec_dorucovaci')
            ->maxLength('obec_dorucovaci', 105)
            ->allowEmptyString('obec_dorucovaci');

        $validator
            ->scalar('cast_obce_dorucovaci')
            ->maxLength('cast_obce_dorucovaci', 105)
            ->allowEmptyString('cast_obce_dorucovaci');

        $validator
            ->scalar('ulice_dorucovaci')
            ->maxLength('ulice_dorucovaci', 105)
            ->allowEmptyString('ulice_dorucovaci');

        $validator
            ->scalar('cislo_popisne_dorucovaci')
            ->maxLength('cislo_popisne_dorucovaci', 45)
            ->allowEmptyString('cislo_popisne_dorucovaci');

        $validator
            ->scalar('cislo_orientacni_dorucovaci')
            ->maxLength('cislo_orientacni_dorucovaci', 45)
            ->allowEmptyString('cislo_orientacni_dorucovaci');

        $validator
            ->scalar('psc_dorucovaci')
            ->maxLength('psc_dorucovaci', 45)
            ->allowEmptyString('psc_dorucovaci');

        $validator
            ->scalar('telefon')
            ->maxLength('telefon', 45)
            ->allowEmptyString('telefon');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('poznamka')
            ->allowEmptyString('poznamka');

        $validator
            ->numeric('kod_pravni_formy')
            ->greaterThanOrEqual('kod_pravni_formy', 0)
            ->notEmptyString('kod_pravni_formy');

        $validator
            ->scalar('cislo_jednaci')
            ->allowEmptyString('cislo_jednaci');

        $validator
            ->scalar('sz')
            ->allowEmptyString('sz');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
