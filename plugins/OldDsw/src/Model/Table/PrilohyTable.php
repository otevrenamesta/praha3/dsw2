<?php

namespace OldDsw\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use OldDsw\Model\Entity\Prilohy;

/**
 * Prilohy Model
 *
 * @property RequestsTable&BelongsTo $Requests
 * @property OhlasovaciFormularTable&BelongsToMany $OhlasovaciFormular
 *
 * @method Prilohy get($primaryKey, $options = [])
 * @method Prilohy newEntity($data = null, array $options = [])
 * @method Prilohy[] newEntities(array $data, array $options = [])
 * @method Prilohy|false save(EntityInterface $entity, $options = [])
 * @method Prilohy saveOrFail(EntityInterface $entity, $options = [])
 * @method Prilohy patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Prilohy[] patchEntities($entities, array $data, array $options = [])
 * @method Prilohy findOrCreate($search, callable $callback = null, $options = [])
 */
class PrilohyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prilohy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Zadost', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            'className' => 'OldDsw.Zadosti',
        ]);
        $this->belongsToMany('OhlasovaciFormular', [
            'foreignKey' => 'prilohy_id',
            'targetForeignKey' => 'ohlasovaci_formular_id',
            'joinTable' => 'ohlasovaci_formular_prilohy',
            'className' => 'OldDsw.OhlasovaciFormular',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('filename')
            ->maxLength('filename', 255)
            ->notEmptyFile('filename');

        $validator
            ->scalar('orig_filename')
            ->maxLength('orig_filename', 255)
            ->notEmptyFile('orig_filename');

        $validator
            ->numeric('filesize')
            ->notEmptyFile('filesize');

        $validator
            ->dateTime('added_when')
            ->notEmptyDateTime('added_when');

        $validator
            ->scalar('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'Requests'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'dsw';
    }
}
