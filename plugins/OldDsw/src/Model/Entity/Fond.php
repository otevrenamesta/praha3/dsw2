<?php

namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fondy Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property bool $active
 * @property bool $deleted
 * @property string $url_identifier
 * @property string $classname
 *
 * @property GrantAttachmentSetting[] $grant_attachment_settings
 * @property GrantRequest[] $grant_requests
 * @property GridsColumnsSetting[] $grids_columns_settings
 * @property PrilohyFormulareNastaveni[] $prilohy_formulare_nastaveni
 * @property Program[] $programs
 * @property Role[] $roles
 * @property Zadost[] $zadosti
 */
class Fond extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'active' => true,
        'deleted' => true,
        'url_identifier' => true,
        'classname' => true,
        'grant_attachment_settings' => true,
        'grant_requests' => true,
        'grids_columns_settings' => true,
        'prilohy_formulare_nastaveni' => true,
        'programs' => true,
        'roles' => true,
        'zadosti' => true,
    ];
}
