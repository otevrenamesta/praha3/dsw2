<?php

namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * StatutarniOrganFyzickeOsoby Entity
 *
 * @property int $account_id
 * @property int $fyzicke_osoby_id
 *
 * @property Ucet $ucet
 * @property FyzickaOsoba $fyzicka_osoba
 */
class StatutarniOrganFyzickeOsoby extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account' => true,
        'fyzicke_osoby' => true,
    ];
}
