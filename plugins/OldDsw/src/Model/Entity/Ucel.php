<?php
namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ucely Entity
 *
 * @property int $id
 * @property int $program_id
 * @property string $name
 * @property string $description
 * @property bool $active
 *
 * @property Program $program
 * @property DotacniFondData[] $dotacni_fond_data
 * @property GrantAttachmentSetting[] $grant_attachment_settings
 * @property GrantRequest[] $grant_requests
 * @property Hodnoceni[] $hodnoceni
 * @property PrilohyFormulareNastaveni[] $prilohy_formulare_nastaveni
 * @property Questionnaire[] $questionnaires
 * @property RatingQuestionnaire[] $rating_questionnaires
 * @property RozvojData[] $rozvoj_data
 */
class Ucel extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'program_id' => true,
        'name' => true,
        'description' => true,
        'active' => true,
        'program' => true,
        'dotacni_fond_data' => true,
        'grant_attachment_settings' => true,
        'grant_requests' => true,
        'hodnoceni' => true,
        'prilohy_formulare_nastaveni' => true,
        'questionnaires' => true,
        'rating_questionnaires' => true,
        'rozvoj_data' => true,
    ];
}
