<?php
namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Prilohy Entity
 *
 * @property int $id
 * @property int $request_id
 * @property string $filename
 * @property string $orig_filename
 * @property float $filesize
 * @property FrozenTime $added_when
 * @property string $identifier
 *
 * @property Zadost $zadost
 * @property OhlasovaciFormular[] $ohlasovaci_formular
 */
class Prilohy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'request_id' => true,
        'filename' => true,
        'orig_filename' => true,
        'filesize' => true,
        'added_when' => true,
        'identifier' => true,
        'request' => true,
        'ohlasovaci_formular' => true,
    ];
}
