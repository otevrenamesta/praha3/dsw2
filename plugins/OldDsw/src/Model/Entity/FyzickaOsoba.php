<?php

namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * FyzickeOsoby Entity
 *
 * @property int $id
 * @property string|null $funkce
 * @property string|null $titul
 * @property string $jmeno
 * @property string $prijmeni
 * @property string|null $datum_narozeni
 * @property string|null $email
 * @property string|null $cislo_OP
 * @property string|null $podil
 * @property string|null $podil_splaceno_procent
 * @property string|null $podil_vkladu
 * @property string|null $obec
 * @property string|null $cast_obce
 * @property string|null $ulice
 * @property string|null $cislo_popisne
 * @property string|null $cislo_orientacni
 * @property string|null $psc
 * @property string|null $stat
 * @property string|null $telefon
 * @property string|null $rodne_cislo
 * @property string|null $titul_za_jmenem
 *
 * @property Ucet[] $ucty
 */
class FyzickaOsoba extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'funkce' => true,
        'titul' => true,
        'jmeno' => true,
        'prijmeni' => true,
        'datum_narozeni' => true,
        'email' => true,
        'cislo_OP' => true,
        'podil' => true,
        'podil_splaceno_procent' => true,
        'podil_vkladu' => true,
        'obec' => true,
        'cast_obce' => true,
        'ulice' => true,
        'cislo_popisne' => true,
        'cislo_orientacni' => true,
        'psc' => true,
        'stat' => true,
        'telefon' => true,
        'rodne_cislo' => true,
        'titul_za_jmenem' => true,
        'osoby_s_podilem_fyzicke_osoby' => true,
        'statutarni_organ_fyzicke_osoby' => true,
        'ucty' => true,
    ];

    public function formatFullName(): string
    {
        return sprintf("%s %s %s %s", $this->titul, $this->jmeno, $this->prijmeni, $this->titul_za_jmenem);
    }
}
