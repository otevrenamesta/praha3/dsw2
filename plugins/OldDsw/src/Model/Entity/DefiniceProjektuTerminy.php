<?php
namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\ORM\Entity;

/**
 * DefiniceProjektuTerminy Entity
 *
 * @property int $id
 * @property int $zadost_id
 * @property FrozenDate $zahajeni
 * @property FrozenDate $ukonceni
 * @property string $misto_realizace
 *
 * @property Zadost $zadost
 */
class DefiniceProjektuTerminy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'zadost_id' => true,
        'zahajeni' => true,
        'ukonceni' => true,
        'misto_realizace' => true,
        'zadost' => true,
    ];
}
