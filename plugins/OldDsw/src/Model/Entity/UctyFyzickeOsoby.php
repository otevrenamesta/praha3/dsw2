<?php
namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * UctyFyzickeOsoby Entity
 *
 * @property int $ucty_id
 * @property int $fyzicke_osoby_id
 *
 * @property Ucty $ucty
 * @property FyzickeOsoby $fyzicke_osoby
 */
class UctyFyzickeOsoby extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ucty' => true,
        'fyzicke_osoby' => true,
    ];
}
