<?php

namespace OldDsw\Model\Entity;

use Cake\ORM\Entity;

/**
 * PrilohyFormulareProhlaseniOVerejnePodpore Entity
 *
 * @property int $zadost_id
 * @property int $poskytnuti_verejne_podpory
 * @property int $rezim_podpory
 */
class PrilohyFormulareProhlaseniOVerejnePodpore extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'poskytnuti_verejne_podpory' => true,
        'rezim_podpory' => true,
    ];

    public function summary(): string
    {
        $poskytnuti = $this->poskytnuti_verejne_podpory === 1 ? 'ANO' : 'NE';
        $rezim = '';
        switch ($this->rezim_podpory) {
            default:
            case 0:
                $rezim = 'Podpora de minimis';
                break;
            // socialka 1 = vyrovnavaci platba
            case 1:
            case 2:
                $rezim = 'Mimo režim veřejné podpory';
                break;
        }

        return sprintf("Veřejná podpora? %s, Režim podpory: %s", $poskytnuti, $rezim);
    }
}
