<?php
namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Uzivatele Entity
 *
 * @property int $id
 * @property int $ucet_id
 * @property string $jmeno
 * @property string $prijmeni
 * @property string|null $email
 * @property string|null $telefon
 * @property bool $aktivni
 * @property FrozenTime $added_when
 * @property bool $is_superuser
 * @property string $login
 * @property string $password
 * @property bool $smazano
 * @property FrozenTime|null $smazano_kdy
 * @property int|null $smazano_kym
 * @property string|null $restart_hash
 *
 * @property Ucet $ucet
 */
class Uzivatel extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ucet_id' => true,
        'jmeno' => true,
        'prijmeni' => true,
        'email' => true,
        'telefon' => true,
        'aktivni' => true,
        'added_when' => true,
        'is_superuser' => true,
        'login' => true,
        'password' => true,
        'smazano' => true,
        'smazano_kdy' => true,
        'smazano_kym' => true,
        'restart_hash' => true,
        'ucet' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
}
