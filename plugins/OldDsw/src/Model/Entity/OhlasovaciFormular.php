<?php

namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * OhlasovaciFormular Entity
 *
 * @property int $id
 * @property int $zadost_id
 * @property int $typ
 * @property FrozenDate|null $datum_zahajeni
 * @property FrozenDate $datum_ukonceni
 * @property string $misto_konani
 * @property string $garant
 * @property string $garant_telefon
 * @property string $garant_email
 * @property float $cerpana_castka
 * @property int|null $priloha_id
 * @property string $poznamka
 * @property FrozenTime $added_when
 * @property int $added_by_uzivatel_id
 * @property string $pocet_ucastniku
 * @property bool $celorocni_cinnost
 *
 * @property Zadost $zadost
 * @property OhlasovaciFormularPriloha $prilohy
 * @property Uzivatel $added_by_uzivatel
 */
class OhlasovaciFormular extends Entity
{
    public const TYPY_OHLASENI = [
        0 => 'akce',
        1 => 'dílo',
        2 => 'celoroční činnost'
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'zadost_id' => true,
        'typ' => true,
        'datum_zahajeni' => true,
        'datum_ukonceni' => true,
        'misto_konani' => true,
        'garant' => true,
        'garant_telefon' => true,
        'garant_email' => true,
        'cerpana_castka' => true,
        'priloha_id' => true,
        'poznamka' => true,
        'added_when' => true,
        'added_by_uzivatel_id' => true,
        'pocet_ucastniku' => true,
        'celorocni_cinnost' => true,
        'zadost' => true,
        'priloha' => true,
        'added_by_uzivatel' => true,
        'prilohy' => true,
    ];
}
