<?php
namespace OldDsw\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\ORM\Entity;

/**
 * DefiniceProjektu Entity
 *
 * @property int $zadost_id
 * @property string|null $ucel
 * @property string|null $cilova_skupina
 * @property string|null $vztah_k_Praha_3
 * @property FrozenDate|null $termin_zahajeni
 * @property string|null $misto_realizace
 * @property string|null $dosavadni_cinnost
 * @property string|null $pouziti_financi
 */
class DefiniceProjektu extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ucel' => true,
        'cilova_skupina' => true,
        'vztah_k_Praha_3' => true,
        'termin_zahajeni' => true,
        'misto_realizace' => true,
        'dosavadni_cinnost' => true,
        'pouziti_financi' => true,
    ];
}
