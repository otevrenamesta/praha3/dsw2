<?php
/**
 * @var $this AppView
 * @var $zadosti Zadost[]
 * @var $showingHidden bool
 */

use App\View\AppView;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Zadost;

$this->assign('title', $showingHidden ? __d('olddsw', 'Skryté žádosti') : __d('olddsw', 'Archiv žádostí staré verze DSW'));
echo $this->element('simple_datatable');
?>
    <div class="row">
        <div class="col-md-6">
            <h1><?= $this->fetch('title') ?></h1>
        </div>
        <div class="col-md text-right">
            <?php
            $route2020 = ['action' => 'dswZadosti', 'year' => 2020];
            if (Router::routeExists($route2020)) {
                echo $this->Html->link(__('Přehled žádostí na období 2020'), $route2020, ['class' => 'btn btn-success m-2']);
            }
            $hiddenRoute = ['hidden' => true];
            if (!$showingHidden && Router::routeExists($hiddenRoute)) {
                echo $this->Html->link(__('Zobrazit pouze skryté žádosti'), $hiddenRoute, ['class' => 'btn btn-primary m-2']);
            }
            ?>
        </div>
    </div>

<?= $this->element('OldDsw.zadosti', compact('zadosti', 'vyuctovani'));