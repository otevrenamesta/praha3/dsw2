<?php
/**
 * @var $this AppView
 * @var $accounts Ucet[]
 * @var $showingHidden bool
 */

use App\View\AppView;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Ucet;


$this->assign('title', $showingHidden ? __d('olddsw', 'Skrytí žadatelé') : __d('olddsw', 'Archiv žadatelů staré verze DSW'));
echo $this->element('simple_datatable');
?>
<div class="row">
    <div class="col-6">
        <h1><?= $this->fetch('title') ?></h1>
    </div>
    <div class="col text-right">
        <?php
        $hiddenRoute = ['hidden' => true];
        if (!$showingHidden && Router::routeExists($hiddenRoute)) {
            echo $this->Html->link(__('Zobrazit pouze skryté žadatele'), $hiddenRoute, ['class' => 'btn btn-primary m-2']);
        }
        ?>
    </div>
</div>
<table id="dtable" class="table">
    <thead>
    <tr>
        <th><?= __d('olddsw', 'Název / Jméno Příjmení') ?></th>
        <th><?= __d('olddsw', 'Adresa') ?></th>
        <th><?= __d('olddsw', 'IČO') ?></th>
        <th><?= __d('olddsw', 'Telefon') ?></th>
        <th><?= __d('olddsw', 'Právní Forma') ?></th>
        <th><?= __d('olddsw', 'Vytvořeno') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($accounts as $account): ?>
        <tr>
            <td><?= $this->Html->link($account->name_with_year, ['action' => 'dswDetail', $account->id]) ?></td>
            <td><?= $account->formatAddress() ?></td>
            <td><?= $account->ico ?></td>
            <td><?= $account->telefon ?></td>
            <td><?= $account->pravni_forma ?></td>
            <td><?= $account->added_when ? $account->added_when->toDateString() : __('N/A') ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
