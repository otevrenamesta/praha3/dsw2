<?php
/**
 * @var $this AppView
 * @var $zadost Zadost
 */

use App\View\AppView;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Zadost;

$this->assign('title', $zadost->nazev);

$route = ['action' => 'dswHideZadost', $zadost->id];
if (!$zadost->is_hidden && Router::routeExists($route)) {
    echo $this->Html->link(__('Skrýt žádost'), $route, ['class' => 'btn btn-warning']);
}
$ohlaseniRoute = ['action' => 'dswOhlasit', $zadost->id];
if (Router::routeExists($ohlaseniRoute)) {
    echo $this->Html->link(__('Ohlásit akci/činnost'), $ohlaseniRoute, ['class' => 'btn btn-success']);
}

echo $this->element('OldDsw.zadost', ['zadost' => $zadost]);