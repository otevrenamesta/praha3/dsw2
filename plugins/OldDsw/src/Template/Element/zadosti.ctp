<?php
/**
 * @var $this AppView
 * @var $zadosti Zadost[]
 * @var $vyuctovani Settlement[]
 */

if (!isset($zadosti) || empty($zadosti)) {
    return;
}

$indexedVyuctovani = [];
foreach ($vyuctovani ?? [] as $settlement) {
    foreach ($settlement->settlements_to_olddsw_requests ?? [] as $old_link) {
        $indexedVyuctovani[$old_link->zadost_id] = $settlement;
    }
}

use App\Model\Entity\Settlement;
use App\Model\Entity\SettlementState;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Zadost;

?>
<table class="table" id="dtable">
    <thead>
    <tr>
        <th><?= __d('olddsw', 'Název') ?></th>
        <th><?= __d('olddsw', 'Program') ?></th>
        <th><?= __d('olddsw', 'Účel') ?></th>
        <th><?= __d('olddsw', 'Dotační Období') ?></th>
        <th><?= __d('olddsw', 'Požadovaná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Navrhovaná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Konečná výše dotace') ?></th>
        <th><?= __d('olddsw', 'Ohlášeno') ?></th>
        <th><?= __d('olddsw', 'Vyúčtováno') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($zadosti as $zadost): ?>
        <?php if (empty($zadost) || !isset($zadost->nazev) || empty($zadost->nazev)) {
            continue;
        } ?>
        <tr>
            <td><?= $this->Html->link($zadost->nazev, ['action' => 'dswZadost', $zadost->id]) ?></td>
            <td><?= $zadost->program ? $zadost->program->name : __('N/A') ?></td>
            <td><?= $zadost->ucel ? $zadost->ucel->name : __('N/A') ?></td>
            <td><?= $zadost->dotacni_obdobi ?></td>
            <td><?= Number::currency($zadost->pozadovana_castka, 'CZK') ?></td>
            <td><?= Number::currency($zadost->navrhovana_castka, 'CZK') ?></td>
            <td><?= Number::currency($zadost->konecna_castka, 'CZK') ?></td>
            <td class="text-center">
                <span class="d-none"><?= empty($zadost->ohlasovaci_formular) ? '1' : '0' ?></span>
                <?php if (empty($zadost->ohlasovaci_formular) && Router::routeExists(['action' => 'dswOhlasit', $zadost->id])): ?>
                    <?= $this->Html->link(__('Ohlásit akci/činnost'), ['action' => 'dswOhlasit', $zadost->id], ['class' => 'text-success']) ?>
                <?php else: ?>
                    <button type="button" data-toggle="modal" data-target="#modal-<?= $zadost->id ?>"
                            class="btn pr-2 <?= empty($zadost->ohlasovaci_formular) ? 'btn-danger' : 'btn-success' ?>">
                        &nbsp;
                    </button>
                    <?php if (!empty($zadost->ohlasovaci_formular)): ?>
                        <div class="modal fade" id="modal-<?= $zadost->id ?>" tabindex="-1" role="dialog"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <?php if (Router::routeExists(['action' => 'dswOhlasit', $zadost->id])) {
                                        echo $this->Html->link(__('Ohlásit akci/činnost'), ['action' => 'dswOhlasit', $zadost->id], ['class' => 'btn btn-success']);
                                        echo '<hr/>';
                                    }
                                    ?>
                                    <?php foreach ($zadost->ohlasovaci_formular as $ohlaseni) {
                                        echo $this->element('OldDsw.ohlaseni', ['ohlaseni' => $ohlaseni, 'card' => true]);
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td>
                <?php if (!empty($indexedVyuctovani)) {
                    $vyuctovani = $indexedVyuctovani[$zadost->id] ?? null;
                    ?>
                    <span class="d-none"><?= empty($vyuctovani) ? '1' : '0' ?></span>
                    <?php
                    $settlementRoute = ['action' => 'dswVyuctovani', 'id' => $zadost->id];
                    if (empty($vyuctovani) && Router::routeExists($settlementRoute)) {
                        echo $this->Html->link(__('Vytvořit vyúčtování'), $settlementRoute, ['class' => 'text-success']);
                    } elseif (!empty($vyuctovani) && $vyuctovani->canUserEdit() && $vyuctovani->settlement_state_id !== SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES && Router::routeExists($settlementRoute)) {
                        echo $this->Html->link(__('Upravit rozpracované vyúčtování'), $settlementRoute, ['class' => 'text-success']);
                    } elseif (!empty($vyuctovani) && $vyuctovani->settlement_state_id === SettlementState::STATE_SUBMITTED_READY_TO_REVIEW && Router::routeExists($settlementRoute)) {
                        echo $this->Html->link(__('Zobrazit odeslané vyúčtování'), $settlementRoute, ['class' => 'text-success']);
                    } elseif (!empty($vyuctovani) && Router::routeExists($settlementRoute)) {
                        if ($vyuctovani->settlement_state_id === SettlementState::STATE_ECONOMICS_APPROVED) {
                            $text = __('Vyúčtování bylo potvrzeno');
                            $class = 'success';
                        } elseif ($vyuctovani->settlement_state_id === SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES) {
                            $text = __('Opravit vrácené vyúčtování');
                            $class = 'danger';
                        } else {
                            $text = __('Zobrazit vyúčtování');
                            $class = 'info';
                        }
                        echo $this->Html->link($text, $settlementRoute, ['class' => 'text-' . $class]);
                    } else {
                        $color = '';
                        $label = '';
                        switch ($vyuctovani->settlement_state_id ?? null) {
                            default:
                                $color = 'danger';
                                $label = __('Nevyúčtováno');
                                break;
                            case SettlementState::STATE_NEW:
                            case SettlementState::STATE_FILLED_COMPLETELY:
                            case SettlementState::STATE_SUBMITTED_READY_TO_REVIEW:
                            case SettlementState::STATE_ECONOMICS_REQUESTED_CHANGES:
                                $color = 'warning';
                                $label = SettlementState::getLabel($vyuctovani->settlement_state_id);
                                break;
                            case SettlementState::STATE_ECONOMICS_APPROVED:
                                $color = 'success';
                                $label = SettlementState::getLabel($vyuctovani->settlement_state_id);
                                break;
                        }
                        ?>

                        <button type="button"
                                class="btn pr-2 btn-<?= $color ?>">
                            <?= $label ?>
                        </button>
                        <?php
                    }
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
