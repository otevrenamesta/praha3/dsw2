<?php
/**
 * @var $this AppView
 * @var $zadost Zadost
 */

if (!isset($zadost) || empty($zadost)) {
    return;
}

use App\View\AppView;
use Cake\I18n\Number;
use Cake\Routing\Router;
use OldDsw\Model\Entity\Zadost;

?>

<div class="card mt-2">
    <h2 class="card-header">Žádost</h2>
    <div class="card-body">
        <table class="table">
            <tr>
                <td><strong>Název projektu</strong></td>
                <td><?= $zadost->nazev ?></td>
                <td><strong>Identifikační číslo žádosti</strong></td>
                <td><?= $zadost->id ?></td>
            </tr>
        </table>
    </div>
</div>
<div class="card mt-2">
    <h2 class="card-header">Žadatel</h2>
    <div class="card-body">
        <?= $this->element('OldDsw.ucet', ['ucet' => $zadost->ucet]) ?>
    </div>
</div>
<div class="card mt-2">
    <h2 class="card-header">Základní informace</h2>
    <div class="card-body">
        <div class="row">
            <div class="col-md">
                <table class="table">
                    <tr>
                        <td><strong>Název dotačního programu</strong></td>
                        <td><?= $zadost->fond ? $zadost->fond->name : __('N/A') ?></td>
                    </tr>
                    <tr>
                        <td><strong>Oblast dotačního programu</strong></td>
                        <td><?= $zadost->program ? $zadost->program->name : __('N/A') ?></td>
                    </tr>
                    <tr>
                        <td><strong>Účel</strong></td>
                        <td><?= $zadost->ucel ? $zadost->ucel->name : __('N/A') ?></td>
                    </tr>
                    <tr>
                        <td><strong>Prohlášení o veřejné podpoře</strong></td>
                        <td><?= $zadost->prilohy_formulare_prohlaseni_o_verejne_podpore ? $zadost->prilohy_formulare_prohlaseni_o_verejne_podpore->summary() : '' ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Účel, oblast a cíl projektu, zdůvodnění a předpokládaný
                                přínos</strong></td>
                    </tr>
                    <?php if ($zadost->definice_projektu): ?>
                        <tr>
                            <td colspan="2">
                                <?= $zadost->definice_projektu->ucel ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>
            <?php if ($zadost->definice_projektu): ?>
                <div class="col-md">
                    <table class="table">
                        <tr>
                            <td colspan="2"><strong>Vztah projektu k MČ Praha 3</strong></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?= $zadost->definice_projektu->vztah_k_Praha_3 ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="row">
                                    <div class="col-md"><strong>Předpokládané zahájení</strong></div>
                                    <div class="col-md"><strong>Místo realizace</strong></div>
                                    <div class="col-md"><strong>Předpokládané ukončení</strong></div>
                                </div>
                                <?php if ($zadost->definice_projektu && $zadost->definice_projektu->terminy): ?>
                                    <div class="row">
                                        <div class="col-md"><?= $zadost->definice_projektu->terminy->zahajeni ?></div>
                                        <div class="col-md"><?= $zadost->definice_projektu->terminy->misto_realizace ?></div>
                                        <div class="col-md"><?= $zadost->definice_projektu->terminy->ukonceni ?></div>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Dosavadní činnost žadatele</strong></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?= str_replace('\n', '<br/>', $zadost->definice_projektu->dosavadni_cinnost) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><strong>Konkrétní účel použití fin. prostředků v souladu s rozpočtem a
                                    zásadami pro příslušný program</strong></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?= str_replace('\n', '<br/>', $zadost->definice_projektu->pouziti_financi) ?>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($zadost->rozpocty_projektu): ?>
    <div class="card mt-2">
        <h2 class="card-header">Rozpočet projektu</h2>
        <div class="card-body">
            <table class="table">
                <tr>
                    <td><strong>Celkové náklady projektu</strong></td>
                    <td><?= Number::currency($zadost->rozpocty_projektu->celkove_naklady, 'CZK') ?></td>
                </tr>
                <tr>
                    <td><strong>Celkové výnosy projektu</strong></td>
                    <td><?= Number::currency($zadost->rozpocty_projektu->celkove_vynosy, 'CZK') ?></td>
                </tr>
                <tr>
                    <td><strong>Požadovaná výše dotace</strong></td>
                    <td><?= Number::currency($zadost->pozadovana_castka, 'CZK') ?></td>
                </tr>
            </table>
            <strong>Vlastní Zdroje</strong>
            <table class="table">
                <?php $vlastni_zdroje_celkem = 0;
                foreach ($zadost->rozpocty_projektu->rozpocty_projektu_vlastni_zdroje as $vlastni_zdroj):
                    $vlastni_zdroje_celkem += $vlastni_zdroj->castka; ?>
                    <tr>
                        <td><?= $vlastni_zdroj->nazev ?></td>
                        <td><?= Number::currency($vlastni_zdroj->castka, 'CZK') ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td><strong>Vlastní Zdroje Celkem</strong></td>
                    <td><strong><?= Number::currency($vlastni_zdroje_celkem, 'CZK') ?></strong></td>
                </tr>
            </table>
            <strong>Dotace z jiných zdrojů - včetně podaných žádostí</strong>
            <table class="table">
                <tr>
                    <td><strong>Ministerstva ČR</strong></td>
                    <td><?= Number::currency($zadost->rozpocty_projektu->dotace_z_ministerstva_cr, 'CZK') ?></td>
                </tr>
                <tr>
                    <td><strong>EU</strong></td>
                    <td><?= Number::currency($zadost->rozpocty_projektu->dotace_z_eu, 'CZK') ?></td>
                </tr>
                <tr>
                    <td><strong>MHMP</strong></td>
                    <td><?= Number::currency($zadost->rozpocty_projektu->dotace_z_mhmp, 'CZK') ?></td>
                </tr>
                <?php foreach ($zadost->rozpocty_projektu->rozpocty_projektu_jine_zdroje as $jiny_zdroj): ?>
                    <tr>
                        <td><strong><?= $jiny_zdroj->nazev ?></strong></td>
                        <td><?= Number::currency($jiny_zdroj->castka, 'CZK') ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>

    <div class="card mt-2">
        <h2 class="card-header">Ekonomická rozvaha</h2>
        <div class="card-body">
            <div class="row">
                <div class="col-md">
                    <strong>Položkový rozpis výnosů</strong>
                    <table class="table">
                        <?php foreach ($zadost->rozpocty_projektu->rozpocty_projektu_vynosy as $vynos): ?>
                            <tr>
                                <td><?= $vynos->nazev ?></td>
                                <td><?= Number::currency($vynos->castka, 'CZK') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div class="col-md">
                    <strong>Položkový rozpis nákladů</strong>
                    <table class="table">
                        <?php foreach ($zadost->rozpocty_projektu->rozpocty_projektu_naklady as $naklad): ?>
                            <tr>
                                <td><?= $naklad->nazev ?></td>
                                <td><?= Number::currency($naklad->castka, 'CZK') ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <?php foreach ($zadost->rozpocty_projektu->rozpocty_projektu_naklady_investicni as $naklad): ?>
                            <tr>
                                <td><?= $naklad->nazev ?></td>
                                <td><?= Number::currency($naklad->castka, 'CZK') ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <strong>Bilance (výnosy minus náklady)</strong>
                </div>
                <div class="col-md text-right font-weight-bold">
                    <?= Number::currency($zadost->rozpocty_projektu->celkove_vynosy - $zadost->rozpocty_projektu->celkove_naklady, 'CZK') ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (false): ?>
    <div class="card mt-2">
        <h4 class="card-header">Přehled obdržených finančních prostředků z rozpočtu MČ Praha 3, MHMP nebo státního
            rozpočtu v posledních dvou letech.</h4>
        <div class="card-body">

        </div>
    </div>
<?php endif; ?>

<div class="card mt-2">
    <h2 class="card-header">Hodnocení</h2>
    <div class="card-body">
        <table class="table">
            <tr>
                <td><strong>Slovní komentář</strong></td>
                <td><strong>Výsledné hodnocení</strong></td>
            </tr>
            <tr>
                <td>
                    <?= $zadost->ratingMessage ?>
                </td>
                <td>
                    <?= $zadost->pocet_bodu ?>
                </td>
            </tr>
        </table>
    </div>
</div>

<?php if (!empty($zadost->ohlasovaci_formular)): ?>
    <div class="card mt-2">
        <h2 class="card-header">Ohlášení akce/činnosti</h2>
        <div class="card-body">
            <?php foreach ($zadost->ohlasovaci_formular as $ohlaseni): ?>
                <?= $this->element('OldDsw.ohlaseni', compact('ohlaseni')) ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<div class="card mt-2">
    <h2 class="card-header">Přílohy žádosti</h2>
    <div class="card-body">
        <table class="table">
            <thead>
            <tr>
                <th>Typ Přílohy</th>
                <th>Soubor</th>
                <th>Velikost</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($zadost->prilohy_zadosti as $priloha): ?>
                <tr>
                    <td><?= $priloha->identifier ?></td>
                    <td><?= Router::routeExists(['action' => 'dswDownload', $priloha->id]) ? $this->Html->link($priloha->orig_filename, ['action' => 'dswDownload', $priloha->id], ['target' => '_blank']) : h($priloha->orig_filename) ?></td>
                    <td><?= round($priloha->filesize / 1024 / 1024, 2) ?> MB</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
