<?php
/**
 * @var $this AppView
 * @var $ucet Ucet
 * @var $short bool
 * @var $public_only bool
 */

if (!isset($ucet) || empty($ucet)) {
    return;
}
if (!isset($short) || !is_bool($short)) {
    $short = false;
}
if (!isset($public_only) || !is_bool($public_only)) {
    $public_only = false;
}

use App\View\AppView;
use OldDsw\Model\Entity\Ucet;

?>
<div class="row">
    <div class="col-md">
        <table class="table">
            <tr>
                <td>
                    <?= __d('olddsw', 'Název') ?>
                </td>
                <td>
                    <?= $ucet->nazev ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'Právní Forma') ?>
                </td>
                <td>
                    <?= $ucet->pravni_forma ?>
                </td>
            </tr>
            <?php if (!$public_only): ?>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Adresa') ?>
                    </td>
                    <td>
                        <?= $ucet->formatAddress() ?>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if (!$short && !$ucet->isPO()): ?>
                <?php if (!$public_only): ?>
                    <tr>
                        <td>
                            <?= __d('olddsw', 'Adresa doručovací') ?>
                        </td>
                        <td>
                            <?= $ucet->formatDorucovaciAddress() ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Datum Narození') ?>
                    </td>
                    <td>
                        <?= empty($ucet->fyzicke_osoby) ? '' : substr($ucet->fyzicke_osoby[0]->datum_narozeni, 0, $public_only ? -6 : 10) ?>
                    </td>
                </tr>
                <?php if (!$public_only): ?>
                    <tr>
                        <td>
                            <?= __d('olddsw', 'Číslo OP') ?>
                        </td>
                        <td>
                            <?= empty($ucet->fyzicke_osoby) ? '' : $ucet->fyzicke_osoby[0]->cislo_OP ?>
                        </td>
                    </tr>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (!$public_only): ?>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Telefon') ?>
                    </td>
                    <td>
                        <?= $ucet->telefon ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Email') ?>
                    </td>
                    <td>
                        <?= $ucet->email ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>
                    <?= __d('olddsw', 'IČO') ?>
                </td>
                <td>
                    <?= $ucet->dic ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= __d('olddsw', 'DIČ') ?>
                </td>
                <td>
                    <?= $ucet->ico ?>
                </td>
            </tr>
        </table>
        <?php if (!$short && $ucet->isPO() && !empty($ucet->statutarni_organ)): ?>
            <strong>Statutární orgán, osoby zastupující právnickou osobu</strong>
            <table class="table">
                <tr>
                    <td>Jméno a Příjmení</td>
                    <td><?= $ucet->statutarni_organ[0]->formatFullName() ?></td>
                </tr>
                <tr>
                    <td>Právní důvod zastoupení</td>
                    <td><?= $ucet->statutarni_organ[0]->funkce ?></td>
                </tr>
                <?php if (!$public_only): ?>
                    <tr>
                        <td>Telefon</td>
                        <td><?= $ucet->statutarni_organ[0]->telefon ?></td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td><?= $ucet->statutarni_organ[0]->email ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        <?php endif; ?>
    </div>
    <?php if (!$short && !$public_only): ?>
        <div class="col-md">
            <table class="table">
                <tr>
                    <td>
                        <?= __d('olddsw', 'Bankovní spojení') ?>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Název Banky') ?>
                    </td>
                    <td>
                        <?= $ucet->nazev_banky ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= __d('olddsw', 'Číslo účtu / Kód banky') ?>
                    </td>
                    <td>
                        <?= $ucet->cislo_uctu . '/' . $ucet->kod_banky ?>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif; ?>
</div>