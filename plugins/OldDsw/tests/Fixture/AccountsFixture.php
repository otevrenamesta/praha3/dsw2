<?php
namespace OldDsw\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AccountsFixture
 */
class AccountsFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'ucty';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'identifier' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazev' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'pravni_forma' => ['type' => 'string', 'length' => 85, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ico' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dic' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ulice' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'obec' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'psc' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nazev_banky' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cislo_uctu' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'kod_banky' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'typ_uctu' => ['type' => 'string', 'length' => 1, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'added_when' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'cast_obce' => ['type' => 'string', 'length' => 105, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cislo_popisne' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cislo_orientacni' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'obec_dorucovaci' => ['type' => 'string', 'length' => 105, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cast_obce_dorucovaci' => ['type' => 'string', 'length' => 105, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'ulice_dorucovaci' => ['type' => 'string', 'length' => 105, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cislo_popisne_dorucovaci' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cislo_orientacni_dorucovaci' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'psc_dorucovaci' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'telefon' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'email' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'poznamka' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'kod_pravni_formy' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => ''],
        'cislo_jednaci' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_czech_ci', 'comment' => '', 'precision' => null],
        'sz' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_czech_ci', 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'identifier' => 'Lorem ipsum dolor sit amet',
                'nazev' => 'Lorem ipsum dolor sit amet',
                'pravni_forma' => 'Lorem ipsum dolor sit amet',
                'ico' => 'Lorem ipsum dolor sit amet',
                'dic' => 'Lorem ipsum dolor sit amet',
                'ulice' => 'Lorem ipsum dolor sit amet',
                'obec' => 'Lorem ipsum dolor sit amet',
                'psc' => 'Lorem ipsum dolor sit amet',
                'nazev_banky' => 'Lorem ipsum dolor sit amet',
                'cislo_uctu' => 'Lorem ipsum dolor sit amet',
                'kod_banky' => 'Lorem ipsum dolor sit amet',
                'typ_uctu' => 'L',
                'added_when' => '2020-04-28 11:01:53',
                'cast_obce' => 'Lorem ipsum dolor sit amet',
                'cislo_popisne' => 'Lorem ipsum dolor sit amet',
                'cislo_orientacni' => 'Lorem ipsum dolor sit amet',
                'obec_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'cast_obce_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'ulice_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'cislo_popisne_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'cislo_orientacni_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'psc_dorucovaci' => 'Lorem ipsum dolor sit amet',
                'telefon' => 'Lorem ipsum dolor sit amet',
                'email' => 'Lorem ipsum dolor sit amet',
                'poznamka' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'kod_pravni_formy' => 1,
                'cislo_jednaci' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'sz' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            ],
        ];
        parent::init();
    }
}
