<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementLogsTable Test Case
 */
class SettlementLogsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementLogsTable
     */
    public $SettlementLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementLogs',
        'app.Settlements',
        'app.SettlementStates',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementLogs') ? [] : ['className' => SettlementLogsTable::class];
        $this->SettlementLogs = TableRegistry::getTableLocator()->get('SettlementLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
