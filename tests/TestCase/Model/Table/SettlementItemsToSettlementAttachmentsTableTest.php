<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementItemsToSettlementAttachmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementItemsToSettlementAttachmentsTable Test Case
 */
class SettlementItemsToSettlementAttachmentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementItemsToSettlementAttachmentsTable
     */
    public $SettlementItemsToSettlementAttachments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementItemsToSettlementAttachments',
        'app.SettlementItems',
        'app.SettlementAttachments',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementItemsToSettlementAttachments') ? [] : ['className' => SettlementItemsToSettlementAttachmentsTable::class];
        $this->SettlementItemsToSettlementAttachments = TableRegistry::getTableLocator()->get('SettlementItemsToSettlementAttachments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementItemsToSettlementAttachments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
