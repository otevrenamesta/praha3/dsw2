<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementItemsTable Test Case
 */
class SettlementItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementItemsTable
     */
    public $SettlementItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementItems',
        'app.Settlements',
        'app.SettlementAttachments',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementItems') ? [] : ['className' => SettlementItemsTable::class];
        $this->SettlementItems = TableRegistry::getTableLocator()->get('SettlementItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
