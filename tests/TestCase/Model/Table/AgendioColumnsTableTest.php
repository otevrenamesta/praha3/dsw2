<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AgendioColumnsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AgendioColumnsTable Test Case
 */
class AgendioColumnsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var AgendioColumnsTable
     */
    public $AgendioColumns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AgendioColumns',
        'app.AgendioReports',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AgendioColumns') ? [] : ['className' => AgendioColumnsTable::class];
        $this->AgendioColumns = TableRegistry::getTableLocator()->get('AgendioColumns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AgendioColumns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
