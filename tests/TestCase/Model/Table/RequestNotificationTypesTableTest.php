<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestNotificationTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestNotificationTypesTable Test Case
 */
class RequestNotificationTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestNotificationTypesTable
     */
    public $RequestNotificationTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestNotificationTypes',
        'app.Organizations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestNotificationTypes') ? [] : ['className' => RequestNotificationTypesTable::class];
        $this->RequestNotificationTypes = TableRegistry::getTableLocator()->get('RequestNotificationTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestNotificationTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
