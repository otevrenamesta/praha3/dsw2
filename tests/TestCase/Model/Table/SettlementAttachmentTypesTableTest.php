<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementAttachmentTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementAttachmentTypesTable Test Case
 */
class SettlementAttachmentTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementAttachmentTypesTable
     */
    public $SettlementAttachmentTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementAttachmentTypes',
        'app.SettlementAttachments',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementAttachmentTypes') ? [] : ['className' => SettlementAttachmentTypesTable::class];
        $this->SettlementAttachmentTypes = TableRegistry::getTableLocator()->get('SettlementAttachmentTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementAttachmentTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
