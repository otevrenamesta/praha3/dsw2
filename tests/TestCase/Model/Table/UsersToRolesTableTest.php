<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersToRolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersToRolesTable Test Case
 */
class UsersToRolesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var UsersToRolesTable
     */
    public $UsersToRoles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersToRoles',
        'app.Users',
        'app.UserRoles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersToRoles') ? [] : ['className' => UsersToRolesTable::class];
        $this->UsersToRoles = TableRegistry::getTableLocator()->get('UsersToRoles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersToRoles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
