<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementAttachmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementAttachmentsTable Test Case
 */
class SettlementAttachmentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementAttachmentsTable
     */
    public $SettlementAttachments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementAttachments',
        'app.Settlements',
        'app.SettlementItems',
        'app.SettlementAttachmentTypes',
        'app.Files',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementAttachments') ? [] : ['className' => SettlementAttachmentsTable::class];
        $this->SettlementAttachments = TableRegistry::getTableLocator()->get('SettlementAttachments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementAttachments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
