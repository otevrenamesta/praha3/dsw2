<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestsToSettlementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestsToSettlementsTable Test Case
 */
class RequestsToSettlementsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var RequestsToSettlementsTable
     */
    public $RequestsToSettlements;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestsToSettlements',
        'app.Requests',
        'app.Settlements',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestsToSettlements') ? [] : ['className' => RequestsToSettlementsTable::class];
        $this->RequestsToSettlements = TableRegistry::getTableLocator()->get('RequestsToSettlements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestsToSettlements);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
