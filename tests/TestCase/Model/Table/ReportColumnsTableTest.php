<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReportColumnsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReportColumnsTable Test Case
 */
class ReportColumnsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ReportColumnsTable
     */
    public $ReportColumns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ReportColumns',
        'app.Reports',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ReportColumns') ? [] : ['className' => ReportColumnsTable::class];
        $this->ReportColumns = TableRegistry::getTableLocator()->get('ReportColumns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReportColumns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
