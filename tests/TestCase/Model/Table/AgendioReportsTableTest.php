<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AgendioReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AgendioReportsTable Test Case
 */
class AgendioReportsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var AgendioReportsTable
     */
    public $AgendioReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AgendioReports',
        'app.AgendioColumns',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AgendioReports') ? [] : ['className' => AgendioReportsTable::class];
        $this->AgendioReports = TableRegistry::getTableLocator()->get('AgendioReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AgendioReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
