<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestNotificationsToFilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestNotificationsToFilesTable Test Case
 */
class RequestNotificationsToFilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestNotificationsToFilesTable
     */
    public $RequestNotificationsToFiles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestNotificationsToFiles',
        'app.RequestNotifications',
        'app.Files',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestNotificationsToFiles') ? [] : ['className' => RequestNotificationsToFilesTable::class];
        $this->RequestNotificationsToFiles = TableRegistry::getTableLocator()->get('RequestNotificationsToFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestNotificationsToFiles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
