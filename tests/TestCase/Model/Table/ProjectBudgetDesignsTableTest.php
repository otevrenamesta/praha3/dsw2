<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectBudgetDesignsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectBudgetDesignsTable Test Case
 */
class ProjectBudgetDesignsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectBudgetDesignsTable
     */
    public $ProjectBudgetDesigns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProjectBudgetDesigns',
        'app.Programs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProjectBudgetDesigns') ? [] : ['className' => ProjectBudgetDesignsTable::class];
        $this->ProjectBudgetDesigns = TableRegistry::getTableLocator()->get('ProjectBudgetDesigns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectBudgetDesigns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
