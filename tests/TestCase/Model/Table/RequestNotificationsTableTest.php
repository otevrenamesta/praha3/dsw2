<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestNotificationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestNotificationsTable Test Case
 */
class RequestNotificationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestNotificationsTable
     */
    public $RequestNotifications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RequestNotifications',
        'app.Requests',
        'app.RequestNotificationTypes',
        'app.RequestNotificationsToFiles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RequestNotifications') ? [] : ['className' => RequestNotificationsTable::class];
        $this->RequestNotifications = TableRegistry::getTableLocator()->get('RequestNotifications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RequestNotifications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
