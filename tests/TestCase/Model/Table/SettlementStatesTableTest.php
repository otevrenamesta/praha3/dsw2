<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementStatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementStatesTable Test Case
 */
class SettlementStatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementStatesTable
     */
    public $SettlementStates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementStates',
        'app.Settlements',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementStates') ? [] : ['className' => SettlementStatesTable::class];
        $this->SettlementStates = TableRegistry::getTableLocator()->get('SettlementStates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementStates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
