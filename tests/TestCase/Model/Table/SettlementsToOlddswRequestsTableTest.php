<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SettlementsToOlddswRequestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SettlementsToOlddswRequestsTable Test Case
 */
class SettlementsToOlddswRequestsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var SettlementsToOlddswRequestsTable
     */
    public $SettlementsToOlddswRequests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SettlementsToOlddswRequests',
        'app.Settlements',
        'app.Zadosti',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettlementsToOlddswRequests') ? [] : ['className' => SettlementsToOlddswRequestsTable::class];
        $this->SettlementsToOlddswRequests = TableRegistry::getTableLocator()->get('SettlementsToOlddswRequests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SettlementsToOlddswRequests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
