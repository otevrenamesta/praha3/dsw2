<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SettlementAttachmentsFixture
 */
class SettlementAttachmentsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'settlement_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'settlement_item_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'settlement_attachment_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'is_public' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'file_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'settlement_attachment_type_id' => ['type' => 'index', 'columns' => ['settlement_attachment_type_id'], 'length' => []],
            'settlement_id' => ['type' => 'index', 'columns' => ['settlement_id'], 'length' => []],
            'settlement_item_id' => ['type' => 'index', 'columns' => ['settlement_item_id'], 'length' => []],
            'file_id' => ['type' => 'index', 'columns' => ['file_id'], 'length' => []],
            'is_public' => ['type' => 'index', 'columns' => ['is_public'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'settlement_attachments_ibfk_1' => ['type' => 'foreign', 'columns' => ['settlement_id'], 'references' => ['settlements', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'settlement_attachments_ibfk_2' => ['type' => 'foreign', 'columns' => ['settlement_item_id'], 'references' => ['settlement_items', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'settlement_attachments_ibfk_3' => ['type' => 'foreign', 'columns' => ['settlement_attachment_type_id'], 'references' => ['settlement_attachment_types', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'settlement_attachments_ibfk_4' => ['type' => 'foreign', 'columns' => ['file_id'], 'references' => ['files', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'settlement_id' => 1,
                'settlement_item_id' => 1,
                'settlement_attachment_type_id' => 1,
                'is_public' => 1,
                'file_id' => 1,
                'modified' => '2020-10-15 00:18:50',
                'created' => '2020-10-15 00:18:50',
            ],
        ];
        parent::init();
    }
}
