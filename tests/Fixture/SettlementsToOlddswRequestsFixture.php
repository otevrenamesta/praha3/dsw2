<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SettlementsToOlddswRequestsFixture
 */
class SettlementsToOlddswRequestsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'settlement_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'zadost_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'settlement_id' => ['type' => 'index', 'columns' => ['settlement_id'], 'length' => []],
            'zadost_id' => ['type' => 'index', 'columns' => ['zadost_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'settlements_to_olddsw_requests_ibfk_1' => ['type' => 'foreign', 'columns' => ['settlement_id'], 'references' => ['settlements', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'settlements_to_olddsw_requests_ibfk_2' => ['type' => 'foreign', 'columns' => ['zadost_id'], 'references' => ['zadosti', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'settlement_id' => 1,
                'zadost_id' => 1,
                'modified' => '2020-10-15 00:19:09',
            ],
        ];
        parent::init();
    }
}
