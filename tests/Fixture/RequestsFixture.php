<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequestsFixture
 */
class RequestsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'organization_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'program_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_czech_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'is_locked' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => 'zda je mozne zadost aktualne upravovat', 'precision' => null],
        'lock_when' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'budouci datum kdy se automaticky zamkne, kvuli formalni kontrole', 'precision' => null],
        'is_reported' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => 'bylo provedeno ohlaseni akce?', 'precision' => null],
        'is_settled' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => 'bylo provedeno vyuctovani dotace?', 'precision' => null],
        'final_subsidy_amount' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'finalni castka schvalena a pridelena např. zastupitelstvem', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'appeal_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'konkretni vyzva ve ktere si zadatel vybral obsazeny program', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'organization_id' => ['type' => 'index', 'columns' => ['organization_id'], 'length' => []],
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'program_id' => ['type' => 'index', 'columns' => ['program_id'], 'length' => []],
            'appeal_id' => ['type' => 'index', 'columns' => ['appeal_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'requests_ibfk_1' => ['type' => 'foreign', 'columns' => ['appeal_id'], 'references' => ['appeals', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'requests_ibfk_2' => ['type' => 'foreign', 'columns' => ['organization_id'], 'references' => ['organizations', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'requests_ibfk_3' => ['type' => 'foreign', 'columns' => ['program_id'], 'references' => ['programs', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'requests_ibfk_4' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_czech_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'organization_id' => 1,
                'user_id' => 1,
                'program_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'is_locked' => 1,
                'lock_when' => '2020-05-18 11:10:05',
                'is_reported' => 1,
                'is_settled' => 1,
                'final_subsidy_amount' => 1,
                'modified' => '2020-05-18 11:10:05',
                'created' => '2020-05-18 11:10:05',
                'appeal_id' => 1,
            ],
        ];
        parent::init();
    }
}
