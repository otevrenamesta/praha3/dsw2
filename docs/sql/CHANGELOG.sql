-- 2025-01-01 --
INSERT INTO i18n_messages (`domain`, `locale`, `singular`) VALUES
  ('default', 'cs', 'Komentář žadatele k vyúčtování'),
  ('default', 'cs', 'Náklady celkem – skutečně vynaložené celkové náklady na projekt');

ALTER TABLE `settlement_attachments`
  ADD `approved` tinyint(1) NULL DEFAULT '0' AFTER `is_public`,
  ADD `approved_note` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `approved`;

-- 2024-11-26 --
ALTER TABLE `programs`
CHANGE `name` `name` text COLLATE 'utf8mb4_czech_ci' NOT NULL AFTER `merging_of_requests`;

-- 2024-10-25 --
INSERT INTO `settlement_attachment_types` (`id`, `type_name`, `order`, `modified`, `created`)
    VALUES ('9', 'Smlouva', '9', now(), now());

-- 2024-10-25 --
ALTER TABLE `settlements`
    ADD `expenses_total` decimal(18,2) NULL AFTER `submitted`;

ALTER TABLE `settlements`
    ADD `income_total` decimal(18,2) NULL AFTER `expenses_total`;

-- 2024-10-24 --
ALTER TABLE `organizations` ADD `test_id` int(11) NULL;

-- 2024-05-24 --
ALTER TABLE `settlement_items`
  ADD `budget_item` varchar(64) COLLATE 'utf8mb4_czech_ci' NULL AFTER `approved_note`;

-- 2024-05-10 --
INSERT INTO `settlement_states` (`id`, `name`, `modified`, `created`)
  VALUES (6, 'Vyúčtování nevyhovuje podmínkám', now(), now());

ALTER TABLE `settlements`
ADD `rejected_statement` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `econom_statement`;

ALTER TABLE `settlements`
  ADD `repair_date` date NULL AFTER `return_reason`;

ALTER TABLE `settlement_items`
  ADD `approved` tinyint(1) NULL DEFAULT '0' AFTER `order_number`,
  ADD `approved_note` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `approved`;

ALTER TABLE `programs`
  ADD `settlement_date` date NULL AFTER `description`;

ALTER TABLE `settlements`
  ADD `settlement_date_email` tinyint(1) NULL DEFAULT '0' AFTER `additional_comment`;

-- 2024-01-10 --
ALTER TABLE `settlement_items`
  ADD `own_amount` decimal(18,2) NOT NULL DEFAULT '0'
  AFTER `original_amount`;

-- 2023-12-28 --
ALTER TABLE `appeals`
  ADD `begin_sent` datetime NULL DEFAULT NULL
  AFTER `open_to`;

ALTER TABLE `appeals`
  ADD `end_sent` datetime NULL DEFAULT NULL
  AFTER `begin_sent`;

CREATE TABLE `teams_messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8mb4_czech_ci,
  `organization_id` int NOT NULL,
  `recipient_user_id` int NOT NULL,
  `response_to_id` int NULL DEFAULT NULL,
  `user_id` int NOT NULL,
  `readed` datetime DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp(),
PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `recipient_user_id` (`recipient_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- 2023-12-27 --
ALTER TABLE `request_logs`
  ADD `is_history` tinyint(1) NOT NULL DEFAULT '0'
  AFTER `executed_by_user_id`;

-- 2023-12-12 --
ALTER TABLE `programs`
  ADD `merging_of_requests` tinyint(1) NOT NULL DEFAULT '0'
  AFTER `not_require_tax_documents`;

ALTER TABLE `requests`
  ADD `merged_request` int(11) DEFAULT NULL
  AFTER `request_comment`;

-- 2023-11-26 --
ALTER TABLE `programs`
  ADD `not_require_tax_documents` tinyint(1) NOT NULL DEFAULT '0'
  AFTER `requires_pre_evaluation`;

-- 2023-11-22 --
INSERT
  INTO `form_field_types` (`id`, `name`, `weight`)
  VALUES ('17', 'Odpověď ANO/NE s vložením přílohy', '0');

-- 2023-05-21 --
UPDATE agendio_columns
  SET can_have_organization_preset=1
  WHERE export_name
  IN ('kod_pripad', 'cislo_smlouvy', 'kod_subjekt_role', 'id_organ', 'nazev_smlouva', 'doba_neurcita');

-- 2023-05-16 --
CREATE TABLE `identity_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `identity_attachment_type_id` int(11) NOT NULL,
  `description` tinytext NOT NULL,
  `is_archived` tinyint(1) NOT NULL DEFAULT 0,
  `file_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `identity_attachment_type_id` (`identity_attachment_type_id`),
  KEY `identity_id` (`user_id`),
  KEY `file_id` (`file_id`),
  KEY `is_public` (`is_public`),
  CONSTRAINT `identity_attachments_ibfk_3` FOREIGN KEY (`identity_attachment_type_id`) REFERENCES `identity_attachment_types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `identity_attachments_ibfk_4` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_attachments_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;


CREATE TABLE `identity_attachment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- 2023-05-01 --
ALTER TABLE `programs`
ADD `requires_pre_evaluation` tinyint(1) NOT NULL AFTER `requires_extended_budget`;

ALTER TABLE `requests`
ADD `pre_evaluation` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `purpose`;

-- 2023-03-31 --
ALTER TABLE `forms`
  ADD `shared` tinyint(1) NULL AFTER `allow_change`;

-- 2023-03-08 --
ALTER TABLE `settlements`
  ADD `submitted` timestamp NULL AFTER `additional_comment`;

-- 2023-02-18 --
ALTER TABLE `requests`
  ADD `settlement_date` date NULL AFTER `request_comment`;

-- 2023-02-10 --
ALTER TABLE `requests`
  ADD `de_minimis_comment` text COLLATE 'utf8mb4_czech_ci' NULL COMMENT 'poznamka k de minimis' AFTER `de_minimis`,
  ADD `public_control` tinyint(1) NULL DEFAULT '0' COMMENT 'probehla verejna kontrola' AFTER `de_minimis_comment`,
  ADD `public_control_ok` tinyint(1) NULL DEFAULT '0' COMMENT 'verejna kontrola ok' AFTER `public_control`,
  ADD `public_control_comment` text COLLATE 'utf8mb4_czech_ci' NULL COMMENT 'poznamka ke kontrole' AFTER `public_control_ok`,
  ADD `request_comment` text COLLATE 'utf8mb4_czech_ci' NULL COMMENT 'poznamka k zadosti' AFTER `public_control_comment`;

ALTER TABLE `files`
  ADD `file_type` varchar(255) COLLATE 'utf8mb4_czech_ci' NULL AFTER `original_filename`;

-- 2023-01-22 --
ALTER TABLE `users`
  ADD `new_email` varchar(255) COLLATE 'utf8mb4_czech_ci' NULL AFTER `is_enabled`,
  ADD `original_email` varchar(255) COLLATE 'utf8mb4_czech_ci' NULL AFTER `new_email`;

-- 2023-01-11 --
CREATE TABLE `payments_to_files` (
  `id` INT NOT NULL auto_increment,
  `payment_id` INT NOT NULL,
  `file_id` INT NOT NULL,
  `modified` DATETIME DEFAULT NULL,
  PRIMARY KEY ( `id` ),
  KEY `payment_id` ( `payment_id` ),
  KEY `file_id` ( `file_id` ),
  CONSTRAINT `payments_to_files_ibfk_1` FOREIGN KEY ( `file_id` ) REFERENCES `files` ( `id` ) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payments_to_files_ibfk_2` FOREIGN KEY ( `payment_id` ) REFERENCES `payments` ( `id` ) ON DELETE CASCADE ON UPDATE CASCADE
) engine = innodb DEFAULT charset = utf8mb4 COLLATE = utf8mb4_czech_ci;

ALTER TABLE `payments` ADD `in_addition` tinyint(1) NULL DEFAULT '0' AFTER `is_refund`;
ALTER TABLE `payments` ADD `comment` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `in_addition`;

ALTER TABLE `requests` ADD `original_subsidy_amount` int NULL AFTER `final_subsidy_amount`;

-- 2022-12-20 --
INSERT INTO `settlement_attachment_types` (`id`, `type_name`, `order`, `modified`, `created`) VALUES ('8', 'Dofinancování', '8', now(), now());

ALTER TABLE `settlements` ADD `additional_financing` float NULL AFTER `anynonym_mask`;
ALTER TABLE `settlements` ADD `additional_comment` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `additional_financing`;

ALTER TABLE `requests` ADD `additional_financing` int NULL AFTER `final_subsidy_amount`;

-- 2022-10-12 --
ALTER TABLE `evaluation_criteria` ADD `min_points` int NOT NULL COMMENT 'minimální počet bodů' AFTER `description`;

-- 2022-06-06 --
CREATE TABLE `request_filled_fields_changes` ( `id` int(11) NOT NULL, `change_id` int(11) NOT NULL, `request_id` int(11) NOT NULL, `form_id` int(11) NOT NULL, `form_field_id` int(11) NOT NULL, `value` text COLLATE utf8mb4_czech_ci DEFAULT NULL, `modified` datetime DEFAULT NULL, `created` datetime DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci

-- 2022-06-06 --
ALTER TABLE `forms` ADD `allow_change` BOOLEAN NULL DEFAULT NULL COMMENT 'Povolit změny ve formuláři u podaných žádostí' AFTER `weight`;

-- 2022-05-24 --
-- missing AUTO_INCREMENT in table `programs`
ALTER TABLE programs MODIFY id INTEGER NOT NULL AUTO_INCREMENT;

-- 2022-04-08 --
-- new column for user comments in the settlement

ALTER TABLE `settlements`
    ADD `user_comment` text COLLATE 'utf8mb4_czech_ci' NULL AFTER `econom_statement`;

-- 2022-03-17 --
-- public income history with optional source name

ALTER TABLE `public_income_histories`
ADD `public_income_source_name` varchar(255) COLLATE 'utf8mb4_czech_ci' NOT NULL
AFTER `public_income_source_id`;

-- 2022-02-17 --
-- Multiple teams having a role in a program

ALTER TABLE dsw2.programs DROP FOREIGN KEY programs_ibfk_4;
ALTER TABLE `programs`
  DROP `formal_check_team_id`,
  DROP `price_proposal_team_id`,
  DROP `price_approval_team_id`,
  DROP `comments_team_id`,
  DROP `request_manager_team_id`,
  DROP `preview_team_id`;

--
-- Struktura tabulky `programs_roles`
--

CREATE TABLE `programs_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `modified` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci COMMENT='Statický číselník, který musí odpovídat implementaci';

--
-- Vypisuji data pro tabulku `programs_roles`
--

INSERT INTO `programs_roles` (`id`, `name`, `modified`) VALUES
(1, 'Formální kontrola', '2022-02-11 14:46:25'),
(2, 'Hodnotitelé', '2022-02-11 14:46:25'),
(3, 'Navrhovatelé', '2022-02-11 14:46:25'),
(4, 'Schvalovatelé', '2022-02-11 14:46:25'),
(5, 'Finalizace', '2022-02-11 14:46:25'),
(6, 'Náhled', '2022-02-11 14:46:25');

--
-- Struktura tabulky `teams_roles_in_programs`
--

CREATE TABLE `teams_roles_in_programs` (
  `id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `program_role_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `modified` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci COMMENT='Role přiřazené jednotlivým týmům v jednotlivých programech';

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `teams_roles_in_programs`
--
ALTER TABLE `teams_roles_in_programs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Program` (`program_id`),
  ADD KEY `Team` (`team_id`),
  ADD KEY `Role in program` (`program_role_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `teams_roles_in_programs`
--
ALTER TABLE `teams_roles_in_programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `teams_roles_in_programs`
--
ALTER TABLE `teams_roles_in_programs`
  ADD CONSTRAINT `Program` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Role in program` FOREIGN KEY (`program_role_id`) REFERENCES `programs_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Team` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;


-- 2022-01-09 --
-- settlements publishing and anonymisation
ALTER TABLE `settlements` ADD `published` BOOLEAN NOT NULL DEFAULT FALSE COMMENT 'Příznak zda je vyúčtování zveřejněno' AFTER `econom_statement`, ADD `anynonym_mask` MEDIUMTEXT NULL DEFAULT NULL COMMENT 'Serializovaná data pro anonymizaci vyúčtování' AFTER `published`;

-- 2021-11-30
-- exception for organisation with ID 14 - show "time" in the appeal records
INSERT INTO `organization_settings` (`organization_id`, `name`, `value`, `modified`, `created`)
  VALUES ('14', 'appeals.open_to.with_time', '1', NOW(), NOW());

  -- 2021-11-29
-- exception for organisation with ID 14 - show "histories" records in the identity
INSERT INTO `organization_settings` (`organization_id`, `name`, `value`, `modified`, `created`)
  VALUES ('14', 'identity.history.show_histories', '1', NOW(), NOW());

-- 2021-11-25
ALTER TABLE `realms` ADD `weight` INT NULL DEFAULT '0' COMMENT 'Váha určuje pořadí' AFTER `description`;

-- 2021-11-24
ALTER TABLE `programs` ADD `weight` INT NULL DEFAULT '0' COMMENT 'Váha určuje pořadí' AFTER `description`;

-- 2021-11-24
-- rename individual domain records from "org_ID" to "default_ID"
UPDATE i18n_messages
SET domain = REPLACE(domain, 'org', 'default')
WHERE POSITION("_" IN domain) > 0;

-- id  - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_1") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_1");

-- id 7 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_7") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_7");

-- id 8 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_8") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_8");

-- id 9 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_9") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_9");

-- id 10 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_10") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_10");

-- id 11 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_11") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_11");

-- id 12 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_12") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_12");

-- id 13 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_13") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_13");

-- id 14 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_14") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_14");

-- id 15 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_15") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_15");

-- id 16 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_16") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_16");

-- id 17  - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_17") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_17");

-- id 18 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_18") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_18");

-- id 20 - copy texts from default/email
INSERT INTO i18n_messages (`domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2`)
SELECT CONCAT (domain, "_20") AS `domain`, `locale`, `singular`, `plural`, `context`, `value_0`, `value_1`, `value_2` FROM `i18n_messages`
WHERE value_0 IS NOT NULL AND domain IN ('default', 'email') AND singular NOT IN (SELECT singular FROM i18n_messages WHERE domain = "default_20");
--

-- 2021-11-18
ALTER TABLE `requests` ADD `published` INT NOT NULL DEFAULT '0' COMMENT 'Příznak zda je žádost publikovaná pro veřejnost.' AFTER `de_minimis`, ADD `anynonym_mask` TEXT NULL DEFAULT NULL COMMENT 'Serializovaná Data použitá pro anonymizaci žádosti' AFTER `published`;

-- 2021-11-16
ALTER TABLE `requests`
  ADD `de_minimis` tinyint(1) NULL AFTER `final_evaluation`;

-- 2021-11-08
ALTER TABLE `appeals_to_programs` CHANGE `max_support` `max_support` int(11) NULL DEFAULT '100' AFTER `max_request_budget`

-- 2021-10-22
ALTER TABLE `appeals_to_programs`
  ADD `max_support` INT UNSIGNED NOT NULL DEFAULT '100' AFTER `max_request_budget`;

ALTER TABLE `project_budget_designs` ADD `exclusive_to` INT NULL DEFAULT NULL COMMENT 'Design formuláře specifický pro konkrétní organizaci' AFTER `name`;

ALTER TABLE `project_budget_designs`
  ADD KEY `exclusive_to` (`exclusive_to`)

ALTER TABLE `project_budget_designs`
  ADD CONSTRAINT `project_budget_designs_ibfk_1` FOREIGN KEY (`exclusive_to`) REFERENCES `organizations` (`id`) ON UPDATE CASCADE

COMMIT

-- 2021-10-22
INSERT INTO `form_field_types` (`id`, `name`, `weight`)
  VALUES ('14', 'Vícepoložkový výběr z hodnot', '0');

-- 2021-10-19
ALTER TABLE `forms`
  ADD `name_displayed` varchar(255) COLLATE 'utf8mb4_czech_ci' NOT NULL AFTER `name`;
