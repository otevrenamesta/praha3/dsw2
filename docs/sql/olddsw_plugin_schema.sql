-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost
-- Vytvořeno: Stř 18. lis 2020, 10:07
-- Verze serveru: 10.5.8-MariaDB-1:10.5.8+maria~buster
-- Verze PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `dsw2`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `settlements_to_olddsw_requests`
--

CREATE TABLE `settlements_to_olddsw_requests` (
  `id` int(11) NOT NULL,
  `settlement_id` int(11) NOT NULL,
  `zadost_id` int(10) UNSIGNED NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `users_to_olddsw_accounts`
--

CREATE TABLE `users_to_olddsw_accounts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ucet_id` int(10) UNSIGNED NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `settlements_to_olddsw_requests`
--
ALTER TABLE `settlements_to_olddsw_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settlement_id` (`settlement_id`),
  ADD KEY `zadost_id` (`zadost_id`);

--
-- Klíče pro tabulku `users_to_olddsw_accounts`
--
ALTER TABLE `users_to_olddsw_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `ucet_id` (`ucet_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `settlements_to_olddsw_requests`
--
ALTER TABLE `settlements_to_olddsw_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `users_to_olddsw_accounts`
--
ALTER TABLE `users_to_olddsw_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `settlements_to_olddsw_requests`
--
ALTER TABLE `settlements_to_olddsw_requests`
  ADD CONSTRAINT `settlements_to_olddsw_requests_ibfk_1` FOREIGN KEY (`settlement_id`) REFERENCES `settlements` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `settlements_to_olddsw_requests_ibfk_2` FOREIGN KEY (`zadost_id`) REFERENCES `dsw`.`zadosti` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `users_to_olddsw_accounts`
--
ALTER TABLE `users_to_olddsw_accounts`
  ADD CONSTRAINT `users_to_olddsw_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_to_olddsw_accounts_ibfk_2` FOREIGN KEY (`ucet_id`) REFERENCES `dsw`.`ucty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
