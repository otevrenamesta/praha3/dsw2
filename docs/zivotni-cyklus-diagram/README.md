# Diagram životního cyklu žádosti

### Legenda

  - Oranžově - stav, ve kterém se žádost může nacházet
  - Tmavě zeleně - proces, který skrze DSW2, provádí žadatel
  - Světle modře - proces, který skrze DSW2, provádí zaměstnanec dotačního úřadu
  - Světle zeleně - úspěšný výsledek procesu
  - Červeně - záporný / negativní výsledek procesu
  - Tmavě modrá - Tým / Organizační jednotky dotačního úřadu - popis týmu odpovídá popisu role v DSW2


### soubory .drawio

V adresáři jsou jak obrazové exporty, tak zdrojové soubory .drawio, aby se nechal diagram později upravit

Soubor jednoduše nahrajete do webové aplikace https://app.diagrams.net/ a můžete upravovat, případně exportovat v jiném formátu (PDF, SVG, ...)

