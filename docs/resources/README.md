# Data převzatá z jiných zdrojů, při tvorbě projektu

## Číselníky Českého Statistického Úřadu ČSÚ

zdroj: https://apl.czso.cz/iSMS/cislist.jsp

kód. 42		- CISOBE - Část obce					- CIS0042_CS.utf8.csv
kód. 43		- CISOB - Obec a vojenský újezd				- CIS0043_CS.utf8.csv
kód. 44		- CISMC - Městský obvod, městská část			- CIS0044_CS.utf8.csv
kód. 60		- COBCE - Část obce díl					- CIS0060_CS.utf8.csv
kód. 86		- CZEM - Číselník zemí					- CIS0086_CS.utf8.csv
kód. 149	- ROSFORMA - Právní forma registru osob (ROS)		- CIS0149_CS.utf8.csv
kód. 2755	- AGRFORMA - Právní forma organizace - agregace		- CIS2755_CS.utf8.csv

nad číselníky provést konverzi do UTF-8
např. "iconv -f cp1250 -t utf8 CIS0042_CS.csv > CIS0042_CS.utf8.csv"

vložit do DB pomocí přiložených SQL nebo použít [/docs/sql/schema.sql](../docs/sql/schema.sql),
kde už data jsou součástí základního schématu

ciselnik 149 je potreba jeste doplnit o historicke zaznamy, jiz neplatne pravni formy
k tomu slouzi jednoduchy insert ignore v souboru CIS0149_CS.historicke.sql

pak je třeba vložit vazbu mezi číselníky 42+44 (části obce, městské části) a 43 (obec a vojenský újezd)
