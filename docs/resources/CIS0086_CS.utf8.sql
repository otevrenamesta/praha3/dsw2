CREATE TABLE IF NOT EXISTS `csu_countries` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `number` varchar(38) COLLATE utf8mb4_czech_ci NOT NULL,
 `short_name` varchar(42) COLLATE utf8mb4_czech_ci NOT NULL,
 `name` varchar(64) COLLATE utf8mb4_czech_ci NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `number` (`number`),
 FULLTEXT KEY `short_name` (`short_name`),
 FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci

LOAD DATA INFILE 'CIS0086_CS.utf8.csv' INTO TABLE csu_countries FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS (@i, @i, @i, @number, @short_name, @long_name, @i, @i) SET id=NULL, short_name=@short_name, name=@long_name, number=@number;
