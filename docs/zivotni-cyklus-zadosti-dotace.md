# Životní cyklus žádosti o dotaci

Pro zjednodušení pochopení je k dispozici diagram v podsložce [zivotni-cyklus-diagram](../docs/zivotni-cyklus-diagram/)

### Předpoklady

1. Musí být alespoň 1 aktivní výzva k podání žádostí
2. Uživatel musí být povolen a mít kompletní identifikaci FO/FPO/PO

### Možné nastavení

1. Zda žádosti podstupují formální kontrolu
2. Zda žádosti podstupují hodnocení dle definovaných hodnotících kritérií
3. Který orgán má právo navrhovat výši dotace
4. Který orgán má právo udělovat slovní hodnocení žádosti, které pak je součástí vyrozumění o (ne)přidělení dotace
5. Který orgán má právo schvalovat (a zároveň i upravovat) finální výši dotace a slovní hodnocení
6. Který orgán má právo nastavovat zda konkrétní žádost byla vyřízena kladně nebo záporně
7. Který orgán má právo potvrzovat přijatá ohlášení akce/díle
8. Který orgán má právo potvrzovat přijatá vyúčtování dotace a jejich správnost

## Příklady
pro ilustraci možného dotačního procesu, který DSW2 podporuje, 2 příklady možných variant, které vycházejí ze specifického nastavení dotačního portálu,
a proces žádosti, tak jak probíhá na Praze 3, který systém podporuje

### Příklad: Nejjednodušší dotační proces

1. Žadatel odešle kompletní žádost systémem
2. Neprobíhá žádná formální kontrola, ani hodnocení (např. před jednáním výboru/komise)
3. Jediný orgán (např. ředitel odboru nebo předseda dotačního výboru) definuje částky, které schvaluje zastupitelstvo, a neuděluje slovní hodnocení
4. Stejný orgán pak provede nastavení výsledku jednání zastupitelstva/rady, tj. zda byly žádosti vyřízeny kladně nebo záporně
5. Proběhne odeslání vyrozumění o rozhodnutí o dotační žádosti (datová schránka, spisová služba nebo e-mail)
5. Nepožaduje se ohlášení akcí ani vyúčtování dotace, proces se dále řeší mimo DSW2 (smlouva, veřejnoprávní kontrola, atp.)

### Příklad: Velmi složitý dotační proces

1. Žadatel odešle kompletní žádost systémem
2. Probíhá formální kontrola, týmem formálních kontrolorů
3. Proběhne hodnocení, podle definovaných hodnotících kritérií, týmem hodnotitelů
4. Orgán A (např. referent odboru) navrhne přidělené částky 
5. Orgán B (např. komise) upraví navržené částky a udělí slovní hodnocení
6. Orgán C (např. tajemník dotačního výboru) upraví navržené částky a slovní hodnocení (např. gramatiku)
7. Orgán D (např. předseda dotačního výboru) upraví navržené částky a slovní hodnocení (např. o usnesení/názor výboru)
8. Orgán E (např. ředitel odboru) upraví účel na který je dotace poskytnuta, bude vycházet z účelu uvedeného v žádosti, pokud byl popsán
9. Zastupitelstvo/Rada schválí navrženou výši všech dotací vč. odůvodnění (slovního hodnocení) a příp. upraveného účelu
10. Orgán F (např. starostka) provede nastavení rozhodnutí zastupitelstva/rady v DSW2 a odešle vyrozumění
11. Žadatel (mimo elektronický proces v DSW2) podepíše smlouvu o veřejné podpoře 
12. Žadatel provede 1 a více ohlášení akce/činnosti skrze DSW2
13. Žadatel provede vyúčtování udělené dotace v DSW2
14. Orgán G (např. referent ekonomického oddělení) potvrdí správnost vyúčtování dotace 


### Originální postup (Praha 3), který systém podporuje


#### 1: Před komisí

1. Žádost je zformulována
2. Žádost je podána
3. Projde formální kontrolou - role Formální Kontrolor v dané Organizaci
  1. pokud jsou shledány závady, kontrolor určí datum do kdy musí být provedeny opravy/úpravy, pridá svou poznámku (instrukce k opravě) a vrátí žádost do kroku (1)
  2. kontrolu a vrácení lze provést i po ukončení období pro podání žádostí, dle nastavení výzvy
4. Všechny žádosti, které formální kontrola uzná jako platné, postupují dále ke komisi

#### 2: Hodnocení a Komise

1. Jednotliví členové komise (role Hodnotitel) mají možnost do termínu uzavření hodnocení nebo do chvíle kdy předseda/tajemník komise uzavře daný program/oblast a zapíše do systému výsledky jednání komise, možnost hodnotit
  1. hodnocení není povinné, pokud žádost nebude hodnocena, přesto ji komise může projednat a schválit
  2. hodnocení lze upravit i po uložení, dokud není hodnocení uzavřeno
  3. Z jednotlivých hodnocení se dělá aritmetický průměr (2 desetinná místa) a tento průměr a počet hodnotitelů, kteří se vyjádřili, je pak podkladem pro jednání komise
  4. Termín uzavření hodnocení může být nastaven dopředu na konkrétní vteřinu, nebo se možnost vkládat a upravovat hodnocení ukončí ve chvíli, kdy komise dokončí své jednání a v systému zaeviduje výsledky svého rozhodování
2. Podkladem pro jednání komise je seznam žádostí s informací o průměrně dosažené známce v každém jednotlivém hodnoceném kritériu a celkový počet dosažených bodů a informací o účelu a žadateli
3. Komise se shodne na zapsaném výsledku hodnocení a slovním odůvodněním pro jednotlivé žádosti
  1. tj. počet bodů z individuálních hodnocení bude komisí potvrzen nebo jakkoli upraven
  2. ke každé žádosti komise sepíše slovní odůvodnění svého rozhodnutí
4. Výsledek jednání komise pomocí svého předsedy nebo tajemníka zapíše do systému

#### 3: Výbor a návrh výše dotace

1. Po ukončení jednání komise, se žádostmi zabývá dotační výbor, který navrhuje konkrétní výši finanční podpory, na základě informací uvedených v žádosti (rozpočet, požadovaná částka) a dosaženého hodnocení (výsledek jednání komise)
2. Dotační výbor potvrzuje nebo upravuje dosavadní výsledek jednání komise, rozhodnutí komise není pro výbor závazné
3. Výbor taktéž může upravit účely, na které finanční podporu schvaluje, které se mohou lišit od účelů, které žadatel uvedl v žádosti
4. Všechny žádosti u kterých výbor navrhl nenulovou finanční podporu jdou následně jako podklad pro jednání rady a zastupitelstva

#### 4: Usnesení zastupitelstva

1. Jakmile jsou konkrétní žádosti zastupitelstvem schváleny, na základě příslušného usnesení, se odesílá žadatelům vyrozumění o udělení nebo neudělení dotace
2. Pokud má jednotlivý žadatel více žádostí, je mu zaslána souhrnná informace za všechny žádosti, které podal, s příslušnou informací jaká podpora mu byla přiznána, případně odůvodněním komise/výboru/zastupitelstva

#### 5: Ohlašování činnosti / akce / díla

1. Úspěšný žadatel má povinnost ohlásit celoroční činnost, jednotlivou akci nebo realizaci díla, na které mu byla poskytnuta finanční podpora
2. Ohlášení těchto se provádí opět v systému, který následně reflektuje v reportech a datových sestavách, že žadatel naplnil povinnosti dle pravidel dotační politiky

#### 6. Vyúčtování dotace

(zatím nerealizovaná, ale plánovaná funkce)
1. Žadatel samostatně nebo na výzvu v DSW2 vyplní vyúčtování
2. Člen ekonomického oddělení potvrdí správnost a přijetí vyúčtování skrze DSW2
