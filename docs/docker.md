# Docker

## První spuštění

```bash
docker-compose up -d
```

## Zastavení služby

```bash
docker-compose stop
```
## Smazání služby i s DATY!!!

```bash
docker-compose down
# smazání již nepoužívaných datových volumes
docker volume prune
```

## Vývoj s dockerem

(1) spouštět docker-compose s .dev konfigurací takto
```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
```

(2) Pro lokální přístup můžete využít i jméno kontajneru nebo jeho ID
spuštěním skriptu (pro Linux pouze) se bude aktualizovat lokální mapping docker-ip(privátní) vs. záznam v /etc/hosts

```bash
./docker/run-hoster.sh
```

## Spuštění na produkci

  - nastavit bezpečnější hesla
  - nastavit SECURITY_SALT
  - nastavit DEBUG=false
  - zajistit persistentní uložiště pro databázi, Redis a soubory nahrané na web
  - sledovat nově vydané změny a průběžně aktualizovat
