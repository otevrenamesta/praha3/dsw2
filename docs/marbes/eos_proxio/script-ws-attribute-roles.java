import java.util.ArrayList;
import java.util.List;
import cz.marbes.eos4.dataobject.application.TreeRangeValue;

List<Object> params = new ArrayList<Object>();

wsResp = DYNAMIC_WS_CALL_SERVICE.callWS("roles", params.toArray(), MODEL);

result = new ArrayList<TreeRangeValue>();

Iterator items = wsResp.getItem().iterator();

for (Object o : items) {

    TreeRangeValue rangeValue = new TreeRangeValue();

    //throw new Exception(Arrays.toString(o.getClass().getDeclaredMethods()));

    rangeValue.setContent(o.getKey());
    rangeValue.setExternId(o.getKey());
    rangeValue.setTreeId(o.getKey());
    rangeValue.setDescription(o.getValue());

    result.add(rangeValue);
}

