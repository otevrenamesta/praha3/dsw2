# Integrace v EOS Proxio IdM

Po založení aplikace DSW2 v EOS je potřeba načíst 2 číselníky z DSW2

  - Role (statický číselník systémových rolí) - [skript script-ws-attribute-roles.java](../script-ws-attribute-roles.java)
  - Týmy (teams, organizační jednotky dotačního úřadu) - [skript script-ws-attribute-teams.java](../script-ws-attribute-teams.java)

## Postup zavedení atributů do Aplikace v EOS

  1. V atributech zavedené aplikace vytvořit nový atribut typu "Webservice atribut"
  2. URL pro načtení nastavit na https://dotace.praha3.cz/api/ws/v1/public
  3. Pojmenování a popis atributu je volitelný
  4. V záložce "Úprava skriptu" pak vložit obsah příslušného skriptu v tomto adresáři

Pokud vše proběhlo správně, v číselníku rolí uvidíte min. 6 rolí, které můžete uživateli zpřístupnit,
v číselníku týmů pak uvidíte jednotlivé týmy, které si vaše organizace v DSW2 vytvořila
