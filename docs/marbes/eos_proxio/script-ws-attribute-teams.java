import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Method;
import cz.marbes.eos4.dataobject.application.RangeValue;

List<Object> params = new ArrayList<Object>();

wsResp = DYNAMIC_WS_CALL_SERVICE.callWS("teams", params.toArray(), MODEL);

result = new ArrayList<RangeValue>();

Iterator rangeValues = wsResp.getItem().iterator();

for (Object o : rangeValues) {

    RangeValue rangeValue = new RangeValue();

    rangeValue.setContent(o.getId().toString());
    rangeValue.setExternId(o.getId().toString());

    String description = "";
    for (Method m : o.getClass().getDeclaredMethods()) {
        if (m.getParameterCount() == 0) {
          Object mVal = m.invoke(o, null);
          if (mVal.getClass().getName().contains("ArrayOfKeyValueElement")) {
            for (Object kve : mVal.getItem().iterator()) {
              description += kve.getValue() + "\n\n";
            }
          }
        }
    }

    if (!description.trim().isEmpty()) {
      description = "Kompetence v dotačních programech: \n" + description;
    }

    rangeValue.setDescription(o.getName() + ", \n" + description);

    result.add(rangeValue);
}
