// https://stackoverflow.com/a/25621277/492624
;$(function () {
    $('textarea').filter(function () {
        let $this = $(this);

        if ($this.data('data-noquilljs')) {
            return true;
        }

        return $this.css('display') !== 'none';


    }).each(function () {
        this.setAttribute('style', '');
        // removed: 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;'
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
});