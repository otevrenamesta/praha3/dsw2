(function () {
    jQuery.fn.dataTableExt.aTypes.unshift(
        function (data) {
            // Detect Czech currency number by " Kč"
            if (data.indexOf(' Kč') !== -1 || data.indexOf(';Kč') !== -1 ) {
                return 'cz-currency'
            } else {
                return null
            }
        }
    );
}());

/* Sorting */

_anyNumberSort = function (a, b, high) {
    var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
    a = a.replace(',', '.').match(reg);
    a = a !== null ? parseFloat(a[0]) : high;
    b = b.replace(',', '.').match(reg);
    b = b !== null ? parseFloat(b[0]) : high;
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
}

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "cz-currency-asc": function (a, b) {
        return _anyNumberSort(a, b, Number.POSITIVE_INFINITY);
    },
    "cz-currency-desc": function (a, b) {
        return _anyNumberSort(a, b, Number.NEGATIVE_INFINITY) * -1;
    }
});