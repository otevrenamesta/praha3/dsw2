// original from https://github.com/tangien/quilljs-textarea/blob/59daef9494ca76004f5fd98f7156dbb1486bf9a1/quill-textarea.js

function quilljs_textarea(elem = null, options = null) {
    let editorElems;
    if (elem) {
        editorElems = Array.prototype.slice.call(document.querySelectorAll(elem));
    } else {
        editorElems = Array.prototype.slice.call(document.querySelectorAll('[data-quilljs]'));
    }
    editorElems.forEach(function (el) {
        // added blacklist ability through data-noquilljs attribute
        if ((elem && el.hasAttribute("data-quilljs")) || el.hasAttribute('data-noquilljs')) {
            return;
        }
        const elemType = el.type;
        let placeholder = null;
        let editorDiv = el;
        if (elemType === 'textarea') {
            editorDiv = document.createElement('div');
            editorDiv.innerHTML = el.value;
            el.parentNode.insertBefore(editorDiv, el.nextSibling);
            el.style.display = "none";
            placeholder = el.placeholder;
        }
        const toolbarOptions = [
            // toggled buttons
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            // custom button values
            [{'header': 1}, {'header': 2}],
            [{'list': 'ordered'}, {'list': 'bullet'}],
            // superscript/subscript
            [{'script': 'sub'}, {'script': 'super'}],
            // outdent/indent
            [{'indent': '-1'}, {'indent': '+1'}],
            // custom dropdown
            [{'size': ['small', false, 'large', 'huge']}],
            [{'header': [1, 2, 3, 4, 5, 6, false]}],
            // dropdown with defaults from theme
            [{'color': []}, {'background': []}],
            [{'align': []}],
            ['link', 'image', 'video'],
            // remove formatting button
            ['clean'],
        ];
        let default_options = {
            modules: {
                toolbar: {
                    container: toolbarOptions,
                    handlers: {
                        image: imageHandler
                    }
                },
                keyboard: {
                    bindings: {
                        indent: {
                            key: 'Tab',
                            format: ['blockquote', 'indent', 'list'],
                            handler: (_range, _context) => {
                                // nop
                            }
                        }
                    }
                }
            }
        };
        if (!options) {
            default_options = Object.assign({}, default_options, {
                theme: 'snow',
                placeholder: placeholder,
            });
        } else {
            if (!options.placeholder) {
                options.placeholder = placeholder;
            }
            default_options = Object.assign({}, options, default_options);
        }

        const editor = new Quill(editorDiv, default_options);
        editor.on('text-change', function (delta, oldDelta, source) {
            el.value = editor.root.innerHTML;
        });
    });
}

function imageHandler() {
    let range = this.quill.getSelection();
    let value = prompt('Vložte odkaz na obrázek');
    if (value) {
        this.quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
    }
}

(function () {
    quilljs_textarea();
})();