#!/usr/bin/env bash
cd "$(dirname "$(dirname "$(realpath "$0")")")";

set -xe

./docker/wait-for-it.sh -h db.dsw2 -p 3306

bin/cake migrations migrate

/start.sh
