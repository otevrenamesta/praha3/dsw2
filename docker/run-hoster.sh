#!/bin/bash

# Linux compatible, Mac / Windows needs different syntax / tool

docker run -d \
    -v /var/run/docker.sock:/tmp/docker.sock \
    -v /etc/hosts:/tmp/hosts \
    dvdarias/docker-hoster
